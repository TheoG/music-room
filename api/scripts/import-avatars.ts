import * as fs from 'fs';
import * as path from 'path';
import * as Jimp from 'jimp';

const srcFolder: string = path.join(__dirname, '/../public/avatars/default-avatars/to-import');
const outFolder: string = path.join(__dirname, '/../public/avatars/default-avatars');

const MIN_IMG_DIM: number = 200;
const MAX_IMG_DIM: number = 500;

const PREFIX: string = 'avatar-';
const EXT: string = 'png';

const importNewImages = async () => {
	const imagesToImport: string[] = await fs.readdirSync(srcFolder).filter(filepath => fs.statSync(path.join(srcFolder, filepath)).isFile() && filepath != '.gitignore');
	if (!imagesToImport) {console.log('error reading directory ' + srcFolder); return;}
	
	// get filenames in out directory
	const existingImages: string[] = fs.readdirSync(outFolder).filter(filepath => fs.statSync(path.join(outFolder, filepath)).isFile());
	if (!existingImages) {console.log('error reading directory ' + outFolder); return;}

	let largestNumber: number = existingImages.reduce((x: number, fileName: string): number => {
		if (fileName.includes(PREFIX) && fileName.includes(EXT)) {
			
			let y: number = parseInt(fileName.split(PREFIX)[1].split(EXT)[0]);
			if (y > x) {
				return y;
			} else {
				return x;
			}
		}
	}, 0);
	
	await Promise.all(imagesToImport.map(async (filepath: string, index: number) => {
		try {
			var img: Jimp = await Jimp.read(fs.readFileSync(path.join(srcFolder, filepath)));
		} catch {
			return ;
		}
		if (img.getHeight() < MIN_IMG_DIM || img.getWidth() < MIN_IMG_DIM) {
			return ;
		}
		// resize
		if (img.getHeight() == img.getWidth() && img.getHeight() > MAX_IMG_DIM) {
			await img.scaleToFit(MAX_IMG_DIM, MAX_IMG_DIM);
		}
		if (img.getHeight() > MAX_IMG_DIM) {
			await img.resize(Jimp.AUTO, MAX_IMG_DIM);
		}
		if (img.getWidth() > MAX_IMG_DIM) {
			await img.resize(MAX_IMG_DIM, Jimp.AUTO);
		}
		// crop
		if (img.getHeight() > img.getWidth()) {
			if (img.getWidth() > MAX_IMG_DIM) {
				await img.cover(MAX_IMG_DIM, MAX_IMG_DIM);
			} else {
				await img.cover(img.getWidth(), img.getWidth());
			}
		} else if (img.getWidth() > img.getHeight()) {
			if (img.getHeight() > MAX_IMG_DIM) {
				await img.cover(MAX_IMG_DIM, MAX_IMG_DIM);
			} else {
				await img.cover(img.getHeight(), img.getHeight());
			}
		}
		// save image
		let i: string = (largestNumber + index + 1).toString();
		while (i.length < 3) {
			i = '0' + i;
		}
		await img.write(path.join(outFolder, `${PREFIX}${i}.${EXT}`));
		await fs.unlinkSync(path.join(srcFolder, filepath));
	}));
		
}

importNewImages();