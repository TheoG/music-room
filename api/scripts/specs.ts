import * as os from 'os';

let CPUs = os.cpus();

console.log('CPU:');
for (let i in CPUs) {
	console.log(`${i}: ${CPUs[i].model} (${CPUs[i].speed} MHz)`);
}

console.log('\nTotal Memory (mB)');
console.log(Math.floor(os.totalmem() / 1000000));