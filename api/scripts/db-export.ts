import { MongoClient, Db } from "mongodb";
import { exec } from 'child_process';
import chalk from 'chalk';
import * as path from 'path';

const config = require('../config.json');
const private_config = require('../private_config.json');
const defaultExportDir: string = 'export';
const collectionNames: string[] = ['events', 'playlists', 'users', 'friendships'];

const exportDatabase = async () => {
	let users = private_config.mongodb.users;
	const admin = users.find(user => user.roles.find(role => role.db == 'admin'));
	const exportDir: string = path.join(__dirname, '../', getUserDefinedExportFile() || defaultExportDir);
	console.log(chalk.cyan(`[export database] Preparing to export to ${exportDir}`));
	try {
	
		await Promise.all(collectionNames.map(async name => new Promise(async (resolve, reject) => {
			const exportPath: string = path.join(exportDir, name);
			const execString = `mongoexport --db ${config.database_name} --authenticationDatabase admin --username ${admin.name} --password ${admin.password} --collection ${name} --out ${exportPath}.json`;
			console.log(chalk.cyan(`[export database] executing '${execString}' ...`));			
			exec(execString, (error, out, err) => {
				if (error) {
					reject(error);
					return ;
				}
				console.log(chalk.cyan(`[export database] ${name} successfully exported`));
				resolve();
			});
		})));
	
	} catch (e) {
		console.log(chalk.red(`[mongo error] ${e.message}`));
		process.exit(0);
		return ;
	}

	function getUserDefinedExportFile(): string {
		let i: number = process.argv.indexOf('-o');
		if (i > 0 && process.argv[i + 1]) {
			return process.argv[i + 1];
		}
		console.log(chalk.cyan(`[export database] using default export dir ${defaultExportDir}. You can pass preferred export dir with '-- -o [dir]' option`));
		return null;
	}
};

exportDatabase();