import { MongoClient, Db } from "mongodb";
import { exec } from 'child_process';
import chalk from 'chalk';
import * as path from 'path';

const config = require('../config.json');
const private_config = require('../private_config.json');
const defaultImportDir: string = 'export';
const collectionNames: string[] = ['events', 'playlists', 'users', 'friendships'];

const importDatabase = async () => {
	let users = private_config.mongodb.users;
	const admin = users.find(user => user.roles.find(role => role.db == 'admin'));
	const importDir: string = path.join(__dirname, '../', getUserDefinedImportFile() || defaultImportDir);
	console.log(chalk.green(`[import database] Preparing to import to ${importDir}`));
	try {
	
		await Promise.all(collectionNames.map(async name => new Promise(async (resolve, reject) => {
			const importPath: string = path.join(importDir, name);
			const execString = `mongoimport --db ${config.database_name} --authenticationDatabase admin --username ${admin.name} --password ${admin.password} --collection ${name} --drop --file ${importPath}.json`;
			console.log(chalk.green(`[import database] executing '${execString}' ...`));			
			exec(execString, (error, out, err) => {
				if (error) {
					reject(error);
					return ;
				}
				console.log(chalk.green(`[import database] ${name} successfully imported`));
				resolve();
			});
		})));
		
	} catch (e) {
		console.log(chalk.red(`[mongo error] ${e.message}`));
		process.exit(0);
		return ;
	}

	function getUserDefinedImportFile(): string {
		let i: number = process.argv.indexOf('-o');
		if (i > 0 && process.argv[i + 1]) {
			return process.argv[i + 1];
		}
		console.log(chalk.green(`[import database] using default import dir ${defaultImportDir}. You can pass preferred import dir with '-- -o [dir]' option`));
		return null;
	}
};

importDatabase();
