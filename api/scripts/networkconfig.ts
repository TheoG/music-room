import * as fs from 'fs'
import * as os from 'os'
import * as inquirer from 'inquirer'

let ifaces = os.networkInterfaces();
let ipAddress = [];
Object.keys(ifaces).forEach(function (ifname) {
	let alias = 0;

	ifaces[ifname].forEach(function (iface) {
		if ('IPv4' !== iface.family || iface.internal !== false) {
			return;
		}
		if (alias == 0) {
			ipAddress.push({name: iface.address});
		}
		++alias;
	});
});

inquirer.prompt([
	{
		type: 'list',
		message: 'Select address',
		name: 'address',
		choices: ipAddress,
		validate: function(answer) {
		if (answer.length < 1) {
			return 'You must choose at least one topping.';
		}

		return true;
		}
	}
	])
	.then(answers => {
		fs.readFile('./config.json', "utf-8", (err, data) => {	
			if (err) {
				throw err;
			}
			let config = JSON.parse(data);
			config.serverName = "http://" + answers.address;
			let json = JSON.stringify(config, null, 4);
			fs.writeFile('./config.json', json, 'utf8', () => {
				console.log(`Set ${config.serverName} as server address`);
			});
		});
	});
