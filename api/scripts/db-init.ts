import { MongoClient, Db, Admin, AddUserOptions } from "mongodb";
import chalk from 'chalk';
import * as path from 'path';


const initDatabase = async () => {
	console.log(chalk.magenta('[init database]'));
	try {
		var private_config = require(path.join(__dirname, '../private_config.json'));
		var users = private_config.mongodb.users;
		var config = require(path.join(__dirname, '../config.json'));
	} catch (e) {
		console.log(chalk.red(`[init database error] ${e.message}`));
		console.log(chalk.red(`[init database error] Did you set up the private_config.json file?`));		
		process.exit(0);
	}
	try {

		// get client with no auth control
		console.log(chalk.magenta('[init database] login with no auth'));

		let client: MongoClient = await MongoClient.connect(config.mongodb_address, { useNewUrlParser: true });
		// create admin with roles
		
		let db: Db = await client.db('admin');
		let adminDb: Admin = db.admin();
		const admin = users.find(user => user.roles.find(role => role.db == 'admin'));
		await adminDb.addUser(admin.name, admin.password, { fsync: true, roles: admin.roles });
		console.log(chalk.magenta(`[init database] added user ${admin}`));
		
		// close
		await client.close();
		console.log(chalk.magenta('[init database] close client'));
		
		// login with admin
		console.log(chalk.magenta('[init database] login with admin'));
		client = await MongoClient.connect(config.mongodb_address, { useNewUrlParser: true, authSource: "admin", auth: {user: admin.name, password: admin.password}});
		// find user with music_room privileges 
		const APIAdmin = users.find(user => user.roles.find(role => role.db == config.database_name));
		db = await client.db(config.database_name);
		adminDb = db.admin();
		// create user in admin with roles in music_room
		await adminDb.addUser(APIAdmin.name, APIAdmin.password, { fsync: true, roles: APIAdmin.roles });
		console.log(chalk.magenta('[init database] added user', admin));
	
		await client.close();
		console.log(chalk.magenta('[init database] close client'));
	} catch (e) {
		console.log(chalk.red(`[init database error] ${e.message}`))
		process.exit(0);		
	}
	console.log(chalk.magenta('[init database] success'));
};

initDatabase();