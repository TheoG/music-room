import * as express from "express";
import { Request, Response } from "express";
import * as bodyParser from "body-parser";
import { MongoClient, Db } from "mongodb";
import * as bcrypt from "bcrypt";
import * as http from "http";
import * as socketio from "socket.io";
import * as methodOverride from "method-override";
const RateLimit = require('express-rate-limit');
import * as winston from 'winston';	
import * as path from 'path';
import chalk from "chalk";

import { Routes } from "./routes/routes";
import { EventController, eventController } from "./controllers/eventController";
import { SocketController, socketController } from "./controllers/socketController";
import ResponseStatusTypes from "./utils/ResponseStatusTypes";
import { env } from "./utils/env";

let config, private_config;

initNativeOverload();

class App {
	
	public app: express.Application;
	public io: socketio.Server;
	
	public routes: Routes = new Routes();
	private static salt: string = "$2b$05$PD21LwJzPhCGI8XjSPcHzO";
	
	private logger: winston.Logger;
	
	private auth: {user: string, password: string} = null;

	private ready: Promise<any> = Promise.resolve();
	
	constructor() {
		// get configs and APIAdmin credentials
		try {
			config = require('../config.json');
			private_config = require('../private_config.json');
			const admin = private_config.mongodb.users.find(user => user.roles.find(role => role.db == 'admin'));
			this.auth = {user: admin.name, password: admin.password};
		} catch (e) {
			console.log(chalk.red(`[init app] ${e.message}`));
			process.exit(0);
			return ;
		}
		this.app = express();
		this.initSocketio();
		this.initLogger();
		this.config();
	}
	
	public async asyncInit() {
		// check mongodb roles correctly set up
		const checkDBAuth = async () => {
			var client: MongoClient = await MongoClient.connect(config.mongodb_address, { useNewUrlParser: true, authSource: 'admin', auth: this.auth});
			var db: Db = client.db(config.database_name);
			await db.collection("events").findOne({});
		}

		this.ready = checkDBAuth().then(() => eventController.rebuildJobs());
		this.ready.catch(e => {
			console.error(chalk.red(`[Mongodb authentication error] ${e.message}`));
			process.exit(1);
		});
	}
	
	public async getClient(overrideReady?: boolean): Promise<MongoClient> {
		if (!overrideReady) await this.ready;
		return await MongoClient.connect(config.mongodb_address, { useNewUrlParser: true, authSource: 'admin', auth: this.auth});
	}
	
	public async getDB(client: MongoClient): Promise<Db> {
		return await client.db(config.database_name);
	} 
	
	public async closeClient(client: MongoClient): Promise<any> {
		return client.close();
	}
	
	private config(): void {
		this.app.use(express.static(path.join(__dirname, '../doc')));
		this.app.use(express.static(path.join(__dirname, '../public')));
		this.app.use(bodyParser.json());
		this.app.use(bodyParser.urlencoded({ extended: false }));
		this.app.use(methodOverride('X-HTTP-Method'));			// Microsoft
		this.app.use(methodOverride('X-HTTP-Method-Override'));	// Google/GData
		this.app.use(methodOverride('X-Method-Override'));		// IBM
		this.app.use(express.json({limit: '2mb'}));			// limit file size
		this.app.use(this.log);
		this.app.use(this.hashPassword);
		if (env.displayApiRequests()) {
			this.app.use(this.displayRequest);
		}
		this.routes.routes(this.app);
		// 404
		this.app.use(function(req, res, next) {
			return res.status(404).send({ message: 'Route'+req.url+' Not found.' });
		});
		// 429
		if (env.limitLoad()) {
			const globalLimiter = RateLimit({
				windowMs: 60 * 1000,
				max: 100,
				message: 'API calls limited to 100 per minute per IP'
			});
			this.app.use(globalLimiter);
		}
		// 500 - Any server error
		this.app.use(function(err, req, res, next) {
			return res.status(500).send({ error: err });
		});
	}

	private initSocketio(): void {
		let server: http.Server = new http.Server(this.app);
		server.listen(config.serverSocketPort);
		this.io = socketio(server);
		socketController.init(this.io);
	}

	private initLogger(): void {
		this.logger = winston.createLogger({
			level: 'info',
			format: winston.format.json(),
			defaultMeta: { service: 'user-service' },
			transports: [
			  new winston.transports.File({ filename: path.join(__dirname, '../logs/error.log'), level: 'error' }),
			  new winston.transports.File({ filename: path.join(__dirname, '../logs/combined.log') })
			]
		});
	}

	private async log(req: Request, res: Response, next:Function): Promise<any> {
		app.logger.info({
			"method": req.method,
			"endpoint": req.path,
			"MusicRoomInfo-DeviceOS": req.get("MusicRoomInfo-DeviceOS"),
			"MusicRoomInfo-DeviceModel": req.get("MusicRoomInfo-DeviceModel"),
			"MusicRoomInfo-DeviceVersion": req.get("MusicRoomInfo-DeviceVersion"),
			"MusicRoomInfo-AppVersion": req.get("MusicRoomInfo-AppVersion"),
			"timestamp": new Date().toISOString()
		});
		next();
	}

	private async hashPassword(req: Request, res: Response, next:Function): Promise<any> {
		if (!req.hasOwnProperty('body') || !req.body.hasOwnProperty('password')) {
			next();
			return ;
		}
		if (typeof req.body.password !== 'string') {
			res.status(ResponseStatusTypes.BAD_REQUEST).send({message: 'Password must be a string'});
			return ;
		}
		if (req.body.password.length < 6) {
			res.status(ResponseStatusTypes.BAD_REQUEST).send({message: 'Password too short'});
			return ;
		}
		if (req.body.password.length > 72) {
			res.status(ResponseStatusTypes.BAD_REQUEST).send({message: 'Password too long'});
			return ;
		}
		req.body.password = bcrypt.hashSync(req.body.password, App.salt);
		next();
	}

	private async displayRequest(req: Request, res: Response, next): Promise<any> {
		console.log(req.method.toUpperCase() + ': ' + req.url);
		
		if (Object.keys(req.query).length > 0) {
			console.log("\tqueries:");
			Object.keys(req.query).forEach(key => {
				console.log(`\t\t${key}: ${req.query[key]}`);
			});
		}
		if (Object.keys(req.body).length > 0) {
			console.log("\tbody:");
			Object.keys(req.body).forEach(key => {
				console.log(`\t\t${key}: `, req.body[key]);
			});
		}
		if (Object.keys(req.params).length > 0) {
			console.log("\tparams:");
			Object.keys(req.params).forEach(key => {
				console.log(`\t\t${key}: ${req.params[key]}`);
			});
		}
		next();
	}
}

const app = new App();
export default app;

app.asyncInit();

declare global {
	interface String {
		capitalize(): string;
	}
}

function initNativeOverload() {
	if (!String.prototype.hasOwnProperty('capitalize')) {
		String.prototype.capitalize = function() {
			return this.charAt(0).toUpperCase() + this.substring(1);
		}
	}
}
