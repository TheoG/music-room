import * as AsyncLock from "async-lock";
import chalk from 'chalk';

const lock = new AsyncLock({domainReentrant : false});
const lockKeys = {
	user: (val?: string) => `user${val ? '-' + val : ''}`,
	friendship: (val?: string) => `friendship${val ? '-' + val : ''}`,
	playlist: (val?: string) => `playlist${val ? '-' + val : ''}`,
	event: (val?: string) => `event${val ? '-' + val : ''}`,
	connections: (val?: string) => `connections${val ? '-' + val : ''}`,
};

const acquireLock = async (keys: string|string[], callback: any) => {
	if (keys.length == 0) throw (new Error('acquire lock error keys: ' + keys))
	if (typeof keys == 'string') {
		await lock.acquire(keys, callback);
	} else {
		let uniqueKeys = [];
		keys.forEach(key => !uniqueKeys.includes(key) ? uniqueKeys.push(key) : null);
		await lock.acquire(uniqueKeys, callback);
	}
};

export {
	acquireLock,
	lockKeys
};