enum ErrorCodes {
	BAD_REQUEST,
	NO_USER,
	NO_FRIENDSHIP,
	NO_EVENT,
	NO_PLAYLIST,
	NO_TRACK,
	INVALID_API_KEY,
	USER_ALREADY_EXISTS,
	INVALID_PASSWORD,
	CANNOT_ADD_SELF_AS_FRIEND,
	ALREADY_FRIENDS,
	NOT_FRIEND,
	INVALID_SETTING,
	INVALID_EMAIL,
	NO_VALID_ATTRIBUTES,
	FRIENDSHIP_EXISTS,
	NO_FRIENDSHIP_ACTION,
	INVALID_FRIENDSHIP_ACTION,
	INVALID_EVENT_ACTION,
	INVALID_PLAYER_ACTION,
	FRIENDSHIP_NOT_PENDING,
	USER_CANNOT_TAKE_ACTION_ON_FRIENDSHIP,
	USER_ACCOUNT_ACTIVATED,
	USER_ACCOUNT_NOT_ACTIVATED,
	USER_NOT_PREMIUM,
	INVALID_ACTIVATION_CODE,
	ACTIVATION_CODE_EXPIRED,
	INVALID_VISIBILITY,
	INVALID_EDITING,
	INVALID_NAME,
	INVALID_SORTING_TYPE,
	MISSING_PARAMETER,
	NOT_OWNER,
	ACCESS_NOT_AUTHORIZED,
	EDIT_NOT_AUTHORIZED,
	TRACK_ALREADY_IN_PLAYLIST,
	TRACK_NOT_IN_PLAYLIST,
	CONTRIBUTOR_ALREADY_IN_PLAYLIST,
	CONTRIBUTOR_NOT_IN_PLAYLIST,
	CANNOT_DELETE_OTHER_USER,
	IMAGE_ERROR,
	CANNOT_ADD_PLAYLIST_TO_ACTIVE_EVENT,
	CANNOT_REMOVE_PLAYLIST_TO_ACTIVE_EVENT,
	CANNOT_ADD_OWNER_AS_GUEST_IN_PLAYLIST,
	CANNOT_ADD_OWNER_AS_EDITOR_IN_PLAYLIST,
	CANNOT_ADD_OWNER_AS_GUEST_IN_EVENT,
	CANNOT_ADD_OWNER_AS_DJ_IN_EVENT,
	EVENT_NOT_ACTIVE,
	NO_ACTIVE_TRACK,
	NO_ACTIVE_PLAYLIST,
	VOTING_NOT_ACTIVE,
	FACEBOOK_FAIL,
}

const ErrorMessages = [];
ErrorMessages[ErrorCodes.BAD_REQUEST] = "Bad request";
ErrorMessages[ErrorCodes.NO_USER] = "User does not exist";
ErrorMessages[ErrorCodes.NO_FRIENDSHIP] = "Friendship does not exist";
ErrorMessages[ErrorCodes.NO_EVENT] = "Event does not exist";
ErrorMessages[ErrorCodes.NO_PLAYLIST] = "Playlist does not exist";
ErrorMessages[ErrorCodes.NO_TRACK] = "Track does not exist";
ErrorMessages[ErrorCodes.INVALID_API_KEY] = "Invalid api key";
ErrorMessages[ErrorCodes.USER_ALREADY_EXISTS] = "User already exists";
ErrorMessages[ErrorCodes.INVALID_PASSWORD] = "Invalid password";
ErrorMessages[ErrorCodes.CANNOT_ADD_SELF_AS_FRIEND] = "Cannot add self as friend";
ErrorMessages[ErrorCodes.ALREADY_FRIENDS] = "User is already friends with friend";
ErrorMessages[ErrorCodes.NOT_FRIEND] = "User is not friends with friend";
ErrorMessages[ErrorCodes.INVALID_SETTING] = "Setting is invalid";
ErrorMessages[ErrorCodes.INVALID_EMAIL] = "Bad email format";
ErrorMessages[ErrorCodes.NO_VALID_ATTRIBUTES] = "No valid attributes given";
ErrorMessages[ErrorCodes.FRIENDSHIP_EXISTS] = "A friendship between the users already exists";
ErrorMessages[ErrorCodes.NO_FRIENDSHIP_ACTION] = "Must provide action as param: accept, reject, read";
ErrorMessages[ErrorCodes.INVALID_FRIENDSHIP_ACTION] = "Invalid friendship action. Actions: accept, reject and read";
ErrorMessages[ErrorCodes.INVALID_EVENT_ACTION] = "Invalid event action. Actions: startvoting, endvoting, startevent and endevent";
ErrorMessages[ErrorCodes.INVALID_PLAYER_ACTION] = "Invalid player action. Actions: start, pause and seek";
ErrorMessages[ErrorCodes.FRIENDSHIP_NOT_PENDING] = "Friendship no longer pending. Cannot take action.";
ErrorMessages[ErrorCodes.USER_CANNOT_TAKE_ACTION_ON_FRIENDSHIP] = "This user cannot take actions on this pending friendship request.";
ErrorMessages[ErrorCodes.USER_ACCOUNT_ACTIVATED] = "User account already activated.";
ErrorMessages[ErrorCodes.USER_ACCOUNT_NOT_ACTIVATED] = "User account not activated.";
ErrorMessages[ErrorCodes.USER_NOT_PREMIUM] = "Feature only available to premium users.";
ErrorMessages[ErrorCodes.INVALID_ACTIVATION_CODE] = "Activation code invalid.";
ErrorMessages[ErrorCodes.ACTIVATION_CODE_EXPIRED] = "Activation code expired. Please request new activation code.";
ErrorMessages[ErrorCodes.INVALID_VISIBILITY] = "Visibility invalid (PRIVATE, PUBLIC)";
ErrorMessages[ErrorCodes.INVALID_EDITING] = "Editing invalid (PRIVATE, PUBLIC)";
ErrorMessages[ErrorCodes.INVALID_NAME] = "Name invalid";
ErrorMessages[ErrorCodes.INVALID_SORTING_TYPE] = "Sorting type invalid (vote, move)";
ErrorMessages[ErrorCodes.MISSING_PARAMETER] = "Missing parameter :param as :origin";
ErrorMessages[ErrorCodes.NOT_OWNER] = "User is not owner of this resource";
ErrorMessages[ErrorCodes.ACCESS_NOT_AUTHORIZED] = "User is not authorized to access this resource";
ErrorMessages[ErrorCodes.EDIT_NOT_AUTHORIZED] = "User is not authorized to edit this resource";
ErrorMessages[ErrorCodes.TRACK_ALREADY_IN_PLAYLIST] = "Track already in this playlist";
ErrorMessages[ErrorCodes.TRACK_NOT_IN_PLAYLIST] = "Track not in this playlist";
ErrorMessages[ErrorCodes.CONTRIBUTOR_ALREADY_IN_PLAYLIST] = "Contributor already in this playlist";
ErrorMessages[ErrorCodes.CONTRIBUTOR_NOT_IN_PLAYLIST] = "Contirbutor no in this playlist";
ErrorMessages[ErrorCodes.CANNOT_DELETE_OTHER_USER] = "Cannot delete other user";
ErrorMessages[ErrorCodes.IMAGE_ERROR] = "Image error";
ErrorMessages[ErrorCodes.CANNOT_ADD_PLAYLIST_TO_ACTIVE_EVENT] = "Cannot add playlist to active event";
ErrorMessages[ErrorCodes.CANNOT_REMOVE_PLAYLIST_TO_ACTIVE_EVENT] = "Cannot remove playlist to active event";
ErrorMessages[ErrorCodes.CANNOT_ADD_OWNER_AS_GUEST_IN_PLAYLIST] = "Cannot add owner as guest in playlist";
ErrorMessages[ErrorCodes.CANNOT_ADD_OWNER_AS_EDITOR_IN_PLAYLIST] = "Cannot add owner as editor in playlist";
ErrorMessages[ErrorCodes.CANNOT_ADD_OWNER_AS_GUEST_IN_EVENT] = "Cannot add owner as guest in event";
ErrorMessages[ErrorCodes.CANNOT_ADD_OWNER_AS_DJ_IN_EVENT] = "Cannot add owner as dj in event";
ErrorMessages[ErrorCodes.EVENT_NOT_ACTIVE] = "Event not active";
ErrorMessages[ErrorCodes.NO_ACTIVE_TRACK] = "No active track";
ErrorMessages[ErrorCodes.NO_ACTIVE_PLAYLIST] = "No active playlist";
ErrorMessages[ErrorCodes.VOTING_NOT_ACTIVE] = "Voting not active";
ErrorMessages[ErrorCodes.FACEBOOK_FAIL] = "Facebook error";

class ServerError {
	public errorCode: Number;
	public errorMessage: string;

	constructor(code: ErrorCodes, msg?: string, paramName?: string, paramOrigin?: string) {
		this.errorCode = code;
		this.errorMessage = msg || ErrorMessages[code];
		// special message format for code based on additional input
		if (code === ErrorCodes.MISSING_PARAMETER && paramName && paramOrigin) {
			this.errorMessage = this.errorMessage.replace(':param', paramName).replace(':origin', paramOrigin == 'body' ? 'body attribute' : paramOrigin);
		}
	}
}

export {
	ErrorCodes,
	ErrorMessages,
	ServerError
};