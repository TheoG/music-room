import * as v4 from "uuid/v4";

export const prefixes = {
	user: 'USER',
	friendship: 'FRIE',
	playlist: 'PLAY',
	eventplaylist: 'EPLA',
	track: 'TRAC',
	eventtrack: 'ETRA',
	event: 'EVEN',
	apiKey: '_KEY'
};

export const uuid = (type: 'user'|'friendship'|'playlist'|'track'|'event'|'apiKey'|'eventplaylist'|'eventtrack') => {
	return prefixes[type] + '-' + v4();
}