enum EnvVar {
	NODE_ENV = "NODE_ENV",
	MUSIC_ROOM_SEND_EMAILS = "MUSIC_ROOM_SEND_EMAILS",
	MUSIC_ROOM_USER_AUTO_ACTIVATE = "MUSIC_ROOM_USER_AUTO_ACTIVATE",
	MUSIC_ROOM_USER_AUTO_PREMIUM = "MUSIC_ROOM_USER_AUTO_PREMIUM",
	MUSIC_ROOM_DISPLAY_REQUESTS = "MUSIC_ROOM_DISPLAY_REQUESTS",
	MUSIC_ROOM_LIMIT_LOAD = "MUSIC_ROOM_LIMIT_LOAD",
};

const env = {
	test: () => process.env[EnvVar.NODE_ENV] == 'test',
	sendEmails: () => (!env.test() && process.env[EnvVar.MUSIC_ROOM_SEND_EMAILS] != 'false') || (env.test() && process.env[EnvVar.MUSIC_ROOM_SEND_EMAILS] == 'true'),
	autoActivate: () => (env.test() && process.env[EnvVar.MUSIC_ROOM_USER_AUTO_ACTIVATE] != 'false') || (process.env[EnvVar.MUSIC_ROOM_USER_AUTO_ACTIVATE] == 'true'),
	autoPremium: () => (env.test() && process.env[EnvVar.MUSIC_ROOM_USER_AUTO_PREMIUM] != 'false') || (process.env[EnvVar.MUSIC_ROOM_USER_AUTO_PREMIUM] == 'true'),
	displayApiRequests: () => process.env[EnvVar.MUSIC_ROOM_DISPLAY_REQUESTS] == 'true',
	limitLoad: () => (!env.test() && process.env[EnvVar.MUSIC_ROOM_LIMIT_LOAD] != 'false') || (env.test() && process.env[EnvVar.MUSIC_ROOM_LIMIT_LOAD] == 'true'),
};

export {
	env,
	EnvVar
};