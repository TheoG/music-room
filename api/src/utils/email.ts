import * as nodemailer from "nodemailer";
import { Transporter } from "nodemailer";
import { MailOptions } from "nodemailer/lib/stream-transport";
import * as path from 'path';
import * as fs from 'fs';
import * as util from 'util';
const readFile = util.promisify(fs.readFile);
import chalk from "chalk";

import { User } from "../classes/User"; 
import { env } from "./env";

const private_config = require('../../private_config.json');

const transporter: Transporter = nodemailer.createTransport({
	service: 'gmail',
	auth: {
		user: private_config.emailService.email,
		pass: private_config.emailService.password
	}
});

type EmailType = 'activation' | 'passwordReset' | 'login';
type EmailSubjects = {[key in EmailType]: string};
let emailSubjects: EmailSubjects = {
	'activation': 'Music Room: Activate Your Account',
	'passwordReset': 'Music Room: Reset Your Password',
	'login': 'Music Room: Login Notification'
};
type SendEmailOptions = {
	deviceOS?: string,
	deviceModel?: string,
	deviceVersion?: string,
	appVersion?: string,
	ipAddress?: string
};

export const sendEmail = async (user: User, type: EmailType, options?: SendEmailOptions) => {
	if (!env.sendEmails()) return ;

	try {
		const mailOptions: MailOptions = {
			from: private_config.emailService.email,
			to: user.email,
			// to: 'musicroom42noreply@gmail.com',
			subject: emailSubjects[type],
			html: await createEmailFromTemplate(user, type, options)
		};
		await transporter.sendMail(mailOptions)
		.then(() => {
			console.log(`Sent ${type} email to user ${user.getDisplayName()}`);
		})
		.catch(err => {
			console.log(chalk.red('MAIL ERROR'));
			throw(err);
		});
	} catch (err) {
		console.log(chalk.red(err));
	}
};

async function createEmailFromTemplate(user: User, emailType: EmailType, options?: SendEmailOptions): Promise<string> {
	let email: string = await getTemplate(emailType);
	
	// replace user with name
	email = email.replace('{{name}}', user.getDisplayName());
	
	switch (emailType) {
		case 'activation':
			// replace urlactivateaccount with url
			email = email.replace('{{urlactivateaccount}}', user.getActivationAccountLink());
			break;
		case 'passwordReset':		
			// replace passwordupdatecode with code
			email = email.replace('{{passwordupdatecode}}', user.passwordResetCode);
			break;
		case 'login':
			if (options) {
				if (options.ipAddress) {
					options.ipAddress = options.ipAddress.replace('::ffff:', '');
					email = email.replace('{{ipAddress}}', options.ipAddress);
				} else {
					email = email.replace('{{ipAddress}}', 'unknown ip address');
				}
				email = email.replace('{{deviceName}}', options.deviceModel ? options.deviceModel : 'unknown device');
				email = email.replace('{{deviceOS}}', options.deviceOS ? options.deviceOS : 'unknown OS');
			}
			break;
	}

	return email;
}

type Templates = {[key in EmailType]?: string};
let templates: Templates = {};

const relDirPath: string = 'emailTemplates';

async function getTemplate(emailType: EmailType): Promise<string> {
	if (templates.hasOwnProperty(emailType)) {
		return templates[emailType];
	}

	templates[emailType] = await readFile(path.join(__dirname, relDirPath, emailType + 'EmailTemplate.html'), "utf8");
	return templates[emailType];
}