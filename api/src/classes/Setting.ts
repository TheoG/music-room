/*
	Defines available configurations per setting
*/
export class MasterSetting {
	constructor(
		public name: string,
		public defaultValue: string,
		public values: string[]
	) { }

	toSetting(): Setting {
		return {name: this.name, value: this.defaultValue};
	}
}

/*
	Interface for object saved in db
*/
export interface Setting {
	name: string,
	value: string
}

export enum SettingValues {
	PUBLIC = 'PUBLIC',
	FRIENDS = 'FRIENDS',
	PRIVATE = 'PRIVATE'
}