import { MongoClient, Db } from "mongodb";
import chalk from "chalk";

import app from "../app";
import Track from "./Track";
import { Visibility, SortingType, Editing } from "../types/types"; 
import { User } from "./User";
import { acquireLock, lockKeys } from "../utils/mutex";
import { uuid } from "../utils/uuid";

const search_limit: number = require('../../config.json').mongo_search_limit;

export class Playlist {
	public id: string;
	public name: string;
	public owner: string; // id

	public events: string[];
	public tracks: Track[] = []; // list of tracks

	public visibility: Visibility; // public private. only defines visibility, not rights
	public guests: string[] = []; // those can see if visibility is private
	public editing: Editing; // public private. only defines editing rights, not visibility
	public editors: string[] = []; // list of user ids who can add/rm tracks

	public sortingType: SortingType; // move tracks, vote for tracks

	constructor(config: {id?: string, name: string, owner: string, events?: string[], tracks?: Track[], visibility?: Visibility, guests?: string[], editing?: Editing, editors?: string[], sortingType?: SortingType}) {
		if (config.hasOwnProperty('id')) {
			this.id = config.id;
		} else {
			this.id = uuid('playlist');
		}
		this.name = config.name;
		this.owner = config.owner;
		this.events = config.events || [];
		this.tracks = config.tracks || [];
		this.visibility = config.visibility || 'PUBLIC';
		this.guests = config.guests || [];
		if (this.visibility == 'PRIVATE') {
			this.editing = 'PRIVATE';
		} else {
			this.editing = config.editing || 'PUBLIC';
		}
		this.editors = config.editors || [];
		this.sortingType = config.sortingType || 'vote';
	}

	static async get(filter: object): Promise<Playlist> {
		const client: MongoClient = await app.getClient();
		const db: Db = await app.getDB(client);

		const dbPlaylist = await db.collection("playlists").findOne(filter);

		await app.closeClient(client);
		if (!dbPlaylist) return undefined;
		const playlist: Playlist = new Playlist({...dbPlaylist});
		return playlist;
	}

	public async create() {
		const client: MongoClient = await app.getClient();
		const db: Db = await app.getDB(client);
		await db.collection("playlists").insertOne(this.toPlainObj());
		await app.closeClient(client);
	}

	public async save() {
		const client: MongoClient = await app.getClient();
		const db: Db = await app.getDB(client);
		await db.collection("playlists").updateOne({id: this.id}, { $set: this.toPlainObj() });
		await app.closeClient(client);
	}

	public addTrack(serviceid: string, service: string): boolean {
		// if no track with serviceid and no service exists AND
		// if no track with serviceid and service exists
		if (!this.tracks.find((x: Track) => x.serviceid == serviceid && !x.service) &&
		!this.tracks.find((x: Track) => x.serviceid == serviceid && x.service == service)) {
			this.tracks.push(new Track({serviceid, service}));
			return true;
		}
		return false;
	}

	public removeTrack(id: string): boolean  {
		const index = this.tracks.findIndex((x: Track) => x.id == id);
		if (index > -1) {
			this.tracks.splice(index, 1);
			return true;
		}
		return false;
	}

	public moveTrack(trackid: string, index: string): boolean  {
		let i: number = Number(index);
		let track: Track = this.tracks.find(x => x.id == trackid);
		if (!track) return false;
		let currentIndex: number = this.tracks.findIndex(x => x.id == trackid);
		this.tracks.splice(currentIndex, 1);
		if (i > this.tracks.length) {
			i = this.tracks.length;
		}		
		this.tracks.splice(i, 0, track);
		return true;
	}

	public addGuest(id: string): boolean  {
		if (!this.guests.find((x: string) => x == id)) {
			this.guests.push(id);
			return true;
		} else {
			// console.error(chalk.yellow('[Playlist.addGuest]: User already in guests. playlist: ' + this.id + ', user: ' + id));
			return false;
		}
	}

	public removeGuest(id: string): boolean  {
		let index = this.guests.findIndex((x: string) => x == id);
		if (index > -1) {
			this.guests.splice(index, 1);
			// remove from editors
			this.removeEditor(id);
			return true;
		} else {
			// console.error(chalk.yellow('[Playlist.removeGuest]: User not in guests. playlist: ' + this.id + ', user: ' + id));
			return false;
		}
	}

	public addEditor(id: string): boolean  {
		if (!this.editors.find((x: string) => x == id)) {
			this.editors.push(id);
			// add to guests
			this.addGuest(id);
			return true;
		} else {
			// console.error(chalk.yellow('[Playlist.addEditor]: User already in editor. playlist: ' + this.id + ', user: ' + id));
			return false;
		}
	}

	public removeEditor(id: string): boolean  {
		const index = this.editors.findIndex((x: string) => x == id);
		if (index > -1) {
			this.editors.splice(index, 1);
			// does not remove from guests
			return true;
		} else {
			// console.error(chalk.yellow('[Playlist.removeEditor]: User not in editors. playlist: ' + this.id + ', user: ' + id));
			return false;
		}
	}

	public addEvent(eventid: string): boolean {
		if (!this.events.find((x: string) => x == eventid)) {
			this.events.push(eventid);
			return true;
		} else {
			// console.error(chalk.yellow('[Playlist.addGuest]: Event already in events. playlist: ' + this.id + ', event: ' + eventid));
			return false;
		}
	}

	public removeEvent(eventid: string): boolean  {
		const index = this.events.findIndex((x: string) => x == eventid);
		if (index > -1) {
			this.events.splice(index, 1);
			return true;
		} else {
			// console.error(chalk.yellow('[Playlist.removeEvent]: Event not in events. playlist: ' + this.id + ', event: ' + eventid));
			return false;
		}
	}

	public async duplicate(userid: string, options?: {copyEditors?: boolean, copyGuests?: boolean}): Promise<Playlist> {
		const client: MongoClient = await app.getClient();
		const db: Db = await app.getDB(client);
		// only reading, no need to lock 
		const dbPlaylist = await db.collection("playlists").findOne({id: this.id});

		await app.closeClient(client);
		if (!dbPlaylist) return undefined;

		// update properties
		delete dbPlaylist.id;
		dbPlaylist.owner = userid;

		if (!options.copyEditors) dbPlaylist.editors = [];
		if (!options.copyGuests ) dbPlaylist.guests  = [];

		const playlist: Playlist = new Playlist({...dbPlaylist});
		
		// update users
		if (options.copyEditors || options.copyGuests) {
			let usersToUpdate = [];
			if (options.copyEditors) usersToUpdate.push(...playlist.editors);
			if (options.copyGuests ) usersToUpdate.push(...playlist.guests );
			await Promise.all(usersToUpdate.map(async id => {
				let user: User = await User.get({id});
				if (!user) return ;
				await acquireLock(lockKeys.user(id), async () => {
					user = await User.get({id});
					if (!user) return ;
					user.addGuestInPlaylist(playlist.id);
					playlist.addGuest(id);
					await user.save();
				});
			}));
		}
		return playlist;
	}

	public toPlainObj(): Object {
		return Object.assign({}, this);
	}

	public userCanAccess(user: User): boolean {
		return this.visibility == 'PUBLIC' ||
			this.userIsOwner(user) ||
			this.userIsEditor(user) ||
			this.userIsGuest(user);
	}

	public userCanEdit(user: User): boolean {
		return (this.visibility == 'PUBLIC' && this.editing == 'PUBLIC') ||
			this.userIsOwner(user) ||
			this.userIsEditor(user);
	}

	public userIsOwner(user: User): boolean {
		return this.owner == user.id;
	}

	public userIsGuest(user: User): boolean {
		return this.guests.includes(user.id);
	}

	public userIsEditor(user: User): boolean {
		return this.editors.includes(user.id);
	}

	public update(params: {name?: string, visibility?: Visibility, editing?: Editing, sortingType?: SortingType}): boolean {
		if (params.visibility == 'PRIVATE') {
			params.editing = 'PRIVATE';
		} else if (params.editing == 'PUBLIC') {
			params.visibility = 'PUBLIC';
		}
		let updated: boolean = false;
		Object.keys(params).forEach(attribute => {
			if (params[attribute]) {
				if (this[attribute] != params[attribute]) {
					updated = true;
				}
				this[attribute] = params[attribute];
			}
		});
		return updated;
	}

	public async delete(): Promise<any> {
		const client: MongoClient = await app.getClient();
		const db: Db = await app.getDB(client);
		await db.collection("playlists").deleteOne({id: this.id});
		await app.closeClient(client);


		let _this = this;

		await Promise.all([
			// delete playlist in user.playlists
			(async () => {
				let user: User = await User.get({id: this.owner});
				if (!user) {
					// console.error(chalk.yellow('[Playlist.delete]: user does not exist. userid: ' + this.owner));
					return ;
				}
				await acquireLock(lockKeys.user(this.owner), async () => {
					user = await User.get({id: this.owner});
					if (!user) return;
					user.removePlaylist(this.id);
					await user.save();
				});
			})(),
			(async () => {
				// delete editors in user.editorInPlaylists
				await Promise.all(this.editors.map(async userId => {
					let user: User = await User.get({id: userId});
					if (!user) {
						// console.error(chalk.yellow('[Playlist.delete]: user does not exist. userid: ' + userId));
						return ;
					}
					await acquireLock(lockKeys.user(userId), async () => {
						user = await User.get({id: userId});
						if (!user) return ;
						user.removeEditorInPlaylist(this.id);
						_this.removeEditor(user.id);
						await user.save();
					});
				}))
				// then delete guests in user.guestInPlaylists
				.then(() => Promise.all(this.guests.map(async userId => {
					let user: User = await User.get({id: userId});
					if (!user) {
						// console.error(chalk.red('[Playlist.delete]: user does not exist. userid: ' + userId));
						return ;
					}
					await acquireLock(lockKeys.user(userId), async () => {
						user = await User.get({id: userId});
						if (!user) return ;
						user.removeGuestInPlaylist(this.id);
						await user.save();
					});
				})));
			})(),
			
			// delete playlists from events
			...this.events.map(async eventid => {
				let event: Event = await Event.get({id: eventid});
				if (!event) {
					// console.error(chalk.red('[Playlist.delete]: event does not exist. eventid: ' + eventid));
					return ;
				}
				await acquireLock(lockKeys.event(eventid), async () => {
					event = await Event.get({id: eventid});
					if (!event) return ;
					await event.removePlaylist(this.id);
					await event.save();
				});
			}),
		]);
	}

	public async expand(client: User): Promise<any> {
		let ret: any = this;
		await Promise.all([
			new Promise((async (resolve, reject) => {
				let user: User = await User.get({id: this.owner});
				if (user) {
					ret.owner = user.filterByRelationship(client);
				}
				resolve();
			})),
			...this.editors.map(id => new Promise((async (resolve, reject) => {
				let user: User = await User.get({id});
				if (user) {
					let index: number = ret.editors.findIndex(x => x == id);
					if (index >= 0) {
						ret.editors[index] = user.filterByRelationship(client);
					}
				}
				resolve();
			}))),
			...this.guests.map(id => new Promise((async (resolve, reject) => {
				let user: User = await User.get({id});
				if (user) {
					let index: number = ret.guests.findIndex(x => x == id);
					if (index >= 0) {
						ret.guests[index] = user.filterByRelationship(client);
					}
				}
				resolve();
			})))
		]);
		return ret;
	}

	/*
		Searches by id, ownerid, name, if playlist visibility is PUBLIC.
	*/
	static async search(keyword: string): Promise<any> {
		const client: MongoClient = await app.getClient();
		const db: Db = await app.getDB(client);
		const regex: string = '.*' + keyword + '.*';
		const results = await db.collection("playlists").find({
			$and: [
				{visibility: 'PUBLIC'},
				{$or: [
					{id: {$regex: new RegExp(regex, 'i') }},
					{name: {$regex: new RegExp(regex, 'i') }},
					{owner: {$regex: new RegExp(regex, 'i') }},
				]}
			]
		}).limit(search_limit).toArray();
		await app.closeClient(client);
		const playlists: Playlist[] = results.map(x => new Playlist({...x}));
		return playlists;
	}

}

import { Event } from './Event';
