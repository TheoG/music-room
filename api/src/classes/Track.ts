import { uuid } from "../utils/uuid"

class TrackInfo {
	
}

export default class Track {
	public id: string;
	public serviceid: string; // Deezer track id
	public service: string; // Deezer or other

	constructor(config: {id?: string, serviceid: string, service?: string}) {
		if (config.hasOwnProperty('id')) {
			this.id = config.id;
		} else {
			this.id = uuid('track');
		}
		this.serviceid = config.serviceid;
		this.service = config.service;
	}
}