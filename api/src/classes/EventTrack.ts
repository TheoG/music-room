import { uuid } from "../utils/uuid";
import Track from "./Track";

export default class EventTrack extends Track {
	public isPlaying: boolean; // playing != active. active is the selected track
	public likes: string[]; // list of user id's who upvoted the track
	public dislikes: string[]; // list of user id's who downvoted the track

	constructor(config: {id?: string, serviceid: string, service?: string, isPlaying?: boolean, likes?: string[], dislikes?: string[]}) {
		if (!config.hasOwnProperty('id')) {
			config.id = uuid('eventtrack');
		}
		super({id: config.id, serviceid: config.serviceid, service: config.service});
		this.isPlaying = config.isPlaying || false;
		this.likes = config.likes || [];
		this.dislikes = config.dislikes || [];
	}

	static createFromTrack(track: Track) {
		return new EventTrack({
			serviceid: track.serviceid,
			service: track.service
		});
	}

	public like(userid: string): boolean {
		if (this.likes.includes(userid)) {
			return false;
		}
		this.likes.push(userid);
		return true;
	}

	public unlike(userid: string): boolean {
		if (!this.likes.includes(userid)) {
			return false;
		}
		this.likes.splice(this.likes.indexOf(userid), 1);
		return true;
	}

	public dislike(userid: string): boolean {
		if (this.dislikes.includes(userid)) {
			return false;
		}
		this.dislikes.push(userid);
		return true;
	}

	public undislike(userid: string): boolean {
		if (!this.dislikes.includes(userid)) {
			return false;
		}
		this.dislikes.splice(this.dislikes.indexOf(userid), 1);
		return true;
	}

	public play(): boolean {
		if (this.isPlaying) return false;
		this.isPlaying = true;
		return true;
	}

	public stop(): boolean {
		if (!this.isPlaying) return false;
		this.isPlaying = false;
		return true; 
	}

	public pause(): boolean {
		if (!this.isPlaying) return false;
		this.isPlaying = false;
		return true;
	}

	public rank(): number {
		return this.likes.length - this.dislikes.length;
	}
}