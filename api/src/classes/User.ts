import { uuid } from "../utils/uuid"
import * as bcrypt from "bcrypt";
import * as Jimp from 'jimp';
import * as path from 'path';
import * as util from 'util';
import * as fs from 'fs';
const unlink = util.promisify(fs.unlink);
import chalk from "chalk";

import app from "../app";
import { Db, MongoClient } from "mongodb";
import { Setting } from "./Setting";
import { sendEmail } from "../utils/email";
import settingsController from "../controllers/settingsController";
import { acquireLock, lockKeys } from "../utils/mutex";
import { Theme } from "../types/types";

const config = require('../../config.json');

const salt: string = "$2b$05$PD21LwJzPhCGI8XjSPcHzO";

export class User {
	public id: string;
	public apiKey: string;

	public facebookid: string;

	public activationCode: string;
	public activationCodeExpiryDate: string; // js Date in string format
	public activated: boolean;

	public passwordResetCode: string;
	public passwordResetAttemptsRemaining: number; 

	public isPremium: boolean;

	public email: string;
	public password: string;
	public displayName: string;
	public firstName: string;
	public lastName: string;
	public settings: Setting[] = [];
	public theme: Theme;
	public avatar: string;
	public musicalPreferences: string;
 
	public friendships: string[] = [];
	public friends: string[];

	public playlists: string[];
	public guestInPlaylists: string[];
	public editorInPlaylists: string[];

	public events: string[];
	public guestInEvents: string[];
	public djInEvents: string[];

	constructor(config?) {		
		if (config.hasOwnProperty('id')) {
			this.id = config.id;
			this.apiKey = config.apiKey;
			
			this.activationCode = config.activationCode;
			this.activationCodeExpiryDate = config.activationCodeExpiryDate;
			this.activated = config.activated || false;
		} else {
			this.id = uuid('user');
			this.apiKey = uuid('apiKey');
			this.resetActivationCode();
		}
		this.facebookid = config.facebookid;
		this.email = config.email;
		this.password = config.password;
		this.displayName = config.displayName;
		this.firstName = config.firstName;
		this.lastName = config.lastName;
		this.theme = config.theme;
		this.avatar = config.avatar;
		this.musicalPreferences = config.musicalPreferences;

		this.passwordResetCode = config.passwordResetCode;
		this.passwordResetAttemptsRemaining = config.passwordResetAttemptsRemaining;

		this.isPremium = config.isPremium || false;

		this.settings = config.settings || settingsController.getDefaultSettings();

		this.friends = config.friends || [];
		this.friendships = config.friendships || [];
		this.playlists = config.playlists || [];
		this.guestInPlaylists = config.guestInPlaylists || [];
		this.editorInPlaylists = config.editorInPlaylists || [];
		this.events = config.events || [];
		this.guestInEvents = config.guestInEvents || [];
		this.djInEvents = config.djInEvents || [];
	}

	/*
		Gets a user from the database based on filter
	*/
	public static async get(filter: object) : Promise<User> {
		const client: MongoClient = await app.getClient();
		const db: Db = await app.getDB(client);
		const dbUser = await db.collection("users").findOne(filter);
		await app.closeClient(client);		
		if (!dbUser) return undefined;
		const user: User = new User({...dbUser});
		return user;
	}

	public async save(): Promise<any> {
		const client: MongoClient = await app.getClient();
		const db: Db = await app.getDB(client);
		await db.collection("users").updateOne({id: this.id}, { $set: this.toPlainObj() });
		await app.closeClient(client);
	}

	/*
		Saves new used to database
	*/
	public async create(): Promise<any> {
		const client: MongoClient = await app.getClient();
		const db: Db = await app.getDB(client);
		await db.collection("users").insertOne(this.toPlainObj());
		await app.closeClient(client);
	}

	public async update(params: {email?: string, password?: string, displayName?: string, firstName?: string, lastName?: string, theme?: Theme, avatar?: string, musicalPreferences?: string}): Promise<void> {
		if (params.hasOwnProperty('avatar') && !!params.avatar) {
			await this.updateAvatar(params.avatar);
			delete params.avatar;
		}
		Object.keys(params).forEach(key => {
			if (params[key] != undefined) {
				this[key] = params[key];
			}
		});
		if ((params.hasOwnProperty('password') && !!params.password) || (params.hasOwnProperty('email') && !!params.email)) {
			this.apiKey = uuid('apiKey');
		}
	}

	public async assignRandomDefaultAvatar(): Promise<void> {
		// check if there are any images in default-avatars
		const outFolder: string = path.join(__dirname, '../../public/avatars/default-avatars');
		const availableImages: string[] = fs.readdirSync(outFolder).filter(filepath => fs.statSync(path.join(outFolder, filepath)).isFile());
		if (!availableImages) {
			console.log(chalk.red('error reading directory ' + outFolder));
			return;
		}
		if (availableImages.length == 0) return ;
		
		let i: number = Math.floor(Math.random() * (availableImages.length));
		this.avatar = `avatars/default-avatars/${availableImages[i]}`;
	}

	public async updateAvatar(avatar: string): Promise<void> {
		const EXT: string = '.png';
		const MAX_IMG_DIM: number = 500;
		const MIN_IMG_DIM: number = 200;
		// check file at path exists
		try {
			var img: Jimp = await Jimp.read(avatar);
		} catch {
			console.error(chalk.red('[User.updateAvatar]: error creating Jimp from avatar ' + avatar));
			return ;
		}
		// save to public/avatars/ (should delete previous avatar)
		const imgPath: string = path.join(__dirname, `../../public/avatars/${this.id}${EXT}`);
		// scale
		if (img.getHeight() == img.getWidth() && img.getHeight() > MAX_IMG_DIM) {
			await img.scaleToFit(MAX_IMG_DIM, MAX_IMG_DIM);
		}
		if (img.getHeight() > img.getWidth()) {
			if (img.getWidth() > MAX_IMG_DIM) {
				await img.cover(MAX_IMG_DIM, MAX_IMG_DIM);
			} else {
				await img.cover(img.getWidth(), img.getWidth());
			}
		} else if (img.getWidth() > img.getHeight()) {
			if (img.getHeight() > MAX_IMG_DIM) {
				await img.cover(MAX_IMG_DIM, MAX_IMG_DIM);
			} else {
				await img.cover(img.getHeight(), img.getHeight());
			}
		}
		await img.writeAsync(imgPath);
		
		// delete from uploads file
		await unlink(avatar);
		// add as path without 'public'
		this.avatar = `avatars/${this.id}${EXT}`;
	}

	public async deleteAvatar(): Promise<void> {
		if (this.avatar) {
			if (this.avatar.includes('default-avatars')) {
				return ;
			}
			try {
				await unlink('./public/' + this.avatar);
			} catch {
				// console.error(chalk.red('[User.deleteAvatar]: failed to delete avatar'));
			}
		}
	}

	public addFriend(id: string): boolean {
		if (this.friends.includes(id)) {
			console.error(chalk.yellow('[AddFriend]: user already in friend list. user: ' + this.id + ', friend: ' + id));
			return false;
		} else {
			this.friends.push(id);
			return true;
		}
	}

	public removeFriend(id: string): boolean {
		const index = this.friends.indexOf(id);
		if (index > -1) {
			this.friends.splice(index, 1);
			return true;
		} else {
			// console.error(chalk.yellow('[User.RemoveFriend]: user not in friend list. user: ' + this.id + ', friend: ' + id));
			return false;
		}
	}

	public addFriendship(id: string): boolean {
		if (this.friendships.includes(id)) {
			// console.error(chalk.yellow('[User.AddFriendship]: friendship already in friendship list. user: ' + this.id + ', friendship: ' + id));
			return false;
		} else {
			this.friendships.push(id);
			return true;
		}
	}

	public removeFriendship(id: string): boolean {
		const index = this.friendships.indexOf(id);
		if (index > -1) {
			this.friendships.splice(index, 1);
			return true;
		} else {
			// console.error(chalk.yellow('[RemoveFriendship]: friendship not in friendship list. user: ' + this.id + ', friendship: ' + id));
			return false;
		}
	}

	public addPlaylist(id: string): boolean {
		if (this.playlists.includes(id)) {
			// console.error(chalk.yellow('[User.AddPlaylist]: Playlist already in playlists list. user: ' + this.id + ', playlist: ' + id));
			return false;
		} else {
			this.playlists.push(id);
			return true;
		}
	}

	public removePlaylist(id: string): boolean {
		const index = this.playlists.indexOf(id);
		if (index > -1) {
			this.playlists.splice(index, 1);
			return true;
		} else {
			// console.error(chalk.yellow('[User.RemovePlaylist]: Playlist not in playlist list. user: ' + this.id + ', playlist: ' + id));
			return false;
		}
	}

	public addEvent(id: string): boolean {
		if (this.events.includes(id)) {
			// console.error(chalk.yellow('[User.AddEvent]: Event already in events list. user: ' + this.id + ', event: ' + id));
			return false;
		} else {
			this.events.push(id);
			return true;
		}
	}

	public removeEvent(id: string): boolean {
		const index = this.events.indexOf(id);
		if (index > -1) {
			this.events.splice(index, 1);
			return true;
		} else {
			// console.error(chalk.yellow('[User.RemoveEvents]: Event not in events list. user: ' + this.id + ', event: ' + id));
			return false;
		}
	}

	public addGuestInEvent(id: string): boolean {
		if (this.guestInEvents.includes(id)) {
			// console.error(chalk.yellow('[User.AddGuestInEvent]: Event already in guestInEvents list. user: ' + this.id + ', event: ' + id));
			return false;
		} else {
			this.guestInEvents.push(id);
			return true;
		}
	}

	public removeGuestInEvent(id: string): boolean {
		const index = this.guestInEvents.indexOf(id);
		if (index > -1) {
			this.guestInEvents.splice(index, 1);
			return true;
		} else {
			// console.error(chalk.yellow('[User.RemoveGuestInEvents]: Event not in guestInEvents list. user: ' + this.id + ', event: ' + id));
			return false;
		}
	}

	public addDJInEvent(id: string): boolean {
		if (this.djInEvents.includes(id)) {
			// console.error(chalk.yellow('[User.AddDJInEvent]: Event already in djInEvents list. user: ' + this.id + ', event: ' + id));
			return false;
		} else {
			this.djInEvents.push(id);
			return true;
		}
	}

	public removeDJInEvent(id: string): boolean {
		const index = this.djInEvents.indexOf(id);
		if (index > -1) {
			this.djInEvents.splice(index, 1);
			return true;
		} else {
			// console.error(chalk.yellow('[User.RemoveDJInEvents]: Event not in djInEvents list. user: ' + this.id + ', event: ' + id));
			return false;
		}
	}

	public addEditorInPlaylist(id: string): boolean {
		if (this.editorInPlaylists.includes(id)) {
			// console.error(chalk.yellow('[AddEditorInPlaylist]: Playlist already in editorInPlaylists list. user: ' + this.id + ', playlist: ' + id));
			return false;
		} else {
			this.editorInPlaylists.push(id);
			// add user as guest
			this.addGuestInPlaylist(id);
			return true;
		}
	}

	public removeEditorInPlaylist(id: string): boolean {
		const index = this.editorInPlaylists.indexOf(id);
		if (index > -1) {
			this.editorInPlaylists.splice(index, 1);
			return true;
		} else {
			// console.error(chalk.yellow('[User.removeEditorInPlaylist]: Playlist not in editorInPlaylists list. user: ' + this.id + ', playlist: ' + id));
			return false;
		}
	}

	public addGuestInPlaylist(id: string): boolean {
		if (this.guestInPlaylists.includes(id)) {
			// console.error(chalk.yellow('[AddGuestInPlaylist]: playlist already in guestInPlaylists list. user: ' + this.id + ', playlist: ' + id));
			return false;
		} else {
			this.guestInPlaylists.push(id);
			return true;
		}
	}

	public removeGuestInPlaylist(id: string): boolean {
		const index = this.guestInPlaylists.indexOf(id);
		if (index > -1) {
			this.guestInPlaylists.splice(index, 1);
			// remove as editor
			this.removeEditorInPlaylist(id);
			return true;
		} else {
			// console.error(chalk.yellow('[RemoveGuestInPlaylist]: Playlist not in guestInPlaylists list. user: ' + this.id + ', playlist: ' + id));
			return false;
		}
	}

	public setIsPremium(value: boolean): boolean {
		if (this.isPremium == value) {
			return false;
		}
		this.isPremium = value;
		return true;
	}

	/*
		For each setting, find it in the db and update the value
		// TODO: RM and just update user, then user.save()
	*/
	public async updateSettings(settings: Setting[]): Promise<any> {
		const client: MongoClient = await app.getClient();
		const db: Db = await app.getDB(client);
		
		let promises = settings.map(setting => {
			return db.collection("users").updateOne(
				{apiKey: this.apiKey, 'settings.name': setting.name},
				{$set: {'settings.$.value': setting.value}}
			);
		});
		await Promise.all(promises);
		await app.closeClient(client);
	}

	public filterByRelationship(user: User) {
		return settingsController.filterUserAttributesByClientRelationship(user, this);
	}

	/*
		to send to db and client
	*/
	public toPlainObj(): Object {
		return Object.assign({}, this)
	}

	/*
		Searches by id, email, displayName, firstName, lastName if those attributes are PUBLIC.
	*/
	static async search(apiKey: string, keyword: string): Promise<any> {
		const client: MongoClient = await app.getClient();
		const db: Db = await app.getDB(client);
		const regex: string = '.*' + keyword + '.*';
		const results = await db.collection("users").find({
			$and: [
				{apiKey: {$ne: apiKey}},
				{$or: [
					{id: {$regex: new RegExp(regex, 'i') }},
					{
						$and: [
							{email: {$regex: new RegExp(regex, 'i') }},
							{settings: {name: 'email', value: 'PUBLIC'}}
						]
					},
					{
						$and: [
							{displayName: {$regex: new RegExp(regex, 'i') }},
							{settings: {name: 'displayName', value: 'PUBLIC'}}
						]
					},
					{
						$and: [
							{firstName: {$regex: new RegExp(regex, 'i') }},
							{settings: {name: 'firstName', value: 'PUBLIC'}}
						]
					},
					{
						$and: [
							{lastName: {$regex: new RegExp(regex, 'i') }},
							{settings: {name: 'lastName', value: 'PUBLIC'}}
						]
					}
				]}
			]
		}).limit(config.mongo_search_limit).toArray();
		await app.closeClient(client);
		const users: User[] = results.map(x => new User({...x}));
		return users;
	}

	public resetActivationCode() {
		this.activationCode = bcrypt.hashSync(this.apiKey, salt);
		// set expiry date to +1 day
		let date: Date = new Date();
		date.setDate(date.getDate() + 1);
		this.activationCodeExpiryDate = date.toISOString();
		this.activated = false;
	}

	public activate() {
		this.activated = true;
		this.activationCode = null;
		this.activationCodeExpiryDate = null;  
	}

	public async sendActivationCode() : Promise<any> {
		if (!this.activationCode) return null;
		await sendEmail(this, 'activation');
	}

	public async sendPasswordResetEmail() : Promise<any> {
		if (!this.passwordResetCode) return null;
		await sendEmail(this, 'passwordReset');
	}

	public resetPassword(password: string) {
		this.password = password;
		this.passwordResetCode = null;
		this.passwordResetAttemptsRemaining = null;
	}

	public getDisplayName(): string {
		if (this.displayName) return this.displayName;
		if (this.firstName && this.lastName) return `${this.firstName} ${this.lastName}`;
		if (this.firstName) return this.firstName;
		if (this.email) return this.email;
 	}

	public getActivationAccountLink(): string {
		if (!this.activationCode) return '#';
		return `${config.serverName}${config.serverPort ? `:${config.serverPort}` : ''}/users/${this.id}/activate?code=${this.activationCode}`;
	}

	public async delete(): Promise<any> {
		const client: MongoClient = await app.getClient();
		const db: Db = await app.getDB(client);
		await db.collection("users").deleteOne({apiKey: this.apiKey});
		await app.closeClient(client);

		await Promise.all([
			// delete friendships
			...this.friendships.map(async friendshipId => {
				let friendship: Friendship = await Friendship.get({id: friendshipId});
				if (!friendship) {
					// console.error(chalk.red('[User.delete]: friendship to delete does not exist: ' + friendshipId));
					return ;
				}
				await acquireLock(lockKeys.friendship(friendshipId), async () => {
					friendship = await Friendship.get({id: friendshipId});
					if (friendship) await friendship.delete();
				});
			}),
			// delete playlists
			...this.playlists.map(async playlistId => {
				let playlist: Playlist = await Playlist.get({id: playlistId});
				if (!playlist) {
					// console.error(chalk.red('[User.delete]: playlist to delete does not exist: ' + playlistId));
					return ;
				}
				await acquireLock(lockKeys.playlist(playlistId), async () => {
					playlist = await Playlist.get({id: playlistId});
					if (playlist) await playlist.delete();
				});
			}),
			// delete events
			...this.events.map(async eventId => {
				let event: Event = await Event.get({id: eventId});
				if (!event) {
					// console.error(chalk.red('[User.delete]: event to delete does not exist: ' + eventId));
					return ;
				}
				await acquireLock(lockKeys.event(eventId), async () => {
					event = await Event.get({id: eventId});
					if (event) await event.delete();
				});
			}),
			// remove user as editor in playlists
			...this.editorInPlaylists.map(async playlistId => {
				let playlist: Playlist = await Playlist.get({id: playlistId});
				if (!playlist) {
					// console.error(chalk.red('[User.delete]: playlist in which to remove user as editor does not exist: ' + playlistId));
					return ;
				}
				await acquireLock(lockKeys.playlist(playlistId), async () => {
					playlist = await Playlist.get({id: playlistId});
					if (!playlist) return ;
					playlist.removeEditor(this.id);
					await playlist.save();
				});
			}),
			// remove user as guest in playlists
			...this.guestInPlaylists.map(async playlistId => {
				let playlist: Playlist = await Playlist.get({id: playlistId});
				if (!playlist) {
					// console.error(chalk.red('[User.delete]: playlist in which to remove user as guest does not exist: ' + playlistId));
					return ;
				}
				await acquireLock(lockKeys.playlist(playlistId), async () => {
					playlist = await Playlist.get({id: playlistId});
					if (!playlist) return;
					playlist.removeGuest(this.id);
					await playlist.save();
				});
			}),
			// remove user as guest in events
			...this.guestInEvents.map(async eventId => {
				let event: Event = await Event.get({id: eventId});
				if (!event) {
					// console.error(chalk.red('[User.delete]: event in which to remove user as guest does not exist: ' + eventId));
					return ;
				}
				await acquireLock(lockKeys.event(eventId), async () => {
					event = await Event.get({id: eventId});
					if (!event) return ;
					event.removeGuest(this.id);
					await event.save();
				});
			}),
			// remove user as dj in events
			...this.djInEvents.map(async eventId => {
				let event: Event = await Event.get({id: eventId});
				if (!event) {
					// console.error(chalk.red('[User.delete]: event in which to remove user as DJ does not exist: ' + eventId));
					return ;
				}
				await acquireLock(lockKeys.event(eventId), async () => {
					event = await Event.get({id: eventId});
					if (!event) return ;
					event.removeDJ(this.id);
					await event.save();
				});
			}),
			// delete avatar
			this.deleteAvatar()
		]);
	}
}

import { Friendship } from "./Friendship";
import { Playlist } from "./Playlist";
import { Event } from "./Event";

