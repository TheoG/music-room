import { uuid } from "../utils/uuid";

import EventTrack from "./EventTrack";
import { SortingType } from "../types/types";
import Track from "./Track";
import { Playlist } from "./Playlist";
import chalk from "chalk";

export class EventPlaylist {
	public id: string;
	public refPlaylistId: string;
	public name: string;

	public events: string[];
	public playedTracks: EventTrack[] = [];		// in order of history of played tracks
	public activeTrack: string = null;		// id of currently playing or paused track
	public unplayedTracks: EventTrack[] = [];	// tracks not played sorted according to vote or move

	public sortingType: SortingType; // move tracks, vote for tracks

	constructor(config: {id?: string, refPlaylistId?: string, name: string, playedTracks?: EventTrack[], activeTrack?: string, unplayedTracks?: EventTrack[], sortingType?: SortingType}) {
		if (config.hasOwnProperty('id')) {
			this.id = config.id;
		} else {
			this.id = uuid('eventplaylist');
		}
		this.refPlaylistId = config.refPlaylistId;
		this.name = config.name;
		this.playedTracks = (config.playedTracks && config.playedTracks.length > 0) ? config.playedTracks.map(x => new EventTrack(x)) : [];
		this.activeTrack = config.activeTrack || null;
		this.unplayedTracks = (config.unplayedTracks && config.unplayedTracks.length > 0) ? config.unplayedTracks.map(x => new EventTrack(x)) : [];
		this.sortingType = config.sortingType || 'vote';
	}

	static createFromPlaylist(playlist: Playlist) {
		return new EventPlaylist({
			refPlaylistId: playlist.id,
			name: playlist.name,
			unplayedTracks: playlist.tracks.map((track: Track) => EventTrack.createFromTrack(track)),
			sortingType: playlist.sortingType
		})
	}

	public play(trackid: string): boolean {
		if (this.activeTrack == trackid && this.getActiveTrack().isPlaying) return false;
		let activeTrack: EventTrack = this.getActiveTrack();
		if (activeTrack) {
			activeTrack.pause();
			this.activeTrack = null;
		}
		let i: number = this.unplayedTracks.findIndex(x => x.id == trackid);
		if (i > -1) {
			activeTrack = this.unplayedTracks.splice(i, 1)[0];
			this.playedTracks.push(activeTrack);
			this.activeTrack = activeTrack.id;
		} else {
			i = this.playedTracks.findIndex(x => x.id == trackid);
			if (i > -1) {
				this.activeTrack = this.playedTracks[i].id;
			} else  {
				return false;
			}
		}
		activeTrack = this.getActiveTrack();
		if (!activeTrack) return false;
		return activeTrack.play();
	}

	public pause(): boolean {
		let activeTrack: EventTrack = this.getActiveTrack();
		if (!activeTrack) return false;
		return this.getActiveTrack().pause();
	}

	public sortUnplayedTracks(): void {
		if (this.sortingType == 'move') return;
		// add index to each track
		this.unplayedTracks.forEach(track => (<any>track).index = this.unplayedTracks.indexOf(track));
		// sort
		this.unplayedTracks.sort((a: EventTrack, b: EventTrack) => {
			let ar = a.rank();
			let br = b.rank();
			if (ar > br) return -1;
			if (ar < br) return 1;
			return (<any>a).index - (<any>b).index;
		});
		// remove index
		this.unplayedTracks.forEach(track => delete (<any>track).index);
	}

	public likeTrack(trackid: string, userid: string): boolean {
		let track: EventTrack = this.getTrack(trackid);
		if (!track) return false;
		track.undislike(userid);
		if (track.like(userid)) {
			this.sortUnplayedTracks();
			return true;
		}
		return false;
	}

	public unlikeTrack(trackid: string, userid: string): boolean {
		let track: EventTrack = this.getTrack(trackid);
		if (!track) return false;
		if (track.unlike(userid)) {
			this.sortUnplayedTracks();
			return true;
		}
		return false;
	}

	public dislikeTrack(trackid: string, userid: string): boolean {
		let track: EventTrack = this.getTrack(trackid);
		if (!track) return false;
		track.unlike(userid);
		if (track.dislike(userid)) {
			this.sortUnplayedTracks();
			return true;
		}
		return false;
	}

	public undislikeTrack(trackid: string, userid: string): boolean {
		let track: EventTrack = this.getTrack(trackid);
		if (!track) return false;
		if (track.undislike(userid)) {
			this.sortUnplayedTracks();
			return true;
		}
		return false;
	}

	public moveTrack(trackid: string, index: number): boolean {
		let track: EventTrack = this.unplayedTracks.find(x => x.id == trackid);
		if (!track) return false;
		let oldIndex: number = this.unplayedTracks.findIndex(x => x.id == trackid);		
		this.unplayedTracks.splice(oldIndex, 1);
		this.unplayedTracks.splice(index, 0, track);
		return oldIndex != this.unplayedTracks.findIndex(x => x.id == trackid);
	}

	public getTrack(trackid: string): EventTrack {
		return [...this.unplayedTracks, ...this.playedTracks].find(x => x.id == trackid);
	}

	public getActiveTrack(): EventTrack {
		if (!this.activeTrack) return null;
		return [...this.unplayedTracks, ...this.playedTracks].find(x => x.id == this.activeTrack);
	}
}