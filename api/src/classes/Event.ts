import { Db, MongoClient } from "mongodb";
import chalk from "chalk";

import app from "../app";
import { User } from "./User";
import { Playlist } from "./Playlist";
import { Schedule, Visibility, Coordinates } from "../types/types";
import { acquireLock, lockKeys } from "../utils/mutex";
import { uuid } from "../utils/uuid"
import { EventPlaylist } from "./EventPlaylist";
import EventTrack from "./EventTrack";
import { socketController } from "../controllers/socketController";

const search_limit: number = require('../../config.json').mongo_search_limit;

export class Event {
	public id: string;
	public name: string;
	public description: string;
	public owner: string;
	public guests: string[] = [];
	public djs: string[] = [];

	public visibility: Visibility;

	public votingSchedule: Schedule;
	public eventSchedule: Schedule;
	public coordinates: Coordinates;

	public playlists: string[] = [];
	public eventPlaylists = []; // EventPlaylist[]
	public activePlaylist: string = null;

	public votingActive: boolean = false;
	public eventActive: boolean = false;

	public trackPosition: number = 0;
	public lastTrackActionTimestamp: string = null;

	constructor(config: {id?: string, name: string, owner: string, description?: string, visibility?: Visibility, guests?: string[], djs?: string[], votingSchedule?: Schedule, eventSchedule?: Schedule, coordinates?: Coordinates, playlists?: string[], eventPlaylists?: [], activePlaylist?: string, votingActive?: boolean, eventActive?: boolean, trackPosition?: number, lastTrackActionTimestamp?: string}) {
		if (config.hasOwnProperty('id')) {
			this.id = config.id;
		} else {
			this.id = uuid('event');
		}
		this.name = config.name;
		this.description = config.description;
		this.owner = config.owner;
		this.guests = config.guests || [];
		this.djs = config.djs || [];

		this.visibility = config.visibility || 'PUBLIC';

		this.votingSchedule = config.votingSchedule || null;
		this.eventSchedule = config.eventSchedule || null;
		this.coordinates = config.coordinates || null;

		this.playlists = config.playlists || []; // list of ids
		this.eventPlaylists = (config.eventPlaylists && config.eventPlaylists.length > 0) ? config.eventPlaylists.map(x => new EventPlaylist(x)) : []; // converted to class
		this.activePlaylist = config.activePlaylist || null; // id

		this.votingActive = config.votingActive || false;
		this.eventActive = config.eventActive || false;

		this.trackPosition = config.trackPosition || 0;
		this.lastTrackActionTimestamp = config.lastTrackActionTimestamp || null;
	}

	static async get(filter: object): Promise<Event> {
		const client: MongoClient = await app.getClient();
		const db: Db = await app.getDB(client);

		const dbEvent = await db.collection("events").findOne(filter);

		await app.closeClient(client);
		if (!dbEvent) return undefined;
		const event: Event = new Event({...dbEvent});
		return event;
	}

	static async getAll(overrideReady?: boolean): Promise<Event[]> {
		const client: MongoClient = await app.getClient(overrideReady);
		const db: Db = await app.getDB(client);
		var dbEvents: any[] = await db.collection("events").find().toArray();
		await app.closeClient(client);
		if (!dbEvents) return undefined;
		const events: Event[] = dbEvents.map(dbEvent => new Event({...dbEvent}));
		return events;
	}

	public async create() {
		const client: MongoClient = await app.getClient();
		const db: Db = await app.getDB(client);
		await db.collection("events").insertOne(this.toPlainObj());
		await app.closeClient(client);
	}

	public async save() {
		const client: MongoClient = await app.getClient();
		const db: Db = await app.getDB(client);
		await db.collection("events").updateOne({id: this.id}, { $set: this.toPlainObj() });
		await app.closeClient(client);
	}

	public addGuest(id: string): boolean  {
		if (!this.guests.find((x: string) => x == id)) {
			this.guests.push(id);
			return true;
		} else {
			// console.error(chalk.yellow('[Event.addGuest]: User already in guests. event: ' + this.id + ', user: ' + id));
			return false;
		}
	}

	public removeGuest(id: string): boolean  {
		let index = this.guests.findIndex((x: string) => x == id);
		if (index > -1) {
			this.guests.splice(index, 1);
			return true;
		} else {
			// console.error(chalk.yellow('[Event.removeGuest]: User not in guests. event: ' + this.id + ', user: ' + id));
			return false;
		}
	}

	public addDJ(id: string): boolean  {
		if (!this.djs.find((x: string) => x == id)) {
			this.djs.push(id);
			return true;
		} else {
			// console.error(chalk.yellow('[Event.addDJ]: User already in djs. event: ' + this.id + ', user: ' + id));
			return false;
		}
	}

	public removeDJ(id: string): boolean  {
		let index = this.djs.findIndex((x: string) => x == id);
		if (index > -1) {
			this.djs.splice(index, 1);
			return true;
		} else {
			// console.error(chalk.yellow('[Event.removeDJ]: User not in djs. event: ' + this.id + ', user: ' + id));
			return false;
		}
	}

	public userCanAccess(user: User): boolean {
		return this.visibility == 'PUBLIC' ||
			this.userIsOwner(user) ||
			this.userIsDJ(user) ||
			this.userIsGuest(user);
	}

	public userIsOwner(user: User): boolean {
		return this.owner == user.id;
	}

	public userIsGuest(user: User): boolean {
		return this.guests.includes(user.id);
	}

	public userIsDJ(user: User): boolean {
		return this.djs.includes(user.id);
	}

	public update(params: {name?: string, description?: string, visibility?: Visibility, votingSchedule?: Schedule, eventSchedule?: Schedule, coordinates?: Coordinates}): boolean {
		let updated: boolean = false;
		Object.keys(params).forEach(attribute => {
			if (params.hasOwnProperty(attribute)) {
				if (this[attribute] != params[attribute]) {
					updated = true;
				}
				this[attribute] = params[attribute];
			}
		});

		return updated;
	}

	public addPlaylist(id: string): boolean {
		if (!this.playlists.find((x: string) => x == id)) {
			this.playlists.push(id);
			return true;
		} else {
			// console.error(chalk.yellow('[Event.addPlaylist]: Playlist not in playlists of event: ' + this.id + ', playlist: ' + id));
			return false;
		}
	}

	public removePlaylist(id: string): boolean {
		let index = this.playlists.findIndex((x: string) => x == id);
		if (index > -1) {
			this.playlists.splice(index, 1);
			return true;
		} else {
			// console.error(chalk.yellow('[Event.removePlaylist]: Playlist not in playlists. event: ' + this.id + ', playlist: ' + id));
			return false;
		}
	}

	public likeTrack(playlistid: string, trackid: string, userid: string): boolean {
		if (!this.votingActive) return false;
		let playlist: EventPlaylist = this.eventPlaylists.find(x => x.id == playlistid);
		if (!playlist) return false;
		return playlist.likeTrack(trackid, userid);
	}

	public unlikeTrack(playlistid: string, trackid: string, userid: string): boolean {
		if (!this.votingActive) return false;
		let playlist: EventPlaylist = this.eventPlaylists.find(x => x.id == playlistid);
		if (!playlist) return false;
		return playlist.unlikeTrack(trackid, userid);
	}

	public dislikeTrack(playlistid: string, trackid: string, userid: string): boolean {
		if (!this.votingActive) return false;
		let playlist: EventPlaylist = this.eventPlaylists.find(x => x.id == playlistid);
		if (!playlist) return false;
		return playlist.dislikeTrack(trackid, userid);
	}

	public undislikeTrack(playlistid: string, trackid: string, userid: string): boolean {
		if (!this.votingActive) return false;
		let playlist: EventPlaylist = this.eventPlaylists.find(x => x.id == playlistid);
		if (!playlist) return false;
		return playlist.undislikeTrack(trackid, userid);
	}

	public moveTrack(playlistid: string, trackid: string, index: number): boolean {
		if (!this.votingActive) return false;
		let playlist: EventPlaylist = this.eventPlaylists.find(x => x.id == playlistid);
		if (!playlist) return false;
		return playlist.moveTrack(trackid, index);
	}

	public play(playlistid: string, trackid: string, timestamp: string, trackPosition: number): boolean {
		let activePlaylist: EventPlaylist = this.getActivePlaylist();
		if (activePlaylist) {
			activePlaylist.pause();
		}
		let playlist: EventPlaylist = this.eventPlaylists.find(x => x.id == playlistid);
		if (!playlist) return false;
		const updated: boolean = playlist.play(trackid);
		if (updated == false) return false;
		this.activePlaylist = playlist.id;
		this.updateTimestampAndTrackPosition(timestamp, trackPosition);
		return true;
	}

	public pause(timestamp: string, trackPosition: number): boolean {
		if (!this.activePlaylist) return false;
		let playlist: EventPlaylist = this.getActivePlaylist();
		if (!playlist) return false;
		const updated: boolean = playlist.pause();
		if (!updated) return false;
		this.updateTimestampAndTrackPosition(timestamp, trackPosition);
		return true;
	}

	public seek(timestamp: string, trackPosition: number): boolean {
		this.updateTimestampAndTrackPosition(timestamp, trackPosition);
		return true;
	}

	private updateTimestampAndTrackPosition(timestamp: string, trackPosition: number): void {
		this.lastTrackActionTimestamp = timestamp;
		this.trackPosition = trackPosition;
	}

	public toPlainObj(): Object {
		return Object.assign({}, this);
	}

	public async delete(): Promise<any> {
		const client: MongoClient = await app.getClient();
		const db: Db = await app.getDB(client);
		await db.collection("events").deleteOne({id: this.id});
		await app.closeClient(client);

		await Promise.all([
			// delete event in user.events
			(async () => {
				let user: User = await User.get({id: this.owner});
				if (!user) {
					// console.error(chalk.red('[Event.delete]: user does not exist. userid: ' + this.owner));
					return ;
				}
				await acquireLock(lockKeys.user(this.owner), async () => {
					user = await User.get({id: this.owner});
					if (!user) return;
					user.removeEvent(this.id);
					await user.save();
				});
			})(),
			// delete event in user.guestInEvents
			...this.guests.map(async (userId: string) => {
				let user: User = await User.get({id: userId});
				if (!user) {
					// console.error(chalk.red('[Event.delete]: user does not exist. userid: ' + userId));
					return ;
				}
				await acquireLock(lockKeys.user(userId), async () => {
					user = await User.get({id: userId});
					if (!user) return;
					user.removeGuestInEvent(this.id);
					await user.save();
				});
			}),
			// delete event in user.djsInEvents
			...this.djs.map(async (userId: string) => {
				let user: User = await User.get({id: userId});
				if (!user) {
					// console.error(chalk.red('[Event.delete]: user does not exist. userid: ' + userId));
					return ;
				}
				await acquireLock(lockKeys.user(userId), async () => {
					user = await User.get({id: userId});
					if (!user) return ;
					user.removeDJInEvent(this.id);
					await user.save();
				});
			}),
		]);
	}

	public async expand(client: User): Promise<any> {
		let ret: any = JSON.parse(JSON.stringify(this.toPlainObj()));
		await Promise.all([
			//expand owner
			(async () => {
				let user: User = await User.get({id: this.owner});
				if (user) {
					ret.owner = user.filterByRelationship(client);
				}
			})(),
			// expand guests
			(async () => {
				ret.guests = await Promise.all(this.guests.map(async id => {
					let user: User = await User.get({id});
					if (user) return user.filterByRelationship(client);
					return null;
				}));
			})(),
			// expand djs
			(async () => {
				ret.djs = await Promise.all(this.djs.map(async id => {
					let user: User = await User.get({id});
					if (user) return user.filterByRelationship(client);
					return null;
				}));
			})(),
			// expand playlists
			(async () => {
				ret.playlists = await Promise.all(this.playlists.map(async id => {
					let playlist: Playlist = await Playlist.get({id});
					if (playlist) return playlist;
					return null;
				}));
			})()
		]);
		return ret;
	}

	public async startVoting(): Promise<void> {
		await this.convertPlaylistsToEventPlaylists();
		this.votingActive = true;
	}

	public async endVoting(): Promise<void> {
		this.votingActive = false;
		await this.convertEventPlaylistsToPlaylists();
	}

	public async startEvent(): Promise<void> {
		await this.convertPlaylistsToEventPlaylists();
		this.eventActive = true;
	}

	public async endEvent(): Promise<void> {
		this.eventActive = false;
		await this.convertEventPlaylistsToPlaylists();
	}

	public getActivePlaylist(): EventPlaylist {
		if (!this.activePlaylist) return null;
		return this.eventPlaylists.find(x => x.id == this.activePlaylist);
	}

	public getEventPlaylistWithTrack(trackid: string) {
		return this.eventPlaylists.find((playlist: EventPlaylist) =>
			!!playlist.unplayedTracks.find(x => x.id == trackid) ||
			!!playlist.playedTracks.find(x => x.id == trackid));
	}

	public getStarterPack() {
		let starterPack: any = {
			votingActive: this.votingActive,
			eventActive: this.eventActive
		};

		if (!this.votingActive && !this.eventActive) return starterPack;

		starterPack.ownerIsConnected = socketController.connections.find(connection => connection.userId == this.owner && connection.room == this.id) ? true : false;

		starterPack.trackPosition = this.trackPosition;

		if (this.lastTrackActionTimestamp != null)
			starterPack.lastTrackActionTimestamp = this.lastTrackActionTimestamp;

		let activePlaylist: EventPlaylist = this.getActivePlaylist();
		if (activePlaylist) {
			this.activePlaylist = activePlaylist.id;
			let activeTrack: EventTrack = activePlaylist.getActiveTrack();
			if (activeTrack) {
				starterPack.activeTrack = activeTrack.id;
				starterPack.isPlaying = activeTrack.isPlaying;
			}
		}
		return starterPack;
	}

	/*
		Searches by id, ownerid, name, description, if event visibility is PUBLIC.
	*/
	static async search(keyword: string): Promise<any> {
		const client: MongoClient = await app.getClient();
		const db: Db = await app.getDB(client);
		const regex: string = '.*' + keyword + '.*';
		const results = await db.collection("events").find({
			$and: [
				{visibility: 'PUBLIC'},
				{$or: [
					{id: {$regex: new RegExp(regex, 'i') }},
					{name: {$regex: new RegExp(regex, 'i') }},
					{description: {$regex: new RegExp(regex, 'i') }},
					{owner: {$regex: new RegExp(regex, 'i') }},
				]}
			]
		}).limit(search_limit).toArray();
		await app.closeClient(client);
		const events: Event[] = results.map(x => new Event({...x}));
		return events;
	}

	private async convertPlaylistsToEventPlaylists(): Promise<void> {
		if (this.eventPlaylists && this.eventPlaylists.length > 0) return ;
		this.eventPlaylists = await Promise.all(this.playlists.map(async (playlistid: string) => {
			let playlist: Playlist = await Playlist.get({id: playlistid});
			await this.removePlaylist(playlistid);
			if (!playlist) {
				// console.log(chalk.red('playlist does not exist'));
				return;
			}
			return EventPlaylist.createFromPlaylist(playlist);
		}));
	}

	private async convertEventPlaylistsToPlaylists(): Promise<void> {
		if (!this.eventPlaylists || this.eventPlaylists.length == 0 || this.votingActive || this.eventActive) return ;
		await Promise.all(this.eventPlaylists.map(async (eventPlaylist: EventPlaylist) => {
			let playlist: Playlist = await Playlist.get({id: eventPlaylist.refPlaylistId});
			if (!playlist) return null;
			await this.addPlaylist(playlist.id);
		}));
		this.eventPlaylists = [];
		this.activePlaylist = null;
		this.lastTrackActionTimestamp = null;
		this.trackPosition = 0;
	}
}
