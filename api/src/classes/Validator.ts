import { ServerError, ErrorCodes } from "../utils/errorTypes";
import SettingsController from "../controllers/settingsController";
import { Setting, MasterSetting } from "./Setting"; 
import { Schedule, Theme } from "../types/types";
import * as Jimp from 'jimp';
import * as fs from 'fs';
import * as util from 'util';
const unlink = util.promisify(fs.unlink);

const emailRegex: RegExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

class Parameter {
	constructor(public name: string, public origin: 'query' | 'param' | 'body') {}
}

/**
 *	Validates values in param. Expects them to exist if in mandatory
 * 	Does not access database
 */

class Validator {
	public err: Promise<ServerError> = null;
	public params: object;
	public mandatoryParams: Parameter[];

	constructor(params: object, mandatory?: Parameter[]) {
		this.params = params;
		this.mandatoryParams = mandatory || [];
		this.err = this.validate();
	}

	private _validate = {
		apiKey: (val: string): ServerError => this._validate._isString(val, 'apiKey'),
		id: (val: string): ServerError => this._validate._isString(val, 'id'),
		userid: (val: string): ServerError => this._validate._isString(val, 'userid'),
		friendid: (val: string): ServerError => this._validate._isString(val, 'friendid'),
		friendshipid: (val: string): ServerError => this._validate._isString(val, 'friendshipid'),
		playlistid: (val: string): ServerError => this._validate._isString(val, 'playlistid'),
		trackid: (val: string): ServerError => this._validate._isString(val, 'trackid'),
		eventid: (val: string): ServerError => this._validate._isString(val, 'eventid'),
		eventids: (val: string): ServerError => this._validate._isArrayOfStrings(val, 'eventids'),
		userids: (val: string): ServerError => this._validate._isArrayOfStrings(val, 'userids'),
		playlistids: (val: string): ServerError => this._validate._isArrayOfStrings(val, 'playlistids'),
		displayName: (val: string): ServerError => this._validate._isString(val, 'displayName'),
		firstName: (val: string): ServerError => this._validate._isString(val, 'firstName'),
		lastName: (val: string): ServerError => this._validate._isString(val, 'lastName'),
		accessToken: (val: string): ServerError => this._validate._isString(val, 'accessToken'),
		musicalPreferences: (val: string): ServerError => {
			let err: ServerError = this._validate._isString(val, 'musicalPreferences');
			if (err) return err;
			if (val.length > 180) {
				return new ServerError(ErrorCodes.BAD_REQUEST, 'musicalPreferences cannot be longer than 180 characters');
			}
		},
		email: (val: string): ServerError => {
			let err: ServerError = this._validate._isString(val, 'email')
			if (err) return err;
			if (!emailRegex.test(val)) {
				return new ServerError(ErrorCodes.INVALID_EMAIL);
			}
		},
		password: (val: string): ServerError => {
			let err: ServerError = this._validate._isString(val, 'password');
			if (err) return err;
			// password verification done in middleware
			return null;
		},
		keyword: (val: string): ServerError => {
			let err: ServerError = this._validate._isString(val, 'keyword')
			if (err) return err;
			if (val.length < 3) {
				return new ServerError(ErrorCodes.BAD_REQUEST, 'Keyword must be at least 3 characters long');
			}
		},
		user_settings: (params: object): ServerError => {
			// iterate over object key values
			for (const key in params) {
				// check if key has value
				if (params.hasOwnProperty(key) && params[key] != undefined) {
					const masterSetting = SettingsController.masterSettings.find(x => x.name === key);
					if (!masterSetting) {
						return new ServerError(ErrorCodes.INVALID_SETTING, 'Invalid setting name: ' + key);
					}
					// check if value acceptable based on masterSetting
					if (!masterSetting.values.includes(params[key])) {
						return new ServerError(ErrorCodes.INVALID_SETTING, 'Invalid setting value: ' + params[key]);
					}
				}
			}
		},
		message: (val: string): ServerError => this._validate._isString(val, 'message'),
		friendshipaction: (val: string): ServerError => {
			if (!['read', 'accept', 'reject'].includes(val)) {
				return new ServerError(ErrorCodes.INVALID_FRIENDSHIP_ACTION);
			}
		},
		eventaction: (val: string): ServerError => {
			if (!['startvoting', 'endvoting', 'startevent', 'endevent'].includes(val)) {
				return new ServerError(ErrorCodes.INVALID_EVENT_ACTION);
			}
		},
		playeraction: (val: string): ServerError => {
			if (!['play', 'pause', 'seek'].includes(val)) {
				return new ServerError(ErrorCodes.INVALID_PLAYER_ACTION);
			}
			if (val === 'play') {
				if (!this.params.hasOwnProperty('trackid'      ) || this.params['trackid'      ] == undefined) return new ServerError(ErrorCodes.MISSING_PARAMETER, null, 'trackid'      , 'body');
			} else if (val == 'seek') {
				if (!this.params.hasOwnProperty('trackPosition') || this.params['trackPosition'] == undefined) return new ServerError(ErrorCodes.MISSING_PARAMETER, null, 'trackPosition', 'body');
			}
		},
		name: (val: string): ServerError => {
			let err: ServerError = this._validate._isString(val, 'name'); 
			if (err) return err;
			if (val.length == 0) {
				return new ServerError(ErrorCodes.INVALID_NAME);
			}
			if (val.length > 72) {
				return new ServerError(ErrorCodes.INVALID_NAME, 'Name too long');
			}
		},
		visibility: (val: string): ServerError => {
			if (!['PUBLIC', 'PRIVATE'].includes(val)) {
				return new ServerError(ErrorCodes.INVALID_VISIBILITY);
			}
		},
		editing: (val: string): ServerError => {
			if (!['PUBLIC', 'PRIVATE'].includes(val)) {
				return new ServerError(ErrorCodes.INVALID_EDITING);
			}
		},
		sortingType: (val: string): ServerError => {
			if (!['vote', 'move'].includes(val)) {
				return new ServerError(ErrorCodes.INVALID_SORTING_TYPE);
			}
		},
		service: (val: string): ServerError => this._validate._isString(val, 'service'),
		code: (val: string): ServerError => this._validate._isString(val, 'code'),
		index: (val: string): ServerError => {
			let i:number = Number(val);
			if (isNaN(i) || i < 0 || i % 1 !== 0) {
				return new ServerError(ErrorCodes.BAD_REQUEST, 'index must be a positive integer');
			}
		},
		description: (val: string): ServerError => this._validate._isString(val, 'description'),
		votingSchedule: (val: Schedule): ServerError => this._validate._schedule(val, 'votingSchedule'),
		eventSchedule: (val: Schedule): ServerError => this._validate._schedule(val, 'eventSchedule'),
		_schedule: (val: Schedule, propertyName: string): ServerError => {
			if (val === null) return ;
			if (typeof val != 'object' ||
				Object.keys(val).length != 2 ||
				typeof(val.start) != 'string' ||
				typeof(val.end) != 'string' ) {
					return new ServerError(ErrorCodes.BAD_REQUEST, `${propertyName} must be a Schedule{start: string, end: string} object`);
			}
			if (isNaN((new Date(val.start)).getTime()) ||
				isNaN((new Date(val.end)).getTime())) {
					return new ServerError(ErrorCodes.BAD_REQUEST, `${propertyName} start or end is invalid date`);					
			}
			if ((new Date(val.start)).getTime() > (new Date(val.end)).getTime()) {
				return new ServerError(ErrorCodes.BAD_REQUEST, `${propertyName}: start time must be before end time`);					
			}
		},
		coordinates: (val: Coordinates): ServerError => {
			if (val === null) return ;
			if (typeof val != 'object' ||
				Object.keys(val).length != 2 ||
				typeof(val.latitude) != 'number' ||
				typeof(val.longitude) != 'number' ) {
					return new ServerError(ErrorCodes.BAD_REQUEST, `coordinates must be a Coordinates{latitude: number, longitude: number} object`);
			}
			if (Math.abs(val.latitude) > 90) {
				return new ServerError(ErrorCodes.BAD_REQUEST, `coordinates.latitude must be between -90 and 90`);
			}
			if (Math.abs(val.longitude) > 180) {
				return new ServerError(ErrorCodes.BAD_REQUEST, `coordinates.longitude must be between -180 and 180`);
			}
		},
		theme: (val: Theme): ServerError => {
			const themes: Theme[] = ['dark', 'light'];
			if (!themes.includes(val)) {
				return new ServerError(ErrorCodes.BAD_REQUEST, 'theme invalid. Available themes: ' + themes.join());
			}
		},
		avatar: async (val: string): Promise<ServerError> => {
			const MIN_IMG_DIM: number = 200;
			let ret: ServerError = null;
			try {
				// check if is image
				var img: Jimp = await Jimp.read(val);
				// check min dimensions
				if (img.getHeight() < MIN_IMG_DIM || img.getWidth() < MIN_IMG_DIM) {
					ret = new ServerError(ErrorCodes.IMAGE_ERROR, `Image too small. Must be at least ${MIN_IMG_DIM}x${MIN_IMG_DIM}`);
				}
			} catch {
				ret = new ServerError(ErrorCodes.IMAGE_ERROR);
			}
			if (ret) {
				await unlink(val);
				return ret;
			}
		},
		inviteGuests: async (val: string): Promise<ServerError> => this._validate._isBoolean(val, 'inviteGuests'),
		inviteEditors: async (val: string): Promise<ServerError> => this._validate._isBoolean(val, 'inviteEditors'),
		copyGuests: async (val: string): Promise<ServerError> => this._validate._isBoolean(val, 'copyGuests'),
		copyEditors: async (val: string): Promise<ServerError> => this._validate._isBoolean(val, 'copyEditors'),
		copyPlaylist: async (val: string): Promise<ServerError> => this._validate._isBoolean(val, 'copyPlaylist'),
		duplicatePlaylist: async (val: string): Promise<ServerError> => this._validate._isBoolean(val, 'duplicatePlaylist'),
		_isBoolean: (val: any, propertyName: string): ServerError => {
			if (typeof val !== 'boolean') {
				return new ServerError(ErrorCodes.BAD_REQUEST, `${propertyName} must be a boolean`);
			}
		},
		_isString: (val: any, propertyName: string): ServerError => {
			if (typeof val !== 'string') {
				return new ServerError(ErrorCodes.BAD_REQUEST, `${propertyName} must be a string`);
			}
		},
		_isArrayOfStrings: (val: any, propertyName: string): ServerError => {
			if (!Array.isArray(val) || val.some(x => typeof x !== 'string')) {
				return new ServerError(ErrorCodes.BAD_REQUEST, `${propertyName} must be an array of strings`);
			}
		},
		timestamp: (val: any, propertyName: string): ServerError => {
			if (isNaN((new Date(val)).getTime())) {
				return new ServerError(ErrorCodes.BAD_REQUEST, `timestamp is invalid date`);					
			}
		},
		trackPosition: async (val: any): Promise<ServerError> => {
			let err: ServerError = this._validate._isNumber(val, 'trackPosition');
			if (err) return err;
			val = parseFloat(val);
			if (val < 0) {
				return new ServerError(ErrorCodes.BAD_REQUEST, `trackPosition must be a positive number`);
			}
		},
		_isNumber: (val: any, propertyName: string): ServerError => {
			if (typeof val !== 'number') {
				return new ServerError(ErrorCodes.BAD_REQUEST, `${propertyName} must be a number`);
			}
		},
		vote: (val: any): ServerError => {
			if (!['like', 'dislike'].includes(val)) {
				return new ServerError(ErrorCodes.BAD_REQUEST, `'vote' must be a either 'like' or 'dislike'`);
			}
		},
	}

	private async validate(): Promise<ServerError> {
		for (const key in this.params) {
			// check if mandatory
			const param: Parameter = this.mandatoryParams ? this.mandatoryParams.find(x => x.name === key) : null;			
			if (param != null && this.params[key] == undefined) {
				const err: ServerError = new ServerError(ErrorCodes.MISSING_PARAMETER, null, param.name, param.origin);
				return err;
			}

			// check if valid
			if (this._validate.hasOwnProperty(key) && this.params[key] != undefined) {
				const ret: ServerError = await this._validate[key](this.params[key]);
				if (ret) return ret;
			}
		}
		return null;
	}

	public printParams(): void {
		Object.keys(this.params).forEach((key: string) => {
			if (this.params[key] != undefined) {
				console.log(key, ':', this.params[key]);
			}
		})
	}
}

export {
	Parameter,
	Validator
};