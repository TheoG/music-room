import { uuid } from "../utils/uuid"
import { MongoClient, Db } from "mongodb";
import chalk from "chalk";

import app from "../app";
import { acquireLock, lockKeys } from "../utils/mutex";

class FriendshipRequest {
	
	public from: string|User;
	public to: string;
	public message: string;
	public createdOn: Date;
	public read: boolean;
	
	constructor(config) {

		this.from = config.from;
		this.to = config.to;
		this.message = config.message;
		if (config.hasOwnProperty('createdOn')) {
			// build from database
			this.createdOn = new Date(config.createdOn);
			this.read = config.read;
		} else {
			this.createdOn = new Date();
			this.read = false;
		}
	}
}

export class Friendship {
	public id: string;
	public pending: boolean;
	public request: FriendshipRequest;
	public acceptedOn: Date;
	public users: string[];

	constructor(config) {
		if (config.hasOwnProperty('id')) {
			this.id = config.id;
			this.pending = config.pending;
			if (this.pending && config.request) {
				this.request = new FriendshipRequest(config.request);
			} else {
				this.request = null;
			}
			if (config.acceptedOn != null) {
				this.acceptedOn = new Date(config.acceptedOn)
			} else {
				this.acceptedOn = null;
			}
			this.users = config.users;
		} else {
			this.id = uuid('friendship');
			this.pending = true;
			this.request = new FriendshipRequest({
				from: config.from,
				to: config.to,
				message: config.message,
			})
			this.users = [config.from, config.to];
		}
	}

	public async create(): Promise<any> {
		const client: MongoClient = await app.getClient();
		const db: Db = await app.getDB(client);
		await db.collection("friendships").insertOne(this.toPlainObj());
		await app.closeClient(client);
	}

	public async save(): Promise<any> {
		const client: MongoClient = await app.getClient();
		const db: Db = await app.getDB(client);
		await db.collection("friendships").updateOne({id: this.id}, { $set: this.toPlainObj() });
		await app.closeClient(client);
	}

	public async delete(): Promise<any> {
		const client: MongoClient = await app.getClient();
		const db: Db = await app.getDB(client);
		await db.collection("friendships").deleteOne({id: this.id});
		await app.closeClient(client);

		// for both users of friendship ... 
		await Promise.all(this.users.map(async id => {
			// get user
			let user: User = await User.get({id});
			if (!user) {
				// console.error(chalk.yellow('[Friendship.delete]: user does not exist. userid: ' + id));
				return ;
			}
			await acquireLock(lockKeys.user(id), async () => {
				user = await User.get({id});
				if (!user) return;
				// remove friendship
				user.removeFriendship(this.id);
				// remove friend if friendship is not pending
				if (!this.pending) {
					user.removeFriend(this.users.find(x => x != user.id));
				} 
				// update user in database
				await user.save();
			});
		}));
	}

	static async get(filter): Promise<any> {
		const client: MongoClient = await app.getClient();
		const db: Db = await app.getDB(client);
		const dbFriendship = await db.collection("friendships").findOne(filter);
		await app.closeClient(client);
		if (!dbFriendship) return undefined;
		const friendship: Friendship = new Friendship({...dbFriendship});
		return friendship;
	}

	public read(): void {
		this.request.read = true;
	}

	public accept(): void {
		this.pending = false;
		this.request = null;
		this.acceptedOn = new Date();
	}

	public toPlainObj() {
		return Object.assign({}, this)
	}
}

import { User } from './User';
