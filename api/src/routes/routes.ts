import {Request, Response} from "express";
const RateLimit = require('express-rate-limit');
import * as multer from 'multer';
const upload = multer({ dest: './uploads/' });

import {UserController, userController} from "../controllers/userController"
import {FriendshipController, friendshipController} from "../controllers/friendshipController";
import {PlaylistController, playlistController} from "../controllers/playlistController";
import { EventController, eventController } from "../controllers/eventController";
import ResponseStatusTypes from "../utils/ResponseStatusTypes";
import { env } from "../utils/env";

export class Routes {

  private user: UserController = userController;
  private friendship: FriendshipController = friendshipController;
  private playlist: PlaylistController = playlistController;
  private event: EventController = eventController;
  
  private userCreationLimiter = null; 

  constructor() {
	  if (env.limitLoad()) {
		  this.userCreationLimiter = new RateLimit({
			  windowMs: 60 * 60 * 1000,
			  max: 10,
			  message: 'User creation limited to 10 per hour per IP'
		  });
	  }
  }

  public routes(app): void {
	app.post('/user/')

	app.get('/', this.root);

	app.get('/redirect', (req, res) => {
		res.write("ok");
		res.end();
	});

	/* ========== USERS ========== */
	{
	/**
	 * @api {get} /users/:userid/settings Get user settings
	 * @apiName GetUserSettings
	 * @apiGroup Users
	 * @apiVersion 1.0.0
	 * 
	 * @apiParam (Params) {String} userid The user's id.
	 * 
	 * @apiParam (Queries) {String} apiKey The user's api key.
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *    "email": "PUBLIC",
	 *    "displayName": "PUBLIC",
	 *    "firstName": "PUBLIC",
	 *    "lastName": "PUBLIC",
	 *    "friends": "PUBLIC",
	 *    "musicalPreferences": "PUBLIC",
	 *    "playlists": "PUBLIC",
	 *    "guestInPlaylists": "FRIENDS",
	 *    "editorInPlaylists": "FRIENDS",
	 *    "events": "PUBLIC",
	 *    "guestInEvents": "FRIENDS",
	 *    "djInEvents": "FRIENDS",
	 *    "availableSettings": {
	 *        "email": [
	 *            "PUBLIC",
	 *            "FRIENDS",
	 *            "PRIVATE"
	 *        ],
	 *        "displayName": [
	 *            "PUBLIC",
	 *            "FRIENDS",
	 *            "PRIVATE"
	 *        ],
	 *        "firstName": [
	 *            "PUBLIC",
	 *            "FRIENDS",
	 *            "PRIVATE"
	 *        ],
	 *        "lastName": [
	 *            "PUBLIC",
	 *            "FRIENDS",
	 *            "PRIVATE"
	 *        ],
	 *        "friends": [
	 *            "PUBLIC",
	 *            "FRIENDS",
	 *            "PRIVATE"
	 *        ],
	 *        "musicalPreferences": [
	 *            "PUBLIC",
	 *            "FRIENDS",
	 *            "PRIVATE"
	 *        ],
	 *        "playlists": [
	 *            "PUBLIC",
	 *            "FRIENDS",
	 *            "PRIVATE"
	 *        ],
	 *        "guestInPlaylists": [
	 *            "PUBLIC",
	 *            "FRIENDS",
	 *            "PRIVATE"
	 *        ],
	 *    		"editorInPlaylists": [
	 *            "PUBLIC",
	 *            "FRIENDS",
	 *            "PRIVATE"
	 *        ],
	 *    		"events": [
	 *            "PUBLIC",
	 *            "FRIENDS",
	 *            "PRIVATE"
	 *        ],
	 *    		"guestInEvents": [
	 *            "PUBLIC",
	 *            "FRIENDS",
	 *            "PRIVATE"
	 *        ],
	 *        "djInEvents": [
	 *            "PUBLIC",
	 *            "FRIENDS",
	 *            "PRIVATE"
	 *        ]
	 *     }
	 * }
	 */
	app.get('/users/:userid/settings', this.user.getSettings);

	/**
	 * @api {put} /users/:userid/settings Update user settings
	 * @apiName UpdateUserSettings
	 * @apiGroup Users
	 * @apiVersion 1.0.0
	 * 
	 * @apiParam (Params) {String} userid The user's id.
	 * 
	 * @apiParam (Queries) {String} apiKey The user's api key.
	 * 
	 * @apiParam (Body) {String="PRIVATE","FRIENDS","PUBLIC"} [email] Privacy seting for the user's email
	 * @apiParam (Body) {String="PRIVATE","FRIENDS","PUBLIC"} [displayName] Privacy seting for the user's displayName
	 * @apiParam (Body) {String="PRIVATE","FRIENDS","PUBLIC"} [firstName] Privacy seting for the user's firstName
	 * @apiParam (Body) {String="PRIVATE","FRIENDS","PUBLIC"} [lastName] Privacy seting for the user's lastName
	 * @apiParam (Body) {String="PRIVATE","FRIENDS","PUBLIC"} [friends] Privacy seting for the user's friends
	 * @apiParam (Body) {String="PRIVATE","FRIENDS","PUBLIC"} [musicalPreferences] Privacy seting for the user's musicalPreferences
	 * @apiParam (Body) {String="PRIVATE","FRIENDS","PUBLIC"} [playlists] Privacy seting for the user's playlists
	 * @apiParam (Body) {String="PRIVATE","FRIENDS","PUBLIC"} [guestInPlaylists] Privacy seting for the user's guestInPlaylists
	 * @apiParam (Body) {String="PRIVATE","FRIENDS","PUBLIC"} [editorInPlaylists] Privacy seting for the user's editorInPlaylists
	 * @apiParam (Body) {String="PRIVATE","FRIENDS","PUBLIC"} [events] Privacy seting for the user's events
	 * @apiParam (Body) {String="PRIVATE","FRIENDS","PUBLIC"} [guestInEvents] Privacy seting for the user's guestInEvents
	 * @apiParam (Body) {String="PRIVATE","FRIENDS","PUBLIC"} [djInEvents] Privacy seting for the user's djInEvents
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
     *    "message": "Settings updated"
	 * }
	 */
	app.put('/users/:userid/settings', this.user.updateSettings);

	/**
	 * @api {get} /users/search Search users
	 * @apiName SearchUsers
	 * @apiGroup Users
	 * @apiVersion 1.0.0
	 * 
	 * @apiParam (Queries) {String} apiKey The user's api key.
	 * @apiParam (Queries) {String} keyword Keyword[s] to search with.
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
     *     "results": [
     *         {
     *            "id": "USER-8bd822eb-6bfa-4d99-b995-911e84acc848",
     *            "avatar": "avatars/default-avatars/avatar-001.png",
     *            "isPremium": true,
     *            "email": "a@a.com",
     *            "displayName": null,
     *            "firstName": null,
     *            "lastName": null,
     *            "friends": [],
     *            "playlists": [],
     *            "events": [
     *                "EVEN-b19118d4-8715-4a71-bb6f-379e5fbf3232"
     *            ]
     *        },
     *        {
     *            "id": "USER-baaa8163-367c-432f-a7fd-37c82dccef9c",
     *            "avatar": "avatars/default-avatars/avatar-001.png",
     *            "isPremium": true,
     *            "email": "b@b.com",
     *            "displayName": null,
     *            "firstName": null,
     *            "lastName": null,
     *            "musicalPreferences": null,
     *            "friends": [],
     *            "playlists": [
     *                "PLAY-01999d74-4740-449c-9e3f-0a7b8bd3a65b",
     *                "PLAY-864a4ccb-e2da-4719-a931-7d01f67e6e0f"
     *            ],
     *            "events": [
     *                "EVEN-a7dbf7d6-9995-46d5-825e-b82b3626ae70",
     *                "EVEN-97bdfa36-a524-41cc-acd0-700a7a60c0bf"
     *            ]
	 *        }
	 *    ]
	 * }
	 */
	app.get('/users/search', this.user.search);

	/**
	 * @api {post} /users/resetpassword Reset Password Request
	 * @apiName ResetPasswordRequest
	 * @apiGroup Users
	 * @apiVersion 1.0.0
	 * 
	 * @apiParam (Body) {String} email The user's email.
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "message": "sent new password reset code"
	 * }
	 */
	/**
	 * @api {post} /users/resetpassword Reset Password
	 * @apiName ResetPassword
	 * @apiGroup Users
	 * @apiVersion 1.0.0
	 * 
	 * @apiParam (Body) {String} email The user's email.
	 * @apiParam (Body) {String} code The reset code.
	 * @apiParam (Body) {String} password The user's new password.
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "message": "Password updated"
	 * }
	 */
	app.post('/users/resetpassword', this.user.resetPassword);

	/**
	 * @api {get} /users/:id Get user 
	 * @apiVersion 1.0.0
	 * @apiName GetUser
	 * @apiGroup Users
	 * 
	 * @apiParam (Param) {String} id The requested user's id.
	 * 
	 * @apiParam (Queries) {String} apiKey The requesting user's apiKey.
	 * @apiSuccess {String} [theme] If user is self
	 * @apiSuccess {String} [apiKey] If user is self
	 * @apiSuccess {String} [friendships] If user is self
	 * @apiSuccess {String[]} [guestInPlaylists] If user is self
	 * @apiSuccess {String[]} [editorInPlaylists] If user is self
	 * @apiSuccess {String[]} [guestInEvents] If user is self
	 * @apiSuccess {String[]} [djInEvents] If user is self
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *    "id": "USER-989b9b18-a7db-4090-9ffb-c0ca51940d1f",
	 *    "avatar": "avatars/default-avatars/avatar-001.png",
	 *    "isPremium": false,
	 *    "theme": null,
	 *    "apiKey": "_KEY-892772c3-e838-4301-ab61-405d01ba61a7",
	 *    "friendships": [],
	 *    "email": "a@a.com",
	 *    "displayName": null,
	 *    "firstName": null,
	 *    "lastName": null,
	 *    "musicalPreferences": null,
	 *    "friends": [],
	 *    "playlists": [],
	 *    "guestInPlaylists": [],
	 *    "editorInPlaylists": [],
	 *    "events": [],
	 *    "guestInEvents": [],
	 *    "djInEvents": []
	 * }
	 */
	app.get('/users/:id', this.user.get);
	
	/**
	 * @api {post} /users Create user 
	 * @apiVersion 1.0.0
	 * @apiName CreateUser
	 * @apiGroup Users
	 * 
	 * @apiParam (Body) {String} email The user's email.
	 * @apiParam (Body) {String} password The user's password.
	 * @apiParam (Body) {String} [displayName] The user's name to be displayed..
	 * @apiParam (Body) {String} [firstName] The user's first name.
	 * @apiParam (Body) {String} [lastName] The user's last name.
	 * @apiParam (Body) {String} [theme] The user's theme.
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *    "message": "User created. Please activate account for apiKey.",
	 *    "id": "USER-989b9b18-a7db-4090-9ffb-c0ca51940d1f"
	 * }
	 */
	if (this.userCreationLimiter) {
		app.post('/users', this.userCreationLimiter, this.user.createWithEmail);
	} else {
		app.post('/users', this.user.createWithEmail);
	}
	
	/**
	 * @api {put} /users/:id Update user 
	 * @apiVersion 1.0.0
	 * @apiName UpdateUser
	 * @apiGroup Users
	 * 
	 * @apiParam (Param) {String} id The user's id.
	 * 
	 * @apiParam (Queries) {String} apiKey The user's apiKey.
	 *
	 * @apiParam (Body) {String} [password] The user's password.
	 * @apiParam (Body) {String} [displayName] The user's name to be displayed.
	 * @apiParam (Body) {String} [firstName] The user's first name.
	 * @apiParam (Body) {String} [lastName] The user's last name.
	 * @apiParam (Body) {String} [musicalPreferences] The user's muscial preferences.
	 * @apiParam (Body) {String} [theme] The user's theme.
	 * 
	 * @apiParam (File) {String} [avatar] The user's avatar.
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *    "id": "USER-989b9b18-a7db-4090-9ffb-c0ca51940d1f",
	 *    "avatar": "avatars/default-avatars/avatar-001.png",
	 *    "isPremium": false,
	 *    "theme": null,
	 *    "apiKey": "_KEY-892772c3-e838-4301-ab61-405d01ba61a7",
	 *    "friendships": [],
	 *    "email": "a@a.com",
	 *    "displayName": null,
	 *    "firstName": null,
	 *    "lastName": null,
	 *    "musicalPreferences": null,
	 *    "friends": [],
	 *    "playlists": [],
	 *    "guestInPlaylists": [],
	 *    "editorInPlaylists": [],
	 *    "events": [],
	 *    "guestInEvents": [],
	 *    "djInEvents": [],
	 *    "message": "Update success"
	 * }
	 */
	app.put('/users/:id', upload.single('avatar'), this.user.update);
	
	/**
	 * @api {delete} /users/:userid Delete user
	 * @apiVersion 1.0.0
	 * @apiName DeleteUser
	 * @apiGroup Users
	 * 
	 * @apiParam (Params) {String} userid The user's id.
	 * 
	 * @apiParam (Queries) {String} apiKey The user's apiKey.
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "message": "Delete success"
	 * }
	 */
	app.delete('/users/:userid', this.user.delete);
	
	/**
	 * @api {get} /users/:userid/activate Activate user
	 * @apiVersion 1.0.0
	 * @apiName ActivateUser
	 * @apiGroup Users
	 * 
	 * @apiParam (Params) {String} userid The user's id.
	 * 
	 * @apiParam (Queries) {String} code The validation code.
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "apiKey": "_KEY-892772c3-e838-4301-ab61-405d01ba61a7",
	 *     "message": "account activated"
	 * }
	 */
	/**
	 * @api {get} /users/:userid/activate Send activation code
	 * @apiVersion 1.0.0
	 * @apiName SendActivationCode
	 * @apiGroup Users
	 * 
	 * @apiParam (Params) {String} userid The user's id.
	 * 
	 * @apiParam (Queries) {String} [email] Update the user's email before sending new activation code.
	 */
	app.get('/users/:userid/activate', this.user.activateAccount);

	/**
	 * @api {put} /users/:userid/premium Activate Premium
	 * @apiVersion 1.0.0
	 * @apiName ActivatePremium
	 * @apiGroup Users
	 * 
	 * @apiParam (Params) {String} userid The user's id.
	 * 
	 * @apiParam (Queries) {String} apiKey The user's apiKey.
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "message": "Account upgraded"
	 * }
	 */
	app.put('/users/:userid/premium', this.user.updateIsPremium);

	/**
	 * @api {delete} /users/:userid/premium Disactivate Premium
	 * @apiVersion 1.0.0
	 * @apiName DisctivatePremium
	 * @apiGroup Users
	 * 
	 * @apiParam (Params) {String} userid The user's id.
	 * 
	 * @apiParam (Queries) {String} apiKey The user's apiKey.
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "message": "Account downgraded"
	 * }
	 */
	app.delete('/users/:userid/premium', this.user.updateIsPremium);

	/**
	 * @api {get} /users/:userid/friends Get friends
	 * @apiVersion 1.0.0
	 * @apiName GetFriends
	 * @apiGroup Friends
	 * 
	 * @apiParam (Params) {String} userid The user's id.
	 * 
	 * @apiParam (Queries) {String} apiKey The user's apiKey.
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "friends": [
	 *         {
	 *             "id": "USER-8ed6e858-ae0f-4e40-b2e1-eea77107d693",
	 *             "avatar": "avatars/default-avatars/avatar-001.png",
	 *             "isPremium": true,
	 *             "email": "b@b.com",
	 *             "displayName": "",
	 *             "firstName": null,
	 *             "lastName": null,
	 *             "musicalPreferences": null,
	 *             "friends": [
	 *                 "USER-a8c4b5cb-810c-4c6c-a834-ccf40732095f"
	 *             ],
	 *             "playlists": [
	 *                 "PLAY-3580bfbd-d415-4c6b-a1be-74fb7e342d95"
	 *             ],
	 *             "guestInPlaylists": [],
	 *             "editorInPlaylists": [],
	 *             "events": [
	 *                 "EVEN-86cac9bf-bb5c-4757-85f1-822c7abb6e7d"
	 *             ],
	 *             "guestInEvents": [],
	 *             "djInEvents": []
	 *         }
	 *     ]
	 * }
	 */
	app.get('/users/:userid/friends', this.user.getFriends);
	
	/**
	 * @api {get} /users/:userid/friends/:friendid Get friend
	 * @apiVersion 1.0.0
	 * @apiName GetFriend
	 * @apiGroup Friends
	 * 
	 * @apiParam (Params) {String} userid The user's id.
	 * @apiParam (Params) {String} friendid The user's friend's id.
	 * 
	 * @apiParam (Queries) {String} apiKey The user's apiKey.
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "id": "USER-8ed6e858-ae0f-4e40-b2e1-eea77107d693",
	 *     "avatar": "avatars/default-avatars/avatar-001.png",
	 *     "isPremium": true,
	 *     "email": "b@b.com",
	 *     "displayName": "",
	 *     "firstName": null,
	 *     "lastName": null,
	 *     "musicalPreferences": null,
	 *     "friends": [
	 *         "USER-a8c4b5cb-810c-4c6c-a834-ccf40732095f"
	 *     ],
	 *     "playlists": [
	 *         "PLAY-3580bfbd-d415-4c6b-a1be-74fb7e342d95"
	 *     ],
	 *     "guestInPlaylists": [],
	 *     "editorInPlaylists": [],
	 *     "events": [
	 *         "EVEN-86cac9bf-bb5c-4757-85f1-822c7abb6e7d"
	 *     ],
	 *     "guestInEvents": [],
	 *     "djInEvents": []
	 * }
	 */
	app.get('/users/:userid/friends/:friendid', this.user.getFriends);

	/* ========== LOGIN ========= */

	/**
	 * @api {post} /login Login 
	 * @apiVersion 1.0.0
	 * @apiName Login
	 * @apiGroup Users
	 * 
	 * @apiParam (Body) {String} email The user's email.
	 * @apiParam (Body) {String} password The user's password.
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "apiKey": "_KEY-6279b709-0abb-420c-be96-8efdd1f9efea",
	 *     "id": "USER-8ed6e858-ae0f-4e40-b2e1-eea77107d693"
	 * }
	 */
	app.post('/login', this.user.loginWithEmail);

	/* ======== FACEBOOK ======== */

	/**
	 * @api {post/put} /users/facebook Facebook 
	 * @apiVersion 1.0.0
	 * @apiName Facebook
	 * @apiGroup Users
	 * 
	 * @apiParam (Query) {String} accessToken The user's facebook accessToken.
	 * @apiParam (Query) {String} [apiKey] The user's apiKey (required for link) 
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK - Create with Facebook
	 * {
	 *     "id": "USER-0cd01746-cf63-4734-bc20-5ac685b4d43f",
	 *     "isPremium": true,
	 *     "apiKey": "_KEY-287ac438-75b0-479b-bf4d-25866f37e884",
	 *     "friendships": [],
	 *     "email": "email@ymail.com",
	 *     "firstName": "Janny",
	 *     "lastName": "Walshy",
	 *     "friends": [],
	 *     "playlists": [],
	 *     "guestInPlaylists": [],
	 *     "editorInPlaylists": [],
	 *     "events": [],
	 *     "guestInEvents": [],
	 *     "djInEvents": [],
	 *     "message": "User created"
	 * }
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK - Login with Facebook
	 * {
	 *     "apiKey": "_KEY-6279b709-0abb-420c-be96-8efdd1f9efea",
	 *     "id": "USER-8ed6e858-ae0f-4e40-b2e1-eea77107d693"
	 * }
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK - Link Account with Facebook 
	 * {
	 *     "apiKey": "_KEY-4703fae1-5cb9-48e9-ae79-1d1b3004efe1",
     *     "id": "USER-759be386-922c-4af3-8b9c-2b989b5b8518",
	 *     "message": "Facebook and Music Room accounts linked",
	 *     "facebookid": 01234060871354
	 * }
	 */
	app.post('/users/facebook', this.user.facebook);
	app.put('/users/facebook', this.user.facebook); 
	}

	/* ======= FRIENDSHIPS ======= */
	{
	/**
	 * @api {put} /friendships/:friendshipid/:friendshipaction Update friendship 
	 * @apiVersion 1.0.0
	 * @apiName UpdateFriendship
	 * @apiGroup Friendships
	 * 
	 * @apiParam (Queries) {String} apiKey The user's apiKey.
	 * 
	 * @apiParam (Params) {String} friendshipid The friendship's id.
	 * @apiParam (Params) {String="read","accept","reject"} friendshipaction The action to perform.
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "id": "FRIE-048e4bb2-4fb7-4b8a-b75e-e03b0c9c31ba",
	 *     "pending": false,
	 *     "request": null,
	 *     "acceptedOn": "2019-04-30T19:09:55.220Z",
	 *     "users": [
	 *       "USER-8ed6e858-ae0f-4e40-b2e1-eea77107d693",
	 *       "USER-8bd822eb-6bfa-4d99-b995-911e84acc848"
	 *     ],
	 *     "message": "Friendship updated"
	 * }
	 */
	app.put('/friendships/:friendshipid/:friendshipaction', this.friendship.update);
	
	/**
	 * @api {post} /friendships Create friendship 
	 * @apiVersion 1.0.0
	 * @apiName CreateFriendship
	 * @apiGroup Friendships
	 * 
	 * @apiParam (Queries) {String} apiKey The user's apiKey.
	 * 
	 * @apiParam (Body) {String} id The other user's id.
	 * @apiParam (Body) {String} message A message to send other user.
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "id": "FRIE-d202583d-9bf6-4ab5-8b4c-271014fbc388",
	 *     "pending": true,
	 *     "request": {
	 *         "from": "USER-8ed6e858-ae0f-4e40-b2e1-eea77107d693",
	 *         "to": "USER-baaa8163-367c-432f-a7fd-37c82dccef9c",
	 *         "message": "Please",
	 *         "createdOn": "2019-04-30T18:34:24.381Z",
	 *         "read": false
	 *     },
	 *     "users": [
	 *         "USER-8ed6e858-ae0f-4e40-b2e1-eea77107d693",
	 *         "USER-baaa8163-367c-432f-a7fd-37c82dccef9c"
	 *     ],
	 *     "message": "Friendship created"
	 * }
	 */
	app.post('/friendships', this.friendship.create);

	/**
	 * @api {delete} /friendships/:friendshipid Delete friendship 
	 * @apiVersion 1.0.0
	 * @apiName DeleteFriendship
	 * @apiGroup Friendships
	 * 
	 * @apiParam (Queries) {String} apiKey The user's apiKey.
	 * 
	 * @apiParam (Params) {String} friendshipid The friendship's id.
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "message": "Friendship deleted"
	 * }
	 */
	app.delete('/friendships/:friendshipid', this.friendship.delete);

	/**
	 * @api {get} /friendships/:friendshipid Get friendship 
	 * @apiVersion 1.0.0
	 * @apiName GetFriendship
	 * @apiGroup Friendships
	 * 
	 * @apiParam (Queries) {String} apiKey The user's apiKey.
	 * 
	 * @apiParam (Params) {String} friendshipid The friendship's id.
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "id": "FRIE-048e4bb2-4fb7-4b8a-b75e-e03b0c9c31ba",
	 *     "pending": false,
	 *     "request": null,
	 *     "acceptedOn": "2019-04-30T19:09:55.220Z",
	 *     "users": [
	 *         {
	 *             "id": "USER-8ed6e858-ae0f-4e40-b2e1-eea77107d693",
	 *             "avatar": "avatars/default-avatars/avatar-001.png",
	 *             "isPremium": true,
	 *             "email": "a@a.com",
	 *             "displayName": "",
	 *             "firstName": null,
	 *             "lastName": null,
	 *             "musicalPreferences": null,
	 *             "friends": [
	 *                 "USER-a8c4b5cb-810c-4c6c-a834-ccf40732095f",
	 *                 "USER-8bd822eb-6bfa-4d99-b995-911e84acc848"
	 *             ],
	 *             "playlists": [
	 *                 "PLAY-3580bfbd-d415-4c6b-a1be-74fb7e342d95"
	 *             ],
	 *             "guestInPlaylists": [],
	 *             "editorInPlaylists": [],
	 *             "events": [
	 *                 "EVEN-86cac9bf-bb5c-4757-85f1-822c7abb6e7d"
	 *             ],
	 *             "guestInEvents": [],
	 *             "djInEvents": []
	 *         },
	 *         {
	 *             "id": "USER-8bd822eb-6bfa-4d99-b995-911e84acc848",
	 *             "avatar": "avatars/default-avatars/avatar-001.png",
	 *             "isPremium": true,
	 *             "theme": null,
	 *             "apiKey": "_KEY-e7ddf2f9-1fad-4237-9757-a8e426e2bf49",
	 *             "friendships": [
	 *                 "FRIE-048e4bb2-4fb7-4b8a-b75e-e03b0c9c31ba"
	 *             ],
	 *             "email": "b@b.com",
	 *             "displayName": null,
	 *             "firstName": null,
	 *             "lastName": null,
	 *             "friends": [
	 *                 "USER-8ed6e858-ae0f-4e40-b2e1-eea77107d693"
	 *             ],
	 *             "playlists": [],
	 *             "guestInPlaylists": [],
	 *             "editorInPlaylists": [],
	 *             "events": [],
	 *             "guestInEvents": [],
	 *             "djInEvents": []
	 *         }
	 *     ]
	 * }
	 */
	app.get('/friendships/:friendshipid', this.friendship.get);

	/**
	 * @api {get} /friendships Get friendships
	 * @apiVersion 1.0.0
	 * @apiName GetFriendships
	 * @apiGroup Friendships
	 * 
	 * @apiParam (Queries) {String} apiKey The user's apiKey.
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "friendships": [
	 *         {
	 *             "id": "FRIE-048e4bb2-4fb7-4b8a-b75e-e03b0c9c31ba",
	 *             "pending": "false",
	 *             ...
	 *         },
	 *         ...
	 *     ]
	 * }
	 */
	app.get('/friendships', this.friendship.get);
	}

	/* ========= PLAYLISTS ======= */
	{
	/**
	 * @api {post} /playlists Create playlist
	 * @apiVersion 1.0.0
	 * @apiName CreatePlaylist
	 * @apiGroup Playlists
	 * 
	 * @apiParam (Queries) {String} apiKey The user's apiKey.
	 * 
	 * @apiParam (Body) {String} name Name of the playlist. 
	 * @apiParam (Body) {String=["PUBLIC","PRIVATE"]} [visibility] Visibility of the playlist. 
	 * @apiParam (Body) {String=["PUBLIC","PRIVATE"]} [editing] Editing rights for the playlist. 
	 * @apiParam (Body) {String=["vote","move"]} [sortingType] Sorting Type of the playlist.
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "tracks": [],
	 *     "guests": [],
	 *     "editors": [],
	 *     "id": "PLAY-99577091-194f-4be2-8071-f21e4d911084",
	 *     "name": "Playlist-1",
	 *     "owner": "USER-8bd822eb-6bfa-4d99-b995-911e84acc848",
	 *     "events": [],
	 *     "visibility": "PUBLIC",
	 *     "editing": "PUBLIC",
	 *     "sortingType": "vote",
	 *     "message": "Playlist created"
	 * }
	 */
	app.post('/playlists', this.playlist.create);

	/**
	 * @api {get} /playlists/search Search playlists
	 * @apiName SearchPlaylists
	 * @apiGroup Playlists
	 * @apiVersion 1.0.0
	 * 
	 * @apiParam (Queries) {String} apiKey The user's api key.
	 * @apiParam (Queries) {String} keyword The keyword[s] to search with.
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "results":
	 *     [
	 *         {
	 *             "tracks": [],
	 *             "guests": [],
	 *             "editors": [],
	 *             "id": "PLAY-99577091-194f-4be2-8071-f21e4d911084",
	 *             "name": "Playlist-1",
	 *             "owner": "USER-8bd822eb-6bfa-4d99-b995-911e84acc848",
	 *             "events": [],
	 *             "visibility": "PUBLIC",
	 *             "editing": "PUBLIC",
	 *             "sortingType": "vote"
	 *         },
	 *         ...
	 *     ]
	 * }
	 */
	app.get('/playlists/search', playlistController.search);

	/**
	 * @api {get} /playlists/:playlistid Get playlist
	 * @apiVersion 1.0.0
	 * @apiName GetPlaylist
	 * @apiGroup Playlists
	 * 
	 * @apiParam (Queries) {String} apiKey The user's apiKey.
	 * 
	 * @apiParam (Param) {String} playlistid The playlist's id.
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "tracks": [
	 *         {
	 *             "id": "TRAC-60175658-42de-4bbb-aafa-193172060ee4",
	 *             "serviceid": 392284592,
	 *             "service": null
	 *         },
	 *         {
	 *             "id": "TRAC-27beab4b-4cf4-4e13-be6a-5349f56b7b2f",
	 *             "serviceid": 559934912,
	 *             "service": null
	 *         }
	 *     ],
	 *     "guests": [],
	 *     "editors": [],
	 *     "id": "PLAY-e6022cff-e5b0-4bef-9b1c-4e0f4f88ba73",
	 *     "name": "Playlist-1",
	 *     "owner": {
	 *         "id": "USER-8bd822eb-6bfa-4d99-b995-911e84acc848",
	 *         "avatar": "avatars/default-avatars/avatar-004.png",
	 *         "isPremium": true,
	 *         "theme": null,
	 *         "apiKey": "_KEY-e7ddf2f9-1fad-4237-9757-a8e426e2bf49",
	 *         "friendships": [
	 *             "FRIE-048e4bb2-4fb7-4b8a-b75e-e03b0c9c31ba"
	 *         ],
	 *         "email": "a@a.com",
	 *         "displayName": null,
	 *         "firstName": null,
	 *         "lastName": null,
	 *         "friends": [],
	 *         "playlists": [
	 *             "PLAY-e6022cff-e5b0-4bef-9b1c-4e0f4f88ba73"
	 *         ],
	 *         "guestInPlaylists": [],
	 *         "editorInPlaylists": [],
	 *         "events": [],
	 *         "guestInEvents": [],
	 *         "djInEvents": []
	 *     },
	 *     "events": [],
	 *     "visibility": "PUBLIC",
	 *     "editing": "PUBLIC",
	 *     "sortingType": "vote"
	 * }
	 */
	app.get('/playlists/:playlistid', this.playlist.get);

	/**
	 * @api {get} /playlists Get playlists
	 * @apiVersion 1.0.0
	 * @apiName GetPlaylists
	 * @apiGroup Playlists
	 * 
	 * @apiParam (Queries) {String} apiKey The user's apiKey.
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "owner":
	 *     [
	 *         {
	 *             "tracks": [
	 *                 {
	 *                     "id": "TRAC-60175658-42de-4bbb-aafa-193172060ee4",
	 *                     "serviceid": 392284592,
	 *                     "service": null
	 *                 },
	 *                 {
	 *                      "id": "TRAC-27beab4b-4cf4-4e13-be6a-5349f56b7b2f",
	 *                      "serviceid": 559934912,
	 *                      "service": null
	 *                 }
	 *             ],
	 *             "guests": [],
	 *             "editors": [],
	 *             "id": "PLAY-e6022cff-e5b0-4bef-9b1c-4e0f4f88ba73",
	 *             "name": "Test 2",
	 *             "owner": "USER-8bd822eb-6bfa-4d99-b995-911e84acc848",
	 *             "events": [],
	 *             "visibility": "PUBLIC",
	 *             "editing": "PUBLIC",
	 *             "sortingType": "vote"
	 *         }
	 *     ],
	 *     "editor": [
	 *         ...
	 *     ],
	 *     "guest": [
	 *         ...
	 *     ]
	 * }
	 */
	app.get('/playlists', this.playlist.get);

	/**
	 * @api {put} /playlists/:playlistid Update playlist
	 * @apiVersion 1.0.0
	 * @apiName UpdatePlaylist
	 * @apiGroup Playlists
	 * 
	 * @apiParam (Queries) {String} apiKey The user's apiKey.
	 * 
	 * @apiParam (Params) {String} playlistid The playlist id.
	 * 
	 * @apiParam (Body) {String} [name] Name of the playlist. 
	 * @apiParam (Body) {String=["PUBLIC","PRIVATE"]} [visibility] Visibility of the playlist. 
	 * @apiParam (Body) {String=["PUBLIC","PRIVATE"]} [editing] Editing rights for the playlist. 
	 * @apiParam (Body) {String=["vote","move"]} [sortingType] Sorting Type of the playlist.
	 * 
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "tracks":
	 *     [
	 *         {
	 *             "id": "TRAC-60175658-42de-4bbb-aafa-193172060ee4",
	 *             "serviceid": 392284592,
	 *             "service": null
	 *         },
	 *         {
	 *             "id": "TRAC-27beab4b-4cf4-4e13-be6a-5349f56b7b2f",
	 *             "serviceid": 559934912,
	 *             "service": null
	 *         }
	 *     ],
	 *     "guests": [],
	 *     "editors": [],
	 *     "id": "PLAY-e6022cff-e5b0-4bef-9b1c-4e0f4f88ba73",
	 *     "name": "Playlist-1",
	 *     "owner": "USER-8bd822eb-6bfa-4d99-b995-911e84acc848",
	 *     "events": [],
	 *     "visibility": "PUBLIC",
	 *     "editing": "PUBLIC",
	 *     "sortingType": "vote",
	 *     "message": "Playlist updated"
	 * }
	 */
	app.put('/playlists/:playlistid', this.playlist.update);

	/**
	 * @api {put} /playlists/:playlistid/tracks/:trackid/move Move track
	 * @apiVersion 1.0.0
	 * @apiName MoveTrackInPlaylist
	 * @apiGroup Playlists
	 * 
	 * @apiParam (Queries) {String} apiKey The user's apiKey.
	 * 
	 * @apiParam (Params) {String} playlistid The playlist id.
	 * @apiParam (Params) {String} trackid The track's id to move.
	 * 
	 * @apiParam (Body) {String} index The track's new index in list.
	 * 
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "tracks":
	 *     [
	 *         {
	 *             "id": "TRAC-60175658-42de-4bbb-aafa-193172060ee4",
	 *             "serviceid": 392284592,
	 *             "service": null
	 *         },
	 *         {
	 *             "id": "TRAC-27beab4b-4cf4-4e13-be6a-5349f56b7b2f",
	 *             "serviceid": 559934912,
	 *             "service": null
	 *         }
	 *     ],
	 *     "guests": [],
	 *     "editors": [],
	 *     "id": "PLAY-e6022cff-e5b0-4bef-9b1c-4e0f4f88ba73",
	 *     "name": "Playlist-1",
	 *     "owner": "USER-8bd822eb-6bfa-4d99-b995-911e84acc848",
	 *     "events": [],
	 *     "visibility": "PUBLIC",
	 *     "editing": "PUBLIC",
	 *     "sortingType": "vote",
	 *     "message": "Track moved"
	 * }
	 */
	app.put('/playlists/:playlistid/tracks/:trackid/move', this.playlist.moveTrack);

	/**
	 * @api {put} /playlists/:playlistid/tracks Add track
	 * @apiVersion 1.0.0
	 * @apiName AddTrackToPlaylist
	 * @apiGroup Playlists
	 * 
	 * @apiParam (Queries) {String} apiKey The user's apiKey.
	 * 
	 * @apiParam (Params) {String} playlistid The playlist id.
	 * 
	 * @apiParam (Body) {String} serviceid The track's service id (eg, deezerid)
	 * @apiParam (Body) {String} [service] The service used for this id (eg, deezer)
	 * 
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "id": "TRAC-5de0991d-1e10-466c-980e-2126980cc94e",
	 *     "serviceid": 511840342,
	 *     "message": "Track added"
	 * }
	 */
	app.put('/playlists/:playlistid/tracks', this.playlist.addTrack);

	/**
	 * @api {delete} /playlists/:playlistid/tracks/:trackid Deletes track
	 * @apiVersion 1.0.0
	 * @apiName RemoveTrackFromPlaylist
	 * @apiGroup Playlists
	 * 
	 * @apiParam (Queries) {String} apiKey The user's apiKey.
	 * 
	 * @apiParam (Params) {String} playlistid The playlist id.
	 * @apiParam (Params) {String} trackid The id of the track to delete.
	 * 
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "tracks":
	 *     [
	 *         {
	 *             "id": "TRAC-60175658-42de-4bbb-aafa-193172060ee4",
	 *             "serviceid": 392284592,
	 *             "service": null
	 *         },
	 *         {
	 *             "id": "TRAC-27beab4b-4cf4-4e13-be6a-5349f56b7b2f",
	 *             "serviceid": 559934912,
	 *             "service": null
	 *         }
	 *     ],
	 *     "guests": [],
	 *     "editors": [],
	 *     "id": "PLAY-e6022cff-e5b0-4bef-9b1c-4e0f4f88ba73",
	 *     "name": "Playlist-1",
	 *     "owner": "USER-8bd822eb-6bfa-4d99-b995-911e84acc848",
	 *     "events": [],
	 *     "visibility": "PUBLIC",
	 *     "editing": "PUBLIC",
	 *     "sortingType": "vote",
	 *     "message": "Track removed"
	 * }
	 */
	app.delete('/playlists/:playlistid/tracks/:trackid', this.playlist.removeTrack);

	/**
	 * @api {put} /playlists/:playlistid/editors/:userid Add editor
	 * @apiVersion 1.0.0
	 * @apiName AddEditorToPlaylist
	 * @apiGroup Playlists
	 * 
	 * @apiParam (Queries) {String} apiKey The user's apiKey.
	 * 
	 * @apiParam (Params) {String} playlistid The playlist id.
	 * @apiParam (Params) {String} userid The editor's user id to add.
	 * 
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "tracks":
	 *     [
	 *         {
	 *             "id": "TRAC-27beab4b-4cf4-4e13-be6a-5349f56b7b2f",
	 *             "serviceid": 559934912,
	 *             "service": null
	 *         },
	 *         {
	 *             "id": "TRAC-60175658-42de-4bbb-aafa-193172060ee4",
	 *             "serviceid": 392284592,
	 *             "service": null
	 *         }
	 *     ],
	 *     "guests": [
	 *         "USER-8ed6e858-ae0f-4e40-b2e1-eea77107d693"
	 *     ],
	 *     "editors": [
	 *         "USER-8ed6e858-ae0f-4e40-b2e1-eea77107d693"
	 *     ],
	 *     "id": "PLAY-e6022cff-e5b0-4bef-9b1c-4e0f4f88ba73",
	 *     "name": "Playlist-1",
	 *     "owner": "USER-8bd822eb-6bfa-4d99-b995-911e84acc848",
	 *     "events": [],
	 *     "visibility": "PUBLIC",
	 *     "editing": "PUBLIC",
	 *     "sortingType": "vote",
	 *     "message": "editor added"
	 * }
	 */
	app.put('/playlists/:playlistid/editors/:userid', this.playlist.addEditor);

	/**
	 * @api {put} /playlists/:playlistid/editors Add editors
	 * @apiVersion 1.0.0
	 * @apiName AddEditorsToPlaylist
	 * @apiGroup Playlists
	 * 
	 * @apiParam (Queries) {String} apiKey The user's apiKey.
	 * 
	 * @apiParam (Params) {String} playlistid The playlist id.
	 *
	 * @apiParam (Body) {String[]} userids A list of users to add as editors.
	 * 
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "tracks":
	 *     [
	 *         {
	 *             "id": "TRAC-27beab4b-4cf4-4e13-be6a-5349f56b7b2f",
	 *             "serviceid": 559934912,
	 *             "service": null
	 *         },
	 *         {
	 *             "id": "TRAC-60175658-42de-4bbb-aafa-193172060ee4",
	 *             "serviceid": 392284592,
	 *             "service": null
	 *         }
	 *     ],
	 *     "guests": [
	 *         "USER-8ed6e858-ae0f-4e40-b2e1-eea77107d693"
	 *     ],
	 *     "editors": [
	 *         "USER-8ed6e858-ae0f-4e40-b2e1-eea77107d693"
	 *         "USER-rezcv498-f5zc-4e40-b2e1-f7dfv7eruykb"
	 *     ],
	 *     "id": "PLAY-e6022cff-e5b0-4bef-9b1c-4e0f4f88ba73",
	 *     "name": "Playlist-1",
	 *     "owner": "USER-8bd822eb-6bfa-4d99-b995-911e84acc848",
	 *     "events": [],
	 *     "visibility": "PUBLIC",
	 *     "editing": "PUBLIC",
	 *     "sortingType": "vote",
	 *     "message": "editors added"
	 * }
	 */
	app.put('/playlists/:playlistid/editors', this.playlist.addEditor);

	/**
	 * @api {delete} /playlists/:playlistid/editors/:userid Remove editor
	 * @apiVersion 1.0.0
	 * @apiName RemoveEditorFromPlaylist
	 * @apiGroup Playlists
	 * 
	 * @apiParam (Queries) {String} apiKey The user's apiKey.
	 * 
	 * @apiParam (Params) {String} playlistid The playlist id.
	 * @apiParam (Params) {String} userid The editor's user id to remove.
	 * 
	 * 
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "tracks":
	 *     [
	 *         {
	 *             "id": "TRAC-27beab4b-4cf4-4e13-be6a-5349f56b7b2f",
	 *             "serviceid": 559934912,
	 *             "service": null
	 *         },
	 *         {
	 *             "id": "TRAC-60175658-42de-4bbb-aafa-193172060ee4",
	 *             "serviceid": 392284592,
	 *             "service": null
	 *         }
	 *     ],
	 *     "guests": [
	 *         "USER-8ed6e858-ae0f-4e40-b2e1-eea77107d693"
	 *         "USER-rezcv498-f5zc-4e40-b2e1-f7dfv7eruykb"
	 *     ],
	 *     "editors": [
	 *         "USER-8ed6e858-ae0f-4e40-b2e1-eea77107d693"
	 *     ],
	 *     "id": "PLAY-e6022cff-e5b0-4bef-9b1c-4e0f4f88ba73",
	 *     "name": "Playlist-1",
	 *     "owner": "USER-8bd822eb-6bfa-4d99-b995-911e84acc848",
	 *     "events": [],
	 *     "visibility": "PUBLIC",
	 *     "editing": "PUBLIC",
	 *     "sortingType": "vote",
	 *     "message": "editor removed"
	 * }
	 */	
	app.delete('/playlists/:playlistid/editors/:userid', this.playlist.removeEditor);

	/**
	 * @api {delete} /playlists/:playlistid/editors Remove editors
	 * @apiVersion 1.0.0
	 * @apiName RemoveEditorsFromPlaylist
	 * @apiGroup Playlists
	 * 
	 * @apiParam (Queries) {String} apiKey The user's apiKey.
	 * 
	 * @apiParam (Params) {String} playlistid The playlist id.
	 * 
	 * @apiParam (Body) {String[]} userids A list of users to remove as editors.
	 * 
	 * 
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "tracks":
	 *     [
	 *         {
	 *             "id": "TRAC-27beab4b-4cf4-4e13-be6a-5349f56b7b2f",
	 *             "serviceid": 559934912,
	 *             "service": null
	 *         },
	 *         {
	 *             "id": "TRAC-60175658-42de-4bbb-aafa-193172060ee4",
	 *             "serviceid": 392284592,
	 *             "service": null
	 *         }
	 *     ],
	 *     "guests": [
	 *         "USER-8ed6e858-ae0f-4e40-b2e1-eea77107d693"
	 *         "USER-rezcv498-f5zc-4e40-b2e1-f7dfv7eruykb"
	 *     ],
	 *     "editors": [
	 *         "USER-8ed6e858-ae0f-4e40-b2e1-eea77107d693"
	 *     ],
	 *     "id": "PLAY-e6022cff-e5b0-4bef-9b1c-4e0f4f88ba73",
	 *     "name": "Playlist-1",
	 *     "owner": "USER-8bd822eb-6bfa-4d99-b995-911e84acc848",
	 *     "events": [],
	 *     "visibility": "PUBLIC",
	 *     "editing": "PUBLIC",
	 *     "sortingType": "vote",
	 *     "message": "editors removed"
	 * }
	 */	
	app.delete('/playlists/:playlistid/editors', this.playlist.removeEditor);

	/**
	 * @api {put} /playlists/:playlistid/guests/:userid Add guest
	 * @apiVersion 1.0.0
	 * @apiName AddGuestToPlaylist
	 * @apiGroup Playlists
	 * 
	 * @apiParam (Queries) {String} apiKey The user's apiKey.
	 * 
	 * @apiParam (Params) {String} playlistid The playlist id.
	 * @apiParam (Params) {String} userid The guest's user id to add.
	 * 
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "tracks":
	 *     [
	 *         {
	 *             "id": "TRAC-27beab4b-4cf4-4e13-be6a-5349f56b7b2f",
	 *             "serviceid": 559934912,
	 *             "service": null
	 *         },
	 *         {
	 *             "id": "TRAC-60175658-42de-4bbb-aafa-193172060ee4",
	 *             "serviceid": 392284592,
	 *             "service": null
	 *         }
	 *     ],
	 *     "guests": [
	 *         "USER-8ed6e858-ae0f-4e40-b2e1-eea77107d693"
	 *         "USER-rezcv498-f5zc-4e40-b2e1-f7dfv7eruykb"
	 *     ],
	 *     "editors": [
	 *         "USER-8ed6e858-ae0f-4e40-b2e1-eea77107d693"
	 *     ],
	 *     "id": "PLAY-e6022cff-e5b0-4bef-9b1c-4e0f4f88ba73",
	 *     "name": "Playlist-1",
	 *     "owner": "USER-8bd822eb-6bfa-4d99-b995-911e84acc848",
	 *     "events": [],
	 *     "visibility": "PUBLIC",
	 *     "editing": "PUBLIC",
	 *     "sortingType": "vote",
	 *     "message": "guest added"
	 * }
	 */
	app.put('/playlists/:playlistid/guests/:userid', this.playlist.addGuest);

	/**
	 * @api {put} /playlists/:playlistid/guests Add guests
	 * @apiVersion 1.0.0
	 * @apiName AddGuestsToPlaylist
	 * @apiGroup Playlists
	 * 
	 * @apiParam (Queries) {String} apiKey The user's apiKey.
	 * 
	 * @apiParam (Params) {String} playlistid The playlist id.
	 * 
	 * @apiParam (Body) {String[]} userids A list of users to add as guests.
	 * 
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "tracks":
	 *     [
	 *         {
	 *             "id": "TRAC-27beab4b-4cf4-4e13-be6a-5349f56b7b2f",
	 *             "serviceid": 559934912,
	 *             "service": null
	 *         },
	 *         {
	 *             "id": "TRAC-60175658-42de-4bbb-aafa-193172060ee4",
	 *             "serviceid": 392284592,
	 *             "service": null
	 *         }
	 *     ],
	 *     "guests": [
	 *         "USER-8ed6e858-ae0f-4e40-b2e1-eea77107d693"
	 *         "USER-rezcv498-f5zc-4e40-b2e1-f7dfv7eruykb"
	 *     ],
	 *     "editors": [
	 *         "USER-8ed6e858-ae0f-4e40-b2e1-eea77107d693"
	 *     ],
	 *     "id": "PLAY-e6022cff-e5b0-4bef-9b1c-4e0f4f88ba73",
	 *     "name": "Playlist-1",
	 *     "owner": "USER-8bd822eb-6bfa-4d99-b995-911e84acc848",
	 *     "events": [],
	 *     "visibility": "PUBLIC",
	 *     "editing": "PUBLIC",
	 *     "sortingType": "vote",
	 *     "message": "guests added"
	 * }
	 */
	app.put('/playlists/:playlistid/guests', this.playlist.addGuest);

	/**
	 * @api {delete} /playlists/:playlistid/guests/:userid Remove guest
	 * @apiVersion 1.0.0
	 * @apiName RemoveGuestFromPlaylist
	 * @apiGroup Playlists
	 * 
	 * @apiParam (Queries) {String} apiKey The user's apiKey.
	 * 
	 * @apiParam (Params) {String} playlistid The playlist id.
	 * @apiParam (Params) {String} userid The guest's user id to remove.
	 * 
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "tracks":
	 *     [
	 *         {
	 *             "id": "TRAC-27beab4b-4cf4-4e13-be6a-5349f56b7b2f",
	 *             "serviceid": 559934912,
	 *             "service": null
	 *         },
	 *         {
	 *             "id": "TRAC-60175658-42de-4bbb-aafa-193172060ee4",
	 *             "serviceid": 392284592,
	 *             "service": null
	 *         }
	 *     ],
	 *     "guests": [
	 *         "USER-8ed6e858-ae0f-4e40-b2e1-eea77107d693"
	 *         "USER-rezcv498-f5zc-4e40-b2e1-f7dfv7eruykb"
	 *     ],
	 *     "editors": [
	 *         "USER-8ed6e858-ae0f-4e40-b2e1-eea77107d693"
	 *     ],
	 *     "id": "PLAY-e6022cff-e5b0-4bef-9b1c-4e0f4f88ba73",
	 *     "name": "Playlist-1",
	 *     "owner": "USER-8bd822eb-6bfa-4d99-b995-911e84acc848",
	 *     "events": [],
	 *     "visibility": "PUBLIC",
	 *     "editing": "PUBLIC",
	 *     "sortingType": "vote",
	 *     "message": "guest removed"
	 * }
	 */	
	app.delete('/playlists/:playlistid/guests/:userid', this.playlist.removeGuest);

	/**
	 * @api {delete} /playlists/:playlistid/guests Remove guests
	 * @apiVersion 1.0.0
	 * @apiName RemoveGuestsFromPlaylist
	 * @apiGroup Playlists
	 * 
	 * @apiParam (Queries) {String} apiKey The user's apiKey.
	 * 
	 * @apiParam (Params) {String} playlistid The playlist id.
	 * 
	 * @apiParam (Body) {String[]} userids A list of users to remove as guests.
	 * 
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "tracks":
	 *     [
	 *         {
	 *             "id": "TRAC-27beab4b-4cf4-4e13-be6a-5349f56b7b2f",
	 *             "serviceid": 559934912,
	 *             "service": null
	 *         },
	 *         {
	 *             "id": "TRAC-60175658-42de-4bbb-aafa-193172060ee4",
	 *             "serviceid": 392284592,
	 *             "service": null
	 *         }
	 *     ],
	 *     "guests": [
	 *         "USER-8ed6e858-ae0f-4e40-b2e1-eea77107d693"
	 *         "USER-rezcv498-f5zc-4e40-b2e1-f7dfv7eruykb"
	 *     ],
	 *     "editors": [
	 *         "USER-8ed6e858-ae0f-4e40-b2e1-eea77107d693"
	 *     ],
	 *     "id": "PLAY-e6022cff-e5b0-4bef-9b1c-4e0f4f88ba73",
	 *     "name": "Playlist-1",
	 *     "owner": "USER-8bd822eb-6bfa-4d99-b995-911e84acc848",
	 *     "events": [],
	 *     "visibility": "PUBLIC",
	 *     "editing": "PUBLIC",
	 *     "sortingType": "vote",
	 *     "message": "guests removed"
	 * }
	 */	
	app.delete('/playlists/:playlistid/guests', this.playlist.removeGuest);

	/**
	 * @api {post} /playlists/:playlistid Duplicate Playlist
	 * @apiVersion 1.0.0
	 * @apiName DuplicatePlaylist
	 * @apiGroup Playlists
	 * 
	 * @apiParam (Queries) {String} apiKey The user's apiKey.
	 * 
	 * @apiParam (Body) {Boolean} [copyEditors] Copies editors if you're owner of playlist
	 * @apiParam (Body) {Boolean} [copyGuests] Copies guests if you're owner of playlist
	 * 
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "tracks": [
	 *         {
	 *             "id": "TRAC-27beab4b-4cf4-4e13-be6a-5349f56b7b2f",
	 *             "serviceid": 559934912,
	 *             "service": null
	 *         },
	 *         {
	 *             "id": "TRAC-60175658-42de-4bbb-aafa-193172060ee4",
	 *             "serviceid": 392284592,
	 *             "service": null
	 *         }
	 *     ],
	 *     "guests": [],
	 *     "editors": [],
	 *     "id": "PLAY-7df81f74-5b54-43fd-819b-02e77e57001a",
	 *     "name": "Playlist-1",
	 *     "owner": "USER-8bd822eb-6bfa-4d99-b995-911e84acc848",
	 *     "events": [],
	 *     "visibility": "PUBLIC",
	 *     "editing": "PUBLIC",
	 *     "sortingType": "vote",
	 *     "message": "Playlist duplicated"
	 * }
	 */	
	app.post('/playlists/:playlistid', this.playlist.duplicate);

	/**
	 * @api {delete} /playlists/:playlistid Delete playlist
	 * @apiVersion 1.0.0
	 * @apiName DeletePlaylist
	 * @apiGroup Playlists
	 * 
	 * @apiParam (Queries) {String} apiKey The user's apiKey.
	 * 
	 * @apiParam (Params) {String} playlistid The playlist's id to delete.
	 * 
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "message": "Playlist deleted"
	 * }
	 */
	app.delete('/playlists/:playlistid', this.playlist.delete);

	/**
	 * @api {delete} /playlists Delete playlists
	 * @apiVersion 1.0.0
	 * @apiName DeletePlaylists
	 * @apiGroup Playlists
	 * 
	 * @apiParam (Queries) {String} apiKey The user's apiKey.
	 * 
	 * @apiParam (Body) {String[]} playlistids The list of playlist ids to delete.
	 * 
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "message": "Playlist deleted"
	 * } 
	 */
	app.delete('/playlists', this.playlist.delete);
	}

	/* ========== EVENTS ========= */
	{
	/**
	 * @api {post} /events Create event
	 * @apiVersion 1.0.0
	 * @apiName CreateEvent
	 * @apiGroup Events
	 * 
	 * @apiParam (Queries) {String} apiKey The user's apiKey.
	 * 
	 * @apiParam (Body) {String} name Name of the event.
	 * @apiParam (Body) {String} [description] A description for the event.
	 * @apiParam (Body) {String=["PUBLIC","PRIVATE"]} [visibility] Visibility of event. 
	 * @apiParam (Body) {Object} [votingSchedule] Voting schedule of event.
	 * @apiParam (Body) {Object} [eventSchedule] Event schedule.
	 * @apiParam (Body) {Object} [coordinates] Coordinates of event lcoation.
	 * @apiParamExample {json} Schedule Example:
	 *	{
	 *		"votingSchedule":
	 *		{
	 *			start: "2019-03-20T19:00:00.000Z",
	 *			end: "2019-03-20T23:30:00.000Z"
	 * 		}
	 *	}
	 * @apiParamExample {json} Coordinates Example:
	 *	{
	 *		"coordinates":
	 *		{
	 *			latitude: 48.8606,
	 *			longitude: 2.3376
	 * 		}
	 *	}
	 *
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "guests": [],
	 *     "djs": [],
	 *     "playlists": [],
	 *     "eventPlaylists": [],
	 *     "activePlaylist": null,
	 *     "votingActive": false,
	 *     "eventActive": false,
	 *     "trackPosition": 0,
	 *     "lastTrackActionTimestamp": null,
	 *     "id": "EVEN-9204d3c7-e7d3-459b-af3c-fb4ada2767c5",
	 *     "name": "Event-1",
	 *     "owner": {
	 *         "id": "USER-8ed6e858-ae0f-4e40-b2e1-eea77107d693",
	 *         "avatar": "avatars/default-avatars/avatar-001.png",
	 *         "isPremium": true,
	 *         "theme": null,
	 *         "apiKey": "_KEY-6279b709-0abb-420c-be96-8efdd1f9efea",
	 *         "friendships": [],
	 *         "email": "a@a.com",
	 *         "displayName": "",
	 *         "firstName": null,
	 *         "lastName": null,
	 *         "musicalPreferences": null,
	 *         "friends": [],
	 *         "playlists": [
	 *             "PLAY-3580bfbd-d415-4c6b-a1be-74fb7e342d95"
	 *         ],
	 *         "guestInPlaylists": [],
	 *         "editorInPlaylists": [],
	 *         "events": [
	 *             "EVEN-86cac9bf-bb5c-4757-85f1-822c7abb6e7d",
	 *             "EVEN-b89496c4-e786-402b-b4b7-a177e83a82d0",
	 *             "EVEN-9204d3c7-e7d3-459b-af3c-fb4ada2767c5"
	 *         ],
	 *         "guestInEvents": [],
	 *         "djInEvents": []
	 *     },
	 *     "visibility": "PUBLIC",
	 *     "votingSchedule": null,
	 *     "eventSchedule": {
	 *         "start": "2019-05-01T17:30:00.000Z",
	 *         "end": "2019-05-02T02:00:00.000Z"
	 *     },
	 *     "coordinates": null,
	 *     "message": "Event created"
	 * }
	 */
	app.post('/events', this.event.create);

	/**
	 * @api {get} /events/search Search events
	 * @apiName SearchEvents
	 * @apiGroup Events
	 * @apiVersion 1.0.0
	 * 
	 * @apiParam (Queries) {String} apiKey The user's api key.
	 * @apiParam (Queries) {String} keyword The keyword[s] to search with.
	 * 
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "results": [
	 *         {
	 *             "guests": [],
	 *             "djs": [],
	 *             "playlists": [],
	 *             "eventPlaylists": [],
	 *             "activePlaylist": null,
	 *             "votingActive": false,
	 *             "eventActive": false,
	 *             "trackPosition": 0,
	 *             "lastTrackActionTimestamp": null,
	 *             "id": "EVEN-b89496c4-e786-402b-b4b7-a177e83a82d0",
	 *             "name": "Event-1",
	 *             "description": null,
	 *             "owner": "USER-8ed6e858-ae0f-4e40-b2e1-eea77107d693",
	 *             "visibility": "PUBLIC",
	 *             "votingSchedule": {
	 *                 "start": "2019-05-01T07:35:54.680Z",
	 *                 "end": "2019-05-04T07:35:54.680Z"
	 *             },
	 *             "eventSchedule": {
	 *                 "start": "2019-05-06T17:30:00.000Z",
	 *                 "end": "2019-05-07T00:00:00.000Z"
	 *             },
	 *             "coordinates": null
	 *         },
	 *         {
	 *             "guests": [],
	 *             "djs": [],
	 *             "playlists": [],
	 *             "eventPlaylists": [],
	 *             "activePlaylist": null,
	 *             "votingActive": false,
	 *             "eventActive": false,
	 *             "trackPosition": 0,
	 *             "lastTrackActionTimestamp": null,
	 *             "id": "EVEN-9204d3c7-e7d3-459b-af3c-fb4ada2767c5",
	 *             "name": "Event-2",
	 *             "description": null,
	 *             "owner": "USER-8ed6e858-ae0f-4e40-b2e1-eea77107d693",
	 *             "visibility": "PUBLIC",
	 *             "votingSchedule": null,
	 *             "eventSchedule": {
	 *                 "start": "2019-05-01T17:30:00.000Z",
	 *                 "end": "2019-05-02T02:00:00.000Z"
	 *             },
	 *             "coordinates": null
	 *         }
	 *     ]
	 * }
	 */
	app.get('/events/search', this.event.search);

	/**
	 * @api {get} /events/:eventid Get event
	 * @apiVersion 1.0.0
	 * @apiName GetEvent
	 * @apiGroup Events
	 * 
	 * @apiParam (Queries) {String} apiKey The user's apiKey.
	 * 
	 * @apiParam (Param) {String} eventid The id of the event.
	 * 
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "guests": [],
	 *     "djs": [],
	 *     "playlists": [],
	 *     "eventPlaylists": [],
	 *     "activePlaylist": null,
	 *     "votingActive": false,
	 *     "eventActive": false,
	 *     "trackPosition": 0,
	 *     "lastTrackActionTimestamp": null,
	 *     "id": "EVEN-9204d3c7-e7d3-459b-af3c-fb4ada2767c5",
	 *     "name": "Event-1",
	 *     "description": null,
	 *     "owner": {
	 *         "id": "USER-8ed6e858-ae0f-4e40-b2e1-eea77107d693",
	 *         "avatar": "avatars/default-avatars/avatar-002.png",
	 *         "isPremium": true,
	 *         "theme": null,
	 *         "apiKey": "_KEY-6279b709-0abb-420c-be96-8efdd1f9efea",
	 *         "friendships": [
	 *             "FRIE-048e4bb2-4fb7-4b8a-b75e-e03b0c9c31ba",
	 *             "FRIE-3bcef577-519d-48e5-afbb-1e5b5a02182c",
	 *             "FRIE-76312415-3038-4c63-90f5-840f0e1c8c4a"
	 *         ],
	 *         "email": "a@a.com",
	 *         "displayName": "",
	 *         "firstName": null,
	 *         "lastName": null,
	 *         "musicalPreferences": null,
	 *         "friends": [
	 *             "USER-a8c4b5cb-810c-4c6c-a834-ccf40732095f",
	 *             "USER-8bd822eb-6bfa-4d99-b995-911e84acc848"
	 *         ],
	 *         "playlists": [
	 *             "PLAY-3580bfbd-d415-4c6b-a1be-74fb7e342d95"
	 *         ],
	 *         "guestInPlaylists": [],
	 *         "editorInPlaylists": [],
	 *         "events": [
	 *             "EVEN-86cac9bf-bb5c-4757-85f1-822c7abb6e7d",
	 *             "EVEN-b89496c4-e786-402b-b4b7-a177e83a82d0",
	 *             "EVEN-9204d3c7-e7d3-459b-af3c-fb4ada2767c5"
	 *         ],
	 *         "guestInEvents": [],
	 *         "djInEvents": []
	 *     },
	 *     "visibility": "PUBLIC",
	 *     "votingSchedule": null,
	 *     "eventSchedule": {
	 *         "start": "2019-05-01T17:30:00.000Z",
	 *         "end": "2019-05-02T02:00:00.000Z"
	 *     },
	 *     "coordinates": null
	 * }
	 */
	app.get('/events/:eventid', this.event.get);

	/**
	 * @api {get} /events Get events
	 * @apiVersion 1.0.0
	 * @apiName GetEvents
	 * @apiGroup Events
	 * 
	 * @apiParam (Queries) {String} apiKey The user's apiKey.
	 * 
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "owner": [
	 *         {
	 *             "guests": [],
	 *             "djs": [],
	 *             "playlists": [
	 *                 "PLAY-3580bfbd-d415-4c6b-a1be-74fb7e342d95"
	 *             ],
	 *             "eventPlaylists": [],
	 *             "activePlaylist": null,
	 *             "votingActive": false,
	 *             "eventActive": false,
	 *             "trackPosition": 0,
	 *             "lastTrackActionTimestamp": null,
	 *             "id": "EVEN-86cac9bf-bb5c-4757-85f1-822c7abb6e7d",
	 *             "name": "Test",
	 *             "description": null,
	 *             "owner": "USER-8ed6e858-ae0f-4e40-b2e1-eea77107d693",
	 *             "visibility": "PUBLIC",
	 *             "votingSchedule": {
	 *                 "start": "2019-04-30T13:34:00.000Z",
	 *                 "end": "2019-04-30T18:42:00.000Z"
	 *             },
	 *             "eventSchedule": null,
	 *             "coordinates": null
	 *         },
	 *         {
	 *             "guests": [],
	 *             "djs": [],
	 *             "playlists": [],
	 *             "eventPlaylists": [],
	 *             "activePlaylist": null,
	 *             "votingActive": false,
	 *             "eventActive": false,
	 *             "trackPosition": 0,
	 *             "lastTrackActionTimestamp": null,
	 *             "id": "EVEN-b89496c4-e786-402b-b4b7-a177e83a82d0",
	 *             "name": "Event-1",
	 *             "description": null,
	 *             "owner": "USER-8ed6e858-ae0f-4e40-b2e1-eea77107d693",
	 *             "visibility": "PUBLIC",
	 *             "votingSchedule": {
	 *                 "start": "2019-05-01T07:35:54.680Z",
	 *                 "end": "2019-05-04T07:35:54.680Z"
	 *             },
	 *             "eventSchedule": {
	 *                 "start": "2019-05-06T17:30:00.000Z",
	 *                 "end": "2019-05-07T00:00:00.000Z"
	 *             },
	 *             "coordinates": null
	 *         }
	 *     ],
	 *     "dj": [
	 *         {
	 *             "guests": [],
	 *             "djs": [],
	 *             "playlists": [],
	 *             "eventPlaylists": [],
	 *             "activePlaylist": null,
	 *             "votingActive": false,
	 *             "eventActive": false,
	 *             "trackPosition": 0,
	 *             "lastTrackActionTimestamp": null,
	 *             "id": "EVEN-9204d3c7-e7d3-459b-af3c-fb4ada2767c5",
	 *             "name": "Event-42",
	 *             "description": null,
	 *             "owner": "USER-8ed6e858-ae0f-4e40-b2e1-eea77107d693",
	 *             "visibility": "PUBLIC",
	 *             "votingSchedule": null,
	 *             "eventSchedule": {
	 *                 "start": "2019-05-01T17:30:00.000Z",
	 *                 "end": "2019-05-02T02:00:00.000Z"
	 *             },
	 *             "coordinates": null
	 *         }
	 *     ],
	 *     "guest": [
	 *         ...
	 *     ]
	 * }
	 */
	app.get('/events', this.event.get);

	/**
	 * @api {put} /events/:eventid Update event
	 * @apiVersion 1.0.0
	 * @apiName UpdateEvent
	 * @apiGroup Events
	 * 
	 * @apiParam (Queries) {String} apiKey The user's apiKey.
	 * 
	 * @apiParam (Param) {String} eventid Id of event.
	 * 
	 * @apiParam (Body) {String} [name] New name of event.
	 * @apiParam (Body) {String} [description] new description for the event.
	 * @apiParam (Body) {String=["PUBLIC","PRIVATE"]} [visibility] New visibility of event. 
	 * @apiParam (Body) {Schedule} [votingSchedule] New voting schedule of event.
	 * @apiParam (Body) {Schedule} [eventSchedule] New event schedule of event.
	 * @apiParam (Body) {Coordinates} [coordinates] New coordinates of event lcoation.
	 * @apiParamExample {json} Schedule Example:
	 *	{
	 *		"eventSchedule":
	 *		{
	 *			start: "2019-03-20T19:00:00.000Z",
	 *			end: "2019-03-20T23:30:00.000Z"
	 * 		}
	 *	}
	 * @apiParamExample {json} Coordinates Example:
	 *	{
	 *		"coordinates":
	 *		{
	 *			latitude: 48.8606,
	 *			longitude: 2.3376
	 * 		}
	 *	}
	 * 
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "guests": [],
	 *     "djs": [],
	 *     "playlists": [],
	 *     "eventPlaylists": [],
	 *     "activePlaylist": null,
	 *     "votingActive": true,
	 *     "eventActive": false,
	 *     "trackPosition": 0,
	 *     "lastTrackActionTimestamp": null,
	 *     "id": "EVEN-b89496c4-e786-402b-b4b7-a177e83a82d0",
	 *     "name": "Event-1",
	 *     "owner": {
	 *         "id": "USER-8ed6e858-ae0f-4e40-b2e1-eea77107d693",
	 *         "avatar": "avatars/default-avatars/avatar-002.png",
	 *         "isPremium": true,
	 *         "theme": null,
	 *         "apiKey": "_KEY-6279b709-0abb-420c-be96-8efdd1f9efea",
	 *         "friendships": [
	 *             "FRIE-048e4bb2-4fb7-4b8a-b75e-e03b0c9c31ba",
	 *             "FRIE-3bcef577-519d-48e5-afbb-1e5b5a02182c",
	 *             "FRIE-76312415-3038-4c63-90f5-840f0e1c8c4a"
	 *         ],
	 *         "email": "a@a.com",
	 *         "displayName": "",
	 *         "firstName": null,
	 *         "lastName": null,
	 *         "musicalPreferences": null,
	 *         "friends": [
	 *             "USER-a8c4b5cb-810c-4c6c-a834-ccf40732095f",
	 *             "USER-8bd822eb-6bfa-4d99-b995-911e84acc848"
	 *         ],
	 *         "playlists": [
	 *             "PLAY-3580bfbd-d415-4c6b-a1be-74fb7e342d95"
	 *         ],
	 *         "guestInPlaylists": [],
	 *         "editorInPlaylists": [],
	 *         "events": [
	 *             "EVEN-86cac9bf-bb5c-4757-85f1-822c7abb6e7d",
	 *             "EVEN-b89496c4-e786-402b-b4b7-a177e83a82d0",
	 *             "EVEN-9204d3c7-e7d3-459b-af3c-fb4ada2767c5"
	 *         ],
	 *         "guestInEvents": [],
	 *         "djInEvents": []
	 *     },
	 *     "visibility": "PUBLIC",
	 *     "eventSchedule": {
	 *         "start": "2019-05-01T17:30:00.000Z",
	 *         "end": "2019-05-02T02:00:00.000Z"
	 *     },
	 *     "message": "Event updated"
	 * }
	 */
	app.put('/events/:eventid', this.event.update);

	/**
	 * @api {put} /events/:eventid/guests/:userid Add guest
	 * @apiVersion 1.0.0
	 * @apiName AddGuestToEvent
	 * @apiGroup Events
	 * 
	 * @apiParam (Queries) {String} apiKey The user's apiKey.
	 * 
	 * @apiParam (Param) {String} eventid The id of the event.
	 * @apiParam (Param) {String} userid The id of the user to add as guest.
	 * 
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "guests": [
	 *         {
	 *             "id": "USER-8bd822eb-6bfa-4d99-b995-911e84acc848",
	 *             "avatar": "avatars/default-avatars/avatar-004.png",
	 *             "isPremium": true,
	 *             "email": "b@b.com",
	 *             "displayName": null,
	 *             "firstName": null,
	 *             "lastName": null,
	 *             "friends": [],
	 *             "playlists": [],
	 *             "events": [
	 *                 "EVEN-86c9d446-463b-4aee-927a-a1df932383da",
	 *                 "EVEN-d6e6ba6f-b36a-45d3-9244-e9e7a1154a81"
	 *             ]
	 *         }
	 *     ],
	 *     "djs": [],
	 *     "playlists": [],
	 *     "eventPlaylists": [],
	 *     "activePlaylist": null,
	 *     "votingActive": true,
	 *     "eventActive": false,
	 *     "trackPosition": 0,
	 *     "lastTrackActionTimestamp": null,
	 *     "id": "EVEN-b89496c4-e786-402b-b4b7-a177e83a82d0",
	 *     "name": "Event-1",
	 *     "description": null,
	 *     "owner": {
	 *         "id": "USER-8ed6e858-ae0f-4e40-b2e1-eea77107d693",
	 *         "avatar": "avatars/default-avatars/avatar-002.png",
	 *         "isPremium": true,
	 *         "theme": null,
	 *         "apiKey": "_KEY-6279b709-0abb-420c-be96-8efdd1f9efea",
	 *         "friendships": [
	 *             "FRIE-048e4bb2-4fb7-4b8a-b75e-e03b0c9c31ba",
	 *             "FRIE-3bcef577-519d-48e5-afbb-1e5b5a02182c",
	 *             "FRIE-76312415-3038-4c63-90f5-840f0e1c8c4a"
	 *         ],
	 *         "email": "a@a.com",
	 *         "displayName": "",
	 *         "firstName": null,
	 *         "lastName": null,
	 *         "musicalPreferences": null,
	 *         "friends": [
	 *             "USER-a8c4b5cb-810c-4c6c-a834-ccf40732095f",
	 *             "USER-8bd822eb-6bfa-4d99-b995-911e84acc848"
	 *         ],
	 *         "playlists": [
	 *             "PLAY-3580bfbd-d415-4c6b-a1be-74fb7e342d95"
	 *         ],
	 *         "guestInPlaylists": [],
	 *         "editorInPlaylists": [],
	 *         "events": [
	 *             "EVEN-86cac9bf-bb5c-4757-85f1-822c7abb6e7d",
	 *             "EVEN-b89496c4-e786-402b-b4b7-a177e83a82d0",
	 *             "EVEN-9204d3c7-e7d3-459b-af3c-fb4ada2767c5"
	 *         ],
	 *         "guestInEvents": [],
	 *         "djInEvents": []
	 *     },
	 *     "visibility": "PUBLIC",
	 *     "votingSchedule": null,
	 *     "eventSchedule": {
	 *         "start": "2019-05-01T17:30:00.000Z",
	 *         "end": "2019-05-02T02:00:00.000Z"
	 *     },
	 *     "coordinates": null,
	 *     "message": "guest added"
	 * }
	 */
	app.put('/events/:eventid/guests/:userid', this.event.addGuest);

	/**
	 * @api {put} /events/:eventid/guests Add guests
	 * @apiVersion 1.0.0
	 * @apiName AddGuestsToEvent
	 * @apiGroup Events
	 * 
	 * @apiParam (Queries) {String} apiKey The user's apiKey.
	 * 
	 * @apiParam (Param) {String} eventid The id of the event.
	 * 
	 * @apiParam (Body) {String[]} userids A list of ids of users to add as guests.
	 * 
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "guests": [
	 *         {
	 *             "id": "USER-8bd822eb-6bfa-4d99-b995-911e84acc848",
	 *             "avatar": "avatars/default-avatars/avatar-004.png",
	 *             "isPremium": true,
	 *             "email": "b@b.com",
	 *             "displayName": null,
	 *             "firstName": null,
	 *             "lastName": null,
	 *             "friends": [],
	 *             "playlists": [],
	 *             "events": []
	 *         },
	 *         {
	 *             "id": "USER-45645g6fd-6bfa-dfsf-trez-911e84acdsrs",
	 *             "avatar": "avatars/default-avatars/avatar-001.png",
	 *             "isPremium": true,
	 *             "email": "c@c.com",
	 *             "displayName": null,
	 *             "firstName": null,
	 *             "lastName": null,
	 *             "friends": [],
	 *             "playlists": [],
	 *             "events": []
	 *         },
	 *     ],
	 *     "djs": [],
	 *     "playlists": [],
	 *     "eventPlaylists": [],
	 *     "activePlaylist": null,
	 *     "votingActive": true,
	 *     "eventActive": false,
	 *     "trackPosition": 0,
	 *     "lastTrackActionTimestamp": null,
	 *     "id": "EVEN-b89496c4-e786-402b-b4b7-a177e83a82d0",
	 *     "name": "Event-1",
	 *     "description": null,
	 *     "owner": {
	 *         "id": "USER-8ed6e858-ae0f-4e40-b2e1-eea77107d693",
	 *         "avatar": "avatars/default-avatars/avatar-002.png",
	 *         "isPremium": true,
	 *         "theme": null,
	 *         "apiKey": "_KEY-6279b709-0abb-420c-be96-8efdd1f9efea",
	 *         "friendships": [
	 *             "FRIE-048e4bb2-4fb7-4b8a-b75e-e03b0c9c31ba",
	 *             "FRIE-3bcef577-519d-48e5-afbb-1e5b5a02182c",
	 *             "FRIE-76312415-3038-4c63-90f5-840f0e1c8c4a"
	 *         ],
	 *         "email": "a@a.com",
	 *         "displayName": "",
	 *         "firstName": null,
	 *         "lastName": null,
	 *         "musicalPreferences": null,
	 *         "friends": [
	 *             "USER-a8c4b5cb-810c-4c6c-a834-ccf40732095f",
	 *             "USER-8bd822eb-6bfa-4d99-b995-911e84acc848"
	 *         ],
	 *         "playlists": [
	 *             "PLAY-3580bfbd-d415-4c6b-a1be-74fb7e342d95"
	 *         ],
	 *         "guestInPlaylists": [],
	 *         "editorInPlaylists": [],
	 *         "events": [
	 *             "EVEN-86cac9bf-bb5c-4757-85f1-822c7abb6e7d",
	 *             "EVEN-b89496c4-e786-402b-b4b7-a177e83a82d0",
	 *             "EVEN-9204d3c7-e7d3-459b-af3c-fb4ada2767c5"
	 *         ],
	 *         "guestInEvents": [],
	 *         "djInEvents": []
	 *     },
	 *     "visibility": "PUBLIC",
	 *     "votingSchedule": null,
	 *     "eventSchedule": {
	 *         "start": "2019-05-01T17:30:00.000Z",
	 *         "end": "2019-05-02T02:00:00.000Z"
	 *     },
	 *     "coordinates": null,
	 *     "message": "guests added"
	 * }
	 */
	app.put('/events/:eventid/guests', this.event.addGuest);

	/**
	 * @api {delete} /events/:eventid/guests/:userid Remove guest
	 * @apiVersion 1.0.0
	 * @apiName RemoveGuestFromEvent
	 * @apiGroup Events
	 * 
	 * @apiParam (Queries) {String} apiKey The user's apiKey.
	 * 
	 * @apiParam (Param) {String} eventid The id of the event.
	 * @apiParam (Param) {String} userid The id of the user to remove as guest.
	 * 
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "guests": [
	 *         {
	 *             "id": "USER-8bd822eb-6bfa-4d99-b995-911e84acc848",
	 *             "avatar": "avatars/default-avatars/avatar-004.png",
	 *             "isPremium": true,
	 *             "email": "b@b.com",
	 *             "displayName": null,
	 *             "firstName": null,
	 *             "lastName": null,
	 *             "friends": [],
	 *             "playlists": [],
	 *             "events": []
	 *         }
	 *     ],
	 *     "djs": [],
	 *     "playlists": [],
	 *     "eventPlaylists": [],
	 *     "activePlaylist": null,
	 *     "votingActive": true,
	 *     "eventActive": false,
	 *     "trackPosition": 0,
	 *     "lastTrackActionTimestamp": null,
	 *     "id": "EVEN-b89496c4-e786-402b-b4b7-a177e83a82d0",
	 *     "name": "Event-1",
	 *     "description": null,
	 *     "owner": {
	 *         "id": "USER-8ed6e858-ae0f-4e40-b2e1-eea77107d693",
	 *         "avatar": "avatars/default-avatars/avatar-002.png",
	 *         "isPremium": true,
	 *         "theme": null,
	 *         "apiKey": "_KEY-6279b709-0abb-420c-be96-8efdd1f9efea",
	 *         "friendships": [
	 *             "FRIE-048e4bb2-4fb7-4b8a-b75e-e03b0c9c31ba",
	 *             "FRIE-3bcef577-519d-48e5-afbb-1e5b5a02182c",
	 *             "FRIE-76312415-3038-4c63-90f5-840f0e1c8c4a"
	 *         ],
	 *         "email": "a@a.com",
	 *         "displayName": "",
	 *         "firstName": null,
	 *         "lastName": null,
	 *         "musicalPreferences": null,
	 *         "friends": [
	 *             "USER-a8c4b5cb-810c-4c6c-a834-ccf40732095f",
	 *             "USER-8bd822eb-6bfa-4d99-b995-911e84acc848"
	 *         ],
	 *         "playlists": [
	 *             "PLAY-3580bfbd-d415-4c6b-a1be-74fb7e342d95"
	 *         ],
	 *         "guestInPlaylists": [],
	 *         "editorInPlaylists": [],
	 *         "events": [
	 *             "EVEN-86cac9bf-bb5c-4757-85f1-822c7abb6e7d",
	 *             "EVEN-b89496c4-e786-402b-b4b7-a177e83a82d0",
	 *             "EVEN-9204d3c7-e7d3-459b-af3c-fb4ada2767c5"
	 *         ],
	 *         "guestInEvents": [],
	 *         "djInEvents": []
	 *     },
	 *     "visibility": "PUBLIC",
	 *     "votingSchedule": null,
	 *     "eventSchedule": {
	 *         "start": "2019-05-01T17:30:00.000Z",
	 *         "end": "2019-05-02T02:00:00.000Z"
	 *     },
	 *     "coordinates": null,
	 *     "message": "guest removed"
	 * }
	 */
	app.delete('/events/:eventid/guests/:userid', this.event.removeGuest);

	/**
	 * @api {delete} /events/:eventid/guests Remove guests
	 * @apiVersion 1.0.0
	 * @apiName RemoveGuestsFromEvent
	 * @apiGroup Events
	 * 
	 * @apiParam (Queries) {String} apiKey The user's apiKey.
	 * 
	 * @apiParam (Param) {String} eventid The id of the event.
	 * 
	 * @apiParam (Body) {String[]} userids The list of ids of users to remove as guests.
	 * 
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "guests": [],
	 *     "djs": [],
	 *     "playlists": [],
	 *     "eventPlaylists": [],
	 *     "activePlaylist": null,
	 *     "votingActive": true,
	 *     "eventActive": false,
	 *     "trackPosition": 0,
	 *     "lastTrackActionTimestamp": null,
	 *     "id": "EVEN-b89496c4-e786-402b-b4b7-a177e83a82d0",
	 *     "name": "Event-1",
	 *     "description": null,
	 *     "owner": {
	 *         "id": "USER-8ed6e858-ae0f-4e40-b2e1-eea77107d693",
	 *         "avatar": "avatars/default-avatars/avatar-002.png",
	 *         "isPremium": true,
	 *         "theme": null,
	 *         "apiKey": "_KEY-6279b709-0abb-420c-be96-8efdd1f9efea",
	 *         "friendships": [
	 *             "FRIE-048e4bb2-4fb7-4b8a-b75e-e03b0c9c31ba",
	 *             "FRIE-3bcef577-519d-48e5-afbb-1e5b5a02182c",
	 *             "FRIE-76312415-3038-4c63-90f5-840f0e1c8c4a"
	 *         ],
	 *         "email": "a@a.com",
	 *         "displayName": "",
	 *         "firstName": null,
	 *         "lastName": null,
	 *         "musicalPreferences": null,
	 *         "friends": [
	 *             "USER-a8c4b5cb-810c-4c6c-a834-ccf40732095f",
	 *             "USER-8bd822eb-6bfa-4d99-b995-911e84acc848"
	 *         ],
	 *         "playlists": [
	 *             "PLAY-3580bfbd-d415-4c6b-a1be-74fb7e342d95"
	 *         ],
	 *         "guestInPlaylists": [],
	 *         "editorInPlaylists": [],
	 *         "events": [
	 *             "EVEN-86cac9bf-bb5c-4757-85f1-822c7abb6e7d",
	 *             "EVEN-b89496c4-e786-402b-b4b7-a177e83a82d0",
	 *             "EVEN-9204d3c7-e7d3-459b-af3c-fb4ada2767c5"
	 *         ],
	 *         "guestInEvents": [],
	 *         "djInEvents": []
	 *     },
	 *     "visibility": "PUBLIC",
	 *     "votingSchedule": null,
	 *     "eventSchedule": {
	 *         "start": "2019-05-01T17:30:00.000Z",
	 *         "end": "2019-05-02T02:00:00.000Z"
	 *     },
	 *     "coordinates": null,
	 *     "message": "guests removed"
	 * }
	 */
	app.delete('/events/:eventid/guests', this.event.removeGuest);

	/**
	 * @api {put} /events/:eventid/djs/:userid Add dj
	 * @apiVersion 1.0.0
	 * @apiName AddDJToEvent
	 * @apiGroup Events
	 * 
	 * @apiParam (Queries) {String} apiKey The user's apiKey.
	 * 
	 * @apiParam (Param) {String} eventid The id of the event.
	 * @apiParam (Param) {String} userid The id of the user to add as dj.
	 * 
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "guests": [],
	 *     "djs": [
	 *         {
	 *             "id": "USER-8bd822eb-6bfa-4d99-b995-911e84acc848",
	 *             "avatar": "avatars/default-avatars/avatar-004.png",
	 *             "isPremium": true,
	 *             "email": "b@b.com",
	 *             "displayName": null,
	 *             "firstName": null,
	 *             "lastName": null,
	 *             "friends": [],
	 *             "playlists": [],
	 *             "events": [
	 *                 "EVEN-86c9d446-463b-4aee-927a-a1df932383da",
	 *                 "EVEN-d6e6ba6f-b36a-45d3-9244-e9e7a1154a81"
	 *             ]
	 *         }
	 *     ],
	 *     "playlists": [],
	 *     "eventPlaylists": [],
	 *     "activePlaylist": null,
	 *     "votingActive": true,
	 *     "eventActive": false,
	 *     "trackPosition": 0,
	 *     "lastTrackActionTimestamp": null,
	 *     "id": "EVEN-b89496c4-e786-402b-b4b7-a177e83a82d0",
	 *     "name": "Event-1",
	 *     "description": null,
	 *     "owner": {
	 *         "id": "USER-8ed6e858-ae0f-4e40-b2e1-eea77107d693",
	 *         "avatar": "avatars/default-avatars/avatar-002.png",
	 *         "isPremium": true,
	 *         "theme": null,
	 *         "apiKey": "_KEY-6279b709-0abb-420c-be96-8efdd1f9efea",
	 *         "friendships": [
	 *             "FRIE-048e4bb2-4fb7-4b8a-b75e-e03b0c9c31ba",
	 *             "FRIE-3bcef577-519d-48e5-afbb-1e5b5a02182c",
	 *             "FRIE-76312415-3038-4c63-90f5-840f0e1c8c4a"
	 *         ],
	 *         "email": "a@a.com",
	 *         "displayName": "",
	 *         "firstName": null,
	 *         "lastName": null,
	 *         "musicalPreferences": null,
	 *         "friends": [
	 *             "USER-a8c4b5cb-810c-4c6c-a834-ccf40732095f",
	 *             "USER-8bd822eb-6bfa-4d99-b995-911e84acc848"
	 *         ],
	 *         "playlists": [
	 *             "PLAY-3580bfbd-d415-4c6b-a1be-74fb7e342d95"
	 *         ],
	 *         "guestInPlaylists": [],
	 *         "editorInPlaylists": [],
	 *         "events": [
	 *             "EVEN-86cac9bf-bb5c-4757-85f1-822c7abb6e7d",
	 *             "EVEN-b89496c4-e786-402b-b4b7-a177e83a82d0",
	 *             "EVEN-9204d3c7-e7d3-459b-af3c-fb4ada2767c5"
	 *         ],
	 *         "guestInEvents": [],
	 *         "djInEvents": []
	 *     },
	 *     "visibility": "PUBLIC",
	 *     "votingSchedule": null,
	 *     "eventSchedule": {
	 *         "start": "2019-05-01T17:30:00.000Z",
	 *         "end": "2019-05-02T02:00:00.000Z"
	 *     },
	 *     "coordinates": null,
	 *     "message": "dj added"
	 * }
	 */
	app.put('/events/:eventid/djs/:userid', this.event.addDJ);

	/**
	 * @api {put} /events/:eventid/djs Add djs
	 * @apiVersion 1.0.0
	 * @apiName AddDJsToEvent
	 * @apiGroup Events
	 * 
	 * @apiParam (Queries) {String} apiKey The user's apiKey.
	 * 
	 * @apiParam (Param) {String} eventid The id of the event.
	 * 
	 * @apiParam (Body) {String[]} userids A list of ids of users to add as djs.
	 * 
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "guests": [],
	 *     "djs": [
	 *         {
	 *             "id": "USER-8bd822eb-6bfa-4d99-b995-911e84acc848",
	 *             "avatar": "avatars/default-avatars/avatar-004.png",
	 *             "isPremium": true,
	 *             "email": "b@b.com",
	 *             "displayName": null,
	 *             "firstName": null,
	 *             "lastName": null,
	 *             "friends": [],
	 *             "playlists": [],
	 *             "events": []
	 *         },
	 *         {
	 *             "id": "USER-45645g6fd-6bfa-dfsf-trez-911e84acdsrs",
	 *             "avatar": "avatars/default-avatars/avatar-001.png",
	 *             "isPremium": true,
	 *             "email": "c@c.com",
	 *             "displayName": null,
	 *             "firstName": null,
	 *             "lastName": null,
	 *             "friends": [],
	 *             "playlists": [],
	 *             "events": []
	 *         },
	 *     ],
	 *     "playlists": [],
	 *     "eventPlaylists": [],
	 *     "activePlaylist": null,
	 *     "votingActive": true,
	 *     "eventActive": false,
	 *     "trackPosition": 0,
	 *     "lastTrackActionTimestamp": null,
	 *     "id": "EVEN-b89496c4-e786-402b-b4b7-a177e83a82d0",
	 *     "name": "Event-1",
	 *     "description": null,
	 *     "owner": {
	 *         "id": "USER-8ed6e858-ae0f-4e40-b2e1-eea77107d693",
	 *         "avatar": "avatars/default-avatars/avatar-002.png",
	 *         "isPremium": true,
	 *         "theme": null,
	 *         "apiKey": "_KEY-6279b709-0abb-420c-be96-8efdd1f9efea",
	 *         "friendships": [
	 *             "FRIE-048e4bb2-4fb7-4b8a-b75e-e03b0c9c31ba",
	 *             "FRIE-3bcef577-519d-48e5-afbb-1e5b5a02182c",
	 *             "FRIE-76312415-3038-4c63-90f5-840f0e1c8c4a"
	 *         ],
	 *         "email": "a@a.com",
	 *         "displayName": "",
	 *         "firstName": null,
	 *         "lastName": null,
	 *         "musicalPreferences": null,
	 *         "friends": [
	 *             "USER-a8c4b5cb-810c-4c6c-a834-ccf40732095f",
	 *             "USER-8bd822eb-6bfa-4d99-b995-911e84acc848"
	 *         ],
	 *         "playlists": [
	 *             "PLAY-3580bfbd-d415-4c6b-a1be-74fb7e342d95"
	 *         ],
	 *         "guestInPlaylists": [],
	 *         "editorInPlaylists": [],
	 *         "events": [
	 *             "EVEN-86cac9bf-bb5c-4757-85f1-822c7abb6e7d",
	 *             "EVEN-b89496c4-e786-402b-b4b7-a177e83a82d0",
	 *             "EVEN-9204d3c7-e7d3-459b-af3c-fb4ada2767c5"
	 *         ],
	 *         "guestInEvents": [],
	 *         "djInEvents": []
	 *     },
	 *     "visibility": "PUBLIC",
	 *     "votingSchedule": null,
	 *     "eventSchedule": {
	 *         "start": "2019-05-01T17:30:00.000Z",
	 *         "end": "2019-05-02T02:00:00.000Z"
	 *     },
	 *     "coordinates": null,
	 *     "message": "djs added"
	 * }
	 */
	app.put('/events/:eventid/djs', this.event.addDJ);

	/**
	 * @api {delete} /events/:eventid/djs/:userid Remove dj
	 * @apiVersion 1.0.0
	 * @apiName RemoveDJFromEvent
	 * @apiGroup Events
	 * 
	 * @apiParam (Queries) {String} apiKey The user's apiKey.
	 * 
	 * @apiParam (Param) {String} eventid The id of the event.
	 * @apiParam (Param) {String} userid The id of the user to remove as dj.
	 * 
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "guests": [],
	 *     "djs": [
	 *         {
	 *             "id": "USER-8bd822eb-6bfa-4d99-b995-911e84acc848",
	 *             "avatar": "avatars/default-avatars/avatar-004.png",
	 *             "isPremium": true,
	 *             "email": "b@b.com",
	 *             "displayName": null,
	 *             "firstName": null,
	 *             "lastName": null,
	 *             "friends": [],
	 *             "playlists": [],
	 *             "events": []
	 *         }
	 *     ],
	 *     "playlists": [],
	 *     "eventPlaylists": [],
	 *     "activePlaylist": null,
	 *     "votingActive": true,
	 *     "eventActive": false,
	 *     "trackPosition": 0,
	 *     "lastTrackActionTimestamp": null,
	 *     "id": "EVEN-b89496c4-e786-402b-b4b7-a177e83a82d0",
	 *     "name": "Event-1",
	 *     "description": null,
	 *     "owner": {
	 *         "id": "USER-8ed6e858-ae0f-4e40-b2e1-eea77107d693",
	 *         "avatar": "avatars/default-avatars/avatar-002.png",
	 *         "isPremium": true,
	 *         "theme": null,
	 *         "apiKey": "_KEY-6279b709-0abb-420c-be96-8efdd1f9efea",
	 *         "friendships": [
	 *             "FRIE-048e4bb2-4fb7-4b8a-b75e-e03b0c9c31ba",
	 *             "FRIE-3bcef577-519d-48e5-afbb-1e5b5a02182c",
	 *             "FRIE-76312415-3038-4c63-90f5-840f0e1c8c4a"
	 *         ],
	 *         "email": "a@a.com",
	 *         "displayName": "",
	 *         "firstName": null,
	 *         "lastName": null,
	 *         "musicalPreferences": null,
	 *         "friends": [
	 *             "USER-a8c4b5cb-810c-4c6c-a834-ccf40732095f",
	 *             "USER-8bd822eb-6bfa-4d99-b995-911e84acc848"
	 *         ],
	 *         "playlists": [
	 *             "PLAY-3580bfbd-d415-4c6b-a1be-74fb7e342d95"
	 *         ],
	 *         "guestInPlaylists": [],
	 *         "editorInPlaylists": [],
	 *         "events": [
	 *             "EVEN-86cac9bf-bb5c-4757-85f1-822c7abb6e7d",
	 *             "EVEN-b89496c4-e786-402b-b4b7-a177e83a82d0",
	 *             "EVEN-9204d3c7-e7d3-459b-af3c-fb4ada2767c5"
	 *         ],
	 *         "guestInEvents": [],
	 *         "djInEvents": []
	 *     },
	 *     "visibility": "PUBLIC",
	 *     "votingSchedule": null,
	 *     "eventSchedule": {
	 *         "start": "2019-05-01T17:30:00.000Z",
	 *         "end": "2019-05-02T02:00:00.000Z"
	 *     },
	 *     "coordinates": null,
	 *     "message": "dj removed"
	 * }
	 */
	app.delete('/events/:eventid/djs/:userid', this.event.removeDJ);

	/**
	 * @api {delete} /events/:eventid/djs Remove djs
	 * @apiVersion 1.0.0
	 * @apiName RemoveDJsFromEvent
	 * @apiGroup Events
	 * 
	 * @apiParam (Queries) {String} apiKey The user's apiKey.
	 * 
	 * @apiParam (Param) {String} eventid The id of the event.
	 * 
	 * @apiParam (Body) {String[]} userids The list of ids of users to remove as djs.
	 * 
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "guests": [],
	 *     "djs": [],
	 *     "playlists": [],
	 *     "eventPlaylists": [],
	 *     "activePlaylist": null,
	 *     "votingActive": true,
	 *     "eventActive": false,
	 *     "trackPosition": 0,
	 *     "lastTrackActionTimestamp": null,
	 *     "id": "EVEN-b89496c4-e786-402b-b4b7-a177e83a82d0",
	 *     "name": "Event-1",
	 *     "description": null,
	 *     "owner": {
	 *         "id": "USER-8ed6e858-ae0f-4e40-b2e1-eea77107d693",
	 *         "avatar": "avatars/default-avatars/avatar-002.png",
	 *         "isPremium": true,
	 *         "theme": null,
	 *         "apiKey": "_KEY-6279b709-0abb-420c-be96-8efdd1f9efea",
	 *         "friendships": [
	 *             "FRIE-048e4bb2-4fb7-4b8a-b75e-e03b0c9c31ba",
	 *             "FRIE-3bcef577-519d-48e5-afbb-1e5b5a02182c",
	 *             "FRIE-76312415-3038-4c63-90f5-840f0e1c8c4a"
	 *         ],
	 *         "email": "a@a.com",
	 *         "displayName": "",
	 *         "firstName": null,
	 *         "lastName": null,
	 *         "musicalPreferences": null,
	 *         "friends": [
	 *             "USER-a8c4b5cb-810c-4c6c-a834-ccf40732095f",
	 *             "USER-8bd822eb-6bfa-4d99-b995-911e84acc848"
	 *         ],
	 *         "playlists": [
	 *             "PLAY-3580bfbd-d415-4c6b-a1be-74fb7e342d95"
	 *         ],
	 *         "guestInPlaylists": [],
	 *         "editorInPlaylists": [],
	 *         "events": [
	 *             "EVEN-86cac9bf-bb5c-4757-85f1-822c7abb6e7d",
	 *             "EVEN-b89496c4-e786-402b-b4b7-a177e83a82d0",
	 *             "EVEN-9204d3c7-e7d3-459b-af3c-fb4ada2767c5"
	 *         ],
	 *         "guestInEvents": [],
	 *         "djInEvents": []
	 *     },
	 *     "visibility": "PUBLIC",
	 *     "votingSchedule": null,
	 *     "eventSchedule": {
	 *         "start": "2019-05-01T17:30:00.000Z",
	 *         "end": "2019-05-02T02:00:00.000Z"
	 *     },
	 *     "coordinates": null,
	 *     "message": "djs removed"
	 * }
	 */
	app.delete('/events/:eventid/djs', this.event.removeDJ);

	/**
	 * @api {put} /events/:eventid/playlists/:playlistid Add Playlist
	 * @apiVersion 1.0.0
	 * @apiName AddPlaylistToEvent
	 * @apiGroup Events
	 * 
	 * @apiParam (Queries) {String} apiKey The user's apiKey.
	 * 
	 * @apiParam (Param) {String} eventid The id of the event.
	 * @apiParam (Param) {String} playlistid Id of playlist to add.
	 * 
	 * @apiParam (Body) {Boolean} [inviteGuests] Set to true to invite guests of playlist as guests of event
	 * @apiParam (Body) {Boolean} [inviteEditors] Set to true to invite editors of playlist as guests of event
	 * @apiParam (Body) {Boolean} [duplicatePlaylist] Set to true to duplicate the playlist before adding it to the event
	 * @apiParam (Body) {Boolean} [copyEditors] Copies editors into new playlist if user with submitted apiKey is owner of playlist (ignored if duplicatePlaylist not set to true)
	 * @apiParam (Body) {Boolean} [copyGuests] Copies guests into new playlist if user with submitted apiKey is owner of playlist (ignored if duplicatePlaylist not set to true)
	 * 
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "guests": [],
	 *     "djs": [],
	 *     "playlists": [
	 *         {
	 *             "tracks": [
	 *                 {
	 *                     "id": "TRAC-325d18c3-c90f-470b-953b-4483bbe7c260",
	 *                     "serviceid": 125270816,
	 *                     "service": null
	 *                 },
	 *                 {
	 *                     "id": "TRAC-cbf8a098-3bd3-46ce-87c9-33eee46bbd48",
	 *                     "serviceid": 597627262,
	 *                     "service": null
	 *                 },
	 *                 {
	 *                     "id": "TRAC-eefd1071-b742-4b2b-9af9-5478eb4803e7",
	 *                     "serviceid": 597627232,
	 *                     "service": null
	 *                 }
	 *             ],
	 *             "guests": [],
	 *             "editors": [],
	 *             "id": "PLAY-3580bfbd-d415-4c6b-a1be-74fb7e342d95",
	 *             "name": "Playlist-1",
	 *             "owner": "USER-8ed6e858-ae0f-4e40-b2e1-eea77107d693",
	 *             "events": [
	 *                 "EVEN-86cac9bf-bb5c-4757-85f1-822c7abb6e7d",
	 *                 "EVEN-b89496c4-e786-402b-b4b7-a177e83a82d0"
	 *             ],
	 *             "visibility": "PUBLIC",
	 *             "editing": "PUBLIC",
	 *             "sortingType": "vote"
	 *         }
	 *     ],
	 *     "eventPlaylists": [],
	 *     "activePlaylist": null,
	 *     "votingActive": false,
	 *     "eventActive": false,
	 *     "trackPosition": 0,
	 *     "lastTrackActionTimestamp": null,
	 *     "id": "EVEN-b89496c4-e786-402b-b4b7-a177e83a82d0",
	 *     "name": "Event-1",
	 *     "description": null,
	 *     "owner": {
	 *         "id": "USER-8ed6e858-ae0f-4e40-b2e1-eea77107d693",
	 *         "avatar": "avatars/default-avatars/avatar-002.png",
	 *         "isPremium": true,
	 *         "theme": null,
	 *         "apiKey": "_KEY-6279b709-0abb-420c-be96-8efdd1f9efea",
	 *         "friendships": [
	 *             "FRIE-76312415-3038-4c63-90f5-840f0e1c8c4a"
	 *         ],
	 *         "email": "a@a.com",
	 *         "displayName": "",
	 *         "firstName": null,
	 *         "lastName": null,
	 *         "musicalPreferences": null,
	 *         "friends": [],
	 *         "playlists": [
	 *             "PLAY-3580bfbd-d415-4c6b-a1be-74fb7e342d95"
	 *         ],
	 *         "guestInPlaylists": [],
	 *         "editorInPlaylists": [],
	 *         "events": [
	 *             "EVEN-9204d3c7-e7d3-459b-af3c-fb4ada2767c5"
	 *         ],
	 *         "guestInEvents": [],
	 *         "djInEvents": []
	 *     },
	 *     "visibility": "PUBLIC",
	 *     "votingSchedule": null,
	 *     "eventSchedule": {
	 *         "start": "2019-05-01T17:30:00.000Z",
	 *         "end": "2019-05-02T02:00:00.000Z"
	 *     },
	 *     "coordinates": null,
	 *     "message": "playlist added"
	 * }
	 */
	app.put('/events/:eventid/playlists/:playlistid', this.event.addPlaylist);
	
	/**
	 * @api {put} /events/:eventid/playlists Add Playlists
	 * @apiVersion 1.0.0
	 * @apiName AddPlaylistsToEvent
	 * @apiGroup Events
	 * 
	 * @apiParam (Queries) {String} apiKey The user's apiKey.
	 * 
	 * @apiParam (Param) {String} eventid The id of the event.
	 * 
	 * @apiParam (Body) {String[]} playlistids Ids of playlist to add.
	 * @apiParam (Body) {Boolean} [inviteGuests] Set to true to invite guests of playlist as guests of event
	 * @apiParam (Body) {Boolean} [inviteEditors] Set to true to invite editors of playlist as guests of event
	 * @apiParam (Body) {Boolean} [duplicatePlaylist] Set to true to duplicate all playlists before adding them to the event
	 * @apiParam (Body) {Boolean} [copyEditors] Copies editors into new playlist if user with submitted apiKey is owner of playlist (ignored if duplicatePlaylist not set to true)
	 * @apiParam (Body) {Boolean} [copyGuests] Copies guests into new playlist if user with submitted apiKey is owner of playlist (ignored if duplicatePlaylist not set to true)
	 * 
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "guests": [],
	 *     "djs": [],
	 *     "playlists": [
	 *         {
	 *             "tracks": [
	 *                 {
	 *                     "id": "TRAC-325d18c3-c90f-470b-953b-4483bbe7c260",
	 *                     "serviceid": 125270816,
	 *                     "service": null
	 *                 },
	 *                 {
	 *                     "id": "TRAC-cbf8a098-3bd3-46ce-87c9-33eee46bbd48",
	 *                     "serviceid": 597627262,
	 *                     "service": null
	 *                 },
	 *                 {
	 *                     "id": "TRAC-eefd1071-b742-4b2b-9af9-5478eb4803e7",
	 *                     "serviceid": 597627232,
	 *                     "service": null
	 *                 }
	 *             ],
	 *             "guests": [],
	 *             "editors": [],
	 *             "id": "PLAY-3580bfbd-d415-4c6b-a1be-74fb7e342d95",
	 *             "name": "Playlist-2",
	 *             "owner": "USER-8ed6e858-ae0f-4e40-b2e1-eea77107d693",
	 *             "events": [
	 *                 "EVEN-86cac9bf-bb5c-4757-85f1-822c7abb6e7d",
	 *                 "EVEN-b89496c4-e786-402b-b4b7-a177e83a82d0"
	 *             ],
	 *             "visibility": "PUBLIC",
	 *             "editing": "PUBLIC",
	 *             "sortingType": "vote"
	 *         },
	 *         {
	 *             "tracks": [
	 *                 {
	 *                     "id": "TRAC-fe4f8e4c-c90f-rzes-rfez-f4e8f4ztj4n6",
	 *                     "serviceid": 1252456,
	 *                     "service": null
	 *                 },
	 *                 {
	 *                     "id": "TRAC-fds4c5ds1-fsdv-uyis-vcx4-fdsf7ds4f9s7",
	 *                     "serviceid": 456489,
	 *                     "service": null
	 *                 }
	 *             ],
	 *             "guests": [],
	 *             "editors": [],
	 *             "id": "PLAY-3580bfbd-d415-4c6b-a1be-74fb7e342d95",
	 *             "name": "Playlist-1",
	 *             "owner": "USER-8ed6e858-ae0f-4e40-b2e1-eea77107d693",
	 *             "events": [
	 *                 "EVEN-86cac9bf-bb5c-4757-85f1-822c7abb6e7d",
	 *                 "EVEN-b89496c4-e786-402b-b4b7-a177e83a82d0"
	 *             ],
	 *             "visibility": "PUBLIC",
	 *             "editing": "PUBLIC",
	 *             "sortingType": "vote"
	 *         }
	 *     ],
	 *     "eventPlaylists": [],
	 *     "activePlaylist": null,
	 *     "votingActive": false,
	 *     "eventActive": false,
	 *     "trackPosition": 0,
	 *     "lastTrackActionTimestamp": null,
	 *     "id": "EVEN-b89496c4-e786-402b-b4b7-a177e83a82d0",
	 *     "name": "Event-1",
	 *     "description": null,
	 *     "owner": {
	 *         "id": "USER-8ed6e858-ae0f-4e40-b2e1-eea77107d693",
	 *         "avatar": "avatars/default-avatars/avatar-002.png",
	 *         "isPremium": true,
	 *         "theme": null,
	 *         "apiKey": "_KEY-6279b709-0abb-420c-be96-8efdd1f9efea",
	 *         "friendships": [
	 *             "FRIE-76312415-3038-4c63-90f5-840f0e1c8c4a"
	 *         ],
	 *         "email": "a@a.com",
	 *         "displayName": "",
	 *         "firstName": null,
	 *         "lastName": null,
	 *         "musicalPreferences": null,
	 *         "friends": [],
	 *         "playlists": [
	 *             "PLAY-3580bfbd-d415-4c6b-a1be-74fb7e342d95"
	 *         ],
	 *         "guestInPlaylists": [],
	 *         "editorInPlaylists": [],
	 *         "events": [
	 *             "EVEN-9204d3c7-e7d3-459b-af3c-fb4ada2767c5"
	 *         ],
	 *         "guestInEvents": [],
	 *         "djInEvents": []
	 *     },
	 *     "visibility": "PUBLIC",
	 *     "votingSchedule": null,
	 *     "eventSchedule": {
	 *         "start": "2019-05-01T17:30:00.000Z",
	 *         "end": "2019-05-02T02:00:00.000Z"
	 *     },
	 *     "coordinates": null,
	 *     "message": "playlists added"
	 * }
	 */
	app.put('/events/:eventid/playlists', this.event.addPlaylist);

	/**
	 * @api {delete} /events/:eventid/playlists/:playlistid Remove Playlist
	 * @apiVersion 1.0.0
	 * @apiName RemovePlaylistFromEvent
	 * @apiGroup Events
	 * 
	 * @apiParam (Queries) {String} apiKey The user's apiKey.
	 * 
	 * @apiParam (Param) {String} eventid The id of the event.
	 * @apiParam (Param) {String} playlistid Id of playlist to remove.
	 * 
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "guests": [],
	 *     "djs": [],
	 *     "playlists": [],
	 *     "eventPlaylists": [],
	 *     "activePlaylist": null,
	 *     "votingActive": false,
	 *     "eventActive": false,
	 *     "trackPosition": 0,
	 *     "lastTrackActionTimestamp": null,
	 *     "id": "EVEN-b89496c4-e786-402b-b4b7-a177e83a82d0",
	 *     "name": "Event-1",
	 *     "description": null,
	 *     "owner": {
	 *         "id": "USER-8ed6e858-ae0f-4e40-b2e1-eea77107d693",
	 *         "avatar": "avatars/default-avatars/avatar-002.png",
	 *         "isPremium": true,
	 *         "theme": null,
	 *         "apiKey": "_KEY-6279b709-0abb-420c-be96-8efdd1f9efea",
	 *         "friendships": [
	 *             "FRIE-76312415-3038-4c63-90f5-840f0e1c8c4a"
	 *         ],
	 *         "email": "a@a.com",
	 *         "displayName": "",
	 *         "firstName": null,
	 *         "lastName": null,
	 *         "musicalPreferences": null,
	 *         "friends": [],
	 *         "playlists": [
	 *             "PLAY-3580bfbd-d415-4c6b-a1be-74fb7e342d95"
	 *         ],
	 *         "guestInPlaylists": [],
	 *         "editorInPlaylists": [],
	 *         "events": [
	 *             "EVEN-9204d3c7-e7d3-459b-af3c-fb4ada2767c5"
	 *         ],
	 *         "guestInEvents": [],
	 *         "djInEvents": []
	 *     },
	 *     "visibility": "PUBLIC",
	 *     "votingSchedule": null,
	 *     "eventSchedule": {
	 *         "start": "2019-05-01T17:30:00.000Z",
	 *         "end": "2019-05-02T02:00:00.000Z"
	 *     },
	 *     "coordinates": null,
	 *     "message": "playlist removed"
	 * }
	 */
	app.delete('/events/:eventid/playlists/:playlistid', this.event.removePlaylist);

	/**
	 * @api {delete} /events/:eventid/playlists Remove Playlists
	 * @apiVersion 1.0.0
	 * @apiName RemovePlaylistsFromEvent
	 * @apiGroup Events
	 * 
	 * @apiParam (Queries) {String} apiKey The user's apiKey.
	 * 
	 * @apiParam (Param) {String} eventid The id of the event.
	 * 
	 * @apiParam (Body) {String[]} playlistids Id of playlist to remove.
	 * 
 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "guests": [],
	 *     "djs": [],
	 *     "playlists": [],
	 *     "eventPlaylists": [],
	 *     "activePlaylist": null,
	 *     "votingActive": false,
	 *     "eventActive": false,
	 *     "trackPosition": 0,
	 *     "lastTrackActionTimestamp": null,
	 *     "id": "EVEN-b89496c4-e786-402b-b4b7-a177e83a82d0",
	 *     "name": "Event-1",
	 *     "description": null,
	 *     "owner": {
	 *         "id": "USER-8ed6e858-ae0f-4e40-b2e1-eea77107d693",
	 *         "avatar": "avatars/default-avatars/avatar-002.png",
	 *         "isPremium": true,
	 *         "theme": null,
	 *         "apiKey": "_KEY-6279b709-0abb-420c-be96-8efdd1f9efea",
	 *         "friendships": [
	 *             "FRIE-76312415-3038-4c63-90f5-840f0e1c8c4a"
	 *         ],
	 *         "email": "a@a.com",
	 *         "displayName": "",
	 *         "firstName": null,
	 *         "lastName": null,
	 *         "musicalPreferences": null,
	 *         "friends": [],
	 *         "playlists": [
	 *             "PLAY-3580bfbd-d415-4c6b-a1be-74fb7e342d95"
	 *         ],
	 *         "guestInPlaylists": [],
	 *         "editorInPlaylists": [],
	 *         "events": [
	 *             "EVEN-9204d3c7-e7d3-459b-af3c-fb4ada2767c5"
	 *         ],
	 *         "guestInEvents": [],
	 *         "djInEvents": []
	 *     },
	 *     "visibility": "PUBLIC",
	 *     "votingSchedule": null,
	 *     "eventSchedule": {
	 *         "start": "2019-05-01T17:30:00.000Z",
	 *         "end": "2019-05-02T02:00:00.000Z"
	 *     },
	 *     "coordinates": null,
	 *     "message": "playlists removed"
	 * }
	 */
	app.delete('/events/:eventid/playlists', this.event.removePlaylist);

	/**
	 * @api {put} /events/:eventid/:eventaction Event Action
	 * @apiVersion 1.0.0
	 * @apiName EventAction
	 * @apiGroup Events
	 * 
	 * @apiParam (Queries) {String} apiKey The user's apiKey.
	 * 
	 * @apiParam (Param) {String} eventid The id of the event.
	 * @apiParam (Param) {String="startvoting","endvoting","startevent","endevent"} eventaction
	 * 
 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "guests": [],
	 *     "djs": [],
	 *     "playlists": [],
	 *     "eventPlaylists": [],
	 *     "activePlaylist": null,
	 *     "votingActive": false,
	 *     "eventActive": false,
	 *     "trackPosition": 0,
	 *     "lastTrackActionTimestamp": null,
	 *     "id": "EVEN-b89496c4-e786-402b-b4b7-a177e83a82d0",
	 *     "name": "Event-1",
	 *     "description": null,
	 *     "owner": {
	 *         "id": "USER-8ed6e858-ae0f-4e40-b2e1-eea77107d693",
	 *         "avatar": "avatars/default-avatars/avatar-002.png",
	 *         "isPremium": true,
	 *         "theme": null,
	 *         "apiKey": "_KEY-6279b709-0abb-420c-be96-8efdd1f9efea",
	 *         "friendships": [
	 *             "FRIE-76312415-3038-4c63-90f5-840f0e1c8c4a"
	 *         ],
	 *         "email": "a@a.com",
	 *         "displayName": "",
	 *         "firstName": null,
	 *         "lastName": null,
	 *         "musicalPreferences": null,
	 *         "friends": [],
	 *         "playlists": [
	 *             "PLAY-3580bfbd-d415-4c6b-a1be-74fb7e342d95"
	 *         ],
	 *         "guestInPlaylists": [],
	 *         "editorInPlaylists": [],
	 *         "events": [
	 *             "EVEN-9204d3c7-e7d3-459b-af3c-fb4ada2767c5"
	 *         ],
	 *         "guestInEvents": [],
	 *         "djInEvents": []
	 *     },
	 *     "visibility": "PUBLIC",
	 *     "votingSchedule": null,
	 *     "eventSchedule": {
	 *         "start": "2019-05-01T17:30:00.000Z",
	 *         "end": "2019-05-02T02:00:00.000Z"
	 *     },
	 *     "coordinates": null,
	 *     "message": "{voting / event} {started / ended}"
	 * }
	 */
	app.put('/events/:eventid/:eventaction', this.event.eventAction);

	/**
	 * @api {put} /events/:eventid/player/:playeraction Player Action
	 * @apiVersion 1.0.0
	 * @apiName PlayerAction
	 * @apiGroup Events
	 * 
	 * @apiParam (Queries) {String} apiKey The user's apiKey.
	 * 
	 * @apiParam (Param) {String} eventid The id of the event.
	 * @apiParam (Param) {String="play","pause","seek"} playeraction
	 * 
	 * @apiParam (Body) {String} trackid (with playeraction=play only)
	 * @apiParam (Body) {String(Date)} timestamp Timestamp at which action is taken. (as event owner only)
	 * @apiParam (Body) {number} trackPosition Position in track in milliseconds. (with playeraction=seek, or as event owner)
	 * 
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "guests": [],
	 *     "djs": [],
	 *     "playlists": [],
	 *     "eventPlaylists": [
	 *         {
	 *             "playedTracks": [
	 *                 {
	 *                     "id": "ETRA-0f15764c-97ae-4ed6-8a50-fb7993fc6443",
	 *                     "serviceid": 125270816,
	 *                     "service": null,
	 *                     "isPlaying": true,
	 *                     "likes": [],
	 *                     "dislikes": []
	 *                 }
	 *             ],
	 *             "activeTrack": "ETRA-0f15764c-97ae-4ed6-8a50-fb7993fc6443",
	 *             "unplayedTracks": [
	 *                 {
	 *                     "id": "ETRA-5826f663-fb43-49bb-adbb-098be14be035",
	 *                     "serviceid": 597627262,
	 *                     "service": null,
	 *                     "isPlaying": false,
	 *                     "likes": [],
	 *                     "dislikes": []
	 *                 }
	 *             ],
	 *             "id": "EPLA-ff4f618a-f6b0-4ecf-b8e1-536c35a561fc",
	 *             "refPlaylistId": "PLAY-3580bfbd-d415-4c6b-a1be-74fb7e342d95",
	 *             "name": "Playlist-1",
	 *             "sortingType": "vote"
	 *         }
	 *     ],
	 *     "activePlaylist": "EPLA-ff4f618a-f6b0-4ecf-b8e1-536c35a561fc",
	 *     "votingActive": true,
	 *     "eventActive": true,
	 *     "trackPosition": 0,
	 *     "lastTrackActionTimestamp": "2019-05-02T08:53:05.700Z",
	 *     "id": "EVEN-86cac9bf-bb5c-4757-85f1-822c7abb6e7d",
	 *     "name": "Test",
	 *     "description": null,
	 *     "owner": {
	 *         "id": "USER-8ed6e858-ae0f-4e40-b2e1-eea77107d693",
	 *         "avatar": "avatars/default-avatars/avatar-002.png",
	 *         "isPremium": true,
	 *         "theme": null,
	 *         "apiKey": "_KEY-6279b709-0abb-420c-be96-8efdd1f9efea",
	 *         "friendships": [
	 *             "FRIE-76312415-3038-4c63-90f5-840f0e1c8c4a"
	 *         ],
	 *         "email": "a@a.com",
	 *         "displayName": "",
	 *         "firstName": null,
	 *         "lastName": null,
	 *         "musicalPreferences": null,
	 *         "friends": [
	 *             "USER-a8c4b5cb-810c-4c6c-a834-ccf40732095f"
	 *         ],
	 *         "playlists": [
	 *             "PLAY-3580bfbd-d415-4c6b-a1be-74fb7e342d95"
	 *         ],
	 *         "guestInPlaylists": [],
	 *         "editorInPlaylists": [],
	 *         "events": [
	 *             "EVEN-86cac9bf-bb5c-4757-85f1-822c7abb6e7d",
	 *             "EVEN-b89496c4-e786-402b-b4b7-a177e83a82d0",
	 *             "EVEN-9204d3c7-e7d3-459b-af3c-fb4ada2767c5"
	 *         ],
	 *         "guestInEvents": [],
	 *         "djInEvents": []
	 *     },
	 *     "visibility": "PUBLIC",
	 *     "votingSchedule": {
	 *         "start": "2019-04-30T13:34:00.000Z",
	 *         "end": "2019-04-30T18:42:00.000Z"
	 *     },
	 *     "eventSchedule": null,
	 *     "coordinates": null,
	 *     "message": "track playing"
	 * }
	 */
	app.put('/events/:eventid/player/:playeraction', this.event.playerAction);

	/**
	 * @api {put} /events/:eventid/:trackid/move Track Move
	 * @apiVersion 1.0.0
	 * @apiName TrackMove
	 * @apiGroup Events
	 * 
	 * @apiParam (Queries) {String} apiKey The user's apiKey.
	 * 
	 * @apiParam (Param) {String} eventid The id of the event.
	 * @apiParam (Param) {String} trackid
	 * 
	 * @apiParam (Body) {String/Number} index New index
	 * 
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "id": "ETRA-a004ee62-756f-422e-bd98-03bf65984bb7",
	 *     "serviceid": 670564592,
	 *     "service": null,
	 *     "isPlaying": false,
	 *     "likes": [],
	 *     "dislikes": [],
	 *     "message": "track moved to index 0"
	 * }

	 */
	app.put('/events/:eventid/:trackid/move', this.event.trackMove);

	/**
	 * @api {put} /events/:eventid/:trackid/:vote Track Vote
	 * @apiVersion 1.0.0
	 * @apiName TrackVote
	 * @apiGroup Events
	 * 
	 * @apiParam (Queries) {String} apiKey The user's apiKey.
	 * 
	 * @apiParam (Param) {String} eventid The id of the event.
	 * @apiParam (Param) {String} trackid
	 * @apiParam (Param) {String="like","dislike"} vote
	 * 
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "id": "ETRA-0f15764c-97ae-4ed6-8a50-fb7993fc6443",
	 *     "serviceid": 125270816,
	 *     "service": null,
	 *     "isPlaying": true,
	 *     "likes": [],
	 *     "dislikes": [],
	 *     "message": "track liked"
	 * }
	 */
	app.put('/events/:eventid/:trackid/:vote', this.event.trackVote);
	
	/**
	 * @api {delete} /events/:eventid/:trackid/:vote Track UnVote
	 * @apiVersion 1.0.0
	 * @apiName TrackUnVote
	 * @apiGroup Events
	 * 
	 * @apiParam (Queries) {String} apiKey The user's apiKey.
	 * 
	 * @apiParam (Param) {String} eventid The id of the event.
	 * @apiParam (Param) {String} trackid
	 * @apiParam (Param) {String="like","dislike"} vote
	 * 
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "id": "ETRA-0f15764c-97ae-4ed6-8a50-fb7993fc6443",
	 *     "serviceid": 125270816,
	 *     "service": null,
	 *     "isPlaying": true,
	 *     "likes": [],
	 *     "dislikes": [],
	 *     "message": "track unliked"
	 * }
	 */
	app.delete('/events/:eventid/:trackid/:vote', this.event.trackVote);

	/**
	 * @api {delete} /events/:eventid Delete event
	 * @apiVersion 1.0.0
	 * @apiName DeleteEvent
	 * @apiGroup Events
	 * 
	 * @apiParam (Queries) {String} apiKey The user's apiKey.
	 * 
	 * @apiParam (Param) {String} eventid The id of the event.
	 * 
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "message": "Event deleted"
	 * }
	 */
	app.delete('/events/:eventid', this.event.delete);

	/**
	 * @api {delete} /events Delete events
	 * @apiVersion 1.0.0
	 * @apiName DeleteEvents
	 * @apiGroup Events
	 * 
	 * @apiParam (Queries) {String} apiKey The user's apiKey.
	 * 
	 * @apiParam (Body) {String[]} eventids List of ids of events to delete.
	 * 
	 * @apiSuccessExample
	 * HTTP/1.1 200 OK
	 * {
	 *     "message": "Events deleted"
	 * }
	 */
	app.delete('/events', this.event.delete);
	}
  }


  public async root(req: Request, res: Response) : Promise<any> {
	res.status(ResponseStatusTypes.OK).send(
		{message: 'check out our doc!'}
	)
  }
}
