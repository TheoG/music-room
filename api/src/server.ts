import app from "./app";

const config = require('../config.json');

app.app.listen(config.serverPort, () => {
    console.log('Express server listening on port ' + config.serverPort);
})