import { Request, Response } from "express";
import ResponseStatusTypes from "../utils/ResponseStatusTypes";
import { ErrorCodes, ServerError } from "../utils/errorTypes";
import { Parameter, Validator } from "../classes/Validator";
import { Visibility, SortingType, Editing } from '../types/types'; 
import * as socketio from "socket.io";

import { Playlist } from "../classes/Playlist";
import { User } from "../classes/User";
import { acquireLock, lockKeys } from "../utils/mutex";
import Track from "../classes/Track";
import SocketActions from "../utils/SocketActions"
import { SocketController, socketController } from "./socketController";

let io: socketio.Namespace = null;
let socketUsers: {userApiKey: string, }[]

class PlaylistController {

	public async create(req: Request, res: Response) : Promise<any> {
		const mandatoryParameters: Parameter[] = [new Parameter('apiKey', 'query'), new Parameter('name', 'body')];

		const apiKey: string = req.query.apiKey;
		const name: string = req.body.name;
		const visibility: Visibility = req.body.visibility;
		const editing: Editing = req.body.editing;
		const sortingType: SortingType = req.body.sortingType;

		const validator: Validator = new Validator({apiKey, name, visibility, editing, sortingType}, mandatoryParameters);
		if (await validator.err) {
			res.status(ResponseStatusTypes.BAD_REQUEST).send(await validator.err);			
			return ;
		}

		// check if user exists and account is activated and user is premium 
		let user: User = await User.get({apiKey});
		if (!user) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.INVALID_API_KEY));
			return ;
		}
		if (!user.activated) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.USER_ACCOUNT_NOT_ACTIVATED));
			return ;
		}
		if (!user.isPremium) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.USER_NOT_PREMIUM));
			return ;
		}

		await acquireLock([lockKeys.playlist('create'), lockKeys.user(user.id)], async () => {
			user = await User.get({apiKey});
			if (!user) {
				res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.INVALID_API_KEY));
				return ;
			}
			const playlist: Playlist = new Playlist({
				name,
				owner: user.id,
				visibility: visibility,
				editing: editing,
				sortingType: sortingType
			});
			await playlist.create();
			// add playlist to user
			user.addPlaylist(playlist.id);
			await user.save();
			res.status(ResponseStatusTypes.OK).send({...playlist, message: 'Playlist created'});
			return ;
		});
	}

	public async get(req: Request, res: Response) : Promise<any> {
		const mandatoryParameters: Parameter[] = [new Parameter('apiKey', 'query')];

		const apiKey: string = req.query.apiKey;
		const playlistid: string = req.params.playlistid;
		
		const validator: Validator = new Validator({apiKey, playlistid}, mandatoryParameters);
		if (await validator.err) {
			res.status(ResponseStatusTypes.BAD_REQUEST).send(await validator.err);			
			return ;
		}

		const user: User = await User.get({apiKey});
		if (!user) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.INVALID_API_KEY));
			return ;
		}
		if (!user.activated) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.USER_ACCOUNT_NOT_ACTIVATED));
			return ;
		}

		if (playlistid) {
			const playlist = await Playlist.get({id: playlistid});
			if (!playlist) {
				res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_PLAYLIST));
				return ;
			}
	
			if (playlist.userCanAccess(user)) {
				let ret = await playlist.expand(user);
				res.status(ResponseStatusTypes.OK).send(ret);
			} else {
				res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.ACCESS_NOT_AUTHORIZED));
			}

		} else {
			let ret = {
				owner: await Promise.all(user.playlists.map(async (id: string) => {
					return Playlist.get({id});
				})),
				editor: await Promise.all(user.editorInPlaylists.map(async (id: string) => {
					return Playlist.get({id});
				})),
				guest: await Promise.all(user.guestInPlaylists.map(async (id: string) => {
					return Playlist.get({id});
				}))
			};
			res.status(ResponseStatusTypes.OK).send(ret);
		}
	}

	public async update(req: Request, res: Response) : Promise<any> {
		const mandatoryParameters: Parameter[] = [new Parameter('apiKey', 'query'), new Parameter('playlistid', 'param')];

		const apiKey: string = req.query.apiKey;
		const playlistid: string = req.params.playlistid;
		const name: string = req.body.name;		
		const visibility: Visibility = req.body.visibility;
		const editing: Visibility = req.body.editing;
		const sortingType: SortingType = req.body.sortingType;

		const validator: Validator = new Validator({playlistid, apiKey, name, visibility, editing, sortingType}, mandatoryParameters);
		if (await validator.err) {
			res.status(ResponseStatusTypes.BAD_REQUEST).send(await validator.err);			
			return ;
		}

		// check if user exists and account is activated
		let user: User = await User.get({apiKey});
		if (!user) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.INVALID_API_KEY));
			return ;
		}
		if (!user.activated) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.USER_ACCOUNT_NOT_ACTIVATED));
			return ;
		}

		await acquireLock(lockKeys.playlist(playlistid), async () => {
			const playlist: Playlist = await Playlist.get({id: playlistid});
			if (!playlist) {
				res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_PLAYLIST));
				return ;
			}

			if (playlist.owner != user.id) {
				res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.EDIT_NOT_AUTHORIZED));
				return ;
			}

			const updated: boolean = playlist.update({name, visibility, editing, sortingType});
			if (!updated) {
				res.status(ResponseStatusTypes.OK).send({message: 'No change'});
				return ;
			}
			await playlist.save();
			res.status(ResponseStatusTypes.OK).send({...playlist, message: 'Playlist updated'});
			socketController.emitToRoom('/playlists', playlist.id, SocketActions.PLAYLIST_UPDATE, {name: playlist.name, visibility: playlist.visibility, editing: playlist.editing, sortingType: playlist.sortingType});
			await socketController.updateConnections("/playlists", playlist.id);
		});
	}

	public async addTrack(req: Request, res: Response) : Promise<any> {
		const mandatoryParameters: Parameter[] = [new Parameter('apiKey', 'query'), new Parameter('playlistid', 'param'), new Parameter('serviceid', 'body')];
		const apiKey: string = req.query.apiKey;
		const playlistid: string = req.params.playlistid;
		const serviceid: string = req.body.serviceid;
		const service: string = req.body.service;

		const validator: Validator = new Validator({apiKey, playlistid, serviceid, service}, mandatoryParameters);
		if (await validator.err) {
			res.status(ResponseStatusTypes.BAD_REQUEST).send(await validator.err);
			return ;
		}

		const user: User = await User.get({apiKey});
		if (!user) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.INVALID_API_KEY));
			return ;
		}
		if (!user.activated) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.USER_ACCOUNT_NOT_ACTIVATED));
			return ;
		}

		await acquireLock(lockKeys.playlist(playlistid), async () => {
			const playlist = await Playlist.get({id: playlistid});
			if (!playlist) {
				res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_PLAYLIST));
				return ;
			}
			
			if (!playlist.userCanEdit(user)) {
				res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.EDIT_NOT_AUTHORIZED));
				return ;
			}

			const updated: boolean = playlist.addTrack(serviceid, service);
			if (!updated) {
				res.status(ResponseStatusTypes.OK).send({message: 'No change'});
				return ;
			}
			const track: Track = playlist.tracks.find(x => x.serviceid == serviceid && service == service); 
			await playlist.save();
			
			res.status(ResponseStatusTypes.OK).send({...track, message: 'Track added'});
			socketController.emitToRoom('/playlists', playlist.id, SocketActions.PLAYLIST_ADD_TRACK, track);
		});
	}

	public async removeTrack(req: Request, res: Response) : Promise<any> {
		const mandatoryParameters: Parameter[] = [new Parameter('apiKey', 'query'), new Parameter('playlistid', 'param'), new Parameter('trackid', 'param')];

		const apiKey: string = req.query.apiKey;
		const playlistid: string = req.params.playlistid;
		const trackid: string = req.params.trackid;

		const validator: Validator = new Validator({apiKey, playlistid, trackid}, mandatoryParameters);
		if (await validator.err) {
			res.status(ResponseStatusTypes.BAD_REQUEST).send(await validator.err);			
			return ;
		}

		const user: User = await User.get({apiKey});
		if (!user) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.INVALID_API_KEY));
			return ;
		}
		if (!user.activated) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.USER_ACCOUNT_NOT_ACTIVATED));
			return ;
		}

		await acquireLock(lockKeys.playlist(playlistid), async () => {
			const playlist: Playlist = await Playlist.get({id: playlistid});
			if (!playlist) {
				res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_PLAYLIST));
				return ;
			}

			if (!playlist.userCanEdit(user)) {
				res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.EDIT_NOT_AUTHORIZED));
				return ;
			}

			const updated: boolean = playlist.removeTrack(trackid);
			if (!updated) {
				res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_TRACK));
				return ;
			}
			await playlist.save();
			res.status(ResponseStatusTypes.OK).send({...playlist, message: 'Track removed'});
			socketController.emitToRoom('/playlists', playlist.id, SocketActions.PLAYLIST_REMOVE_TRACK, trackid);
		});
		return ;
	}

	public async addEditor(req: Request, res: Response) : Promise<any> {
		const mandatoryParameters: Parameter[] = [new Parameter('apiKey', 'query'), new Parameter('playlistid', 'param'), req.params.userid ? new Parameter('userid', 'param'): new Parameter('userids', 'body')];

		const apiKey: string = req.query.apiKey;
		const playlistid: string = req.params.playlistid;
		const userid: string = req.params.userid;
		let   userids: string[] = req.body.userids;

		const validator: Validator = new Validator({apiKey, playlistid, userid, userids}, mandatoryParameters);
		if (await validator.err) {
			res.status(ResponseStatusTypes.BAD_REQUEST).send(await validator.err);			
			return ;
		}

		// check if user exists and account is activated
		let user: User = await User.get({apiKey});
		if (!user) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.INVALID_API_KEY));
			return ;
		}
		if (!user.activated) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.USER_ACCOUNT_NOT_ACTIVATED));
			return ;
		}

		userids = userid ? [userid] : userids;
		// check if users exist
		let users: User[] = await Promise.all(userids.map(async id => await User.get({id})));
		if (!users.every(x => !!x === true)) {
			res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_USER));
			return ;
		}
		await acquireLock([lockKeys.playlist(playlistid), ...userids.map(id => lockKeys.user(id))], async () => {
			// check that playlist exists
			const playlist: Playlist = await Playlist.get({id: playlistid});
			if (!playlist) {
				res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_PLAYLIST));
				return ;
			}
			// only playlist owner can add editors
			if (!playlist.userIsOwner(user)) {
				res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.EDIT_NOT_AUTHORIZED));
				return ;
			}
			// get users to add as editors
			users = await Promise.all(userids.map(async id => await User.get({id})));
			if (!users.every(x => !!x === true)) {
				res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_USER));
				return ;
			}
			// check that owner is not in list of users (owner cannot be guest or editor)
			if (users.some(user => playlist.userIsOwner(user))) {
				res.status(ResponseStatusTypes.BAD_REQUEST).send(new ServerError(ErrorCodes.CANNOT_ADD_OWNER_AS_EDITOR_IN_PLAYLIST))
				return ;
			}
			// add editors from playlist and playlist to editors
			let updated = [
				...users.map(user => playlist.addEditor(user.id)),
				...users.map(user => user.addEditorInPlaylist(playlist.id))
			];
			if (updated.every(x => x == false)) {
				res.status(ResponseStatusTypes.OK).send({message: 'No change'});
				return ;
			}
			// save editors and playlist
			await Promise.all([
				...users.map(user => user.save()),
				playlist.save()
			]);
			socketController.emitToRoom('/playlists', playlist.id, SocketActions.PLAYLIST_ADD_EDITOR, {userids});
			res.status(ResponseStatusTypes.OK).send({...playlist, message: (userid ? 'editor' : 'editors') + ' added'});					
			await socketController.updateConnections("/playlists", playlist.id);
		});
	}

	public async removeEditor(req: Request, res: Response) : Promise<any> {
		const mandatoryParameters: Parameter[] = [new Parameter('apiKey', 'query'), new Parameter('playlistid', 'param'), req.params.userid ? new Parameter('userid', 'param'): new Parameter('userids', 'body')];

		const apiKey: string = req.query.apiKey;
		const playlistid: string = req.params.playlistid;
		const userid: string = req.params.userid;
		let   userids: string[] = req.body.userids;

		const validator: Validator = new Validator({apiKey, playlistid, userid, userids}, mandatoryParameters);
		if (await validator.err) {
			res.status(ResponseStatusTypes.BAD_REQUEST).send(await validator.err);			
			return ;
		}

		// check if user exists and account is activated
		let user: User = await User.get({apiKey});
		if (!user) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.INVALID_API_KEY));
			return ;
		}
		if (!user.activated) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.USER_ACCOUNT_NOT_ACTIVATED));
			return ;
		}
		
		userids = userid ? [userid] : userids;
		// check that users exist
		let users: User[] = await Promise.all(userids.map(async id => await User.get({id})));
		if (!users.every(x => !!x === true)) {
			res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_USER));
			return ;
		}
		await acquireLock([lockKeys.playlist(playlistid), ...userids.map(id => lockKeys.user(id))], async () => {
			// check that playlist exists
			const playlist: Playlist = await Playlist.get({id: playlistid});
			if (!playlist) {
				res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_PLAYLIST));
				return ;
			}
			// only owner can remove editors OR editor can remove self and only self
			if (!playlist.userIsOwner(user) && !(playlist.userIsEditor(user) && userids.length == 1 && userids[0] == user.id)) {
				res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.EDIT_NOT_AUTHORIZED));
				return ;
			}
			// get editors
			users = await Promise.all(userids.map(async id => await User.get({id})));
			if (!users.every(x => !!x === true)) {
				res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_USER));
				return ;
			}
			// remove editors from playlist and playlist from editors
			let updated = [
				...users.map(user => playlist.removeEditor(user.id)),
				...users.map(user => user.removeEditorInPlaylist(playlist.id))
			];
			if (updated.every(x => x == false)) {
				res.status(ResponseStatusTypes.OK).send({message: 'No change'});
				return ;
			}
			// save editors and playlist
			await Promise.all([
				...users.map(user => user.save()),
				playlist.save()
			]);
			res.status(ResponseStatusTypes.OK).send({...playlist, message: (userid ? 'editor' : 'editors') + ' removed'});
			socketController.emitToRoom('/playlists', playlist.id, SocketActions.PLAYLIST_REMOVE_EDITOR, {userids});
			await socketController.updateConnections("/playlists", playlist.id);
		});
	}

	public async addGuest(req: Request, res: Response) : Promise<any> {
		const mandatoryParameters: Parameter[] = [new Parameter('apiKey', 'query'), new Parameter('playlistid', 'param'), req.params.userid ? new Parameter('userid', 'param'): new Parameter('userids', 'body')];

		const apiKey: string = req.query.apiKey;
		const playlistid: string = req.params.playlistid;
		const userid: string = req.params.userid;
		let   userids: string[] = req.body.userids;

		const validator: Validator = new Validator({apiKey, playlistid, userid, userids}, mandatoryParameters);
		if (await validator.err) {
			res.status(ResponseStatusTypes.BAD_REQUEST).send(await validator.err);			
			return ;
		}
		// check if user exists and account is activated
		let user: User = await User.get({apiKey});
		if (!user) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.INVALID_API_KEY));
			return ;
		}
		if (!user.activated) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.USER_ACCOUNT_NOT_ACTIVATED));
			return ;
		}

		userids = userid ? [userid] : userids;
		// check if users exist
		let users: User[] = await Promise.all(userids.map(async id => await User.get({id})));
		if (!users.every(x => !!x === true)) {
			res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_USER));
			return ;
		}
		await acquireLock([lockKeys.playlist(playlistid), ...userids.map(id => lockKeys.user(id))], async () => {
			// check that playlist exists
			const playlist: Playlist = await Playlist.get({id: playlistid});
			if (!playlist) {
				res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_PLAYLIST));
				return ;
			}
			// only playlist owner can add guests
			if (!playlist.userIsOwner(user)) {
				res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.EDIT_NOT_AUTHORIZED));
				return ;
			}
			// get users to add as guests
			users = await Promise.all(userids.map(async id => await User.get({id})));
			if (!users.every(x => !!x === true)) {
				res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_USER));
				return ;
			}
			// check that owner is not in list of users (owner cannot be guest or editor)
			if (users.some(user => playlist.userIsOwner(user))) {
				res.status(ResponseStatusTypes.BAD_REQUEST).send(new ServerError(ErrorCodes.CANNOT_ADD_OWNER_AS_GUEST_IN_PLAYLIST))
				return ;
			}
			// add guests from playlist and playlist to guests
			let updated = [
				...users.map(user => playlist.addGuest(user.id)),
				...users.map(user => user.addGuestInPlaylist(playlist.id))
			];
			if (updated.every(x => x == false)) {
				res.status(ResponseStatusTypes.OK).send({message: 'No change'});
				return ;
			}
			// save guests and playlist
			await Promise.all([
				...users.map(user => user.save()),
				playlist.save()
			]);
			socketController.emitToRoom('/playlists', playlist.id, SocketActions.PLAYLIST_ADD_GUEST, {userids});
			res.status(ResponseStatusTypes.OK).send({...playlist, message: (userid ? 'guest' : 'guests') + ' added'});
			await socketController.updateConnections("/playlists", playlist.id);					
		});
	}

	public async removeGuest(req: Request, res: Response) : Promise<any> {
		const mandatoryParameters: Parameter[] = [new Parameter('apiKey', 'query'), new Parameter('playlistid', 'param'), req.params.userid ? new Parameter('userid', 'param'): new Parameter('userids', 'body')];

		const apiKey: string = req.query.apiKey;
		const playlistid: string = req.params.playlistid;
		const userid: string = req.params.userid;
		let   userids: string[] = req.body.userids;

		const validator: Validator = new Validator({apiKey, playlistid, userid, userids}, mandatoryParameters);
		if (await validator.err) {
			res.status(ResponseStatusTypes.BAD_REQUEST).send(await validator.err);			
			return ;
		}

		// check if user exists and account is activated
		let user: User = await User.get({apiKey});
		if (!user) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.INVALID_API_KEY));
			return ;
		}
		if (!user.activated) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.USER_ACCOUNT_NOT_ACTIVATED));
			return ;
		}

		userids = userid ? [userid] : userids;
		// check if users exist
		let users: User[] = await Promise.all(userids.map(async id => await User.get({id})));
		if (!users.every(x => !!x === true)) {
			res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_USER));
			return ;
		}
		await acquireLock([lockKeys.playlist(playlistid), ...userids.map(id => lockKeys.user(id))], async () => {
			// check that playlist exists
			const playlist: Playlist = await Playlist.get({id: playlistid});
			if (!playlist) {
				res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_PLAYLIST));
				return ;
			}
			// only owner can remove guests OR guest can remove self and only self
			if (!playlist.userIsOwner(user) && !(playlist.userIsGuest(user) && userids.length == 1 && userids[0] == user.id)) {
				res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.EDIT_NOT_AUTHORIZED));
				return ;
			}
			// get guests
			users = await Promise.all(userids.map(async id => await User.get({id})));
			if (!users.every(x => !!x === true)) {
				res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_USER));
				return ;
			}
			// removed guests from playlist and playlist from guests
			let updated = [
				...users.map(user => playlist.removeGuest(user.id)),
				...users.map(user => user.removeGuestInPlaylist(playlist.id))
			];
			if (updated.every(x => x == false)) {
				res.status(ResponseStatusTypes.OK).send({message: 'No change'});
				return ;
			}
			// save guests and playlist
			await Promise.all([
				...users.map(user => user.save()),
				playlist.save()
			]);
			socketController.emitToRoom('/playlists', playlist.id, SocketActions.PLAYLIST_REMOVE_GUEST, {userids});
			res.status(ResponseStatusTypes.OK).send({...playlist, message: (userid ? 'guest' : 'guests') + ' removed'});					
			await socketController.updateConnections("/playlists", playlist.id);
		});
	}

	public async moveTrack(req: Request, res: Response) : Promise<any> {
		const mandatoryParameters: Parameter[] = [new Parameter('apiKey', 'query'), new Parameter('playlistid', 'param'), new Parameter('trackid', 'param'), new Parameter('index', 'body')];
	
		const apiKey: string = req.query.apiKey;
		const playlistid: string = req.params.playlistid;
		const trackid: string = req.params.trackid;
		const index: string = req.body.index;

		const validator: Validator = new Validator({apiKey, playlistid, trackid, index}, mandatoryParameters);
		if (await validator.err) {
			res.status(ResponseStatusTypes.BAD_REQUEST).send(await validator.err);			
			return ;
		}

		// check if user exists and account is activated
		let user: User = await User.get({apiKey});
		if (!user) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.INVALID_API_KEY));
			return ;
		}
		if (!user.activated) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.USER_ACCOUNT_NOT_ACTIVATED));
			return ;
		}

		await acquireLock(lockKeys.playlist(playlistid), async () => {
			const playlist: Playlist = await Playlist.get({id: playlistid});
			if (!playlist) {
				res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_PLAYLIST));
				return ;
			}
			
			if (!playlist.userCanEdit(user)) {
				res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.EDIT_NOT_AUTHORIZED));
				return ;
			}
			if (!playlist.tracks.find(track => track.id == trackid)) {
				res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_TRACK));
				return ; 
			}

			const updated: boolean = playlist.moveTrack(trackid, index);
			if (!updated) {
				res.status(ResponseStatusTypes.OK).send({message: 'No change'});
				return ;
			}
			await playlist.save();
			socketController.emitToRoom('/playlists', playlist.id, SocketActions.PLAYLIST_MOVE_TRACK, {trackid, index});
			res.status(ResponseStatusTypes.OK).send({...playlist, message: 'Track moved'});					
		});
	}

	public async duplicate(req: Request, res: Response) : Promise<any> {
		const mandatoryParameters: Parameter[] = [new Parameter('apiKey', 'query'), new Parameter('playlistid', 'param')];
	
		const apiKey: string = req.query.apiKey;
		const playlistid: string = req.params.playlistid;
		const copyGuests: string = req.body.copyGuests;
		const copyEditors: string = req.body.copyEditors;

		const validator: Validator = new Validator({apiKey, playlistid, copyGuests, copyEditors}, mandatoryParameters);
		if (await validator.err) {
			res.status(ResponseStatusTypes.BAD_REQUEST).send(await validator.err);			
			return ;
		}

		// check if user exists and account is activated and user is premium
		let user: User = await User.get({apiKey});
		if (!user) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.INVALID_API_KEY));
			return ;
		}
		if (!user.activated) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.USER_ACCOUNT_NOT_ACTIVATED));
			return ;
		}
		if (!user.isPremium) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.USER_NOT_PREMIUM));
			return ;
		}

		await acquireLock([lockKeys.playlist('create'), lockKeys.user(user.id)], async () => {
			user = await User.get({apiKey});
			let playlist: Playlist = await Playlist.get({id: playlistid});
			if (!playlist) {
				res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_PLAYLIST));
				return ;
			}
			// check if user has access rights to playlist
			if (!playlist.userCanAccess(user)) {
				res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.ACCESS_NOT_AUTHORIZED));
				return ;
			}
			let newPlaylist: Playlist = await playlist.duplicate(user.id, {copyEditors: copyEditors && playlist.userIsOwner(user), copyGuests: copyGuests && playlist.userIsOwner(user)});
			user.addPlaylist(newPlaylist.id);
			await Promise.all([
				newPlaylist.create(),
				user.save()
			]);
			res.status(ResponseStatusTypes.OK).send({...newPlaylist, message: 'Playlist duplicated'});
		});
	}

	public async search(req: Request, res: Response) : Promise<any> {
		const mandatoryParameters: Parameter[] = [new Parameter('apiKey', 'query'), new Parameter('keyword', 'query')];

		const apiKey: string = req.query.apiKey;
		const keyword: string = req.query.keyword;

		const validator: Validator = new Validator({apiKey, keyword}, mandatoryParameters);
		if (await validator.err) {
			res.status(ResponseStatusTypes.BAD_REQUEST).send(await validator.err);			
			return ;
		}

		const user = await User.get({apiKey: apiKey});
		if (!user) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.INVALID_API_KEY));
			return ;
		}
		if (!user.activated) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.USER_ACCOUNT_NOT_ACTIVATED));
			return ;
		}

		let playlists: Playlist[] = await Playlist.search(keyword);
		
		res.status(ResponseStatusTypes.OK).send({results: playlists});
		return ;
	}

	public async delete(req: Request, res: Response) : Promise<any> {
		const mandatoryParameters: Parameter[] = [new Parameter('apiKey', 'query'), req.params.playlistid ? new Parameter('playlistid', 'param') : new Parameter('playlistids', 'body')];

		const apiKey: string = req.query.apiKey;
		const playlistid: string = req.params.playlistid;
		let   playlistids: string[] = req.body.playlistids;

		const validator: Validator = new Validator({apiKey, playlistid, playlistids}, mandatoryParameters);
		if (await validator.err) {
			res.status(ResponseStatusTypes.BAD_REQUEST).send(await validator.err);			
			return ;
		}

		// check if user exists and account is activated
		let user: User = await User.get({apiKey});
				
		if (!user) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.INVALID_API_KEY));
			return ;
		}
		if (!user.activated) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.USER_ACCOUNT_NOT_ACTIVATED));
			return ;
		}

		playlistids = playlistid ? [playlistid] : playlistids;
		// check that playlists exist
		let playlists: Playlist[] = await Promise.all(playlistids.map((id: string) => Playlist.get({id})));
		if (!playlists.every(x => !!x === true)) {
			res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_PLAYLIST));
			return ;
		}
		await acquireLock(playlistids.map(id => lockKeys.playlist(id)), async () => {
			// get playlists
			playlists = await Promise.all(playlistids.map((id: string) => Playlist.get({id})));
			if (!playlists.every(x => !!x === true)) {
				res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_PLAYLIST));
				return ;
			}
			// check if user is owner of all playlists
			if (playlists.map(playlist => playlist.userIsOwner(user)).some(x => x == false)) {
				res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.EDIT_NOT_AUTHORIZED));
				return ;
			}
			
			await Promise.all(playlists.map(playlist => playlist.delete()));
			res.status(ResponseStatusTypes.OK).send({message: `Playlist${playlists.length == 1 ? '' : 's'} deleted`});
			await Promise.all(playlists.map(async playlist => {
				await socketController.emitToRoom('/playlists', playlist.id, SocketActions.PLAYLIST_DELETED);
				await socketController.updateConnections('/playlists', playlist.id);
			}));
		});
	}

	public initSocketIO(ioServer: socketio.Server) {
		io = ioServer.of('/playlists');
		io.on('connection', async function(socket) {
			const mandatoryParameters: Parameter[] = [new Parameter('apiKey', 'query'), new Parameter('playlistid', 'query')];
			
			const apiKey: string = socket.handshake.query.apiKey;
			const playlistid: string = socket.handshake.query.playlistid;
			
			const validator: Validator = new Validator({apiKey, playlistid}, mandatoryParameters);
			if (await validator.err) {	
				socket.emit(SocketActions.ERROR, await validator.err);
				socket.disconnect();
				return ;
			}
			
			const user: User = await User.get({apiKey});
			if (!user) {
				socket.emit(SocketActions.ERROR, new ServerError(ErrorCodes.INVALID_API_KEY));
				socket.disconnect();
				return ;
			}
			if (!user.activated) {
				socket.emit(SocketActions.ERROR, new ServerError(ErrorCodes.USER_ACCOUNT_NOT_ACTIVATED));
				socket.disconnect();
				return ;
			}
			
			const playlist = await Playlist.get({id: playlistid});
			if (!playlist) {
				socket.emit(SocketActions.ERROR, new ServerError(ErrorCodes.NO_PLAYLIST));
				socket.disconnect();
				return ;
			}
			
			if (!playlist.userCanAccess(user)) {
				socket.emit(SocketActions.ERROR, new ServerError(ErrorCodes.ACCESS_NOT_AUTHORIZED));
				socket.disconnect();
				return ;
			}
			
			let isRegistered = await socketController.registerClient('/playlists', playlist.id, user.id, socket, true);
			
			socket.on('disconnect', async () => {
				await isRegistered;
				socketController.unregisterClient(socket.id);
			});
			socket.on(SocketActions.REQUEST_DISCONNECT, async () => {
				await isRegistered;
				socket.disconnect();
			});
		});
	}
}

const playlistController: PlaylistController = new PlaylistController();
export {
	PlaylistController,
	playlistController
}