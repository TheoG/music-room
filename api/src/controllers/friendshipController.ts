import { Request, Response } from "express";

import ResponseStatusTypes from "../utils/ResponseStatusTypes";
import { ErrorCodes, ServerError } from "../utils/errorTypes";
import { Friendship } from "../classes/Friendship";
import { User } from "../classes/User";
import { acquireLock, lockKeys } from "../utils/mutex";
import { Parameter, Validator } from "../classes/Validator";

class FriendshipController {
	
	public async create(req: Request, res: Response) : Promise<any> {
		const mandatoryParameters: Parameter[] = [new Parameter('apiKey', 'query'), new Parameter('id', 'body'), new Parameter('message', 'body')];
		
		const apiKey: string = req.query.apiKey;
		const id: string = req.body.id;
		const message: string = req.body.message	

		const validator: Validator = new Validator({apiKey, id, message}, mandatoryParameters);
		if (await validator.err) {
			res.status(ResponseStatusTypes.BAD_REQUEST).send(await validator.err);
			return ;
		}
		
		// check if user exists and account is activated
		let user: User = await User.get({apiKey});
		if (!user) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.INVALID_API_KEY));
			return ;
		}
		if (!user.activated) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.USER_ACCOUNT_NOT_ACTIVATED));
			return ;
		}
		// check if friend exists, account is activated, and friend is not user
		let friend: User = await User.get({id});
		if (!friend) {
			res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_USER, 'Friend with id ' + id + ' does not exist'));
			return ;
		}
		if (friend.id === user.id) {
			res.status(ResponseStatusTypes.BAD_REQUEST).send(new ServerError(ErrorCodes.CANNOT_ADD_SELF_AS_FRIEND));
			return ;
		}
		if (!friend.activated) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.USER_ACCOUNT_NOT_ACTIVATED));
			return ;
		}
		// check an fiendship does not already exist between them
		const existingFriendship = await Friendship.get({
			users: { $all: [friend.id, user.id] },
		});
		if (existingFriendship) {
			res.status(ResponseStatusTypes.BAD_REQUEST).send(new ServerError(ErrorCodes.FRIENDSHIP_EXISTS));
			return ;
		}
		await acquireLock([lockKeys.friendship('create'), lockKeys.user(user.id), lockKeys.user(friend.id)], async () => {
			// get user again
			user = await User.get({apiKey});
			if (!user) {
				res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.NO_USER, 'User with apiKey ' + apiKey + ' does not exist'));
				return ;
			}
			// get friend again
			friend = await User.get({id});
			if (!friend) {
				res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_USER, 'Friend with id ' + id + ' does not exist'));
				return ;
			}
			// create friendship 
			const friendship: Friendship = new Friendship({
				to: friend.id,
				from: user.id,
				message
			})
			// add friendships to users
			user.addFriendship(friendship.id);
			friend.addFriendship(friendship.id);
			// save friendship, user, and friend
			await Promise.all([friendship.create(), user.save(), friend.save()]);
			res.status(ResponseStatusTypes.OK).send({...friendship, message: 'Friendship created'});
		});
	}
	
	public async get(req: Request, res: Response) : Promise<any> {
		const mandatoryParameters: Parameter[] = [new Parameter('apiKey', 'query')];

		const apiKey: string = req.query.apiKey;
		const id: string = req.params.friendshipid;
		
		const validator: Validator = new Validator({apiKey, friendshipid: id}, mandatoryParameters);
		if (await validator.err) {
			res.status(ResponseStatusTypes.BAD_REQUEST).send(await validator.err);
			return ;
		}

		// check if user exists and account is activated
		const user: User = await User.get({apiKey});
		if (!user) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.INVALID_API_KEY));
			return ;
		}
		if (!user.activated) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.USER_ACCOUNT_NOT_ACTIVATED));
			return ;
		}

		if (id) {
			const friendship: Friendship = await Friendship.get({id});
			if (!friendship) {
				res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_FRIENDSHIP, 'Friendship with id ' + id + ' does not exist'));
				return ;
			}
			let ret = await replaceFriendshipUserIdsWithUsers(friendship);
			res.status(ResponseStatusTypes.OK).send(ret);
			return ;
		} else {
			let ret = user.friendships.map(async (id: string) => {
				const friendship: Friendship = await Friendship.get({id});				
				return replaceFriendshipUserIdsWithUsers(friendship);
			});
			ret = await Promise.all(ret);
			res.status(ResponseStatusTypes.OK).send({friendships: ret});
			return ;
		}

		async function replaceFriendshipUserIdsWithUsers(friendship: Friendship): Promise<any> {
			// TODO: handle error ?
			let ret: any = friendship.toPlainObj();
			ret.users = await Promise.all(ret.users.map(async id => await (await User.get({id})).filterByRelationship(user)));
			return ret;
		}
	}

	public async update(req: Request, res: Response) : Promise<any> {
		const mandatoryParameters: Parameter[] = [new Parameter('apiKey', 'query'), new Parameter('friendshipid', 'param'), new Parameter('friendshipaction', 'param')];

		const apiKey: string = req.query.apiKey;
		const id: string = req.params.friendshipid;
		const friendshipaction: string = req.params.friendshipaction;
		
		const validator: Validator = new Validator({apiKey, friendshipid: id, friendshipaction}, mandatoryParameters);
		if (await validator.err) {
			res.status(ResponseStatusTypes.BAD_REQUEST).send(await validator.err);
			return ;
		}

		// do not need to lock user id, since only getting user here.
		await acquireLock(lockKeys.friendship(id), async () => {
			// friendship to update
			const friendship: Friendship = await Friendship.get({id: id});
			if (!friendship) {
				res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_FRIENDSHIP, 'Friendship with id ' + id + ' does not exist'));
				return ;
			}
			if (!friendship.pending) {
				res.status(ResponseStatusTypes.BAD_REQUEST).send(new ServerError(ErrorCodes.FRIENDSHIP_NOT_PENDING));
				return ;
			}

			// check that user exists and account is activated
			let user: User = await User.get({apiKey});
			if (!user) {
				res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.NO_USER, 'User with apiKey ' + apiKey + ' does not exist'));
				return ;
			}
			if (!user.activated) {
				res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.USER_ACCOUNT_NOT_ACTIVATED));
				return ;
			}
			
			// check that friend exists
			let friend: User = await User.get({id: friendship.users.find(x => x != user.id)});
			if (!friend) {
				res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.NO_USER));
				return ;
			}
			// user request update must be 'to' user
			if (user.id != friendship.request.to) {
				res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.USER_CANNOT_TAKE_ACTION_ON_FRIENDSHIP));
				return ;
			}
			switch (friendshipaction) {
				case 'read':
					friendship.read();
					await friendship.save();
					break;
				case 'accept':
					await Promise.all([
						friendship.accept(),
						acquireLock(lockKeys.user(user.id  ), async () => { user   = await User.get({id: user  .id}); if (!user  ) {return ;} await user.addFriend(friend.id); await user  .save(); }),
						acquireLock(lockKeys.user(friend.id), async () => { friend = await User.get({id: friend.id}); if (!friend) {return ;} await friend.addFriend(user.id); await friend.save(); }),
						friendship.save()
					]);
					break;
				case 'reject':
					await friendship.delete();
					break;
				default:
					res.status(ResponseStatusTypes.BAD_REQUEST).send(new ServerError(ErrorCodes.INVALID_FRIENDSHIP_ACTION));
					return ;
			}
			res.status(ResponseStatusTypes.OK).send({...friendship, message: 'Friendship updated'});
			return ;
		});
	}

	public async delete(req: Request, res: Response) : Promise<any> {
		const mandatoryParameters: Parameter[] = [new Parameter('apiKey', 'query'), new Parameter('friendshipid', 'body')];
		
		const apiKey: string = req.query.apiKey;
		const id: string = req.params.friendshipid;
		
		const validator: Validator = new Validator({apiKey, friendshipid: id}, mandatoryParameters);
		if (await validator.err) {
			res.status(ResponseStatusTypes.BAD_REQUEST).send(await validator.err);
			return ;
		}

		const user: User = await User.get({apiKey});
		if (!user) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.INVALID_API_KEY));
			return ;
		}
		if (!user.activated) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.USER_ACCOUNT_NOT_ACTIVATED));
			return ;
		}

		// lock friendship resource
		await acquireLock(lockKeys.friendship(id), async () => {
			const friendship: Friendship = await Friendship.get({id});
			if (!friendship) {
				res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_FRIENDSHIP, 'Friendship with id ' + id + ' does not exist'));
				return ;
			}
			if (!friendship.users.includes(user.id)) {
				res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.EDIT_NOT_AUTHORIZED, 'This isn\'t this user\'s friendship!'));
				return ;
			}

			await friendship.delete();
			res.status(ResponseStatusTypes.OK).send({message: 'Friendship deleted'});
			return ;
		});
	}
}

const friendshipController: FriendshipController = new FriendshipController();
export {
	FriendshipController,
	friendshipController
};