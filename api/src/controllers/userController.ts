import { Request, Response, response } from "express";

import { User } from "../classes/User";
import ResponseStatusTypes from "../utils/ResponseStatusTypes";
import { ErrorCodes, ServerError } from "../utils/errorTypes";
import SettingsController from "./settingsController";
import { MasterSetting, Setting } from "../classes/Setting"; 
import { acquireLock, lockKeys } from "../utils/mutex";
import { Parameter, Validator } from "../classes/Validator";
import { Theme } from "../types/types";
import { sendEmail } from "../utils/email";
import * as request from "request-promise-native";
import chalk from "chalk";
import { env } from "../utils/env";

class UserController {
	public async get(req: Request, res: Response) : Promise<any> {
		const mandatoryParameters: Parameter[] = [new Parameter('apiKey', 'query'), new Parameter('id', 'param')];
		
		const apiKey: string = req.query.apiKey;
		const id: string = req.params.id;
		
		const validator: Validator = new Validator({apiKey, id}, mandatoryParameters);
		if (await validator.err) {
			res.status(ResponseStatusTypes.BAD_REQUEST).send(await validator.err);
			return ;
		}

		const user: User = await User.get({apiKey});
		if (!user) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.INVALID_API_KEY));
			return ;
		}
		if (!user.activated) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.USER_ACCOUNT_NOT_ACTIVATED));
			return ;
		}

		const queriedUser: User = await User.get({id});
		if (!queriedUser) {
			res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_USER));
			return ;
		}
		res.status(ResponseStatusTypes.OK).send(queriedUser.filterByRelationship(user));
		return ;
	}
	
	public async createWithEmail(req: Request, res: Response) : Promise<any> {
		const mandatoryParameters: Parameter[] = [new Parameter('email', 'body'), new Parameter('password', 'body')];

		const email: string = req.body.email;
		const password: string = req.body.password;

		const displayName: string = req.body.displayName;
		const firstName: string = req.body.firstName;
		const lastName: string = req.body.lastName;
		const theme: Theme = req.body.theme;

		const attributes: any = {email, password, displayName, firstName, lastName, theme};

		const validator: Validator = new Validator(attributes, mandatoryParameters);
		if (await validator.err) {
			res.status(ResponseStatusTypes.BAD_REQUEST).send(await validator.err);
			return ;
		}

		await userController.create(req, res, {email}, attributes);
	}

	async loginWithEmail(req: Request, res: Response) : Promise<any> {
		const mandatoryParameters: Parameter[] = [new Parameter('email', 'body'), new Parameter('password', 'body')];
		
		const email: string = req.body.email;
		const password: string = req.body.password;

		const validator: Validator = new Validator({email, password}, mandatoryParameters);
		if (await validator.err) {
			res.status(ResponseStatusTypes.BAD_REQUEST).send(await validator.err);
			return ;
		}

		await userController.login(req, res, {email, password});
	}

	async resetPassword(req: Request, res: Response) : Promise<any> {
		const mandatoryParameters: Parameter[] = [new Parameter('email', 'body')];
		
		const email: string = req.body.email;
		const code: string = req.body.code;
		const password: string = req.body.password;

		const validator: Validator = new Validator({email, code, password}, mandatoryParameters);
		if (await validator.err) {
			res.status(ResponseStatusTypes.BAD_REQUEST).send(await validator.err);
			return ;
		}

		let user: User = await User.get({email});

		if (!user) {
			res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_USER));
			return ;
		}
		if (!user.activated) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.USER_ACCOUNT_NOT_ACTIVATED));
			return ;
		}
		if (code || password) {
			if (!code) {
				res.status(ResponseStatusTypes.BAD_REQUEST).send(new ServerError(ErrorCodes.MISSING_PARAMETER, null, 'code', 'body'));
				return ;				
			}
			if (!password) {
				res.status(ResponseStatusTypes.BAD_REQUEST).send(new ServerError(ErrorCodes.MISSING_PARAMETER, null, 'password', 'body'));
				return ;				
			}
			// check if code exists
			if (!user.passwordResetCode) {
				res.status(ResponseStatusTypes.BAD_REQUEST).send({message: 'Please request a password activation code first'});
				return ;
			}
			// check if attempts remaining
			if (user.passwordResetAttemptsRemaining != null && user.passwordResetAttemptsRemaining <= 0) {
				res.status(ResponseStatusTypes.UNAUTHORIZED).send({message: 'No more password reset attempts remaining ...'});
				return ;
			}
			await acquireLock(lockKeys.user(user.id), async () => {
				const user: User = await User.get({email});
				if (!user) {
					res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_USER));
					return ;
				}

				// check if code matches
				if (user.passwordResetCode != code) {
					user.passwordResetAttemptsRemaining--;
					await user.save();				
					res.status(ResponseStatusTypes.UNAUTHORIZED).send({message: `Invalid code. Attempts remaining: ${user.passwordResetAttemptsRemaining}`});
					return ;
				}

				// reset password
				user.resetPassword(password);
				await user.save();
				res.status(ResponseStatusTypes.OK).send({message: `Password updated`});
			});
		} else {
			// check if code already exists and reset email
			if (!user.passwordResetCode) {
				await acquireLock(lockKeys.user(user.id), async () => {
					user = await User.get({email});
					if (!user) {
						res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_USER));
						return ;
					}
					// generate code
					user.passwordResetCode = Math.floor(Math.random() * (100000 - 10000) + 10000).toString();
					user.passwordResetAttemptsRemaining = 3;
					await user.save();
				});
			}
			// check if attempts remaining
			if (user.passwordResetAttemptsRemaining != null && user.passwordResetAttemptsRemaining <= 0) {
				res.status(ResponseStatusTypes.UNAUTHORIZED).send({message: 'No more password reset attempts remaining ...'});
				return ;
			}
			await user.sendPasswordResetEmail();
			res.status(ResponseStatusTypes.OK).send({message: 'sent new password reset code'});
			return ;
		}
	}

	async update(req: Request, res: Response) : Promise<any> {
		const mandatoryParameters: Parameter[] = [new Parameter('apiKey', 'query'), new Parameter('id', 'param')];
		
		const apiKey: string = req.query.apiKey;
		const id: string = req.params.id;
		
		const password: string = req.body.password;
		const displayName: string = req.body.displayName;
		const firstName: string = req.body.firstName;
		const lastName: string = req.body.lastName;
		const theme: Theme = req.body.theme;
		const musicalPreferences: string = req.body.musicalPreferences;
		const avatar: string = req.file ? req.file.path : undefined;

		const attributesToUpdate: any = {password, displayName, firstName, lastName, theme, musicalPreferences, avatar};

		const validator: Validator = new Validator({apiKey, id, ...attributesToUpdate}, mandatoryParameters);
		if (await validator.err) {
			res.status(ResponseStatusTypes.BAD_REQUEST).send(await validator.err);
			return ;
		}
		
		await acquireLock(lockKeys.user(id), async () => {
			const user: User = await User.get({apiKey});
			if (!user) {
				res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.INVALID_API_KEY));
				return ;
			}
			if (!user.activated) {
				res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.USER_ACCOUNT_NOT_ACTIVATED));
				return ;
			}
			
			await user.update(attributesToUpdate);
			await user.save();
			res.status(ResponseStatusTypes.OK).send({...user.filterByRelationship(user), message: 'Update success'});
		});
	}

	async delete(req: Request, res: Response) : Promise<any> {
		const mandatoryParameters: Parameter[] = [new Parameter('apiKey', 'query'), new Parameter('userid', 'param')];

		const apiKey: string = req.query.apiKey;		
		const userid: string = req.params.userid;

		const validator: Validator = new Validator({apiKey, userid}, mandatoryParameters);
		if (await validator.err) {
			res.status(ResponseStatusTypes.BAD_REQUEST).send(await validator.err);
			return ;
		}

		const user: User = await User.get({apiKey});
		if (!user) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.INVALID_API_KEY));
			return ;
		}
		// NOTE: Can delete own account even if not validated

		if (user.id != userid) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.EDIT_NOT_AUTHORIZED));
			return ;
		}
		
		await user.delete();
		res.status(ResponseStatusTypes.OK).send({message: 'Delete success'});
	}

	async search(req: Request, res: Response) : Promise<any> {
		const mandatoryParameters: Parameter[] = [new Parameter('apiKey', 'query'), new Parameter('keyword', 'query')];

		const apiKey: string = req.query.apiKey;
		const keyword: string = req.query.keyword;

		const validator: Validator = new Validator({apiKey, keyword}, mandatoryParameters);
		if (await validator.err) {
			res.status(ResponseStatusTypes.BAD_REQUEST).send(await validator.err);
			return ;
		}

		const user = await User.get({apiKey: apiKey});
		if (!user) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.INVALID_API_KEY));
			return ;
		}
		if (!user.activated) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.USER_ACCOUNT_NOT_ACTIVATED));
			return ;
		}

		let users: User[] = await User.search(apiKey, keyword);
		
		const results = users.map(((x: User) => x.filterByRelationship(user)));
		
		res.status(ResponseStatusTypes.OK).send({results});
		return ;
	}

	public async getSettings(req: Request, res: Response) : Promise<any> {
		const mandatoryParameters: Parameter[] = [new Parameter('apiKey', 'query'), new Parameter('userid', 'param')];

		const apiKey: string = req.query.apiKey;
		const userid: string = req.params.userid;
		
		const validator: Validator = new Validator({apiKey, userid}, mandatoryParameters);
		if (await validator.err) {
			res.status(ResponseStatusTypes.BAD_REQUEST).send(await validator.err);
			return ;
		}

		const user: User = await User.get({apiKey});
		if (!user) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.INVALID_API_KEY));
			return ;
		}
		if (!user.activated) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.USER_ACCOUNT_NOT_ACTIVATED));
			return ;
		}
		if (user.id != userid) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.EDIT_NOT_AUTHORIZED));
			return ;
		}

		res.status(ResponseStatusTypes.OK).send({
			...(user.settings.reduce((o: any, setting: Setting) : any => {
				o[setting.name] = setting.value;
				return o;
			}, {})),
			availableSettings: SettingsController.masterSettings.reduce((o: any, setting: MasterSetting) : any => {
				o[setting.name] = setting.values;
				return o;
			}, {})
		});
		return ;
	}

	public async updateSettings(req: Request, res: Response) : Promise<any> {
		const mandatoryParameters: Parameter[] = [new Parameter('apiKey', 'query'), new Parameter('userid', 'param')];

		const apiKey: string = req.query.apiKey;
		const userid: string = req.params.userid;

		const email: string = req.body.email;
		const displayName: string = req.body.displayName;
		const firstName: string = req.body.firstName;
		const lastName: string = req.body.lastName;
		const friends: string = req.body.friends;
		const playlists: string = req.body.playlists;
		const guestInPlaylists: string = req.body.guestInPlaylists;
		const editorInPlaylists: string = req.body.editorInPlaylists;
		const events: string = req.body.events;
		const guestInEvents: string = req.body.guestInEvents;
		const djInEvents: string = req.body.djInEvents;

		const user_settings: object = {email, displayName, firstName, lastName, friends, playlists, guestInPlaylists, editorInPlaylists, events, guestInEvents, djInEvents};
		
		const validator: Validator = new Validator({apiKey, userid, user_settings}, mandatoryParameters);
		if (await validator.err) {
			res.status(ResponseStatusTypes.BAD_REQUEST).send(await validator.err);
			return ;
		}

		await acquireLock(lockKeys.user(userid), async () => {
			const user: User = await User.get({apiKey});
			if (!user) {
				res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.INVALID_API_KEY));
				return ;
			}
			if (!user.activated) {
				res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.USER_ACCOUNT_NOT_ACTIVATED));
				return ;
			}
			if (user.id != userid) {
				res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.EDIT_NOT_AUTHORIZED));
				return ;
			}
			
			let settings: Setting[] = Object.keys(user_settings).map(key => {
				return { name: key, value: user_settings[key] };
			}).filter(x => x.value != undefined);
			
			await user.updateSettings(settings);

			res.status(ResponseStatusTypes.OK).send({message: 'Settings updated'});
		});
	}

	public async getFriends(req: Request, res: Response) : Promise<any> {
		const mandatoryParameters: Parameter[] = [new Parameter('apiKey', 'query'), new Parameter('userid', 'param')];

		const apiKey: string = req.query.apiKey;
		const userid: string = req.params.userid;

		const friendid: string = req.params.friendid;

		const validator: Validator = new Validator({apiKey, userid, friendid}, mandatoryParameters);
		if (await validator.err) {
			res.status(ResponseStatusTypes.BAD_REQUEST).send(await validator.err);
			return ;
		}

		const user: User = await User.get({apiKey});
		if (!user) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.NO_USER));
			return ;
		}
		if (!user.activated) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.USER_ACCOUNT_NOT_ACTIVATED));
			return ;
		}
		if (user.id != userid) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.EDIT_NOT_AUTHORIZED));
			return ;
		}

		if (!friendid) {
			// return all friends
			const promises = user.friends.map(async friendid => {
				const friend: User = await User.get({id: friendid});
				if (!friend) {
					res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_USER));
					return ;
				}
				if (!friend.activated) {
					res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.USER_ACCOUNT_NOT_ACTIVATED));
					return ;
				}
				return friend.filterByRelationship(user);
			});
			const friends = await Promise.all(promises);
			if (!res.headersSent) {
				res.status(ResponseStatusTypes.OK).send({friends});
			}
			return ;
		} else {
			// return single friend
			if (!user.friends.includes(friendid)) {
				res.status(ResponseStatusTypes.BAD_REQUEST).send(new ServerError(ErrorCodes.NOT_FRIEND));
				return ;
			}
			const friend: User = await User.get({id: friendid});
			if (!friend) {
				res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_USER, "Friend does not exit"));
				return ;
			}
			if (!friend.activated) {
				res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.USER_ACCOUNT_NOT_ACTIVATED));
				return ;
			}
			res.status(ResponseStatusTypes.OK).send(friend.filterByRelationship(user));
			return ;
		}
	}

	public async activateAccount(req: Request, res: Response) : Promise<any> {
		const mandatoryParameters: Parameter[] = [new Parameter('userid', 'param')];

		const userid: string = req.params.userid;
		const code: string = req.query.code;
		const email: string = req.query.email;

		const validator: Validator = new Validator({userid, code, email}, mandatoryParameters);
		if (await validator.err) {
			res.status(ResponseStatusTypes.BAD_REQUEST).send(await validator.err);
			return ;
		}

		let user: User = await User.get({id: userid});
		if (!user) {
			res.status(ResponseStatusTypes.BAD_REQUEST).send(new ServerError(ErrorCodes.NO_USER));
			return ;
		}
		if (user.activated) {
			res.status(ResponseStatusTypes.BAD_REQUEST).send(new ServerError(ErrorCodes.USER_ACCOUNT_ACTIVATED));
			return ;
		}
		await acquireLock(lockKeys.user(userid), async () => {
			user = await User.get({id: userid});
			if (!user) {
				res.status(ResponseStatusTypes.BAD_REQUEST).send(new ServerError(ErrorCodes.NO_USER));
				return ;
			}
			if (!code) {
				if (email) {
					await user.update({email});
				}
				// send new code
				user.resetActivationCode()
				await user.save();
				await user.sendActivationCode();
				res.status(ResponseStatusTypes.OK).send({message: 'Sent new activation code'});				
				return ;
			} else {
				// check expiry date
				const expiryDate: Date = new Date(user.activationCodeExpiryDate);
				if (expiryDate < new Date()) {
					res.status(ResponseStatusTypes.BAD_REQUEST).send(new ServerError(ErrorCodes.ACTIVATION_CODE_EXPIRED));
					return ;
				}
				// check value
				if (code != user.activationCode) {
					res.status(ResponseStatusTypes.BAD_REQUEST).send(new ServerError(ErrorCodes.INVALID_ACTIVATION_CODE));
					return ;
				}
				user.activate();
				await user.save();
				res.status(ResponseStatusTypes.OK).send({apiKey: user.apiKey, message: 'account activated'});				
			}
		});
	}

	public async updateIsPremium(req: Request, res: Response) : Promise<any> {
		const mandatoryParameters: Parameter[] = [new Parameter('apiKey', 'query'), new Parameter('userid', 'param')];

		const apiKey: string = req.query.apiKey;
		const userid: string = req.params.userid;

		const validator: Validator = new Validator({apiKey, userid}, mandatoryParameters);
		if (await validator.err) {
			res.status(ResponseStatusTypes.BAD_REQUEST).send(await validator.err);
			return ;
		}

		let user: User = await User.get({id: userid});
		if (!user) {
			res.status(ResponseStatusTypes.BAD_REQUEST).send(new ServerError(ErrorCodes.NO_USER));
			return ;
		}
		if (!user.activated) {
			res.status(ResponseStatusTypes.BAD_REQUEST).send(new ServerError(ErrorCodes.USER_ACCOUNT_NOT_ACTIVATED));
			return ;
		}
		await acquireLock(lockKeys.user(userid), async () => {
			user = await User.get({id: userid});
			if (!user) {
				res.status(ResponseStatusTypes.BAD_REQUEST).send(new ServerError(ErrorCodes.NO_USER));
				return ;
			}
			const method: string = req.method;
			let update: boolean = false;
			update = user.setIsPremium((method == 'PUT') ? true : false);
			if (!update) {
				res.status(ResponseStatusTypes.OK).send({message: 'no change'});
				return ;
			}
			await user.save();
			res.status(ResponseStatusTypes.OK).send({message: `Account ${(method == 'PUT' ? 'up':'down')}graded`});
		});
	}
	
	// creates, logs in or links user with facebook account
	public async facebook(req: Request, res: Response) : Promise<any> {
		const mandatoryParameters: Parameter[] = [new Parameter('accessToken', 'query')];

		const accessToken: string = req.query.accessToken;
		const apiKey: string = req.query.apiKey;

		const validator: Validator = new Validator({accessToken, apiKey}, mandatoryParameters);
		if (await validator.err) {
			res.status(ResponseStatusTypes.BAD_REQUEST).send(await validator.err);
			return ;
		}

		request.get({url: `https://graph.facebook.com/me?access_token=${accessToken}&fields=email,first_name,last_name`, json: true, timeout: 5000})
		.then(async response => {
			const requiredProperties: string[] = ['email', 'first_name', 'last_name', 'id'];

			for (let i = 0; i < requiredProperties.length; i++) {
				const prop: string = requiredProperties[i];
				if (!response.hasOwnProperty(prop)) {
					res.status(ResponseStatusTypes.BAD_REQUEST).send({message: `missing property ${prop} from FB: increase scope access`});
					return ;
				}
			}

			let user: User = await User.get({facebookid: response.id});
			if (user) {
				await userController.login(req, res, {facebookid: response.id});
				return ;
			}
			if (!apiKey) {
				// check if email not already in use
				user = await User.get({email: response.email});
				if (user) {
					res.status(ResponseStatusTypes.CONFLICT).send({message: `User with email ${response.email} already exists`});
					return ;
				}
				// create user
				await userController.create(req, res, {facebookid: response.id}, {
					facebookid: response.id,
					email: response.email,
					firstName: response.first_name,
					lastName: response.last_name
				});
				return ;
			}
			// check if user with apiKey exists
			user = await User.get({apiKey});
			if (!user) {
				res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.NO_USER));
				return ;
			}
			// link with facebook account
			await acquireLock(lockKeys.user(user.id), async () => {
				user = await User.get({apiKey});
				if (!user) return ;
				user.facebookid = response.id;
				await user.save();
			});
			await userController.login(req, res, {facebookid: response.id}, true);
			return ;
		})
		.catch((err: Error) => {
			console.log(chalk.red(`facebook err: `, err.message));
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.FACEBOOK_FAIL, err.message));
		});
	}

	private async create(req: Request, res: Response, identifyby: {email: string} | {facebookid: string}, attributes: any) : Promise<any> {
		await acquireLock(lockKeys.user('create'), async () => {
			const existingUser = await User.get(identifyby);
			if (existingUser) {
				res.status(ResponseStatusTypes.CONFLICT).send(new ServerError(ErrorCodes.USER_ALREADY_EXISTS));
				return ;
			}

			let user: User = new User(attributes);

			// automatically activate user based on environement variables, else, needs to activate by email.
			if (env.autoActivate()) user.activate();
			if (env.autoPremium()) user.setIsPremium(true);
			
			await user.assignRandomDefaultAvatar();
			await user.create();

			if (!user.activated) {
				await user.sendActivationCode();
				res.status(ResponseStatusTypes.OK).send({
					message: 'User created. Please activate account for apiKey.',
					id: user.id
				});
			} else {
				res.status(ResponseStatusTypes.OK).send({
					...user.filterByRelationship(user),
					message: 'User created'
				});
			}
		});
	}

	private async login(req: Request, res: Response, identifyby: {email?: string, password?: string, facebookid?: string}, linked?: boolean) : Promise<any> {
		const user: User = await User.get(identifyby);
		if (!user) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.NO_USER));
			return ;
		}
		if (!user.activated) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.USER_ACCOUNT_NOT_ACTIVATED));
			return ;
		}
		// if with pw and username
		if (identifyby.email && identifyby.password) {
			if (identifyby.password == user.password) {
				if (user.passwordResetCode != null) {
					await acquireLock(lockKeys.user(user.id), async () => {
						const user: User = await User.get({email: identifyby.email});
						if (!user) {
							res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_USER));
							return ;
						}
						user.passwordResetCode = null;
						user.passwordResetAttemptsRemaining = null;
						await user.save();
					});
				}
				await sendLoginEmail(user);
				res.status(ResponseStatusTypes.OK).send({apiKey: user.apiKey, id: user.id});
			} else {
				res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.INVALID_PASSWORD));
			}
		} else if (identifyby.facebookid) {
			await sendLoginEmail(user);
			// link with facebook
			if (linked) {
				res.status(ResponseStatusTypes.OK).send({apiKey: user.apiKey, id: user.id, message: 'Facebook and Music Room accounts linked', facebookid: user.facebookid});
			} else {
				res.status(ResponseStatusTypes.OK).send({apiKey: user.apiKey, id: user.id});
			}
		}

		async function sendLoginEmail(user: User): Promise<any> {
			return sendEmail(user, 'login', {
				ipAddress: req.ip,
				appVersion: req.get("MusicRoomInfo-AppVersion"),
				deviceModel: req.get("MusicRoomInfo-DeviceModel"),
				deviceVersion: req.get("MusicRoomInfo-DeviceVersion"),
				deviceOS: req.get("MusicRoomInfo-DeviceOS")
			});
		}
	}
}

const userController: UserController = new UserController();
export {
	UserController,
	userController
};