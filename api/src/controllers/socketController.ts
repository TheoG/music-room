import * as socketio from "socket.io";
import { acquireLock, lockKeys } from '../utils/mutex'; 
import chalk from "chalk";

import SocketActions from "../utils/SocketActions";

type Namespace = '/playlists' | '/events';

type Connection = {
	userId: string,
	namespace: Namespace,
	room: string,
	socket: socketio.Socket,
	allowMultipleConnections: boolean // if true, only allows one connection per user, per room, per namespace
};

class SocketController {
	public io: socketio.Server;
	
	public connections: Connection[] = [];

	private isInit: boolean = false;

	public init(io: socketio.Server) {
		this.io = io;

		playlistController.initSocketIO(this.io);
		eventController.initSocketIO(this.io);

		this.isInit = true;
	}

	/*
	*	Adds a Connection. Unique per socketId. User allowMultipleConnections = false to destroy other connections with the same user, namespace, and room.
	*/
	public async registerClient(namespace: Namespace, room: string, userId: string, socket: socketio.Socket, allowMultipleConnections: boolean) {
		if (!this.isInit) return ;

		socket.join(room);
		
		// if connection not in list of connections for that user, add it
		await acquireLock(lockKeys.connections(), async () => {
			if (!this.connections.find(x => x.socket.id == socket.id)) {
				let existingConnection: Connection = this.connections.find(x => x.userId == userId && x.namespace == namespace && x.room == room && x.allowMultipleConnections == false);
				if (existingConnection) {
					this.connections.splice(this.connections.indexOf(existingConnection), 1);
					existingConnection.socket.disconnect();
				}
				this.connections.push({userId, namespace, room, socket, allowMultipleConnections});
			}
		});
		
		socket.emit(SocketActions.CONNECTED);
	}

	public async unregisterClient(socketid: string) {
		if (!this.isInit) return;

		// disconnects all connections between a client and a room
		await acquireLock(lockKeys.connections(), () => {
			let connection: Connection = this.connections.find(x => x.socket.id === socketid);
		
			if (connection) {
				this.connections.splice(this.connections.indexOf(connection), 1);
				connection.socket.disconnect();
			} else {
				// console.log(chalk.yellow('[SocketController.unregisterClient] connection NOT found', socketid));
			}
		}); 
	}

	public async updateConnections(namespace: Namespace, room: string) {
		if (!this.isInit) return;

		await acquireLock(lockKeys.connections(), async () => {
			let connections: Connection[] = this.connections.filter((x: Connection) => x.namespace == namespace && x.room == room);
			
			await Promise.all(connections.map(async (connection: Connection) => {
				let user: User = await User.get({id: connection.userId});
				
				if (!user) return ;
				switch (namespace) {
	
					case '/playlists':
						let playlist: Playlist = await Playlist.get({id: connection.room});
						if (!playlist || !playlist.userCanAccess(user)) {
							this.connections.splice(this.connections.indexOf(connection), 1);
							connection.socket.disconnect();
						}
						break;
					case '/events':
						let event: Event = await Event.get({id: connection.room});
						if (!event || !event.userCanAccess(user)) {
							this.connections.splice(this.connections.indexOf(connection), 1);
							connection.socket.disconnect();
						}
						break;
				}
			}));
		});
	}

	public emitToRoom(namespace: Namespace, room: string, action: SocketActions, data?: any) {
		if (!this.isInit) return;
		this.io.of(namespace).to(room).emit(action, data);
	}

	public emitToUser(userid: string, action: SocketActions, data?: any) {
		if (!this.isInit) return;
		let connection: Connection = this.connections.find(x => x.userId == userid);
		let socket: socketio.Socket = connection ? connection.socket : null;
		if (socket) {
			socket.emit(action, data);
		} else {
			// console.log(chalk.red('user not connected ' + userid))
		}
	}

	public printConnections() {
		console.log('CONNECTIONS: ', this.connections.map(x => {return {
			user: x.userId, 
			room: x.room,
			socketid: x.socket.id,
			allowMultipleConnections: x.allowMultipleConnections
		}}));
	}
}

const socketController: SocketController = new SocketController();
export {
	socketController, 
	SocketController
}

import { playlistController } from "./playlistController";
import { eventController } from "./eventController";
import { User } from "../classes/User";
import { Playlist } from "../classes/Playlist";
import { Event } from "../classes/Event";


