import { Request, Response } from "express";
import * as schedule from "node-schedule";
import chalk from "chalk";
import * as socketio from "socket.io";

import ResponseStatusTypes from "../utils/ResponseStatusTypes";
import { ErrorCodes, ServerError } from "../utils/errorTypes";
import { Parameter, Validator } from "../classes/Validator";
import { Visibility, SortingType, Editing, Schedule, Coordinates } from '../types/types'; 
import { acquireLock, lockKeys } from "../utils/mutex";
import SocketActions from "../utils/SocketActions"

import { Playlist } from "../classes/Playlist";
import { Event } from "../classes/Event";
import { User } from "../classes/User";
import { SocketController, socketController } from "./socketController";
import EventTrack from "../classes/EventTrack";
import { EventPlaylist } from "../classes/EventPlaylist";

let io: socketio.Namespace = null;

class EventController {
	private jobs: { [eventid: string]: { [name: string]: schedule.Job } } = {};

	public async create(req: Request, res: Response): Promise<any> {
		const mandatoryParameters: Parameter[] = [new Parameter('apiKey', 'query'), new Parameter('name', 'body')];

		const apiKey: string = req.query.apiKey;
		const name: string = req.body.name;
		const description: string = req.body.description;
		const visibility: Visibility = req.body.visibility;		
		const votingSchedule: Schedule = req.body.votingSchedule;
		const eventSchedule: Schedule = req.body.eventSchedule;
		const coordinates: Coordinates = req.body.coordinates;

		const validator: Validator = new Validator({apiKey, name, description, visibility, votingSchedule, eventSchedule, coordinates}, mandatoryParameters);
		if (await validator.err) {
			res.status(ResponseStatusTypes.BAD_REQUEST).send(await validator.err);			
			return ;
		}

		// check if user exists and account is activated and user is premium
		let user: User = await User.get({apiKey});
		if (!user) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.INVALID_API_KEY));
			return ;
		}
		if (!user.activated) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.USER_ACCOUNT_NOT_ACTIVATED));
			return ;
		}
		if (!user.isPremium) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.USER_NOT_PREMIUM));
			return ;
		}

		await acquireLock([lockKeys.event('create'), lockKeys.user(user.id)], async () => {
			let user: User = await User.get({apiKey});
			if (!user) {
				res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.INVALID_API_KEY));
				return ;
			}

			const event: Event = new Event({
				name,
				owner: user.id,
				description,
				visibility,
				votingSchedule,
				eventSchedule,
				coordinates
			});
			await event.create();

			eventController.updateJobs(event, votingSchedule, eventSchedule);

			// add event to user
			user.addEvent(event.id);
			await user.save();
			res.status(ResponseStatusTypes.OK).send({...(await event.expand(user)), message: 'Event created'});
		});
	}

	public async get(req: Request, res: Response): Promise<any> {
		const mandatoryParameters: Parameter[] = [new Parameter('apiKey', 'query')];

		const apiKey: string = req.query.apiKey;
		const eventid: string = req.params.eventid;
		
		const validator: Validator = new Validator({apiKey, eventid}, mandatoryParameters);
		if (await validator.err) {
			res.status(ResponseStatusTypes.BAD_REQUEST).send(await validator.err);			
			return ;
		}

		const user: User = await User.get({apiKey});
		if (!user) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.INVALID_API_KEY));
			return ;
		}
		if (!user.activated) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.USER_ACCOUNT_NOT_ACTIVATED));
			return ;
		}

		if (eventid) {
			const event = await Event.get({id: eventid});
			if (!event) {
				res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_EVENT));
				return ;
			}
	
			// check if user has rights to access
			if (event.userCanAccess(user)) {
				res.status(ResponseStatusTypes.OK).send((await event.expand(user)));
			} else {
				res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.ACCESS_NOT_AUTHORIZED));
			}

		} else {
			let ret = {
				owner: await Promise.all(user.events.map(async (id: string) => {
					return Event.get({id});
				})),
				dj: await Promise.all(user.djInEvents.map(async (id: string) => {
					return Event.get({id});
				})),
				guest: await Promise.all(user.guestInEvents.map(async (id: string) => {
					return Event.get({id});
				}))
			};
			res.status(ResponseStatusTypes.OK).send(ret);
		}
	}

	public async update(req: Request, res: Response): Promise<any> {
		const mandatoryParameters: Parameter[] = [new Parameter('apiKey', 'query'), new Parameter('eventid', 'param')];

		const apiKey: string = req.query.apiKey;
		const eventid: string = req.params.eventid;

		const name: string = req.body.name;		
		const description: string = req.body.description;
		const visibility: Visibility = req.body.visibility;
		const votingSchedule: Schedule = req.body.votingSchedule;
		const eventSchedule: Schedule = req.body.eventSchedule;
		const coordinates: Coordinates = req.body.coordinates;

		const validator: Validator = new Validator({eventid, apiKey, name, visibility, description, votingSchedule, eventSchedule, coordinates}, mandatoryParameters);
		if (await validator.err) {
			res.status(ResponseStatusTypes.BAD_REQUEST).send(await validator.err);			
			return ;
		}

		// check if user exists and account is activated
		let user: User = await User.get({apiKey});
		if (!user) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.INVALID_API_KEY));
			return ;
		}
		if (!user.activated) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.USER_ACCOUNT_NOT_ACTIVATED));
			return ;
		}

		await acquireLock(lockKeys.event(eventid), async () => {
			const event: Event = await Event.get({id: eventid});
			if (!event) {
				res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_EVENT));
				return ;
			}
			
			if (!event.userIsOwner(user)) {
				res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.EDIT_NOT_AUTHORIZED));
				return ;
			}
			
			const updated: boolean = event.update({name, description, visibility, votingSchedule, eventSchedule, coordinates});
			if (!updated) {
				res.status(ResponseStatusTypes.OK).send({message: 'No change'});
				return ;
			}
			eventController.updateJobs(event, votingSchedule, eventSchedule);
			await event.save();
			res.status(ResponseStatusTypes.OK).send({...(await event.expand(user)), message: 'Event updated'});
			socketController.emitToRoom('/events', event.id, SocketActions.EVENT_UPDATE, {by: user.id, name: event.name, description: event.description, visibility: event.visibility, votingSchedule: event.votingSchedule, eventSchedule: event.eventSchedule, coordinates: event.coordinates});
			await socketController.updateConnections("/events", event.id);
		});
	}

	public async delete(req: Request, res: Response): Promise<any> {
		const mandatoryParameters: Parameter[] = [new Parameter('apiKey', 'query'), req.params.eventid ? new Parameter('eventid', 'param') : new Parameter('eventids', 'body')];

		const apiKey: string = req.query.apiKey;
		const eventid: string = req.params.eventid;
		let   eventids: string[] = req.body.eventids;

		const validator: Validator = new Validator({apiKey, eventid, eventids}, mandatoryParameters);
		if (await validator.err) {
			res.status(ResponseStatusTypes.BAD_REQUEST).send(await validator.err);			
			return ;
		}

		// check if user exists and account is activated
		let user: User = await User.get({apiKey});
				
		if (!user) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.INVALID_API_KEY));
			return ;
		}
		if (!user.activated) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.USER_ACCOUNT_NOT_ACTIVATED));
			return ;
		}

		eventids = eventid ? [eventid] : eventids;
		// check that events exist
		let events: Event[] = await Promise.all(eventids.map((id: string) => Event.get({id})));
		if (!events.every(x => !!x === true)) {
			res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_EVENT));
			return ;
		}
		await acquireLock(eventids.map(id => lockKeys.event(id)), async () => {
			// get events
			events = await Promise.all(eventids.map((id: string) => Event.get({id})));
			if (!events.every(x => !!x === true)) {
				res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_EVENT));
				return ;
			}
			// check if user is owner of all events
			if (events.map(event => event.userIsOwner(user)).some(x => x == false)) {
				res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.EDIT_NOT_AUTHORIZED));
				return ;
			}
			
			await Promise.all(events.map(async event => {
				await event.delete();
				await eventController.updateJobs(event, {start: null, end: null}, {start: null, end: null});
			}));
			res.status(ResponseStatusTypes.OK).send({message: `Event${events.length == 1 ? '' : 's'} deleted`});
			await Promise.all(events.map(async event => {
				await socketController.emitToRoom('/events', event.id, SocketActions.EVENT_DELETED, {by: user.id, eventid: event.id})
				await socketController.updateConnections("/events", event.id);
			}));
		});
	}

	public async addGuest(req: Request, res: Response): Promise<any> {
		const mandatoryParameters: Parameter[] = [new Parameter('apiKey', 'query'), new Parameter('eventid', 'param'), req.params.userid ? new Parameter('userid', 'param'): new Parameter('userids', 'body')];

		const apiKey: string = req.query.apiKey;
		const eventid: string = req.params.eventid;
		const userid: string = req.params.userid;
		let   userids: string[] = req.body.userids;

		const validator: Validator = new Validator({apiKey, eventid, userid, userids}, mandatoryParameters);
		if (await validator.err) {
			res.status(ResponseStatusTypes.BAD_REQUEST).send(await validator.err);			
			return ;
		}

		// check if user exists and account is activated
		let user: User = await User.get({apiKey});
		if (!user) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.INVALID_API_KEY));
			return ;
		}
		if (!user.activated) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.USER_ACCOUNT_NOT_ACTIVATED));
			return ;
		}

		userids = userid ? [userid] : userids;
		// check that guests exist
		let users: User[] = await Promise.all(userids.map(async id => await User.get({id})));
		if (!users.every(x => !!x === true)) {
			res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_USER));
			return ;
		}
		await acquireLock([lockKeys.event(eventid), ...userids.map(id => lockKeys.user(id))], async () => {
			// check that event exists
			const event: Event = await Event.get({id: eventid});
			if (!event) {
				res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_EVENT));
				return ;
			}
			// only owner can add guests
			if (!event.userIsOwner(user)) {
				res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.EDIT_NOT_AUTHORIZED));
				return ;
			}
			// get guests
			users = await Promise.all(userids.map(async id => await User.get({id})));
			if (!users.every(x => !!x === true)) {
				res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_USER));
				return ;
			}
			// check that owner is not in list of users (owner cannot be guest or dj)
			if (users.some(user => event.userIsOwner(user))) {
				res.status(ResponseStatusTypes.BAD_REQUEST).send(new ServerError(ErrorCodes.CANNOT_ADD_OWNER_AS_GUEST_IN_EVENT))
				return ;
			}
			// add guests from event and event from guests
			let updated = [
				...users.map(guest => event.addGuest(guest.id)),
				...users.map(guest => guest.addGuestInEvent(event.id))
			];
			if (updated.every(x => x == false)) {
				res.status(ResponseStatusTypes.OK).send({message: 'No change'});
				return ;
			}
			// save guests and event
			await Promise.all([
				...users.map(user => user.save()),
				event.save()
			]);
			socketController.emitToRoom('/events', event.id, SocketActions.EVENT_ADD_GUESTS, {by: user.id, userids});
			res.status(ResponseStatusTypes.OK).send({...(await event.expand(user)), message: (userid ? 'guest' : 'guests') + ' added'});
		});
	}

	public async removeGuest(req: Request, res: Response): Promise<any> {
		const mandatoryParameters: Parameter[] = [new Parameter('apiKey', 'query'), new Parameter('eventid', 'param'), req.params.userid ? new Parameter('userid', 'param'): new Parameter('userids', 'body')];

		const apiKey: string = req.query.apiKey;
		const eventid: string = req.params.eventid;
		const userid: string = req.params.userid;
		let   userids: string[] = req.body.userids;

		const validator: Validator = new Validator({apiKey, eventid, userid, userids}, mandatoryParameters);
		if (await validator.err) {
			res.status(ResponseStatusTypes.BAD_REQUEST).send(await validator.err);			
			return ;
		}

		// check if user exists and account is activated
		let user: User = await User.get({apiKey});
		if (!user) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.INVALID_API_KEY));
			return ;
		}
		if (!user.activated) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.USER_ACCOUNT_NOT_ACTIVATED));
			return ;
		}

		userids = userid ? [userid] : userids;
		// check that guests exist
		let users: User[] = await Promise.all(userids.map(async id => await User.get({id})));
		if (!users.every(x => !!x === true)) {
			res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_USER));
			return ;
		}
		await acquireLock([lockKeys.event(eventid), ...userids.map(id => lockKeys.user(id))], async () => {
			// check that event exists
			const event: Event = await Event.get({id: eventid});
			if (!event) {
				res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_EVENT));
				return ;
			}
			// only owner can remove guests OR guest can remove self and only self
			if (!event.userIsOwner(user) && !(event.userIsGuest(user) && userids.length == 1 && userids[0] == user.id)) {
				res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.EDIT_NOT_AUTHORIZED));
				return ;
			}
			// get guests
			users = await Promise.all(userids.map(async id => await User.get({id})));
			if (!users.every(x => !!x === true)) {
				res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_USER));
				return ;
			}
			// remove guests from event and event from guests
			let updated = [
				...users.map(guest => event.removeGuest(guest.id)),
				...users.map(guest => guest.removeGuestInEvent(event.id))
			];
			if (updated.every(x => x == false)) {
				res.status(ResponseStatusTypes.OK).send({message: 'No change'});
				return ;
			}
			// save guests and event
			await Promise.all([
				...users.map(user => user.save()),
				event.save()
			]);
			socketController.emitToRoom('/events', event.id, SocketActions.EVENT_REMOVE_GUESTS, {by: user.id, userids});
			res.status(ResponseStatusTypes.OK).send({...(await event.expand(user)), message: (userid ? 'guest' : 'guests') + ' removed'});
			await socketController.updateConnections("/events", event.id);
		});
	}

	public async addDJ(req: Request, res: Response): Promise<any> {
		const mandatoryParameters: Parameter[] = [new Parameter('apiKey', 'query'), new Parameter('eventid', 'param'), req.params.userid ? new Parameter('userid', 'param'): new Parameter('userids', 'body')];

		const apiKey: string = req.query.apiKey;
		const eventid: string = req.params.eventid;
		const userid: string = req.params.userid;
		let   userids: string[] = req.body.userids;

		const validator: Validator = new Validator({apiKey, eventid, userid, userids}, mandatoryParameters);
		if (await validator.err) {
			res.status(ResponseStatusTypes.BAD_REQUEST).send(await validator.err);			
			return ;
		}

		// check if user exists and account is activated
		let user: User = await User.get({apiKey});
		if (!user) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.INVALID_API_KEY));
			return ;
		}
		if (!user.activated) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.USER_ACCOUNT_NOT_ACTIVATED));
			return ;
		}

		userids = userid ? [userid] : userids;
		// check that djs exist
		let users: User[] = await Promise.all(userids.map(async id => await User.get({id})));
		if (!users.every(x => !!x === true)) {
			res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_USER));
			return ;
		}
		await acquireLock([lockKeys.event(eventid), ...userids.map(id => lockKeys.user(id))], async () => {
			// check that event exists
			const event: Event = await Event.get({id: eventid});
			if (!event) {
				res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_EVENT));
				return ;
			}
			// only owner can add djs
			if (!event.userIsOwner(user)) {
				res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.EDIT_NOT_AUTHORIZED));
				return ;
			}
			// get djs
			users = await Promise.all(userids.map(async id => await User.get({id})));
			if (!users.every(x => !!x === true)) {
				res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_USER));
				return ;
			}
			// check that owner is not in list of users (owner cannot be guest or dj)
			if (users.some(user => event.userIsOwner(user))) {
				res.status(ResponseStatusTypes.BAD_REQUEST).send(new ServerError(ErrorCodes.CANNOT_ADD_OWNER_AS_DJ_IN_EVENT))
				return ;
			}
			// add djs from event and event to djs
			let updated = [
				...users.map(user => event.addDJ(user.id)),
				...users.map(user => user.addDJInEvent(event.id))
			];
			if (updated.every(x => x == false)) {
				res.status(ResponseStatusTypes.OK).send({message: 'No change'});
				return ;
			}
			// save djs and event
			await Promise.all([
				...users.map(user => user.save()),
				event.save()
			]);
			socketController.emitToRoom('/events', event.id, SocketActions.EVENT_ADD_DJS, {by: user.id, userids});
			res.status(ResponseStatusTypes.OK).send({...(await event.expand(user)), message: (userid ? 'dj' : 'djs') + ' added'});
		});
	}

	public async removeDJ(req: Request, res: Response): Promise<any> {
		const mandatoryParameters: Parameter[] = [new Parameter('apiKey', 'query'), new Parameter('eventid', 'param'), req.params.userid ? new Parameter('userid', 'param'): new Parameter('userids', 'body')];

		const apiKey: string = req.query.apiKey;
		const eventid: string = req.params.eventid;
		const userid: string = req.params.userid;
		let   userids: string[] = req.body.userids;

		const validator: Validator = new Validator({apiKey, eventid, userid, userids}, mandatoryParameters);
		if (await validator.err) {
			res.status(ResponseStatusTypes.BAD_REQUEST).send(await validator.err);			
			return ;
		}

		// check if user exists and account is activated
		let user: User = await User.get({apiKey});
		if (!user) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.INVALID_API_KEY));
			return ;
		}
		if (!user.activated) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.USER_ACCOUNT_NOT_ACTIVATED));
			return ;
		}

		userids = userid ? [userid] : userids;
		// check that djs exist
		let users: User[] = await Promise.all(userids.map((id: string) => User.get({id})));
		if (!users.every(x => !!x === true)) {
			res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_USER));
			return ;
		}
		await acquireLock([lockKeys.event(eventid), ...userids.map(id => lockKeys.user(id))], async () => {
			// check that event exists
			const event: Event = await Event.get({id: eventid});
			if (!event) {
				res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_EVENT));
				return ;
			}
			// only owner can remove djs OR dj can remove self and only self
			if (!event.userIsOwner(user) && !(event.userIsDJ(user) && userids.length == 1 && userids[0] == user.id)) {
				res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.EDIT_NOT_AUTHORIZED));
				return ;
			}
			// get djs
			users = await Promise.all(userids.map((id: string) => User.get({id})));
			if (!users.every(x => !!x === true)) {
				res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_USER));
				return ;
			}
			// remove djs from event and event from djs
			let updated = [
				...users.map((user: User) => event.removeDJ(user.id)),
				...users.map((user: User) => user.removeDJInEvent(event.id))
			];
			if (updated.every(x => x == false)) {
				res.status(ResponseStatusTypes.OK).send({message: 'No change'});
				return ;
			}
			// save djs and event
			await Promise.all([
				...users.map(user => user.save()),
				event.save()
			]);
			socketController.emitToRoom('/events', event.id, SocketActions.EVENT_REMOVE_DJS, {by: user.id, userids});
			res.status(ResponseStatusTypes.OK).send({...(await event.expand(user)), message: (userid ? 'dj' : 'djs') + ' removed'});
			await socketController.updateConnections("/events", event.id);
		});
	}

	public async addPlaylist(req: Request, res: Response): Promise<any> {
		const mandatoryParameters: Parameter[] = [new Parameter('apiKey', 'query'), new Parameter('eventid', 'param'), req.params.playlistid ? new Parameter('playlistid', 'param'): new Parameter('playlistids', 'body')];

		const apiKey: string = req.query.apiKey;
		const eventid: string = req.params.eventid;
		const playlistid: string = req.params.playlistid;
		let   playlistids: string[] = req.body.playlistids;
		const inviteEditors: boolean = req.body.inviteEditors;
		const inviteGuests: boolean = req.body.inviteGuests;
		const duplicatePlaylist: boolean = req.body.duplicatePlaylist;
		const copyGuests: string = req.body.copyGuests;
		const copyEditors: string = req.body.copyEditors;

		const validator: Validator = new Validator({apiKey, eventid, playlistid, playlistids, inviteEditors, inviteGuests, duplicatePlaylist, copyGuests, copyEditors}, mandatoryParameters);
		if (await validator.err) {
			res.status(ResponseStatusTypes.BAD_REQUEST).send(await validator.err);			
			return ;
		}

		// check if user exists and account is activated
		const user: User = await User.get({apiKey});
		if (!user) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.INVALID_API_KEY));
			return ;
		}
		if (!user.activated) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.USER_ACCOUNT_NOT_ACTIVATED));
			return ;
		}
		
		playlistids = playlistid ? [playlistid] : playlistids;
		// check that all playlists exist
		let playlists: Playlist[] = await Promise.all(playlistids.map(async (id: string) => await Playlist.get({id})));
		if (!playlists.every(x => !!x === true)) {
			res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_PLAYLIST));
			return ;
		}
		await acquireLock([lockKeys.event(eventid), ...playlistids.map(id => lockKeys.playlist(id))], async () => {
			// check that event exists
			let event: Event = await Event.get({id: eventid});
			if (!event) {
				res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_EVENT));
				return ;
			}
			// check that user is owner of event (does not have to be owner of playlist) and can access playlist
			if (!event.userIsOwner(user)) {
				res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.EDIT_NOT_AUTHORIZED));
				return ;
			}
			// cannot add playlists if event has started
			if (event.votingActive || event.eventActive) {
				res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.CANNOT_ADD_PLAYLIST_TO_ACTIVE_EVENT));
				return ;
			}
			// check that all playlists exist
			playlists = await Promise.all(playlistids.map(async (id: string) => await Playlist.get({id})));
			if (!playlists.every(x => !!x === true)) {
				res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_PLAYLIST));
				return ;
			}
			// check that user can acces all playlists
			let canAccess = (await Promise.all(playlists.map(async (playlist: Playlist) => playlist.userCanAccess(user) ))).every(x => x == true);
			if (!canAccess) {
				res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.ACCESS_NOT_AUTHORIZED));
				return ;
			}
			// copy playlists (user must be owner of event) IF duplicatePlaylist option passed
			if (duplicatePlaylist && event.userIsOwner(user)) {
				playlists = await Promise.all(playlists.map(async (playlist: Playlist) => {
					let duplicatePlaylist: Playlist = await playlist.duplicate(user.id, {copyEditors: copyEditors && playlist.userIsOwner(user), copyGuests: copyGuests && playlist.userIsOwner(user)})
					await duplicatePlaylist.create();
					return duplicatePlaylist;
				}));
			}
			// add playlists to event, add events to playlists		
			let updated: boolean[] = [
				...playlists.map(playlist => playlist.addEvent(event.id)),
				...playlists.map(playlist => event.addPlaylist(playlist.id)),
			];
			if (updated.every(x => x == false)) {
				res.status(ResponseStatusTypes.OK).send({message: 'No change'});
				return ;
			}
			// invite guests and editors from playlists whose owner is event owner to event IF options passed
			if (inviteGuests || inviteEditors) {
				let ownedPlaylists: Playlist[] = playlists.filter(playlist => playlist.userIsOwner(user));
				await Promise.all(ownedPlaylists.map(async playlist => {
					let userids = [];
					if (inviteGuests ) userids.push(...playlist.guests );
					if (inviteEditors) userids.push(...playlist.editors);
					// for all users to add to event as guests
					await Promise.all(userids.map(async id => {
						let user: User = await User.get({id});
						if (!user) return ;
						await acquireLock(lockKeys.user(id), async () => {
							let user: User = await User.get({id});
							if (!user) return ;
							user.addGuestInEvent(event.id);
							event.addGuest(id);
							user.save();
						});	
					}));
				}));
			}
			// save playlists and events
			await Promise.all([
				...playlists.map((playlist: Playlist) => playlist.save()),
				event.save()
			]);
			res.status(ResponseStatusTypes.OK).send({...(await event.expand(user)), message: (playlistid ? 'playlist' : 'playlists') + ' added'});
		});
	}

	public async removePlaylist(req: Request, res: Response): Promise<any> {
		const mandatoryParameters: Parameter[] = [new Parameter('apiKey', 'query'), new Parameter('eventid', 'param'), req.params.playlistid ? new Parameter('playlistid', 'param'): new Parameter('playlistids', 'body')];

		const apiKey: string = req.query.apiKey;
		const eventid: string = req.params.eventid;
		const playlistid: string = req.params.playlistid;
		let   playlistids: string[] = req.body.playlistids;
		
		const validator: Validator = new Validator({apiKey, eventid, playlistid, playlistids}, mandatoryParameters);
		if (await validator.err) {
			res.status(ResponseStatusTypes.BAD_REQUEST).send(await validator.err);			
			return ;
		}

		// check if user exists and account is activated
		let user: User = await User.get({apiKey});
		if (!user) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.INVALID_API_KEY));
			return ;
		}
		if (!user.activated) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.USER_ACCOUNT_NOT_ACTIVATED));
			return ;
		}
		
		playlistids = playlistid ? [playlistid] : playlistids;
		// check that playlists exist
		let playlists: Playlist[] = await Promise.all(playlistids.map(id => Playlist.get({id})));
		if (!playlists.every(x => !!x === true)) {
			res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_PLAYLIST));
			return ;
		}
		await acquireLock([lockKeys.event(eventid), ...playlistids.map(id => lockKeys.playlist(id))], async () => {
			// check that event exists
			const event: Event = await Event.get({id: eventid});
			if (!event) {
				res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_EVENT));
				return ;
			}
			// check that user is owner of event (does not have to be owner of playlist) and can access playlist
			if (!event.userIsOwner(user)) {
				res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.EDIT_NOT_AUTHORIZED));
				return ;
			}
			// cannot remove playlists if event has started
			if (event.votingActive || event.eventActive) {
				res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.CANNOT_REMOVE_PLAYLIST_TO_ACTIVE_EVENT));
				return ;
			}
			// get playlists
			playlists = await Promise.all(playlistids.map(id => Playlist.get({id})));
			if (!playlists.every(x => !!x === true)) {
				res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_PLAYLIST));
				return ;
			}
			// Note: user can remove playlist from their event even if they have no rights on playlist
			// remove playlists from event and event from playlists
			let updated: boolean[] = [
				...playlists.map(playlist => playlist.removeEvent(event.id)),
				...playlists.map(playlist => event.removePlaylist(playlist.id)),
			];
			if (updated.every(x => x == false)) {
				res.status(ResponseStatusTypes.OK).send({message: 'No change'});
				return ;
			}
			// save playlists and event
			await Promise.all([
				...playlists.map((playlist: Playlist) => playlist.save()),
				event.save()
			]);
			res.status(ResponseStatusTypes.OK).send({...(await event.expand(user)), message: (playlistid ? 'playlist' : 'playlists') + ' removed'});
		});
	}

	public async eventAction(req: Request, res: Response): Promise<any> {
		const mandatoryParameters: Parameter[] = [new Parameter('apiKey', 'query'), new Parameter('eventid', 'param'), new Parameter('eventaction', 'param')];

		const apiKey: string = req.query.apiKey;
		const eventid: string = req.params.eventid;
		const eventaction: string = req.params.eventaction;
		const validator: Validator = new Validator({apiKey, eventid, eventaction}, mandatoryParameters);
		if (await validator.err) {
			res.status(ResponseStatusTypes.BAD_REQUEST).send(await validator.err);			
			return ;
		}

		// check if user exists and account is activated
		let user: User = await User.get({apiKey});
				
		if (!user) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.INVALID_API_KEY));
			return ;
		}
		if (!user.activated) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.USER_ACCOUNT_NOT_ACTIVATED));
			return ;
		}

		await acquireLock(lockKeys.event(eventid), async () => {
			// check that event exists
			let event: Event = await Event.get({id: eventid});
			if (!event) {
				res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_EVENT));
				return ;
			}
			if (!event.userIsOwner(user)) {
				res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.EDIT_NOT_AUTHORIZED));
				return ;
			}

			switch (eventaction) {
				case 'startvoting':
					if (event.votingActive) {
						res.status(ResponseStatusTypes.OK).send({...(await event.expand(user)), message: 'voting already active'});
						return ;
					}
					await event.startVoting();
					await event.save();
					res.status(ResponseStatusTypes.OK).send({...(await event.expand(user)), message: 'voting started'});
					socketController.emitToRoom('/events', event.id, SocketActions.EVENT_VOTING_STARTED, {by: user.id, eventPlaylists: event.eventPlaylists});
					break;
				case 'endvoting':
					if (!event.votingActive) {
						res.status(ResponseStatusTypes.OK).send({...(await event.expand(user)), message: 'voting already not active'});
						return ;
					}
					await event.endVoting();
					await event.save();
					res.status(ResponseStatusTypes.OK).send({...(await event.expand(user)), message: 'voting ended'});
					socketController.emitToRoom('/events', event.id, SocketActions.EVENT_VOTING_ENDED, (event.votingActive || event.eventActive) ? {by: user.id, eventPlaylists: event.eventPlaylists} : {by: user.id, playlists: (await expandPlaylists(event.playlists))});
					break;
				case 'startevent':
					if (event.eventActive) {
						res.status(ResponseStatusTypes.OK).send({...(await event.expand(user)), message: 'event already active'});
						return ;
					}
					await event.startEvent();
					await event.save();
					res.status(ResponseStatusTypes.OK).send({...(await event.expand(user)), message: 'event started'});
					socketController.emitToRoom('/events', event.id, SocketActions.EVENT_EVENT_STARTED, {by: user.id, eventPlaylists: event.eventPlaylists});
					break;
				case 'endevent':
					if (!event.eventActive) {
						res.status(ResponseStatusTypes.OK).send({...(await event.expand(user)), message: 'event already not active'});
						return ;
					}
					await event.endEvent();
					await event.save();
					res.status(ResponseStatusTypes.OK).send({...(await event.expand(user)), message: 'event ended'});
					socketController.emitToRoom('/events', event.id, SocketActions.EVENT_EVENT_ENDED, (event.votingActive || event.eventActive) ? {by: user.id, eventPlaylists: event.eventPlaylists} : {by: user.id, playlists: (await expandPlaylists(event.playlists))});
					break;
				default:
					res.status(ResponseStatusTypes.BAD_REQUEST).send(new ServerError(ErrorCodes.INVALID_EVENT_ACTION));
					return ;
			}
		});
	}

	public async playerAction(req: Request, res: Response): Promise<any> {
		const mandatoryParameters: Parameter[] = [new Parameter('apiKey', 'query'), new Parameter('eventid', 'param'), new Parameter('playeraction', 'param')];

		const apiKey: string = req.query.apiKey;
		const eventid: string = req.params.eventid;
		const playeraction: string = req.params.playeraction;
		let   trackid: string = req.body.trackid;
		const timestamp: string = req.body.timestamp;
		let   trackPosition: number = req.body.trackPosition;
		const validator: Validator = new Validator({apiKey, eventid, playeraction, trackid, timestamp, trackPosition}, mandatoryParameters);
		if (await validator.err) {
			res.status(ResponseStatusTypes.BAD_REQUEST).send(await validator.err);			
			return ;
		}
	
		// check if user exists and account is activated
		let user: User = await User.get({apiKey});

		if (!user) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.INVALID_API_KEY));
			return ;
		}
		if (!user.activated) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.USER_ACCOUNT_NOT_ACTIVATED));
			return ;
		}

		await acquireLock(lockKeys.event(eventid), async () => {
			// check that event exists
			let event: Event = await Event.get({id: eventid});
			if (!event) {
				res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_EVENT));
				return ;
			}

			// check that event is active and user is owner or dj
			if (!event.eventActive) {
				res.status(ResponseStatusTypes.BAD_REQUEST).send(new ServerError(ErrorCodes.EVENT_NOT_ACTIVE));
				return ;
			}
			if (!(event.userIsDJ(user) || event.userIsOwner(user))) {
				res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.EDIT_NOT_AUTHORIZED));
				return ;
			}
			if (!trackid) {
				let activePlaylist: EventPlaylist = event.getActivePlaylist();
				if (activePlaylist) {
					let track: EventTrack = activePlaylist.getActiveTrack();
					if (track) {
						trackid = track.id;
					} else {
						res.status(ResponseStatusTypes.BAD_REQUEST).send(new ServerError(ErrorCodes.NO_ACTIVE_TRACK));
						return ;
					}
				} else {
					res.status(ResponseStatusTypes.BAD_REQUEST).send(new ServerError(ErrorCodes.NO_ACTIVE_PLAYLIST));
					return ;
				}
			}
			let playlist: EventPlaylist = event.getEventPlaylistWithTrack(trackid);
			let track: EventTrack = playlist ? playlist.getTrack(trackid) : null;
			if (!track) {
				res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_TRACK));
				return ;
			}
			let trackInfo: any = {trackid: track.id, playlistid: playlist.id};
			// if timestamp or trackPosition is missing
			if (timestamp == undefined || trackPosition == undefined) {
				// if owner 
				if (event.userIsOwner(user)) {
					if (!timestamp)
						res.status(ResponseStatusTypes.BAD_REQUEST).send(new ServerError(ErrorCodes.MISSING_PARAMETER, null, 'timestamp', 'body'));
					else
						res.status(ResponseStatusTypes.BAD_REQUEST).send(new ServerError(ErrorCodes.MISSING_PARAMETER, null, 'trackPosition', 'body'));
					return ;
				} else {
					// if timestamp or trackPosition not included, socketio to owner to request timestamp and trackPosition; return ;
					switch (playeraction) {
						case 'play':
							socketController.emitToUser(event.owner, SocketActions.EVENT_TRACK_PLAY_REQUEST, {by: user.id, ...trackInfo});
							break;
						case 'pause':
							socketController.emitToUser(event.owner, SocketActions.EVENT_TRACK_PAUSE_REQUEST, {by: user.id, ...trackInfo});
							break;
						case 'seek':
							socketController.emitToUser(event.owner, SocketActions.EVENT_TRACK_SEEK_REQUEST, {by: user.id, ...trackInfo, trackPosition});
							break;
						default:
							res.status(ResponseStatusTypes.BAD_REQUEST).send(new ServerError(ErrorCodes.INVALID_EVENT_ACTION));
							return ;
					}
					res.status(ResponseStatusTypes.OK).send({message: 'sent request to event owner'});
					return ;
				}
			}
			trackPosition = parseInt(<any>trackPosition);
			let updated: boolean = false;
			switch (playeraction) {
				case 'play':
					updated = await event.play(playlist.id, trackid, timestamp, trackPosition);
					break;
				case 'pause':
					updated = await event.pause(timestamp, trackPosition);
					break;
				case 'seek':
					updated = await event.seek(timestamp, trackPosition);
					break;
				default:
					res.status(ResponseStatusTypes.BAD_REQUEST).send(new ServerError(ErrorCodes.INVALID_EVENT_ACTION));
					return ;
			}
			if (!updated) {
				res.status(ResponseStatusTypes.OK).send({message: 'no change'});
				return ;
			}
			await event.save();
			trackInfo.timestamp = timestamp;
			trackInfo.trackPosition = event.trackPosition;
			switch (playeraction) {
				case 'play':
					socketController.emitToRoom('/events', event.id, SocketActions.EVENT_TRACK_PLAY, {by: user.id, ...trackInfo});
					res.status(ResponseStatusTypes.OK).send({...(await event.expand(user)), message: 'track playing'});
					break;
				case 'pause':
					socketController.emitToRoom('/events', event.id, SocketActions.EVENT_TRACK_PAUSE, {by: user.id, ...trackInfo});
					res.status(ResponseStatusTypes.OK).send({...(await event.expand(user)), message: 'track paused'});
					break;
				case 'seek':
					socketController.emitToRoom('/events', event.id, SocketActions.EVENT_TRACK_SEEK, {by: user.id, ...trackInfo});
					res.status(ResponseStatusTypes.OK).send({...(await event.expand(user)), message: 'updated track position'});
					break;
				default:
					res.status(ResponseStatusTypes.BAD_REQUEST).send(new ServerError(ErrorCodes.INVALID_EVENT_ACTION));
					return ;
			}
		});
	}

	public async trackVote(req: Request, res: Response): Promise<any> {
		const mandatoryParameters: Parameter[] = [new Parameter('apiKey', 'query'), new Parameter('eventid', 'param'), new Parameter('trackid', 'param'), new Parameter('vote', 'param')];

		const apiKey: string = req.query.apiKey;
		const eventid: string = req.params.eventid;
		let   trackid: string = req.params.trackid;
		const vote: string = req.params.vote;
		const validator: Validator = new Validator({apiKey, eventid, trackid, vote}, mandatoryParameters);
		if (await validator.err) {
			res.status(ResponseStatusTypes.BAD_REQUEST).send(await validator.err);			
			return ;
		}

		const method: string = req.method;

		// check if user exists and account is activated
		let user: User = await User.get({apiKey});
				
		if (!user) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.INVALID_API_KEY));
			return ;
		}
		if (!user.activated) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.USER_ACCOUNT_NOT_ACTIVATED));
			return ;
		}

		await acquireLock(lockKeys.event(eventid), async () => {
			// check that event exists
			let event: Event = await Event.get({id: eventid});
			if (!event) {
				res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_EVENT));
				return ;
			}

			if (!event.votingActive) {
				res.status(ResponseStatusTypes.BAD_REQUEST).send(new ServerError(ErrorCodes.VOTING_NOT_ACTIVE));
				return ;
			}
			if (!event.userCanAccess(user)) {
				res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.EDIT_NOT_AUTHORIZED));
				return ;
			}

			const action: string = `${method == 'PUT' ? '' : 'un'}${vote == 'like' ? '' : 'dis'}likeTrack`;
			let playlist: EventPlaylist = event.getEventPlaylistWithTrack(trackid);
			if (!playlist) {
				res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_TRACK));
				return ;
			}
			let track: EventTrack = playlist.getTrack(trackid);
			if (method == 'PUT') {
				var oldLength = vote == 'like' ? track.dislikes.length : track.likes.length; 
			}
			let oldIndex: number = playlist.unplayedTracks.findIndex((track: EventTrack) => track.id == trackid);
			let update: boolean = event[action](playlist.id, trackid, user.id);
			if (!update) {
				res.status(ResponseStatusTypes.OK).send({message: 'no change'});
				return ;
			}
			let newIndex: number = playlist.unplayedTracks.findIndex((track: EventTrack) => track.id == trackid);
			await event.save();
			let trackInfo: any = {userid: user.id, trackid: track.id, playlistid: playlist.id, trackLikesTotal: track.likes.length, trackDislikesTotal: track.dislikes.length};
			socketController.emitToRoom('/events', event.id, SocketActions[`EVENT_TRACK_${method == 'PUT' ? '' : 'UN'}${vote == 'like' ? '' : 'DIS'}LIKE`], {by: user.id, ...trackInfo});
			if (method == 'PUT' && (oldLength != (vote == 'like' ? track.dislikes.length : track.likes.length))) {
				socketController.emitToRoom('/events', event.id, SocketActions[`EVENT_TRACK_UN${vote == 'like' ? 'DIS' : ''}LIKE`], {by: user.id, ...trackInfo});				
			}
			if (oldIndex >= 0 && newIndex >= 0 && oldIndex != newIndex) {
				socketController.emitToRoom('/events', event.id, SocketActions.EVENT_TRACK_MOVE, {by: user.id, ...trackInfo, index: newIndex});
			}
			res.status(ResponseStatusTypes.OK).send({...track, message: `track ${method == 'PUT' ? '' : 'un'}${vote == 'like' ? '' : 'dis'}liked`});
		});
	}

	public async trackMove(req: Request, res: Response): Promise<any> {
		const mandatoryParameters: Parameter[] = [new Parameter('apiKey', 'query'), new Parameter('eventid', 'param'), new Parameter('userid', 'param'), new Parameter('index', 'body')];

		const apiKey: string = req.query.apiKey;
		const eventid: string = req.params.eventid;
		let   trackid: string = req.params.trackid;
		let   index: string = req.body.index;
		const validator: Validator = new Validator({apiKey, eventid, trackid, index}, mandatoryParameters);
		if (await validator.err) {
			res.status(ResponseStatusTypes.BAD_REQUEST).send(await validator.err);			
			return ;
		}

		// check if user exists and account is activated
		let user: User = await User.get({apiKey});
		
		if (!user) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.INVALID_API_KEY));
			return ;
		}
		if (!user.activated) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.USER_ACCOUNT_NOT_ACTIVATED));
			return ;
		}

		await acquireLock(lockKeys.event(eventid), async () => {
			// check that event exists
			let event: Event = await Event.get({id: eventid});
			if (!event) {
				res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_EVENT));
				return ;
			}

			// check that event is ready for voting and that user has rights
			if (!event.votingActive) {
				res.status(ResponseStatusTypes.BAD_REQUEST).send(new ServerError(ErrorCodes.VOTING_NOT_ACTIVE));
				return ;
			}
			if (!event.userCanAccess(user)) {
				res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.EDIT_NOT_AUTHORIZED));
				return ;
			}

			// check that track exists in event.eventplaylists.unplayed
			let playlist: EventPlaylist = event.getEventPlaylistWithTrack(trackid);
			if (!playlist) {
				res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_TRACK));
				return ;
			}
			if (!playlist.unplayedTracks.find(x => x.id == trackid)) {
				res.status(ResponseStatusTypes.NOT_FOUND).send(new ServerError(ErrorCodes.NO_TRACK));
				return ;
			}

			let track: EventTrack = playlist.getTrack(trackid);
			let update: boolean = event.moveTrack(playlist.id, trackid, parseInt(index));
			if (!update) {
				res.status(ResponseStatusTypes.OK).send({message: 'no change'});
				return ;
			}
			await event.save();
			let trackInfo = {trackid: track.id, playlistid: playlist.id, index: playlist.unplayedTracks.findIndex(x => x.id == trackid)};
			socketController.emitToRoom('/events', event.id, SocketActions.EVENT_TRACK_MOVE, {by: user.id, ...trackInfo});
			res.status(ResponseStatusTypes.OK).send({...track, message: `track moved to index ${trackInfo.index}`});
		});
	}

	public async search(req: Request, res: Response) : Promise<any> {
		const mandatoryParameters: Parameter[] = [new Parameter('apiKey', 'query'), new Parameter('keyword', 'query')];

		const apiKey: string = req.query.apiKey;
		const keyword: string = req.query.keyword;

		const validator: Validator = new Validator({apiKey, keyword}, mandatoryParameters);
		if (await validator.err) {
			res.status(ResponseStatusTypes.BAD_REQUEST).send(await validator.err);			
			return ;
		}

		const user = await User.get({apiKey: apiKey});
		if (!user) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.INVALID_API_KEY));
			return ;
		}
		if (!user.activated) {
			res.status(ResponseStatusTypes.UNAUTHORIZED).send(new ServerError(ErrorCodes.USER_ACCOUNT_NOT_ACTIVATED));
			return ;
		}

		let events: Event[] = await Event.search(keyword);
		
		res.status(ResponseStatusTypes.OK).send({results: events});
		return ;
	}
	
	private updateJobs(event: Event, votingSchedule?: Schedule, eventSchedule?: Schedule): boolean {
		let now: Date;
		if (!votingSchedule && !eventSchedule) return false;
		
		[ {name: 'voting', schedule: votingSchedule},
		  {name: 'event' , schedule: eventSchedule } ]
			.forEach(x => updateScheduleJobs(<'voting'|'event'>x.name, x.schedule));
		
		return true;

		function updateScheduleJobs(name: 'voting'|'event', schedule: Schedule) {
			if (schedule == undefined) return ;
			let name2: string = name.capitalize();
			if (schedule == null) {
				event[`end${name2}`]();
				setJob(event, name, 'start', null);
				setJob(event, name, 'end', null);
				return ;
			}
			try {
				var start: Date = new Date(schedule.start);
				var end: Date = new Date(schedule.end);
			} catch (e) {
				// console.log(chalk.red(`[EventController.updateJobs] invalid ${name}Schedule Dates`));
				return ;
			}
			now = new Date();
			if (start < now && now < end) {
				event[`start${name2}`]();
				setJob(event, name, 'start', null);
				setJob(event, name, 'end', end);
			} else if (end < now) {
				event[`end${name2}`]();
				setJob(event, name, 'start', null);
				setJob(event, name, 'end', null);
			} else if (now < start) {
				event[`end${name2}`]();
				setJob(event, name, 'start', start);
				setJob(event, name, 'end', end);
			}
		}

		function setJob(event: Event, scheduleType: 'voting'|'event', action: 'start'|'end', date: Date) {
			let jobs: { [name: string]: schedule.Job } = eventController.jobs[event.id];
			if (!jobs) {
				eventController.jobs[event.id] = {};
				jobs = eventController.jobs[event.id];
			}
			let name: string = action + scheduleType.capitalize();
			if (jobs.hasOwnProperty(name)) {
				jobs[name].cancel;
				delete jobs[name];
			}
			
			if (!date) return ;

			const job = async () => {
				// console.log(chalk.yellowBright(`executing job ${event.id}, ${name}, ${date}`))
				await acquireLock(lockKeys.event(event.id), async () => {
					event = await Event.get({id: event.id});
					if (!event) {
						// console.log(chalk.red(`[EventController.setJob] event not found`));
						return ;
					}
					await event[name]();
					await event.save();
				});
				if (!event) return ;
				switch (name) {
					case 'startVoting':
						socketController.emitToRoom('/events', event.id, SocketActions.EVENT_VOTING_STARTED, {by: null, eventPlaylists: event.eventPlaylists});
						socketController.emitToRoom('/events', event.id, SocketActions.EVENT_STARTER_PACK, event.getStarterPack());
						break ;
					case 'startEvent':
						socketController.emitToRoom('/events', event.id, SocketActions.EVENT_EVENT_STARTED, {by: null, eventPlaylists: event.eventPlaylists});
						socketController.emitToRoom('/events', event.id, SocketActions.EVENT_STARTER_PACK, event.getStarterPack());
						break ;
					case 'endVoting':
						socketController.emitToRoom('/events', event.id, SocketActions.EVENT_VOTING_ENDED, (event.votingActive || event.eventActive) ? {by: null, eventPlaylists: event.eventPlaylists} : {by: null, playlists: (await expandPlaylists(event.playlists))});
						break ;
					case 'endEvent':
						socketController.emitToRoom('/events', event.id, SocketActions.EVENT_EVENT_ENDED, (event.votingActive || event.eventActive) ? {by: null, eventPlaylists: event.eventPlaylists} : {by: null, playlists: (await expandPlaylists(event.playlists))});
						break ;
				}
			}

			jobs[name] = schedule.scheduleJob(date, async () => {
				await job();
				if (!jobs[name]) return ;
				jobs[name].cancel();
				delete jobs[name];
			});
		}
	}

	public destroyJobs() {
		Object.keys(this.jobs).forEach(eventid => {
			Object.keys(this.jobs[eventid]).forEach(name => {
				this.jobs[eventid][name].cancel();
				delete this.jobs[eventid][name];
			});
			delete this.jobs[eventid];
		});
	}

	public async rebuildJobs() {
		// get all Events
		let events: Event[] = await Event.getAll(true);
		// recreate jobs for each event in database
		events.forEach((event: Event) => eventController.updateJobs(
			event,
			event.votingSchedule ? event.votingSchedule : null,
			event.eventSchedule ? event.eventSchedule : null)
		)
	}

	public initSocketIO(ioServer: socketio.Server) {
		io = ioServer.of('/events');
		io.on('connection', async function(socket) {
			const mandatoryParameters: Parameter[] = [new Parameter('apiKey', 'query'), new Parameter('eventid', 'query')];
			
			const apiKey: string = socket.handshake.query.apiKey;
			const eventid: string = socket.handshake.query.eventid;
			
			const validator: Validator = new Validator({apiKey, eventid}, mandatoryParameters);
			if (await validator.err) {	
				socket.emit(SocketActions.ERROR, await validator.err);
				socket.disconnect();
				return ;
			}
			
			const user: User = await User.get({apiKey});
			if (!user) {
				socket.emit(SocketActions.ERROR, new ServerError(ErrorCodes.INVALID_API_KEY));
				socket.disconnect();
				return ;
			}
			if (!user.activated) {
				socket.emit(SocketActions.ERROR, new ServerError(ErrorCodes.USER_ACCOUNT_NOT_ACTIVATED));
				socket.disconnect();
				return ;
			}
			
			const event = await Event.get({id: eventid});
			if (!event) {
				socket.emit(SocketActions.ERROR, new ServerError(ErrorCodes.NO_EVENT));
				socket.disconnect();
				return ;
			}
			if (!event.userCanAccess(user)) {
				socket.emit(SocketActions.ERROR, new ServerError(ErrorCodes.ACCESS_NOT_AUTHORIZED));
				socket.disconnect();
				return ;
			}

			let isRegistered = socketController.registerClient('/events', event.id, user.id, socket, !event.userIsOwner(user));
			
			socket.on('disconnect', async () => {
				await isRegistered;
				let event = await Event.get({id: eventid});

				if (event && event.userIsOwner(user)) {
					await acquireLock(lockKeys.event(event.id), async () => {
						event = await Event.get({id: eventid});
						if (event) {
							event.trackPosition = 0;
							let playlist: EventPlaylist = event.getActivePlaylist();
							if (playlist) {
								let track: EventTrack = playlist.getActiveTrack();
								if (track) {
									track.stop();
									event.seek(new Date().toISOString(), 0);
									let trackInfo: any = {trackid: track.id, playlistid: playlist.id, trackPosition: event.trackPosition, timestamp: event.lastTrackActionTimestamp, isPlaying: false};
									socketController.emitToRoom('/events', event.id, SocketActions.EVENT_TRACK_SEEK, {by: event.owner, ...trackInfo});
								}
							}
							await event.save();
							socketController.emitToRoom('/events', event.id, SocketActions.EVENT_OWNER_DISCONNECT);
						}
					});
				}
				await socketController.unregisterClient(socket.id);
			});
			socket.on(SocketActions.REQUEST_DISCONNECT, async () => {
				await socketController.unregisterClient(socket.id);
				socket.disconnect();
			});

			await isRegistered;
			if (user.id == event.owner) {
				socketController.emitToRoom('/events', event.id, SocketActions.EVENT_OWNER_CONNECT);
			}
			socketController.emitToUser(user.id, SocketActions.EVENT_STARTER_PACK, event.getStarterPack());
		});
	}
}

async function expandPlaylists(playlistids: string[]) {
	return (await Promise.all(playlistids.map(id => Playlist.get({id})))).filter(x => x != undefined);
}

const eventController: EventController = new EventController();
export {
	EventController,
	eventController
}	