import { Setting, MasterSetting, SettingValues} from "../classes/Setting";
import { ServerError, ErrorCodes } from "../utils/errorTypes"; 
import { User } from "../classes/User";

class SettingsController {

	public masterSettings: MasterSetting[] = [
		new MasterSetting('email', SettingValues.PUBLIC, [SettingValues.PUBLIC, SettingValues.FRIENDS, SettingValues.PRIVATE]),
		new MasterSetting('displayName', SettingValues.PUBLIC, [SettingValues.PUBLIC, SettingValues.FRIENDS, SettingValues.PRIVATE]),
		new MasterSetting('firstName', SettingValues.PUBLIC, [SettingValues.PUBLIC, SettingValues.FRIENDS, SettingValues.PRIVATE]),
		new MasterSetting('lastName', SettingValues.PUBLIC, [SettingValues.PUBLIC, SettingValues.FRIENDS, SettingValues.PRIVATE]),
		new MasterSetting('friends', SettingValues.PUBLIC, [SettingValues.PUBLIC, SettingValues.FRIENDS, SettingValues.PRIVATE]),
		new MasterSetting('musicalPreferences', SettingValues.PUBLIC, [SettingValues.PUBLIC, SettingValues.FRIENDS, SettingValues.PRIVATE]),
		new MasterSetting('playlists', SettingValues.PUBLIC, [SettingValues.PUBLIC, SettingValues.FRIENDS, SettingValues.PRIVATE]),
		new MasterSetting('guestInPlaylists', SettingValues.FRIENDS, [SettingValues.PUBLIC, SettingValues.FRIENDS, SettingValues.PRIVATE]),
		new MasterSetting('editorInPlaylists', SettingValues.FRIENDS, [SettingValues.PUBLIC, SettingValues.FRIENDS, SettingValues.PRIVATE]),
		new MasterSetting('events', SettingValues.PUBLIC, [SettingValues.PUBLIC, SettingValues.FRIENDS, SettingValues.PRIVATE]),
		new MasterSetting('guestInEvents', SettingValues.FRIENDS, [SettingValues.PUBLIC, SettingValues.FRIENDS, SettingValues.PRIVATE]),
		new MasterSetting('djInEvents', SettingValues.FRIENDS, [SettingValues.PUBLIC, SettingValues.FRIENDS, SettingValues.PRIVATE]),
	];

	public getDefaultSettings() : Setting[] {
		return this.masterSettings.map(x => x.toSetting());
	}

	public filterUserAttributesByClientRelationship(client: User, user: User) {
		// return everything public if client is NOT user's friend OR if client unidentified, or if client is user
		const settingValues: SettingValues[] = [SettingValues.PUBLIC];
		if (client) {
			if (client.id == user.id) {
				settingValues.push(SettingValues.FRIENDS, SettingValues.PRIVATE);
			} else if (user.friends.find(x => x == client.id)) {
				settingValues.push(SettingValues.FRIENDS)
			}
		}	

		// always return id, avatar, isPremium
		let ret: any = {
			id: user.id,
			avatar: user.avatar,
			isPremium: user.isPremium
		};

		// if self, return apiKey, friendships
		if (client.id == user.id) {
			ret = {
				...ret,
				facebookid: user.facebookid,
				theme: user.theme,
				apiKey: user.apiKey,
				friendships: user.friendships
			}
		}

		// returns stuff based on settings
		ret = {
			...ret,
			...Object.keys(user).reduce((o: object, key: string) : object => {
					const setting: Setting = user.settings.find(x => x.name == key);
					if (!setting) return o;
					if (!settingValues.includes(setting.value as SettingValues)) return o;
					o[key] = user[key];
					return o;
				},
				{}
			)
		};

		return ret;
	}
}

const settingsController = new SettingsController();
export default settingsController;