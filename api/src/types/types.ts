export type SortingType = "move" | "vote";
export type Visibility = "PUBLIC" | "PRIVATE";
export type Editing = "PUBLIC" | "PRIVATE";
export type Theme = "light" | 'dark';
export interface Schedule {
	start: string,
	end: string
};
export interface Coordinates {
	longitude: number,
	latitude: number
}
