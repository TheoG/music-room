### API Requirements
Mongodb (Update path (export PATH=/usr/local/mongodb/bin:$PATH))
Node
### Config
Set up the private_config.json and config.json files

	npm run netconf
### Install 
	npm i
    npm run db-start
    npm run db-init
### Start Dev Server
	npm run dev
### Build
	npm build
### Start
	npm run start
### Tests
##### Unit Tests
	npm test
##### View coverage
	npm test
	npm run view-coverage
##### Load Tests
	npm -g artillery
	npm run load-test
### Database Management
##### Load Dummy Data
	npm run db-dummy
##### Import Default Avatars
Place images in ./api/public/avatars/default-avatars/to-import/

    npm run import-avatars
###### Export Database
	npm run db-export [-- -o [exportDir]]
###### Import Database
	npm run db-import [-- -o [importDir]]
### Update ServerName
	npm run netconf
### Documentation
	npm run doc
### Remove logs
	npm run rm-logs

### ENVIRONMENT VARIABLES:

##### NODE_ENV:

'test':
- does not send emails to users
- automatically activates users upon creation
- automatically sets users as premium upon creation
- does not limit load

##### MUSIC_ROOM_SEND_EMAILS:
'true':
- overrides NODE_ENV=test

'false:
- does not send emails to users

##### MUSIC_ROOM_USER_AUTO_ACTIVATE
'true':
- automatically activates users upon creation

'false':
- override NODE_ENV=test

##### MUSIC_ROOM_USER_AUTO_PREMIUM
'true':
- automatically sets users as premium upon creation

'false':
- override NODE_ENV=test

##### MUSIC_ROOM_DISPLAY_REQUESTS
'true':
- logs requests in the console

##### MUSIC_ROOM_LIMIT_LOAD
'true'  
- limits load

'false'
- override NODE_ENV=test

#### By default (providing no env variables):
- emails are sent
- users are not automatically activated
- users are not automatically set to premium
- requests are not displayed in the console
- load is limited
