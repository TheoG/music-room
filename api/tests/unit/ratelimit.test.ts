import * as chai from 'chai';
import chaiHttp = require('chai-http');
chai.use(chaiHttp);
const expect = chai.expect;

import { reset, createUser, printret } from './common';
import { EnvVar } from '../../src/utils/env';

describe.skip('Rate Limiter', function() {
	let ret: any;

	before(async function() {
		if (process.env[EnvVar.MUSIC_ROOM_LIMIT_LOAD] != 'true') {
			throw ('must set MUSIC_ROOM_LIMIT_LOAD to true for this test');
		}
		await reset();
	});

	it('Should limit user creation to 10', async function() {
		ret = await Promise.all('abcdefghijklmnop'.split('').map(x => new Promise(async (resolve, reject) => {
			let r = await createUser({email: `${x}@${x}.com`, password: `${x}${x}${x}${x}${x}${x}`});
			resolve(r.status);
		})));
		expect(ret.filter(x => x == 200)).length(10)
	});

});