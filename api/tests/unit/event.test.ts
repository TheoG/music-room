import * as chai from 'chai';
import chaiHttp = require('chai-http');
import chaiAsPromised = require('chai-as-promised');
chai.use(chaiHttp);
chai.use(chaiAsPromised);
const expect = chai.expect;
import * as socketio from "socket.io-client";
import SocketActions from "../../src/utils/SocketActions";

import { reset, users, createUser, createEvent, getEvent, getEvents, updateEvent, deleteEvent, createPlaylist, addGuestToEvent, addGuestsToEvent, getUser, addDJToEvent, addDJsToEvent, removeGuestFromEvent, removeGuestsFromEvent, removeDJFromEvent, removeDJsFromEvent, addPlaylistsToEvent, addPlaylistToEvent, getPlaylist, updatePlaylist, addGuestToPlaylist, addEditorToPlaylist, removePlaylistFromEvent, removePlaylistsFromEvent, addTrack, eventStartEvent, eventStartVoting, eventEndVoting, eventEndEvent, playerAction, eventSocketConnect, voteEventTrack, moveEventTrack, deleteEvents, searchEvent, printret } from './common';
import app from "../../src/app";
import ResponseStatusTypes from "../../src/utils/ResponseStatusTypes";
import { Visibility, Editing, SortingType, Schedule, Coordinates } from "../../src/types/types";

import { Event } from '../../src/classes/Event';
import { socketController } from '../../src/controllers/socketController';

const search_limit: number = require('../../config.json').mongo_search_limit;

interface user {
	apiKey: string;
	id: string;
	playlists?: string[]; 
}

interface event {
	id?: string
	name: string,
}

describe('Event', function() {
	this.timeout(10000);
	let ret: any;
	
	let owner: user;
	let guest: user;
	let dj: user;
	let other: user;

	let eventName: string = 'My event';
	let eventDescription: string = 'This is an awesome event.'
	let eventVotingSchedule: Schedule = {start: (new Date(2019, 10, 1, 6, 0, 0).toISOString()), end: (new Date(2019, 10, 1, 18, 0, 0).toISOString())};
	let eventEventSchedule: Schedule = {start: (new Date(2019, 10, 1, 18, 0, 0).toISOString()), end: (new Date(2019, 10, 1, 23, 0, 0).toISOString())};
	let eventCoordinates: Coordinates = {latitude: 20, longitude: 40};

	let eventid: string;
	let eventid2: string;

	let playlistid: string;
	let playlistid1: string;
	let playlistid2: string;
	let playlistid3: string;

	describe('Create Event', function() {
		before(async function() {
			await reset();
			ret = await Promise.all(users.map(createUser));
			[owner, guest, dj, other] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});
		});
		it('Should create an event', async function() {
			ret = await createEvent(owner.apiKey, {name: eventName});
			expect(ret.status).equals(ResponseStatusTypes.OK);
			expect(ret.body.name).equals(eventName);
			expect(ret.body.visibility).equals('PUBLIC');
			expect(ret.body.owner.id).equals(owner.id);
		});
		it('Should create an event with all options', async function() {
			ret = await createEvent(owner.apiKey, {name: eventName, description: eventDescription, visibility: 'PRIVATE', votingSchedule: eventVotingSchedule, eventSchedule: eventEventSchedule, coordinates: eventCoordinates});
			expect(ret.status).equals(ResponseStatusTypes.OK);
			expect(ret.body.name).equals(eventName);
			expect(ret.body.visibility).equals('PRIVATE');
			expect(ret.body.description).equals(eventDescription);
			expect(ret.body.votingSchedule).deep.equals(eventVotingSchedule);
			expect(ret.body.eventSchedule).deep.equals(eventEventSchedule);
			expect(ret.body.coordinates).deep.equals(eventCoordinates);
			expect(ret.body.owner.id).equals(owner.id);
		});
		describe('It should fail to create an event if ...', function() {
			before(async function() {
				await reset();
				ret = await Promise.all(users.map(createUser));
				[owner, guest, dj, other] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});	
			});
			it('apiKey not given', async function() {
				ret = await chai.request(app.app)
					.post(`/events`)
					.send({name: eventName});
				expect(ret.status).equals(ResponseStatusTypes.BAD_REQUEST);
				ret = await getEvents(owner.apiKey);
				expect(ret.body.owner).length(0);
				expect(ret.body.dj).length(0);
				expect(ret.body.guest).length(0);
			});
			it('apiKey not valid', async function() {
				ret = await createEvent('0123456789', {name: eventName});
				expect(ret.status).equals(ResponseStatusTypes.UNAUTHORIZED);
				ret = await getEvents(owner.apiKey);
				expect(ret.body.owner).length(0);
				expect(ret.body.dj).length(0);
				expect(ret.body.guest).length(0);
			});
			it('name not given', async function() {
				ret = await chai.request(app.app)
					.post(`/events?apiKey=${owner.apiKey}`)
					.send({});
				expect(ret.status).equals(ResponseStatusTypes.BAD_REQUEST);
				ret = await getEvents(owner.apiKey);
				expect(ret.body.owner).length(0);
				expect(ret.body.dj).length(0);
				expect(ret.body.guest).length(0);
			});
			describe('option invalid', function() {
				it('visibility', async function() {
					ret = await chai.request(app.app)
						.post(`/events?apiKey=${owner.apiKey}`)
						.send({name: eventName, visibility: 'SUPERPRIVATE'});
					expect(ret.status).equals(ResponseStatusTypes.BAD_REQUEST);
					ret = await getEvents(owner.apiKey);
					expect(ret.body.owner).length(0);
					expect(ret.body.dj).length(0);
					expect(ret.body.guest).length(0);
				});
				describe('schedule', function() {
					it('not Schedule object (1)', async function() {
						ret = await chai.request(app.app)
							.post(`/events?apiKey=${owner.apiKey}`)
							.send({name: eventName, votingSchedule: {testing: '0123456789'}});
						expect(ret.status).equals(ResponseStatusTypes.BAD_REQUEST);
						ret = await getEvents(owner.apiKey);
						expect(ret.body.owner).length(0);
						expect(ret.body.dj).length(0);
						expect(ret.body.guest).length(0);
					});
					it('not Schedule object (2)', async function() {
						ret = await chai.request(app.app)
							.post(`/events?apiKey=${owner.apiKey}`)
							.send({name: eventName, eventSchedule: '0123456789'});
						expect(ret.status).equals(ResponseStatusTypes.BAD_REQUEST);
						ret = await getEvents(owner.apiKey);
						expect(ret.body.owner).length(0);
						expect(ret.body.dj).length(0);
						expect(ret.body.guest).length(0);
					});
					it('invalid start or end values (1)', async function() {
						ret = await createEvent(owner.apiKey, {name: eventName, eventSchedule: {start: 'lalaland', end: 'toodles'}})
						expect(ret.status).equals(ResponseStatusTypes.BAD_REQUEST);
						ret = await getEvents(owner.apiKey);
						expect(ret.body.owner).length(0);
						expect(ret.body.dj).length(0);
						expect(ret.body.guest).length(0);
					});
					it('invalid start or end values (2)', async function() {
						ret = await chai.request(app.app)
							.post(`/events?apiKey=${owner.apiKey}`)
							.send({name: eventName, eventSchedule: {start: {tets: 'test'}, end: (new Date()).toISOString()}});
						expect(ret.status).equals(ResponseStatusTypes.BAD_REQUEST);
						ret = await getEvents(owner.apiKey);
						expect(ret.body.owner).length(0);
						expect(ret.body.dj).length(0);
						expect(ret.body.guest).length(0);
					});
					it('start comes before end', async function() {
						ret = await chai.request(app.app)
							.post(`/events?apiKey=${owner.apiKey}`)
							.send({name: eventName, eventSchedule: { start: (new Date()).setHours(20), end: (new Date()).setHours(0) }});
						expect(ret.status).equals(ResponseStatusTypes.BAD_REQUEST);
						ret = await getEvents(owner.apiKey);
						expect(ret.body.owner).length(0);
						expect(ret.body.dj).length(0);
						expect(ret.body.guest).length(0);
					});
				});
				describe('coordinates', function() {
					it('not Coordinates object (1)', async function() {
						ret = await chai.request(app.app)
							.post(`/events?apiKey=${owner.apiKey}`)
							.send({name: eventName, coordinates: {lol: '1023456'}});
						expect(ret.status).equals(ResponseStatusTypes.BAD_REQUEST);
						ret = await getEvents(owner.apiKey);
						expect(ret.body.owner).length(0);
						expect(ret.body.dj).length(0);
						expect(ret.body.guest).length(0);
					});
					it('not Coordinates object (2)', async function() {
						ret = await chai.request(app.app)
							.post(`/events?apiKey=${owner.apiKey}`)
							.send({name: eventName, coordinates: {latitude: {test: 123456}}});
						expect(ret.status).equals(ResponseStatusTypes.BAD_REQUEST);
						ret = await getEvents(owner.apiKey);
						expect(ret.body.owner).length(0);
						expect(ret.body.dj).length(0);
						expect(ret.body.guest).length(0);
					});
					it('Coordinates.latitude invalid', async function() {
						ret = await createEvent(owner.apiKey, {name: eventName, coordinates: {latitude: 500, longitude: 20}})
						expect(ret.status).equals(ResponseStatusTypes.BAD_REQUEST);
						ret = await getEvents(owner.apiKey);
						expect(ret.body.owner).length(0);
						expect(ret.body.dj).length(0);
						expect(ret.body.guest).length(0);
					});
					it('Coordinates.longitude invalid', async function() {
						ret = await createEvent(owner.apiKey, {name: eventName, coordinates: {latitude: 10, longitude: -200.322}})
						expect(ret.status).equals(ResponseStatusTypes.BAD_REQUEST);
						ret = await getEvents(owner.apiKey);
						expect(ret.body.owner).length(0);
						expect(ret.body.dj).length(0);
						expect(ret.body.guest).length(0);
					});
				});
			});
		});
	});

	describe('Get Event', function() {
		
		before(async function() {
			await reset();
			ret = await Promise.all(users.map(createUser));
			[owner, guest, dj, other] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});
			eventid = (await createEvent(owner.apiKey, {name: eventName})).body.id;
			[playlistid1, playlistid2] = [(await createPlaylist(owner.apiKey, {name: 'p1'})).body.id, (await createPlaylist(owner.apiKey, {name: 'p2'})).body.id];
		});

		it('Should get the event', async function() {
			ret = await getEvent(owner.apiKey, eventid);
			expect(ret.status).equals(ResponseStatusTypes.OK);
			expect(ret.body.owner.id).equals(owner.id);
			// should not include playlists created by owner and not added
			expect(ret.body.playlists).length(0);
		});

		describe('Should fail if ...', function() {
			it('apiKey not provided', async function() {
				ret = await chai.request(app.app)
					.get(`/events/${eventid}`)
					.send();
				expect(ret.status).equals(ResponseStatusTypes.BAD_REQUEST);
			});
			it('apiKey invalid', async function() {
				ret = await chai.request(app.app)
					.get(`/events/${eventid}?apiKey=0123456789`)
					.send();
				expect(ret.status).equals(ResponseStatusTypes.UNAUTHORIZED);
			});
			it('eventid not provided', async function() {
				ret = await chai.request(app.app)
					.get(`/events?apiKey=${owner.apiKey}`)
					.send();
				expect(ret.status).equals(ResponseStatusTypes.OK); // equvalient to getEvents
				expect(ret.body).not.haveOwnProperty('id');
			});
			it('eventid invalid', async function() {
				ret = await chai.request(app.app)
					.get(`/events/0123456789?apiKey=${owner.apiKey}`)
					.send();
				expect(ret.status).equals(ResponseStatusTypes.NOT_FOUND);
			});
			it('event visibility is private', async function() {
				ret = await updateEvent(owner.apiKey, eventid, {visibility: 'PRIVATE'});
				ret = await getEvent(other.apiKey, eventid);
				expect(ret.status).equals(ResponseStatusTypes.UNAUTHORIZED);
			});
		});
	});

	describe('Get Events', function() {
		before(async function() {
			await reset();
			ret = await Promise.all(users.map(createUser));
			[owner, guest, dj, other] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});
			ret = await Promise.all([eventName, eventName + '2'].map(name => createEvent(owner.apiKey, {name})));
			eventid = ret[0].body.id;
			eventid2 = ret[1].body.id;
			await Promise.all([eventid, eventid2].map(id => {
				return Promise.all([
					// invite dj
					addDJToEvent(owner.apiKey, id, dj.id),
					// invite guest
					addGuestToEvent(owner.apiKey, id, guest.id)
				]);
			}));
		});
		it('Owner should get all events', async function() {
			ret = await getEvents(owner.apiKey);
			expect(ret.status).equals(ResponseStatusTypes.OK);
			expect(ret.body.owner).length(2);
			expect(ret.body.guest).length(0);
			expect(ret.body.dj).length(0);
		});
		it('Guest should get event', async function() {
			ret = await getEvents(guest.apiKey);
			expect(ret.status).equals(ResponseStatusTypes.OK);
			expect(ret.body.owner).length(0);
			expect(ret.body.guest).length(2);
			expect(ret.body.dj).length(0);
		});
		it('DJ should get event', async function() {
			ret = await getEvents(dj.apiKey);
			expect(ret.status).equals(ResponseStatusTypes.OK);
			expect(ret.body.owner).length(0);
			expect(ret.body.guest).length(0);
			expect(ret.body.dj).length(2);
		});
		describe('Should fail if ...', function() {
			it('apiKey not provided', async function() {
				ret = await chai.request(app.app)
					.get(`/events`)
					.send();
				expect(ret.status).equals(ResponseStatusTypes.BAD_REQUEST);
			});
			it('apiKey invalid', async function() {
				ret = await chai.request(app.app)
					.get(`/events?apiKey=0123456789`)
					.send();
				expect(ret.status).equals(ResponseStatusTypes.UNAUTHORIZED);
			});
			it('event visibility is private', async function() {
				ret = await Promise.all([eventid, eventid2].map(id => updateEvent(owner.apiKey, id, {visibility: 'PRIVATE'})));
				ret = await getEvents(other.apiKey);
				expect(ret.status).equals(ResponseStatusTypes.OK);
				expect(ret.body.owner).length(0);
				expect(ret.body.guest).length(0);
				expect(ret.body.dj).length(0);
			});
		});
	});

	describe('Update Event', function() {
		let newName: string = 'new name';
		let newCoordinates: Coordinates = {latitude: 50, longitude: 50};
		let newInvalidCoords = 'lala';

		before(async function() {
			await reset();
			ret = await Promise.all(users.map(createUser));
			[owner, guest, dj, other] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});
			ret = await createEvent(owner.apiKey, {name: eventName, description: eventDescription, visibility: 'PRIVATE', votingSchedule: eventVotingSchedule, eventSchedule: eventEventSchedule});
			eventid = ret.body.id;
		});

		it('Should update event', async function() {
			ret = await updateEvent(owner.apiKey, eventid, {name: newName, coordinates: newCoordinates});
			expect(ret.status).equals(ResponseStatusTypes.OK);
			expect(ret.body.name).equals(newName);
			expect(ret.body.coordinates).deep.equals(newCoordinates);
		});

		it('Should delete schedule', async function() {
			ret = await updateEvent(owner.apiKey, eventid, {votingSchedule: null, eventSchedule: null});
			expect(ret.status).equals(ResponseStatusTypes.OK);
			ret = await getEvent(owner.apiKey, eventid);
			expect(ret.body.eventSchedule).equals(null);
			expect(ret.body.votingSchedule).equals(null);
		});

		describe('Should fail if ...', function() {
			before(async function() {
				await reset();
				ret = await Promise.all(users.map(createUser));
				[owner, guest, dj, other] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});
				ret = await createEvent(owner.apiKey, {name: eventName, description: eventDescription, visibility: 'PRIVATE', votingSchedule: eventVotingSchedule, eventSchedule: eventEventSchedule});
				eventid = ret.body.id;
				await Promise.all([
					addDJToEvent(owner.apiKey, eventid, dj.id),
					addGuestToEvent(owner.apiKey, eventid, guest.id)
				]);
			});
	
			it('User is guest', async function() {
				ret = await updateEvent(guest.apiKey, eventid, {name: newName, coordinates: newCoordinates});
				expect(ret.status).equals(ResponseStatusTypes.UNAUTHORIZED);
			});
			it('User is dj', async function() {
				ret = await updateEvent(dj.apiKey, eventid, {name: newName, coordinates: newCoordinates});
				expect(ret.status).equals(ResponseStatusTypes.UNAUTHORIZED);
			});
			it('User is other', async function() {
				ret = await updateEvent(other.apiKey, eventid, {name: newName, coordinates: newCoordinates});
				expect(ret.status).equals(ResponseStatusTypes.UNAUTHORIZED);
			});
			it('apiKey not provided', async function() {
				ret = await chai.request(app.app)
					.put(`/events/${eventid}`)
					.send({name: newName, coordinates: newCoordinates});
				expect(ret.status).equal(ResponseStatusTypes.BAD_REQUEST);
				});
			it('apiKey invalid', async function() {
				ret = await updateEvent('0123456789', eventid, {name: newName, coordinates: newCoordinates});
				expect(ret.status).equals(ResponseStatusTypes.UNAUTHORIZED);
			});
			it('eventid not found', async function() {
				ret = await updateEvent(owner.apiKey, '0123456789', {name: newName, coordinates: newCoordinates});
				expect(ret.status).equals(ResponseStatusTypes.NOT_FOUND);
			});
			it('value invalid', async function() {
				ret = await chai.request(app.app)
					.put(`/events/${eventid}?apiKey=${owner.apiKey}`)
					.send({name: newName, coordinates: newInvalidCoords});
				expect(ret.status).equals(ResponseStatusTypes.BAD_REQUEST);
				ret = await getEvent(owner.apiKey, eventid);
				expect(ret.body.coordinates).not.deep.equal(newInvalidCoords)
			});
		});
	});

	describe('Add Guest', function() {
		beforeEach(async function() {
			await reset();
			ret = await Promise.all(users.map(createUser));
			[owner, guest, dj, other] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});
			ret = await createEvent(owner.apiKey, {name: eventName, description: eventDescription, visibility: 'PRIVATE', votingSchedule: eventVotingSchedule, eventSchedule: eventEventSchedule});
			eventid = ret.body.id;
		});

		it('Should add a guest', async function() {
			ret = await addGuestToEvent(owner.apiKey, eventid, guest.id);
			expect(ret.status).equals(ResponseStatusTypes.OK);
			ret = await getEvent(owner.apiKey, eventid);
			expect(ret.body.guests.find(x => x.id == guest.id)).exist;
			ret = await getUser(guest.apiKey, guest.id);
			expect(ret.body.guestInEvents).include(eventid);
		});

		describe('Should fail if ... ', function() {
			it('guest is owner of event', async function() {
				let ret = await addGuestToEvent(owner.apiKey, eventid, owner.id)
				expect(ret.status).equal(ResponseStatusTypes.BAD_REQUEST);
				ret = await getEvent(owner.apiKey, eventid);
				expect(ret.body.guests).satisfy(guests => !guests.find(user => user.id == owner.id));
			});
			it('User is already guest', async function() {
				ret = await addGuestToEvent(owner.apiKey, eventid, guest.id);
				expect(ret.status).equals(ResponseStatusTypes.OK);
				ret = await addGuestToEvent(owner.apiKey, eventid, guest.id);
				expect(ret.status).equals(ResponseStatusTypes.OK);

				ret = await getEvent(owner.apiKey, eventid);
				expect(ret.body.guests.find(x => x.id == guest.id)).exist;
				ret = await getUser(guest.apiKey, guest.id);
				expect(ret.body.guestInEvents).include(eventid);
			});
			it('User does not exist', async function() {
				let id: string = '0123456789';
				ret = await addGuestToEvent(owner.apiKey, eventid, id);
				expect(ret.status).equals(ResponseStatusTypes.NOT_FOUND);
				ret = await getEvent(owner.apiKey, eventid);
				expect(ret.body.guests.find(x => x.id == id)).not.exist;
				ret = await getUser(guest.apiKey, id);
				expect(ret.status).equals(ResponseStatusTypes.NOT_FOUND);
			});
			it('Guest is requesting to be added', async function() {
				ret = await addGuestToEvent(guest.apiKey, eventid, guest.id);
				expect(ret.status).equals(ResponseStatusTypes.UNAUTHORIZED);
				ret = await getEvent(owner.apiKey, eventid);
				expect(ret.body.guests.find(x => x.id == guest.id)).not.exist;
				ret = await getUser(guest.apiKey, guest.id);
				expect(ret.body.guestInEvents).not.include(eventid);
			});
		});
	});

	describe('Add Guests', function() {
		before(async function() {
			await reset();
			ret = await Promise.all(users.map(createUser));
			[owner, guest, dj, other] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});
			ret = await createEvent(owner.apiKey, {name: eventName, description: eventDescription, visibility: 'PRIVATE', votingSchedule: eventVotingSchedule, eventSchedule: eventEventSchedule});
			eventid = ret.body.id;
		});

		it('Should add guests', async function() {
			ret = await addGuestsToEvent(owner.apiKey, eventid, [guest.id, dj.id, other.id]);
			expect(ret.status).equals(ResponseStatusTypes.OK);
			ret = await getEvent(owner.apiKey, eventid);
			expect(ret.body.guests.map(x => x.id)).include.members([guest.id, dj.id, other.id]);
			await Promise.all([guest, dj, other].map(async (user: user) => {
				ret = await getUser(user.apiKey, user.id);
				expect(ret.body.guestInEvents).include(eventid);
			}));
		});
		// other tests done in 'Add Guest'
	});

	describe('Remove Guest', function() {
		beforeEach(async function() {
			await reset();
			ret = await Promise.all(users.map(createUser));
			[owner, guest, dj, other] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});
			ret = await createEvent(owner.apiKey, {name: eventName, description: eventDescription, visibility: 'PRIVATE', votingSchedule: eventVotingSchedule, eventSchedule: eventEventSchedule});
			eventid = ret.body.id;
			ret = await addGuestToEvent(owner.apiKey, eventid, guest.id);
		});

		it('Owner should remove a guest', async function() {
			ret = await removeGuestFromEvent(owner.apiKey, eventid, guest.id);
			expect(ret.status).equals(ResponseStatusTypes.OK);
			ret = await getEvent(owner.apiKey, eventid);
			expect(ret.body.guests.find(x => x.id == guest.id)).not.exist;
			ret = await getUser(guest.apiKey, guest.id);
			expect(ret.body.guestInEvents).not.include(eventid);
		});
		it('Guest should remove self', async function() {
			ret = await removeGuestFromEvent(guest.apiKey, eventid, guest.id);
			expect(ret.status).equals(ResponseStatusTypes.OK);
			ret = await getEvent(owner.apiKey, eventid);
			expect(ret.body.guests.find(x => x.id == guest.id)).not.exist;
			ret = await getUser(guest.apiKey, guest.id);
			expect(ret.body.guestInEvents).not.include(eventid);
		});
		describe('Should fail if ... ', function() {
			it('user is not guest in event', async function() {
				let ret = await removeGuestFromEvent(owner.apiKey, eventid, other.id);
				expect(ret.status).equal(ResponseStatusTypes.OK);
				expect(ret.body.message).equals('No change');
			});
			it('User does not exist', async function() {
				let id: string = '0123456789';
				ret = await addGuestToEvent(owner.apiKey, eventid, id);
				expect(ret.status).equals(ResponseStatusTypes.NOT_FOUND);
			});
		});
	});

	describe('Remove Guests', function() {
		before(async function() {
			await reset();
			ret = await Promise.all(users.map(createUser));
			[owner, guest, dj, other] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});
			eventid = (await createEvent(owner.apiKey, {name: eventName, description: eventDescription, visibility: 'PRIVATE', votingSchedule: eventVotingSchedule, eventSchedule: eventEventSchedule})).body.id;
			ret = await addGuestsToEvent(owner.apiKey, eventid, [guest.id, dj.id, other.id]);
		});

		it('Should remove guests', async function() {
			ret = await removeGuestsFromEvent(owner.apiKey, eventid, [guest.id, dj.id, other.id]);			
			expect(ret.status).equals(ResponseStatusTypes.OK);
			ret = await getEvent(owner.apiKey, eventid);
			expect(ret.body.guests.map(x => x.id)).not.include.members([guest.id, dj.id, other.id]);
			await Promise.all([guest, dj, other].map(async (user: user) => {
				ret = await getUser(user.apiKey, user.id);
				expect(ret.body.guestInEvents).not.include(eventid);
			}));
		});
	});

	describe('Add DJ', function() {
		beforeEach(async function() {
			await reset();
			ret = await Promise.all(users.map(createUser));
			[owner, guest, dj, other] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});
			eventid = (await createEvent(owner.apiKey, {name: eventName, description: eventDescription, visibility: 'PRIVATE', votingSchedule: eventVotingSchedule, eventSchedule: eventEventSchedule})).body.id;
		});

		it('Should add a dj', async function() {
			ret = await addDJToEvent(owner.apiKey, eventid, dj.id);
			expect(ret.status).equals(ResponseStatusTypes.OK);
			ret = await getEvent(owner.apiKey, eventid);
			expect(ret.body.djs.find(x => x.id == dj.id)).exist;
			ret = await getUser(dj.apiKey, dj.id);
			expect(ret.body.djInEvents).include(eventid);
		});

		describe('Should fail if ... ', function() {
			it('dj is owner of event', async function() {
				let ret = await addDJToEvent(owner.apiKey, eventid, owner.id)
				expect(ret.status).equal(ResponseStatusTypes.BAD_REQUEST);
				ret = await getEvent(owner.apiKey, eventid);
				expect(ret.body.djs).satisfy(djs => !djs.find(user => user.id == owner.id));
			});
			it('User is already dj', async function() {
				ret = await addDJToEvent(owner.apiKey, eventid, dj.id);
				expect(ret.status).equals(ResponseStatusTypes.OK);
				ret = await addDJToEvent(owner.apiKey, eventid, dj.id);
				expect(ret.status).equals(ResponseStatusTypes.OK);

				ret = await getEvent(owner.apiKey, eventid);
				expect(ret.body.djs.find(x => x.id == dj.id)).exist;
				ret = await getUser(dj.apiKey, dj.id);
				expect(ret.body.djInEvents).include(eventid);
			});
			it('User does not exist', async function() {
				let id: string = '0123456789';
				ret = await addDJToEvent(owner.apiKey, eventid, id);
				expect(ret.status).equals(ResponseStatusTypes.NOT_FOUND);
				ret = await getEvent(owner.apiKey, eventid);
				expect(ret.body.djs.find(x => x.id == id)).not.exist;
				ret = await getUser(dj.apiKey, id);
				expect(ret.status).equals(ResponseStatusTypes.NOT_FOUND);
			});
			it('DJ is requesting to be added', async function() {
				ret = await addDJToEvent(dj.apiKey, eventid, dj.id);
				expect(ret.status).equals(ResponseStatusTypes.UNAUTHORIZED);
				ret = await getEvent(owner.apiKey, eventid);
				expect(ret.body.djs.find(x => x.id == dj.id)).not.exist;
				ret = await getUser(dj.apiKey, dj.id);
				expect(ret.body.djInEvents).not.include(eventid);
			});
		});
	});

	describe('Add DJs', function() {
		before(async function() {
			await reset();
			ret = await Promise.all(users.map(createUser));
			[owner, guest, dj, other] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});
			eventid = (await createEvent(owner.apiKey, {name: eventName, description: eventDescription, visibility: 'PRIVATE', votingSchedule: eventVotingSchedule, eventSchedule: eventEventSchedule})).body.id;
		});

		it('Should add djs', async function() {
			ret = await addDJsToEvent(owner.apiKey, eventid, [guest.id, dj.id, other.id]);
			expect(ret.status).equals(ResponseStatusTypes.OK);
			ret = await getEvent(owner.apiKey, eventid);
			expect(ret.body.djs.map(x => x.id)).include.members([guest.id, dj.id, other.id]);
			await Promise.all([guest, dj, other].map(async (user: user) => {
				ret = await getUser(user.apiKey, user.id);
				expect(ret.body.djInEvents).include(eventid);
			}));
		});
	});

	describe('Remove DJ', function() {
		beforeEach(async function() {
			await reset();
			ret = await Promise.all(users.map(createUser));
			[owner, guest, dj, other] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});
			eventid = (await createEvent(owner.apiKey, {name: eventName, description: eventDescription, visibility: 'PRIVATE', votingSchedule: eventVotingSchedule, eventSchedule: eventEventSchedule})).body.id;
			await addDJToEvent(owner.apiKey, eventid, dj.id);
		});

		it('Should remove dj', async function() {
			ret = await removeDJFromEvent(owner.apiKey, eventid, dj.id);
			expect(ret.status).equals(ResponseStatusTypes.OK);
			ret = await getEvent(owner.apiKey, eventid);
			expect(ret.body.djs.find(x => x.id == dj.id)).not.exist;
			ret = await getUser(dj.apiKey, dj.id);
			expect(ret.body.djInEvents).not.include(eventid);
		});
		it('DJ should remove self', async function() {
			ret = await removeDJFromEvent(dj.apiKey, eventid, dj.id);
			expect(ret.status).equals(ResponseStatusTypes.OK);
			ret = await getEvent(owner.apiKey, eventid);
			expect(ret.body.djs.find(x => x.id == dj.id)).not.exist;
			ret = await getUser(dj.apiKey, dj.id);
			expect(ret.body.djInEvents).not.include(eventid);
		});
		describe('Should fail if ... ', function() {
			it('user is not dj in event', async function() {
				let ret = await removeDJFromEvent(owner.apiKey, eventid, other.id);
				expect(ret.status).equal(ResponseStatusTypes.OK);
				expect(ret.body.message).equals('No change');
			});
			it('User does not exist', async function() {
				let id: string = '0123456789';
				ret = await addDJToEvent(owner.apiKey, eventid, id);
				expect(ret.status).equals(ResponseStatusTypes.NOT_FOUND);
			});
		});
	});

	describe('Remove DJs', function() {
		before(async function() {
			await reset();
			ret = await Promise.all(users.map(createUser));
			[owner, guest, dj, other] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});
			eventid = (await createEvent(owner.apiKey, {name: eventName, description: eventDescription, visibility: 'PRIVATE', votingSchedule: eventVotingSchedule, eventSchedule: eventEventSchedule})).body.id;
			await addDJsToEvent(owner.apiKey, eventid, [guest.id, dj.id, other.id]);
		});

		it('Should remove djs', async function() {
			ret = await removeDJsFromEvent(owner.apiKey, eventid, [other.id, dj.id, guest.id]);
			expect(ret.status).equals(ResponseStatusTypes.OK);
			ret = await getEvent(owner.apiKey, eventid);
			expect(ret.body.djs.map(x => x.id)).not.include.members([guest.id, dj.id, other.id]);
			await Promise.all([guest, dj, other].map(async (user: user) => {
				ret = await getUser(user.apiKey, user.id);
				expect(ret.body.djInEvents).not.include(eventid);
			}));
		});
	});

	describe('Delete Event', function() {
		let eventid: string;
		before(async function() {
			let start = new Date();
			let end = new Date();
			end.setSeconds(end.getSeconds() + 2);
			// create users
			await reset();
			ret = await Promise.all(users.map(createUser));
			[owner, guest, dj, other] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});
			// create event
			ret = await createEvent(owner.apiKey, {name: 'my event', visibility: 'PUBLIC', votingSchedule: {start: start.toISOString(), end: end.toISOString()}});
			eventid = ret.body.id;
			await Promise.all([
				
				// == Add Playlists ==
				...['1', '2'].map(async (name): Promise<any> =>  {
					ret = await createPlaylist(owner.apiKey, {name});
					await addPlaylistToEvent(owner.apiKey, eventid, ret.body.id);
				}),
				// add guest to event
				addGuestToEvent(owner.apiKey, eventid, guest.id),
				// add dj to event
				addDJToEvent(owner.apiKey, eventid, dj.id),

			]);
			ret = await deleteEvent(owner.apiKey, eventid);
			expect(ret.status).equals(ResponseStatusTypes.OK);
		});
		it('Should delete event', async function() {
			// event does not exist
			ret = await getEvent(owner.apiKey, eventid);
			expect(ret.status).equals(ResponseStatusTypes.NOT_FOUND);
			let event: Event = await Event.get({id: eventid});
			expect(!!!event);
			// owner does not have event
			ret = await getUser(owner.apiKey, owner.id);
			expect(ret.body.events).not.include(eventid);
			expect(ret.body.events).length(0);
			// guest is not guest of event
			ret = await getUser(guest.apiKey, guest.id);
			expect(ret.body.guestInEvents).length(0);
			// dj is not dj of event
			ret = await getUser(dj.apiKey, dj.id);
			expect(ret.body.djInEvents).length(0);
		});
	});

	describe('Delete Events', function() {
		let eventids: string[];
		before(async function() {
			// create users
			await reset();
			ret = await Promise.all(users.map(createUser));
			[owner, guest, dj, other] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});
			// create events
			eventids = await Promise.all(['1', '2', '3', '4'].map(async name => {
				ret = await createEvent(owner.apiKey, {name, visibility: 'PUBLIC'});
				let eventid = ret.body.id;
				await Promise.all([
					
					// == Add Playlists ==
					...['1', '2'].map(async (name): Promise<any> =>  {
						ret = await createPlaylist(owner.apiKey, {name})
						await addPlaylistToEvent(owner.apiKey, eventid, ret.body.id);
					}),
					// add guest to event
					addGuestToEvent(owner.apiKey, eventid, guest.id),
					// add dj to event
					addDJToEvent(owner.apiKey, eventid, dj.id),
	
				]);
				return eventid;
			}));
			ret = await deleteEvents(owner.apiKey, eventids);
			expect(ret.status).equals(ResponseStatusTypes.OK);
		});
		it('Should delete all the events', async function() {
			await Promise.all(eventids.map(async eventid => {
				// event does not exist
				ret = await getEvent(owner.apiKey, eventid);
				expect(ret.status).equals(ResponseStatusTypes.NOT_FOUND);
				let event: Event = await Event.get({id: eventid});
				expect(!!!event);
				// owner does not have event
				ret = await getUser(owner.apiKey, owner.id);
				expect(ret.body.events).not.include(eventid);
				expect(ret.body.events).length(0);
				// guest is not guest of event
				ret = await getUser(guest.apiKey, guest.id);
				expect(ret.body.guestInEvents).length(0);
				// dj is not dj of event
				ret = await getUser(dj.apiKey, dj.id);
				expect(ret.body.djInEvents).length(0);
			}));
		});
	});

	describe('Add Playlist', function() {
		let editor: user;
		describe('Should add a playlist to the event if ...', function() {
			beforeEach(async function() {
				await reset();
				ret = await Promise.all(users.map(createUser));
				[owner, guest, editor, other] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});
				ret = await createEvent(owner.apiKey, {name: eventName});
				eventid = ret.body.id;
				// owner creates playlist1
				ret = await createPlaylist(owner.apiKey, {name: 'my playlist'});
				playlistid1 = ret.body.id;
				await addGuestToPlaylist(owner.apiKey, playlistid1, guest.id);
				await addEditorToPlaylist(owner.apiKey, playlistid1, editor.id);
				// other creates playlist2
				ret = await createPlaylist(other.apiKey, {name: 'my playlist'});
				playlistid2 = ret.body.id;
			});
			it('owner of playlist performs action', async function() {
				ret = await addPlaylistToEvent(owner.apiKey, eventid, playlistid1);
				expect(ret.status).equals(ResponseStatusTypes.OK);
				ret = await getEvent(owner.apiKey, eventid);
				expect(ret.body.playlists[0].events).include(eventid);
				ret = await getPlaylist(owner.apiKey, playlistid1);
				expect(ret.body.events).include(eventid);	
			});
			it('non owner of playlist performs action', async function() {
				ret = await addPlaylistToEvent(owner.apiKey, eventid, playlistid2);
				expect(ret.status).equals(ResponseStatusTypes.OK);
				ret = await getEvent(owner.apiKey, eventid);
				expect(ret.body.playlists[0].events).include(eventid);
				ret = await getPlaylist(owner.apiKey, playlistid2);
				expect(ret.body.events).include(eventid);
			});
			it('option: invite guests', async function() {
				ret = await addPlaylistToEvent(owner.apiKey, eventid, playlistid1, {inviteGuests: true});
				expect(ret.status).equals(ResponseStatusTypes.OK);
				// event should has playlist, which should have playlist id
				ret = await getEvent(owner.apiKey, eventid);
				expect(ret.body.playlists[0].events).include(eventid);
				// playlist should have event
				ret = await getPlaylist(owner.apiKey, playlistid1);
				expect(ret.body.events).include(eventid);
				// guest of playlist should be guest of event
				ret = await getUser(guest.apiKey, guest.id);
				expect(ret.body.guestInEvents).include(eventid);
			});
			it('option: invite editors', async function() {
				ret = await addPlaylistToEvent(owner.apiKey, eventid, playlistid1, {inviteEditors: true});
				expect(ret.status).equals(ResponseStatusTypes.OK);
				// event should has playlist, which should have playlist id
				ret = await getEvent(owner.apiKey, eventid);
				expect(ret.body.playlists[0].events).include(eventid);
				// playlist should have event
				ret = await getPlaylist(owner.apiKey, playlistid1);
				expect(ret.body.events).include(eventid);
				// guest of playlist should be guest of event
				ret = await getUser(editor.apiKey, editor.id);
				expect(ret.body.guestInEvents).include(eventid);
			});
			it('option: duplicatePlaylists', async function() {
				ret = await addPlaylistToEvent(owner.apiKey, eventid, playlistid1, {duplicatePlaylist: true});
				expect(ret.status).equals(ResponseStatusTypes.OK);
				// event should has playlist, which should have playlist id
				ret = await getEvent(owner.apiKey, eventid);
				expect(ret.body.playlists[0].events).include(eventid);
				let newPlaylistid: string = ret.body.playlists[0].id;
				// new playlist should exist and have event and should have no editors or guests
				ret = await getPlaylist(owner.apiKey, newPlaylistid);
				expect(ret.body.events).include(eventid);
				expect(ret.body.editors).length(0);
				expect(ret.body.guests).length(0);
				// old playlist should not have event
				ret = await getPlaylist(owner.apiKey, playlistid1);
				expect(ret.body.events).not.include(eventid);
			});
			it('option: copyEditors', async function() {
				ret = await addPlaylistToEvent(owner.apiKey, eventid, playlistid1, {duplicatePlaylist: true, copyEditors: true});
				expect(ret.status).equals(ResponseStatusTypes.OK);
				// event should has playlist, which should have playlist id
				ret = await getEvent(owner.apiKey, eventid);
				expect(ret.body.playlists[0].events).include(eventid);
				let newPlaylistid: string = ret.body.playlists[0].id;
				// new playlist should exist and have old playlist editors
				expect( (await getPlaylist(owner.apiKey, newPlaylistid)).body.editors ).deep.equals( (await getPlaylist(owner.apiKey, playlistid1)).body.editors );
			});
			it('option: copyGuests', async function() {
				ret = await addPlaylistToEvent(owner.apiKey, eventid, playlistid1, {duplicatePlaylist: true, copyGuests: true});
				expect(ret.status).equals(ResponseStatusTypes.OK);
				// event should has playlist, which should have playlist id
				ret = await getEvent(owner.apiKey, eventid);
				expect(ret.body.playlists[0].events).include(eventid);
				let newPlaylistid: string = ret.body.playlists[0].id;
				// new playlist should exist and have old playlist editors
				expect( (await getPlaylist(owner.apiKey, newPlaylistid)).body.guests.map(x => x.id) ).deep.equals( (await getPlaylist(owner.apiKey, playlistid1)).body.guests.map(x => x.id) );
			});
		});

		describe('Should fail to add playlist to event if ...', function() {
			before(async function() {
				await reset();
				ret = await Promise.all(users.map(createUser));
				[owner, guest, dj, other] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});
				ret = await createEvent(owner.apiKey, {name: eventName});
				eventid = ret.body.id;
				ret = await createPlaylist(owner.apiKey, {name: 'my playlist'});
				playlistid1 = ret.body.id;
				ret = await createPlaylist(guest.apiKey, {name: 'my playlist'});
				playlistid2 = ret.body.id;
				await updatePlaylist(guest.apiKey, playlistid2, {visibility: 'PRIVATE'});
			});
			it('apiKey invalid', async function() {
				ret = await addPlaylistToEvent('023456789', eventid, playlistid1);
				expect(ret.status).equals(ResponseStatusTypes.UNAUTHORIZED);
			});
			it('apiKey not provided', async function() {
				ret = await chai.request(app.app)
					.put(`/events/${eventid}/playlists/${playlistid1}`)
					.send();
				expect(ret.status.BAD_REQUEST);
			});
			it('event does not exist', async function() {
				ret = await addPlaylistToEvent(owner.apiKey, '023456789', playlistid1);
				expect(ret.status).equals(ResponseStatusTypes.NOT_FOUND);
			});
			it('playlist does not exist', async function() {
				ret = await addPlaylistToEvent(owner.apiKey, eventid, '0123456789');
				expect(ret.status).equals(ResponseStatusTypes.NOT_FOUND);
			});
			it('user does not have rights to access playlist', async function() {
				ret = await addPlaylistToEvent(other.apiKey, eventid, playlistid2);
				expect(ret.status).equals(ResponseStatusTypes.UNAUTHORIZED);
			});
		});
	});

	describe('Add Playlists', function() {
		let editor: user;

		describe('Should add playlists to the event', function() {
			before(async function() {
				await reset();
				ret = await Promise.all(users.map(createUser));
				[owner, guest, editor, other] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});
				ret = await createEvent(owner.apiKey, {name: eventName});
				eventid = ret.body.id;
				// owner creates playlist1 and playlist2
				ret = await Promise.all([createPlaylist(owner.apiKey, {name: 'my playlist'}), createPlaylist(owner.apiKey, {name: 'my playlist'})]);
				playlistid1 = ret[0].body.id;
				playlistid2 = ret[1].body.id;
			});
			it('owner of playlist performs action', async function() {
				ret = await addPlaylistsToEvent(owner.apiKey, eventid, [playlistid1, playlistid2]);
				expect(ret.status).equals(ResponseStatusTypes.OK);
				ret = await getEvent(owner.apiKey, eventid);
				expect(ret.body.playlists).length(2);
				ret = await Promise.all([getPlaylist(owner.apiKey, playlistid1), getPlaylist(owner.apiKey, playlistid2)]);
				expect(ret[0].body.events).include(eventid);
				expect(ret[1].body.events).include(eventid);
			});
		});
	});

	describe('Remove Playlist', function() {
		beforeEach(async function() {
			await reset();
			ret = await Promise.all(users.map(createUser));
			[owner] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});
			ret = await createEvent(owner.apiKey, {name: eventName});
			eventid = ret.body.id;
			// owner creates playlist1
			ret = await createPlaylist(owner.apiKey, {name: 'my playlist'});
			playlistid1 = ret.body.id;
			ret = await addPlaylistToEvent(owner.apiKey, eventid, playlistid1);
		});
		describe('Should remove playlist from the event', function() {
			it('Should remove playlist from the event', async function() {
				ret = await removePlaylistFromEvent(owner.apiKey, eventid, playlistid1);
				expect(ret.status).equals(ResponseStatusTypes.OK);
				ret = await Promise.all([getEvent(owner.apiKey, eventid), getPlaylist(owner.apiKey, playlistid1)]);
				expect(ret[0].body.playlists).length(0);
				expect(ret[1].body.events).length(0);
			});
		});
		describe('Should fail if ...', function() {
			it('Playlist does not exist', async function() {
				ret = await removePlaylistFromEvent(owner.apiKey, eventid, '0123456789');
				expect(ret.status).equals(ResponseStatusTypes.NOT_FOUND);
			});
		});
	});

	describe('Remove Playlists', function() {
		describe('Should remove playlists from the event', function() {
			before(async function() {
				await reset();
				ret = await Promise.all(users.map(createUser));
				[owner] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});
				ret = await createEvent(owner.apiKey, {name: eventName});
				eventid = ret.body.id;
				// owner creates playlist1 and playlist2
				ret = await Promise.all([createPlaylist(owner.apiKey, {name: 'my playlist'}), createPlaylist(owner.apiKey, {name: 'my playlist'})]);
				playlistid1 = ret[0].body.id;
				playlistid2 = ret[1].body.id;
			});
			it('Should remove playlists from the event', async function() {
				ret = await removePlaylistsFromEvent(owner.apiKey, eventid, [playlistid1, playlistid2]);
				expect(ret.status).equals(ResponseStatusTypes.OK);
				ret = await Promise.all([getEvent(owner.apiKey, eventid), getPlaylist(owner.apiKey, playlistid1), getPlaylist(owner.apiKey, playlistid2)]);
				expect(ret[0].body.playlists).length(0);
				expect(ret[1].body.events).length(0);
				expect(ret[2].body.events).length(0);
			});
		});
	});

	describe('Event Schedules', function() {
		let now: Date;
		let start: Date;
		let end: Date;

		beforeEach(async function() {
			await reset();
			ret = await Promise.all(users.map(createUser));
			[owner] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});
			ret = await createEvent(owner.apiKey, {name: eventName});
			eventid = ret.body.id;
			ret = await Promise.all([...Array(2).keys()].map(_ => createPlaylist(owner.apiKey, {name: 'my playlist'})));
			playlistid1 = ret[0].body.id;
			playlistid2 = ret[1].body.id;
			now = new Date();
			start = new Date(now);
			end = new Date(now);
		});

		describe('voting', function() {
			it('start in 1.5 seconds and end in 2.5 seconds', async function() {
				start.setSeconds(start.getSeconds() + 1);
				end.setSeconds(end.getSeconds() + 2);
				await updateEvent(owner.apiKey, eventid, {votingSchedule: {start: start.toISOString(), end: end.toISOString()}});
				await Promise.all([
					votingActiveShouldBe(false, 0),
					votingActiveShouldBe(true, 1500),
					votingActiveShouldBe(false, 2500),
				]);
			});

			it('start vote without schedule, then set schedule so that voting should not be active', async function() {
				await votingActiveShouldBe(false, 0);
				await eventStartVoting(owner.apiKey, eventid);
				await votingActiveShouldBe(true, 0);
				start.setSeconds(start.getSeconds() + 1);
				end.setSeconds(end.getSeconds() + 2);
				await updateEvent(owner.apiKey, eventid, {votingSchedule: {start: start.toISOString(), end: end.toISOString()}});
				await Promise.all([
					votingActiveShouldBe(false, 0),
					votingActiveShouldBe(true, 1500),
					votingActiveShouldBe(false, 2500),
				]);
			});
		});

		describe('event', function() {
			it('start in 1.5 seconds and end in 2.5 seconds', async function() {
				start.setSeconds(start.getSeconds() + 1);
				end.setSeconds(end.getSeconds() + 2);
				await updateEvent(owner.apiKey, eventid, {eventSchedule: {start: start.toISOString(), end: end.toISOString()}});
				await Promise.all([
					eventActiveShouldBe(false, 0),
					eventActiveShouldBe(true, 1500),
					eventActiveShouldBe(false, 2500),
				]);
			});
		});

		function votingActiveShouldBe(status: boolean, time: number): Promise<any> {
			return new Promise(resolve => {
				setTimeout(async () => {
					let ret = await getEvent(owner.apiKey, eventid);
					// console.log(`expect votingActive to be ${status} after ${time} ms. status: ${ret.body.votingActive}`);
					expect(ret.body.votingActive).equals(status)
					resolve();
				}, time);
			});	
		}

		function eventActiveShouldBe(status: boolean, time: number): Promise<any> {
			return new Promise(resolve => {
				setTimeout(async () => {
					let ret = await getEvent(owner.apiKey, eventid);
					// console.log(`expect eventActive to be ${status} after ${time} ms. status: ${ret.body.eventActive}`);
					expect(ret.body.eventActive).equals(status)
					resolve();
				}, time);
			});	
		}
	});

	describe('Event Actions', function() {
		describe('startvote', function() {
			beforeEach(async function() {
				await reset();
				ret = await Promise.all(users.map(createUser));
				[owner, dj, guest, other] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});
				ret = await createEvent(owner.apiKey, {name: eventName});
				eventid = ret.body.id;
				ret = await Promise.all([...Array(2).keys()].map(_ => createPlaylist(owner.apiKey, {name: 'my playlist'})));
				playlistid1 = ret[0].body.id;
				playlistid2 = ret[1].body.id;
				await Promise.all([
					...[0, 1, 2].map(x => addTrack(owner.apiKey, playlistid1, x.toString())),
					...[3, 4, 5].map(x => addTrack(owner.apiKey, playlistid2, x.toString())),
					addPlaylistsToEvent(owner.apiKey, eventid, [playlistid1, playlistid2])
				]);
			});
			it('Should activate voting, remove playlists, create event playlists', async function() {
				ret = await eventStartVoting(owner.apiKey, eventid);
				expect(ret.status).equals(ResponseStatusTypes.OK);
				ret = await getEvent(owner.apiKey, eventid);
				expect(ret.body.votingActive).equals(true);
				expect(ret.body.playlists).length(0);
				expect(ret.body.eventPlaylists).length(2);
			});
			describe('Should fail if ....', function() {
				it('User is dj, guest, or other', async function() {
					await Promise.all([dj, guest, other].map(async user => {
						ret = await eventStartVoting(user.apiKey, eventid);
						expect(ret.status).equals(ResponseStatusTypes.UNAUTHORIZED);
						ret = await getEvent(owner.apiKey, eventid);
						expect(ret.body.votingActive).equals(false);
						expect(ret.body.playlists).length(2);
						expect(ret.body.eventPlaylists).length(0);
					}));
				});
			});
		});
		describe('endvote', function() {
			beforeEach(async function() {
				await reset();
				ret = await Promise.all(users.map(createUser));
				[owner] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});
				ret = await createEvent(owner.apiKey, {name: eventName});
				eventid = ret.body.id;
				ret = await Promise.all([...Array(2).keys()].map(_ => createPlaylist(owner.apiKey, {name: 'my playlist'})));
				playlistid1 = ret[0].body.id;
				playlistid2 = ret[1].body.id;
				await Promise.all([
					...[0, 1, 2].map(x => addTrack(owner.apiKey, playlistid1, x.toString())),
					...[3, 4, 5].map(x => addTrack(owner.apiKey, playlistid2, x.toString())),
					addPlaylistsToEvent(owner.apiKey, eventid, [playlistid1, playlistid2])
				]);
				ret = await eventStartVoting(owner.apiKey, eventid);
			});
				
			it('Should disactivate voting', async function() {
				ret = await eventEndVoting(owner.apiKey, eventid);
				expect(ret.status).equals(ResponseStatusTypes.OK);
				ret = await getEvent(owner.apiKey, eventid);
				expect(ret.body.votingActive).equals(false);
				expect(ret.body.playlists).length(2);
				expect(ret.body.eventPlaylists).length(0);
			});
			describe('Should fail if ....', function() {
				it('User is dj, guest, or other', async function() {
					await Promise.all([dj, guest, other].map(async user => {
						ret = await eventEndVoting(user.apiKey, eventid);
						expect(ret.status).equals(ResponseStatusTypes.UNAUTHORIZED);
						ret = await getEvent(owner.apiKey, eventid);
						expect(ret.body.votingActive).equals(true);
						expect(ret.body.playlists).length(0);
						expect(ret.body.eventPlaylists).length(2);
					}));
				});
			});
		});
		describe('startevent', function() {
			beforeEach(async function() {
				await reset();
				ret = await Promise.all(users.map(createUser));
				[owner] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});
				ret = await createEvent(owner.apiKey, {name: eventName});
				eventid = ret.body.id;
				ret = await Promise.all([...Array(2).keys()].map(_ => createPlaylist(owner.apiKey, {name: 'my playlist'})));
				playlistid1 = ret[0].body.id;
				playlistid2 = ret[1].body.id;
				await Promise.all([
					...[0, 1, 2].map(x => addTrack(owner.apiKey, playlistid1, x.toString())),
					...[3, 4, 5].map(x => addTrack(owner.apiKey, playlistid2, x.toString())),
					addPlaylistsToEvent(owner.apiKey, eventid, [playlistid1, playlistid2])
				]);
			});
			it('Should start event, remove playlists, create event playlists', async function() {
				ret = await eventStartEvent(owner.apiKey, eventid);
				expect(ret.status).equals(ResponseStatusTypes.OK);
				ret = await getEvent(owner.apiKey, eventid);
				expect(ret.body.eventActive).equals(true);
				expect(ret.body.playlists).length(0);
				expect(ret.body.eventPlaylists).length(2);
				expect(ret.body.eventPlaylists[0]).have.property('id');
				expect(ret.body.eventPlaylists[1]).have.property('id');
			});
			describe('Should fail if ....', function() {
				it('User is dj, guest, or other', async function() {
					await Promise.all([dj, guest, other].map(async user => {
						ret = await eventStartEvent(user.apiKey, eventid);
						expect(ret.status).equals(ResponseStatusTypes.UNAUTHORIZED);
						ret = await getEvent(owner.apiKey, eventid);
						expect(ret.body.eventActive).equals(false);
						expect(ret.body.playlists).length(2);
						expect(ret.body.eventPlaylists).length(0);
					}));
				});
			});
		});
		describe('stopevent', function() {
			beforeEach(async function() {
				await reset();
				ret = await Promise.all(users.map(createUser));
				[owner] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});
				ret = await createEvent(owner.apiKey, {name: eventName});
				eventid = ret.body.id;
				ret = await Promise.all([...Array(2).keys()].map(_ => createPlaylist(owner.apiKey, {name: 'my playlist'})));
				playlistid1 = ret[0].body.id;
				playlistid2 = ret[1].body.id;
				await Promise.all([
					...[0, 1, 2].map(x => addTrack(owner.apiKey, playlistid1, x.toString())),
					...[3, 4, 5].map(x => addTrack(owner.apiKey, playlistid2, x.toString())),
					addPlaylistsToEvent(owner.apiKey, eventid, [playlistid1, playlistid2])
				]);
				ret = await eventStartEvent(owner.apiKey, eventid);
			});
			it('Should stop event', async function() {
				ret = await eventEndEvent(owner.apiKey, eventid);
				expect(ret.status).equals(ResponseStatusTypes.OK);
				ret = await getEvent(owner.apiKey, eventid);
				expect(ret.body.eventActive).equals(false);
				expect(ret.body.playlists).length(2);
				expect(ret.body.eventPlaylists).length(0);

				expect(ret.body.votingActive).equals(false);
				expect(ret.body.eventActive).equals(false);
				expect(ret.body.activePlaylist).equals(null);
				expect(ret.body.trackPosition).equals(0);
				expect(ret.body.lastTrackActionTimestamp).equals(null);
			});
			describe('Should fail if ....', function() {
				it('User is dj, guest, or other', async function() {
					await Promise.all([dj, guest, other].map(async user => {
						ret = await eventEndEvent(user.apiKey, eventid);
						expect(ret.status).equals(ResponseStatusTypes.UNAUTHORIZED);
						ret = await getEvent(owner.apiKey, eventid);
						expect(ret.body.eventActive).equals(true);
						expect(ret.body.playlists).length(0);
						expect(ret.body.eventPlaylists).length(2);
					}));
				});
			});
		});
	});

	describe('Event Player Actions (owners)', function() {
		let now: Date;	
		beforeEach(async function() {
			await reset();
			ret = await Promise.all(users.map(createUser));
			[owner] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});
			ret = await createEvent(owner.apiKey, {name: eventName});
			eventid = ret.body.id;
			ret = await createPlaylist(owner.apiKey, {name: 'my playlist'});
			playlistid1 = ret.body.id;
			await Promise.all([...Array(5).keys()].map(x => addTrack(owner.apiKey, playlistid1, x.toString())));
			await addPlaylistToEvent(owner.apiKey, eventid, playlistid1)
			await eventStartEvent(owner.apiKey, eventid);
		});
		
		describe('Play', function() {
			it('Should set an track as active, remove it from unplayed songs, set playlist as active, set timestamp', async function() {
				now = new Date();
				let pos = 5;
				ret = await getEvent(owner.apiKey, eventid);
				playlistid = ret.body.eventPlaylists[0].id;
				let trackid = ret.body.eventPlaylists[0].unplayedTracks[0].id;
				ret = await playerAction(owner.apiKey, eventid, 'play', {trackid, timestamp: now.toISOString(), trackPosition: pos});
				expect(ret.status).equals(ResponseStatusTypes.OK);
				expect(ret.body.activePlaylist).equals(playlistid);
				let activePlaylist = ret.body.eventPlaylists.find(x => x.id == playlistid);
				expect(activePlaylist.activeTrack).equals(trackid);
				let activeTrack = [...activePlaylist.unplayedTracks, ...activePlaylist.playedTracks].find(x => x.id == activePlaylist.activeTrack);
				expect(activePlaylist.unplayedTracks.every(x => x.id != trackid)).equal(true);
				expect(activePlaylist.playedTracks.filter(x => x.isPlaying)).length(1);
				expect(activeTrack.isPlaying).equals(true);
				expect(ret.body.lastTrackActionTimestamp).equals(now.toISOString());
				expect(ret.body.trackPosition).equals(pos);
			});
			it('Should unset previous active track, set new track as active, remove it from unplayed songs, set playlist as active', async function() {
				now = new Date();
				ret = await getEvent(owner.apiKey, eventid);
				let pos = 5;
				playlistid = ret.body.eventPlaylists[0].id;
				let trackid1 = ret.body.eventPlaylists[0].unplayedTracks[0].id;
				let trackid2 = ret.body.eventPlaylists[0].unplayedTracks[2].id;
				ret = await playerAction(owner.apiKey, eventid, 'play', {trackid: trackid1, timestamp: now.toISOString(), trackPosition: 0});
				ret = await playerAction(owner.apiKey, eventid, 'play', {trackid: trackid2, timestamp: now.toISOString(), trackPosition: pos});
				expect(ret.status).equals(ResponseStatusTypes.OK);
				expect(ret.body.activePlaylist).equals(playlistid);
				let activePlaylist = ret.body.eventPlaylists.find(x => x.id == playlistid);
				expect(activePlaylist.activeTrack).equals(trackid2);
				expect(activePlaylist.unplayedTracks.every(x => x.id != trackid1 && x.id != trackid2)).equal(true);
				expect(activePlaylist.playedTracks.find(x => x.id == trackid1)).exist;
				expect(activePlaylist.playedTracks.filter(x => x.isPlaying == true)).length(1);
				let activeTrack = [...activePlaylist.unplayedTracks, ...activePlaylist.playedTracks].find(x => x.id == activePlaylist.activeTrack);
				expect(activeTrack.isPlaying).equals(true);
				expect(ret.body.lastTrackActionTimestamp).equals(now.toISOString());
				expect(ret.body.trackPosition).equals(pos);
			});
			describe('Should fail if ...', function() {
				it('trackid invalid', async function() {
					now = new Date();
					ret = await getEvent(owner.apiKey, eventid);
					playlistid = ret.body.eventPlaylists[0].id;
					ret = await playerAction(owner.apiKey, eventid, 'play', {trackid: '0123456789', timestamp: now.toISOString(), trackPosition: 0});
					expect(ret.status).equals(ResponseStatusTypes.NOT_FOUND);
				});
				it('timestamp not provided', async function() {
					now = new Date();
					ret = await getEvent(owner.apiKey, eventid);
					playlistid = ret.body.eventPlaylists[0].id;
					let trackid = ret.body.eventPlaylists[0].unplayedTracks[0].id;
					ret = await playerAction(owner.apiKey, eventid, 'play', {trackid, trackPosition: 0});
					expect(ret.status).equals(ResponseStatusTypes.BAD_REQUEST);
				});
				it('trackPosition not provided', async function() {
					now = new Date();
					ret = await getEvent(owner.apiKey, eventid);
					playlistid = ret.body.eventPlaylists[0].id;
					let trackid = ret.body.eventPlaylists[0].unplayedTracks[0].id;
					ret = await playerAction(owner.apiKey, eventid, 'play', {trackid, timestamp: now.toISOString()});
					expect(ret.status).equals(ResponseStatusTypes.BAD_REQUEST);
				});
			});
		});
		describe('Pause', function() {
			it('Should set active track to inactive', async function() {
				now = new Date();
				ret = await getEvent(owner.apiKey, eventid);
				playlistid = ret.body.eventPlaylists[0].id;
				let trackid = ret.body.eventPlaylists[0].unplayedTracks[0].id;
				ret = await playerAction(owner.apiKey, eventid, 'play', {trackid, timestamp: now.toISOString(), trackPosition: 0});
				ret = await playerAction(owner.apiKey, eventid, 'pause', {timestamp: now.toISOString(), trackPosition: 0});
				expect(ret.status).equals(ResponseStatusTypes.OK);
				let activePlaylist = ret.body.eventPlaylists.find(x => x.id == playlistid);
				expect(activePlaylist.activeTrack).equals(trackid);
				let activeTrack = [...activePlaylist.unplayedTracks, ...activePlaylist.playedTracks].find(x => x.id == activePlaylist.activeTrack);
				expect(activeTrack.isPlaying).equals(false);
				expect(ret.body.lastTrackActionTimestamp).equals(now.toISOString());
			});
			describe('Should fail if ...', function() {
				it('timestamp invalid', async function() {
					now = new Date();
					ret = await getEvent(owner.apiKey, eventid);
					playlistid = ret.body.eventPlaylists[0].id;
					let trackid = ret.body.eventPlaylists[0].unplayedTracks[0].id;
					ret = await playerAction(owner.apiKey, eventid, 'play', {trackid, timestamp: now.toISOString(), trackPosition: 0});
					ret = await playerAction(owner.apiKey, eventid, 'pause', {timestamp: 'this is my timestamp', trackPosition: 5});
					expect(ret.status).equals(ResponseStatusTypes.BAD_REQUEST);
				});
				it('timestamp not provided', async function() {
					now = new Date();
					ret = await getEvent(owner.apiKey, eventid);
					playlistid = ret.body.eventPlaylists[0].id;
					let trackid = ret.body.eventPlaylists[0].unplayedTracks[0].id;
					ret = await playerAction(owner.apiKey, eventid, 'play', {trackid, timestamp: now.toISOString(), trackPosition: 0});
					ret = await playerAction(owner.apiKey, eventid, 'pause', {trackPosition: 0});
					expect(ret.status).equals(ResponseStatusTypes.BAD_REQUEST);
				});
				it('trackPosition not provided', async function() {
					now = new Date();
					ret = await getEvent(owner.apiKey, eventid);
					playlistid = ret.body.eventPlaylists[0].id;
					let trackid = ret.body.eventPlaylists[0].unplayedTracks[0].id;
					ret = await playerAction(owner.apiKey, eventid, 'play', {trackid, timestamp: now.toISOString(), trackPosition: 0});
					ret = await playerAction(owner.apiKey, eventid, 'pause', {timestamp: now.toISOString()});
					expect(ret.status).equals(ResponseStatusTypes.BAD_REQUEST);
				});
			});
		});
		describe('Seek', function() {
			it('Should update trackPosition', async function() {
				now = new Date();
				let trackPosition: number = 20;
				ret = await getEvent(owner.apiKey, eventid);
				playlistid = ret.body.eventPlaylists[0].id;
				let trackid = ret.body.eventPlaylists[0].unplayedTracks[0].id;
				ret = await playerAction(owner.apiKey, eventid, 'play', {trackid, timestamp: now.toISOString(), trackPosition: 0});
				ret = await playerAction(owner.apiKey, eventid, 'pause', {timestamp: now.toISOString(), trackPosition: 0});
				ret = await playerAction(owner.apiKey, eventid, 'seek', {timestamp: now.toISOString(), trackPosition});
				expect(ret.status).equals(ResponseStatusTypes.OK);
				expect(ret.body.lastTrackActionTimestamp).equals(now.toISOString());
				expect(ret.body.trackPosition).equals(trackPosition);				 
			});
			describe('Should fail if ...', function() {
				it('timestamp invalid', async function() {
					now = new Date();
					ret = await getEvent(owner.apiKey, eventid);
					playlistid = ret.body.eventPlaylists[0].id;
					let trackid = ret.body.eventPlaylists[0].unplayedTracks[0].id;
					ret = await playerAction(owner.apiKey, eventid, 'play', {trackid, timestamp: now.toISOString(), trackPosition: 0});
					ret = await playerAction(owner.apiKey, eventid, 'seek', {timestamp: 'this is my timestamp', trackPosition: 5});
					expect(ret.status).equals(ResponseStatusTypes.BAD_REQUEST);
				});
				it('timestamp not provided', async function() {
					now = new Date();
					ret = await getEvent(owner.apiKey, eventid);
					playlistid = ret.body.eventPlaylists[0].id;
					let trackid = ret.body.eventPlaylists[0].unplayedTracks[0].id;
					ret = await playerAction(owner.apiKey, eventid, 'play', {trackid, timestamp: now.toISOString(), trackPosition: 0});
					ret = await playerAction(owner.apiKey, eventid, 'seek', {trackPosition: 0});
					expect(ret.status).equals(ResponseStatusTypes.BAD_REQUEST);
				});
				it('trackPosition not provided', async function() {
					now = new Date();
					ret = await getEvent(owner.apiKey, eventid);
					playlistid = ret.body.eventPlaylists[0].id;
					let trackid = ret.body.eventPlaylists[0].unplayedTracks[0].id;
					ret = await playerAction(owner.apiKey, eventid, 'play', {trackid, timestamp: now.toISOString(), trackPosition: 0});
					ret = await playerAction(owner.apiKey, eventid, 'seek', {timestamp: now.toISOString()});
					expect(ret.status).equals(ResponseStatusTypes.BAD_REQUEST);
				});
				it('No active track', async function() {
					now = new Date();
					ret = await getEvent(owner.apiKey, eventid);
					playlistid = ret.body.eventPlaylists[0].id;
					ret = await playerAction(owner.apiKey, eventid, 'seek', {trackPosition: 0, timestamp: now.toISOString()});
					expect(ret.status).equals(ResponseStatusTypes.BAD_REQUEST);
				});
			});
		});	
	});

	describe('Track Voting', function() {
		let trackids: string[];

		before(async function() {
			await reset();
			ret = await Promise.all(users.map(createUser));
			[owner, dj, guest, other] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});	
			ret = await createEvent(owner.apiKey, {name: eventName, visibility: 'PRIVATE'});
			eventid = ret.body.id;
			ret = await createPlaylist(owner.apiKey, {name: 'myplaylist'});
			playlistid = ret.body.id;
			await Promise.all([
				...[0, 1, 2, 3, 4, 5].map(async (serviceid: number) => addTrack(owner.apiKey, playlistid, serviceid.toString())),
				addPlaylistToEvent(owner.apiKey, eventid, playlistid)
			]);
			await eventStartVoting(owner.apiKey, eventid);
			trackids = (await getEvent(owner.apiKey, eventid)).body.eventPlaylists[0].unplayedTracks.map(x => x.id);
		});
	
		it('Should "like" track 0', async function() {
			ret = await voteEventTrack(owner.apiKey, eventid, trackids[0], 'like');
			expect(ret.status).equals(ResponseStatusTypes.OK);
			ret = await getEvent(owner.apiKey, eventid);
			let track = ret.body.eventPlaylists[0].unplayedTracks.find(x => x.id == trackids[0]);
			expect(track.likes).length(1);
			expect(track.likes[0]).equals(owner.id);
		});
		it('Should "unlike" track 1', async function() {
			ret = await voteEventTrack(owner.apiKey, eventid, trackids[1], 'like');
			ret = await voteEventTrack(owner.apiKey, eventid, trackids[1], 'unlike');
			expect(ret.status).equals(ResponseStatusTypes.OK);
			ret = await getEvent(owner.apiKey, eventid);
			let track = ret.body.eventPlaylists[0].unplayedTracks.find(x => x.id == trackids[1]);
			expect(track.likes).length(0);
		});
	
		it('Should "dislike" track 2', async function() {
			ret = await voteEventTrack(owner.apiKey, eventid, trackids[2], 'dislike');
			expect(ret.status).equals(ResponseStatusTypes.OK);
			ret = await getEvent(owner.apiKey, eventid);
			let track = ret.body.eventPlaylists[0].unplayedTracks.find(x => x.id == trackids[2]);
			expect(track.dislikes).length(1);
			expect(track.dislikes[0]).equals(owner.id);
		});
		it('Should "undislike" track 3', async function() {
			ret = await voteEventTrack(owner.apiKey, eventid, trackids[3], 'dislike');
			ret = await voteEventTrack(owner.apiKey, eventid, trackids[3], 'undislike');
			expect(ret.status).equals(ResponseStatusTypes.OK);
			ret = await getEvent(owner.apiKey, eventid);
			let track = ret.body.eventPlaylists[0].unplayedTracks.find(x => x.id == trackids[3]);
			expect(track.dislikes).length(0);
		});
		it('Should "undislike" and "like" track 4', async function() {
			ret = await voteEventTrack(owner.apiKey, eventid, trackids[4], 'dislike');
			ret = await voteEventTrack(owner.apiKey, eventid, trackids[4], 'like');
			expect(ret.status).equals(ResponseStatusTypes.OK);
			ret = await getEvent(owner.apiKey, eventid);
			let track = ret.body.eventPlaylists[0].unplayedTracks.find(x => x.id == trackids[4]);
			expect(track.dislikes).length(0);
			expect(track.likes).length(1);
		});
		it('Should "unlike" and "dislike" track 5', async function() {
			ret = await voteEventTrack(owner.apiKey, eventid, trackids[5], 'like');
			ret = await voteEventTrack(owner.apiKey, eventid, trackids[5], 'dislike');
			expect(ret.status).equals(ResponseStatusTypes.OK);
			ret = await getEvent(owner.apiKey, eventid);
			let track = ret.body.eventPlaylists[0].unplayedTracks.find(x => x.id == trackids[5]);
			expect(track.dislikes).length(1);
			expect(track.likes).length(0);
		});
	});

	describe('Event Socket Updated', function() {
		let ownerSocket: SocketIOClient.Socket;
		let djSocket: SocketIOClient.Socket;
		let guestSocket: SocketIOClient.Socket;
		let otherSocket: SocketIOClient.Socket;
		let sockets: SocketIOClient.Socket[] = [];
		
		describe(SocketActions.CONNECTED, function() {
			before(async function() {
				await reset();
				ret = await Promise.all(users.map(createUser));
				[owner, dj, guest, other] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});
				ret = await createEvent(owner.apiKey, {name: eventName, visibility: 'PRIVATE'});
				eventid = ret.body.id;
				await Promise.all([
					addDJToEvent(owner.apiKey, eventid, dj.id), 
					addGuestToEvent(owner.apiKey, eventid, guest.id)
				]);
			});

			it('Should connect', function(done) {
				ownerSocket = eventSocketConnect(owner.apiKey, eventid);
				ownerSocket.on(SocketActions.CONNECTED, async function() {
					done();
				});
				ownerSocket.on(SocketActions.ERROR, async function() {
					done(new Error('ERROR when trying to connect'))
				});
				ownerSocket.connect();
			});
			afterEach(async function() {
				let disconnected = [ownerSocket].map(socket => new Promise(resolve => {
					if (!socket) resolve(); 
					socket.on('disconnect', () => resolve());
					if (!socket.disconnected) {
						socket.emit(SocketActions.REQUEST_DISCONNECT);
					} else {
						resolve();
					}
				}));
				await Promise.all(disconnected);
				expect(socketController.connections).length(0);				
			});
			describe('Should fail to connect if ...', function() {
				it('eventid not provided', function(done) {
					ownerSocket = socketio('http://localhost:3030/events', {query: {apiKey: owner.apiKey}});
					ownerSocket.on(SocketActions.CONNECTED, async function (data) {
						done(new Error('successfully connected'));
					});
					ownerSocket.on(SocketActions.ERROR, function () {
						done();
					});
				});
				it('apiKey not provided', function(done) {
					ownerSocket = socketio('http://localhost:3030/events', {query: {eventid}});
					ownerSocket.on(SocketActions.CONNECTED, async function (data) {
						done(new Error('successfully connected'));
					});
					ownerSocket.on(SocketActions.ERROR, function () {
						done();
					});
					ownerSocket.connect();
				});
				it('event does not exist', function(done) {
					ownerSocket = socketio('http://localhost:3030/events', {query: {eventid: '0123456789', apiKey: owner.apiKey}});
					ownerSocket.on(SocketActions.CONNECTED, async function (data) {
						done(new Error('successfully connected'));
					});
					ownerSocket.on(SocketActions.ERROR, function () {
						done();
					});
					ownerSocket.connect();
				});
				it('apiKey does not exist', function(done) {
					ownerSocket = socketio('http://localhost:3030/events', {query: {eventid, apiKey: '0123456789'}});
					ownerSocket.on(SocketActions.CONNECTED, async function (data) {
						done(new Error('successfully connected'));
					});
					ownerSocket.on(SocketActions.ERROR, function () {
						done();
					});
					ownerSocket.connect();
				});
				it('user does not have rights to access the event (other)', function(done) {
					ownerSocket = socketio('http://localhost:3030/events', {autoConnect: false, query: {eventid, apiKey: other.apiKey}});
					ownerSocket.on(SocketActions.CONNECTED, async function (data) {
						done(new Error('successfully connected'));
					});
					ownerSocket.on(SocketActions.ERROR, function () {
						done();
					});
					ownerSocket.connect();
				});
			});
		});

		describe('Update', function() {
			this.timeout(10000);
			beforeEach(async function() {
				await reset();
				ret = await Promise.all(users.map(createUser));
				[owner, dj, guest, other] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});	
				ret = await createEvent(owner.apiKey, {name: eventName, visibility: 'PRIVATE'});
				eventid = ret.body.id;
				ret = await createPlaylist(owner.apiKey, {name: 'myplaylist'});
				playlistid = ret.body.id;
				await Promise.all([
					...[0, 1, 2].map(async (serviceid: number) => addTrack(owner.apiKey, playlistid, serviceid.toString())),
					addDJToEvent(owner.apiKey, eventid, dj.id), 
					addGuestToEvent(owner.apiKey, eventid, guest.id),
					addPlaylistToEvent(owner.apiKey, eventid, playlistid)
				]);
				ret = await getEvent(owner.apiKey, eventid);
				// reset socket connections
				sockets = [ownerSocket, djSocket, guestSocket] = [owner, dj, guest].map(user => eventSocketConnect(user.apiKey, eventid));
				await Promise.all(sockets.map(socket => new Promise((resolve, reject) => {
					socket.on(SocketActions.CONNECTED, () => resolve());
					socket.on('error', function() {
						console.log('error');
						reject();
					});
					socket.on(SocketActions.ERROR, function() {
						console.log('ERROR');
						reject();
					});
					socket.connect();
				})));
				expect(socketController.connections).length(3);
			});
			afterEach(async function() {
				let disconnected = [ownerSocket, djSocket, guestSocket, otherSocket].map((socket, i) => new Promise(resolve => {
					if (!socket) resolve(); 
					socket.on('disconnect', () => {
						resolve();
					});
					if (!socket.disconnected) {
						socket.emit(SocketActions.REQUEST_DISCONNECT);
					} else {
						resolve();
					}
				}));
				await Promise.all(disconnected);
				expect(socketController.connections).length(0);				
			});
			describe(SocketActions.EVENT_UPDATE, function() {
				it('Should send update to owner, dj and guest; Should disconnect other when visibility becomes PRIVATE', async function() {
					ret = [];
					await updateEvent(owner.apiKey, eventid, {visibility: 'PUBLIC'});
					otherSocket = await eventSocketConnect(other.apiKey, eventid);
					let otherDisconnected = new Promise((resolve) => {
						otherSocket.on('disconnect', () => resolve(true));
					});
					await otherSocket.connect();
					let gotUpdates = sockets.map(socket => new Promise((resolve, reject) => {
						socket.on(SocketActions.EVENT_UPDATE, function (data) {
							expect(data).have.property('by');
							if (data.visibility == 'PRIVATE') {
								expect(data).to.have.property('visibility');
								expect(data).to.have.property('name');
							}
							resolve(true)
						});
						socket.on(SocketActions.ERROR, function (data) {
							reject(new Error(data));
						});
					}));
					await updateEvent(owner.apiKey, eventid, {visibility: 'PRIVATE', name: 'lalaland'});
					ret = await Promise.all([...gotUpdates, otherDisconnected]);
					expect(ret).satisfies(ret => ret.every(x => x == true));
				});
			});
			describe(SocketActions.EVENT_ADD_DJS, function() {
				it('Should send update to owner, dj and guest', async function() {
					let gotUpdates = sockets.map(socket => new Promise((resolve, reject) => {
						socket.on(SocketActions.EVENT_ADD_DJS, function (data) {
							expect(data).have.property('by');
							expect(data).have.property('userids');
							expect(data.userids).include(other.id);
							resolve(true)
						});
						socket.on(SocketActions.ERROR, function (data) {
							reject(new Error(data));
						});
					}));
					await addDJToEvent(owner.apiKey, eventid, other.id);
					ret = await Promise.all(gotUpdates);
					expect(ret).satisfies(ret => ret.every(x => x == true));
				});
			});
			describe(SocketActions.EVENT_REMOVE_DJS, function() {
				it('Should send update to owner, dj and guest; dj should be disconnected', async function() {
					let gotUpdates = 
					await sockets.map(socket => new Promise((resolve, reject) => {
						socket.on(SocketActions.EVENT_REMOVE_DJS, function (data) {
							expect(data).have.property('by');
							expect(data).have.property('userids');
							expect(data.userids).include(dj.id);
							resolve(true)
						});
						socket.on(SocketActions.ERROR, function (data) {
							reject(new Error(data));
						});
					}));
					let djDisconnected: Promise<any> = new Promise((resolve) => {
						djSocket.on('disconnect', () => {
							resolve(true);
						});
					});
					await removeDJFromEvent(owner.apiKey, eventid, dj.id);
					ret = await Promise.all([...gotUpdates, djDisconnected]);
					expect(ret).satisfies(ret => ret.every(x => x == true));
				});
			});
			describe(SocketActions.EVENT_ADD_GUESTS, function() {
				it('Should send update to owner, dj and guest', async function() {
					let gotUpdates = sockets.map(socket => new Promise((resolve, reject) => {
						socket.on(SocketActions.EVENT_ADD_GUESTS, function (data) {
							expect(data).have.property('by');
							expect(data).have.property('userids');
							expect(data.userids).include(other.id);
							resolve(true)
						});
						socket.on(SocketActions.ERROR, function (data) {
							reject(new Error(data));
						});
					}));
					await addGuestToEvent(owner.apiKey, eventid, other.id);
					ret = await Promise.all(gotUpdates);
					expect(ret).satisfies(ret => ret.every(x => x == true));
				});
			});
			describe(SocketActions.EVENT_REMOVE_GUESTS, function() {
				it('Should send update to owner, dj and guest', async function() {
					let gotUpdates = sockets.map(socket => new Promise((resolve, reject) => {
						socket.on(SocketActions.EVENT_REMOVE_GUESTS, function (data) {
							expect(data).have.property('by');
							expect(data).have.property('userids');
							expect(data.userids).include(guest.id);
							resolve(true)
						});
						socket.on(SocketActions.ERROR, function (data) {
							reject(new Error(data));
						});
					}));
					let guestDisconnected: Promise<any> = new Promise((resolve) => {
						guestSocket.on('disconnect', () => {
							resolve(true);
						});
					});
					await removeGuestFromEvent(owner.apiKey, eventid, guest.id);
					ret = await Promise.all([...gotUpdates, guestDisconnected]);
					expect(ret).satisfies(ret => ret.every(x => x == true));
				});
			});
			describe(SocketActions.EVENT_VOTING_STARTED, function() {
				it('Should send update to owner, dj and guest', async function() {
					let gotUpdates = sockets.map(socket => new Promise((resolve, reject) => {
						socket.on(SocketActions.EVENT_VOTING_STARTED, function (data) {
							expect(data).have.property('by');
							expect(data).have.property('eventPlaylists');
							expect(data.eventPlaylists[0]).have.property('id');
							resolve(true);
						});
						socket.on(SocketActions.ERROR, function (data) {
							reject(new Error(data));
						});
					}));
					await eventStartVoting(owner.apiKey, eventid);
					ret = await Promise.all(gotUpdates);
					expect(ret).satisfies(ret => ret.every(x => x == true));
				});
				it('Should send update to owner, dj and guest if event is started through a job', async function() {
					let gotUpdates = [ownerSocket].map(socket => new Promise((resolve, reject) => {
						socket.on(SocketActions.EVENT_VOTING_STARTED, function (data) {
							expect(data).have.property('by');
							expect(data).have.property('eventPlaylists');
							expect(data.eventPlaylists[0]).have.property('id');
							resolve(true);
						});
						socket.on(SocketActions.ERROR, function (data) {
							reject(new Error(data));
						});
					}));
					let start: Date = new Date();
					start.setSeconds(start.getSeconds() + 1);
					let end: Date = new Date(start);
					end.setSeconds(end.getSeconds() + 1);
					ret = await updateEvent(owner.apiKey, eventid, {votingSchedule: {
						start: start.toISOString(),
						end: end.toISOString()
					}})
					expect(ret.status).equals(ResponseStatusTypes.OK);
					ret = await Promise.all(gotUpdates);
					expect(ret).satisfies(ret => ret.every(x => x == true));
				});
			});
			describe(SocketActions.EVENT_VOTING_ENDED, function() {
				it('Should send update to owner, dj and guest', async function() {
					let gotUpdates = sockets.map(socket => new Promise((resolve, reject) => {
						socket.on(SocketActions.EVENT_VOTING_ENDED, function (data) {
							expect(data).have.property('by');
							expect(data).have.property('playlists');
							expect(data.playlists[0]).have.property('id');
							resolve(true);
						});
						socket.on(SocketActions.ERROR, function (data) {
							reject(new Error(data));
						});
					}));
					await eventStartVoting(owner.apiKey, eventid);
					await eventEndVoting(owner.apiKey, eventid);
					ret = await Promise.all(gotUpdates);
					expect(ret).satisfies(ret => ret.every(x => x == true));
				});
			});
			describe(SocketActions.EVENT_EVENT_STARTED, function() {
				it('Should send update to owner, dj and guest', async function() {
					let gotUpdates = sockets.map(socket => new Promise((resolve, reject) => {
						socket.on(SocketActions.EVENT_EVENT_STARTED, function (data) {
							expect(data).have.property('by');
							expect(data).have.property('eventPlaylists');
							expect(data.eventPlaylists[0]).have.property('id');
							resolve(true);
						});
						socket.on(SocketActions.ERROR, function (data) {
							reject(new Error(data));
						});
					}));
					await eventStartEvent(owner.apiKey, eventid);
					ret = await Promise.all(gotUpdates);
					expect(ret).satisfies(ret => ret.every(x => x == true));
				});
				it('Should send update to owner, dj and guest if event is started through a job', async function() {
					let gotUpdates = sockets.map(socket => new Promise((resolve, reject) => {
						socket.on(SocketActions.EVENT_EVENT_STARTED, function (data) {
							expect(data).have.property('by');
							expect(data).have.property('eventPlaylists');
							expect(data.eventPlaylists[0]).have.property('id');
							resolve(true);
						});
						socket.on(SocketActions.ERROR, function (data) {
							reject(new Error(data));
						});
					}));
					let start: Date = new Date();
					start.setSeconds(start.getSeconds() + 1);
					let end: Date = new Date(start);
					end.setSeconds(end.getSeconds() + 1);
					ret = await updateEvent(owner.apiKey, eventid, {eventSchedule: {
						start: start.toISOString(),
						end: end.toISOString()
					}})
					expect(ret.status).equals(ResponseStatusTypes.OK);
					ret = await Promise.all(gotUpdates);
					expect(ret).satisfies(ret => ret.every(x => x == true));
				});
			});
			describe(SocketActions.EVENT_EVENT_ENDED, function() {
				it('Should send update to owner, dj and guest', async function() {
					let gotUpdates = sockets.map(socket => new Promise((resolve, reject) => {
						socket.on(SocketActions.EVENT_EVENT_ENDED, function (data) {
							expect(data).have.property('by');
							expect(data).have.property('playlists');
							expect(data.playlists[0]).have.property('id');
							resolve(true);
						});
						socket.on(SocketActions.ERROR, function (data) {
							reject(new Error(data));
						});
					}));
					await eventStartEvent(owner.apiKey, eventid);
					await eventEndEvent(owner.apiKey, eventid);
					ret = await Promise.all(gotUpdates);
					expect(ret).satisfies(ret => ret.every(x => x == true));
				});
			});
			describe(SocketActions.EVENT_TRACK_LIKE, function() {
				it('Should send update to owner, dj and guest', async function() {
					await eventStartVoting(owner.apiKey, eventid);
					let event = (await getEvent(owner.apiKey, eventid)).body;
					let trackid: string = event.eventPlaylists[0].unplayedTracks[0].id;
					let playlistid: string = event.eventPlaylists[0].id;
					let promises = sockets.map(socket => new Promise((resolve, reject) => {
						socket.on(SocketActions.EVENT_TRACK_LIKE, (data) => {
							expect(data).have.property('by');
							expect(data.trackid).equals(trackid);
							expect(data.playlistid).equals(playlistid);
							expect(data.userid).equals(owner.id);
							expect(data.trackLikesTotal).equals(1);
							expect(data.trackDislikesTotal).equals(0);
							resolve(true);
						});
						socket.on(SocketActions.ERROR, function (data) {
							reject(new Error(data));
						});
						socket.on('disconnect', function (data) {
							reject(new Error(data));
						});
					}));
					await voteEventTrack(owner.apiKey, eventid, trackid, 'like');
					ret = await Promise.all(promises);
					expect(ret).satisfies(ret => ret.every(x => x == true));
				});
				it('Should send TWO updates to owner, dj and guest when disliked track becomes liked', async function() {
					await eventStartVoting(owner.apiKey, eventid);
					let event = (await getEvent(owner.apiKey, eventid)).body;
					let trackid: string = event.eventPlaylists[0].unplayedTracks[0].id;
					let playlistid: string = event.eventPlaylists[0].id;
					let promises = [
						...sockets.map(socket => new Promise((resolve, reject) => {
							socket.on(SocketActions.EVENT_TRACK_LIKE, (data) => {
								expect(data).have.property('by');
								expect(data.trackid).equals(trackid);
								expect(data.playlistid).equals(playlistid);
								expect(data.userid).equals(owner.id);
								expect(data.trackLikesTotal).equals(1);
								expect(data.trackDislikesTotal).equals(0);
								resolve(true);
							});
							socket.on(SocketActions.ERROR, function (data) {
								reject(new Error(data));
							});
							socket.on('disconnect', function (data) {
								reject(new Error(data));
							});
						})),
						...sockets.map(socket => new Promise((resolve, reject) => {
							socket.on(SocketActions.EVENT_TRACK_UNDISLIKE, (data) => {
								expect(data).have.property('by');
								expect(data.trackid).equals(trackid);
								expect(data.playlistid).equals(playlistid);
								expect(data.userid).equals(owner.id);
								expect(data.trackLikesTotal).equals(1);
								expect(data.trackDislikesTotal).equals(0);
								resolve(true);
							});
						}))
					];
					await voteEventTrack(owner.apiKey, eventid, trackid, 'dislike');
					await voteEventTrack(owner.apiKey, eventid, trackid, 'like');
					ret = await Promise.all(promises);
					expect(ret).satisfies(ret => ret.every(x => x == true));
				});

				// occasional bug?
				it('Should send the EVENT_TRACK_MOVE update if a vote causes a change in order of the tracks', async function() {
					await eventStartVoting(owner.apiKey, eventid);
					let event = (await getEvent(owner.apiKey, eventid)).body;
					let trackid: string = event.eventPlaylists[0].unplayedTracks[2].id; // user last track in list
					let playlistid: string = event.eventPlaylists[0].id;
					let promises = sockets.map(socket => new Promise((resolve, reject) => {
						socket.on(SocketActions.EVENT_TRACK_MOVE, data => {
							expect(data.index).equals(0);
							resolve(true)
						});
						socket.on(SocketActions.ERROR, function (data) {
							reject(new Error(data));
						});
						socket.on('disconnect', function (data) {
							reject(new Error(data));
						});
					}));
					ret = await voteEventTrack(owner.apiKey, eventid, trackid, 'like');
					ret = await Promise.all(promises);
					expect(ret).satisfies(r => r.every(x => x == true));
				});
			});
			describe(SocketActions.EVENT_TRACK_UNLIKE, function() {
				it('Should send update to owner, dj and guest', async function() {
					await eventStartVoting(owner.apiKey, eventid);
					let event = (await getEvent(owner.apiKey, eventid)).body;
					let trackid: string = event.eventPlaylists[0].unplayedTracks[0].id;
					let playlistid: string = event.eventPlaylists[0].id;
					let promises = sockets.map(socket => new Promise((resolve, reject) => {
						socket.on(SocketActions.EVENT_TRACK_UNLIKE, (data) => {
							expect(data).have.property('by');
							expect(data.trackid).equals(trackid);
							expect(data.playlistid).equals(playlistid);
							expect(data.userid).equals(owner.id);
							expect(data.trackLikesTotal).equals(0);
							expect(data.trackDislikesTotal).equals(0);
							resolve(true);
						});
						socket.on(SocketActions.ERROR, function (data) {
							reject(new Error(data));
						});
						socket.on('disconnect', function (data) {
							reject(new Error(data));
						});
					}));
					await voteEventTrack(owner.apiKey, eventid, trackid, 'like');
					await voteEventTrack(owner.apiKey, eventid, trackid, 'unlike');
					ret = await Promise.all(promises);
					expect(ret).satisfies(ret => ret.every(x => x == true));
				});
			});
			describe(SocketActions.EVENT_TRACK_DISLIKE, function() {
				it('Should send update to owner, dj and guest', async function() {
					await eventStartVoting(owner.apiKey, eventid);
					let event = (await getEvent(owner.apiKey, eventid)).body;
					let trackid: string = event.eventPlaylists[0].unplayedTracks[0].id;
					let playlistid: string = event.eventPlaylists[0].id;
					let promises = sockets.map(socket => new Promise((resolve, reject) => {
						socket.on(SocketActions.EVENT_TRACK_DISLIKE, (data) => {
							expect(data).have.property('by');
							expect(data.trackid).equals(trackid);
							expect(data.playlistid).equals(playlistid);
							expect(data.userid).equals(owner.id);
							expect(data.trackLikesTotal).equals(0);
							expect(data.trackDislikesTotal).equals(1);
							resolve(true)
						});
						socket.on(SocketActions.ERROR, function (data) {
							reject(new Error(data));
						});
						socket.on('disconnect', function (data) {
							reject(new Error(data));
						});
					}));
					await voteEventTrack(owner.apiKey, eventid, trackid, 'dislike');
					ret = await Promise.all(promises);
					expect(ret).satisfies(ret => ret.every(x => x == true));
				});
				it('Should send TWO updates to owner, dj and guest when liked track becomes disliked', async function() {
					await eventStartVoting(owner.apiKey, eventid);
					let event = (await getEvent(owner.apiKey, eventid)).body;
					let trackid: string = event.eventPlaylists[0].unplayedTracks[0].id;
					let playlistid: string = event.eventPlaylists[0].id;
					let promises = [
						...sockets.map(socket => new Promise((resolve, reject) => {
							socket.on(SocketActions.EVENT_TRACK_DISLIKE, (data) => {
								expect(data).have.property('by');
								expect(data.trackid).equals(trackid);
								expect(data.playlistid).equals(playlistid);
								expect(data.userid).equals(owner.id);
								expect(data.trackLikesTotal).equals(0);
								expect(data.trackDislikesTotal).equals(1);
								resolve(true);
							});
							socket.on(SocketActions.ERROR, function (data) {
								reject(new Error(data));
							});
							socket.on('disconnect', function (data) {
								reject(new Error(data));
							});
						})),
						...sockets.map(socket => new Promise((resolve, reject) => {
							socket.on(SocketActions.EVENT_TRACK_UNLIKE, (data) => {
								expect(data).have.property('by');
								expect(data.trackid).equals(trackid);
								expect(data.playlistid).equals(playlistid);
								expect(data.userid).equals(owner.id);
								expect(data.trackLikesTotal).equals(0);
								expect(data.trackDislikesTotal).equals(1);
								resolve(true);
							});
						}))
					];
					await voteEventTrack(owner.apiKey, eventid, trackid, 'like');
					await voteEventTrack(owner.apiKey, eventid, trackid, 'dislike');
					ret = await Promise.all(promises);
					expect(ret).satisfies(ret => ret.every(x => x == true));
				});
			});
			describe(SocketActions.EVENT_TRACK_UNDISLIKE, function() {
				it('Should send update to owner, dj and guest', async function() {
					await eventStartVoting(owner.apiKey, eventid);
					let event = (await getEvent(owner.apiKey, eventid)).body;
					let trackid: string = event.eventPlaylists[0].unplayedTracks[0].id;
					let playlistid: string = event.eventPlaylists[0].id;
					let promises = sockets.map(socket => new Promise((resolve, reject) => {
						socket.on(SocketActions.EVENT_TRACK_UNDISLIKE, (data) => {
							expect(data).have.property('by');
							expect(data.trackid).equals(trackid);
							expect(data.playlistid).equals(playlistid);
							expect(data.userid).equals(owner.id);
							expect(data.trackLikesTotal).equals(0);
							expect(data.trackDislikesTotal).equals(0);
							resolve(true)
						});
						socket.on(SocketActions.ERROR, function (data) {
							reject(new Error(data));
						});
						socket.on('disconnect', function (data) {
							reject(new Error(data));
						});
					}));
					await voteEventTrack(owner.apiKey, eventid, trackid, 'dislike');
					await voteEventTrack(owner.apiKey, eventid, trackid, 'undislike');
					ret = await Promise.all(promises);
					expect(ret).satisfies(ret => ret.every(x => x == true));
				});
			});
			describe(SocketActions.EVENT_TRACK_MOVE, function() {
				it('Should send update to owner, dj and guest', async function() {
					await eventStartVoting(owner.apiKey, eventid);
					let index: number = 2;
					let event = (await getEvent(owner.apiKey, eventid)).body;
					let trackid: string = event.eventPlaylists[0].unplayedTracks[0].id;
					let playlistid: string = event.eventPlaylists[0].id;
					let promises = sockets.map(socket => new Promise((resolve, reject) => {
						socket.on(SocketActions.EVENT_TRACK_MOVE, (data) => {
							expect(data).have.property('by');
							expect(data.trackid).equals(trackid);
							expect(data.playlistid).equals(playlistid);
							expect(data.index).equals(index);
							resolve(true)
						});
						socket.on(SocketActions.ERROR, function (data) {
							reject(new Error(data));
						});
						socket.on('disconnect', function (data) {
							reject(new Error(data));
						});
					}));
					ret = await moveEventTrack(owner.apiKey, eventid, trackid, index);
					ret = await Promise.all(promises);
					expect(ret).satisfies(ret => ret.every(x => x == true));
				});
			});
			describe(SocketActions.EVENT_TRACK_PLAY, function() {
				it('Should send update to owner, dj and guest', async function() {
					let gotUpdates = sockets.map(socket => new Promise((resolve, reject) => {
						socket.on(SocketActions.EVENT_TRACK_PLAY, function (data) {
							expect(data).have.property('by');
							expect(data).have.property('trackid');
							expect(data).have.property('playlistid');
							expect(data).have.property('timestamp');
							expect(data).have.property('trackPosition');
							resolve(true)
						});
						socket.on(SocketActions.ERROR, function (data) {
							reject(new Error(data));
						});
					}));
					await eventStartEvent(owner.apiKey, eventid);
					await playerAction(owner.apiKey, eventid, 'play', {
						trackid: (await getEvent(owner.apiKey, eventid)).body.eventPlaylists[0].unplayedTracks[0].id,
						timestamp: (new Date()).toISOString(),
						trackPosition: 0
					});					
					ret = await Promise.all(gotUpdates);
					expect(ret).satisfies(ret => ret.every(x => x == true));
				});
			});
			describe(SocketActions.EVENT_TRACK_PAUSE, function() {
				it('Should send update to owner, dj and guest', async function() {
					await eventStartEvent(owner.apiKey, eventid);
					let trackid: string = (await getEvent(owner.apiKey, eventid)).body.eventPlaylists[0].unplayedTracks[0].id;					
					await playerAction(owner.apiKey, eventid, 'play', {
						trackid,
						timestamp: (new Date()).toISOString(),
						trackPosition: 0
					});		
					let gotUpdates = sockets.map(socket => new Promise((resolve, reject) => {
						socket.on(SocketActions.EVENT_TRACK_PAUSE, function (data) {
							expect(data).have.property('by');
							expect(data).have.property('trackid');
							expect(data).have.property('playlistid');
							expect(data).have.property('timestamp');
							expect(data).have.property('trackPosition');
							resolve(true)
						});
						socket.on(SocketActions.ERROR, function (data) {
							reject(new Error(data));
						});
					}));
					await playerAction(owner.apiKey, eventid, 'pause', {timestamp: (new Date()).toISOString(), trackPosition: 0});
					ret = await Promise.all(gotUpdates);
					expect(ret).satisfies(ret => ret.every(x => x == true));
				});
			});
			describe(SocketActions.EVENT_TRACK_SEEK, function() {
				it('Should send update to owner, dj and guest', async function() {
					await eventStartEvent(owner.apiKey, eventid);
					let trackid: string = (await getEvent(owner.apiKey, eventid)).body.eventPlaylists[0].unplayedTracks[0].id;					
					await playerAction(owner.apiKey, eventid, 'play', {
						trackid,
						timestamp: (new Date()).toISOString(),
						trackPosition: 0
					});
					let gotUpdates = sockets.map(socket => new Promise((resolve, reject) => {
						socket.on(SocketActions.EVENT_TRACK_SEEK, function (data) {
							expect(data).have.property('by');
							expect(data).have.property('trackid');
							expect(data).have.property('playlistid');
							expect(data).have.property('timestamp');
							expect(data).have.property('trackPosition');
							resolve(true)
						});
						socket.on(SocketActions.ERROR, function (data) {
							reject(new Error(data));
						});
					}));
					await playerAction(owner.apiKey, eventid, 'seek', {trackPosition: 50, timestamp: (new Date()).toISOString()});
					ret = await Promise.all(gotUpdates);
					expect(ret).satisfies(ret => ret.every(x => x == true));
				});
			});
			describe(SocketActions.EVENT_TRACK_PLAY_REQUEST, function() {
				it('Should send update to owner, dj and guest', async function() {
					let djRecievesEVENT_TRACK_PLAY: Promise<any> = new Promise((resolve, reject) => {
						djSocket.on(SocketActions.EVENT_TRACK_PLAY, data => {
							expect(data).have.property('by');
							expect(data).have.property('trackid');
							expect(data).have.property('playlistid');
							expect(data).have.property('timestamp');
							expect(data).have.property('trackPosition');
							resolve(true);
						});
						djSocket.on(SocketActions.ERROR, function (data) {
							reject(new Error(data));
						});
						djSocket.on('disconnect', function (data) {
							reject('dj disonnceted');
						});
					});
					let ownerRecievesEVENT_TRACK_PLAY_REQUEST: Promise<any> = new Promise((resolve, reject) => {
						ownerSocket.on(SocketActions.EVENT_TRACK_PLAY_REQUEST, async data => {
							expect(data).have.property('by');
							expect(data).have.property('trackid');
							expect(data).have.property('playlistid');
							let ret: any = await playerAction(owner.apiKey, eventid, 'play', {trackid: data.trackid, timestamp: (new Date()).toISOString(), trackPosition: 0});
							expect(ret.status).equals(ResponseStatusTypes.OK);
							resolve(true);
						});
						ownerSocket.on(SocketActions.ERROR, function (data) {
							reject(new Error(data));
						});
						ownerSocket.on('disconnect', function (data) {
							reject('owner disonnceted');
						});
					});
					await eventStartEvent(owner.apiKey, eventid);
					ret = await getEvent(dj.apiKey, eventid);
					ret = await playerAction(dj.apiKey, eventid, 'play', {
						trackid: ret.body.eventPlaylists[0].unplayedTracks[0].id,
					});
					ret = await Promise.all([djRecievesEVENT_TRACK_PLAY, ownerRecievesEVENT_TRACK_PLAY_REQUEST]);
					expect(ret).satisfies(ret => ret.every(x => x == true));
				});
			});
			describe(SocketActions.EVENT_TRACK_PAUSE_REQUEST, function() {
				it('Should send update to owner, dj and guest', async function() {
					let djRecievesEVENT_TRACK_PAUSE: Promise<any> = new Promise((resolve, reject) => {
						djSocket.on(SocketActions.EVENT_TRACK_PAUSE, data => {
							expect(data).have.property('by');
							expect(data).have.property('trackid');
							expect(data).have.property('playlistid');
							expect(data).have.property('timestamp');
							expect(data).have.property('trackPosition');
							resolve(true);
						});
						djSocket.on(SocketActions.ERROR, function (data) {
							reject(new Error(data));
						});
						djSocket.on('disconnect', function (data) {
							reject('dj disonnceted');
						});
					});
					let ownerRecievesEVENT_TRACK_PAUSE_REQUEST: Promise<any> = new Promise((resolve, reject) => {
						ownerSocket.on(SocketActions.EVENT_TRACK_PAUSE_REQUEST, async data => {
							expect(data).have.property('by');
							expect(data).have.property('trackid');
							expect(data).have.property('playlistid');
							let ret: any = await playerAction(owner.apiKey, eventid, 'pause', {timestamp: (new Date()).toISOString(), trackPosition: 0});
							expect(ret.status).equals(ResponseStatusTypes.OK);
							resolve(true);
						});
						ownerSocket.on(SocketActions.ERROR, function (data) {
							reject(new Error(data));
						});
						ownerSocket.on('disconnect', function (data) {
							reject('owner disonnceted');
						});
					});
					await eventStartEvent(owner.apiKey, eventid);
					ret = await getEvent(dj.apiKey, eventid);
					ret = await playerAction(owner.apiKey, eventid, 'play', {trackid: ret.body.eventPlaylists[0].unplayedTracks[0].id, timestamp: (new Date()).toISOString(), trackPosition: 0});
					ret = await playerAction(dj.apiKey, eventid, 'pause');
					ret = await Promise.all([djRecievesEVENT_TRACK_PAUSE, ownerRecievesEVENT_TRACK_PAUSE_REQUEST]);
					expect(ret).satisfies(ret => ret.every(x => x == true));
				});
			});
			describe(SocketActions.EVENT_TRACK_SEEK_REQUEST, function() {
				it('Should send update to owner, dj and guest', async function() {
					let djRecievesEVENT_TRACK_SEEK: Promise<any> = new Promise((resolve, reject) => {
						djSocket.on(SocketActions.EVENT_TRACK_SEEK, data => {
							expect(data).have.property('by');
							expect(data).have.property('trackid');
							expect(data).have.property('playlistid');
							expect(data).have.property('timestamp');
							expect(data).have.property('trackPosition');
							resolve(true);
						});
						djSocket.on(SocketActions.ERROR, function (data) {
							reject(new Error(data));
						});
						djSocket.on('disconnect', function (data) {
							reject('dj disonnceted');
						});
					});
					let ownerRecievesEVENT_TRACK_SEEK_REQUEST: Promise<any> = new Promise((resolve, reject) => {
						ownerSocket.on(SocketActions.EVENT_TRACK_SEEK_REQUEST, async data => {
							expect(data).have.property('by');
							expect(data).have.property('trackid');
							expect(data).have.property('playlistid');
							expect(data).have.property('trackPosition');
							let ret: any = await playerAction(owner.apiKey, eventid, 'seek', {trackPosition: data.trackPosition, timestamp: (new Date()).toISOString()});
							expect(ret.status).equals(ResponseStatusTypes.OK);
							resolve(true);
						});
						ownerSocket.on(SocketActions.ERROR, function (data) {
							reject(new Error(data));
						});
						ownerSocket.on('disconnect', function (data) {
							reject('owner disonnceted');
						});
					});
					await eventStartEvent(owner.apiKey, eventid);
					ret = await getEvent(dj.apiKey, eventid);
					await playerAction(owner.apiKey, eventid, 'play', {trackid: ret.body.eventPlaylists[0].unplayedTracks[0].id, timestamp: (new Date()).toISOString(), trackPosition: 0});
					ret = await playerAction(dj.apiKey, eventid, 'seek', {trackPosition: 50});
					ret = await Promise.all([djRecievesEVENT_TRACK_SEEK, ownerRecievesEVENT_TRACK_SEEK_REQUEST]);
					expect(ret).satisfies(ret => ret.every(x => x == true));
				});
			});
			describe(SocketActions.EVENT_STARTER_PACK, function() {
				it('Guest should recieve a starter pack on connect to votingActive event', async function() {
					await new Promise(resolve => {
						guestSocket.on('disconnect', () => resolve());
						guestSocket.emit(SocketActions.REQUEST_DISCONNECT);
					});
					
					await eventStartVoting(owner.apiKey, eventid);
					
					guestSocket = await eventSocketConnect(guest.apiKey, eventid);
					let guestGotStarterPack: Promise<any> = new Promise((resolve, reject) => {
						guestSocket.on(SocketActions.EVENT_STARTER_PACK, data => {
							expect(data.votingActive).equals(true);
							expect(data.eventActive).equals(false);
							expect(data.ownerIsConnected).equals(true);
							resolve(true)
						});
					});
					await new Promise((resolve) => {
						guestSocket.on(SocketActions.CONNECTED, () => resolve(true));
						guestSocket.connect();
					});
					ret = await guestGotStarterPack;
					expect(ret).equals(true);
				});
				it('Guest should recieve a starter pack with ownerIsConnect set to false on connect to eventStart event', async function() {
					await Promise.all([ownerSocket, guestSocket].map(socket => new Promise(resolve => {
						socket.on('disconnect', () => resolve());
						socket.emit(SocketActions.REQUEST_DISCONNECT);
					})));

					await eventStartVoting(owner.apiKey, eventid);
					guestSocket = await eventSocketConnect(guest.apiKey, eventid);
					let guestGotStarterPack: Promise<any> = new Promise((resolve, reject) => {
						guestSocket.on(SocketActions.EVENT_STARTER_PACK, data => {
							expect(data.votingActive).equals(true);
							expect(data.eventActive).equals(false);
							expect(data.ownerIsConnected).equals(false);
							resolve(true)
						});
					});
					await new Promise((resolve) => {
						guestSocket.on(SocketActions.CONNECTED, () => resolve(true));
						guestSocket.connect();
					});
					ret = await guestGotStarterPack;
					expect(ret).equals(true);
				});
			});
			describe(SocketActions.EVENT_OWNER_DISCONNECT, function() {
				it('Should send events to dj and guest', async function() {
					await eventStartEvent(owner.apiKey, eventid);
					ret = await getEvent(owner.apiKey, eventid);
					let trackid: string = ret.body.eventPlaylists[0].unplayedTracks[0].id;
					const now: Date = new Date();
					ret = await playerAction(owner.apiKey, eventid, 'play', {trackid, timestamp: now.toISOString(), trackPosition: 10});
					let ownerDisconnected = [guestSocket, djSocket].map(socket => new Promise(resolve => {
						socket.on(SocketActions.EVENT_OWNER_DISCONNECT, data => {
							resolve(true);
						});
					}));
					let trackPositionReset = [guestSocket, djSocket].map(socket => new Promise(resolve => {
						socket.on(SocketActions.EVENT_TRACK_SEEK, data => {
							expect(data.trackPosition).equals(0);
							resolve(true);
						});
					}));
					await new Promise(resolve => {
						ownerSocket.on('disconnect', () => resolve());
						ownerSocket.emit(SocketActions.REQUEST_DISCONNECT);
					})
					ret = await Promise.all([...ownerDisconnected, ...trackPositionReset]);
					expect(ret).satisfies(ret => ret.every(x => x == true));
				});
			});
		});
		// for (let i = 0; i < 100; i++) {
		describe('Multiple connection', function() {
			this.timeout(10000)
			before(async function() {
				await reset();
				ret = await Promise.all(users.map(createUser));
				[owner, dj, guest, other] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});	
				ret = await createEvent(owner.apiKey, {name: eventName, visibility: 'PRIVATE'});
				expect(ret.status).equals(ResponseStatusTypes.OK);
				eventid = ret.body.id;
				ret = await createPlaylist(owner.apiKey, {name: 'myplaylist'});
				expect(ret.status).equals(ResponseStatusTypes.OK);
				playlistid = ret.body.id;
				ret = await Promise.all([
					...[0, 1, 2].map(async (serviceid: number) => addTrack(owner.apiKey, playlistid, serviceid.toString())),
					addDJToEvent(owner.apiKey, eventid, dj.id), 
					addGuestToEvent(owner.apiKey, eventid, guest.id),
					addPlaylistToEvent(owner.apiKey, eventid, playlistid)
				]);
				ret.map(r => expect(r.status).equals(ResponseStatusTypes.OK));
			});
			after(async function() {
				let disconnected = [...sockets, otherSocket].map(socket => new Promise(resolve => {
					if (!socket) resolve(); 
					socket.on('disconnect', () => resolve());
					if (!socket.disconnected) {
						socket.emit(SocketActions.REQUEST_DISCONNECT);
					} else {
						resolve();
					}
				}));
				await Promise.all(disconnected);
				expect(socketController.connections).length(0);				
			});
			it('Connections by event owner should disconnect previous socketio connections', async function() {
				let connected = [];
				let disconnected = [];
				sockets = await Promise.all([1, 2].map(_ => eventSocketConnect(owner.apiKey, eventid)));
				disconnected = sockets.map(socket => new Promise(resolve => {
					socket.on('disconnect', () => resolve(true));
				}));
				connected = sockets.map(socket => new Promise(resolve => {
					socket.on(SocketActions.CONNECTED, () => resolve(true));
				}));
				sockets[0].connect();	

				await connected[0];
				expect(socketController.connections).length(1);
				sockets[1].connect();
				await Promise.all([disconnected[0], connected[1]]);
				expect(socketController.connections).length(1);
				sockets[1].emit(SocketActions.REQUEST_DISCONNECT);
				await disconnected[1];
				expect(socketController.connections.length).equal(0);
			});
		});
		// }
	});

	describe('Event Search', function() {
		let name: string = 'supercomplicatedname';
		let events: any[] = [];

		before(async function() {
			await reset();
			ret = await createUser(users[0]);
			owner = {apiKey: ret.body.apiKey, id: ret.body.id};
			ret = await Promise.all('ABCDE'.split('').map(x => createEvent(owner.apiKey, {name: x + x + x + x})));
			events = ret.map(r => r.body);
		});
		
		it('Should find event by ID', async function() {
			ret = await searchEvent(owner.apiKey, events[1].id);
			expect(ret.body.results[0].id).equals(events[1].id);
		});
		it('Should find event by ownerid', async function() {
			ret = await searchEvent(owner.apiKey, owner.id);
			expect(ret.body.results).length(events.length);
		});
		it('Should find event by description', async function() {
			await createEvent(owner.apiKey, {name: 'special', description: 'LALALAND'})
			ret = await searchEvent(owner.apiKey, 'LALALAND');
			expect(ret.body.results[0].description).equals('LALALAND');
		});
		it('Should find event by name', async function() {
			ret = await searchEvent(owner.apiKey, 'AAAA');
			expect(ret.body.results[0].name).equals('AAAA');
		});
		it(`Should limit results to limit set in config: ${search_limit}`, async function() {
			ret = await Promise.all('ABCDEFGHSLDKFJLKJ'.split('').map(x => createPlaylist(owner.apiKey, {name})));
			ret = await searchEvent(owner.apiKey, name);
			expect(ret.body.results.length).lte(search_limit);
		});
		it('Should not return event whose visibility are set to private', async function() {
			let name: string = 'private event';
			await updateEvent(owner.apiKey, eventid, {visibility: 'PRIVATE', name});
			ret = await searchEvent(owner.apiKey, name);
			expect(ret.body.results).length(0);
		});
	});
});
