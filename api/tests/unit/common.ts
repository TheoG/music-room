import { Db, MongoClient } from "mongodb";
import * as chai from 'chai';
import chaiHttp = require('chai-http');
chai.use(chaiHttp);
import * as socketio from "socket.io-client";
import * as fs from 'fs';

import app from "../../src/app";
import { Visibility, Editing, SortingType, Schedule, Coordinates } from "../../src/types/types";
import chalk from "chalk";
import { eventController } from "../../src/controllers/eventController"; 
 
// resets the database
// setup the database with setup function
export const reset = async () : Promise<any> => {
	eventController.destroyJobs();
	const client: MongoClient = await app.getClient();
	const db: Db = await app.getDB(client);
	await Promise.all(['users', 'events', 'friendships', 'playlists'].map(async name => {
		try {
			await db.collection(name).drop();
		} catch(e) {};
	}));
	await app.closeClient(client);
};

export const users = [
	{
		email: 'a@a.com',
		password: 'aaaaaa'
	},
	{
		email: 'b@b.com',
		password: 'bbbbbb'
	},
	{
		email: 'c@c.com',
		password: 'cccccc'
	},
	{
		email: 'd@d.com',
		password: 'dddddd'
	},
	{
		email: 'e@e.com',
		password: 'eeeeee'
	}
];

export const tracksServiceids = [0, 1, 2, 3, 4, 5];

// helper functions
export const printret = ret => {
	console.log({status: ret.status, body: ret.body});
};


// ======= USER =======

export const createUser = (params: {email: string, password: string, displayName?: string, firstName?: string, lastName?: string}) =>  {
	return chai.request(app.app)
		.post('/users')
		.send(params);
};

export const loginUser = (user: {email: string, password: string}) => {
	return chai.request(app.app)
		.post('/login')
		.send(user);
};

export const updateUser = (apiKey: string, id: string, update: object, avatar?: string) => {
	if (avatar) {
		return chai.request(app.app)
		.put(`/users/${id}?apiKey=${apiKey}`)
		.attach('avatar', fs.readFileSync(avatar), avatar);
	} else {
		return chai.request(app.app)
			.put(`/users/${id}?apiKey=${apiKey}`)
			.send(update);
	}
};

export const getAvatar = (path: string) => {
	return chai.request(app.app)
		.get('/' + path);
};

export const updateSettings = (apiKey: string, id: string, update: object) => {
	return chai.request(app.app)
		.put(`/users/${id}/settings?apiKey=${apiKey}`)
		.send(update);
};

export const getUserSettings = (apiKey: string, id: string) => {
	return chai.request(app.app)
		.get(`/users/${id}/settings?apiKey=${apiKey}`)
		.send();
};

export const getUser = (apiKey: string, id: string) => {
	return chai.request(app.app)
		.get(`/users/${id}?apiKey=${apiKey}`)
		.send();
};

export const getFriends = (apiKey: string, userid: string, friendid?: string) => {
	return chai.request(app.app)
		.get(`/users/${userid}/friends${friendid ? ('/' + friendid) : ''}?apiKey=${apiKey}`)
		.send();
};

export const deleteUser = (apiKey: string, id: string) => {
	return chai.request(app.app)
		.delete(`/users/${id}?apiKey=${apiKey}`)
		.send();
};

export const searchUser = (apiKey: string, keyword: string) => {
	return chai.request(app.app)
		.get(`/users/search?apiKey=${apiKey}&keyword=${keyword}`)
		.send();
};

export const userPasswordReset = (email: string, code?: string, password?: string) => {
	return chai.request(app.app)
		.post(`/users/resetpassword`)
		.send({email, code, password});
};

export const userActivate = (userid: string, code?: string, email?: string) => {
	if (code) {
		return chai.request(app.app)
			.get(`/users/${userid}/activate?code=${code}`)
			.send();
	} else if (email) {
		return chai.request(app.app)
			.get(`/users/${userid}/activate?email=${email}`)
			.send();
	} else {
		return chai.request(app.app)
			.get(`/users/${userid}/activate`)
			.send();
	}
};

// ======= FRIENDSHIP =======

export const requestFriendship = (apiKey: string, friendId: string, message?: string) => {
	return chai.request(app.app)
		.post(`/friendships?apiKey=${apiKey}`)
		.send({id: friendId, message: message ? message : 'Please be my friend'});
};

export const getFriendship = (friendshipId: string, apiKey: string) => {
	return chai.request(app.app)
		.get(`/friendships/${friendshipId}?apiKey=${apiKey}`)
		.send();
};

export const getFriendships = (apiKey: string) => {
	return chai.request(app.app)
		.get(`/friendships?apiKey=${apiKey}`)
		.send();
};

export const updateFriendship = (apiKey: string, friendshipId: string, action: 'read' | 'accept' | 'reject') => {
	return chai.request(app.app)
		.put(`/friendships/${friendshipId}/${action}?apiKey=${apiKey}`)
		.send();
};

export const deleteFriendship = (apiKey: string, friendshipId: string) => {
	return chai.request(app.app)
		.delete(`/friendships/${friendshipId}?apiKey=${apiKey}`)
		.send();
};

// ====== PLAYLIST =======

export const createPlaylist = (apiKey: string, options:{name: string, visibility?: Visibility, editing?: Editing, sortingType?: SortingType}) => {
	return chai.request(app.app)
		.post(`/playlists?apiKey=${apiKey}`)
		.send(options);
};

export const getPlaylist = (apiKey: string, id: string) => {
	return chai.request(app.app)
		.get(`/playlists/${id}?apiKey=${apiKey}`)
		.send();
};

export const getPlaylists = (apiKey: string) => {
	return chai.request(app.app)
		.get(`/playlists?apiKey=${apiKey}`)
		.send();
};

export const updatePlaylist = (apiKey: string, playlistid: string, options: object) => {
	return chai.request(app.app)
		.put(`/playlists/${playlistid}?apiKey=${apiKey}`)
		.send(options);
};

export const addTrack = (apiKey: string, playlistid: string, serviceid: string, service?: string) => {
	return chai.request(app.app)
		.put(`/playlists/${playlistid}/tracks?apiKey=${apiKey}`)
		.send(service ? {serviceid, service} : {serviceid});
};

export const removeTrack = (apiKey: string, playlistid: string, trackId: string) => {
	return chai.request(app.app)
		.delete(`/playlists/${playlistid}/tracks/${trackId}?apiKey=${apiKey}`)
		.send();
};

export const moveTrack = (apiKey: string, playlistid: string, trackId: string, index: string|number) => {
	return chai.request(app.app)
		.put(`/playlists/${playlistid}/tracks/${trackId}/move?apiKey=${apiKey}`)
		.send({index});
};

export const addEditorToPlaylist = (apiKey: string, playlistid: string, userid: string) => {
	return chai.request(app.app)
		.put(`/playlists/${playlistid}/editors/${userid}?apiKey=${apiKey}`)
		.send();
};

export const addEditorsToPlaylist = (apiKey: string, playlistid: string, userids: string[]) => {
	return chai.request(app.app)
		.put(`/playlists/${playlistid}/editors?apiKey=${apiKey}`)
		.send({userids});
};

export const removeEditorFromPlaylist = (apiKey: string, playlistid: string, userid: string) => {
	return chai.request(app.app)
		.delete(`/playlists/${playlistid}/editors/${userid}?apiKey=${apiKey}`)
		.send();
};

export const removeEditorsFromPlaylist = (apiKey: string, playlistid: string, userids: string[]) => {
	return chai.request(app.app)
		.delete(`/playlists/${playlistid}/editors?apiKey=${apiKey}`)
		.send({userids});
};

export const addGuestToPlaylist = (apiKey: string, playlistid: string, userid: string) => {
	return chai.request(app.app)
		.put(`/playlists/${playlistid}/guests/${userid}?apiKey=${apiKey}`)
		.send();
};

export const addGuestsToPlaylist = (apiKey: string, playlistid: string, userids: string[]) => {
	return chai.request(app.app)
		.put(`/playlists/${playlistid}/guests?apiKey=${apiKey}`)
		.send({userids});
};

export const removeGuestFromPlaylist = (apiKey: string, playlistid: string, userid: string) => {
	return chai.request(app.app)
		.delete(`/playlists/${playlistid}/guests/${userid}?apiKey=${apiKey}`)
		.send();
};

export const removeGuestsFromPlaylist = (apiKey: string, playlistid: string, userids: string[]) => {
	return chai.request(app.app)
		.delete(`/playlists/${playlistid}/guests?apiKey=${apiKey}`)
		.send({userids});
};

export const deletePlaylist = (apiKey: string, playlistid: string) => {
	return chai.request(app.app)
		.delete(`/playlists/${playlistid}?apiKey=${apiKey}`)
		.send();
};

export const deletePlaylists = (apiKey: string, playlistids: string[]) => {
	return chai.request(app.app)
		.delete(`/playlists?apiKey=${apiKey}`)
		.send({playlistids});
};

export const playlistSocketConnect = (apiKey: string, playlistid: string): SocketIOClient.Socket => {
	return socketio('http://localhost:3030/playlists', {autoConnect: false, query: {playlistid, apiKey}, reconnection: false, forceNew: true});
};

export const duplicatePlaylist = (apiKey: string, playlistid: string, options?: {copyEditors?: boolean, copyGuests?: boolean}) => {
	return chai.request(app.app)
		.post(`/playlists/${playlistid}?apiKey=${apiKey}`)
		.send(options);
};

export const searchPlaylist = (apiKey: string, keyword: string) => {
	return chai.request(app.app)
		.get(`/playlists/search?apiKey=${apiKey}&keyword=${keyword}`)
		.send();
};

// ====== EVENT =======

export const createEvent = (apiKey: string, options: {name: string, description?: string, visibility?: Visibility, votingSchedule?: Schedule, eventSchedule?: Schedule, coordinates?: Coordinates}) => {
	return chai.request(app.app)
		.post(`/events?apiKey=${apiKey}`)
		.send(options);
};

export const getEvent = (apiKey: string, eventid: string) => {
	return chai.request(app.app)
		.get(`/events/${eventid}?apiKey=${apiKey}`)
		.send();
};

export const getEvents = (apiKey: string) => {
	return chai.request(app.app)
		.get(`/events?apiKey=${apiKey}`)
		.send();
};

export const updateEvent = (apiKey: string, eventid: string, options: {name?: string, description?: string, visibility?: Visibility, votingSchedule?: Schedule, eventSchedule?: Schedule, coordinates?: Coordinates}) => {
	return chai.request(app.app)
		.put(`/events/${eventid}?apiKey=${apiKey}`)
		.send(options);
};

export const deleteEvent = (apiKey: string, eventid: string) => {
	return chai.request(app.app)
		.delete(`/events/${eventid}?apiKey=${apiKey}`)
		.send();
};

export const deleteEvents = (apiKey: string, eventids: string[]) => {
	return chai.request(app.app)
		.delete(`/events?apiKey=${apiKey}`)
		.send({eventids});
};

export const addGuestToEvent = (apiKey: string, eventid: string, userid: string) => {
	return chai.request(app.app)
		.put(`/events/${eventid}/guests/${userid}?apiKey=${apiKey}`)
		.send();
};

export const addGuestsToEvent = (apiKey: string, eventid: string, userids: string[]) => {
	return chai.request(app.app)
		.put(`/events/${eventid}/guests?apiKey=${apiKey}`)
		.send({userids});
};

export const removeGuestFromEvent = (apiKey: string, eventid: string, userid: string) => {
	return chai.request(app.app)
		.delete(`/events/${eventid}/guests/${userid}?apiKey=${apiKey}`)
		.send();
};

export const removeGuestsFromEvent = (apiKey: string, eventid: string, userids: string[]) => {
	return chai.request(app.app)
		.delete(`/events/${eventid}/guests?apiKey=${apiKey}`)
		.send({userids});
};

export const addDJToEvent = (apiKey: string, eventid: string, userid: string) => {
	return chai.request(app.app)
		.put(`/events/${eventid}/djs/${userid}?apiKey=${apiKey}`)
		.send();
};

export const addDJsToEvent = (apiKey: string, eventid: string, userids: string[]) => {
	return chai.request(app.app)
		.put(`/events/${eventid}/djs?apiKey=${apiKey}`)
		.send({userids});
};

export const removeDJFromEvent = (apiKey: string, eventid: string, userid: string) => {
	return chai.request(app.app)
		.delete(`/events/${eventid}/djs/${userid}?apiKey=${apiKey}`)
		.send();
};

export const removeDJsFromEvent = (apiKey: string, eventid: string, userids: string[]) => {
	return chai.request(app.app)
		.delete(`/events/${eventid}/djs?apiKey=${apiKey}`)
		.send({userids});
};

export const addPlaylistToEvent = (apiKey: string, eventid: string, playlistid: string, options?: {inviteGuests?: boolean, inviteEditors?: boolean, duplicatePlaylist?: boolean, copyEditors?: boolean, copyGuests?: boolean}) => {
	return chai.request(app.app)
		.put(`/events/${eventid}/playlists/${playlistid}?apiKey=${apiKey}`)
		.send(options);
};

export const addPlaylistsToEvent = (apiKey: string, eventid: string, playlistids: string[], options?: {inviteGuests?: boolean, inviteEditors?: boolean, duplicatePlaylist?: boolean, copyEditors?: boolean, copyGuests?: boolean}) => {
	return chai.request(app.app)
		.put(`/events/${eventid}/playlists?apiKey=${apiKey}`)
		.send({...options, playlistids});
};

export const removePlaylistFromEvent = (apiKey: string, eventid: string, playlistid: string) => {
	return chai.request(app.app)
		.delete(`/events/${eventid}/playlists/${playlistid}?apiKey=${apiKey}`)
		.send();
};

export const removePlaylistsFromEvent = (apiKey: string, eventid: string, playlistids: string[]) => {
	return chai.request(app.app)
		.delete(`/events/${eventid}/playlists?apiKey=${apiKey}`)
		.send({playlistids});
};

export const eventStartVoting = (apiKey: string, eventid: string) => {
	return chai.request(app.app)
		.put(`/events/${eventid}/startvoting?apiKey=${apiKey}`)
		.send();
};

export const eventEndVoting = (apiKey: string, eventid: string) => {
	return chai.request(app.app)
		.put(`/events/${eventid}/endvoting?apiKey=${apiKey}`)
		.send();
};

export const eventStartEvent = (apiKey: string, eventid: string) => {
	return chai.request(app.app)
		.put(`/events/${eventid}/startevent?apiKey=${apiKey}`)
		.send();
};

export const eventEndEvent = (apiKey: string, eventid: string) => {
	return chai.request(app.app)
		.put(`/events/${eventid}/endevent?apiKey=${apiKey}`)
		.send();
};

export const playerAction = (apiKey: string, eventid: string, playeraction: 'play'|'pause'|'seek', options?: {trackid?: string, timestamp?: string, trackPosition?: number}) => {
	return chai.request(app.app)
		.put(`/events/${eventid}/player/${playeraction}?apiKey=${apiKey}`)
		.send(options);
};

export const eventSocketConnect = (apiKey: string, eventid: string): SocketIOClient.Socket => {
	return socketio('http://localhost:3030/events', {autoConnect: false, query: {eventid, apiKey}, reconnection: false, forceNew: true});
};

export const voteEventTrack = (apiKey: string, eventid: string, trackid: string, vote: 'like'|'dislike'|'unlike'|'undislike') => {
	if (['like', 'dislike'].includes(vote)) {
		return chai.request(app.app)
			.put(`/events/${eventid}/${trackid}/${vote}?apiKey=${apiKey}`)
			.send();
	} else if (['unlike', 'undislike'].includes(vote)) {
		return chai.request(app.app)
			.delete(`/events/${eventid}/${trackid}/${vote.split('un')[1]}?apiKey=${apiKey}`)
			.send();
	}
};

export const moveEventTrack = (apiKey: string, eventid: string, trackid: string, index: number) => {
	return chai.request(app.app)
		.put(`/events/${eventid}/${trackid}/move?apiKey=${apiKey}`)
		.send({index});
};

export const searchEvent = (apiKey: string, keyword: string) => {
	return chai.request(app.app)
		.get(`/events/search?apiKey=${apiKey}&keyword=${keyword}`)
		.send();
};

export const facebook = async (token: string) => {
	return chai.request(app.app)
		.post(`/users/facebook?accessToken=${token}`)
		.send();
};