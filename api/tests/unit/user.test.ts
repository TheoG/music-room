import * as chai from 'chai';
import * as Jimp from 'jimp';
import * as path from 'path';
import chaiHttp = require('chai-http');
chai.use(chaiHttp);
const expect = chai.expect;
import * as fs from 'fs';
import * as util from 'util';
const unlink = util.promisify(fs.unlink);

import app from "../../src/app";
import ResponseStatusTypes from "../../src/utils/ResponseStatusTypes";
import { ErrorCodes, ServerError } from "../../src/utils/errorTypes";
import { reset, users, createUser, loginUser, updateUser, getFriends, deleteUser, requestFriendship, updateFriendship, updateSettings, getUser, getFriendship, getUserSettings, getAvatar, createPlaylist, addEditorToPlaylist, addGuestToPlaylist, createEvent, addGuestToEvent, addDJToEvent, facebook, searchUser, printret, userPasswordReset, userActivate } from './common';

import {User} from '../../src/classes/User';
import {Friendship} from '../../src/classes/Friendship';
import {Playlist} from '../../src/classes/Playlist';
import chalk from "chalk";
import { EnvVar } from '../../src/utils/env';

const search_limit: number = require('../../config.json').mongo_search_limit;

interface user {
	apiKey: string;
	id: string;
}

describe('User', function() {
	this.timeout(10000);
	let user1: user, user2: user, user3: user;
	let ret: any;
	describe('User Creation', function() {
		beforeEach(async function() {
			await reset()
		});

		it('Should create a user', async function() {
			ret = await createUser(users[0]);
			expect(ret.status).equal(ResponseStatusTypes.OK);
			expect(ret.body.id).exist;
			expect(ret.body.apiKey).exist;
			expect(ret.body.email).equals(users[0].email);
			user1 = {
				id: ret.body.id,
				apiKey: ret.body.apiKey,
			};
			ret = await getUser(user1.apiKey, user1.id);
			
			expect(ret.status).equal(ResponseStatusTypes.OK);
			expect(ret.body.id).equal(user1.id);
			expect(ret.body.email).equal(users[0].email);
			let outFolder = path.join(__dirname, '../../public/avatars/default-avatars');
			expect(ret.body.avatar).hasOwnProperty('avatar');
			// should return a random avatar if default avatars are available
			if (fs.readdirSync(outFolder).filter(filepath => fs.statSync(path.join(outFolder, filepath)).isFile()).length > 0) {
				ret = await getAvatar(ret.body.avatar);
				expect(ret.status).equals(ResponseStatusTypes.OK);
			}
		});
		it('Should create a user with optional parameters and ignore invalid ones', async function() {
			const displayName: string = 'jannyboi';
			const firstName: string = 'jan';
			const lastName: string = 'wally';
			const theme: string = 'dark';
			const invalid: string = 'lalal';

			ret = await chai.request(app.app)
				.post('/users')
				.send({...users[0], displayName, firstName, lastName, invalid, theme});
			user1 = {
				id: ret.body.id,
				apiKey: ret.body.apiKey,
			};
			ret = await getUser(user1.apiKey, user1.id);
			expect(ret.status).equal(ResponseStatusTypes.OK);
			expect(ret.body.id).equal(user1.id);
			expect(ret.body.email).equal(users[0].email);
			expect(ret.body.displayName).equal(displayName);
			expect(ret.body.firstName).equal(firstName);
			expect(ret.body.lastName).equal(lastName);
			expect(ret.body.theme).equal(theme);
			expect(ret.body.invalid).not.exist;
		});
		it('[mutex] Should create many users at the same time', async function() {
			let users: {email: string, password: string}[];
			users = 'a'.split('').map(a => {return {email: `${a}@${a}.com`, password: `${a}${a}${a}${a}${a}${a}`}})
			ret = await Promise.all(users.map(createUser));
			ret.map(r => expect(r.status).equals(ResponseStatusTypes.OK));
			ret = await Promise.all(users.map(loginUser));
			ret.map(r => expect(r.status).equals(ResponseStatusTypes.OK));
		});
		it('Should by default create a nonpremium user. A non premium user cannot create playlists or events', async function() {
			const oldValue = process.env[EnvVar.MUSIC_ROOM_USER_AUTO_PREMIUM];
			process.env[EnvVar.MUSIC_ROOM_USER_AUTO_PREMIUM] = "false";
			
			const user = {email: 'musicroom42noreply@gmail.com', password: 'janisawesome'};
			ret = await createUser(user);
			expect(ret.status).equal(ResponseStatusTypes.OK);
			expect(ret.body.isPremium).equals(false);

			ret = await Promise.all([
				createPlaylist(ret.body.apiKey, {name: 'test'}),
				createEvent(ret.body.apiKey, {name: 'test'}),
			]);
			ret.map(r => expect(r.status).equals(ResponseStatusTypes.UNAUTHORIZED));

			process.env[EnvVar.MUSIC_ROOM_USER_AUTO_PREMIUM] = oldValue;
		});
		describe('Should fail to create a user if ...', function() {
			it('no email is provided', async function() {
				ret = await chai.request(app.app)
					.post('/users')
					.send({password: 'janisawesome'});
				expect(ret.status).equal(ResponseStatusTypes.BAD_REQUEST);
			});
			it('no password is provided', async function() {
				ret = await chai.request(app.app)
					.post('/users')
					.send({email: 'jan2@jan.com'})
				expect(ret.status).equal(ResponseStatusTypes.BAD_REQUEST);
			});
			it('password is empty', async function() {
				ret = await chai.request(app.app)
					.post('/users')
					.send({email: 'jan2@jan.com', password: ''})
				expect(ret.status).equal(ResponseStatusTypes.BAD_REQUEST);
			});
			it('email is invalid', async function() {
				ret = await createUser({email: 'a@a', password: 'janisawesome'});
				expect(ret.status).equal(ResponseStatusTypes.BAD_REQUEST);
			});
			it('user already exists', async function() {
				const params = {email: 'jan@jan.com', password: 'janisawesome'};
				await createUser(params);
				ret = await createUser(params);
				expect(ret.status).equal(ResponseStatusTypes.CONFLICT);
				expect(ret.body.errorCode).equal(ErrorCodes.USER_ALREADY_EXISTS);
			});
			it('[mutex] user is created twice at same time', async function() {
				ret = await Promise.all([users[0], users[0]].map(createUser))
				const statuses = ret.map(r => r.status);
				expect(statuses[0]).to.not.equal(statuses[1]);
			});
		});
		
	});

	describe('User Password Reset', function() {
		const newPassword: string = '0123456789';
		const email: string = users[0].email;

		beforeEach(async function() {
			await reset();
			await createUser(users[0]);
		});

		it('Should set a password reset code on user, then update password', async function() {
			ret = await userPasswordReset(email);
			expect(ret.status).equals(ResponseStatusTypes.OK);
			ret = await User.get({email});
			expect(ret.passwordResetCode).exist;
			expect(ret.passwordResetAttemptsRemaining).equals(3);
			ret = await userPasswordReset(email, ret.passwordResetCode, newPassword);
			expect(ret.status).equals(ResponseStatusTypes.OK);
			ret = await loginUser({email, password: newPassword});
			expect(ret.status).equals(ResponseStatusTypes.OK);
			expect(ret.body.apiKey).exist;
		});
		it('Should fail to reset password if code is invalid and decrement attempts remaining', async function() {
			ret = await userPasswordReset(email);
			ret = await userPasswordReset(email, 'awesomecode', newPassword);
			expect(ret.status).equals(ResponseStatusTypes.UNAUTHORIZED);
			ret = await User.get({email});
			expect(ret.passwordResetCode).exist;
			expect(ret.passwordResetAttemptsRemaining).equals(2);
			ret = await userPasswordReset(email, 'awesomecode', newPassword);
			expect(ret.status).equals(ResponseStatusTypes.UNAUTHORIZED);
			ret = await User.get({email});
			expect(ret.passwordResetCode).exist;
			expect(ret.passwordResetAttemptsRemaining).equals(1);
			ret = await userPasswordReset(email, 'awesomecode', newPassword);
			expect(ret.status).equals(ResponseStatusTypes.UNAUTHORIZED);
			ret = await User.get({email});
			expect(ret.passwordResetCode).exist;
			expect(ret.passwordResetAttemptsRemaining).equals(0);
			ret = await userPasswordReset(email, 'awesomecode', newPassword);
			expect(ret.status).equals(ResponseStatusTypes.UNAUTHORIZED);
			ret = await User.get({email});
			expect(ret.passwordResetCode).exist;
			expect(ret.passwordResetAttemptsRemaining).equals(0);
		});
		it('Should remove password reset info on succesful login', async function() {
			ret = await userPasswordReset(email);
			ret = await User.get({email});
			expect(ret.passwordResetCode).exist;
			expect(ret.passwordResetAttemptsRemaining).equals(3);
			ret = await loginUser({email, password: users[0].password})
			expect(ret.status).equals(ResponseStatusTypes.OK);
			ret = await User.get({email});
			expect(ret.passwordResetCode).not.exist;
			expect(ret.passwordResetAttemptsRemaining).not.exist;
		});
	});

	describe('User Activate', function() {
		let userid: string;
		let oldValue: string;

		before(function() {
			let oldValue = process.env[EnvVar.MUSIC_ROOM_USER_AUTO_ACTIVATE];
			process.env[EnvVar.MUSIC_ROOM_USER_AUTO_ACTIVATE] = 'false';
		});

		after(function() {
			process.env[EnvVar.MUSIC_ROOM_USER_AUTO_ACTIVATE] = oldValue;
		});

		beforeEach(async function() {
			await reset();
			ret = await createUser(users[0]);
			userid = ret.body.id;
		});

		it('Should activate the account', async function() {
			let user: User = await User.get({email: users[0].email});
			ret = await userActivate(userid, user.activationCode);
			expect(ret.status).equals(ResponseStatusTypes.OK);
			expect(ret.body.apiKey).exist;
			ret = await loginUser(users[0]);
			expect(ret.status).equals(ResponseStatusTypes.OK);
		});

		it('Should send the activation code again', async function() {
			let user: User = await User.get({email: users[0].email});
			ret = await userActivate(userid);			
			expect(ret.status).equals(ResponseStatusTypes.OK);
			user = await User.get({email: users[0].email});
			expect(user.activationCode).exist;
		});

		it('Should change email upon new activation code request', async function() {
			let newEmail: string = 'x@x.com';
			ret = await userActivate(userid, undefined, newEmail);
			expect(ret.status).equals(ResponseStatusTypes.OK);
			let user: User = await User.get({email: newEmail});
			expect(user.email).equals(newEmail);
		});
	});

	describe('User Login', function() {
		before(async function() {
			await reset();
			await createUser(users[0]);
		});

		it('Should login user and return id and apiKey', async function() {
			ret = await loginUser(users[0])
			expect(ret.body.apiKey).exist;
			expect(ret.body.id).exist;
		});
		describe('Should not login (not return apiKey) if ...', function() {
			it('email is not present', async function() {
				ret = await chai.request(app.app)
					.post('/login')
					.send({password: 'aaaaaa'})
				expect(ret.body.apiKey).not.exist;
				expect(ret.body.id).not.exist;
			});
			it('password is not present', async function() {
				ret = await chai.request(app.app)
					.post('/login')
					.send({email: 'a@a.com'})
				expect(ret.body.apiKey).not.exist;
				expect(ret.body.id).not.exist;
			});
			it('password is invalid', async function() {
				ret = await loginUser({email: 'a@a.com', password: 'myawesomepassword'})
				expect(ret.body.apiKey).not.exist;
				expect(ret.body.id).not.exist;
			});
			it('user does not exit', async function() {
				ret = await loginUser({email: 'fake@email.com', password: 'myawesomepassword'});
				expect(ret.body.errorMessage).equals('User does not exist');
				expect(ret.body.apiKey).not.exist;
				expect(ret.body.id).not.exist;
			});
		});
	});

	describe('Get User', function() {
		before(async function() {
			await reset();
			ret = await Promise.all(users.map(createUser));
			[user1, user2, user3] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});	
		});

		it('Should return a user (self)', async function() {
			ret = await getUser(user1.apiKey, user1.id);
			expect(ret.status).equal(ResponseStatusTypes.OK);
			expect(ret.body.id).exist;
			expect(ret.body.apiKey).exist;
			expect(ret.body.email).exist;
			expect(ret.body.password).not.exist;
			expect(ret.body).haveOwnProperty('displayName');
			expect(ret.body).haveOwnProperty('firstName');
			expect(ret.body).haveOwnProperty('lastName');
			expect(ret.body).not.haveOwnProperty('settings');
			expect(ret.body).haveOwnProperty('friendships');
			expect(ret.body).haveOwnProperty('friends');
			expect(ret.body).haveOwnProperty('playlists');
			expect(ret.body).haveOwnProperty('guestInPlaylists');
			expect(ret.body).haveOwnProperty('editorInPlaylists');
			expect(ret.body).haveOwnProperty('avatar');
		});
		it('Should return a user (other)', async function() {
			ret = await getUser(user1.apiKey, user2.id);
			expect(ret.status).equal(ResponseStatusTypes.OK);
			expect(ret.body.id).exist;
			expect(ret.body.apiKey).not.exist;
		});
		describe('Should fail if ...', function() {
			it('apiKey is invalid', async function() {
				ret = await getUser('123456789', user1.id);
				expect(ret.status).equals(ResponseStatusTypes.UNAUTHORIZED);		
			});
			it('id is invalid', async function() {
				ret = await getUser(user1.apiKey, '0123456789');
				expect(ret.status).equals(ResponseStatusTypes.NOT_FOUND);		
			});
			it('no id is given', async function() {
				ret = await chai.request(app.app)
					.get(`/users/${user2.id}`);
				expect(ret.status).equals(ResponseStatusTypes.BAD_REQUEST);
			})
			it('no apiKey is given', async function() {
				ret = await chai.request(app.app)
					.get(`/users/${user1.id}`);
				expect(ret.status).equals(ResponseStatusTypes.BAD_REQUEST);		
			});
		});
	});

	describe('Update User', function() {
		const newName: string = 'new name';
		const newTheme: string = 'light';

		beforeEach(async function() {
			await reset();
			ret = await Promise.all(users.map(createUser));
			[user1, user2, user3] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});	
		});

		it('Should change displayName and theme and musical preferences', async function() {
			ret = await updateUser(user1.apiKey, user1.id, {displayName: newName, theme: newTheme, musicalPreferences: 'pop'});
			expect(ret.status).equal(ResponseStatusTypes.OK);
			ret = await getUser(user1.apiKey, user1.id);
			expect(ret.body.displayName).equals(newName);
			expect(ret.body.theme).equals(newTheme);
			expect(ret.body.musicalPreferences).equals('pop');
		});
		it('[mutex] Should change displayName and firstName in two different requests', async function() {
			await Promise.all([
				updateUser(user1.apiKey, user1.id, {displayName: newName}),
				updateUser(user1.apiKey, user1.id, {firstName: newName})
			]);
			ret = await getUser(user1.apiKey, user1.id);
			expect(ret.body.displayName).equals(newName);
			expect(ret.body.firstName).equals(newName);
		});
		it('Should do nothing if no valid attributes are given', async function() {
			const newName: string = 'new name';

			ret = await updateUser(user1.apiKey, user1.id, {bad_attribute: newName});
			expect(ret.status).equal(ResponseStatusTypes.OK);
			ret = await getUser(user1.apiKey, user1.id);
			expect(ret.body).not.haveOwnProperty('bad_attribute');
		});
		it('Should change apiKey if password is updated', async function() {
			const newPassword: string = 'toodles';
			ret = await updateUser(user1.apiKey, user1.id, {password: newPassword});
			expect(ret.status).equal(ResponseStatusTypes.OK);
			ret = await User.get({id: user1.id});
			expect(ret.apiKey).not.equal(user1.apiKey);
		});
		describe('Should fail to update user if ...', function() {
			const newName: string = 'new name';

			it('no apiKey is given', async function() {
				ret = await chai.request(app.app)
					.put(`/users/${user1.id}`)
					.send({displayName: newName});
				expect(ret.status).equal(ResponseStatusTypes.BAD_REQUEST);
			});
			it('apiKey is invalid', async function() {
				ret = await updateUser('01234569789', user1.id, {displayName: newName});
				expect(ret.status).equal(ResponseStatusTypes.UNAUTHORIZED);
			});
			it('attribute is not of correct type (object)', async function() {
				ret = await updateUser(user1.apiKey, user1.id, {displayName: {object: 'this is object'}});
				expect(ret.status).equal(ResponseStatusTypes.BAD_REQUEST);
			});
			it('attribute is not of correct type (array)', async function() {
				ret = await updateUser(user1.apiKey, user1.id, {displayName: ['things', 'thingsismore', 'evenmorethingses']});
				expect(ret.status).equal(ResponseStatusTypes.BAD_REQUEST);
			});
			it('email is prodivded: does not update email', async function() {
				const newEmail: string = 'jan@jan.com';
				ret = await updateUser(user1.apiKey, user1.id, {email: newEmail});
				expect(ret.status).equal(ResponseStatusTypes.OK);
				ret = await getUser(user1.apiKey, user1.id);
				expect(ret.email).not.equals(newEmail);
			});
		});

		describe('Update avatar', function() {
			const MIN_IMG_DIM: number = 200;
			const MAX_IMG_DIM: number = 500;
			const folderPath: string = path.join(__dirname, './files/');

			const files: string[] = ['test0_200x200', 'test1_400x200', 'test2_200x300', 'test3_200x600', 'test4_700x300', 'test5_700x700', 'test6_900x700', 'test7_600x700'].map(file => `${folderPath}${file}.jpg`);
			
			const errFiles: string[] = ['testerr0_150x150', 'testerr1_150x300', 'testerr2_400x175'].map(file => `${folderPath}${file}.jpg`);

			const userids: string[] = [];

			beforeEach(async function() {
				await reset();
				ret = await Promise.all(users.map(createUser));
				[user1] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});
				userids.push(user1.id);
			});

			it(`Should upload an image and not resize ${files[0]}`, async function() {
				ret = await updateUser(user1.apiKey, user1.id, {}, files[0]);
				expect(ret.status).equals(ResponseStatusTypes.OK);
				// get user, check that avatar path exists
				ret = await getUser(user1.apiKey, user1.id);
				expect(ret.body.avatar).not.empty;
				// get img, check is not null and img size OK
				ret = await getAvatar(ret.body.avatar);
				expect(ret.status).equals(ResponseStatusTypes.OK);
				let img: Jimp = await Jimp.read(ret.body);
				expect(img.getHeight()).lte(MAX_IMG_DIM).gte(MIN_IMG_DIM).equals(img.getWidth());
				expect(img.getWidth()).lte(MAX_IMG_DIM).gte(MIN_IMG_DIM);
			});
			it(`Should upload an image and resize ${files[1]}`, async function() {
				ret = await updateUser(user1.apiKey, user1.id, {}, files[1]);
				expect(ret.status).equals(ResponseStatusTypes.OK);
				ret = await getUser(user1.apiKey, user1.id);
				expect(ret.body.avatar).not.empty;
				ret = await getAvatar(ret.body.avatar);
				expect(ret.status).equals(ResponseStatusTypes.OK);
				let img: Jimp = await Jimp.read(ret.body);
				expect(img.getHeight()).lte(MAX_IMG_DIM).gte(MIN_IMG_DIM).equals(img.getWidth());
				expect(img.getWidth()).lte(MAX_IMG_DIM).gte(MIN_IMG_DIM);
			});
			it(`Should upload an image and resize ${files[2]}`, async function() {
				ret = await updateUser(user1.apiKey, user1.id, {}, files[2]);
				expect(ret.status).equals(ResponseStatusTypes.OK);
				ret = await getUser(user1.apiKey, user1.id);
				expect(ret.body.avatar).not.empty;
				ret = await getAvatar(ret.body.avatar);
				expect(ret.status).equals(ResponseStatusTypes.OK);
				let img: Jimp = await Jimp.read(ret.body);
				expect(img.getHeight()).lte(MAX_IMG_DIM).gte(MIN_IMG_DIM).equals(img.getWidth());
				expect(img.getWidth()).lte(MAX_IMG_DIM).gte(MIN_IMG_DIM);
			});
			it(`Should upload an image and resize ${files[3]}`, async function() {
				ret = await updateUser(user1.apiKey, user1.id, {}, files[3]);
				expect(ret.status).equals(ResponseStatusTypes.OK);
				ret = await getUser(user1.apiKey, user1.id);
				expect(ret.body.avatar).not.empty;
				ret = await getAvatar(ret.body.avatar);
				expect(ret.status).equals(ResponseStatusTypes.OK);
				let img: Jimp = await Jimp.read(ret.body);
				expect(img.getHeight()).lte(MAX_IMG_DIM).gte(MIN_IMG_DIM).equals(img.getWidth());
				expect(img.getWidth()).lte(MAX_IMG_DIM).gte(MIN_IMG_DIM);
			});
			it(`Should upload an image and resize ${files[4]}`, async function() {
				ret = await updateUser(user1.apiKey, user1.id, {}, files[4]);
				expect(ret.status).equals(ResponseStatusTypes.OK);
				ret = await getUser(user1.apiKey, user1.id);
				expect(ret.body.avatar).not.empty;
				ret = await getAvatar(ret.body.avatar);
				expect(ret.status).equals(ResponseStatusTypes.OK);
				let img: Jimp = await Jimp.read(ret.body);
				expect(img.getHeight()).lte(MAX_IMG_DIM).gte(MIN_IMG_DIM).equals(img.getWidth());
				expect(img.getWidth()).lte(MAX_IMG_DIM).gte(MIN_IMG_DIM);
			});
			it(`Should upload an image and resize ${files[5]}`, async function() {
				ret = await updateUser(user1.apiKey, user1.id, {}, files[5]);
				expect(ret.status).equals(ResponseStatusTypes.OK);
				ret = await getUser(user1.apiKey, user1.id);
				expect(ret.body.avatar).not.empty;
				ret = await getAvatar(ret.body.avatar);
				expect(ret.status).equals(ResponseStatusTypes.OK);
				let img: Jimp = await Jimp.read(ret.body);
				expect(img.getHeight()).lte(MAX_IMG_DIM).gte(MIN_IMG_DIM).equals(img.getWidth());
				expect(img.getWidth()).lte(MAX_IMG_DIM).gte(MIN_IMG_DIM);
			});
			it(`Should upload an image and resize ${files[6]}`, async function() {
				ret = await updateUser(user1.apiKey, user1.id, {}, files[6]);
				expect(ret.status).equals(ResponseStatusTypes.OK);
				ret = await getUser(user1.apiKey, user1.id);
				expect(ret.body.avatar).not.empty;
				ret = await getAvatar(ret.body.avatar);
				expect(ret.status).equals(ResponseStatusTypes.OK);
				let img: Jimp = await Jimp.read(ret.body);
				expect(img.getHeight()).lte(MAX_IMG_DIM).gte(MIN_IMG_DIM).equals(img.getWidth());
				expect(img.getWidth()).lte(MAX_IMG_DIM).gte(MIN_IMG_DIM);
			});
			it(`Should upload an image and resize ${files[7]} and update username`, async function() {
				let newName: string = 'newname';
				ret = await chai.request(app.app)
					.put(`/users/${user1.id}?apiKey=${user1.apiKey}`)
					.attach('avatar', fs.readFileSync(files[7]), files[7])
					.field('firstName', newName);
				expect(ret.status).equals(ResponseStatusTypes.OK);
				ret = await getUser(user1.apiKey, user1.id);
				expect(ret.body.avatar).not.empty;
				expect(ret.body.firstName).equals(newName);
				ret = await getAvatar(ret.body.avatar);
				expect(ret.status).equals(ResponseStatusTypes.OK);
				let img: Jimp = await Jimp.read(ret.body);
				expect(img.getHeight()).lte(MAX_IMG_DIM).gte(MIN_IMG_DIM).equals(img.getWidth());
				expect(img.getWidth()).lte(MAX_IMG_DIM).gte(MIN_IMG_DIM);
			});
			describe('Should fail if ...', function() {
				it(`image is too small ${errFiles[0]}`, async function() {
					ret = await updateUser(user1.apiKey, user1.id, {}, errFiles[0]);
					expect(ret.status).equals(ResponseStatusTypes.BAD_REQUEST);
					ret = await getUser(user1.apiKey, user1.id);
					expect(ret.body.avatar).equals(null);
					ret = await getAvatar('public/avatar/' + user1.id + '.png');
					expect(ret.status).equals(ResponseStatusTypes.NOT_FOUND);
				});
				it(`image is too small ${errFiles[1]}`, async function() {
					ret = await updateUser(user1.apiKey, user1.id, {}, errFiles[1]);
					expect(ret.status).equals(ResponseStatusTypes.BAD_REQUEST);
					ret = await getUser(user1.apiKey, user1.id);
					expect(ret.body.avatar).equals(null);
					ret = await getAvatar('public/avatar/' + user1.id + '.png');
					expect(ret.status).equals(ResponseStatusTypes.NOT_FOUND);
				});
				it(`image is too small ${errFiles[2]}`, async function() {
					ret = await updateUser(user1.apiKey, user1.id, {}, errFiles[2]);
					expect(ret.status).equals(ResponseStatusTypes.BAD_REQUEST);
					ret = await getUser(user1.apiKey, user1.id);
					expect(ret.body.avatar).equals(null);
					ret = await getAvatar('public/avatar/' + user1.id + '.png');
					expect(ret.status).equals(ResponseStatusTypes.NOT_FOUND);
				});
			});

			// delete created avatar pictures after tests
			after(async function() {
				await Promise.all(userids.map(async (id: string) => {
					try { await unlink(`./public/avatars/${id}.png`) } catch {}
				}));
			});
		});
	});

	describe('Delete User', function() {

		describe('When a user is deleted ...', function() {
			let friendshipId1_2: string;
			let friendshipId1_3: string;

			let playlistId1: string; // user1 is owner
			let playlistId2: string; // user1 is editor
			let playlistId3: string; // user1 is guest
			
			let eventId1: string; // user1 is owner
			let eventId2: string; // user1 is guest
			let eventId3: string; // user1 is dj

			let avatar: string;

			before(async function() {
				await reset();
				ret = await Promise.all(users.map(createUser));
				[user1, user2, user3] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});	

				await Promise.all([
					// === Friendships === //
						// create friendships between users 1-2, and 1-3 
						(async () => {
							ret = await requestFriendship(user1.apiKey, user2.id);
							// console.log('req frendship1', {status: ret.status, body: ret.body});
							friendshipId1_2 = ret.body.id;
							ret = await updateFriendship(user2.apiKey, friendshipId1_2, 'accept');
							// console.log('update frendship1', {status: ret.status, body: ret.body});
						})(),
						(async () => {
							ret = await requestFriendship(user1.apiKey, user3.id);
							// console.log('req frendship2', {status: ret.status, body: ret.body});
							friendshipId1_3 = ret.body.id;
							ret = await updateFriendship(user3.apiKey, friendshipId1_3, 'accept');
							// console.log('update frendship2', {status: ret.status, body: ret.body});
						})(),
					// === Playists === //
						// user1 create playlist, add user2 as guest, add user3 as editor
						(async () => {
							ret = await createPlaylist(user1.apiKey, {name: 'myplaylist'});
							playlistId1 = ret.body.id;
							await Promise.all([
								addGuestToPlaylist(user1.apiKey, playlistId1, user2.id),
								addEditorToPlaylist(user1.apiKey, playlistId1, user3.id),
							]);
						})(),
						// user2 create playlist, add user1 as editor
						(async () => {
							ret = await createPlaylist(user2.apiKey, {name: 'myplaylist'});
							playlistId2 = ret.body.id;
							await addEditorToPlaylist(user2.apiKey, playlistId2, user1.id);
						})(),
						// user3 create playlist, add user1 as guest
						(async () => {
							ret = await createPlaylist(user3.apiKey, {name: 'myplaylist'});
							playlistId3 = ret.body.id;
							await addGuestToPlaylist(user3.apiKey, playlistId3, user1.id);
						})(),
					// === Events === //
					// user1 create event
						(async () => {
							ret = await createEvent(user1.apiKey, {name: 'myevent'});
							eventId1 = ret.body.id;
						})(),
						// user2 create event, add user1 as guest
						(async () => {
							ret = await createEvent(user2.apiKey, {name: 'myevent'});
							eventId2 = ret.body.id;
							await addGuestToEvent(user2.apiKey, eventId2, user1.id);
						})(),
						// user3 create event, add user1 as dj
						(async () => {
							ret = await createEvent(user3.apiKey, {name: 'myevent'});
							eventId3 = ret.body.id;
							await addDJToEvent(user3.apiKey, eventId3, user1.id);
						})(),
					// user1 add avatar
					(async () => {
						ret = await updateUser(user1.apiKey, user1.id, {}, path.join(__dirname, './files/test0_200x200.jpg'));
						avatar = ret.body.avatar;
					})(),
				]);
				ret = await getUser(user1.apiKey, user1.id);
				ret = await deleteUser(user1.apiKey, user1.id);
				expect(ret.status).equal(ResponseStatusTypes.OK);
			});
			it('user should no longer exist', async function() {
				ret = await User.get({id: user1.id});
				expect(ret).equals(undefined);
				ret = await getUser(user1.apiKey, user1.id);
				expect(ret.status).equal(ResponseStatusTypes.UNAUTHORIZED);
			});
			it('should destroy user\'s friendships', async function() {
				ret = await Friendship.get({id: friendshipId1_2});
				expect(ret).equals(undefined);
				ret = await Friendship.get({id: friendshipId1_3});
				expect(ret).equals(undefined);
				ret = await Promise.all([
					getFriendship(friendshipId1_2, user2.apiKey),
					getFriendship(friendshipId1_3, user3.apiKey)
				]);
				expect(ret[0].status).equal(ResponseStatusTypes.NOT_FOUND);
				expect(ret[1].status).equal(ResponseStatusTypes.NOT_FOUND);
			});
			it('should remove user from friend lists', async function() {
				ret = await Promise.all([
					getUser(user2.apiKey, user2.id),
					getUser(user3.apiKey, user3.id),
				]);
				expect(ret[0].body.friends).not.include(user1.id);
				expect(ret[1].body.friends).not.include(user1.id);
			});
			it('user playlists should not exist', async function() {
				ret = await Playlist.get({id: playlistId1});
				expect(ret).equals(undefined);
				await Promise.all([
					(async () => {
						ret = await getUser(user2.apiKey, user2.id);
						expect(ret.body.guestInPlaylists).not.include(playlistId1);
					}),
					(async () => {
						ret = await getUser(user3.apiKey, user3.id);
						expect(ret.body.editorInPlaylists).not.include(playlistId1);
					})
				]);
			});
			// it('user should not be guest in other playlists');
			// it('user should not be editor in other playlists');
			// it('user events should not exist');
			// it('user should not be guest in other events');
			// it('user should not be dj in other events');

			it('user avatar should be deleted', async function() {
				ret = await getAvatar(avatar);
				expect(ret.status).equals(ResponseStatusTypes.NOT_FOUND);
			});
		});
			
		describe('Should fail if ...', function() {
			before(async function() {
				await reset()
				ret = await Promise.all(users.map(createUser));
				[user1, user2, user3] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});	
			});

			it('apiKey not given', async function() {
				ret = await chai.request(app.app)
					.delete(`/users/${user2.id}`)
					.send({});
				expect(ret.status).equal(ResponseStatusTypes.BAD_REQUEST);
			});
			it('apiKey is invalid', async function() {
				ret = await deleteUser('0123456789', user2.id);
				expect(ret.status).equal(ResponseStatusTypes.UNAUTHORIZED);
			});
			it('id is invalid', async function() {
				ret = await deleteUser(user2.apiKey, '0123456789');
				expect(ret.status).equal(ResponseStatusTypes.UNAUTHORIZED);
			});
			it('apiKey and id are not of the same user', async function() {
				ret = await deleteUser(user2.apiKey, user3.id);
				expect(ret.status).equal(ResponseStatusTypes.UNAUTHORIZED);
			});
		});
	});

	describe('User Settings', function() {
		async function createUsers() {
			await reset();
			let ret: any = await Promise.all(users.map(createUser));
			[user1, user2, user3] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});			
		}

		describe('Get Settings', function() {
			before(async function () {
				await createUsers();
			});
			it('Should return user settings', async function() {
				ret = await getUserSettings(user1.apiKey, user1.id);
				expect(ret.status).equal(ResponseStatusTypes.OK);
				expect(ret.body.email).exist;
			});
			describe('Should fail if ...', function() {
				it('apiKey not given', async function() {
					ret = await chai.request(app.app)
						.get(`/users/${user1.id}/settings`)
						.send()
					expect(ret.status).equals(ResponseStatusTypes.BAD_REQUEST);
				});
				it('apiKey invalid', async function() {
					ret = await getUserSettings('0123456789', user1.id);
					expect(ret.status).equals(ResponseStatusTypes.UNAUTHORIZED);
				});
			})
		});

		describe('Update Settings', function() {
			beforeEach(async function() {
				await createUsers();
			});
			it('Should update user settings', async function() {
				const newSetting: string = 'PRIVATE';
				ret = await updateSettings(user1.apiKey, user1.id, {email: newSetting});
				expect(ret.status).equals(ResponseStatusTypes.OK);
				ret = await getUserSettings(user1.apiKey, user1.id);
				expect(ret.status).equals(ResponseStatusTypes.OK);
				expect(ret.body.email).equal(newSetting);
			});

			describe('Should fail if ...', function() {
				it('apiKey is invalid', async function() {
					const newSetting: string = 'PRIVATE';
					ret = await updateSettings(user1.id, '0123456789', {email: newSetting})
					expect(ret.status).equals(ResponseStatusTypes.UNAUTHORIZED);
				});
				it('no apiKey is provided', async function() {
					ret = await chai.request(app.app)
						.put(`/users/${user1.id}/settings`)
						.send({email: 'PRIVATE'});
					expect(ret.status).equal(ResponseStatusTypes.BAD_REQUEST);
				});
				it('Should do nothing if no valid settings are provided', async function() {
					ret = await chai.request(app.app)
						.put(`/users/${user1.id}/settings?apiKey=${user1.apiKey}`)
						.send({invalidSetting: 'PUBLIC'});
					expect(ret.status).equal(ResponseStatusTypes.OK);
				});
				it('there is an invalid value for a settings', async function() {
					ret = await chai.request(app.app)
						.put(`/users/${user1.id}/settings?apiKey=${user1.apiKey}`)
						.send({email: 'INVALID_VALUE'});
					expect(ret.status).equal(ResponseStatusTypes.BAD_REQUEST);
				});
				it('id and apiKey do not belong to same user', async function() {
					const newSetting: string = 'PRIVATE';
					ret = await updateSettings(user1.apiKey, user2.id, {email: newSetting});
					expect(ret.status).equals(ResponseStatusTypes.UNAUTHORIZED);
				});
			});
		});

		describe('Get User based on Privacy Settings', function() {
			before(async function() {
				await createUsers();
				// set user1 and user2 as friends
				ret = await requestFriendship(user1.apiKey, user2.id);
				ret = await updateFriendship(user2.apiKey, ret.body.id, 'accept');
			});
			describe('Leave user1 email to PUBLIC', function() {
				it('user1 should see their email', async function() {
					ret = await getUser(user1.apiKey, user1.id);
					expect(ret.body.email).exist;
				});
				it('user2 should see user1\'s email', async function() {
					ret = await getUser(user2.apiKey, user1.id);
					expect(ret.body.email).exist;
				});
				it('user3 should see user1\'s email', async function() {
					ret = await getUser(user3.apiKey, user1.id);
					expect(ret.body.email).exist;
				});
			});
			describe('Setting user1 email to FRIENDS', function() {
				// update user1's email to FRIENDS
				before(async function() {
					await updateSettings(user1.apiKey, user1.id, {email: 'FRIENDS'});
				});
				it('user1 should see their email', async function() {
					ret = await getUser(user1.apiKey, user1.id);
					expect(ret.body.email).exist;
				});
				it('user2 should see user1\'s email', async function() {
					ret = await getUser(user2.apiKey, user1.id);
					expect(ret.body.email).exist;
				});
				it('user3 should not see user1\'s email', async function() {
					ret = await getUser(user3.apiKey, user1.id);
					expect(ret.body.email).not.exist;
				});
			});
			describe('Setting user1 email to PRIVATE', function() {
				// update user1's email to PRIVATE
				before(async function() {
					await updateSettings(user1.apiKey, user1.id, {email: 'PRIVATE'});
				});
				it('user1 should see their email', async function() {
					ret = await getUser(user1.apiKey, user1.id)
					expect(ret.body.email).exist;
				});
				it('user2 should not see user1\'s email', async function() {
					ret = await getUser(user2.apiKey, user1.apiKey)
					expect(ret.body.email).not.exist;
				});
				it('user3 should not see user1\'s email', async function() {
					ret = await getUser(user2.apiKey, user1.id)
					expect(ret.body.email).not.exist;
				});
			})
		});
	});

	describe('User Friends', function() {
		before(async function() {
			await reset();
			ret = await Promise.all(users.map(createUser));
			[user1, user2, user3] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});	
			ret = await requestFriendship(user1.apiKey, user2.id);
			ret = await updateFriendship(user2.apiKey, ret.body.id, 'accept');
			ret = await requestFriendship(user1.apiKey, user3.id);
			ret = await updateFriendship(user3.apiKey, ret.body.id, 'accept');
		});

		describe('Get Friends / Get Friend', function() {
			it('Should return all friends of a user', async function() {
				ret = await getFriends(user1.apiKey, user1.id);
				expect(ret.body.friends.map(x => x.id)).include.members([user2.id, user3.id])
			});
			it('Should return a friend of a user', async function() {
				ret = await getFriends(user1.apiKey, user1.id, user2.id)
				expect(ret.body.id).equal(user2.id);
			});
			describe('Should fail to return friends if ... ', function() {
				it('apiKey is invalid', async function() {
					ret = await getFriends('0123456789', user1.id)
					expect(ret.status).equal(ResponseStatusTypes.UNAUTHORIZED);
				});
				it('id given is not friend of user', async function() {
					ret = await getFriends(user2.apiKey, user2.id, user3.id);
					expect(ret.status).equal(ResponseStatusTypes.BAD_REQUEST);
				});
			});
		});
	});

	describe('User Search', function() {
		let name: string = 'supercomplicatedname';
		let users: any[] = [{
			email: 'a@a.com',
			password: 'aaaaaa',
			displayName: 'The Buerk',
			firstName: 'Theo', 
			lastName: 'Gros'
		},
		{
			email: 'b@b.com',
			password: 'bbbbbb',
			displayName: 'CaptainBB',
			firstName: 'Jan', 
			lastName: 'Walsh'
		},
		{
			email: 'c@c.com',
			password: 'cccccc',
			displayName: 'Vengel',
			firstName: 'Maxime', 
			lastName: 'Verdier'
		}];
		before(async function() {
			await reset();
			ret = await Promise.all(users.map(createUser));
			[user1, user2, user3] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});
			await Promise.all('defghijklmnopqrstuvwxyzABCDEFGH'.split('').map(x => createUser({email: `${x}@${x}.com`, password: '000000', firstName: name})));
		});

		it('Should find user by ID', async function() {
			ret = await searchUser(user1.apiKey, user2.id);
			expect(ret.body.results[0].id).equals(user2.id);
		});
		it('Should find user by email', async function() {
			ret = await searchUser(user1.apiKey, 'b@b.com');
			expect(ret.body.results[0].id).equals(user2.id);
		});
		it('Should find user by displayName', async function() {
			ret = await searchUser(user1.apiKey, 'Vengel');
			expect(ret.body.results[0].id).equals(user3.id);
		});
		it('Should find user by firstName', async function() {
			ret = await searchUser(user2.apiKey, 'Theo');
			expect(ret.body.results[0].id).equals(user1.id);
		});
		it('Should find user by lastName', async function() {
			ret = await searchUser(user2.apiKey, 'Gros');
			expect(ret.body.results[0].id).equals(user1.id);
		});
		it('Should not include client in results', async function() {
			ret = await searchUser(user1.apiKey, 'Gros');			
			expect(ret.body.results).length(0);
		});
		it(`Should limit results to limit set in config: ${search_limit}`, async function() {
			ret = await searchUser(user1.apiKey, name);
			expect(ret.body.results.length).lte(search_limit);
		});
	});


	// emails: activate, pwreset, login
	describe.skip('Emails', function() {
		let user = {email: 'musicroom42noreply@gmail.com', password: 'janisawesome'};
		before(function() {
			process.env[EnvVar.MUSIC_ROOM_SEND_EMAILS] = 'true';
			process.env[EnvVar.MUSIC_ROOM_USER_AUTO_ACTIVATE] = 'true';
		});
		after(function() {
			process.env[EnvVar.MUSIC_ROOM_SEND_EMAILS] = 'false';
			process.env[EnvVar.MUSIC_ROOM_USER_AUTO_ACTIVATE] = 'false';
			console.log(chalk.greenBright('\t => Check Musique\'s email!'));
		});
		beforeEach(async function() {
			await reset();
		});

		it('Should send an email with activation code', async function() {
			process.env[EnvVar.MUSIC_ROOM_USER_AUTO_ACTIVATE] = 'false';
			ret = await createUser(user);
			expect(ret.status).equal(ResponseStatusTypes.OK);
			process.env[EnvVar.MUSIC_ROOM_USER_AUTO_ACTIVATE] = 'true';
		});
		it('Should send an email on login', async function() {
			ret = await createUser(user);
			expect(ret.status).equal(ResponseStatusTypes.OK);
			ret = await loginUser(user);
			expect(ret.status).equal(ResponseStatusTypes.OK);
		});
		it('Should send an email for password reset', async function() {
			ret = await createUser(user);
			expect(ret.status).equal(ResponseStatusTypes.OK);
			ret = await userPasswordReset(user.email);
			expect(ret.status).equals(ResponseStatusTypes.OK);
		});

		it('Should not block on batch email');
	});	

	describe.skip('Facebook', function() {
		const key: string = '';
		before(function() {
			if (key == '') console.log(chalk.red('\t => Set up facebook key in test'))
		});
		beforeEach(async function() {
			await reset();
		});
		
		it('Should create an account with facebook', async function() {
			ret = await facebook(key);
			expect(ret.status).equals(ResponseStatusTypes.OK);
			expect(ret.body.id).exist;
			expect(ret.body.email).exist;
			expect(ret.body.apiKey).exist;
			ret = await getUser(ret.body.apiKey, ret.body.id);
			expect(ret.status).equals(ResponseStatusTypes.OK);
		});
		it('Should link existing account to facebook', async function() {
			ret = await createUser({email: 'janmwalsh@ymail.com', password: 'password'});
			expect(ret.status).equals(ResponseStatusTypes.OK);
			ret = await facebook(key);
			expect(ret.status).equals(ResponseStatusTypes.OK);
			expect(ret.body.id).exist;
			expect(ret.body.apiKey).exist;
			let user: User = await User.get({id: ret.body.id});
			expect(user).exist;
			expect(user.facebookid).exist;
		});
		it('Should login with facebook', async function() {
			ret = await facebook(key);
			expect(ret.status).equals(ResponseStatusTypes.OK);
			ret = await facebook(key);			
			expect(ret.status).equals(ResponseStatusTypes.OK);
			expect(ret.body.apiKey).exist;
			expect(ret.body.id).exist;
		});
	});
});
