import * as chai from 'chai';
import chaiHttp = require('chai-http');
chai.use(chaiHttp);
const expect = chai.expect;

import { reset, createUser, users, createPlaylist, deletePlaylist, getUser, getPlaylist, addTrack, playerAction, createEvent, addPlaylistToEvent, eventStartEvent, eventStartVoting, getEvent, voteEventTrack, printret } from './common';
import ResponseStatusTypes from '../../src/utils/ResponseStatusTypes';
import chalk from 'chalk';

interface user {
	apiKey: string;
	id: string;
}

describe('BUGS', function() {
	this.timeout(10000);
	let ret: any;
	let owner: user;
	let guest: user;
	let dj: user;
	let other: user;

	/*
	*	If I delete two playlists in a row (not in the same api call), one of them isn't removed from the user playlists,
	*	and I got a "null" instead (in the middle of other playlists).
	*/
	describe('1', function() {
		let playlistids: string[];
		beforeEach(async function() {
			await reset();
			owner = await createUser(users[0]).then(ret => {return {apiKey: ret.body.apiKey, id: ret.body.id}});
			playlistids = await Promise.all(['1', '2', '3', '4'].map(name => createPlaylist(owner.apiKey, {name}))).then(r => r.map(ret => ret.body.id));
		});

		for (let index = 0; index < 1; index++) {
			it('Should delete all playlists', async function() {
				ret = await Promise.all(playlistids.map(id => deletePlaylist(owner.apiKey, id)));
				ret.forEach(r => expect(r.status).equals(ResponseStatusTypes.OK));
				ret = await getUser(owner.apiKey, owner.id);
				expect(ret.body.playlists).length(0);
				ret = await Promise.all(playlistids.map(id => getPlaylist(owner.apiKey, id)))
				ret.forEach(r => {
					expect(r.status).equals(ResponseStatusTypes.NOT_FOUND);
				});
				ret = await getUser(owner.apiKey, owner.id);
				expect(ret.body.playlists).length(0);
			});	
		}
	});


	/*
	*	When playing the first track in eventPlaylist.already played, result is NOT_FOUND (or, no change)
	*/
	describe('2', function() {
		let eventid: string;
		let playlistid: string;
		let trackids: string[];
		beforeEach(async function() {
			await reset();
			owner = await createUser(users[0]).then(ret => {return {apiKey: ret.body.apiKey, id: ret.body.id}});
			playlistid = (await createPlaylist(owner.apiKey, {name: 'my playlist'})).body.id;
			await Promise.all(['1', '2', '3', '4'].map(serviceid => addTrack(owner.apiKey, playlistid, serviceid)));
			eventid = (await createEvent(owner.apiKey, {name: 'my event'})).body.id;
			await addPlaylistToEvent(owner.apiKey, eventid, playlistid);
			ret = await eventStartEvent(owner.apiKey, eventid);
			let eventPlaylist = ret.body.eventPlaylists[0];
			trackids = [...eventPlaylist.playedTracks, eventPlaylist.activeTrack, ...eventPlaylist.unplayedTracks].filter(x => !!x).map(track => track.id);
		});

		it('Should play the first track', async function() {
			let now = new Date();
			// play first unplayed track
			ret = await playerAction(owner.apiKey, eventid, 'play', {trackid: trackids[0], timestamp: now.toISOString(), trackPosition: 0});
			// play second unplayed track
			ret = await playerAction(owner.apiKey, eventid, 'play', {trackid: trackids[1], timestamp: now.toISOString(), trackPosition: 0});
			// play first played track
			ret = await playerAction(owner.apiKey, eventid, 'play', {trackid: trackids[0], timestamp: now.toISOString(), trackPosition: 0});
			expect(ret.status).equals(ResponseStatusTypes.OK);
			expect(ret.body.eventPlaylists.find(x => x.id == ret.body.activePlaylist).activeTrack).equals(trackids[0]);
		});
	});

	/*
	*	Sorting tracks sometimes fails after like/dislike, especially with later amout of tracks.
	*/
	describe('3', function() {
		let eventid: string;
		let playlistid: string;
		let trackids: string[];
		beforeEach(async function() {
			await reset();
			owner = await createUser(users[0]).then(ret => {return {apiKey: ret.body.apiKey, id: ret.body.id}});
			playlistid = (await createPlaylist(owner.apiKey, {name: 'my playlist'})).body.id;
			// add 20 tracks
			ret = await Promise.all(Array.from(Array(20).keys()).map(serviceid => addTrack(owner.apiKey, playlistid, serviceid.toString())));
			ret.map(r => expect(r.status).equals(ResponseStatusTypes.OK));
			eventid = (await createEvent(owner.apiKey, {name: 'my event'})).body.id;
			ret = await addPlaylistToEvent(owner.apiKey, eventid, playlistid);
			ret = await eventStartVoting(owner.apiKey, eventid);
			let eventPlaylist = ret.body.eventPlaylists[0];
			trackids = eventPlaylist.unplayedTracks.map(track => track.id);
		});

		const testCount: number = 2;
		for (let index = 0; index < testCount; index++) {
			let action: 'like'|'dislike' = Math.random() > 0.5 ? 'like' : 'dislike';
			it(`Should move the track to the correct location after ${action}`, async function() {
				let oldIndex: number = Math.floor(Math.random() * (trackids.length - 1));
				// generate index to move from and to
				// console.log(`${action} track ${trackids[oldIndex]} of ${oldIndex}.`);
				// move track
				let trackid: string = trackids[oldIndex];
				ret = await voteEventTrack(owner.apiKey, eventid, trackid, action);
				expect(ret.status).equals(ResponseStatusTypes.OK);
				ret = await getEvent(owner.apiKey, eventid);
				expect(ret.status).equals(ResponseStatusTypes.OK);
				let eventPlaylist = ret.body.eventPlaylists[0];
				trackids = eventPlaylist.unplayedTracks.map(track => track.id);
				// console.log(`Track ${trackid} was in position ${oldIndex}, should be in position ${action == 'like' ? 0 : (trackids.length - 1)}, is in position ${trackids.indexOf(trackid)}.`);
				expect(trackids.indexOf(trackid)).equal(action == 'like' ? 0 : (trackids.length - 1));
			});
		}
	});

	/*
	*	Multiple play / pause calls on the same track should not return 'No change'
	*/
	describe('4', function() {
		let playlistid: string;
		let eventid: string;
		let trackids: string[];

		before(async function() {
			await reset();
			owner = await createUser(users[0]).then(ret => {return {apiKey: ret.body.apiKey, id: ret.body.id}});
			playlistid = (await createPlaylist(owner.apiKey, {name: 'my playlist'})).body.id;
			// add 3 tracks
			ret = await Promise.all(Array.from(Array(3).keys()).map(serviceid => addTrack(owner.apiKey, playlistid, serviceid.toString())));
			ret.map(r => expect(r.status).equals(ResponseStatusTypes.OK));
			ret = await createEvent(owner.apiKey, {name: 'my event'});
			expect(ret.status).equals(ResponseStatusTypes.OK);
			eventid = ret.body.id;
			ret = await addPlaylistToEvent(owner.apiKey, eventid, playlistid);
			expect(ret.status).equals(ResponseStatusTypes.OK);
			ret = await eventStartEvent(owner.apiKey, eventid);
			let eventPlaylist = ret.body.eventPlaylists[0];
			trackids = eventPlaylist.unplayedTracks.map(track => track.id);
		});

		it('Should not return "No change" after play - pause - play', async function() {
			ret = await playerAction(owner.apiKey, eventid, 'play', {trackid: trackids[0], timestamp: (new Date()).toISOString(), trackPosition: 0});
			ret = await playerAction(owner.apiKey, eventid, 'pause', {trackid: trackids[0], timestamp: (new Date()).toISOString(), trackPosition: 0});
			ret = await playerAction(owner.apiKey, eventid, 'play', {trackid: trackids[0], timestamp: (new Date()).toISOString(), trackPosition: 0});
			expect(ret.body.message).not.equal('no change');
		});
	});

	/*
	*	Liking then unliking a track puts in second position in large playlists
	*/

	describe('5', function() {
		let playlistid: string;
		let eventid: string;
		let trackids: string;
		let trackid: string;

		[5, 11].forEach(tracknum => {
			describe(`tracknum: ${tracknum}`, function() {
				before(async function() {
					this.timeout(10000);
					await reset();
					owner = await createUser(users[0]).then(ret => {return {apiKey: ret.body.apiKey, id: ret.body.id}});
					playlistid = (await createPlaylist(owner.apiKey, {name: 'my playlist'})).body.id;
					// add x tracks
					ret = await Promise.all(Array.from(Array(tracknum).keys()).map(serviceid => addTrack(owner.apiKey, playlistid, serviceid.toString())));
					ret.map(r => expect(r.status).equals(ResponseStatusTypes.OK));
					ret = await createEvent(owner.apiKey, {name: 'my event'});
					expect(ret.status).equals(ResponseStatusTypes.OK);
					eventid = ret.body.id;
					ret = await addPlaylistToEvent(owner.apiKey, eventid, playlistid);
					expect(ret.status).equals(ResponseStatusTypes.OK);
					ret = await Promise.all([eventStartEvent(owner.apiKey, eventid), eventStartVoting(owner.apiKey, eventid)]);
					ret.map(r => expect(r.status).equals(ResponseStatusTypes.OK));
					let eventPlaylist = ret[1].body.eventPlaylists[0];
					trackids = eventPlaylist.unplayedTracks.map(track => track.id);
				});
		
				it('Should put track in first position after like and unlike', async function() {
					trackid = trackids[3];
					ret = await getEvent(owner.apiKey, eventid);
					ret = await voteEventTrack(owner.apiKey, eventid, trackid, 'like');
					expect(ret.status).equals(ResponseStatusTypes.OK);
					ret = await getEvent(owner.apiKey, eventid);
					ret = await voteEventTrack(owner.apiKey, eventid, trackid, 'unlike');
					expect(ret.status).equals(ResponseStatusTypes.OK);
					ret = await getEvent(owner.apiKey, eventid);
					expect(ret.body.eventPlaylists[0].unplayedTracks.findIndex(track => track.id == trackid)).equals(0);
				});
			});
		});
	});
});