import * as chai from 'chai';
import chaiHttp = require('chai-http');
chai.use(chaiHttp);
const expect = chai.expect;
import * as socketio from "socket.io-client";
import SocketActions from "../../src/utils/SocketActions";

import { reset, users, createUser, createPlaylist, getPlaylist, getPlaylists, deletePlaylist, getUser, addTrack, removeTrack, addEditorToPlaylist, removeEditorFromPlaylist, addGuestToPlaylist, removeGuestFromPlaylist, updatePlaylist, moveTrack, playlistSocketConnect, duplicatePlaylist, createEvent, addPlaylistsToEvent, addPlaylistToEvent, getEvent, deletePlaylists, addGuestsToPlaylist, addEditorsToPlaylist, removeEditorsFromPlaylist, removeGuestsFromPlaylist, printret, searchPlaylist} from './common';
import app from "../../src/app";
import ResponseStatusTypes from "../../src/utils/ResponseStatusTypes";
import { Visibility, Editing, SortingType } from "../../src/types/types";
import { Playlist } from '../../src/classes/Playlist';
import { socketController } from '../../src/controllers/socketController';
import chalk from 'chalk';

const search_limit: number = require('../../config.json').mongo_search_limit;

interface user {
	apiKey: string;
	id: string;
	playlists?: string[]; 
}

interface track {
	serviceid: string;
	id?: string;
}

describe('Playlist', function() {
	this.timeout(10000);
	let user1: user;
	let user2: user;
	let user3: user;
	let ret: any;

	describe('Create Playlist', function() {
		const name: string = 'My birthday playlist';
		before(async function() {
			await reset();
			ret = await Promise.all(users.map(createUser));
			ret = [user1, user2, user3] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});	
		});

		it('Should return playlist id', async function() {
			ret = await createPlaylist(user1.apiKey, {name})
			expect(ret.status).equal(ResponseStatusTypes.OK);
			expect(ret.body.id).exist;
			user1.playlists = [ret.body.id];
		});
		it('Should have created a playlist with owner', async function() {
			ret = await getPlaylist(user1.apiKey, user1.playlists[0]);
			expect(ret.status).equal(ResponseStatusTypes.OK);
			expect(ret.body.owner.id).equals(user1.id);
		});
		it('Owner should have playlist id', async function() {
			ret = await getUser(user1.apiKey, user1.id);
			expect(ret.body.playlists).include(user1.playlists[0]);
		});
		it('Should create playlist with settings', async function() {
			await reset();
			ret = await Promise.all(users.map(createUser));
			[user1, user2, user3] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});	

			const visibility: Visibility = 'PRIVATE';
			const editing: Editing = 'PRIVATE';
			const sortingType: SortingType = 'move';

			ret = await createPlaylist(user1.apiKey, {name, visibility, editing, sortingType})
			expect(ret.status).equal(ResponseStatusTypes.OK);
			expect(ret.body.visibility).equal(visibility);
			expect(ret.body.sortingType).equal(sortingType);
		});

		describe('Should fail to create a playlist if ...', function() {
			before(async function() {
				await reset();
				ret = await Promise.all(users.map(createUser));
				[user1, user2, user3] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});	
			});

			it('no apiKey is provided', async function() {
				ret = await chai.request(app.app)
					.post(`/playlists`)
					.send({name});
				expect(ret.status).equal(ResponseStatusTypes.BAD_REQUEST);
			});
			it('apiKey is invalid', async function() {
				ret = await chai.request(app.app)
					.post(`/playlists?apiKey=123456789`)
					.send({name});
				expect(ret.status).equal(ResponseStatusTypes.UNAUTHORIZED);
			});
			it('no name is provided', async function() {
				ret = await chai.request(app.app)
					.post(`/playlists?apiKey=${user1.apiKey}`)
					.send({});
				expect(ret.status).equal(ResponseStatusTypes.BAD_REQUEST);
			});
			it('name is invalid', async function() {
				ret = await chai.request(app.app)
					.post(`/playlists?apiKey=${user1.apiKey}`)
					.send({name: ''});
				expect(ret.status).equal(ResponseStatusTypes.BAD_REQUEST);
			});
			it('settings are invalid (visibility)', async function() {
				ret = await chai.request(app.app)
					.post(`/playlists?apiKey=${user1.apiKey}`)
					.send({name, visibility: 'SUPER_PRIVATE'});
				expect(ret.status).equal(ResponseStatusTypes.BAD_REQUEST);
			});
			it('settings are invalid (sortingType)', async function() {
				ret = await chai.request(app.app)
					.post(`/playlists?apiKey=${user1.apiKey}`)
					.send({name, sortingType: 'ihazallrights'});
				expect(ret.status).equal(ResponseStatusTypes.BAD_REQUEST);
			});
			it('settings are invalid (editing)', async function() {
				ret = await chai.request(app.app)
					.post(`/playlists?apiKey=${user1.apiKey}`)
					.send({name, editing: 'MEGAEDITOR'});
				expect(ret.status).equal(ResponseStatusTypes.BAD_REQUEST);
			});
		});
	});

	describe('Get Playlist', function() {
		const name: string = 'My birthday playlist';
		let playlistid: string;
		let playlistid2: string;

		before(async function() {
			await reset();
			ret = await Promise.all(users.map(createUser));
			[user1, user2, user3] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});	
			ret = await createPlaylist(user1.apiKey, {name});
			playlistid = ret.body.id;
			ret = await addEditorToPlaylist(user1.apiKey, playlistid, user2.id);
			ret = await createPlaylist(user1.apiKey, {name: 'privateplaylist', visibility: 'PRIVATE'});
			playlistid2 = ret.body.id;
		});

		it('Should return a playlist', async function() {
			ret = await getPlaylist(user1.apiKey, playlistid);
			expect(ret.status).equal(ResponseStatusTypes.OK);
			expect(ret.body.id).equal(playlistid);
			expect(ret.body.name).equal(name);
			expect(ret.body.owner.id).equal(user1.id);
			expect(ret.body.editors).satisfy(y => y.find(x => x.id == user2.id)); // check you get FULL editor
		});

		describe('Should fail to get playlist if ...', function() {
			it('no apiKey is provided', async function() {
				ret = await chai.request(app.app)
					.get(`/playlists/${playlistid}`)
					.send({name});
				expect(ret.status).equal(ResponseStatusTypes.BAD_REQUEST);
			});
			it('apiKey is invalid', async function() {
				ret = await chai.request(app.app)
					.get(`/playlists/${playlistid}?apiKey=123456789`)
					.send({name});
				expect(ret.status).equal(ResponseStatusTypes.UNAUTHORIZED);
			});
			it('playlist does not exist', async function() {
				ret = await getPlaylist(user1.apiKey, '12345678');
				expect(ret.status).equal(ResponseStatusTypes.NOT_FOUND);
			});
			it('user does not have access to playlist', async function() {
				ret = await getPlaylist(user2.apiKey, playlistid2);
				expect(ret.status).equal(ResponseStatusTypes.UNAUTHORIZED);
			});
		});
	});

	describe('Get Playlists', function() {
		let playlists: any[] = ['playlist1', 'playlist2', 'playlist3'].map(x => {return {name: x}});

		before(async function() {
			await reset();
			ret = await Promise.all(users.map(createUser));
			[user1, user2, user3] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});	
			ret = await Promise.all(playlists.map(x => createPlaylist(user1.apiKey, {name: x.name})));
			ret.map(r => expect(r.status).equals(ResponseStatusTypes.OK));
		});
		it('Should return all playlists of a user (owner, editor and guest)', async function() {
			let ret = await getPlaylists(user1.apiKey);
			expect(ret.status).equal(ResponseStatusTypes.OK);
			expect(ret.body.owner).exist;
			expect(ret.body.editor).exist;
			expect(ret.body.guest).exist;
			expect(ret.body.owner).length(3);
		});

		it('Should return empty lists if user has no playlists (owner, editor, guest)', async function() {
			let ret = await getPlaylists(user2.apiKey);
			expect(ret.status).equal(ResponseStatusTypes.OK);
			expect(ret.body.owner).exist;
			expect(ret.body.editor).exist;
			expect(ret.body.guest).exist;
			expect(ret.body.owner).length(0);
			expect(ret.body.guest).length(0);
			expect(ret.body.editor).length(0);
		});

		describe('Should fail to get playlist if ...', function() {
			it('no apiKey was provided', async function() {
				ret = await chai.request(app.app)
					.get(`/playlists`)
					.send();
				expect(ret.status).equals(ResponseStatusTypes.BAD_REQUEST);
			});
			it('apiKey was invalid', async function() {
				ret = await getPlaylists('0123456789');
				expect(ret.status).equals(ResponseStatusTypes.UNAUTHORIZED);
			});
		});
	});

	describe('Add Track', function() {
		const playlistName: string = 'My birthday playlist';
		const trackId: string = '123456789';
		let playlistid: string;
		
		before(async function() {
			await reset();
			ret = await Promise.all(users.map(createUser));
			[user1, user2, user3] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});	
			ret = await createPlaylist(user1.apiKey, {name: playlistName});
			playlistid = ret.body.id;
		});
		
		it('Should add a new track to the playlist', async function() {
			ret = await addTrack(user1.apiKey, playlistid, trackId);
			expect(ret.status).equal(ResponseStatusTypes.OK);
			expect(ret.body.serviceid).equal(trackId);
		});
		it('Should add a new track with a service to the playlist', async function() {
			const trackId2: string = '00000';
			const service: string = 'Deezer';

			ret = await addTrack(user1.apiKey, playlistid, trackId2, service)
			expect(ret.status).equal(ResponseStatusTypes.OK);
			expect(ret.body.serviceid).equal(trackId2);
			expect(ret.body.service).equal(service);
		});
		it('A user who\'s not a guest or editor can add and remove a track', async function() {
			let serviceid: string = 'BBBBBBB';
			ret = await addTrack(user3.apiKey, playlistid, serviceid);
			expect(ret.status).equal(ResponseStatusTypes.OK);
			expect(ret.body.serviceid).equal(serviceid);
			let trackid: string = ret.body.id;
			ret = await removeTrack(user3.apiKey, playlistid, trackid);
			expect(ret.status).equal(ResponseStatusTypes.OK);
			ret = await getPlaylist(user3.apiKey, playlistid);
			expect(ret.status).equals(ResponseStatusTypes.OK);
			expect(ret.body.tracks.map(x => x.id)).not.include(trackid);
		});

		describe('Should fail to add a track if ... ', function() {
			it('if trying to add track twice', async function() {
				const trackId1: string = '001';
				await addTrack(user1.apiKey, playlistid, trackId1);
				let ret = await addTrack(user1.apiKey, playlistid, trackId1);
				expect(ret.status).equal(ResponseStatusTypes.OK);
			});
			it('if trying to add a track with same id as a track with no service', async function() {
				const trackId: string = '111';
				const service: string = 'Deezer';
				await addTrack(user1.apiKey, playlistid, trackId);
				let ret = await addTrack(user1.apiKey, playlistid, trackId, service);
				expect(ret.status).equal(ResponseStatusTypes.OK);
			});
			it('user has no rights', async function() {
				ret = await updatePlaylist(user1.apiKey, playlistid, {visibility: 'PRIVATE'});
				expect(ret.status).equals(ResponseStatusTypes.OK);
				ret = await addTrack(user3.apiKey, playlistid, '0123456654');
				expect(ret.status).equals(ResponseStatusTypes.UNAUTHORIZED);
			});
		})
	});

	describe('Remove Track', function() {
		const playlistName: string = 'My birthday playlist';
		const serviceid: string = '123456789';
		let playlistid: string;
		let trackId: string;

		before(async function() {
			await reset();
			ret = await Promise.all(users.map(createUser));
			[user1, user2, user3] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});	
			ret = await createPlaylist(user1.apiKey, {name: playlistName});
			playlistid = ret.body.id;
			ret = await addTrack(user1.apiKey, playlistid, serviceid, 'Deezer');
			trackId = ret.body.id;
		});

		it('Should delete the track from the playlist', async function() {
			ret = await removeTrack(user1.apiKey, playlistid, trackId);
			expect(ret.status).equal(ResponseStatusTypes.OK);
			ret = await getPlaylist(user1.apiKey, playlistid);
			expect(ret.body.tracks).not.contain(x => x.id == trackId);		
		});

		describe('Should fail to remove track if ...', function() {
			it('trying to remove a track that doesn\'t exist', async function() {
				const trackId1: string = '0000001';
				let ret = await removeTrack(user1.apiKey, playlistid, trackId1);
				expect(ret.status).equal(ResponseStatusTypes.NOT_FOUND);
				expect(ret.body.errorCode).exist;
			});
		});
	});

	describe('Delete Playlist', function() {
		const name: string = 'My birthday playlist';
		let playlistid: string;
		let eventid: string;

		before(async function() {
			await reset();
			ret = await Promise.all(users.map(createUser));
			[user1, user2, user3] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});	
			ret = await createPlaylist(user1.apiKey, {name});
			playlistid = ret.body.id;
			await addEditorToPlaylist(user1.apiKey, playlistid, user2.id);
			await addGuestToPlaylist(user1.apiKey, playlistid, user3.id);
			eventid = (await createEvent(user1.apiKey, {name: 'my event'})).body.id;
			await addPlaylistToEvent(user1.apiKey, eventid, playlistid);
		});

		it('Should delete the playlist', async function() {
			ret = await deletePlaylist(user1.apiKey, playlistid);
			expect(ret.status).equal(ResponseStatusTypes.OK);
			await Promise.all([
				(async () => {
					// should not be able to get playlist
					ret = await getPlaylist(user1.apiKey, playlistid);
					expect(ret.status).equal(ResponseStatusTypes.NOT_FOUND);
					ret = await Playlist.get({id: playlistid});
					expect(ret).equals(undefined);
				})(),
				(async () => {
					// user should have no playlists left
					ret = await getPlaylists(user1.apiKey);
					expect(ret.status).equal(ResponseStatusTypes.OK);
					expect(ret.body.owner).length(0);
				})(),
				(async () => {
					// event should have not playlists
					ret = await getEvent(user1.apiKey, eventid);
					expect(ret.body.playlists).length(0);
				})(),
				(async () => {
					// guest should no longer be in guest.guestInPlaylist
					ret = await getUser(user2.apiKey, user2.id);
					expect(ret.body.guestInPlaylists).length(0);
				})(),
				(async () => {
					// editor should no longer be in editor.editorInPlaylist
					ret = await getUser(user2.apiKey, user2.id);
					expect(ret.body.editorInPlaylists).length(0);
				})()
			]);
		});
	});

	describe('Delete Playlists', function() {
		let playlistids: string[];
		let eventid: string;

		before(async function() {
			await reset();
			ret = await Promise.all(users.map(createUser));
			[user1, user2, user3] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});	
			eventid = (await createEvent(user1.apiKey, {name: 'my event'})).body.id;
			playlistids = await Promise.all(['1', '2', '3'].map(async name => {
				ret = await createPlaylist(user1.apiKey, {name});
				let id = ret.body.id;
				await addEditorToPlaylist(user1.apiKey, id, user2.id);
				await addGuestToPlaylist(user1.apiKey, id, user3.id);
				await addPlaylistToEvent(user1.apiKey, eventid, id);
				return id;
			}));
		});

		it('Should delete the playlist', async function() {
			ret = await deletePlaylists(user1.apiKey, playlistids);
			expect(ret.status).equal(ResponseStatusTypes.OK);
			await Promise.all([
				...playlistids.map(async playlistid => {
					// should not be able to get playlist
					ret = await getPlaylist(user1.apiKey, playlistid);
					expect(ret.status).equal(ResponseStatusTypes.NOT_FOUND);
					ret = await Playlist.get({id: playlistid});
					expect(ret).equals(undefined);
				}),
				(async () => {
					// event should have not playlists
					ret = await getEvent(user1.apiKey, eventid);
					expect(ret.body.playlists).length(0);
				})(),
				(async () => {
					// guest should no longer be in guest.guestInPlaylist
					ret = await getUser(user2.apiKey, user2.id);
					expect(ret.body.guestInPlaylists).length(0);
				})(),
				(async () => {
					// editor should no longer be in editor.editorInPlaylist
					ret = await getUser(user2.apiKey, user2.id);
					expect(ret.body.editorInPlaylists).length(0);
				})(),
				(async () => {
					// owner should have no playlists left
					ret = await getPlaylists(user1.apiKey);
					expect(ret.status).equal(ResponseStatusTypes.OK);
					expect(ret.body.owner).length(0);
				})(),
			]);
			// event should have not playlists
			ret = await getEvent(user1.apiKey, eventid);
			expect(ret.body.playlists).length(0);
			// guests
		});
	});

	describe('Add Editor', function() {
		const name: string = 'My birthday playlist';
		let playlistid: string;

		before(async function() {
			await reset();
			let ret: any = await Promise.all(users.map(createUser));
			[user1, user2, user3] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});
			ret = await createPlaylist(user1.apiKey, {name});
			playlistid = ret.body.id;
		});

		it('Should add an editor', async function() {
			let ret = await addEditorToPlaylist(user1.apiKey, playlistid, user2.id)
			// ok response
			expect(ret.status).equal(ResponseStatusTypes.OK);
			// added to playlist.editors and playlist.guests
			ret = await getPlaylist(user1.apiKey, playlistid);
			expect(ret.body.editors).satisfy(editors => editors.find(user => user.id == user2.id));
			expect(ret.body.guests).satisfy(guests => guests.find(user => user.id == user2.id));
			// added to user.editor
			ret = await getUser(user2.apiKey, user2.id);	
			expect(ret.body.editorInPlaylists).include(playlistid);
			expect(ret.body.guestInPlaylists).include(playlistid);
		});

		describe('Should fail to add editor if ...', function() {
			it('apiKey is not provided', async function() {
				let ret = await chai.request(app.app)
					.put(`/playlists/${playlistid}/editors/${user3.id}`)
					.send();
				expect(ret.status).equal(ResponseStatusTypes.BAD_REQUEST);
				ret = await getPlaylist(user1.apiKey, playlistid);
				expect(ret.body.editors).not.include(user3.id);
			});
			it('apiKey is invalid', async function() {
				let ret = await addEditorToPlaylist('MYAWESOMEAPIKEY', playlistid, user3.id)
				expect(ret.status).equal(ResponseStatusTypes.UNAUTHORIZED);
				ret = await getPlaylist(user1.apiKey, playlistid);
				expect(ret.body.editors).not.include(user3.id);
			});
			it('playlistid is not prodived', async function() {
				let ret = await chai.request(app.app)
					.put(`/playlists/editors/${user3.id}?apiKey=${user1.apiKey}`)
					.send()
				expect(ret.status).equal(ResponseStatusTypes.NOT_FOUND);
				ret = await getPlaylist(user1.apiKey, playlistid);
				expect(ret.body.editors).not.include(user3.id);
			});
			it('playlist does not exist', async function() {
				let ret = await addEditorToPlaylist(user1.apiKey, '123456789', user3.id)
				expect(ret.status).equal(ResponseStatusTypes.NOT_FOUND);
				ret = await getPlaylist(user1.apiKey, playlistid);
				expect(ret.body.editors).not.include(user3.id);
			});
			it('user is not playlist owner', async function() {
				let ret = await addEditorToPlaylist(user2.apiKey, playlistid, user3.id)
				expect(ret.status).equal(ResponseStatusTypes.UNAUTHORIZED);
				ret = await getPlaylist(user1.apiKey, playlistid);
				expect(ret.body.editors).not.include(user3.id);
			});
			it('editor is already added', async function() {
				let ret = await addEditorToPlaylist(user1.apiKey, playlistid, user3.id)
				expect(ret.status).equal(ResponseStatusTypes.OK);
				ret = await addEditorToPlaylist(user1.apiKey, playlistid, user3.id);
				expect(ret.status).equal(ResponseStatusTypes.OK);
				ret = await getPlaylist(user1.apiKey, playlistid);
				expect(ret.body.editors).satisfies(x => {
					return x.filter(y => y.id == user3.id).length == 1;
				});
			});
			it('editor does not exist', async function() {
				const fakeid: string = '123456789';
				let ret = await addEditorToPlaylist(user1.apiKey, playlistid, fakeid)
				expect(ret.status).equal(ResponseStatusTypes.NOT_FOUND);
				ret = await getPlaylist(user1.apiKey, playlistid);
				expect(ret.body.editors).not.include(fakeid);
			});
			it('editor is owner of playlist', async function() {
				let ret = await addEditorToPlaylist(user1.apiKey, playlistid, user1.id)
				expect(ret.status).equal(ResponseStatusTypes.BAD_REQUEST);
				ret = await getPlaylist(user1.apiKey, playlistid);
				expect(ret.body.editors).satisfy(editors => !editors.find(user => user.id == user1.id));
			});
		});
	});

	describe('Add Editors', function() {
		let playlistid: string;

		before(async function() {
			await reset();
			ret = await Promise.all(users.map(createUser));
			[user1, user2, user3] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});
			ret = await createPlaylist(user1.apiKey, {name: 'playlist'});
			playlistid = ret.body.id;	
		});

		it('Should add editors', async function() {
			ret = await addEditorsToPlaylist(user1.apiKey, playlistid, [user2.id, user3.id]);
			expect(ret.status).equals(ResponseStatusTypes.OK);
			ret = await getPlaylist(user1.apiKey, playlistid);
			expect(ret.body.editors.map(x => x.id)).include.members([user2.id, user3.id]);
			await Promise.all([user2, user3].map(async (user: user) => {
				ret = await getUser(user.apiKey, user.id);
				expect(ret.body.editorInPlaylists).include(playlistid);
			}));
		});
	});	

	describe('Remove Editor', function() {
		const name: string = 'My birthday playlist';
		let playlistid: string;

		describe('Should remove editor if ...', function() {
			beforeEach(async function() {
				await reset();
				let ret:any = await Promise.all(users.map(createUser));
				[user1, user2, user3] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});	
				ret = await createPlaylist(user1.apiKey, {name});
				playlistid = ret.body.id;
				await addEditorToPlaylist(user1.apiKey, playlistid, user2.id);
			});

			it('playlist owner makes request', async function() {
				let ret = await removeEditorFromPlaylist(user1.apiKey, playlistid, user2.id)
				expect(ret.status).equal(ResponseStatusTypes.OK);
				ret = await getPlaylist(user1.apiKey, playlistid);
				expect(ret.body.editors.length).equal(0);
				expect(ret.body.guests.length).equal(1); // remove editor does not remove user as guest
				ret = await getUser(user1.apiKey, user1.id);
				expect(ret.body.editorInPlaylists).length(0);
				expect(ret.body.guestInPlaylists).length(0);
			});
	
			it('editor makes request', async function() {
				let ret = await chai.request(app.app)
					.delete(`/playlists/${playlistid}/editors/${user2.id}?apiKey=${user2.apiKey}`)
					.send();
				expect(ret.status).equal(ResponseStatusTypes.OK);
				ret = await getPlaylist(user1.apiKey, playlistid);
				expect(ret.body.editors).length(0);
				expect(ret.body.guests).length(1);
				ret = await getUser(user2.apiKey, user2.id);
				expect(ret.body.editorInPlaylists).length(0);
				expect(ret.body.guestInPlaylists).length(1);
			});
		})
		

		describe('Should fail to remove editor if ...', function() {
			before(async function() {
				await reset()
				let ret:any = await Promise.all(users.map(createUser));
				[user1, user2, user3] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});	
				ret = await createPlaylist(user1.apiKey, {name});
				playlistid = ret.body.id;
				await addEditorToPlaylist(user1.apiKey, playlistid, user2.id);
			});

			it('apiKey is not provided', async function() {
				let ret = await chai.request(app.app)
					.delete(`/playlists/${playlistid}/editors/${user2.id}`)
					.send();
				expect(ret.status).equal(ResponseStatusTypes.BAD_REQUEST);
				ret = await getPlaylist(user1.apiKey, playlistid);
				expect(ret.body.editors).length(1);
				expect(ret.body.guests).length(1);
				ret = await getUser(user2.apiKey, user2.id);
				expect(ret.body.editorInPlaylists).length(1);
				expect(ret.body.guestInPlaylists).length(1);
			});
			it('apiKey is invalid', async function() {
				let ret = await chai.request(app.app)
					.delete(`/playlists/${playlistid}/editors/${user2.id}?apiKey=0123456789`)
					.send();
				expect(ret.status).equal(ResponseStatusTypes.UNAUTHORIZED);
				ret = await getPlaylist(user1.apiKey, playlistid);
				expect(ret.body.editors).length(1);
				expect(ret.body.guests).length(1);
				ret = await getUser(user2.apiKey, user2.id);
				expect(ret.body.editorInPlaylists).length(1);
				expect(ret.body.guestInPlaylists).length(1);
			});
			it('userid is not prodived', async function() {
				let ret = await chai.request(app.app)
					.delete(`/playlists/${playlistid}/editors?apiKey=${user1.apiKey}`)
					.send();
				expect(ret.status).equal(ResponseStatusTypes.BAD_REQUEST);
				ret = await getPlaylist(user1.apiKey, playlistid);
				expect(ret.body.editors).length(1);
				expect(ret.body.guests).length(1);
				ret = await getUser(user2.apiKey, user2.id);
				expect(ret.body.editorInPlaylists).length(1);
				expect(ret.body.guestInPlaylists).length(1);
			});
			it('playlist does not exist', async function() {
				let ret = await chai.request(app.app)
					.delete(`/playlists/editors/${user2.id}?apiKey=${user1.apiKey}`)
					.send();
				expect(ret.status).equal(ResponseStatusTypes.NOT_FOUND);
				ret = await getPlaylist(user1.apiKey, playlistid);
				expect(ret.body.editors).length(1);
				expect(ret.body.guests).length(1);
				ret = await getUser(user2.apiKey, user2.id);
				expect(ret.body.editorInPlaylists).length(1);
				expect(ret.body.guestInPlaylists).length(1);
			});
			it('user is not editor and not playlist owner', async function() {
				let ret = await chai.request(app.app)
					.delete(`/playlists/${playlistid}/editors/${user2.id}?apiKey=${user3.apiKey}`)
					.send();
				expect(ret.status).equal(ResponseStatusTypes.UNAUTHORIZED);
				ret = await getPlaylist(user1.apiKey, playlistid);
				expect(ret.body.editors).length(1);
				expect(ret.body.guests).length(1);
				ret = await getUser(user2.apiKey, user2.id);
				expect(ret.body.editorInPlaylists).length(1);
				expect(ret.body.guestInPlaylists).length(1);
			});
			it('editor is not in playlist editor list', async function() {
				let ret = await chai.request(app.app)
					.delete(`/playlists/${playlistid}/editors/${user3.id}?apiKey=${user1.apiKey}`)
					.send();
				expect(ret.status).equal(ResponseStatusTypes.OK);
				ret = await getPlaylist(user1.apiKey, playlistid);
				expect(ret.body.editors).length(1);
				expect(ret.body.guests).length(1);
				ret = await getUser(user2.apiKey, user2.id);
				expect(ret.body.editorInPlaylists).length(1);
				expect(ret.body.guestInPlaylists).length(1);
			});
			it('editor does not exist', async function() {
				let ret = await chai.request(app.app)
					.delete(`/playlists/${playlistid}/editors/0123456789?apiKey=${user1.apiKey}`)
					.send();
				expect(ret.status).equal(ResponseStatusTypes.NOT_FOUND);
				ret = await getPlaylist(user1.apiKey, playlistid);
				expect(ret.body.editors).length(1);
				expect(ret.body.guests).length(1);
				ret = await getUser(user2.apiKey, user2.id);
				expect(ret.body.editorInPlaylists).length(1);
				expect(ret.body.guestInPlaylists).length(1);
			});
		});
	});

	describe('Remove Editors', function() {
		let playlistid: string;

		before(async function() {
			await reset();
			ret = await Promise.all(users.map(createUser));
			[user1, user2, user3] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});
			ret = await createPlaylist(user1.apiKey, {name: 'playlist'});
			playlistid = ret.body.id;	
			ret = await addEditorsToPlaylist(user1.apiKey, playlistid, [user2.id, user3.id]);
			expect(ret.status).equals(ResponseStatusTypes.OK);
		});

		it('Should remove editors', async function() {
			ret = await removeEditorsFromPlaylist(user1.apiKey, playlistid, [user2.id, user3.id]);
			expect(ret.status).equals(ResponseStatusTypes.OK);
			ret = await getPlaylist(user1.apiKey, playlistid);
			expect(ret.body.editors.map(x => x.id)).not.include.members([user2.id, user3.id]);
			await Promise.all([user2, user3].map(async (user: user) => {
				ret = await getUser(user.apiKey, user.id);
				expect(ret.body.editorInPlaylists).not.include(playlistid);
			}));
		});
	});	

	describe('Add Guest', function() {
		const name: string = 'My birthday playlist';
		let playlistid: string;

		before(async function() {
			await reset()
			let ret: any = await Promise.all(users.map(createUser));
			[user1, user2, user3] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});
			ret = await createPlaylist(user1.apiKey, {name});
			playlistid = ret.body.id;
		});

		it('Should add a guest', async function() {
			let ret = await addGuestToPlaylist(user1.apiKey, playlistid, user2.id)
			// ok response
			expect(ret.status).equal(ResponseStatusTypes.OK);
			// added to playlist.guests and playlist.guests
			ret = await getPlaylist(user1.apiKey, playlistid);
			
			expect(ret.body.guests).satisfy(guests => guests.find(user => user.id == user2.id));
			expect(ret.body.editors).satisfy(editors => !editors.find(user => user.id == user2.id));
			// added to user.guest
			ret = await getUser(user2.apiKey, user2.id);	
			expect(ret.body.guestInPlaylists).include(playlistid);
			expect(ret.body.editorInPlaylists).not.include(playlistid);
		});

		describe('Should fail to add guest if ...', function() {
			it('apiKey is not provided', async function() {
				let ret = await chai.request(app.app)
					.put(`/playlists/${playlistid}/guests/${user3.id}`)
					.send();
				expect(ret.status).equal(ResponseStatusTypes.BAD_REQUEST);
				ret = await getPlaylist(user1.apiKey, playlistid);
				expect(ret.body.guests).not.include(user3.id);
			});
			it('apiKey is invalid', async function() {
				let ret = await addGuestToPlaylist('MYAWESOMEAPIKEY', playlistid, user3.id)
				expect(ret.status).equal(ResponseStatusTypes.UNAUTHORIZED);
				ret = await getPlaylist(user1.apiKey, playlistid);
				expect(ret.body.guests).not.include(user3.id);
			});
			it('playlistid is not prodived', async function() {
				let ret = await chai.request(app.app)
					.put(`/playlists/guests/${user3.id}?apiKey=${user1.apiKey}`)
					.send()
				expect(ret.status).equal(ResponseStatusTypes.NOT_FOUND);
				ret = await getPlaylist(user1.apiKey, playlistid);
				expect(ret.body.guests).not.include(user3.id);
			});
			it('playlist does not exist', async function() {
				let ret = await addGuestToPlaylist(user1.apiKey, '123456789', user3.id)
				expect(ret.status).equal(ResponseStatusTypes.NOT_FOUND);
				ret = await getPlaylist(user1.apiKey, playlistid);
				expect(ret.body.guests).not.include(user3.id);
			});
			it('user is not playlist owner', async function() {
				let ret = await addGuestToPlaylist(user2.apiKey, playlistid, user3.id)
				expect(ret.status).equal(ResponseStatusTypes.UNAUTHORIZED);
				ret = await getPlaylist(user1.apiKey, playlistid);
				expect(ret.body.guests).not.include(user3.id);
			});
			it('guest is already added', async function() {
				let ret = await addGuestToPlaylist(user1.apiKey, playlistid, user3.id)
				expect(ret.status).equal(ResponseStatusTypes.OK);
				ret = await addGuestToPlaylist(user1.apiKey, playlistid, user3.id);
				expect(ret.status).equal(ResponseStatusTypes.OK);
				ret = await getPlaylist(user1.apiKey, playlistid);
				expect(ret.body.guests).satisfies(x => {
					return x.filter(y => y.id == user3.id).length == 1;
				});
			});
			it('guest does not exist', async function() {
				const fakeid: string = '123456789';
				let ret = await addGuestToPlaylist(user1.apiKey, playlistid, fakeid)
				expect(ret.status).equal(ResponseStatusTypes.NOT_FOUND);
				ret = await getPlaylist(user1.apiKey, playlistid);
				expect(ret.body.guests).not.include(fakeid);
			});
			it('guest is owner of playlist', async function() {
				let ret = await addGuestToPlaylist(user1.apiKey, playlistid, user1.id)
				expect(ret.status).equal(ResponseStatusTypes.BAD_REQUEST);
				ret = await getPlaylist(user1.apiKey, playlistid);
				expect(ret.body.guests).satisfy(guests => !guests.find(user => user.id == user1.id));
			});
		});
	});

	describe('Add Guests', function() {
		let playlistid: string;

		before(async function() {
			await reset();
			ret = await Promise.all(users.map(createUser));
			[user1, user2, user3] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});
			ret = await createPlaylist(user1.apiKey, {name: 'playlist'});
			playlistid = ret.body.id;	
		});

		it('Should add guests', async function() {
			ret = await addGuestsToPlaylist(user1.apiKey, playlistid, [user2.id, user3.id]);
			expect(ret.status).equals(ResponseStatusTypes.OK);
			ret = await getPlaylist(user1.apiKey, playlistid);
			expect(ret.body.guests.map(x => x.id)).include.members([user2.id, user3.id]);
			await Promise.all([user2, user3].map(async (user: user) => {
				ret = await getUser(user.apiKey, user.id);
				expect(ret.body.guestInPlaylists).include(playlistid);
			}));
		});
	});	

	describe('Remove Guest', function() {
		const name: string = 'My birthday playlist';
		let playlistid: string;

		describe('Should remove guest if ...', function() {
			beforeEach(async function() {
				await reset()
				let ret:any = await Promise.all(users.map(createUser));
				[user1, user2, user3] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});	
				ret = await createPlaylist(user1.apiKey, {name});
				playlistid = ret.body.id;
				await addGuestToPlaylist(user1.apiKey, playlistid, user2.id);
			});

			it('playlist owner makes request', async function() {
				let ret = await removeGuestFromPlaylist(user1.apiKey, playlistid, user2.id)
				expect(ret.status).equal(ResponseStatusTypes.OK);
				ret = await getPlaylist(user1.apiKey, playlistid);
				expect(ret.body.guests.length).equal(0);
				expect(ret.body.editors.length).equal(0); // remove guest removes editor
				ret = await getUser(user1.apiKey, user1.id);
				expect(ret.body.guestInPlaylists).length(0);
				expect(ret.body.editorInPlaylists).length(0);
			});
	
			it('guest makes request', async function() {
				let ret = await chai.request(app.app)
					.delete(`/playlists/${playlistid}/guests/${user2.id}?apiKey=${user2.apiKey}`)
					.send();				
				expect(ret.status).equal(ResponseStatusTypes.OK);
				ret = await getPlaylist(user1.apiKey, playlistid);
				expect(ret.body.guests).length(0);
				expect(ret.body.editors).length(0);
				ret = await getUser(user2.apiKey, user2.id);
				expect(ret.body.guestInPlaylists).length(0);
				expect(ret.body.editorInPlaylists).length(0);
			});
		})
		

		describe('Should fail to remove guest if ...', function() {
			before(async function() {
				await reset()
				let ret:any = await Promise.all(users.map(createUser));
				[user1, user2, user3] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});	
				ret = await createPlaylist(user1.apiKey, {name});
				playlistid = ret.body.id;
				await addGuestToPlaylist(user1.apiKey, playlistid, user2.id);
			});

			it('apiKey is not provided', async function() {
				let ret = await chai.request(app.app)
					.delete(`/playlists/${playlistid}/guests/${user2.id}`)
					.send();
				expect(ret.status).equal(ResponseStatusTypes.BAD_REQUEST);
				ret = await getPlaylist(user1.apiKey, playlistid);
				expect(ret.body.guests).length(1);
				expect(ret.body.editors).length(0);
				ret = await getUser(user2.apiKey, user2.id);
				expect(ret.body.guestInPlaylists).length(1);
				expect(ret.body.editorInPlaylists).length(0);
			});
			it('apiKey is invalid', async function() {
				let ret = await chai.request(app.app)
					.delete(`/playlists/${playlistid}/guests/${user2.id}?apiKey=0123456789`)
					.send();
				expect(ret.status).equal(ResponseStatusTypes.UNAUTHORIZED);
				ret = await getPlaylist(user1.apiKey, playlistid);
				expect(ret.body.guests).length(1);
				expect(ret.body.editors).length(0);
				ret = await getUser(user2.apiKey, user2.id);
				expect(ret.body.guestInPlaylists).length(1);
				expect(ret.body.editorInPlaylists).length(0);
			});
			it('userid is not prodived', async function() {
				let ret = await chai.request(app.app)
					.delete(`/playlists/${playlistid}/guests?apiKey=${user1.apiKey}`)
					.send();
				expect(ret.status).equal(ResponseStatusTypes.BAD_REQUEST);
				ret = await getPlaylist(user1.apiKey, playlistid);
				expect(ret.body.guests).length(1);
				expect(ret.body.editors).length(0);
				ret = await getUser(user2.apiKey, user2.id);
				expect(ret.body.guestInPlaylists).length(1);
				expect(ret.body.editorInPlaylists).length(0);
			});
			it('playlist does not exist', async function() {
				let ret = await chai.request(app.app)
					.delete(`/playlists/guests/${user2.id}?apiKey=${user1.apiKey}`)
					.send();
				expect(ret.status).equal(ResponseStatusTypes.NOT_FOUND);
				ret = await getPlaylist(user1.apiKey, playlistid);
				expect(ret.body.guests).length(1);
				expect(ret.body.editors).length(0);
				ret = await getUser(user2.apiKey, user2.id);
				expect(ret.body.guestInPlaylists).length(1);
				expect(ret.body.editorInPlaylists).length(0);
			});
			it('user is not guest and not playlist owner', async function() {
				let ret = await chai.request(app.app)
					.delete(`/playlists/${playlistid}/guests/${user2.id}?apiKey=${user3.apiKey}`)
					.send();
				expect(ret.status).equal(ResponseStatusTypes.UNAUTHORIZED);
				ret = await getPlaylist(user1.apiKey, playlistid);
				expect(ret.body.guests).length(1);
				expect(ret.body.editors).length(0);
				ret = await getUser(user2.apiKey, user2.id);
				expect(ret.body.guestInPlaylists).length(1);
				expect(ret.body.editorInPlaylists).length(0);
			});
			it('guest is not in playlist guest list', async function() {
				let ret = await chai.request(app.app)
					.delete(`/playlists/${playlistid}/guests/${user3.id}?apiKey=${user1.apiKey}`)
					.send();
				expect(ret.status).equal(ResponseStatusTypes.OK);
				ret = await getPlaylist(user1.apiKey, playlistid);
				expect(ret.body.guests).length(1);
				expect(ret.body.editors).length(0);
				ret = await getUser(user2.apiKey, user2.id);
				expect(ret.body.guestInPlaylists).length(1);
				expect(ret.body.editorInPlaylists).length(0);
			});
			it('guest does not exist', async function() {
				let ret = await chai.request(app.app)
					.delete(`/playlists/${playlistid}/guests/0123456789?apiKey=${user1.apiKey}`)
					.send();
				expect(ret.status).equal(ResponseStatusTypes.NOT_FOUND);
				ret = await getPlaylist(user1.apiKey, playlistid);
				expect(ret.body.guests).length(1);
				expect(ret.body.editors).length(0);
				ret = await getUser(user2.apiKey, user2.id);
				expect(ret.body.guestInPlaylists).length(1);
				expect(ret.body.editorInPlaylists).length(0);
			});
		});
	});

	describe('Remove Guests', function() {
		let playlistid: string;

		before(async function() {
			await reset();
			ret = await Promise.all(users.map(createUser));
			[user1, user2, user3] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});
			ret = await createPlaylist(user1.apiKey, {name: 'playlist'});
			playlistid = ret.body.id;	
			ret = await addGuestsToPlaylist(user1.apiKey, playlistid, [user2.id, user3.id]);
			expect(ret.status).equals(ResponseStatusTypes.OK);
		});

		it('Should remove guests', async function() {
			ret = await removeGuestsFromPlaylist(user1.apiKey, playlistid, [user2.id, user3.id]);
			expect(ret.status).equals(ResponseStatusTypes.OK);
			ret = await getPlaylist(user1.apiKey, playlistid);
			expect(ret.body.guests.map(x => x.id)).not.include.members([user2.id, user3.id]);
			await Promise.all([user2, user3].map(async (user: user) => {
				ret = await getUser(user.apiKey, user.id);
				expect(ret.body.guestInPlaylists).not.include(playlistid);
			}));
		});
	});	

	describe('Move track (playlist not in event)', function() {
		let playlistid: string;
		let tracks: track[] = [{serviceid: 'A'}, {serviceid: 'B'}, {serviceid: 'C'}];
		before(async function() {
			await reset();
			let ret: any = await Promise.all(users.map(createUser));
			[user1, user2, user3] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});	
			ret = await createPlaylist(user1.apiKey, {name: 'my playlist'});
			playlistid = ret.body.id;
			await tracks.reduce((p: Promise<any>, track: track) => {
				return p.then(async () => {
					let ret = await addTrack(user1.apiKey, playlistid, track.serviceid);
					track.id = ret.body.id;
				});
			}, Promise.resolve());
			ret = await getPlaylist(user1.apiKey, playlistid);
		});
		
		it('Should move the track to the new index', async function() {
			let ret = await moveTrack(user1.apiKey, playlistid, tracks[0].id, 2);
			expect(ret.status).equal(ResponseStatusTypes.OK);
			expect(ret.body.tracks.map(x => x.serviceid)).to.have.ordered.members(['B', 'C', 'A']);
			ret = await moveTrack(user1.apiKey, playlistid, tracks[0].id, 0);
		});

		it('Should move the track to end of list is index bigger than array', async function() {
			let ret = await moveTrack(user1.apiKey, playlistid, tracks[1].id, 5)
			expect(ret.status).equal(ResponseStatusTypes.OK);
			expect(ret.body.tracks.map(x => x.serviceid)).to.have.ordered.members(['A', 'C', 'B']);
			ret = await moveTrack(user1.apiKey, playlistid, tracks[1].id, 1);
		});

		it('A user who\'s not a guest or editor can move a track', async function() {
			let serviceid: string = 'CCCC';
			ret = await addTrack(user3.apiKey, playlistid, serviceid);
			let trackid: string = ret.body.id;
			ret = await moveTrack(user3.apiKey, playlistid, trackid, 2);
			expect(ret.status).equals(ResponseStatusTypes.OK);
		});

		describe('Should fail to move the track if ...', function() {
			it('apiKey is not provided', async function() {
				let ret = await chai.request(app.app)
					.put(`/playlists/${playlistid}/tracks/${tracks[0].id}/move`)
					.send({index: 2})
				expect(ret.status).equals(ResponseStatusTypes.BAD_REQUEST);
			});
			it('apiKey is invalid', async function() {
				let ret = await moveTrack('0123456789', playlistid, tracks[0].id, 2)
				expect(ret.status).equals(ResponseStatusTypes.UNAUTHORIZED);
			});
			it('playlistid is not prodived', async function() {
				let ret = await chai.request(app.app)
					.put(`/playlists/${playlistid}/tracks/${tracks[0].id}?apiKey=${user1.apiKey}/move`)
					.send({index: 2})
				expect(ret.status).equals(ResponseStatusTypes.NOT_FOUND);
			});
			it('playlist does not exist', async function() {
				let ret = await chai.request(app.app)
					.put(`/playlists/tracks/${tracks[0].id}?apiKey=${user1.apiKey}/move`)
					.send({index: 2})
				expect(ret.status).equals(ResponseStatusTypes.NOT_FOUND);
			});
			it('user is not playlist owner or editor (editing = PRIVATE)', async function() {
				await updatePlaylist(user1.apiKey, playlistid, {editing: 'PRIVATE'})
				let ret = await moveTrack(user2.apiKey, playlistid, tracks[0].id, 2);
				expect(ret.status).equals(ResponseStatusTypes.UNAUTHORIZED);
				await updatePlaylist(user1.apiKey, playlistid, {editing: 'PUBLIC'});
			});
			it('track does not exist', async function() {
				let ret = await moveTrack(user2.apiKey, playlistid, '0123456789', 2);
				expect(ret.status).equals(ResponseStatusTypes.NOT_FOUND);
			});
			it('index is not a number', async function() {
				let ret = await moveTrack(user2.apiKey, playlistid, tracks[0].id, 'iamanindex');
				expect(ret.status).equals(ResponseStatusTypes.BAD_REQUEST);
			});
			it('index is a float', async function() {
				let ret = await moveTrack(user2.apiKey, playlistid, tracks[0].id, 1.234);
				expect(ret.status).equals(ResponseStatusTypes.BAD_REQUEST);
			});
			it('index is negative', async function() {
				let ret = await moveTrack(user2.apiKey, playlistid, tracks[0].id, -1);
				expect(ret.status).equals(ResponseStatusTypes.BAD_REQUEST);
			});
		});
	});
	
	describe('Update Playlist', function() {
		const name: string = 'My birthday playlist';
		let playlistid: string;
		const PUBLIC = 'PUBLIC';
		const PRIVATE = 'PRIVATE';
		const MOVE = 'move';
		const VOTE = 'vote';
		let newName: string = 'new playlist name';

		before(async function() {
			await reset()
			let ret: any = await Promise.all(users.map(createUser));
			[user1, user2, user3] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});	
			ret = await createPlaylist(user1.apiKey, {name});
			playlistid = ret.body.id;
		});

		it('Should update playlist name', async function() {
			let ret = await updatePlaylist(user1.apiKey, playlistid, {name: newName});
			expect(ret.status).equals(ResponseStatusTypes.OK);
			ret = await getPlaylist(user1.apiKey, playlistid);
			expect(ret.body.name).equals(newName);
		});
		it('Should update playlist visibility and editing to PRIVATE if setting visibility to PRIVATE', async function() {
			let ret = await updatePlaylist(user1.apiKey, playlistid, {visibility: PRIVATE});
			expect(ret.status).equals(ResponseStatusTypes.OK);
			ret = await getPlaylist(user1.apiKey, playlistid);
			expect(ret.body.visibility).equals(PRIVATE);
			expect(ret.body.editing).equals(PRIVATE);
		});
		it('Should update playlist visibility to PUBLIC and leave editing to PRIVATE', async function() {
			let ret = await updatePlaylist(user1.apiKey, playlistid, {editing: PRIVATE, visibility: PRIVATE}); // set both to PRIVATE first
			ret = await updatePlaylist(user1.apiKey, playlistid, {visibility: PUBLIC});
			expect(ret.status).equals(ResponseStatusTypes.OK);
			ret = await getPlaylist(user1.apiKey, playlistid);
			expect(ret.body.visibility).equals(PUBLIC);
			expect(ret.body.editing).equals(PRIVATE);
		});
		it('Should update playlist editing to PRIVATE and leave visibility to PUBLIC', async function() {
			let ret = await updatePlaylist(user1.apiKey, playlistid, {editing: PUBLIC, visibility: PUBLIC}); // set both to PUBLIC first
			ret = await updatePlaylist(user1.apiKey, playlistid, {editing: PRIVATE});
			expect(ret.status).equals(ResponseStatusTypes.OK);
			ret = await getPlaylist(user1.apiKey, playlistid);
			expect(ret.body.visibility).equals(PUBLIC);
			expect(ret.body.editing).equals(PRIVATE);
		});
		it('Should update playlist editing and visibility to PUBLIC if editing is set to PUBLIC', async function() {
			let ret = await updatePlaylist(user1.apiKey, playlistid, {editing: PRIVATE, visibility: PRIVATE}); // set both to PRIVATE first
			ret = await updatePlaylist(user1.apiKey, playlistid, {editing: PUBLIC});
			expect(ret.status).equals(ResponseStatusTypes.OK);
			ret = await getPlaylist(user1.apiKey, playlistid);
			expect(ret.body.visibility).equals(PUBLIC);
			expect(ret.body.editing).equals(PUBLIC);
		});
		it('Should update playlist sortingType', async function() {
			let ret = await updatePlaylist(user1.apiKey, playlistid, {sortingType: VOTE});
			expect(ret.status).equals(ResponseStatusTypes.OK);
			ret = await getPlaylist(user1.apiKey, playlistid);
			expect(ret.body.sortingType).equals(VOTE);
		});
		it('Should update many settings at once', async function() {
			let ret = await updatePlaylist(user1.apiKey, playlistid, {sortingType: VOTE, name: newName, visibility: PRIVATE, editing: PRIVATE});
			expect(ret.status).equals(ResponseStatusTypes.OK);
			ret = await getPlaylist(user1.apiKey, playlistid);
			expect(ret.body.sortingType).equals(VOTE);
			expect(ret.body.visibility).equals(PRIVATE);
			expect(ret.body.editing).equals(PRIVATE);
			expect(ret.body.name).equals(newName);
		});
		it('Should ignore other settings', async function() {
			let ret = await updatePlaylist(user1.apiKey, playlistid, {lalala: 'toto'});
			expect(ret.status).equals(ResponseStatusTypes.OK);
			ret = await getPlaylist(user1.apiKey, playlistid);
			expect(ret.body).not.haveOwnProperty('lalala');
		});

		describe('Should fail to update if ... ', function() {
			let name: string = 'cool awesome playlist';
			afterEach(async function() {
				ret = await getPlaylist(user1.apiKey, playlistid);
				expect(ret.body.name).not.equal(name);
			});
			it('apiKey is not provided', async function() {
				ret = await chai.request(app.app)
					.put(`/playlists/${playlistid}`)
					.send({name});
				expect(ret.status).equals(ResponseStatusTypes.BAD_REQUEST);
			});
			it('apiKey is invalid', async function() {
				ret = await updatePlaylist('012456789', playlistid, {name});
				expect(ret.status).equals(ResponseStatusTypes.UNAUTHORIZED);
			});
			it('playlistid is not prodived', async function() {
				ret = await chai.request(app.app)
					.put(`/playlists?apiKey=${user1.apiKey}`)
					.send({name});
				expect(ret.status).equals(ResponseStatusTypes.NOT_FOUND);
			});
			it('playlist does not exist', async function() {
				ret = await updatePlaylist(user1.apiKey, '0123456789', {name});
				expect(ret.status).equals(ResponseStatusTypes.NOT_FOUND);
			});
			it('user is not playlist owner', async function() {
				ret = await updatePlaylist(user2.apiKey, playlistid, {name});
				expect(ret.status).equals(ResponseStatusTypes.UNAUTHORIZED);
			});
			it('name invalid', async function() {
				ret = await Promise.all([{test: 'testname'}, '', new Array(100).join('0')].map(name => updatePlaylist(user1.apiKey, playlistid, {name})));
				ret.forEach(r => expect(r.status).not.equals(ResponseStatusTypes.OK));
			});
			it('visibility invalid', async function() {
				ret = await updatePlaylist(user2.apiKey, playlistid, {visibility: 'INVALID'});
				expect(ret.status).equals(ResponseStatusTypes.BAD_REQUEST);
			});
			it('editing invalid', async function() {
				ret = await updatePlaylist(user2.apiKey, playlistid, {editing: 'INVALID'});
				expect(ret.status).equals(ResponseStatusTypes.BAD_REQUEST);
			});
			it('sortingType invalid', async function() {
				ret = await updatePlaylist(user2.apiKey, playlistid, {sortingType: 'notvoting'});
				expect(ret.status).equals(ResponseStatusTypes.BAD_REQUEST);
			});
		});
	});

	describe('Duplicate Playlist', function() {
		let playlistid: string;
		let newPlaylistid: string;
		let name: string = 'playlistname';
		let owner: user, editor: user, guest: user, other: user;
		const trackServiceIds: string[] = ['0', '1'];
		let playlist;

		beforeEach(async function() {
			await reset();
			let ret: any = await Promise.all(users.map(createUser));
			[owner, editor, guest, other] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});	
			ret = await createPlaylist(owner.apiKey, {name});
			playlistid = ret.body.id;
			ret = await Promise.all([
				addEditorToPlaylist(owner.apiKey, playlistid, editor.id),
				addGuestToPlaylist(owner.apiKey, playlistid, guest.id),
				updatePlaylist(owner.apiKey, playlistid, {visibility: 'PRIVATE'}),
				...trackServiceIds.map((serviceid: string) => addTrack(owner.apiKey, playlistid, serviceid))
			]);
			ret.forEach(r => expect(r.status).equals(ResponseStatusTypes.OK));
			playlist = (await getPlaylist(owner.apiKey, playlistid)).body; 
		});

		it('Should duplicate playlist if user is owner of playlist', async function() {
			ret = await duplicatePlaylist(owner.apiKey, playlistid);
			expect(ret.status).equals(ResponseStatusTypes.OK);
			newPlaylistid = ret.body.id;
			ret = await getPlaylist(owner.apiKey, newPlaylistid);
			expect(ret.status).equals(ResponseStatusTypes.OK);
			expect(ret.body.owner.id).equals(owner.id);
			expect(ret.body.tracks).deep.equals(playlist.tracks);
			expect(ret.body.guests).length(0);
			expect(ret.body.editors).length(0);
		});
		it('Should duplicate playlist if user can access playlist', async function() {
			ret = await duplicatePlaylist(guest.apiKey, playlistid);
			expect(ret.status).equals(ResponseStatusTypes.OK);
			newPlaylistid = ret.body.id;
			ret = await getPlaylist(guest.apiKey, newPlaylistid);
			expect(ret.status).equals(ResponseStatusTypes.OK);
			expect(ret.body.owner.id).equals(guest.id);
			expect(ret.body.tracks).deep.equals(playlist.tracks);
			expect(ret.body.guests).length(0);
			expect(ret.body.editors).length(0);
		});
		it('Should copy over guests if user is owner', async function() {
			ret = await duplicatePlaylist(owner.apiKey, playlistid, {copyGuests: true});
			expect(ret.status).equals(ResponseStatusTypes.OK);
			newPlaylistid = ret.body.id;
			ret = await getPlaylist(owner.apiKey, newPlaylistid);
			expect(ret.status).equals(ResponseStatusTypes.OK);
			expect(ret.body.owner.id).equals(owner.id);
			expect(ret.body.tracks).deep.equals(playlist.tracks);
			expect(ret.body.guests).deep.equals(playlist.guests);
			expect(ret.body.editors).length(0);
		});
		it('Should copy over editors if user is owner', async function() {
			ret = await duplicatePlaylist(owner.apiKey, playlistid, {copyEditors: true});
			expect(ret.status).equals(ResponseStatusTypes.OK);
			newPlaylistid = ret.body.id;
			ret = await getPlaylist(owner.apiKey, newPlaylistid);
			expect(ret.status).equals(ResponseStatusTypes.OK);
			expect(ret.body.owner.id).equals(owner.id);
			expect(ret.body.tracks).deep.equals(playlist.tracks);
			expect(ret.body.guests).deep.equals(playlist.editors);
			expect(ret.body.editors).deep.equals(playlist.editors);
		});
		it('Should not copy guests id user is no owner', async function() {
			ret = await duplicatePlaylist(guest.apiKey, playlistid, {copyGuests: true});
			expect(ret.status).equals(ResponseStatusTypes.OK);
			newPlaylistid = ret.body.id;
			ret = await getPlaylist(guest.apiKey, newPlaylistid);
			expect(ret.status).equals(ResponseStatusTypes.OK);
			expect(ret.body.owner.id).equals(guest.id);
			expect(ret.body.tracks).deep.equals(playlist.tracks);
			expect(ret.body.guests).length(0);
			expect(ret.body.editors).length(0);
		});		
		it('Should not copy editors id user is no owner', async function() {
			ret = await duplicatePlaylist(guest.apiKey, playlistid, {copyEditors: true});
			expect(ret.status).equals(ResponseStatusTypes.OK);
			newPlaylistid = ret.body.id;
			ret = await getPlaylist(guest.apiKey, newPlaylistid);
			expect(ret.status).equals(ResponseStatusTypes.OK);
			expect(ret.body.owner.id).equals(guest.id);
			expect(ret.body.tracks).deep.equals(playlist.tracks);
			expect(ret.body.guests).length(0);
			expect(ret.body.editors).length(0);
		});				

		describe('Should fail to duplicate playlist if ... ', function() {
			it('User cannot access playlist', async function() {
				ret = await duplicatePlaylist(other.apiKey, playlistid);
				expect(ret.status).equals(ResponseStatusTypes.UNAUTHORIZED);
				ret = await getUser(other.apiKey, other.id);
				expect(ret.body.playlists).length(0);
			});
			it('apiKey not provided', async function() {
				ret = await chai.request(app.app)
					.post(`/playlists/${playlistid}`)
					.send();
				expect(ret.status).equals(ResponseStatusTypes.BAD_REQUEST);
			});
			it('apiKey invalid', async function() {
				ret = await duplicatePlaylist('0123456789', playlistid);
				expect(ret.status).equals(ResponseStatusTypes.UNAUTHORIZED);
			});
			it('playlistid invalid', async function() {
				ret = await duplicatePlaylist(owner.apiKey, '0123456789');
				expect(ret.status).equals(ResponseStatusTypes.NOT_FOUND);
			});
		});	

	});

	describe('Playlist Socket Updates', function() {
		let playlistid: string;
		let socket: SocketIOClient.Socket;
		let owner: user;
		let editor: user;
		let guest: user;
		let other: user;
		
		describe(SocketActions.CONNECTED, function() {
			let owner: any = {};			
			let tracks: any[] = [{serviceid: 'A'}, {serviceid: 'B'}, {serviceid: 'C'}];
			before(async function() {
				await reset();
				let ret: any = await Promise.all(users.map(createUser));
				ret.map(r => {
					expect(r.status).equals(ResponseStatusTypes.OK);
				});
				[owner, editor, guest, other] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});	
				ret = await createPlaylist(owner.apiKey, {name: 'my playlist'});
				playlistid = ret.body.id;
				// add three tracks
				tracks = await Promise.all(tracks.map(async track => {
					ret = await addTrack(owner.apiKey, playlistid, track.serviceid);
					expect(ret.status).equals(ResponseStatusTypes.OK);
					return {serviceid: ret.body.serviceid, id: ret.body.id};
				}));
				ret = await Promise.all([
					addEditorToPlaylist(owner.apiKey, playlistid, editor.id),
					addGuestToPlaylist(owner.apiKey, playlistid, guest.id),
					updatePlaylist(owner.apiKey, playlistid, {visibility: 'PRIVATE'})
				]);
				ret.map(r => expect(r.status).equals(ResponseStatusTypes.OK));
			});
			afterEach(async function() {
				let disconnected = new Promise(resolve => {
					if (!socket) resolve(); 
					if (!socket.disconnected) {
						socket.on('disconnect', () => resolve());
						socket.emit(SocketActions.REQUEST_DISCONNECT);
					} else {
						resolve();
					}
				});
				await disconnected;
				expect(socketController.connections).length(0);
			});
			it('Should connect', async function() {
				socket = playlistSocketConnect(owner.apiKey, playlistid);
				let promises = [
					new Promise((resolve) => {
						socket.on(SocketActions.CONNECTED, () => {
							resolve(true);
						});
					}),
					new Promise((resolve) => {
						socket.on('connect', () => {
							resolve(true);
						});
					})
				];
				socket.connect();
				await Promise.all(promises);
			});
			describe('Should fail to connect if ...', function() {
				it('playlistid not provided', function(done) {
					socket = socketio('http://localhost:3030/playlists', {query: {apiKey: owner.apiKey}});
					socket.on(SocketActions.CONNECTED, async function (data) {
						done(new Error('successfully connected'));
					});
					socket.on(SocketActions.ERROR, function () {
						done();
					});
				});
				it('apiKey not provided', function(done) {
					socket = socketio('http://localhost:3030/playlists', {query: {playlistid: playlistid}});
					socket.on(SocketActions.CONNECTED, async function (data) {
						done(new Error('successfully connected'));
					});
					socket.on(SocketActions.ERROR, function () {
						done();
					});
				});
				it('playlist does not exist', function(done) {
					socket = socketio('http://localhost:3030/playlists', {query: {playlistid: '0123456789', apiKey: owner.apiKey}});
					socket.on(SocketActions.CONNECTED, async function (data) {
						done(new Error('successfully connected'));
					});
					socket.on(SocketActions.ERROR, function () {
						done();
					});
				});
				it('apiKey does not exist', function(done) {
					socket = socketio('http://localhost:3030/playlists', {query: {playlistid: playlistid, apiKey: '0123456789'}});
					socket.on(SocketActions.CONNECTED, async function (data) {
						done(new Error('successfully connected'));
					});
					socket.on(SocketActions.ERROR, function () {
						done();
					});
				});
				it('user does not have rights to access the playlist (other)', function(done) {
					socket = socketio('http://localhost:3030/playlists', {autoConnect: false, query: {playlistid: playlistid, apiKey: other.apiKey}});
					socket.on(SocketActions.CONNECTED, async function (data) {
						done(new Error('successfully connected'));
					});
					socket.on(SocketActions.ERROR, function () {
						done();
					});
					socket.connect();
				});
			});
		});

		describe('Update', function() {
			let trackids: string[];
			let ownerSocket: SocketIOClient.Socket;
			let editorSocket: SocketIOClient.Socket;
			let guestSocket: SocketIOClient.Socket;
			let sockets: SocketIOClient.Socket[];
			let ret: any
			let newTrackServiceId: string = '0123';

			beforeEach(async function() {
				await reset();
				let ret: any = await Promise.all(users.map(createUser));
				[owner, editor, guest, other] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});	
				ret = await createPlaylist(owner.apiKey, {name: 'my playlist'});
				playlistid = ret.body.id;
				// add three tracks
				trackids = await Promise.all(['1', '2', '3'].map(async serviceid => {
					ret = await addTrack(owner.apiKey, playlistid, serviceid);
					expect(ret.status).equals(ResponseStatusTypes.OK);
					return ret.body.id;
				}));
				ret = await Promise.all([
					addEditorToPlaylist(owner.apiKey, playlistid, editor.id),
					addGuestToPlaylist(owner.apiKey, playlistid, guest.id),
					updatePlaylist(owner.apiKey, playlistid, {visibility: 'PRIVATE'})
				]);
				
				sockets = [ownerSocket, editorSocket, guestSocket] = await Promise.all([owner, editor, guest].map(async user => await playlistSocketConnect(user.apiKey, playlistid)));
				await Promise.all(sockets.map(socket => new Promise((resolve, reject) => {
					socket.on(SocketActions.CONNECTED, function() {
						resolve();
					});
					socket.on('error', function() {
						console.log('error');
						reject();
					});
					socket.on(SocketActions.ERROR, function(err) {
						console.log('ERROR', err);
						reject(err);
					});
					socket.connect();
				})));
			});

			afterEach(async function() {
				await new Promise(resolve => setTimeout(() => resolve(), 1000));
				let disconnected = [...sockets, socket].map(socket => new Promise(resolve => {
					if (!socket) {
						resolve(); 
					}
					socket.on('disconnect', () => {
						resolve();
					});
					if (!socket.disconnected) {
						socket.emit(SocketActions.REQUEST_DISCONNECT);
					} else {
						resolve();
					}
				}));
				await Promise.all(disconnected);
				expect(socketController.connections).length(0);				
			});

			describe(SocketActions.PLAYLIST_UPDATE, function() {
				it('Should send update to owner, editor and guest', async function() {
					let gotUpdates = sockets.map(socket => new Promise((resolve, reject) => {
						socket.on(SocketActions.PLAYLIST_UPDATE, function (data) {
							resolve(true)
						});
						socket.on(SocketActions.ERROR, function (data) {
							reject(new Error(data));
						});		
					}));
					await updatePlaylist(owner.apiKey, playlistid, {sortingType: 'move'});
					ret = await Promise.all(gotUpdates);
					expect(ret).satisfies(ret => ret.every(x => x == true));
					await updatePlaylist(owner.apiKey, playlistid, {sortingType: 'vote'});
				});
			});
			describe(SocketActions.PLAYLIST_ADD_TRACK, function() {
				it('Should send update to owner, editor and guest', async function() {
					let gotUpdates = sockets.map(socket => new Promise((resolve, reject) => {
						socket.on(SocketActions.PLAYLIST_ADD_TRACK, function (data) {
							resolve(true)
						});
						socket.on(SocketActions.ERROR, function (data) {
							reject(new Error(data));
						});		
					}));
					let newTrackId = (await addTrack(owner.apiKey, playlistid, newTrackServiceId)).body.id;
					ret = await Promise.all(gotUpdates);
					expect(ret).satisfies(ret => ret.every(x => x == true));
					await removeTrack(owner.apiKey, playlistid, newTrackId);
				});
			});
			describe(SocketActions.PLAYLIST_REMOVE_TRACK, function() {
				let newTrackId: string;
				it('Should send update to owner, editor and guest', async function() {
					newTrackId = (await addTrack(owner.apiKey, playlistid, newTrackServiceId)).body.id;
					let gotUpdates = sockets.map(socket => new Promise((resolve, reject) => {
						socket.on(SocketActions.PLAYLIST_REMOVE_TRACK, function (data) {
							resolve(true)
						});
						socket.on(SocketActions.ERROR, function (data) {
							reject(new Error(data));
						});		
					}));
					await removeTrack(owner.apiKey, playlistid, newTrackId);
					ret = await Promise.all(gotUpdates);
					expect(ret).satisfies(ret => ret.every(x => x == true));
				});
			});
			describe(SocketActions.PLAYLIST_MOVE_TRACK, function() {
				it('Should send update to owner, editor and guest', async function() {
					ret = [];
					let gotEvent = sockets.map(socket => new Promise((resolve, reject) => {
						socket.on(SocketActions.PLAYLIST_MOVE_TRACK, function (data) {
							resolve(true)
						});
						socket.on(SocketActions.ERROR, function (data) {
							reject(new Error(data));
						});
					}));
					ret = await moveTrack(owner.apiKey, playlistid, trackids[0], 2);
					ret = await Promise.all(gotEvent);
					expect(ret).satisfies(ret => ret.every(x => x == true));
				});
			});
			describe(SocketActions.PLAYLIST_REMOVE_EDITOR, function() {
				it('Should send update to owner, editor and guest and disconnect the editor', async function() {
					let gotUpdates = sockets.map(socket => new Promise((resolve, reject) => {
						socket.on(SocketActions.PLAYLIST_REMOVE_EDITOR, function (data) {
							resolve(true)
						});
						socket.on(SocketActions.ERROR, function (data) {
							reject(new Error(data));
						});
					}));
					await removeEditorFromPlaylist(owner.apiKey, playlistid, editor.id);
					ret = await Promise.all(gotUpdates);
					expect(ret).satisfies(ret => ret.every(x => x == true));
				});
			});
			describe(SocketActions.PLAYLIST_REMOVE_GUEST, function() {
				it('Should send update to owner, editor and guest and disconnect the guest', async function() {
					let gotUpdates = sockets.map(socket => new Promise((resolve, reject) => {
						if (socket == guestSocket) {
							socket.on('disconnect', function (data) {
								resolve(true)
							});
						} else {
							socket.on(SocketActions.PLAYLIST_REMOVE_GUEST, function (data) {
								resolve(true)
							});
						}
						socket.on(SocketActions.ERROR, function (data) {
							reject(new Error(data));
						});
					}));
					await removeGuestFromPlaylist(owner.apiKey, playlistid, guest.id);
					ret = await Promise.all(gotUpdates);
					expect(ret).satisfies(ret => ret.every(x => x == true));
				});
			});
			describe(SocketActions.PLAYLIST_DELETED, function() {
				it('Should disconnect everyone', async function() {
					let gotUpdates = sockets.map(socket => new Promise((resolve, reject) => {
						socket.on('disconnect', function (data) {
							resolve(true)
						});
						socket.on(SocketActions.ERROR, function (data) {
							reject(new Error(data));
						});
					}));
					await deletePlaylist(owner.apiKey, playlistid);
					ret = await Promise.all(gotUpdates);
					expect(ret).satisfies(ret => ret.every(x => x == true));
				});
			});
			
		});
	});

	describe('Playlist Search', function() {
		let name: string = 'supercomplicatedname';
		let playlists: any[] = [];

		before(async function() {
			await reset();
			ret = await createUser(users[0]);
			user1 = {apiKey: ret.body.apiKey, id: ret.body.id};
			ret = await Promise.all('ABCDE'.split('').map(x => createPlaylist(user1.apiKey, {name: x + x + x + x})));
			playlists = ret.map(r => r.body);
		});
		
		it('Should find playlist by ID', async function() {
			ret = await searchPlaylist(user1.apiKey, playlists[1].id);
			expect(ret.body.results[0].id).equals(playlists[1].id);
		});
		it('Should find playlist by ownerid', async function() {
			ret = await searchPlaylist(user1.apiKey, user1.id);
			expect(ret.body.results).length(playlists.length);
		});
		it('Should find playlist by name', async function() {
			ret = await searchPlaylist(user1.apiKey, 'AAAA');
			expect(ret.body.results[0].name).equals('AAAA');
		});
		it(`Should limit results to limit set in config: ${search_limit}`, async function() {
			ret = await Promise.all('ABCDEFGHSLDKFJLKJ'.split('').map(x => createPlaylist(user1.apiKey, {name})));
			ret = await searchPlaylist(user1.apiKey, name);
			expect(ret.body.results.length).lte(search_limit);
		});
		it('Should not return playlists whose visibility are set to private', async function() {
			let name: string = 'private playlist';
			await updatePlaylist(user1.apiKey, playlists[0].id, {visibility: 'PRIVATE', name});
			ret = await searchPlaylist(user1.apiKey, name);
			expect(ret.body.results).length(0);
		});
	});
});