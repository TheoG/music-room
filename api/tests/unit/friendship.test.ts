import * as chai from 'chai';
import chaiHttp = require('chai-http');
chai.use(chaiHttp);
const expect = chai.expect;

import { reset, users, createUser, getFriendship, getFriendships, requestFriendship, updateFriendship, deleteFriendship, getUser } from './common';
import app from "../../src/app";
import ResponseStatusTypes from "../../src/utils/ResponseStatusTypes";

interface user {
	apiKey: string;
	id: string;
}

describe('Friendship', function() {
	this.timeout(10000);
	let ret: any;
	let user1: user, user2: user, user3: user;
	let friendshipId: string;

	describe('Create friendship', function() {
		before(async function() {
			await reset();
			ret = await Promise.all(users.map(createUser));
			[user1, user2, user3] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});	
		});

		it('Should create a friendship', async function() {
			ret = await requestFriendship(user1.apiKey, user2.id);
			expect(ret.status).equal(ResponseStatusTypes.OK);
			expect(ret.body.id).exist;
			friendshipId = ret.body.id;
			ret = await getFriendships(user1.apiKey);
			expect(ret.status).equals(ResponseStatusTypes.OK);
			expect(ret.body.friendships[0]).exist;
			expect(ret.body.friendships[0].id).exist;
			expect(ret.body.friendships[0].pending).equals(true);
			expect(ret.body.friendships[0].request.to).equals(user2.id);
			expect(ret.body.friendships[0].request.from).equals(user1.id);
			expect(ret.body.friendships[0].users.length).equals(2);
		});

		describe('Should fail to create a friendship if ...', function() {
			it('apiKey was not provided', async function() {
				ret = await chai.request(app.app)
					.post(`/friendships?id=${user1.id}`)
					.send({id: user2.id, message: 'Please be my friend'});
				expect(ret.status).equal(ResponseStatusTypes.BAD_REQUEST);
			});
			it('apiKey is invalid', async function() {
				ret = await requestFriendship('myawesomeapikey', user2.id);
				expect(ret.status).equal(ResponseStatusTypes.UNAUTHORIZED);
			});
			it('friend id was not provided', async function() {
				ret = await chai.request(app.app)
					.post(`/friendships?apiKey=${user1.apiKey}`)
					.send({message: 'Please be my friend'});
				expect(ret.status).equal(ResponseStatusTypes.BAD_REQUEST);
			});
			it('friend id is invalid', async function() {
				ret = await requestFriendship(user1.apiKey, 'awesomefriendid');
				expect(ret.status).equal(ResponseStatusTypes.NOT_FOUND);
			});
			it('no message is provided', async function() {
				ret = await chai.request(app.app)
					.post(`/friendships?apiKey=${user1.apiKey}`)
					.send({id: user2.id});
				expect(ret.status).equal(ResponseStatusTypes.BAD_REQUEST);
			});
			it('friendship between users already exists', async function() {
				ret = await requestFriendship(user1.apiKey, user2.id);
				ret = await requestFriendship(user1.apiKey, user2.id);
				expect(ret.status).equal(ResponseStatusTypes.BAD_REQUEST);
				ret = await requestFriendship(user2.apiKey, user1.id);
				expect(ret.status).equal(ResponseStatusTypes.BAD_REQUEST);
			});
		})
	});

	describe('Update friendship', function() {
		beforeEach(async function () {
			await reset();
			ret = await Promise.all(users.map(createUser));
			[user1, user2, user3] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});	
			ret = await requestFriendship(user1.apiKey, user2.id);
			friendshipId = ret.body.id;
		});

		it('Should set friendship as read', async function() {
			ret = await updateFriendship(user2.apiKey, friendshipId, 'read');
			expect(ret.status).equal(ResponseStatusTypes.OK);
			expect(ret.body.request.read).equal(true);
		});
		it('Should accept friendship, delete request, and set users as friends', async function() {
			ret = await updateFriendship(user2.apiKey, friendshipId, 'accept');
			expect(ret.status).equal(ResponseStatusTypes.OK);
			expect(ret.body.acceptedOn).exist;
			expect(ret.body.request).equal(null);
			expect(ret.body.pending).equal(false);
			ret = await getFriendship(friendshipId, user1.apiKey);
			expect(ret.body.users[0].friends[0]).equal(ret.body.users[1].id);
			expect(ret.body.users[1].friends[0]).equal(ret.body.users[0].id);
			ret = await Promise.all([getUser(user1.apiKey, user1.id), getUser(user2.apiKey, user2.id)]);
			expect(ret[0].body.friends).include(user2.id);
			expect(ret[1].body.friends).include(user1.id);		
		});
		it('Should reject friendship (and delete it)', async function() {
			ret = await updateFriendship(user2.apiKey, friendshipId, 'reject');
			expect(ret.status).equal(ResponseStatusTypes.OK);
			ret = await getFriendship(friendshipId, user1.apiKey);
			expect(ret.status).equal(ResponseStatusTypes.NOT_FOUND);
		});

		describe('Should fail to update friendship if ...', function() {
			beforeEach(async function() {
				await reset();
				ret = await Promise.all(users.map(createUser));
				[user1, user2, user3] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});
				ret = await requestFriendship(user1.apiKey, user2.id);
				friendshipId = ret.body.id;
			});
			it('apiKey was not provided', async function() {
				ret = await chai.request(app.app)
					.put(`/friendships/${friendshipId}/accept`)
					.send();
				expect(ret.status).equal(ResponseStatusTypes.BAD_REQUEST);
			});
			it('apiKey was invalid', async function() {
				ret = await updateFriendship('123456789', friendshipId, 'accept');
				expect(ret.status).equal(ResponseStatusTypes.UNAUTHORIZED);
			});
			it('friendship id was not provided', async function() {
				ret = await chai.request(app.app)
					.put(`/friendships/accept?apiKey=${user2.apiKey}`)
					.send();
				expect(ret.status).equal(ResponseStatusTypes.NOT_FOUND);
			});
			it('friendship id is invalid', async function() {
				ret = await updateFriendship(user2.apiKey, '123456789', 'accept');
				expect(ret.status).equal(ResponseStatusTypes.NOT_FOUND);
			});
			it('wrong user asked for update (one who requested)', async function() {
				ret = await updateFriendship(user1.apiKey, friendshipId, 'accept');
				expect(ret.status).equal(ResponseStatusTypes.UNAUTHORIZED);
			});
			it('wrong user asked for update (one not part of friendship)', async function() {
				ret = await updateFriendship(user3.apiKey, friendshipId, 'accept');
				expect(ret.status).equal(ResponseStatusTypes.UNAUTHORIZED);
			});
			it('friendship request is no longer pending', async function() {
				ret = await updateFriendship(user2.apiKey, friendshipId, 'accept');
				ret = await updateFriendship(user2.apiKey, friendshipId, 'accept');
				expect(ret.status).equal(ResponseStatusTypes.BAD_REQUEST);
			});
			it('friendship action is invalid', async function() {
				ret = await chai.request(app.app)
					.put(`/friendships/${friendshipId}/myawesomeaction?apiKey=${user2.apiKey}&id=${user2.id}`)
					.send();
				expect(ret.status).equal(ResponseStatusTypes.BAD_REQUEST);
			});
		});
	});

	describe('Delete friendship', function() {	
		beforeEach(async function() {
			await reset();
			ret = await Promise.all(users.map(createUser));
			[user1, user2, user3] = ret.map(r => {return {apiKey: r.body.apiKey, id: r.body.id}});	
			ret = await requestFriendship(user1.apiKey, user2.id);
			friendshipId = ret.body.id;
		});

		describe('Should delete the friendship and friend from friend list if ... ', function() {
			it('the friendship was not yet accepted (user1)', async function() {
				ret = await deleteFriendship(user1.apiKey, friendshipId);
				expect(ret.status).equal(ResponseStatusTypes.OK);
				ret = await getFriendship(friendshipId, user1.apiKey);
				expect(ret.status).equals(ResponseStatusTypes.NOT_FOUND);
			});
			it('the friendship was accpeted (user1)', async function() {
				ret = await updateFriendship(user2.apiKey, friendshipId, 'accept');
				ret = await deleteFriendship(user1.apiKey, friendshipId);
				expect(ret.status).equal(ResponseStatusTypes.OK);
				ret = await getFriendship(friendshipId, user1.apiKey);
				expect(ret.status).equals(ResponseStatusTypes.NOT_FOUND);
			});
		})

		describe('Should fail to delete the friendship if ...', function() {
			it('apiKey is missing', async function() {
				ret = await chai.request(app.app)
					.delete(`/friendships/${friendshipId}`)
					.send();
				expect(ret.status).equal(ResponseStatusTypes.BAD_REQUEST);
			});
			it('apiKey is invalid', async function() {
				ret = await deleteFriendship('0123456789', friendshipId);
				expect(ret.status).equal(ResponseStatusTypes.UNAUTHORIZED);
			});
			it('friendship id is missing', async function() {
				ret = await chai.request(app.app)
					.delete(`/friendships?apiKey=${user1.apiKey}`)
					.send();
				expect(ret.status).equal(ResponseStatusTypes.NOT_FOUND);
			});
			it('friendship does not exist', async function() {
				ret = await deleteFriendship(user1.apiKey, '123456789');
				expect(ret.status).equal(ResponseStatusTypes.NOT_FOUND);
			});
			it('user is not in this friendship', async function() {
				ret = await deleteFriendship(user3.apiKey, friendshipId);
				expect(ret.status).equal(ResponseStatusTypes.UNAUTHORIZED);			
			});
		})
	});
});