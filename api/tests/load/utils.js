'use strict';

var request = require('request');

module.exports = {
	createUsersAndContext,
	clean
};

function randomString(length, isSpaces) {
	let result           = '';
	if (isSpaces) {
		var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz           ';
	} else {
		var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
	}
	let charactersLength = characters.length;
	for (let i = 0; i < length; i++) {
	   result += characters.charAt(Math.floor(Math.random() * charactersLength));
	}
	return result;
}

function createUsersAndContext(context, events, done) {
	context.vars.playlistname = randomString(15, true);
	return Promise.all([...Array(4).keys()].map(() => new Promise((resolve) => {
		request.post('http://localhost:3000/users', {
			headers: {'content-type' : 'application/json'},
			body: JSON.stringify({
				email: randomString(5, false) + '@gmail.com',
				password: 'password'
			})
		}, function(error, httpResponse, body) {
			if (error) {
				console.log('fail ' + httpResponse, error);
				return ;
			}
			try {
				resolve(JSON.parse(body));
			} catch (e) {
				
			}
		});
	}))).then((users) => {
		console.log('start');
		// console.log(users);
		context.vars.users = users;
		// context.vars.it = iterations;
		done();
	});
	
}

function clean(context, events, done) {
	// console.log(context.vars);

	return Promise.all(context.vars.users.map((user) => new Promise((resolve) => {
		request.delete(`http://localhost:3000/users/${user.id}?apiKey=${user.apiKey}`, {
			headers: {'content-type' : 'application/json'}
		}, function(error, httpResponse, body) {
			if (error) {
				console.log('reconnaitre: ', error);
				resolve();
			}
			try {
				resolve(JSON.parse(body));
			} catch (e) {
				console.log('Exception: ', e, body);
				console.log('Error: ', error);
			}
		});
	}))).then((users) => {
		console.log('stop');
		// console.log(users);
		// context.vars.it = iterations;
		done();
	});
}

// knownUsers({vars: {}}, null, () => {});