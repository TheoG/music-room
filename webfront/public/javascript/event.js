// ======= INIT ======
async function init() {
	await eventController.init();
	let ret = await apiController.getApiKey();
	await user.init(ret.userID);
	await deezer.init();
	playlistList.init();
	eventOptionsController.init();

	playerController.init();
	await initSocket(ret);
	// trackListController.init();
	// init	playlistlist
	// init eventoptions
}
$(document).ready(() => init());


// creates single list of tracks of playlist
function getPlaylistTracks(playlist) {
	let tracks = [];
	if (!playlist) {
		//console.log('playlist is null');
		return tracks;
	}
	['tracks', 'playedTracks', 'unplayedTracks'].forEach(attr => {
		if (playlist.hasOwnProperty(attr)) {
			let newTracks = playlist[attr];
			newTracks.map(t => {
				t.type = attr;
				return t;
			});
			tracks = [...tracks, ...newTracks];
		}
	});
	return tracks;
}

// finds track in playlist based on param {id: string, serviceid: string}
function findTrack(param, playlist) {
	let tracks = getPlaylistTracks(playlist);
	if (!tracks || !tracks.length) return;

	return tracks.find(track => {
		if (param.hasOwnProperty('id')) {
			return track.id == param.id;
		} else if (param.hasOwnProperty('serviceid')) {
			return track.serviceid == param.serviceid;
		}
	});
}
