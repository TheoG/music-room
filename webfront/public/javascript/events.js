function loadEvents() {
	$.ajax({
		type: "GET",
		url: "/api/events",
		dataType: "json"
	}).done(function(res) {
		displayEvents(res);
	}).fail(function(xhr, status, errorThrown) {
		console.log("Error: " + errorThrown);
		console.log("Status: " + status);
		console.dir(xhr);
	});
}

function displayEvents(res) {
	$("#eventsList").html("");
	$("#djEventsList").html("");
	$("#guestEventsList").html("");

	if (res.owner.length == 0) {
		$("#eventsList").prev().hide();
		$("#eventsList").hide();
	} else {
		$("#eventsList").prev().show();
		$("#eventsList").show();
	}
	$.each(res.owner, function(i, item) {
		$("#eventsList").append(`<a href="/events/${item.id}" class="eventsListItem grey darken-1 collection-item" id="${item.id}">`
		+ `<span class="white-text">${item.name}</span>`
		+ " <i class='fa fa-trash red-text text-darken-4' aria-hidden='true'></i></a>");
		$(".fa-trash").last().click(delEvent);
	});
	if (res.dj.length == 0) {
		$("#djEventsList").prev().hide();
		$("#djEventsList").hide();
	} else {
		$("#djEventsList").prev().show();
		$("#djEventsList").show();
	}
	$.each(res.dj, function(i, item) {
		$("#djEventsList").append(`<a href="/events/${item.id}" class="eventsListItem grey darken-1 collection-item" id="${item.id}">`
		+ `<span class="white-text">${item.name}</span>`
		+ " <i class='fa fa-trash red-text text-darken-4' aria-hidden='true'></i></a>");
		$(".fa-trash").last().click(removeDj);
	});
	if (res.guest.length == 0) {
		$("#guestEventsList").prev().hide();
		$("#guestEventsList").hide();
	} else {
		$("#guestEventsList").prev().show();
		$("#guestEventsList").show();
	}
	$.each(res.guest, function(i, item) {
		$("#guestEventsList").append(`<a href="/events/${item.id}" class="eventsListItem grey darken-1 collection-item" id="${item.id}">`
		+ `<span class="white-text">${item.name}</span>`
		+ " <i class='fa fa-trash red-text text-darken-4' aria-hidden='true'></i></a>");
		$(".fa-trash").last().click(removeGuest);
	});

	if (!res.guest.length && !res.dj.length && !res.owner.length) {
		$("#noEvents").show();
	} else {
		$("#noEvents").hide();
	}
}

function delEvent(e) {

	e.stopPropagation();
	e.preventDefault();

	var url = "/api/events/" + $(this).closest(".eventsListItem").attr("id");

	$.ajax({
		type: "POST",
		headers: {
			"Content-Type": "application/json",
			"X-HTTP-Method-Override": "DELETE"
		},
		url: url,
		dataType: "json"
	}).done(function(res) {
		// console.log("deleted : ");
		// console.log(res);
		loadEvents();
	}).fail(function(xhr, status, errorThrown) {
		console.log("Error: " + errorThrown);
		console.log("Status: " + status);
		console.dir(xhr);
	});
}

function removeDj(e) {
	e.stopPropagation();
	e.preventDefault();

	var url = "api/events/" + $(this).closest(".eventsListItem").attr("id") + "/djs";

	$.ajax({
		type: "POST",
		headers: {
			"Content-Type": "application/json",
			"X-HTTP-Method-Override": "DELETE"
		},
		url: url,
		dataType: "json"
	}).done(function(res) {
		// console.log("removed dj : ");
		// console.log(res);
		loadEvents();
	}).fail(function(xhr, status, errorThrown) {
		console.log("Error: " + errorThrown);
		console.log("Status: " + status);
		console.dir(xhr);
	});
}

function removeGuest(e) {
	e.stopPropagation();
	e.preventDefault();

	var url = "/api/events/" + $(this).closest(".eventsListItem").attr("id") + "/guests";

	$.ajax({
		type: "POST",
		headers: {
			"Content-Type": "application/json",
			"X-HTTP-Method-Override": "DELETE"
		},
		url: url,
		dataType: "json"
	}).done(function(res) {
		// console.log("removed guest : ");
		// console.log(res);
		loadEvents();
	}).fail(function(xhr, status, errorThrown) {
		console.log("Error: " + errorThrown);
		console.log("Status: " + status);
		console.dir(xhr);
	});
}

$(document).ready(loadEvents);
