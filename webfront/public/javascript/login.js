$(document).ready(() => {

	if (window.notificationMessage) {
		toastr.success(window.notificationMessage);
	}



	$('#loginWithFacebookButton').click(() => {
		FB.login(response => {
			if (response.authResponse) {
				$.ajax({
					type: 'POST',
					url: `api/facebook?accessToken=${response.authResponse.accessToken}`
				}).done(res => {
					location.reload();
				}).fail(error => {
					if (error.responseJSON.errorMessage) {
						toastr.error(error.responseJSON.errorMessage);
					} else if (error.responseJSON.message) {
						toastr.error(error.responseJSON.message);
					}
				});

			} else {
				console.log('User cancelled login or did not fully authorize.');
			}
		}, {
			scope: 'email'
		});
	});
});
