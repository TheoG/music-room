const playlistList = {

	init: () => {
		$("#eventPlaylists").html("<h5>Playlists</h5>");

		// console.log('playlistList init');
		eventController.playlists.forEach(playlist => {
			$("#eventPlaylists").append(`<span class="playlist" id="${playlist.id}">${playlist.name}</span>`);
			$(`#${playlist.id}`).click(function() {
				// console.log('ici');
				var id = $(this).attr('id');
				// console.log(id);
				if (!!id) {
					// console.log(1);
					if (trackListController.playlist.id != id) {
						// console.log({id});
						// console.log(eventController.playlists.map(p => {return {id: p.id, ref: p.refPlaylistId}}));
						let playlist = eventController.playlists.find(p => p.refPlaylistId == id || p.id == id);
						if (playlist) {
							// console.log(3);
							trackListController.playlist = playlist;
							playerController.playlist = playlist;
							trackListController.rebuild();
						}
					}
				}
			});
		});
	}
};
