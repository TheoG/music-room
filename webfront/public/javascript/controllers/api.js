
const apiController = {
	dontdoit: false,
	apiLock: false,
	lock: () => {
		apiController.apiLock = true;
	},
	unlock: () => {
		apiController.apiLock = false;
	},
	play: (trackid = null) => new Promise((resolve, reject) => {
		// console.log('[apiController] play');
		if (apiController.apiLock == true) {
			// reject();
			return ;
		}
		else {
			apiController.lock();
		}

		if (user.isGuest) {
			// reject();
			return ;
		}
		if (!eventController.event.eventActive) {
			// reject();
			return ;
		}
		if (user.isDj) {
			playerController.render.updateTrackInfo(trackid); // TODO ?
		}
		var data = {};
		if (trackid != null) {
			data.trackid = trackid;
		} else {
			data.trackid = playerController.currentTrack.id;
		}
		if (user.isOwner) {
			data.timestamp = new Date().toISOString();
			data.trackPosition = playerController.trackPosition;
			playerController.isPlaying = true;
		}
		//console.log('send req', data)
		$.ajax({
			type: "PUT",
			url: `/api/events/${eventController.event.id}/player/play`,
			data: data,
			dataType: "json"
		}).done(res => {
			apiController.unlock();
			// console.log('api play res : ', res);
			if (user.isOwner) {
				// reset playlist if order has changed
				if (res.hasOwnProperty('id')) {
					// console.log('i is id');
					eventController.init(res);
				}
			}
			// console.log('done init', playerController.playlist);
			resolve(res);
		}).fail(error => {
			apiController.unlock();
			reject();
		});
	}),

	pause: () => {
		//console.log('[apiController] pause');
		if (apiController.apiLock == true) {
			return ;
		}
		else {
			apiController.lock();
		}

		if (user.isGuest) {
			return ;
		}
		if (!eventController.event.eventActive) {
			return ;
		}
		var data = {};
		if (user.isOwner) {
			data.timestamp = new Date().toISOString();
			data.trackPosition = playerController.trackPosition;
			playerController.isPlaying = false;
		}
		//console.log('send req', data)
		$.ajax({
			type: "PUT",
			url: `/api/events/${eventController.event.id}/player/pause`,
			data: data,
			dataType: "json"
		}).done(res => {
			apiController.unlock();
			//console.log('paused', res);
		}).fail(error => {
			apiController.unlock();
			console.log('failed to pause', error);
		});
	},

	seek: (position = null) => {
		//console.log('[apiController] seek');
		if (apiController.apiLock == true) {
			return ;
		}
		else {
			apiController.lock();
		}

		if (user.isGuest) {
			return ;
		}
		if (!eventController.event.eventActive) {
			return ;
		}
		var data = {};
		if (position != null) {
			playerController.trackPosition = position;
		}
		data.trackPosition = playerController.trackPosition;
		if (user.isOwner) {
			data.timestamp = new Date().toISOString();
		}
		$.ajax({
			type: "PUT",
			url: `/api/events/${eventController.event.id}/player/seek`,
			data: data,
			dataType: "json"
		}).done(res => {
			apiController.unlock();
			//console.log('seek', res);
		}).fail(error => {
			apiController.unlock();
		});
	},

	getApiKey: () => new Promise((resolve, reject) => {
		if (apiController.apiLock == true) {
			// reject();
			return ;
		}
		else {
			apiController.lock();
		}

		$.ajax({
			type: "GET",
			url: "/api/getApiKey",
			dataType: "json"
		}).done(res => {
			apiController.unlock();
			resolve(res);
		})
		.fail(function(xhr, status, errorThrown) {
			apiController.unlock();
			console.log("getApiKey Error: " + errorThrown);
			console.log("Status: " + status);
			console.dir(xhr);
			reject();
		});
	}),

	vote: (method, vote, trackid) => {
		if (apiController.apiLock == true) {
			return ;
		}
		else {
			apiController.lock();
		}

		$.ajax({
			type: method,
			url: `/api/events/${eventController.event.id}/${trackid}/${vote}`
		}).done(res => {
			apiController.unlock();
		}).fail(error => {
			apiController.unlock();
			console.log(error);
			toastr.error(error.responseJSON.errorMessage);
		});
	},

	move: (trackid, index) => {
		if (apiController.apiLock == true) {
			return ;
		}
		else {
			apiController.lock();
		}

		$.ajax({
			type: 'PUT',
			url: `/api/events/${eventController.event.id}/${trackid}/move`,
			dataType: 'json',
			data: {
				index: index
			}
		}).done(res => {
			apiController.unlock();
		}).fail(error => {
			apiController.unlock();
			console.log(error);
			toastr.error(error.responseJSON.errorMessage);
		});
	}
};
