const eventOptionsController = {
	init: () => {
		if (user.isOwner) {
			$("#options").append('<input type="button" class="waves-effect waves-light btn" id="toggleEvent" value="' + (eventController.event.eventActive ? 'Stop' : 'Start') + ' event">');
			$("#options").append('<input type="button" class="waves-effect waves-light btn" id="toggleVote" value="' + (eventController.event.votingActive ? 'Stop' : 'Start') + ' vote">');

			$("#toggleEvent").click(() => {
				$.ajax({
					type: "PUT",
					url: `/api/events/${eventController.event.id}/${eventController.event.eventActive ? 'end' : 'start'}event`,
					dataType: "json"
				}).done(res => {
					if (res.eventActive == eventController.event.eventActive) {
						return ;
					}
					if (res.eventActive == false) {
						playerController.pause();
					}
					else {
						playerController.trackPosition = 0;
						playerController.lastTrackPosition = 0;
						//playerController.currentTrack = null;
						playerController.render.resetTrackInfo();
						DZ.player.playTracks([]);
						playerController.play();
						playerController.pause();
					}
					eventController.event.eventActive = res.eventActive;
					eventController.init(res).then(() => {
						playerController.init();
						playlistList.init();
						trackListController.rebuild();
						$("#toggleEvent").attr("value", (eventController.event.eventActive ? 'Stop' : 'Start') + ' event');
					});
				}).fail((xhr, status, errorThrown) => {
					console.log("getEvent Error: " + errorThrown);
					console.log("Status: " + status);
					console.dir(xhr);
				});
			});
			$("#toggleVote").click(() => {
				$.ajax({
					type: "PUT",
					url: `/api/events/${eventController.event.id}/${eventController.event.votingActive ? 'end' : 'start'}voting`,
					dataType: "json"
				}).done(res => {
					if (res.votingActive == eventController.event.votingActive) {
						return ;
					}
					eventController.init(res).then(() => {
						eventController.event.votingActive = res.votingActive;
						$("#toggleVote").attr("value", (eventController.event.votingActive ? 'Stop' : 'Start') + ' vote');
						trackListController.rebuild();
						playlistList.init();
					})
				}).fail((xhr, status, errorThrown) => {
					console.log("getEvent Error: " + errorThrown);
					console.log("Status: " + status);
					console.dir(xhr);
				});
				// tracklist init
			});
		}
	}
};
