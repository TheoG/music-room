// All DZ api calls
const deezer = {
	pauseAfterSeek: false,
	tracks: {}, // map id to obj
	init: () => Promise.all([
		// INIT DZ PLAYER
		new Promise((resolve, reject) => {
			if (!user.isOwner) {
				resolve();
				return ;
			}

			DZ.init({
				appId  : '338242',
				// channelUrl : window.location.origin + '/channel',
				channelUrl : 'http://musicroom.theogros.com/redirect',
				player: {
					onload : () => {

						// DEEZER HEADERS BROKEN
						// Need HTTPS to do that
						// DZ.getLoginStatus(function(response) {
						// 	if (response.authResponse) {
						// 		console.log('not cool');
						// 	} else {
						// 		console.log('cool');
						// 		alert('Connecte toi espece de undéphinède');
						// 	}
						// });





						DZ.login(function(response) {
							if (response.authResponse) {
								console.log('Welcome!  Fetching your information.... ');
								DZ.api('/user/me', function(response) {
									console.log('Good to see you, ' + response.name + '.');
								});
							} else {
								console.log('User cancelled login or did not fully authorize.');
							}
						}, { perms: ''});





						DZ.Event.subscribe('player_position', arg => {
							if (!playerController.isSeeking) {
								playerController.setTrackPosition(Math.floor(arg[0] * 1000));
							}
							if (!playerController.isPlaying) {
								DZ.player.pause();
							}
						});
						DZ.Event.subscribe('current_track', async function(event, evt_name) {
							// console.log('current track deezer : ', event);
							// console.log('current track playerController : ', playerController.currentTrack);
							if (!playerController.currentTrack || event.track.id != playerController.currentTrack.serviceid) {
								for (let i in eventController.playlists) {
									let p = eventController.playlists[i];
									let track = findTrack({serviceid: event.track.id}, p);
									if (track) {
										playerController.currentTrack = track;
										playerController.playlist = p;
										playerController.setTrackPosition(0);
										playerController.trackDuration = deezer.tracks[track.serviceid].duration * 1000;
										playerController.render.updateTrackInfo();
										trackListController.render.play(track.id);
										eventController.play(p.id, track.id);
										apiController.play(playerController.currentTrack.id)
										.then(res => {
											deezer.changeTrackOrder(getPlaylistTracks(playerController.playlist).map(t => t.serviceid));
										});
										// .catch(err => {
										// 	console.log('apiController.play err : ', err)
										// });

										break;
									}
								}
							}
						});

						DZ.Event.subscribe('player_play', (event) => {
							if (apiController.dontdoit) {
								apiController.dontdoit = false;
								return ;
							}
							if (deezer.pauseAfterSeek) {
								deezer.pause();
								deezer.pauseAfterSeek = false;
								return ;
							}
							playerController.play(playerController.currentTrack);
							apiController.play(playerController.currentTrack.id);
							playerController.render.updateTrackInfo();
							playerController.render.play();
						});

						DZ.player.setRepeat(1); // Repeat all
						resolve();
					}
				}
			});
		}),
		// CREATE DZ TRACK MAP
		Promise.all(eventController.playlists.map(async (playlist, i) => {
			await deezer.getTracks(playlist, playlist == trackListController.playlist);
			if (i == 0) {
				trackListController.rebuild(playlist); // set up view
				tracks = getPlaylistTracks(playlist);
				trackListController.playlist = playerController.playlist;
			}
		}))
	]),


	getTracks: (playlist, rebuild) => new Promise(async (resolve, reject) => {
		let dzInterval = null;
		var unloadTracks = [];

		await Promise.all(getPlaylistTracks(playlist).map(track => new Promise((resolve, reject) => {
			DZ.api(`/track/${track.serviceid}`, function(response){
				if (response.hasOwnProperty('error')) {
					var unloadTrack = {
						id: track.serviceid,
						title: 'Loading...'
					};
					unloadTracks.push(track.serviceid);
					resolve(unloadTrack);
				}
				else
					resolve(response);
			});
		})))
		.then(function(tracks) {
			resolve();
			tracks.forEach(track => deezer.tracks[track.id] = track);
			if (rebuild) {
				trackListController.rebuild(playlist);
			}
			if (unloadTracks.length > 0) {
				loadMissingTracks(playlist.refPlaylistId, unloadTracks, rebuild);
			}
		});

		function loadMissingTracks(playlistID, unloadTracks, rebuild) {
			dzInterval = setInterval(() => {
				if (unloadTracks.length < 1) {
					clearInterval(dzInterval);
					return ;
				}
				Promise.all(unloadTracks.map(function(track) {
					return new Promise((resolve, reject) => {
						DZ.api(`/track/${track}`, function(response){
							if (response.hasOwnProperty('error')) {
								var unloadTrack = {
									id: track,
									title: 'Loading...'
								};
								resolve(unloadTrack);
							}
							else {
								unloadTracks.splice(unloadTracks.indexOf(track), 1);
								resolve(response);
							}
						});
					});
				})).then(function(tracks) {
					$.each(tracks, function(i, track) {
						deezer.tracks[track.id] = track;
					});
					if (trackListController.playlist && playlist.id == trackListController.playlist.id) {
						trackListController.rebuild(playlist);
					}
				});
			}, 1000);
		}
	}),
	play: () => {
		DZ.player.play();
	},
	changeTrackOrder: serviceidlist => {
		DZ.player.changeTrackOrder(serviceidlist);
	},
	playTracks: (serviceidlist, serviceid, offset) => {
		if (offset) {
			DZ.player.playTracks(serviceidlist, serviceidlist.indexOf(serviceid), offset / 1000);
		} else {
			DZ.player.playTracks(serviceidlist, serviceidlist.indexOf(serviceid));
		}
	},
	next: () => {
		DZ.player.next();
	},
	prev: () => {
		DZ.player.prev();
	},
	pause: () => {
		DZ.player.pause();
	},
	seek: () => {
		if (!playerController.trackDuration) {
			return ;
		}
		let b = playerController.isPlaying;
		let percentage = playerController.trackPosition / playerController.trackDuration * 100;
		DZ.player.seek(percentage);
	},
	isPlaying: () => {
		return DZ.player.isPlaying();
	},
	getCurrentTrack: () => {
		return DZ.player.getCurrentTrack();
	},
	changeTrackOrder: serviceidlist => {
		DZ.player.changeTrackOrder(serviceidlist);
	},
	printTracks: () => {
		console.log(DZ.player.getTrackList());
	}
};
