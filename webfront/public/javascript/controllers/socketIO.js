var	debug_socket_bool = false;

function debug_socket(event, data) {
	if (!debug_socket_bool)
		return ;

	console.log('==============================');
	console.log(event + ' | ', data);
	console.log('==============================');
}

function initSocket(params) {

	var options = {
		query: {
			apiKey: params.apiKey,
			eventid: eventController.event.id
		}
	};

	var socket = io.connect(params.baseUrl + ":" + params.socketIOPort + "/events", options);

	socket.on('CONNECTED', data => {
		debug_socket('CONNETED', data);
	});

	socket.on('ERROR', data => {
		debug_socket('ERROR', data);
	});

	// socket.on('EVENT_ADD_DJS', data => {
	// 	console.log('==============================');
	// 	console.log('EVENT_ADD_DJS | ', data);
	// 	console.log('==============================');
	// 	if (user.id == data.by)
	// 		return ;
	// });

	// socket.on('EVENT_REMOVE_DJS', data => {
	// 	console.log('==============================');
	// 	console.log('EVENT_REMOVE_DJS | ', data);
	// 	console.log('==============================');
	// 	if (user.id == data.by)
	// 		return ;
	// });

	// socket.on('EVENT_ADD_GUESTS', data => {
	// 	console.log('==============================');
	// 	console.log('EVENT_ADD_GUESTS | ', data);
	// 	console.log('==============================');
	// 	if (user.id == data.by)
	// 		return ;
	// });

	// socket.on('EVENT_REMOVE_GUESTS', data => {
	// 	console.log('==============================');
	// 	console.log('EVENT_REMOVE_GUESTS | ', data);
	// 	console.log('==============================');
	// 	if (user.id == data.by)
	// 		return ;
	// });

	socket.on('EVENT_VOTING_STARTED', data => {
		if (user.id == data.by) {
			return ;
		}
		debug_socket('EVENT_VOTING_STARTED', data);
		eventController.event.votingActive = true;
		if (trackListController.sortable) {
			trackListController.sortable.option('disabled', !eventController.event.votingActive);
		}
		trackListController.playlist = (data.eventPlaylists ? data.eventPlaylists : data.playlists).find(p => {
			let id = (p.refPlaylistId ? p.refPlaylistId : p.id);
			return id == trackListController.playlist.id || id == trackListController.playlist.refPlaylistId;
		});
		eventController.setPlaylists({eventPlaylists: data.eventPlaylists});
		playerController.init();
		trackListController.rebuild();
		playlistList.init();
	});

	socket.on('EVENT_VOTING_ENDED', data => {
		if (user.id == data.by) {
			return ;
		}
		debug_socket('EVENT_VOTING_ENDED', data);
		eventController.event.votingActive = false;
		if (trackListController.sortable) {
			trackListController.sortable.option('disabled', !eventController.event.votingActive);
		}
		trackListController.playlist = (data.eventPlaylists ? data.eventPlaylists : data.playlists).find(p => {
			let id = (p.refPlaylistId ? p.refPlaylistId : p.id);
			return id == trackListController.playlist.id || id == trackListController.playlist.refPlaylistId;
		});
		eventController.setPlaylists({
			eventPlaylists: data.eventPlaylists,
			playlists: data.playlists
		});
		playerController.init();
		trackListController.rebuild();
		playlistList.init();
	});

	socket.on('EVENT_EVENT_STARTED', data => {
		if (user.id == data.by) {
			return ;
		}
		debug_socket('EVENT_EVENT_STARTED', data);

		eventController.event.eventActive = true;
		eventController.setPlaylists({eventPlaylists: data.eventPlaylists});

		playerController.playlist = eventController.playlists[0];
		playerController.init();
		playerController.trackPosition = 0;
		playerController.lastTrackPosition = 0;
		playerController.currentTrack = null;
		playerController.render.resetTrackInfo();
		trackListController.playlist = playerController.playlist;
		trackListController.rebuild();
		playlistList.init()
	});

	socket.on('EVENT_EVENT_ENDED', data => {
		if (user.id == data.by) {
			return ;
		}
		debug_socket('EVENT_EVENT_ENDED', data);
		eventController.event.eventActive = false;
		eventController.setPlaylists({
			eventPlaylists: data.eventPlaylists,
			playlists: data.playlists
		});
		playerController.init();
		playerController.pause();
		trackListController.playlist = (data.eventPlaylists ? data.eventPlaylists : data.playlists).find(p => {
			let id = (p.refPlaylistId ? p.refPlaylistId : p.id);
			return id == trackListController.playlist.id || id == trackListController.playlist.refPlaylistId;
		});
		// TODO: remove ?
		eventController.setPlaylists({
			eventPlaylists: data.eventPlaylists,
			playlists: data.playlists
		});
		trackListController.rebuild();
		playlistList.init()
	});


	socket.on('EVENT_TRACK_LIKE', data => {
		debug_socket('EVENT_TRACK_LIKE', data);
		if (user.id == data.by) {
			return ;
		}
		eventController.vote(Methods.ADD, Votes.LIKE, data.trackid, data.userid);
		if (trackListController.playlist.id == data.playlistid) {
			trackListController.render.voteTotalCount(data.trackid);
		}
	});

	socket.on('EVENT_TRACK_UNLIKE', data => {
		debug_socket('EVENT_TRACK_UNLIKE', data);
		if (user.id == data.by) {
			return ;
		}
		eventController.vote(Methods.REMOVE, Votes.LIKE, data.trackid, data.userid);
		if (trackListController.playlist.id == data.playlistid) {
			trackListController.render.voteTotalCount(data.trackid);
		}
	});

	socket.on('EVENT_TRACK_DISLIKE', data => {
		debug_socket('EVENT_TRACK_DISLIKE', data);
		if (user.id == data.by) {
			return ;
		}
		eventController.vote(Methods.ADD, Votes.DISLIKE, data.trackid, data.userid);
		if (trackListController.playlist.id == data.playlistid) {
			trackListController.render.voteTotalCount(data.trackid);
		}
	});

	socket.on('EVENT_TRACK_UNDISLIKE', data => {
		debug_socket('EVENT_TRACK_UNDISLIKE', data);
		if (user.id == data.by) {
			return ;
		}
		eventController.vote(Methods.REMOVE, Votes.DISLIKE, data.trackid, data.userid);
		if (trackListController.playlist.id == data.playlistid) {
			trackListController.render.voteTotalCount(data.trackid);
		}
	});

	socket.on('EVENT_TRACK_MOVE', data => {
		debug_socket('EVENT_TRACK_MOVE', data);

		eventController.move(data.playlistid, data.trackid, data.index);

		if (trackListController.playlist.id == data.playlistid) {
			trackListController.render.move(data.trackid, data.index);
			playerController.playlist = eventController.event.eventPlaylists.find(p => p.id == data.playlistid);
			if (user.isOwner && playerController.currentTrack) {
				deezer.changeTrackOrder(getPlaylistTracks(playerController.playlist).map(t => t.serviceid));
			}
		}
	});

	socket.on('EVENT_TRACK_PLAY', data => {
		if (user.id == data.by) {
			return ;
		}
		debug_socket('EVENT_TRACK_PLAY', data);

		playerController.lastActionTimeStamp = data.timestamp;
		if (!playerController.isPlaying) {
			playerController.render.play();
		}
		let playlist, track;
		[playlist, track] = eventController.getPlaylistAndTrack({id: data.trackid});
		if (!track || !playlist) {
			return ;
		}
		if (!playerController.currentTrack || playerController.currentTrack.id != track.id) {
			// changed track
			playerController.currentTrack = track;
		}
		if (!playerController.playlist || playerController.playlist.id != playlist.id) {
			// changed playlist
			trackListController.rebuild(playlist);
			playerController.playlist = playlist;
		}
		if (playerController.currentTrack && playerController.currentTrack.id != data.trackid) {

			let playlist, track;
			[playlist, track] = eventController.getPlaylistAndTrack({id: data.trackid});
			playerController.currentTrack = track;
			playerController.playlist = playlist;


			//if (trackListController.playlist.id != data.playlistid) {
				trackListController.rebuild(playlist);
			//}


			playerController.trackDuration = deezer.tracks[track.serviceid].duration * 1000;
			playerController.setTrackPosition(data.trackPosition);
			playerController.lastTrackPosition = data.trackPosition;


			playerController.play(track);
			playerController.setTrackInterval();


			// trackListController.render.play(track.id);
			// eventController.play($(event.currentTarget).attr('data-playlist'), track.id);

		} else {

			playerController.lastTrackPosition = data.trackPosition;
			playerController.setTrackPosition(data.trackPosition);
			playerController.play();
			playerController.setTrackInterval();
		}

		playerController.render.updateTrackInfo();
		//console.log('trackListController.render.play');
		// trackListController.render.play(data.trackid, true);
		eventController.play(data.playlistid, data.trackid);
	});

	socket.on('EVENT_TRACK_PAUSE', data => {
		if (user.id == data.by) {
			return ;
		}
		debug_socket('EVENT_TRACK_PAUSE', data);

		playerController.render.updateTrackInfo();
		playerController.lastActionTimeStamp = data.timestamp;
		playerController.lastTrackPosition = data.trackPosition;
		playerController.setTrackPosition(data.trackPosition);
		playerController.pause();
		playerController.render.pause();
	});

	socket.on('EVENT_TRACK_SEEK', data => {
		if (user.id == data.by) {
			return ;
		}
		debug_socket('EVENT_TRACK_SEEK', data);

		playerController.lastActionTimeStamp = data.timestamp;
		playerController.lastTrackPosition = data.trackPosition;
		playerController.setTrackPosition(data.trackPosition);
	});


	socket.on('EVENT_TRACK_PLAY_REQUEST', async data => {
		debug_socket('EVENT_TRACK_PLAY_REQUEST', data);

		let playlist, track;
		[playlist, track] = eventController.getPlaylistAndTrack({id: data.trackid});
		if (!playlist || !track) {
			return ;
		}

		if (trackListController.playlist.id != data.playlistid) {
			trackListController.rebuild(playlist);
		}

		if (!deezer.getCurrentTrack() || playlist.id != playerController.playlist.id || !playerController.currentTrack || track.id != playerController.currentTrack.id) {
			deezer.playTracks(getPlaylistTracks(playlist).map(t => t.serviceid), track.serviceid, 0);
		} else {
			deezer.play();
		}
	});

	socket.on('EVENT_TRACK_PAUSE_REQUEST', data => {
		debug_socket('EVENT_TRACK_PAUSE_REQUEST', data);
		deezer.pause();
		apiController.pause();
		playerController.render.pause();
	});

	socket.on('EVENT_TRACK_SEEK_REQUEST', data => {
		debug_socket('EVENT_TRACK_SEEK_REQUEST', data);
		var isPlaying = DZ.player.isPlaying();

		playerController.setTrackPosition(data.trackPosition);
		apiController.seek(data.trackPosition);
		apiController.dontdoit = true;
		deezer.seek();
		if (!isPlaying) {
			deezer.pause();
		}
		playerController.render.trackPosition();
	});

	socket.on('EVENT_UPDATE', data => {
		debug_socket('EVENT_UPDATE', data);
		if (userID == data.by) {
			return ;
		}
	});

	socket.on('EVENT_DELETED', data => {
		debug_socket('EVENT_DELETED', data);
		if (userID == data.by) {
			return ;
		}
	});


	socket.on('EVENT_OWNER_DISCONNECT', data => {
		debug_socket('EVENT_OWNER_DISCONNECT', data);
		playerController.setTrackPosition(0);
		playerController.pause();
		eventController.isOwnerConnected = false;
	});

	socket.on('disconnect', data => {
		debug_socket('disconnect', data);
	});

	socket.on('EVENT_OWNER_CONNECT', data => {
		debug_socket('EVENT_OWNER_CONNECT', data);
		eventController.isOwnerConnected = true;
	});


	socket.on('EVENT_STARTER_PACK', data => {
		debug_socket('EVENT_STARTER_PACK', data);

		eventController.event.eventActive = data.eventActive;
		eventController.event.votingActive = data.votingActive;
		if (trackListController.sortable) {
			trackListController.sortable.option('disabled', !eventController.event.votingActive);
		}
		if (eventController.event.votingActive || eventController.event.eventActive) {
			eventController.isOwnerConnected = data.ownerIsConnected;
			ownerIsConnected = data.ownerIsConnected;
			playerController.trackPosition = data.trackPosition;
			// set current track with activeTrack
			if (data.activeTrack) {
				let playlist, track;
				[playlist, track] = eventController.getPlaylistAndTrack({id: data.activeTrack});
				// let playlist = eventController.playlists.find(p => [...p.unplayedTracks, p.playedTracks].find(t => t.id == data.activeTrack));
				if (playlist) {
					let track = getPlaylistTracks(playlist).find(t => t.id == data.activeTrack);
					playerController.currentTrack = track;
					playerController.trackDuration = deezer.tracks[playerController.currentTrack.serviceid].duration * 1000;
					playerController.setTrackPosition(data.trackPosition);
					playerController.render.updateTrackInfo();
					trackListController.render.play(track.id);
					// trackListController.rebuild(playlist);
				}
			}
			if (!user.isOwner) {
				playerController.lastActionTimeStamp = data.lastTrackActionTimestamp;
				playerController.lastTrackPosition = data.trackPosition;
			}
			playerController.seek(playerController.trackPosition);
			if (data.isPlaying) {
				playerController.play();
				if (!user.isOwner) {
					playerController.setTrackInterval();
				}
			} else {
				playerController.pause();
				if (user.isOwner) {
					apiController.pause();
				}
			}

			// TODO handle timestamp
		}
	});
}
