const user = {
	isOwner: false,
	isDj: false,
	isGuest: false,
	id: null,

	init: async (id) => {
		
		user.id = id;

		if (user.id == eventController.event.owner.id) {
			user.isOwner = true;
		} else if (eventController.event.djs.some(u => u.id == user.id)) {
			user.isDj = true;
		} else if (eventController.event.guests.some(u => u.id == user.id)) {
			user.isGuest = true;
		} else if (eventController.event.visibility == 'PUBLIC') {
			user.isGuest = true;
		} else {
			window.location.replace('/events');
		}
	},
};
