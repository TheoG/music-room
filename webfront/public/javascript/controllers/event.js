const eventController = {
	event: null,
	get playlists() {
		// console.log('get playlists');
		if (eventController.event.eventActive || eventController.event.votingActive) {
			return eventController.event.eventPlaylists;
		} else {
			return eventController.event.playlists;
		}
	},
	isOwnerConnected: false,

	init: async event => {
		// console.log('eventController init', event);
		if (!event) {
			// get event
			var path = window.location.pathname;
			var eventID = path.substr(8, path.length - 8);
			var newPath = '/api/events/' + eventID;

			await new Promise((resolve, reject) => {
				$.ajax({
					type: "GET",
					url: newPath,
					dataType: "json"
				})
				.done(response => {
					if (response.hasOwnProperty('errorCode')) {
						window.location.replace('/events');
						return ;
					}
					eventController.event = response;
					$('.brand-logo').html('Music Room  - ' + eventController.event.name)
					resolve();
				})
				.fail(function(xhr, status, errorThrown) {
					console.log("getEvent Error: " + errorThrown);
					console.log("Status: " + status);
					console.dir(xhr);
					reject();
				});
			});
		} else {
			eventController.event = event;
		}

		// eventController.playlists.sort((p1, p2) => p1.name.localeCompare(p2.name)); // très bien

		playerController.playlist = trackListController.playlist = eventController.event.activePlaylist ? eventController.playlists.find(x => x.id == eventController.event.activePlaylist) : eventController.playlists[0];
		// console.log('playerController.playlist', playerController.playlist.id)
		// console.log('set playercontoller current track')
		// console.log('playerController.playlist.activeTrack', playerController.playlist.activeTrack)
		if (playerController.playlist) {
			playerController.currentTrack = getPlaylistTracks(playerController.playlist).find(t => t.id == playerController.playlist.activeTrack);
		}
		// console.log('currentTrack: ', playerController.currentTrack);
		['eventPlaylists', 'playlists'].map(pl => eventController.event[pl].sort((p1, p2) => p1.name.localeCompare(p2.name)));
		return ;
	},

	getPlaylistAndTrack: param => {
		// console.log('param', param);
		for (let i in eventController.playlists) {
			let p = eventController.playlists[i];
			let track = findTrack(param, p);
			// console.log('playlist', p);
			// console.log('track', track);
			if (track) {
				return [p, track];
			}
		}
		return [null, null];
	},
	setPlaylists: params => {
		eventController.event.playlists = params.playlists || [];
		eventController.event.eventPlaylists = params.eventPlaylists || [];
		['eventPlaylists', 'playlists'].map(pl => eventController.event[pl].sort((p1, p2) => p1.name.localeCompare(p2.name)));
	},

	vote: (method, vote, trackid, userid) => {
		let track = null;
		for (let i = 0; i < eventController.playlists.length; i++) {
			track = findTrack({id: trackid}, eventController.playlists[i]);

			if (track) {
				if (vote == Votes.LIKE) {
					if (method == Methods.ADD) {
						track.likes.push(userid);
						let idx = track.dislikes.indexOf(userid);
						if (idx >= 0) {
							track.dislikes.splice(idx, 1);
						}
					} else if (method == Methods.REMOVE) {
						track.likes.splice(track.likes.indexOf(userid), 1);
					}
				} else if (vote == Votes.DISLIKE) {
					if (method == Methods.ADD) {
						track.dislikes.push(userid);
						let idx = track.likes.indexOf(userid);
						if (idx >= 0) {
							track.likes.splice(idx, 1);
						}
					} else if (method == Methods.REMOVE) {
						track.dislikes.splice(track.likes.indexOf(userid), 1);
					}
				}
			}
		}
	},

	move: (playlistid, trackid, index) => {
		for (let i = 0; i < eventController.playlists.length; i++) {
			if (eventController.playlists[i].id == playlistid) {
				for (let j = 0; j < eventController.playlists[i].unplayedTracks.length; j++) {
					if (eventController.playlists[i].unplayedTracks[j].id == trackid) {
						let tmp = eventController.playlists[i].unplayedTracks.splice(j, 1)[0];
						eventController.playlists[i].unplayedTracks.splice(index, 0, tmp);
						break;
					}
				}
				break;
			}
		}
	},

	play: (playlistid, trackid) => {
		let playlist = eventController.playlists.find(p => p.id == playlistid);
		if (!playlist) {
			// console.log('playlist not found');
			return ;
		}
		let i = playlist.unplayedTracks.findIndex(t => t.id == trackid);
		if (i < 0) {
			return ;
		}
		let track = playlist.unplayedTracks.splice(i, 1)[0];
		playlist.playedTracks.push(track);
		// console.log('eventController MOVED TRACK TO PLAYEDTRACKS');
	}
};
