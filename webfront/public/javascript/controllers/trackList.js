const Methods = {
	ADD: 'PUT',
	REMOVE: 'DELETE'
};

const Votes = {
	LIKE: 'like',
	DISLIKE: 'dislike'
};

const trackListController = {
	playlist: null,
	sortable: null,

	rebuild: playlist => {
		if (!playlist) {
			playlist = trackListController.playlist;
		}
		trackListController.playlist = playlist;
		$("#currentPlaylistAlreadyPlayed").html("");
		$("#currentPlaylistUnplayed").html("");

		// console.log('playlist : ');
		// console.log(playlist);
		// console.log('=========');
		getPlaylistTracks(playlist).map(track => {
			let dzTrack = deezer.tracks[track.serviceid];
			let formattedTime = '';
			if (dzTrack != null && dzTrack.duration != null) {
				formattedTime = new Date(dzTrack.duration * 1000).toISOString().substr(dzTrack.duration >= 3600 ? 11 : 14, dzTrack.duration >= 3600 ? 8 : 5);
			}

			if (eventController.event.votingActive) {
				var isTrackLikedByUser = track.likes.some(userid => userid == user.id);
				var isTrackDislikedByUser = track.dislikes.some(userid => userid == user.id);
			}

			(track.type == 'playedTracks' ? $("#currentPlaylistAlreadyPlayed") : $("#currentPlaylistUnplayed")).append(`
				<div class="track" data-playlist="${playlist.id}" id=${track.id}>
					${eventController.event.votingActive ?
						(playlist.sortingType == 'vote') ?
								'<span class="left-vote-icon fa fa-thumbs-o-down"><span class="left-vote-count"> ' + track.dislikes.length + '</span></span>' :
								'<span class="left-vote-icon fa-stack"><i class="fa fa-long-arrow-down"></i><i class="fa fa-long-arrow-up"></i></span>'
							:
						''
					}
					<span class="title">${dzTrack.title}</span>
					<span class="artist">${dzTrack.artist ? dzTrack.artist.name : '-'}</span>
					<span class="album-title">${dzTrack.album ? dzTrack.album.title : '-'}</span>
					<span class="time"> ${formattedTime}</span>
					${(playlist.sortingType == 'vote' && eventController.event.votingActive) ?
						'<span class="right-vote-icon fa fa-thumbs-o-up"><span class="right-vote-count"> ' + track.likes.length + '</span></span>' :
						''
					}
				</div>
			`);

			if (track.type == 'playedTracks') {
				trackListController.render.disable(track.id);
			}

			if (eventController.event.votingActive) {
				if (isTrackLikedByUser) {
					trackListController.render.vote(Methods.ADD, Votes.LIKE, track.id);
				} else if (isTrackDislikedByUser) {
					trackListController.render.vote(Methods.ADD, Votes.DISLIKE, track.id);
				}
			}
			if (playerController.currentTrack && track.id == playerController.currentTrack.id) {
				trackListController.render.play(track.id);
			} else {

			}
			$(`#${track.id}`).click(function(event) {
				//console.log('click');
				// play track (deezer player)
				// switch to correct playlist (deezer player)
				if (user.isGuest) {
					return ;
				}
				if (!eventController.event.eventActive) {
					toastr.error('Event not active');
					return ;
				}

				if (user.isDj && !eventController.isOwnerConnected) {
					toastr.error('The owner is not connected');
					return ;
				}

				if (playerController.currentTrack && track.id == playerController.currentTrack.id) {
					playerController.seek(0);
					if (user.isOwner) {
						playerController.dontdoit = true;
						deezer.seek();
					}
					apiController.seek(0);
				}
				playerController.setTrackPosition(0);
				playerController.play(track);
				trackListController.render.play(track.id);
				eventController.play($(event.currentTarget).attr('data-playlist'), track.id);
				playerController.playlist = eventController.playlists.find(p => p.id == $(event.currentTarget).attr('data-playlist'));
				if (user.isDj) {
					apiController.play(track.id);
				}
				if (user.isOwner) {
					deezer.playTracks(getPlaylistTracks(playerController.playlist).map(t => t.serviceid), track.serviceid);
				}
			});
		});

		if (trackListController.playlist && trackListController.playlist.sortingType == 'move') {
			if (trackListController.sortable) {
				trackListController.sortable.destroy();
			}
			if (eventController.event.votingActive) {
				trackListController.sortable = Sortable.create($('#currentPlaylistUnplayed').get(0), {
					animation: 150,
					onEnd: (/**Event*/evt) => {
						var itemEl = evt.item;  // dragged HTMLElement
						evt.to;    // target list
						evt.from;  // previous list
						evt.oldIndex;  // element's old index within old parent
						evt.newIndex;  // element's new index within new parent
						evt.oldDraggableIndex; // element's old index within old parent, only counting draggable elements
						evt.newDraggableIndex; // element's new index within new parent, only counting draggable elements
						evt.clone // the clone element
						evt.pullMode;  // when item is in another sortable: `"clone"` if cloning, `true` if moving

						// console.log('new index: ' + evt.newIndex);

						if (evt.oldIndex != evt.newIndex) {
							// console.log(itemEl);
							// console.log('Moved ' + itemEl.id + ' to ' + evt.newIndex);
							apiController.move(itemEl.id, evt.newIndex);

							eventController.move(playlist.id, $(evt.item).id, evt.newIndex);

							if (playerController.playlist.id == trackListController.playlist.id) {
								playerController.playlist = eventController.event.eventPlaylists.find(p => p.id == playerController.playlist.id);
								if (playerController.currentTrack) {
									deezer.changeTrackOrder(getPlaylistTracks(playerController.playlist).map(t => t.serviceid));
								}
							}
						}
					}
				});
			} else {
				trackListController.sortable = null;
			}
			//console.log('disabled: ' + !eventController.event.votingActive);

		}

		$('.left-vote-icon, .right-vote-icon').click((e) => {
			e.stopPropagation();
			e.preventDefault();

			if (trackListController.playlist.sortingType == 'move') {
				return ;
			}

			if (!eventController.event.votingActive) {
				toastr.error('Voting not active');
				return ;
			}

			let iconSpan = e.target;
			if (!$(iconSpan).hasClass('fa')) {
				iconSpan = $(e.target).parents()[0];
			}

			let track = findTrack({id: $(iconSpan).parents()[0].id}, trackListController.playlist);
			let method, vote;

			if ($(iconSpan).hasClass('right-vote-icon')) {
				vote = Votes.LIKE;
				if ($(iconSpan).hasClass('fa-thumbs-o-up')) {
					method = Methods.ADD;
				} else {
					method = Methods.REMOVE;
				}
			} else {
				vote = Votes.DISLIKE;
				if ($(iconSpan).hasClass('fa-thumbs-o-down')) {
					method = Methods.ADD;
				} else {
					method = Methods.REMOVE;
				}
			}

			apiController.vote(method, vote, track.id);
			eventController.vote(method, vote, track.id, user.id);
			trackListController.render.voteTotalCount(track.id);
			trackListController.render.vote(method, vote, track.id);
		});
	},

	render: {
		vote: (method, vote, trackid) => {

			let dislikeIconSpan = $(`#${trackid} .left-vote-icon`);
			let likeIconSpan = $(`#${trackid} .right-vote-icon`);


			if (vote == Votes.LIKE) {
				if (method == Methods.ADD) {
					$(likeIconSpan).addClass('fa-thumbs-up green-text text-darken-2');
					$(likeIconSpan).removeClass('fa-thumbs-o-up');

					$(dislikeIconSpan).removeClass('fa-thumbs-down red-text text-darken-2');
					$(dislikeIconSpan).addClass('fa-thumbs-o-down');
				} else {
					$(likeIconSpan).removeClass('fa-thumbs-up green-text text-darken-2');
					$(likeIconSpan).addClass('fa-thumbs-o-up');
				}
			} else {
				if (method == Methods.ADD) {
					$(dislikeIconSpan).addClass('fa-thumbs-down red-text text-darken-2');
					$(dislikeIconSpan).removeClass('fa-thumbs-o-down');

					$(likeIconSpan).removeClass('fa-thumbs-up green-text text-darken-2');
					$(likeIconSpan).addClass('fa-thumbs-o-up');
				} else {
					$(dislikeIconSpan).removeClass('fa-thumbs-down red-text text-darken-2');
					$(dislikeIconSpan).addClass('fa-thumbs-o-down');
				}
			}
		},

		voteTotalCount: (trackid) => {
			let track = null;

			for (let i = 0; i < eventController.playlists.length; i++) {
				track = findTrack({id: trackid}, eventController.playlists[i]);

				if (track) {
					let likeIconSpan = $(`#${trackid} .right-vote-count`);
					let dislikeIconSpan = $(`#${trackid} .left-vote-count`);

					$(likeIconSpan).html(' ' + track.likes.length);
					$(dislikeIconSpan).html(' ' + track.dislikes.length);
					return ;
				}
			}
		},

		disable: trackid => {
			$(`#${trackid}`).addClass('disabled');
			$(`#${trackid}`).removeClass('currentTrack');
		},

		currentTrack: trackid => {
			$(`#${trackid}`).removeClass('disabled');
			$(`#${trackid}`).addClass('currentTrack');
		},

		play: (trackid, forceMove) => {

			//console.log((!$(`#${trackid}`).hasClass('disabled')));
			//console.log(!$(`#${trackid}`).hasClass('currentTrack'));
			//console.log((forceMove && !$(`#${trackid}`).hasClass('disabled')));

			if ((!$(`#${trackid}`).hasClass('disabled') && !$(`#${trackid}`).hasClass('currentTrack')) || (forceMove && !$(`#${trackid}`).hasClass('disabled'))) {
				$('#currentPlaylistAlreadyPlayed').append($(`#${trackid}`));
			}
			if ($('.currentTrack')) {
				trackListController.render.disable($($('.currentTrack')[0]).attr('id'));
			}
			// set current track
			trackListController.render.currentTrack(trackid);
		},

		move: (trackid, index) => {

			let currentIndex = $('#currentPlaylistUnplayed .track').index($(`#${trackid}`));

			if (currentIndex < index) {
				$($('#currentPlaylistUnplayed .track')[index]).after($(`#${trackid}`));
			} else {
				$($('#currentPlaylistUnplayed .track')[index]).before($(`#${trackid}`));
			}
		}

	}
};
