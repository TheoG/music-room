// controls player
const playerController = {
	isPlaying: false,
	trackPosition: 0, // MILlISECONDS
	trackDuration: 0, // MILLISECONDS
	lastTrackPosition: 0,
	lastActionTimeStamp: null,
	isSeeking: false,
	trackPositionInterval: null, // updates slider for all non-owners
	playlist: null,
	currentTrack: null,
	alreadyInit: false,

	init: () => {
		// console.log('player init');
		if (!eventController.event.eventActive) {
			$("#player").removeClass("showPlayer");
			return ;
		}

		$("#player").addClass("showPlayer");

		if (!user.isGuest) {
			if (!playerController.alreadyInit) {
				// console.log('player callbacks SET ===========');
				$("#playerPlay").click(() => {
					if (user.isDj && !eventController.isOwnerConnected) {
						toastr.error('The owner is not connected');
						return ;
					}
					if (playerController.isPlaying) {
						playerController.pause();
						apiController.pause();
						if (user.isOwner) {
							deezer.pause();
						}
					} else {
						playerController.play();
						if (user.isOwner) {
							if (!deezer.getCurrentTrack()) {
								deezer.playTracks(getPlaylistTracks(playerController.playlist).map(t => t.serviceid), playerController.currentTrack.serviceid, 0);
							} else {
								deezer.play();
							}
						} else {
							apiController.play(playerController.currentTrack.id);
						}
					}
				});
				$("#playerPrev").click(() => playerController.prev());
				$("#playerNext").click(() => playerController.next());
			}
			$(".playerControls").addClass('showControls');
			$("#player-range").removeClass('disabled');
		} else {
			$(".playerControls").removeClass('showControls');
			$("#player-range").addClass('disabled');
		}

		if (!playerController.alreadyInit) {
			$('#player-range').on('input', event => {
				if (user.isDj && !eventController.isOwnerConnected) {
					return ;
				}
				playerController.isSeeking = true;
				let val = $(event.target).val();
				playerController.trackPosition = Math.floor(val * (playerController.trackDuration / 100));
				playerController.render.trackPositionText();
			});

			$('#player-range').on('change', event => {
				if (user.isDj && !eventController.isOwnerConnected) {
					toastr.error('The owner is not connected');
					return ;
				}
				if (!playerController.isPlaying) {
					deezer.pauseAfterSeek = true;
				}
				let val = $(event.target).val();
				playerController.seek(val);
				if (user.isOwner) {
					playerController.dontdoit = true;
					deezer.seek();
				}
				apiController.seek(val * deezer.tracks[playerController.currentTrack.serviceid].duration * 10);
				playerController.isSeeking = false;
			});
			playerController.alreadyInit = true;
		}

		$('#player-range').val('0');
	},

	render: {
		// udpate button
		play: () => {
			$("#playerPlay").addClass('fa-pause');
			$("#playerPlay").removeClass('fa-play');
		},
		pause: () => {
			$("#playerPlay").removeClass('fa-pause');
			$("#playerPlay").addClass('fa-play');
		},
		// update slider
		trackPosition: () => {
			$('#player-range').val(100 * (playerController.trackPosition / playerController.trackDuration));
			playerController.render.trackPositionText();
		},
		// update text
		trackPositionText: () => {
			//console.log('track position text');
			let m = Math.floor((playerController.trackPosition / 1000) / 60);
			let s = Math.floor((playerController.trackPosition / 1000) % 60);
			$('#currentTrackTimePosition').html((m < 10 ? '0' + m : m) + ":" + (s < 10 ? '0' + s : s));
		},

		updateTrackInfo: () => {
			let serviceid = playerController.currentTrack.serviceid;
			let m = Math.floor((playerController.trackDuration / 1000) / 60);
			let s = Math.floor((playerController.trackDuration / 1000) % 60);
			$('#trackTitle').html(deezer.tracks[serviceid].title);
			$('#albumTitle').html(deezer.tracks[serviceid].album.title);
			$('#albumImage').attr('src', deezer.tracks[serviceid].album.cover_big);
			$('#currentTrackTotalTime').html((m < 10 ? '0' + m : m) + ":" + (s < 10 ? '0' + s : s));
		},

		resetTrackInfo: () => {
			$('#trackTitle').html('');
			$('#albumTitle').html('');
			$('#albumImage').attr('src', '');
			$('#currentTrackTimePosition').html('00:00');
			$('#currentTrackTotalTime').html('00:00');
		}
	},

	setTrackPosition: trackPosition => {
		playerController.trackPosition = trackPosition;
		playerController.render.trackPosition();
	},

	// update isPlaying, render, apicall
	play: track => {
		if (apiController.apiLock == true) {
			return ;
		}
		if (track) {
			//console.log('player controller play track : ', track);
		}
		if (user.isDj && !eventController.isOwnerConnected) {
			toastr.error('The owner is not connected');
			return ;
		}
		if (track) {
			playerController.currentTrack = track;
		} else if (playerController.currentTrack) {

		} else if (playerController.playlist.activeTrack) {
			playerController.currentTrack = findTrack({id: playerController.playlist.activeTrack}, playerController.playlist)
			playerController.trackDuration = deezer.tracks[track.serviceid].duration * 1000;
			playerController.render.updateTrackInfo();
		} else {
			playerController.currentTrack = getPlaylistTracks(playerController.playlist)[0];
		}
		if (!playerController.currentTrack) {
			return ;
		}

		playerController.isPlaying = true;

		playerController.trackDuration = deezer.tracks[playerController.currentTrack.serviceid].duration * 1000;

		playerController.render.play();
		trackListController.render.play(playerController.currentTrack.id);
	},

	pause: () => {
		if (apiController.apiLock == true) {
			return ;
		}
		if (user.isDj && !eventController.isOwnerConnected) {
			toastr.error('The owner is not connected');
			return ;
		}
		playerController.isPlaying = false;
		if (!user.isOwner) {
			clearInterval(playerController.trackPositionInterval);
		}
		playerController.render.pause();
	},
	seek: val => {
		playerController.trackPosition = Math.floor((val/100) * playerController.trackDuration);
	},
	next: async () => {
		if (apiController.apiLock == true) {
			return ;
		}
		if (user.isGuest) {
			return ;
		}
		if (user.isDj && !eventController.isOwnerConnected) {
			toastr.error('The owner is not connected');
			return ;
		}
		let tracks = getPlaylistTracks(playerController.playlist);
		let i = tracks.findIndex(track => track.id == playerController.currentTrack.id);
		if (i + 1 < tracks.length) {
			playerController.currentTrack = tracks[i + 1]
			playerController.play(playerController.currentTrack);
			playerController.setTrackPosition(0);
			playerController.trackDuration = deezer.tracks[playerController.currentTrack.serviceid].duration * 1000;
			playerController.render.play();
			apiController.play(playerController.currentTrack.id);
			trackListController.render.play(playerController.currentTrack.id);
			eventController.play(playerController.playlist.id, playerController.currentTrack.id);
			playerController.render.updateTrackInfo();
			if (user.isOwner) {
				if (!deezer.getCurrentTrack()) {
					deezer.playTracks(getPlaylistTracks(playerController.playlist).map(t => t.serviceid), playerController.currentTrack.serviceid, i + 1);
				}
				else {
					deezer.next();
				}
			}
		}
	},
	prev: () => {
		if (apiController.apiLock == true) {
			return ;
		}
		if (user.isGuest) {
			return ;
		}
		if (user.isDj && !eventController.isOwnerConnected) {
			toastr.error('The owner is not connected');
			return ;
		}
		let tracks = getPlaylistTracks(playerController.playlist);
		let i = tracks.findIndex(track => track.id == playerController.currentTrack.id);
		if (i > 0) {
			playerController.currentTrack = tracks[i - 1];
			playerController.play(playerController.currentTrack);
			playerController.setTrackPosition(0);
			playerController.trackDuration = deezer.tracks[playerController.currentTrack.serviceid].duration * 1000;
			apiController.play(playerController.currentTrack.id);
			trackListController.render.play(playerController.currentTrack.id);
			playerController.render.play();
			playerController.render.updateTrackInfo();
			if (user.isOwner) {
				if (!deezer.getCurrentTrack()) {
					deezer.playTracks(getPlaylistTracks(playerController.playlist).map(t => t.serviceid), playerController.currentTrack.serviceid, i - 1);
				}
				else {
					deezer.prev();
				}
			}
		}
	},

	getTrack: serviceid => {
		playerController.currentTrack = findTrack({serviceid}, playerController.playlist);
	},

	setTrackInterval: () => {
		if (!user.isOwner) {
			clearInterval(playerController.trackPositionInterval);
			playerController.trackPositionInterval = setInterval(() => {
				//console.log('last action timestamp : ' + playerController.lastActionTimeStamp);
				//console.log('diff : ' + new Date().getTime() - new Date(playerController.lastActionTimeStamp).getTime());
				//console.log('last track position : ' + playerController.lastTrackPosition);
				//console.log('----------------------');
				playerController.setTrackPosition(playerController.lastTrackPosition + (new Date().getTime() - new Date(playerController.lastActionTimeStamp).getTime()));
			}, 500);
		}
	},

	changeTrack: (playlistid, trackid) => {
		if (playerController.playlist.id == playlistid && trackid == playerController.currentTrack) {
			return ;
		}

		playerController.playlist = eventController.playlists.find(p => p.id == playlistid);
		playerController.currentTrack = trackid;
	}

};
