import * as request from 'request-promise'

export class DeezerApi {

	static baseUrl: string = "https://api.deezer.com";
	static port: string = "443";

	static readonly searchEP = "search/";
	static readonly trackEP = "track/";

	private timeout: number;

	constructor() {
		this.timeout = 2000;
	}

	async get(endPoint: string, success, error, parameters?: object): Promise<void> {
		console.log("Deezer Api call: " + DeezerApi.baseUrl + ":" + DeezerApi.port + "/" + endPoint + "?" + this.encodeParameters(parameters));
		await request({
			url: DeezerApi.baseUrl + ":" + DeezerApi.port + "/" + endPoint + "?" + this.encodeParameters(parameters),
			method: "GET",
			timeout: this.timeout
		}).then(response => {
			//console.log(response.statusCode);
			success(response);
		}, e => {
			error(e.error);
		});
	}

	async post(endPoint: string, data, success, error, parameters?: object): Promise<void> {
		console.log("sending post: " + DeezerApi.baseUrl + ":" + DeezerApi.port + "/" + endPoint + "?" + this.encodeParameters(parameters));

		await request({
			url: DeezerApi.baseUrl + ":" + DeezerApi.port + "/" + endPoint + "?" + this.encodeParameters(parameters),
			method: "POST",
			headers: { "Content-Type": "application/json" },
			timeout: this.timeout,
			body: data,
			json: true
		}).then(response => {
			success(response);
		}, e => {
			error(e.error);
		});
	}

	async put(endPoint: string, data, success, error, parameters?: object): Promise<void> {
		console.log("sending put: " + DeezerApi.baseUrl + ":" + DeezerApi.port + "/" + endPoint + "?" + this.encodeParameters(parameters));
		console.log("data: ",data);

		await request({
			url: DeezerApi.baseUrl + ":" + DeezerApi.port + "/" + endPoint + "?" + this.encodeParameters(parameters),
			method: "PUT",
			headers: { "Content-Type": "application/json" },
			timeout: this.timeout,
			body: data,
			json: true
		}).then(response => {
			//console.log(response);
			success(response);
		}, e => {
			//console.log(e);
			error(e.error);
		});
	}

	async delete(endPoint: string, success, error, parameters?: object): Promise<void> {
		console.log("delete call");
		console.log("url: " + DeezerApi.baseUrl + ":" + DeezerApi.port + "/" + endPoint + "?" + this.encodeParameters(parameters));

		await request({
			url: DeezerApi.baseUrl + ":" + DeezerApi.port + "/" + endPoint + "?" + this.encodeParameters(parameters),
			timeout: this.timeout,
			method: "DELETE"
		}).then(response => {
			success(response);
		}, e => {
			error(e.error);
		});
	}

	private encodeParameters(data: object, concat?: object): string {
		const ret = [];
		for (let d in data) {
			ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
		}
		if (concat) {
			for (let d in concat) {
				ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(concat[d]));
			}
		}
		return ret.join('&');
	}

}
