export enum StringValues {
	apiKey = 'apiKey',
	userID = 'userID',
	user = 'user',
	userSettings = 'userSettings',
	darkMode = 'darkMode',
	deezerAppID = '338242',
}