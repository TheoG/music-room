import * as request from 'request-promise'
import { StringValues } from "./Values";

import * as fs from "fs";

//import * as bgHttp from "nativescript-background-http";

export class MusicRoomApi {

	static baseUrl: string = ""; //e2r10p12
	static port: string = "";
	static socketIOPort: string = "3030";


	// Users End Points
	static readonly users = "users/";
	static readonly me = "users/:userID/";
	static readonly userSettings = "users/:userID/settings/";
	static readonly userSearch = "users/search/";
	static readonly login = "login/";
	static readonly facebook = "users/facebook/";

	// Friends End Points
	static readonly friends = "friends/";
	static readonly friendships = "friendships/";

	// Playlists End Points
	static readonly playlists = "playlists/";
	static readonly editors = "editors/";
	static readonly guests = "guests/";
	static readonly tracks = "tracks/";
	static readonly move = "move/";

	// Events End Points
	static readonly events = "events/";
	static readonly djs = "djs/";
	static readonly startVoting = "startvoting";
	static readonly endVoting = "endvoting";
	static readonly startEvent = "startevent";
	static readonly endEvent = "endevent";
	// Player Actions
	static readonly play = "play";
	static readonly pause = "pause";
	static readonly seek = "seek";



	public apiKey: string;
	private timeout: number;
	public userID: string;


	public static instance: MusicRoomApi = null;

	constructor() {

		//let configFile = fs.knownFolders.currentApp().getFile("config.json");
		//let data = JSON.parse(configFile.readTextSync());
		let data = JSON.parse(fs.readFileSync('config.json', 'utf-8'));

		MusicRoomApi.baseUrl = data['serverName'];
		MusicRoomApi.port = data['serverPort'];

		//this.apiKey = appSettings.getString(StringValues.apiKey, "none");
		//this.userID = appSettings.getString(StringValues.userID, "none");
		this.apiKey = StringValues.apiKey;
		this.userID = StringValues.userID;

		if (this.apiKey == "none") {
			this.apiKey = "";
		}

		this.timeout = 2500;

		if (MusicRoomApi.instance == null) {
			MusicRoomApi.instance = this;
		}


	}

	async get(endPoint: string, success, error, session, parameters?: object): Promise<void> {
		endPoint = endPoint.replace(':userID', session.userID);

		const url = MusicRoomApi.baseUrl + ":" + MusicRoomApi.port + "/" + endPoint + "?" + this.encodeParameters(parameters, {[StringValues.apiKey]: session.apiKey});
		//console.log("sending get: " + url);

		await request({
			url: url,
			method: "GET",
			timeout: this.timeout
		}).then(response => {
			success(response);
		}, e => {
			error(e.error);
		});
	}

	async post(endPoint: string, data, success, error, session, parameters?: object): Promise<void> {
		endPoint = endPoint.replace(':userID', this.userID);

		const url = MusicRoomApi.baseUrl + ":" + MusicRoomApi.port + "/" + endPoint + "?" + this.encodeParameters(parameters, {[StringValues.apiKey]: session.apiKey});
		//console.log("sending post: " + url);
		//console.log("data: ", data);

		await request({
			url: url,
			method: "POST",
			headers: { "Content-Type": "application/json" },
			timeout: this.timeout,
			body: data,
			json: true
		}).then(response => {
			success(response);
		}, e => {
			error(e.error, e.statusCode);
		});
	}

	async put(endPoint: string, data, success, error, session, parameters?: object): Promise<void> {
		endPoint = endPoint.replace(':userID', this.userID);

		const url = MusicRoomApi.baseUrl + ":" + MusicRoomApi.port + "/" + endPoint + "?" + this.encodeParameters(parameters, {[StringValues.apiKey]: session.apiKey});
		//console.log("sending put: " + url);
		//console.log("data: ", data);

		await request({
			url: url,
			method: "PUT",
			headers: { "Content-Type": "application/json" },
			timeout: this.timeout,
			body: data,
			json: true
		}).then(response => {
			success(response);
		}, e => {
			error(e.error, e.statusCode);
		});
	}

	async delete(endPoint: string, data, success, error, session, parameters?: object): Promise<void> {
		endPoint = endPoint.replace(':userID', this.userID);

		const url = MusicRoomApi.baseUrl + ":" + MusicRoomApi.port + "/" + endPoint + "?" + this.encodeParameters(parameters, {[StringValues.apiKey]: session.apiKey});
		//console.log("sending delete: " + url);
		//console.log("data: ", data);

		await request({
			url: url,
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				"X-HTTP-Method-Override": "DELETE"
			},
			timeout: this.timeout,
			body: data,
			json: true
		}).then(response => {
			success(response);
		}, e => {
			error(e.error, e.statusCode);
		});
	}

	/*async sendFile(endPoint: string, filename: string, success, error, parameters?: object): Promise<void> {
		endPoint = endPoint.replace(':userID', this.userID);

		let name = filename.substr(filename.lastIndexOf("/") + 1);
		let url = MusicRoomApi.baseUrl + ":" + MusicRoomApi.port + "/" + endPoint + "?" + this.encodeParameters(parameters, {[StringValues.apiKey]: this.apiKey})

		let session: bgHttp.Session = bgHttp.session("avatar");
		let request: bgHttp.Request = {
			url: url,
			method: "PUT",
			headers: {
				"Content-Type": "multipart/form-data"
			},
			description: "Uploading " + "avatar"
		};

		console.log(request);

		console.log("Upload file task");

		let task = null;

		try {
			let params = [
				{ name: "avatar", filename, mimeType: "image/jpeg" }
			 ];
			task = session.multipartUpload(params, request);
		} catch (e) {
			console.log("Exception: ", e);
		}

		console.log("Task:");
		console.log(task);

		task.on("progress", progressHandler);
		task.on("error", errorHandler);
		task.on("responded", (response) => {
			console.log("received " + response.responseCode + " codresponse. Server sent: " + response.data);
			success(JSON.parse(response.data));
		});
		task.on("complete", completeHandler);
		task.on("cancelled", cancelledHandler); // Android on

		function progressHandler(e) {
			console.log("uploaded " + e.currentBytes + " / " + e.totalBytes);
		}

		// event arguments:
		// task: Task
		// responseCode: number
		// error: java.lang.Exception (Android) / NSError (iOS)
		// response: net.gotev.uploadservice.ServerResponse (Android) / NSHTTPURLResponse (iOS)
		function errorHandler(e) {
			console.log("received error: ", e);
			// var serverResponse = e.response;
		}

		// event arguments:
		// task: Task
		// responseCode: number
		// response: net.gotev.uploadservice.ServerResponse (Android) / NSHTTPURLResponse (iOS)
		function completeHandler(e) {
			console.log("received " + e.responseCode + " code");
			var serverResponse = e.response;
		}

		// event arguments:
		// task: Task
		function cancelledHandler(e) {
			console.log("upload cancelled");
		}
	}*/

	private encodeParameters(data: object, concat?: object): string {
		const ret = [];
		for (let d in data) {
			ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
		}
		if (concat) {
			for (let d in concat) {
				ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(concat[d]));
			}
		}
		return ret.join('&');
	}
}
