import * as createError from 'http-errors';
import * as express from 'express';
import * as session from 'express-session';
import * as path from 'path';
import * as cookieParser from 'cookie-parser';
import * as sassMiddleware from 'node-sass-middleware';
import * as methodOverride from "method-override";

import { MusicRoomApi } from './utils/MusicRoomApi'
import { DeezerApi } from './utils/DeezerApi'

import indexRouter from './routes/index';
import eventsRouter from './routes/events';
import apiRouter from './routes/api';
//import usersRouter from './routes/users';

const port = '3001';

let app = express();
const musicRoomApi = new MusicRoomApi();
const deezerApi = new DeezerApi();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');
app.set('trust proxy', 1);

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(sassMiddleware({
	src: path.join(__dirname, 'public'),
	dest: path.join(__dirname, 'public'),
	indentedSyntax: true, // true = .sass and false = .scss
	sourceMap: true
}));
app.use(methodOverride('X-HTTP-Method'));			// Microsoft
app.use(methodOverride('X-HTTP-Method-Override'));	// Google/GData
app.use(methodOverride('X-Method-Override'));		// IBM

const sessionParams = {
	name: 'session',
	secret: 'music-room',
	resave: true,
	saveUninitialized: true
}
app.use(session(sessionParams));

app.use(express.static(path.join(__dirname, 'public')));

app.use(function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	next();
});

app.use('/', indexRouter);
app.use('/events', eventsRouter);
app.use('/api', apiRouter);
//app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
	next(createError(404));
});

// error handler

app.use(function(err, req, res, next) {
	// set locals, only providing error in development
	res.locals.message = err.message;
	res.locals.error = req.app.get('env') === 'development' ? err : {};

	// render the error page
	res.status(err.status || 500);
	res.render('error');
});

app.listen(port, () => {
	console.log(`server running on ${port}`);
});

export default app;
