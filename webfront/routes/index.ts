import * as express from 'express';

//import controller
import {LoginController, loginController} from "../controllers/loginController";
import { apiController } from "../controllers/apiController";

var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
	if (req.session.hasOwnProperty('apiKey'))
		res.redirect('/events');
	else
		res.redirect('/login');
});

/* GET login page. */
router.get('/login', function(req, res, next) {
	if (req.session.hasOwnProperty('apiKey'))
		res.redirect('/events');
	else
		res.render('login', { title: 'Login', currentCssPage: 'login' });
});

/* GET logout page. */
router.get('/logout', function(req, res, next) {
	if (req.session.hasOwnProperty('apiKey')) {
		delete req.session.apiKey;
		delete req.session.userID;
	}
	res.redirect('/login');
});

/* GET events page. */
router.get('/events', function(req, res, next) {
	if (!req.session.hasOwnProperty('apiKey'))
		res.redirect('/login');
	else {
		res.render('events', {
			title: 'Events',
			logged: true,
			currentCssPage: 'events'
		});
	}
});

router.get('/channel', function(req, res, next) {
	console.log('DEEZER API CONNECTED');
	res.write('<script src="https://e-cdns-files.dzcdn.net/js/min/dz.js"></script>');
});

/* POST login page. */
router.post('/login', loginController.entry);
router.post('/facebook', apiController.facebook);

export default router;
