import * as express from 'express';

//import controller

var router = express.Router();

/* GET events page. */
router.get('/', function(req, res, next) {
	if (!req.session.hasOwnProperty('apiKey'))
		res.redirect('/login');
});


router.get('/:eventid', function(req, res, next) {
	if (!req.session.hasOwnProperty('apiKey'))
		res.redirect('/login');
	else {
		res.render('event', {
			title: 'Event',
			logged: true,
			currentCssPage: 'event'
		});
	}
});

export default router;
