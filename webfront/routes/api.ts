import * as express from 'express';

//import controller
import {ApiController, apiController} from "../controllers/apiController";

var router = express.Router();

router.post('/facebook', apiController.facebook);

router.get('/getApiKey', apiController.getApiKey);
router.post('/events', apiController.newEvent);
router.get('/events', apiController.getList);
router.get('/events/:eventid', apiController.getEvent);
router.delete('/events/:eventid', apiController.delEvent);
router.delete('/events/:eventid/djs', apiController.removeDj);
router.delete('/events/:eventid/guests', apiController.removeGuest);
router.put('/events/:eventid/player/:playeraction', apiController.playerAction);
router.put('/events/:eventid/:eventaction', apiController.eventAction);

router.put('/events/:eventid/:trackid/move', apiController.move);
router.put('/events/:eventid/:trackid/:votingaction', apiController.addVoteAction);
router.delete('/events/:eventid/:trackid/:votingaction', apiController.removeVoteAction);

export default router;
