import { MusicRoomApi } from "../utils/MusicRoomApi";

class ApiController {

	public async newEvent(req, res): Promise<void> {
		await MusicRoomApi.instance.post(MusicRoomApi.events, {
			name: req.body.name,
			visibility: req.body.visibility
		}, response => {
			//console.log("Add event : ", response);
			res.send(response);
		}, error => {
			console.log("Add event error : ", error);
			res.send(error);
		}, req.session);
	}

	public async getList(req, res): Promise<void> {
		await MusicRoomApi.instance.get(MusicRoomApi.events, response => {
			const events = JSON.parse(response);
			//console.log("Get list events : ", events);
			res.send(events);
		}, error => {
			console.log("Get list events error : ", error);
			res.send(error);
		}, req.session);
	}

	public async delEvent(req, res): Promise<void> {
		await MusicRoomApi.instance.delete(MusicRoomApi.events + req.params.eventid, {}, response => {
			//console.log("Delete event : ", response);
			res.send(response);
		}, error => {
			console.log("Delete event error : ", error);
			res.send(error);
		}, req.session);
	}

	public async removeDj(req, res): Promise<void> {
		await MusicRoomApi.instance.delete(MusicRoomApi.events + req.params.eventid + "/djs/" + req.session.userID,
		{}, response => {
			//console.log("Remove dj : ", response);
			res.send(response);
		}, error => {
			console.log("Remove dj error : ", error);
			res.send(error);
		}, req.session);
	}

	public async removeGuest(req, res): Promise<void> {
		await MusicRoomApi.instance.delete(MusicRoomApi.events + req.params.eventid + "/guests/" + req.session.userID,
		{}, response => {
			//console.log("Remove guest : ", response);
			res.send(response);
		}, error => {
			console.log("Remove guest error : ", error);
			res.send(error);
		}, req.session);
	}

	public async getEvent(req, res): Promise<void> {
		await MusicRoomApi.instance.get(MusicRoomApi.events + req.params.eventid, response => {
			const event = JSON.parse(response);
			//console.log("Render event : ", event);
			res.send(event);
		}, error => {
			console.log("Render event error : ", error);
			res.send(error);
		}, req.session);
	}

	public async playerAction(req, res): Promise<void> {
		req.body.trackPosition = parseInt(req.body.trackPosition, 10);
		await MusicRoomApi.instance.put(MusicRoomApi.events + req.params.eventid + '/player/' + req.params.playeraction, req.body, response => {
			res.send(response);
		}, (error, errorCode) => {
			res.status(errorCode).send(error);
		}, req.session);
	}

	public async eventAction(req, res): Promise<void> {
		await MusicRoomApi.instance.put(MusicRoomApi.events + req.params.eventid + '/' + req.params.eventaction, {}, response => {
			res.send(response);
		}, error => {
			res.send(error);
		}, req.session);
	}

	public getApiKey(req, res) {
		res.send({
			apiKey: req.session.apiKey,
			userID: req.session.userID,
			baseUrl: MusicRoomApi.baseUrl,
			socketIOPort: MusicRoomApi.socketIOPort
		});
	}

	public async addVoteAction(req, res): Promise<void> {
		await MusicRoomApi.instance.put(MusicRoomApi.events + req.params.eventid + '/' + req.params.trackid + '/' + req.params.votingaction, {}, response => {
			res.send(response);
		}, (error, errorCode) => {
			res.status(errorCode).send(error);
		}, req.session);
	}

	public async removeVoteAction(req, res): Promise<void> {
		await MusicRoomApi.instance.delete(MusicRoomApi.events + req.params.eventid + '/' + req.params.trackid + '/' + req.params.votingaction, {}, response => {
			res.send(response);
		}, (error, errorCode) => {
			res.status(errorCode).send(error);
		}, req.session);
	}

	public async move(req, res): Promise<void> {
		await MusicRoomApi.instance.put(MusicRoomApi.events + req.params.eventid + '/' + req.params.trackid + '/move', {
			index: req.body.index
		}, response => {
			res.send(response);
		}, (error, errorCode) => {
			res.status(errorCode).send(error);
		}, req.session);
	}
	
	public async facebook(req, res): Promise<void> {
		await MusicRoomApi.instance.post(MusicRoomApi.facebook, {
		},response => {
			req.session.apiKey = response.apiKey;
			req.session.userID = response.id;
			res.send(response);
		}, (error, errorCode) => {
			res.status(errorCode).send(error);
		}, req.session, {
			accessToken: req.query.accessToken
		});
	}
}

const apiController: ApiController = new ApiController();
export {
	ApiController,
	apiController
};
