import { MusicRoomApi } from "../utils/MusicRoomApi";

class LoginController {

	public async entry(req, res): Promise<void> {
		if (req.body.submit === "Signup") {
			loginController.create(req, res);
		}
		else if (req.body.submit === "Login") {
			loginController.login(req, res);
		}
		else {
			res.redirect('/');
		}
	}

	private async create(req, res): Promise<void> {
		let front: any = {
			title: 'Login',
			currentCssPage: 'login'
		};

		await MusicRoomApi.instance.post(MusicRoomApi.users, {
			email: req.body.email,
			password: req.body.password
		}, response => {
			// console.log("create user : ", response);
			front.notif = 'Please check your emails and click the link to activate your account.';
		}, error => {
			console.log("create user error : ", error);
			if (error.hasOwnProperty('message')) {
				front.error = error.message;
			} else {
				front.error = error.errorMessage;
			}
		}, req.session);
		res.render('login', front);
	}

	private async login(req, res): Promise<void> {
		let front: any = {
			title: 'Login',
			currentCssPage: 'login'
		};

		await MusicRoomApi.instance.post(MusicRoomApi.login, {
			email: req.body.email,
			password: req.body.password
		}, response => {
			// console.log("login : ", response);
			req.session.apiKey = response.apiKey;
			req.session.userID = response.id;
			front.notif = response.message;
			res.redirect('/');
		}, error => {
			console.log("login error : ", error);
			front.error = error.errorMessage;
			res.render('login', front);
		}, req.session);
	}
}

const loginController: LoginController = new LoginController();
export {
	LoginController,
	loginController
};
