import { EventData } from "tns-core-modules/data/observable";
import { Page } from "tns-core-modules/ui/page";
import { TextField } from "tns-core-modules/ui/text-field/text-field";
import { Label } from "tns-core-modules/ui/label/label";
import { Image } from "tns-core-modules/ui/image";
import { Switch } from "tns-core-modules/ui/switch";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { ParametersViewModel } from "./parameters-view-model";
import { AppRootViewModel, Theme } from "~/app-root-view-model";
import { MusicRoomApi } from "~/utils/MusicRoomApi";

import * as dialogs from "tns-core-modules/ui/dialogs";
import * as appSettings from "tns-core-modules/application-settings";
import * as frameModule from "tns-core-modules/ui/frame"
import * as imagepicker from "nativescript-imagepicker";
import * as Toast from "nativescript-toast";

let parametersViewModel: ParametersViewModel = null;

enum PrivacyToLabel {
	"PUBLIC" = "Visible by everyone",
	"FRIENDS" = "Only visible by your friends",
	"PRIVATE" = "Only visible by you"
}

enum LabelToPrivacy {
	"Visible by everyone" = "PUBLIC",
	"Only visible by your friends" = "FRIENDS",
	"Only visible by you" = "PRIVATE"
}

class AccountValue {

	constructor(label: Label, title:string, fieldName: string) {
		this.label = label;
		this.title = title;
		this.fieldName = fieldName;
	}

	label: Label;
	title: string;
	fieldName: string;
}

class PrivacySetting {
	constructor(label: Label, actions: string[], title: string, fieldName: string) {
		this.label = label;
		this.actions = actions;
		this.title = title;
		this.fieldName = fieldName;
	}

	label: Label;
	actions: string[];
	title: string;
	fieldName: string;
}

let accountValueList: AccountValue[] = [];
let privacyList: PrivacySetting[] = [];
let page = null;
let drawer: RadSideDrawer = null;
let avatar: Image = null;

export async function navigatingTo(args: EventData): Promise<void> {
	parametersViewModel = new ParametersViewModel();

	parametersViewModel.set('loading', true);
	
	page = <Page>args.object;
	page.bindingContext = parametersViewModel;
	
	let data = await parametersViewModel.getUserInfoAndSettings();
	
	drawer = <RadSideDrawer>frameModule.topmost().parent;
	
	avatar = page.getViewById("avatar");
	
	createInfoAndPrivacyList(data);
	
	parametersViewModel.set("darkTheme", (AppRootViewModel.instance.theme == Theme.DARK));
	parametersViewModel.set('loading', false);
}

export function changeTheme(args: EventData): void {
	
	let themeSwitch: Switch = <Switch>args.object;

	AppRootViewModel.instance.setTheme(themeSwitch.checked ? Theme.LIGHT : Theme.DARK);
}

async function createInfoAndPrivacyList(data: object): Promise<void> {

	let settings = data['settings']

	accountValueList.push(new AccountValue(
		page.getViewById("emailValueLabel"),
		"Email",
		"email"
	));

	accountValueList.push(new AccountValue(
		page.getViewById("displayNameValueLabel"),
		"Display Name",
		"displayName"
	));

	accountValueList.push(new AccountValue(
		page.getViewById("firstNameValueLabel"),
		"First Name",
		"firstName"
	));

	accountValueList.push(new AccountValue(
		page.getViewById("lastNameValueLabel"),
		"Last Name",
		"lastName"
	));

	accountValueList.push(new AccountValue(
		page.getViewById("musicalPreferencesTextField"),
		"Musical Preferences",
		"musicalPreferences"
	));

	privacyList.push(new PrivacySetting(
		page.getViewById("emailPrivacyLabel"),
		privacyValueToString(settings["availableSettings"]["email"]),
		"Email",
		"email"
	));

	privacyList.push(new PrivacySetting(
		page.getViewById("displayNamePrivacyLabel"),
		privacyValueToString(settings["availableSettings"]["displayName"]),
		"Email",
		"displayName"
	));

	privacyList.push(new PrivacySetting(
		page.getViewById("firstNamePrivacyLabel"),
		privacyValueToString(settings["availableSettings"]["firstName"]),
		"First Name",
		"firstName"
	));

	privacyList.push(new PrivacySetting(
		page.getViewById("lastNamePrivacyLabel"),
		privacyValueToString(settings["availableSettings"]["lastName"]),
		"Last Name",
		"lastName"
	));

	privacyList.push(new PrivacySetting(
		page.getViewById("musicalPreferencesPrivacyLabel"),
		privacyValueToString(settings["availableSettings"]["musicalPreferences"]),
		"Musical Preferences",
		"musicalPreferences"
	));

	parametersViewModel.set("emailPrivacyValue", PrivacyToLabel[settings["email"]]);
	parametersViewModel.set("displayNamePrivacyValue", PrivacyToLabel[settings["displayName"]]);
	parametersViewModel.set("firstNamePrivacyValue", PrivacyToLabel[settings["firstName"]]);
	parametersViewModel.set("lastNamePrivacyValue", PrivacyToLabel[settings["lastName"]]);
	parametersViewModel.set("lastNamePrivacyValue", PrivacyToLabel[settings["lastName"]]);
	parametersViewModel.set("musicalPreferencesPrivacyValue", PrivacyToLabel[settings["musicalPreferences"]]);
	
	parametersViewModel.set("emailValue", AppRootViewModel.instance.me.email);
	parametersViewModel.set("displayNameValue", AppRootViewModel.instance.me.displayName);
	parametersViewModel.set("firstNameValue", AppRootViewModel.instance.me.firstName);
	parametersViewModel.set("lastNameValue", AppRootViewModel.instance.me.lastName);
	parametersViewModel.set("musicalPreferences", AppRootViewModel.instance.me.musicalPreferences);
}

export function navigatingFrom(args): void {

	let musicalPreferences: TextField = page.getViewById('musicalPreferencesTextField');

	if (musicalPreferences && AppRootViewModel.instance.me && musicalPreferences.text && AppRootViewModel.instance.me.musicalPreferences != musicalPreferences.text) {
		AppRootViewModel.instance.me.musicalPreferences = musicalPreferences.text;
		MusicRoomApi.instance.put(MusicRoomApi.me, {
			musicalPreferences: musicalPreferences.text
		}, response => {}, error => {});
	}
}

function privacyValueToString(values: string[]): string[] {
	let newValues: string[] = [];
	values.forEach(element => {
		newValues.push(PrivacyToLabel[element]);
	});
	return newValues;
}

export function selectAvatar(args: EventData): void {
	let context = imagepicker.create({
		mode: "single"
	});
	context.authorize().then(function() {
		return context.present();
	}).then(selection => {
		selection.forEach(selected => {
			parametersViewModel.uploadAvatar(selected.android);
		});
	});
}


export function updateAccountValue(args: EventData): void {
	
	let element: AccountValue = null;

	for (let i: number = 0; i < accountValueList.length; i++) {
		if (accountValueList[i]["label"] == args.object ) {
			element = accountValueList[i];
			break;
		}
	}

	if (element == null) {
		return;
	}

	dialogs.prompt({
		title: "Update " + element.title,
		message: "Enter the new value",
		okButtonText: "Save changes",
		cancelButtonText: "Cancel",
		defaultText: element.label.text,
		inputType: dialogs.inputType.text
	}).then(r => {
		element.label.text = r.text;
		if (r.result) {
			parametersViewModel.updateAccount(element.fieldName, r.text);
		}
	});
}

export function updatePrivacy(args: EventData): void {
	
	let element: PrivacySetting = null;

	for (let i: number = 0; i < privacyList.length; i++) {
		if (privacyList[i]["label"] == args.object ) {
			element = privacyList[i];
			break;
		}
	}

	if (element == null) {
		return;
	}

	dialogs.action({
		message: element.title + " Privacy Level",
		cancelButtonText: "Cancel",
		actions: element.actions
	}).then(result => {
		if (result != "Cancel") {
			element.label.text = result;
			parametersViewModel.updatePrivacy(element.fieldName, LabelToPrivacy[result]);
		}
	});
}

export async function logout(args: EventData) {
	appSettings.clear();
	AppRootViewModel.instance = null;
	MusicRoomApi.instance = null;
	await new AppRootViewModel();

	let sideDrawer: RadSideDrawer = frameModule.topmost().parent.getViewById('sideDrawer');
	sideDrawer.gesturesEnabled = false;
	frameModule.topmost().navigate({
		moduleName: "views/login/login-page",
		clearHistory: true
	});
}

export function deleteAccount(args: EventData) {
	dialogs.confirm({
		title: "Delete Account",
		message: "Are you sure? Friends list, events and playlists will be erased",
		okButtonText: "Yes, bye.",
		cancelButtonText: "Wooops, no!",
	}).then(function (result) {
		if (result == true) {
			parametersViewModel.deleteAccount().then(() => {
				logout(null);
			});
		}
	});
}

export function unlockPremium(args: EventData): void {
	if (!AppRootViewModel.instance.me.isPremium) {
		dialogs.prompt({
			title: "Unlock Premium",
			message: "With premium, you can create playlists and events! Pay whatever you want",
			okButtonText: "Yes, GO!",
			cancelButtonText: "No, thanks",
			inputType: dialogs.inputType.number,
		}).then(r => {
			if (r.result == true) {
				MusicRoomApi.instance.put(MusicRoomApi.me + MusicRoomApi.premium, {
				}, response => {
					AppRootViewModel.instance.me.isPremium = true;
					parametersViewModel.set("premiumButton", AppRootViewModel.instance.me.isPremium ? "Remove Premium" : "Activate Premium");
					Toast.makeText('You are premium, now!').show();
				}, error => {
				});
			}
		});
	} else {
		dialogs.prompt({
			title: "Remove Premium",
			message: "Are you sure? :(",
			okButtonText: "Yes, bye, I don't deserve premium",
			cancelButtonText: "Good lord, cancel!"
		}).then(r => {
			if (r.result == true) {
				MusicRoomApi.instance.delete(MusicRoomApi.me + MusicRoomApi.premium, {
				}, response => {
					AppRootViewModel.instance.me.isPremium = false;
					parametersViewModel.set("premiumButton", AppRootViewModel.instance.me.isPremium ? "Remove Premium" : "Activate Premium");
				Toast.makeText('You are not premium anymore').show();
				}, error => {
				});
			}
		});
	}
}

export function showSideDrawer(): void {
	if (drawer) {
		drawer.showDrawer();
	}
}