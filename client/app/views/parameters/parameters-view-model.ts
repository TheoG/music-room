import { Observable } from "tns-core-modules/data/observable";
import { MusicRoomApi } from "~/utils/MusicRoomApi";
import { AppRootViewModel } from "~/app-root-view-model";
import { formatDisplayName } from "~/utils/Tools";


export class ParametersViewModel extends Observable {

	constructor() {
		super();

		this.set("serverAddress", MusicRoomApi.baseUrl);
		this.set("serverPort", MusicRoomApi.port);
	}

	public async getUserInfoAndSettings(): Promise<object> {
		
		let userInfoSettings: any = {};
		
		if (AppRootViewModel.instance.me) {
			this.set("avatarURL", MusicRoomApi.baseUrl + ':' + MusicRoomApi.port + '/' + AppRootViewModel.instance.me.avatar + "?" + new Date().toISOString());
			this.set("premiumButton", AppRootViewModel.instance.me.isPremium ? "Remove Premium" : "Activate Premium");
		}
		await MusicRoomApi.instance.get(MusicRoomApi.userSettings, response => {
			userInfoSettings.settings = response
		}, error => {
		});

		return userInfoSettings;
	}


	public async updateAccount(fieldName: string, value: string): Promise<void> {
		await MusicRoomApi.instance.put(MusicRoomApi.me, {
			[fieldName]: value
		}, response => {
			AppRootViewModel.instance.me[fieldName] = value;
			if (["displayName", "email", "firstName", "lastName"].indexOf(fieldName) > -1) {
				AppRootViewModel.instance.set("displayName", formatDisplayName(AppRootViewModel.instance.me));
			}
		}, error => {
		});
	}

	public uploadAvatar(filename: string): void {
		MusicRoomApi.instance.sendFile(MusicRoomApi.me, filename, response => {
			AppRootViewModel.instance.me.avatar = response.avatar;
			AppRootViewModel.instance.set("avatarURL", MusicRoomApi.baseUrl + ':' + MusicRoomApi.port + '/' + response.avatar + "?" + new Date().toISOString());
			this.set("avatarURL", MusicRoomApi.baseUrl + ':' + MusicRoomApi.port + '/' + response.avatar + "?" + new Date().toISOString());
		}, null);
	}

	public async updatePrivacy(fieldName: string, value: string): Promise<void> {
		await MusicRoomApi.instance.put(MusicRoomApi.userSettings, {
			[fieldName]: value
		}, response => {
		}, error => {
		});
	}

	public async deleteAccount(): Promise<void> {
		await MusicRoomApi.instance.delete(MusicRoomApi.me, {}, response => {
		}, error => {
		});
	}

	public setServerAddress(serverAddr: string): void {
		MusicRoomApi.baseUrl = serverAddr;
	}

	public setServerPort(port: string): void {
		MusicRoomApi.port = port;
	}
}