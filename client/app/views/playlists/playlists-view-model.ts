import { Observable, fromObject as observableFromObject } from "tns-core-modules/data/observable";
import { ObservableArray } from "tns-core-modules/data/observable-array/observable-array";
import { IconsCode } from "~/utils/Icons"
import { MusicRoomApi } from "~/utils/MusicRoomApi";
import { AppRootViewModel, PlaylistAPI } from "~/app-root-view-model";

export interface Playlist extends PlaylistAPI {
	sortingTypeIcon: string;
	removeIcon: string;
	accessTypeIcon: string;
	selectedClass: string;
}

export enum PlaylistRights {
	Guest = 'guest',
	Editor = 'editor',
	Owner = 'owner'
}

export class PlaylistsViewModel extends Observable {
	
	private playlistsList: ObservableArray<Observable> = new ObservableArray();

	constructor() {
		super();

		this.set("playlists", this.playlistsList);
	}

	public getObservablePlaylist(index: number): Observable {
		return this.playlistsList.getItem(index);
	}

	public async fillPlaylists(apiCall: boolean): Promise<void> {

		this.playlistsList.length = 0;

		this.set("loading", true);

		let playlists: { [rights: string]: {[id: string]: PlaylistAPI} } = {
			owner: {},
			editor: {},
			guest: {}
		};
		let sum: number = Object.keys(AppRootViewModel.instance.playlists.owner).length + Object.keys(AppRootViewModel.instance.playlists.guest).length + Object.keys(AppRootViewModel.instance.playlists.editor).length;
		if (apiCall || sum == 0) {
			await MusicRoomApi.instance.get(MusicRoomApi.playlists, response => {
				response.owner.forEach(playlist => {
					playlists.owner[playlist.id] = playlist;
				});
				response.editor.forEach(playlist => {
					playlists.editor[playlist.id] = playlist;
				});
				response.guest.forEach(playlist => {
					playlists.guest[playlist.id] = playlist;
				});
				
				AppRootViewModel.instance.playlists = playlists;


			}, error => {
			});
		} else {
			playlists = AppRootViewModel.instance.playlists;
		}

		this.playlistsList.length = 0;

		for (let plID in playlists.owner) {
			let playlist: Playlist = {
				id: playlists.owner[plID].id,
				tracks: playlists.owner[plID].tracks,
				editors: playlists.owner[plID].editors,
				guests: playlists.owner[plID].guests,
				name: playlists.owner[plID].name,
				owner: playlists.owner[plID].owner,
				visibility: playlists.owner[plID].visibility,
				editing: playlists.owner[plID].editing,
				sortingType: playlists.owner[plID].sortingType,
				events: playlists.owner[plID].events,
				playedTracks: playlists.owner[plID].playedTracks,
				unplayedTracks: playlists.owner[plID].unplayedTracks,
				activeTrack: playlists.owner[plID].activeTrack,
				sortingTypeIcon: playlists.owner[plID].sortingType == "move" ? IconsCode.ArrowUp + IconsCode.ArrowDown : IconsCode.Like,
				removeIcon: IconsCode.Remove,
				accessTypeIcon: null,
				selectedClass: '',
			}
			this.playlistsList.push(observableFromObject(playlist));
		}


		for (let plID in playlists.editor) {
			let playlist: Playlist = {
				id: playlists.editor[plID].id,
				tracks: playlists.editor[plID].tracks,
				editors: playlists.editor[plID].editors,
				guests: playlists.editor[plID].guests,
				name: playlists.editor[plID].name,
				owner: playlists.editor[plID].owner,
				visibility: playlists.editor[plID].visibility,
				editing: playlists.editor[plID].editing,
				sortingType: playlists.editor[plID].sortingType,
				events: playlists.editor[plID].events,
				playedTracks: playlists.editor[plID].playedTracks,
				unplayedTracks: playlists.editor[plID].unplayedTracks,
				activeTrack: playlists.editor[plID].activeTrack,
				sortingTypeIcon: playlists.editor[plID].sortingType == "move" ? IconsCode.ArrowUp + IconsCode.ArrowDown : IconsCode.Like,
				removeIcon: IconsCode.Remove,
				accessTypeIcon: IconsCode.Pencil,
				selectedClass: ''
			}
			this.playlistsList.push(observableFromObject(playlist));
		} 

		for (let plID in playlists.guest) {

			if (plID in playlists.editor) {
				continue;
			}

			let playlist: Playlist = {
				id: playlists.guest[plID].id,
				tracks: playlists.guest[plID].tracks,
				editors: playlists.guest[plID].editors,
				guests: playlists.guest[plID].guests,
				name: playlists.guest[plID].name,
				owner: playlists.guest[plID].owner,
				visibility: playlists.guest[plID].visibility,
				editing: playlists.guest[plID].editing,
				sortingType: playlists.guest[plID].sortingType,
				events: playlists.guest[plID].events,
				playedTracks: playlists.guest[plID].playedTracks,
				unplayedTracks: playlists.guest[plID].unplayedTracks,
				activeTrack: playlists.guest[plID].activeTrack,
				sortingTypeIcon: playlists.guest[plID].sortingType == "move" ? IconsCode.ArrowUp + IconsCode.ArrowDown : IconsCode.Like,
				removeIcon: IconsCode.Remove,
				accessTypeIcon: IconsCode.Eye,
				selectedClass: ''
			}
			this.playlistsList.push(observableFromObject(playlist));
		}
		this.set("loading", false);
	}

	public delete(playlist: Playlist): void {

		let index: number = -1;
		this.playlistsList.forEach((pl: any, idx) => {
			if (playlist.id == pl.id) {
				index = idx;
			}
		});
		this.playlistsList.splice(index, 1);

		let url: string;

		if (playlist.accessTypeIcon == IconsCode.Pencil) {
			url = MusicRoomApi.playlists + playlist.id + '/' + MusicRoomApi.editors + AppRootViewModel.instance.me.id;
			delete AppRootViewModel.instance.playlists.editor[playlist.id];
		} else if (playlist.accessTypeIcon == IconsCode.Eye) {
			url = MusicRoomApi.playlists + playlist.id + '/' + MusicRoomApi.guests + AppRootViewModel.instance.me.id;
			delete AppRootViewModel.instance.playlists.guest[playlist.id];
		} else {
			url = MusicRoomApi.playlists + playlist.id;
			delete AppRootViewModel.instance.playlists.owner[playlist.id];
		}

		MusicRoomApi.instance.delete(url, {}, response => {
		}, error => {
		});
	}

	public deleteMultiple(playlists: Playlist[]): void {

		let playlistids: string[] = [];

		playlists.forEach(playlist => {
			if (playlist.accessTypeIcon == IconsCode.Pencil) {
				delete AppRootViewModel.instance.playlists.editor[playlist.id];
				MusicRoomApi.instance.delete(MusicRoomApi.playlists + playlist.id + '/' + MusicRoomApi.editors + ':userID',  {
				}, response => {
				}, error => {
				});
			} else if (playlist.accessTypeIcon == IconsCode.Eye || playlist.id in AppRootViewModel.instance.playlists.guest) {
				delete AppRootViewModel.instance.playlists.guest[playlist.id];
				MusicRoomApi.instance.delete(MusicRoomApi.playlists + playlist.id + '/' + MusicRoomApi.guests + ':userID',  {
				}, response => {
				}, error => {
				});
			} else {
				delete AppRootViewModel.instance.playlists.owner[playlist.id];
				playlistids.push(playlist.id);
			}

			let index: number = -1;
			this.playlistsList.forEach((pl: any, idx) => {
				if (playlist.id == pl.id) {
					index = idx;
				}
			});
			if (index != -1) {
				this.playlistsList.splice(index, 1);
			}
		});

		MusicRoomApi.instance.delete(MusicRoomApi.playlists, {
			playlistids
		}, response => {
		}, error => {
		});	
	}
}
