import { Observable, fromObject as observableFromObject} from 'tns-core-modules/data/observable';
import { ObservableArray } from "tns-core-modules/data/observable-array/observable-array";
import { IconsCode } from "~/utils/Icons"
import { ListViewEventData } from "nativescript-ui-listview";
import { toHHMMSS } from "~/utils/Tools";
import { DeezerApi } from '~/utils/DeezerApi';
import { MusicRoomApi } from '~/utils/MusicRoomApi';
import { Playlist } from '../playlists-view-model'
import { SocketIO } from 'nativescript-socketio';
import { AppRootViewModel, TrackAPI, UserAPI } from '~/app-root-view-model';
import { Locker } from '~/utils/Lock';
import { DeezerTrack } from '~/models/Track';
import * as Toast from 'nativescript-toast';
import * as frameModule from "tns-core-modules/ui/frame"

export interface TrackModel {
	id: string;
	serviceid: number;
	name: string;
	album: string;
	artist: string;
	coverImageURL: string;
	duration: number
}

export interface Track extends TrackModel {
	formatedDuration: string;
	reorderIcon: string;
	removeIcon: string;
	addIcon: string;
}

export class PlaylistViewModel extends Observable {

	private deezerApi: DeezerApi = null;
	private musicRoomApi: MusicRoomApi = null;

	private playlistsList: ObservableArray<Observable> = new ObservableArray();
	private searchTrackList: ObservableArray<Observable> = new ObservableArray();

	public playlist: Playlist;

	private locked: boolean;
	private callbacks = [];
	private dragging: boolean;
	private socketIO: SocketIO = null;

	private rights: string;

	private interval = null;

	constructor(playlist: Playlist) {
		super()
		this.deezerApi = new DeezerApi();

		this.locked = false;
		this.dragging = false;
		this.playlist = playlist;
		this.interval = null;

		let canEdit: boolean = false;
		if (playlist.editing == 'PUBLIC') {
			canEdit = true;
		}
		if (typeof playlist.owner == 'string' && playlist.owner == AppRootViewModel.instance.me.id) {
			canEdit = true;
			this.set("isOwner", true);
		} else if (typeof playlist.owner != 'string' && (<UserAPI>playlist.owner).id == AppRootViewModel.instance.me.id) {
			canEdit = true;
			this.set("isOwner", true);
		}

		for (let i: number = 0; i < this.playlist.editors.length; i++) {
			if (typeof this.playlist.editors[i] == 'string') {
				if (AppRootViewModel.instance.me.id == this.playlist.editors[i]) {
					canEdit = true;
					break;
				}
			} else {
				if (AppRootViewModel.instance.me.id == (<UserAPI>this.playlist.editors[i]).id) {
					canEdit = true;
					break;
				}
			}
		}
		this.set("canSearchNewTracks", canEdit);
		this.set("canReorder", canEdit);
		
		if (playlist.owner == AppRootViewModel.instance.me.id) {
			this.rights = 'owner';
		} else if (playlist.accessTypeIcon == IconsCode.Pencil || playlist.editing == 'PUBLIC') {
			this.rights = 'editor';
		} else {
			this.rights = 'guest';
		}

		this.set("playlistName", this.playlist.name);

		this.socketIO = new SocketIO(MusicRoomApi.baseUrl + ':' + MusicRoomApi.sockerIOPort + '/playlists', {
			query: {
				apiKey: AppRootViewModel.instance.me.apiKey,
				playlistid: playlist.id
			}
		});

		this.socketIO.on('CONNECTED', data => {
			console.log('CONNECTED');
		});

		this.socketIO.on('ERROR', data => {
			console.log('ERROR');
		});

		
		this.socketIO.on('PLAYLIST_DELETED', data => {
			console.log('PLAYLIST_DELETED');
			Toast.makeText('Playlist deleted').show();
			frameModule.topmost().navigate({
				moduleName: "views/playlists/playlists"
			});
			
		});


		this.socketIO.on('PLAYLIST_UPDATE', data => {
			console.log('PLAYLIST_UPDATE ');
			if (data.name != this.playlist.name) {
				this.set('playlistName', data.name);
				AppRootViewModel.instance.playlists[this.rights][this.playlist.id].name = data.name;
			}
			if (data.editing != this.playlist.editing) {
				this.playlist.editing = data.editing
				AppRootViewModel.instance.playlists[this.rights][this.playlist.id].editing = data.editing;
			}
			if (data.visibility != this.playlist.visibility) {
				this.playlist.visibility = data.visibility
				AppRootViewModel.instance.playlists[this.rights][this.playlist.id].visibility = data.visibility;
			}
		});

		this.socketIO.on('PLAYLIST_ADD_TRACK', data => {
			console.log('PLAYLIST_ADD_TRACK ');

			let index: number = this.getIndexByTrackID(data.id);
			if (index != -1) {
				return;
			}

			this.deezerApi.get(DeezerApi.trackEP + data.serviceid, response => {
	
				let track: Track = {
					id: data.id,
					serviceid: data.serviceid,
					name: response.title,
					album: response.album.title,
					artist: response.artist.name,
					coverImageURL: response.album.cover_medium,
					duration: response.duration,
					formatedDuration: toHHMMSS(response.duration),
					reorderIcon: IconsCode.Bars,
					removeIcon: null,
					addIcon: IconsCode.Plus
				}
				this.playlistsList.push(observableFromObject(track));
			}, error => {
			});
		});

		this.socketIO.on('PLAYLIST_REMOVE_TRACK', data => {
			console.log('PLAYLIST_REMOVE_TRACK ');
			let index = this.getIndexByTrackID(data);
			if (index > -1) {
				this.playlistsList.splice(index, 1);
			}
		});

		this.socketIO.on('PLAYLIST_REMOVE_EDITOR', data => {
			console.log('PLAYLIST_REMOVE_EDITOR ');


			for (let j: number = 0; j < data.userids.length; j++) {
				for (let i: number = 0; i < AppRootViewModel.instance.playlists[this.rights][this.playlist.id].editors.length; i++) {
					if (typeof AppRootViewModel.instance.playlists[this.rights][this.playlist.id].editors[i] == 'string' && AppRootViewModel.instance.playlists[this.rights][this.playlist.id].editors[i] == data.userids[j]) {
						AppRootViewModel.instance.playlists[this.rights][this.playlist.id].editors.splice(i, 1);
						break;
					} else if (typeof AppRootViewModel.instance.playlists[this.rights][this.playlist.id].editors[i] != 'string' && (<UserAPI>(AppRootViewModel.instance.playlists[this.rights][this.playlist.id].editors[i])).id == data.userids[j]) { 
						AppRootViewModel.instance.playlists[this.rights][this.playlist.id].editors.splice(i, 1);
						break;
					}
				}
			}

			if (data.userids.includes(AppRootViewModel.instance.me.id)) {
				delete AppRootViewModel.instance.playlists.editor[this.playlist.id];

				let canStay: boolean = false;
				if (AppRootViewModel.instance.playlists.guest[this.playlist.id]) {
					for (let i: number = 0; i < AppRootViewModel.instance.playlists.guest[this.playlist.id].guests.length; i++) {
						if ((typeof AppRootViewModel.instance.playlists.guest[this.playlist.id].guests[i] == 'string' && AppRootViewModel.instance.playlists.guest[this.playlist.id].guests[i] == AppRootViewModel.instance.me.id) || 				
							(typeof AppRootViewModel.instance.playlists.guest[this.playlist.id].guests[i] != 'string' && (<UserAPI>(AppRootViewModel.instance.playlists.guest[this.playlist.id].guests[i])).id == AppRootViewModel.instance.me.id)) { 
							canStay = true;
							break;
						}
					}
				}
				this.rights = 'guest';
				if (!canStay) {
					frameModule.topmost().navigate({
						moduleName: "views/playlists/playlists",
						context: {
							refresh: true
						}
					});
					return;
				}
				this.set("canSearchNewTracks", this.playlist.editing == 'PUBLIC');
				this.set("canReorder", this.playlist.editing == 'PUBLIC');
			}
		});

		this.socketIO.on('PLAYLIST_REMOVE_GUEST', data => {
			console.log('PLAYLIST_REMOVE_GUEST ');

			for (let j: number = 0; j < data.userids.length; j++) {
				for (let i: number = 0; i < AppRootViewModel.instance.playlists[this.rights][this.playlist.id].guests.length; i++) {
					if (typeof AppRootViewModel.instance.playlists[this.rights][this.playlist.id].guests[i] == 'string' && AppRootViewModel.instance.playlists[this.rights][this.playlist.id].guests[i] == data.userids[j]) {
						AppRootViewModel.instance.playlists[this.rights][this.playlist.id].guests.splice(i, 1);
						break;
					} else if (typeof AppRootViewModel.instance.playlists[this.rights][this.playlist.id].guests[i] != 'string' && (<UserAPI>(AppRootViewModel.instance.playlists[this.rights][this.playlist.id].guests[i])).id == data.userids[j]) { 
						AppRootViewModel.instance.playlists[this.rights][this.playlist.id].guests.splice(i, 1);
						break;
					}
				}
			}

			if (data.userids.includes(AppRootViewModel.instance.me.id)) {
				delete AppRootViewModel.instance.playlists.editor[this.playlist.id];

				if (this.playlist.visibility != 'PUBLIC') {
					frameModule.topmost().navigate({
						moduleName: "views/playlists/playlists",
						context: {
							refresh: true
						}
					});
					return;
				}
			}
		});

		this.socketIO.on('PLAYLIST_MOVE_TRACK', data => {
			console.log('PLAYLIST_MOVE_TRACK');
			let index: number = this.getIndexByTrackID(data.trackid);
			if (index > -1) {
				let toMove: any = this.playlistsList.splice(index, 1)[0];
				this.playlistsList.splice(data.index, 0, toMove);
			}
		});

		this.socketIO.on('PLAYLIST_DELETED', data => {
			delete AppRootViewModel.instance.playlists[this.rights][this.playlist.id];
			if (this.rights == 'editor') {
				delete AppRootViewModel.instance.playlists['guest'][this.playlist.id];
			}
			frameModule.topmost().navigate({
				moduleName: "views/playlists/playlists"
			});
			this.destroyInterval();
			this.destroySocketIO();
			gc();
			return;
		});

		this.socketIO.connect();
	}

	public async fillTracks(tracks: any[]): Promise<void> {
		this.playlistsList.length = tracks.length;
		this.set("loading", true);
	
		let shouldCallLater: number[] = [];
		let trackList;

		trackList = await Promise.all(tracks.map(element => {
			return new Promise(async resolve => {
				let deezerTrack: DeezerTrack = await AppRootViewModel.instance.trackApiCaller.getTrack(element.serviceid);

				let track: Track = {
					id: element.id,
					serviceid: element.serviceid,
					reorderIcon: IconsCode.Bars,
					removeIcon: null,
					addIcon: IconsCode.Plus,
					name: '',
					album: '',
					artist: '',
					coverImageURL: '',
					duration: 0,
					formatedDuration: '',
				}

				if (deezerTrack) {
					track.name = deezerTrack.title;
					track.album = deezerTrack.albumTitle;
					track.artist = deezerTrack.artistName;
					track.coverImageURL = deezerTrack.albumCover_medium;
					track.duration = deezerTrack.duration;
					track.formatedDuration = toHHMMSS(deezerTrack.duration.toString());
				} else {
					track.name = 'Downloading...';
					track.album = 'Waiting for Deezer Response...';
					shouldCallLater.push(element.serviceid);
				}
				resolve(observableFromObject(track));
			});
		}));

		if (shouldCallLater.length > 0) {
			this.interval = setInterval(() => {
				shouldCallLater.forEach(serviceID => {
					new Promise(async (resolve, reject) => {
						let deezerTrack: DeezerTrack = await AppRootViewModel.instance.trackApiCaller.getTrack(serviceID);
						if (!deezerTrack) {
							reject();
						} else {
							Locker.acquire('receiveDeezerData', () => {
								shouldCallLater.splice(shouldCallLater.indexOf(serviceID), 1);
								for (let i = 0; i < this.playlistsList.length; i++) {
									let track: Track = this.playlistsList.getItem(i) as any;
									if (track.serviceid == serviceID) {
										track.name = deezerTrack.title;
										track.album = deezerTrack.albumTitle;
										track.artist = deezerTrack.artistName;
										track.coverImageURL = deezerTrack.albumCover_medium;
										track.duration = deezerTrack.duration;
										track.formatedDuration = toHHMMSS(deezerTrack.duration.toString());
										break;
									}
								}
								resolve();	
								Locker.release('receiveDeezerData');
							});
						}
					})
				});
				if (shouldCallLater.length == 0) {
					clearInterval(this.interval);
					this.interval = null;
				}
			}, 2000);
		}

		this.playlistsList.length = 0;
		trackList.forEach((element: any) => {
			this.playlistsList.push(element as Observable);
		});
		
		this.set("loading", false);		
		this.set("tracks", this.playlistsList);
		this.set("searchtracks", this.searchTrackList);
	}

	public destroyInterval(): void {
		if (this.interval) {
			clearInterval(this.interval);
			this.interval = null;
		}
	}

	public async refresh(populateListView: boolean): Promise<void> {
		this.playlist = (await AppRootViewModel.instance.playlistApiCaller.getPlaylist(this.playlist.id, true)) as Playlist;
		if (populateListView) {
			this.fillTracks(this.playlist.tracks);
		}
	}

	public toggleRemoveIcon(index, active: boolean): void {		
		if (this.rights == 'guest') {
			return;
		}

		if (this.dragging) {
			return;
		}
		const item = this.playlistsList.getItem(index);

		item['removeIcon'] = active ? IconsCode.Remove : null;
	}

	public removeTrack(args: ListViewEventData): void {
		let track: Track = args.object.bindingContext as Track;
		if (track.removeIcon == null) {
			return;
		}
		let index = this.playlistsList.indexOf(args.object.bindingContext);
		this.playlistsList.splice(index, 1);

		MusicRoomApi.instance.delete(MusicRoomApi.playlists + this.playlist.id + '/' + MusicRoomApi.tracks + track.id, {}, response => {
		}, error => {
		});
	}

	public addTrack(args: ListViewEventData): void {
		let track: Track = args.object.bindingContext as Track;
		if (track.addIcon == IconsCode.Plus) {
			
			track.addIcon = IconsCode.Remove;
			MusicRoomApi.instance.put(MusicRoomApi.playlists + this.playlist.id + '/' + MusicRoomApi.tracks, {
				serviceid: track.serviceid,
			}, response => {
				track.id = response.id;
			}, error => {
				if (error.errorMessage) {
					Toast.makeText(error.errorMessage).show();
					track.addIcon = IconsCode.Plus;
				}
			});
		} else {
			let index = -1;
			this.playlistsList.filter((element, idx) => {
				if (element["serviceid"] == track.serviceid) {
					index = idx;
				}
				return element["serviceid"] == track.serviceid;
			})
			if (index != -1) {
				this.playlistsList.splice(index, 1);
			}
			MusicRoomApi.instance.delete(MusicRoomApi.playlists + this.playlist.id + '/' + MusicRoomApi.tracks + track.id, {}, response => {
			}, error => {
			});
			track.addIcon = IconsCode.Plus;
		}

	}

	public search(keyword: string): void {
		if (keyword.length == 0) {
			this.clearSearchList();
			return ;
		} else if (keyword.length < 3) {
			return ;
		}

		this.deezerApi.get(DeezerApi.searchEP, response => {
			this.fillSearchTracksList(response.data);

		}, error => {
		}, {
			q: keyword
		})
	}

	public fillSearchTracksList(data: any): void {
		if (!this.locked) {
			this.locked = true;
			this.clearSearchList();
			data.forEach(track => {
	
				if (this.playlistsList.some((playlistTrack: any) => track.id == playlistTrack.serviceid)) {
					return ;
				}
	
				let trackDisplay: Track = {
					id: '',
					serviceid: track.id,
					name: track.title,
					album: track.album.title,
					artist: track.artist.name,
					coverImageURL: track.album.cover_medium,
					duration: track.duration,
					formatedDuration: toHHMMSS(track.duration.toString()),
					addIcon: IconsCode.Plus,
					reorderIcon: IconsCode.Bars,
					removeIcon: null
				};
				this.searchTrackList.push(observableFromObject(trackDisplay));
			});

			if (this.callbacks.length > 0) {
				this.locked = false;
				this.fillSearchTracksList(this.callbacks.pop());
			} else {
				this.locked = false;
			}
		} else {
			this.callbacks.push(data);
		}
	}

	public onItemReorderStart(args: ListViewEventData): void {
		this.dragging = true;
	}

	public onItemReordered(args: ListViewEventData): void {
		let track: any = this.playlistsList.getItem(args.data.targetIndex);

		this.dragging = false;
		MusicRoomApi.instance.put(MusicRoomApi.playlists + this.playlist.id + '/' + MusicRoomApi.tracks + track.id + '/' + MusicRoomApi.move, {
			index: args.data.targetIndex,
		}, response => {
		}, error => {
			if (error.errorMessage) {
				Toast.makeText(error.errorMessage).show();
			}
		});
	}	

	public async delete(): Promise<void> {
		delete AppRootViewModel.instance.playlists.owner[this.playlist.id];
		this.set("loading", true);
		await MusicRoomApi.instance.delete(MusicRoomApi.playlists + this.playlist.id, {}, response => {
		}, error => {
		});
		this.set("loading", false);
	}

	public clearSearchList(): void {
		this.searchTrackList.length = 0;
	}

	private getIndexByTrackID(trackID: string): number {
		let index: number = -1;
		this.playlistsList.filter((track, idx) => {
			if (track['id'] == trackID) {
				index = idx;
				return true;
			}
			return false;
		})
		return index;
	}
	
	public destroySocketIO(): void {
		this.socketIO.disconnect();
	}
}
