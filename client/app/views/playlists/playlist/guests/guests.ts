import { EventData, Observable } from "tns-core-modules/data/observable";
import { Page, View } from "tns-core-modules/ui/page";
import { StackLayout } from "tns-core-modules/ui/layouts/stack-layout";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { RadListView, ListViewEventData, SwipeActionsEventData } from "nativescript-ui-listview";
import { TextField } from "tns-core-modules/ui/text-field/text-field";
import { PlaylistGuestsViewModel, GuestSearch } from "~/views/playlists/playlist/guests/guests-view-model";
import { IconsCode } from "~/utils/Icons";
import { ItemEventData } from "tns-core-modules/ui/list-view/list-view";
import { GuestList } from "~/views/events/event/guests/guests-view-model";
import * as frameModule from "tns-core-modules/ui/frame"

let page: Page = null;
let playlistGuestsViewModel: PlaylistGuestsViewModel = null;
let drawer: RadSideDrawer = null;
let guestsListView: RadListView = null;
let searchLayout: StackLayout = null;
let displayed: boolean = false;
let rlvGuests: RadListView = null;
let rlvSearch: RadListView = null;
let onMultipleSelection: boolean = false;
let onMultipleSelectionSearch: boolean = false;

export function navigatingTo(args: EventData): void {
	drawer = <RadSideDrawer>frameModule.topmost().parent;

	page = <Page>args.object;
	onMultipleSelection = false;
	onMultipleSelectionSearch = false;
	
	let context = page.navigationContext;
	if (context) {
		playlistGuestsViewModel = new PlaylistGuestsViewModel(context.playlist);
		playlistGuestsViewModel.fillGuestsList();
		playlistGuestsViewModel.set("actionBarTitle", context.playlist.name + " Guests");
		playlistGuestsViewModel.set("menuIcon", "res://menu_light");
		page.bindingContext = playlistGuestsViewModel;
	}

	guestsListView = page.getViewById("guestsListView");

	searchLayout = <StackLayout>page.getViewById("search-layout");
	searchLayout.visibility = "collapse";
	displayed = false;

	rlvGuests = page.getViewById("guestsListView");
	rlvSearch = page.getViewById("searchGuestsListView");
	rlvSearch.height = 0;
	rlvGuests.height = "auto";

	searchGuestsEvent();
}

exports.backEvent = args => {
	if (displayed) {
		closeSearchMenu();
		args.cancel = true;
	}
}

export function pullRefreshPlaylist(args: ListViewEventData): void {
	rlvGuests.deselectAll();
	playlistGuestsViewModel.refresh(true);
	guestsListView.notifyPullToRefreshFinished();
}


export function searchNewGuests(args: EventData): void {
	if (searchLayout) {
		toggleSearchMenu();	
	}
}

function toggleSearchMenu() {
	if (!displayed) {
		openSearchMenu();
	} else {
		closeSearchMenu();
	}
}

function openSearchMenu() {
	rlvGuests.deselectAll();
	rlvSearch.height = "auto";
	rlvGuests.height = 0;
	displayed = true;
	searchLayout.visibility = "visible";
	playlistGuestsViewModel.clearSearchList(true);
}

function closeSearchMenu() {
	rlvSearch.deselectAll();
	rlvSearch.height = 0;
	rlvGuests.height = "auto";
	searchLayout.visibility = "collapse";
	
	let searchGuestsTF: TextField = page.getViewById("search_guests");
	searchGuestsTF.text = "";
	playlistGuestsViewModel.clearSearchList(true);
	displayed = false;
}


export function onItemSelected(args: ItemEventData): void {
	let guest: GuestSearch = playlistGuestsViewModel.getObservableGuest(args.index) as any;
	if (guest) {
		guest.selectedClass = 'list-group-item-selected';
	}
	playlistGuestsViewModel.set("actionBarTitle", "Remove Guests (" + rlvGuests.getSelectedItems().length + ")");
	
	if (!onMultipleSelection) {
		playlistGuestsViewModel.set("menuIcon", "res://back_arrow");
		playlistGuestsViewModel.set("confirmIcon", IconsCode.Remove);
		onMultipleSelection = true;
	}
}

export function onItemDeselected(args: ItemEventData): void {
	let guest: any = playlistGuestsViewModel.getObservableGuest(args.index);
	if (guest) {
		guest.selectedClass = '';
	}

	playlistGuestsViewModel.set("actionBarTitle", "Remove Guests (" + rlvGuests.getSelectedItems().length + ")");
	if (rlvGuests.getSelectedItems().length == 0) {
		onMultipleSelection = false;
		playlistGuestsViewModel.set("menuIcon", "res://menu_light");
		playlistGuestsViewModel.set("actionBarTitle", playlistGuestsViewModel.getPlaylist().name + " Guests");
		playlistGuestsViewModel.set("confirmIcon", "");
	}
}

export function onItemTap(args: ItemEventData): void {
	if (onMultipleSelection) {
		if (rlvGuests.getSelectedItems().indexOf(args.view.bindingContext) == -1) {
			rlvGuests.selectItemAt(args.index);
			
		} else {
			rlvGuests.deselectItemAt(args.index);
		}
	}
}


export function onItemSelectedSearch(args: ItemEventData): void {
	let guest: any = playlistGuestsViewModel.getObservableGuestSearch(args.index);

	guest.selectedClass = "list-group-item-selected";
	playlistGuestsViewModel.set("actionBarTitle", "Add Guests (" + rlvSearch.getSelectedItems().length + ")");
	if (!onMultipleSelectionSearch) {
		playlistGuestsViewModel.set("menuIcon", "res://back_arrow");
		playlistGuestsViewModel.set("confirmIcon", IconsCode.Check);
		onMultipleSelectionSearch = true;
	}
}

export function onItemDeselectedSearch(args: ItemEventData): void {
	let guest: any = playlistGuestsViewModel.getObservableGuestSearch(args.index);
	if (guest) {
		guest.selectedClass = '';
	}

	playlistGuestsViewModel.set("actionBarTitle", "Add Guests (" + rlvSearch.getSelectedItems().length + ")");
	if (rlvSearch.getSelectedItems().length == 0) {
		onMultipleSelectionSearch = false;
		playlistGuestsViewModel.set("menuIcon", "res://menu_light");
		playlistGuestsViewModel.set("actionBarTitle", playlistGuestsViewModel.getPlaylist().name + " Guests");
		playlistGuestsViewModel.set("confirmIcon", "");
	}
}

export function onItemTapSearch(args: ItemEventData): void {
	if (onMultipleSelectionSearch) {
		if (rlvSearch.getSelectedItems().indexOf(args.view.bindingContext) == -1) {
			rlvSearch.selectItemAt(args.index);
			
		} else {
			rlvSearch.deselectItemAt(args.index);
		}
	}
}

export function onConfirm(args: ItemEventData): void {
	if (onMultipleSelectionSearch) {
		let selectedGuests: GuestSearch[] = rlvSearch.getSelectedItems();
		rlvSearch.deselectAll();
		playlistGuestsViewModel.addMultiple(selectedGuests);
	} else {
		let selectedGuests: GuestList[] = rlvGuests.getSelectedItems();
		rlvGuests.deselectAll();
		playlistGuestsViewModel.removeMultiple(selectedGuests);
	}
}



export function onSwipeCellStarted(args: SwipeActionsEventData) {
	const swipeLimits = args.data.swipeLimits;
	const swipeView = args.object;
	const leftItem = swipeView.getViewById<View>('left-delete-view');
	const rightItem = swipeView.getViewById<View>('right-delete-view');
	swipeLimits.left = leftItem.getMeasuredWidth();
	swipeLimits.right = rightItem.getMeasuredWidth();
	swipeLimits.threshold = leftItem.getMeasuredWidth() / 2;
}

export function onLeftSwipeClick(args: ListViewEventData) {
	let guest: any = args.object.bindingContext;
	playlistGuestsViewModel.remove(guest);
}

export function onRightSwipeClick(args: ListViewEventData) {
	let guest: any = args.object.bindingContext;
	playlistGuestsViewModel.remove(guest);
}

export function onSwipeSearchCellStarted(args: SwipeActionsEventData) {
	const swipeLimits = args.data.swipeLimits;
	const swipeView = args.object;
	const leftItem = swipeView.getViewById<View>('left-add-view');
	const rightItem = swipeView.getViewById<View>('right-add-view');
	swipeLimits.left = leftItem.getMeasuredWidth();
	swipeLimits.right = rightItem.getMeasuredWidth();
	swipeLimits.threshold = leftItem.getMeasuredWidth() / 2;
}

export function onLeftSearchSwipeClick(args: ListViewEventData) {
	let guest: GuestSearch = args.object.bindingContext;
	if (guest.requestSentIcon == IconsCode.UserPlus) {
		playlistGuestsViewModel.add(guest);
	} else {
		playlistGuestsViewModel.remove(guest);
	}
}

export function onRightSearchSwipeClick(args: ListViewEventData) {
	let guest: GuestSearch = args.object.bindingContext;
	if (guest.requestSentIcon == IconsCode.UserPlus) {
		playlistGuestsViewModel.add(guest);
	} else {
		playlistGuestsViewModel.remove(guest);
	}
}

function searchGuestsEvent(): void {
	let searchGuestsTF: TextField = page.getViewById("search_guests");
	searchGuestsTF.on("textChange", () => {
		playlistGuestsViewModel.search(searchGuestsTF.text);
	});
}

export function showSideDrawer(): void {
	if (drawer) {
		if (playlistGuestsViewModel.get("menuIcon") == "res://back_arrow") {
			if (onMultipleSelection) {
				rlvGuests.deselectAll();
			} else if (onMultipleSelectionSearch) {
				rlvSearch.deselectAll();
			}
		} else {
			drawer.showDrawer();
		}
	}
}
