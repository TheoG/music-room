import { Observable, fromObject as observableFromObject } from 'tns-core-modules/data/observable';
import { ObservableArray } from "tns-core-modules/data/observable-array/observable-array";
import { IconsCode } from "~/utils/Icons"
import { MusicRoomApi } from '~/utils/MusicRoomApi';
import { formatDisplayName } from '~/utils/Tools';
import { AppRootViewModel, FriendshipAPI, UserAPI, PlaylistAPI } from '~/app-root-view-model';
import { PlaylistRights } from '~/views/playlists/playlists-view-model';

export interface GuestSearch extends UserAPI {
	requestSentClass: string;
	requestSentIcon: string;
	selectedClass: string;
}

export interface GuestList extends UserAPI {
	leftActionClass: string;
	leftActionIcon: string;
	rightActionClass: string;
	rightActionIcon: string;
	selectedClass: string;
}

export class PlaylistGuestsViewModel extends Observable {

	private searchGuestsList: ObservableArray<Observable> = new ObservableArray();
	private guestsList: ObservableArray<Observable> = new ObservableArray();
	
	private playlist: any;

	private static readonly AddClass = "add-container";
	private static readonly RemoveClass = "delete-container";

	private rights: string;

	constructor(playlist: any) {
		super()
		this.playlist = playlist;
		this.set("searchGuests", this.searchGuestsList);
		this.set("guests", this.guestsList);

		this.fillSearchWithFriends();

		if (this.playlist.id in AppRootViewModel.instance.playlists.owner) {
			this.rights = PlaylistRights.Owner;
		} else if (this.playlist.id in AppRootViewModel.instance.playlists.editor) {
			this.rights = PlaylistRights.Editor;
		} else if (this.playlist.id in AppRootViewModel.instance.playlists.guest) {
			this.rights = PlaylistRights.Guest;
		}
	}

	public getObservableGuest(index: number): Observable {
		return this.guestsList.getItem(index);
	}

	public getObservableGuestSearch(index: number): Observable {
		return this.searchGuestsList.getItem(index);
	}

	public getPlaylist(): PlaylistAPI {
		return this.playlist;
	}

	private async fillSearchWithFriends(): Promise<void> {
		this.searchGuestsList.length = 0;
		let friendships = await AppRootViewModel.instance.friendshipApiCaller.getFriendships(false);
		
		friendships.forEach(friendship => {

			if (!friendship.pending) {
				let friend = friendship.users[0].id != AppRootViewModel.instance.me.id ? friendship.users[0] : friendship.users[1];
				
				if (this.guestsList.some((guest: any) => guest.id == friend.id)) {
					return ;
				}

				let displayName = formatDisplayName(friend);
				let guestSearch: GuestSearch = {
					...friend,
					displayName,
					avatar: MusicRoomApi.baseUrl + ':' + MusicRoomApi.port + '/' + friend.avatar + '?' + new Date().toISOString(),
					requestSentClass: PlaylistGuestsViewModel.AddClass,
					requestSentIcon: IconsCode.UserPlus,
					selectedClass: ''
				};
				this.searchGuestsList.push(observableFromObject(guestSearch));
			}
		});
	}

	public async refresh(populateListView: boolean): Promise<void> {
		this.playlist = await AppRootViewModel.instance.playlistApiCaller.getPlaylist(this.playlist.id, populateListView);
		this.fillGuestsList();
	}

	public async search(keyword: string): Promise<void> {
		if (keyword.length == 0) {
			this.clearSearchList(true);
			return ;
		}
		if (keyword.length < 3) {
			this.clearSearchList(false);
			return ;
		}
		await MusicRoomApi.instance.get(MusicRoomApi.userSearch, response => {
			if (!response.hasOwnProperty("errorCode")) {
				this.fillSearchGuestsList(response.results);
			}
		}, error => {
		}, {
			keyword
		});
	}

	public fillGuestsList(): void {
		this.clearGuestsList();

		this.playlist.guests.forEach(guest => {
			let displayName = formatDisplayName(guest);

			let guestElement: GuestList = {
				...guest,
				displayName,
				avatar: MusicRoomApi.baseUrl + ':' + MusicRoomApi.port + '/' + guest.avatar + '?' + new Date().toISOString(),
				leftActionClass: PlaylistGuestsViewModel.RemoveClass,
				leftActionIcon: IconsCode.UserTimes,
				rightActionClass: PlaylistGuestsViewModel.RemoveClass,
				rightActionIcon: IconsCode.UserTimes,
				selectedClass: ''
			};
			this.guestsList.push(observableFromObject(guestElement));
		});
	}


	private fillSearchGuestsList(guests: any[]): void {
		this.clearSearchList(false);

		guests.forEach(guest => {

			// If already in the list
			if (this.guestsList.some((o: any) => o.id == guest.id)) {
				return ;
			}

			let displayName = formatDisplayName(guest);
			let guestSearch: GuestSearch = {
				...guest,
				displayName,
				avatar: MusicRoomApi.baseUrl + ':' + MusicRoomApi.port + '/' + guest.avatar + '?' + new Date().toISOString(),
				requestSentClass: PlaylistGuestsViewModel.AddClass,
				requestSentIcon: IconsCode.UserPlus,
				selectedClass: ''
			};
			this.searchGuestsList.push(observableFromObject(guestSearch));
		});
	}

	public add(guest: GuestSearch): void {

		let newGuest: UserAPI = {
			id: guest.id,
			avatar: guest.avatar,
			theme: null,
			apiKey: null,
			isPremium: guest.isPremium,
			email: guest.email,
			displayName: guest.displayName,
			firstName: guest.firstName,
			lastName: guest.lastName,
			friendships: guest.friendships,
			friends: guest.friends,
			playlists: guest.playlists,
			musicalPreferences: guest.musicalPreferences,
			facebookid: ''
		}

		let newGuestList: GuestList = {
			...newGuest,
			leftActionIcon: IconsCode.UserTimes,
			rightActionClass: PlaylistGuestsViewModel.RemoveClass,
			rightActionIcon: IconsCode.UserTimes,
			leftActionClass: PlaylistGuestsViewModel.RemoveClass,
			selectedClass: '',
		}

		this.guestsList.push(observableFromObject(newGuestList));
		(<UserAPI[]>AppRootViewModel.instance.playlists[this.rights][this.playlist.id].guests).push(newGuest);
		
		let index: number = -1;
		this.searchGuestsList.filter((g, idx) => {
			if (g['id'] == guest.id) {
				index = idx;
				return true;
			}
			return false;
		});
		if (index != -1) {
			this.searchGuestsList.splice(index, 1);
		}

		MusicRoomApi.instance.put(MusicRoomApi.playlists + this.playlist.id + '/' + MusicRoomApi.guests + guest.id, {
		}, response => {
		}, error => {
		});
	}
	
	public addMultiple(guests: GuestSearch[]): void {
		let guestsids: string[] = [];

		guests.forEach(guest => {

			guest.requestSentIcon = IconsCode.UserTimes;
			guest.requestSentClass = PlaylistGuestsViewModel.RemoveClass;
			
			let newEditor: UserAPI = {
				id: guest.id,
				avatar: guest.avatar,
				theme: null,
				apiKey: null,
				isPremium: guest.isPremium,
				email: guest.email,
				displayName: guest.displayName,
				firstName: guest.firstName,
				lastName: guest.lastName,
				friendships: guest.friendships,
				friends: guest.friends,
				playlists: guest.playlists,
				musicalPreferences: guest.musicalPreferences,
				facebookid: ''
			};
			
			let newEditorList: GuestList = {
				...newEditor,
				leftActionIcon: IconsCode.UserTimes,
				rightActionClass: PlaylistGuestsViewModel.RemoveClass,
				rightActionIcon: IconsCode.UserTimes,
				leftActionClass: PlaylistGuestsViewModel.RemoveClass,
				selectedClass: '',
			}

			newEditor.avatar = 'avatars/' + newEditor.avatar.split('/avatars/').pop();

			this.guestsList.push(observableFromObject(newEditorList));

			let index: number = -1;
			this.searchGuestsList.filter((g, idx) => {
				if (g['id'] == guest.id) {
					index = idx;
					return true;
				}
				return false;
			});
			if (index != -1) {
				this.searchGuestsList.splice(index, 1);
			}

			(<UserAPI[]>AppRootViewModel.instance.playlists[this.rights][this.playlist.id].guests).push(newEditor);
			guestsids.push(guest.id);
		});
		MusicRoomApi.instance.put(MusicRoomApi.playlists + this.playlist.id + '/' + MusicRoomApi.guests, {
			userids: guestsids
		}, response => {
		}, error => {
		});
	}

	public remove(guest: any): void {
		if (guest.hasOwnProperty('requestSentIcon')) {
			let guestSearch: GuestSearch = guest as GuestSearch;
			guestSearch.requestSentIcon = IconsCode.UserPlus;
			guestSearch.requestSentClass = PlaylistGuestsViewModel.AddClass;
			let index = -1;
			this.guestsList.filter((g: any, idx) => {
				if (g.id == guest.id) {
					index = idx;
					return true;
				}
				return false;
			});
			if (index != -1) {
				this.guestsList.splice(index, 1);
			}
		} else {
			this.guestsList.splice(this.guestsList.indexOf(guest), 1);
		}
		
		
		let index: number = -1;
		(<any[]>AppRootViewModel.instance.playlists[this.rights][this.playlist.id].guests).forEach((g: any, idx) => {
			if (typeof g == 'string' && guest.id == g) {
				index = idx;
			} else if (guest.id == g.id) {
				index = idx;
			}
		});

		if (index > -1) {
			AppRootViewModel.instance.playlists[this.rights][this.playlist.id].guests.splice(index, 1);
		}

		MusicRoomApi.instance.delete(MusicRoomApi.playlists + this.playlist.id + '/' + MusicRoomApi.guests + guest.id, {}, response => {
		}, error => {
		});
	}

	public removeMultiple(guests: GuestList[]): void {
		let guestsids: string[] = [];

		guests.forEach(guest => {
		
			guestsids.push(guest.id);

			let index: number = -1;
			this.guestsList.filter((e: any, idx) => {
				if (e.id == guest.id) {
					index = idx;
					return true;
				}
				return false;
			});
			if (index != -1) {
				this.guestsList.splice(index, 1);
			}
		});

		for (let i: number = 0; i < guestsids.length; i++) {

			let index: number = -1;

			(<any[]>AppRootViewModel.instance.playlists[this.rights][this.playlist.id].guests).forEach((g, idx) => {
				if (typeof g == 'string') {
					index = guestsids.indexOf(g);
				} else {
					index = guestsids.indexOf(g.id);
				}
			});

			if (index > -1) {
				AppRootViewModel.instance.playlists[this.rights][this.playlist.id].guests.splice(index, 1);
			}
		}

		MusicRoomApi.instance.delete(MusicRoomApi.playlists + this.playlist.id + '/' + MusicRoomApi.guests, {
			userids: guestsids
		}, response => {
		}, error => {
		})
	}

	public clearGuestsList(): void {
		this.guestsList.length = 0;
	}

	public clearSearchList(fillFriends: boolean): void {
		this.searchGuestsList.length = 0;
		if (fillFriends) {
			this.fillSearchWithFriends();
		}
	}
}