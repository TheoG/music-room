import { Observable } from "tns-core-modules/ui/page/page";
import { ObservableArray } from "tns-core-modules/data/observable-array/observable-array";
import { MusicRoomApi } from "~/utils/MusicRoomApi";
import { Playlist } from "~/views/playlists/playlists-view-model";
import { AppRootViewModel } from "~/app-root-view-model";
import * as Toast from 'nativescript-toast';

export class PlaylistParamsViewModel extends Observable {

	private sortingTypes: ObservableArray<any> = new ObservableArray();

	constructor() {
		super();
	}

	public fillSortingTypes() {
		this.sortingTypes.length = 0;
		this.sortingTypes.push("Like");
		this.sortingTypes.push("Move System");
 
		this.set("sortingTypes", this.sortingTypes);
		this.set("sortingTypesSelectedIndex", 0);
	}

	public async createPlaylist(name: string, isPublic: boolean, editingIsPublic: boolean, sortingType: number): Promise<Playlist> {
		let playlist: Playlist = null;
		await MusicRoomApi.instance.post(MusicRoomApi.playlists, {
			name,
			visibility: isPublic ? 'PUBLIC' : 'PRIVATE',
			editing: editingIsPublic ? 'PUBLIC' : 'PRIVATE',
			sortingType: sortingType == 0 ? 'vote' : 'move'
		}, response => {
			if (!response.hasOwnProperty("errorCode")) {
				playlist = response;
				AppRootViewModel.instance.playlists.owner[response.id] = response;
			}
		}, error => {
		});
		return playlist;
	}

	public async updatePlaylist(playlistId: string, name: string, isPublic: boolean, editingIsPublic: boolean, sortingType: number): Promise<Playlist> {
		let playlist: Playlist = null;

		await MusicRoomApi.instance.put(MusicRoomApi.playlists + playlistId, {
			name,
			visibility: isPublic ? 'PUBLIC' : 'PRIVATE',
			editing: editingIsPublic ? 'PUBLIC' : 'PRIVATE',
			sortingType: sortingType == 0 ? 'vote' : 'move'
		}, response => {
			if (!response.id) {
				Toast.makeText(response.message).show();
			} else {
				playlist = response;
				AppRootViewModel.instance.playlists.owner[playlist.id] = playlist;
			}
		}, error => {
		});

		return playlist;
	}

}