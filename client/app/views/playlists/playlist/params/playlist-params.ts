import { EventData } from "tns-core-modules/data/observable";
import { Page } from "tns-core-modules/ui/page";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { PlaylistParamsViewModel } from "~/views/playlists/playlist/params/playlist-params-view-model";
import { Label } from "tns-core-modules/ui/label/label";
import { TextField } from "tns-core-modules/ui/text-field/text-field";
import { Switch } from "tns-core-modules/ui/switch";
import { DropDown } from "nativescript-drop-down"
import { Playlist } from "~/views/playlists/playlists-view-model";

import * as frameModule from "tns-core-modules/ui/frame"


let drawer: RadSideDrawer = null;
let page: Page = null;
let playlistParamsViewModel: PlaylistParamsViewModel = new PlaylistParamsViewModel();
let playlist: Playlist = null;

export function navigatingTo(args: EventData): void {
	drawer = <RadSideDrawer>frameModule.topmost().parent;

	page = <Page>args.object;
	
	page.bindingContext = playlistParamsViewModel;

	playlistParamsViewModel.fillSortingTypes();

	let context = page.navigationContext;
	if (context) {
		if (context.isNew) {
			playlist = null;
			playlistParamsViewModel.set("actionBarTitle", "New Playlist");
		} else {
			let playlistName: TextField = <TextField>page.getViewById("playlistName");
			let playlistPrivacy: Switch = <Switch>page.getViewById("playlistPrivacy");
			let playlistEditing: Switch = <Switch>page.getViewById("playlistEditing");
			let playlistSortingType: DropDown = <DropDown>page.getViewById("playlistSortingType");

			playlist = context.playlist;
			playlistParamsViewModel.set("actionBarTitle", "Update playlist");
			playlistName.text = playlist.name;
			playlistPrivacy.checked = playlist.visibility == "PUBLIC";
			playlistEditing.checked = playlist.editing == "PUBLIC";
			playlistSortingType.selectedIndex = playlist.sortingType == 'vote' ? 0 : 1;
		}
	}
}

export async function saveChanges(): Promise<void> {
	let errorMessage: Label = <Label>page.getViewById("error-message");
	let playlistName: TextField = <TextField>page.getViewById("playlistName");
	let playlistPrivacy: Switch = <Switch>page.getViewById("playlistPrivacy");
	let playlistEditing: Switch = <Switch>page.getViewById("playlistEditing");
	let playlistSortingType: DropDown = <DropDown>page.getViewById("playlistSortingType");

	if (playlistName.text.length == 0) {
		errorMessage.className = 'display-error'
		return ;
	}

	if (playlistParamsViewModel.get("loading") == true) {
		return ;
	}

	playlistParamsViewModel.set("loading", true);

	playlistName.isEnabled = false;
	playlistPrivacy.isEnabled = false;
	playlistEditing.isEnabled = false;
	playlistSortingType.isEnabled = false;

	if (playlist == null) {
		let playlist = await playlistParamsViewModel.createPlaylist(playlistName.text, playlistPrivacy.checked, playlistEditing.checked, playlistSortingType.selectedIndex);
		frameModule.topmost().navigate({
			moduleName: "views/playlists/playlist/playlist",
			context: {
				playlist,
				wasInParam: true
			}
		});
	} else {
		let updatedPlaylist = await playlistParamsViewModel.updatePlaylist(playlist.id, playlistName.text, playlistPrivacy.checked, playlistEditing.checked, playlistSortingType.selectedIndex);
		if (updatedPlaylist) {
			frameModule.topmost().navigate({
				moduleName: "views/playlists/playlist/playlist",
				context: {
					playlist: updatedPlaylist,
					wasInParam: true
				}
			});
		}
	}
	playlistName.isEnabled = true;
	playlistPrivacy.isEnabled = true;
	playlistEditing.isEnabled = true;
	playlistSortingType.isEnabled = true;

	playlistParamsViewModel.set("loading", false);
}

export function showSideDrawer(): void {
	if (drawer) {
		drawer.showDrawer();
	}
}