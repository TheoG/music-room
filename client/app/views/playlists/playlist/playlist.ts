import { EventData } from "tns-core-modules/data/observable";
import { Page, NavigatedData } from "tns-core-modules/ui/page";
import { StackLayout } from "tns-core-modules/ui/layouts/stack-layout";
import * as frameModule from "tns-core-modules/ui/frame"
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { PlaylistViewModel, TrackModel } from "./playlist-view-model";
import { RadListView, ListViewEventData } from "nativescript-ui-listview";
import { TextField } from "tns-core-modules/ui/text-field/text-field";
import * as dialogs from "tns-core-modules/ui/dialogs";

let page: Page = null;
let playlistViewModel: PlaylistViewModel = null;
let drawer: RadSideDrawer = null;
let tracksListView: RadListView = null;
let searchLayout: StackLayout = null;
let displayed: boolean = false;
let wasInParam: boolean = false;
let rlvTracks: RadListView = null;
let rlvSearch: RadListView = null;

export function navigatingTo(args: EventData): void {
	drawer = <RadSideDrawer>frameModule.topmost().parent;

	page = <Page>args.object;
	
	let context = page.navigationContext;
	if (context) {
		playlistViewModel = new PlaylistViewModel(context.playlist);
		page.bindingContext = playlistViewModel;

		playlistViewModel.refresh(true);
	}

	tracksListView = page.getViewById("tracksListView");

	searchLayout = <StackLayout>page.getViewById("search-layout");
	searchLayout.visibility = "collapse";
	displayed = false;

	rlvTracks = page.getViewById("tracksListView");
	rlvSearch = page.getViewById("searchTracksListView");

	rlvSearch.height = 0;
	rlvTracks.height = "auto";
	
	if (context.wasInParam) {
		wasInParam = context.wasInParam;
	}

	searchTracksEvent();
}

export function navigatedFrom(args: NavigatedData): void {
	playlistViewModel.destroySocketIO();
	playlistViewModel.destroyInterval();
	gc();

}

exports.backEvent = args => {
	if (wasInParam) {
		frameModule.topmost().navigate({
			moduleName: "views/playlists/playlists"
		});
		args.cancel = true;
	} else if (displayed) {
		closeSearchMenu();
		args.cancel = true;
	}
}

export function pullRefreshPlaylist(args: ListViewEventData): void {
	playlistViewModel.refresh(true);
	tracksListView.notifyPullToRefreshFinished();
}

export function onItemSelected(args: ListViewEventData) {
	playlistViewModel.toggleRemoveIcon(args.index, true);
}

export function onItemDeselected(args: ListViewEventData) {
	playlistViewModel.toggleRemoveIcon(args.index, false);
}

function toggleSearchMenu() {
	if (!displayed) {
		openSearchMenu();
	} else {
		closeSearchMenu();
	}
}


function openSearchMenu() {
	rlvTracks.deselectAll();
	rlvSearch.height = "auto";
	rlvTracks.height = 0;
	displayed = true;
	searchLayout.visibility = "visible";
}

function closeSearchMenu() {
	rlvSearch.deselectAll();
	rlvSearch.height = 0;
	rlvTracks.height = "auto";
	searchLayout.visibility = "collapse";
	
	let searchTrackTF: TextField = page.getViewById("search_tracks");
	searchTrackTF.text = "";
	playlistViewModel.clearSearchList();
	displayed = false;
}


export function searchNewTracks(args: EventData): void {
	if (searchLayout) {
		toggleSearchMenu();	
	}
}

export function goToParameters(): void {
	frameModule.topmost().navigate({
		moduleName: "views/playlists/playlist/params/playlist-params",
		context: {
			isNew: false,
			playlist: playlistViewModel.playlist
		}
	});

}

export function manageGuests(): void {
	frameModule.topmost().navigate({
		moduleName: "views/playlists/playlist/guests/guests",
		context: {
			playlist: playlistViewModel.playlist
		}
	});
}

export function manageEditors(): void {
	frameModule.topmost().navigate({
		moduleName: "views/playlists/playlist/editors/editors",
		context: {
			playlist: playlistViewModel.playlist
		}
	});
}

export function onDelete(): void {
	dialogs.confirm({
		title: "Delete Playlist",
		message: "Are you sure?",
		okButtonText: "Yes",
		cancelButtonText: "Wooops, no!",
	}).then(async result => {
		if (result == true) {
			await playlistViewModel.delete();
			frameModule.topmost().navigate({
				moduleName: "views/playlists/playlists"
			});
		}
	});
}

export function showSideDrawer(): void {
	if (drawer) {
		drawer.showDrawer();
	}
}

function searchTracksEvent(): void {
	let searchTracksTF: TextField = page.getViewById("search_tracks");
	searchTracksTF.on("textChange", () => {
		playlistViewModel.search(searchTracksTF.text);
	});
}
