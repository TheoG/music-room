import { Observable, fromObject as observableFromObject } from 'tns-core-modules/data/observable';
import { ObservableArray } from "tns-core-modules/data/observable-array/observable-array";
import { IconsCode } from "~/utils/Icons"
import { MusicRoomApi } from '~/utils/MusicRoomApi';
import { formatDisplayName } from '~/utils/Tools';
import { AppRootViewModel, FriendshipAPI, PlaylistAPI, UserAPI } from '~/app-root-view-model';
import { PlaylistRights } from '~/views/playlists/playlists-view-model';
import * as dialogs from "tns-core-modules/ui/dialogs";

export interface EditorSearch extends UserAPI {
	requestSentClass: string;
	requestSentIcon: string;
	selectedClass: string;
}

export interface EditorList extends UserAPI {
	leftActionClass: string;
	leftActionIcon: string;
	rightActionClass: string;
	rightActionIcon: string;
	selectedClass: string;
}

export class PlaylistEditorsViewModel extends Observable {

	private searchEditorsList: ObservableArray<Observable> = new ObservableArray();
	private editorsList: ObservableArray<Observable> = new ObservableArray();
	
	private playlist: any;

	private static readonly AddClass = "add-container";
	private static readonly RemoveClass = "delete-container";

	private rights: string;

	constructor(playlist: any) {
		super()
		this.playlist = playlist;
		this.set("searchEditors", this.searchEditorsList);
		this.set("editors", this.editorsList);

		this.fillSearchWithFriends();

		if (this.playlist.id in AppRootViewModel.instance.playlists.owner) {
			this.rights = PlaylistRights.Owner;
		} else if (this.playlist.id in AppRootViewModel.instance.playlists.editor) {
			this.rights = PlaylistRights.Editor;
		} else if (this.playlist.id in AppRootViewModel.instance.playlists.guest) {
			this.rights = PlaylistRights.Guest;
		}
	}

	public getObservableEditor(index: number): Observable {
		return this.editorsList.getItem(index);
	}

	public getObservableEditorSearch(index: number): Observable {
		return this.searchEditorsList.getItem(index);
	}

	public getPlaylist(): PlaylistAPI {
		return this.playlist;
	}

	private async fillSearchWithFriends(): Promise<void> {
		this.searchEditorsList.length = 0;
		let friendships = await AppRootViewModel.instance.friendshipApiCaller.getFriendships(false);
		
		friendships.forEach(friendship => {

			if (!friendship.pending) {
				let friend = friendship.users[0].id != AppRootViewModel.instance.me.id ? friendship.users[0] : friendship.users[1];
				
				if (this.editorsList.some((guest: any) => guest.id == friend.id)) {
					return ;
				}

				let displayName = formatDisplayName(friend);
				let guestSearch: EditorSearch = {
					...friend,
					displayName,
					avatar: MusicRoomApi.baseUrl + ':' + MusicRoomApi.port + '/' + friend.avatar + '?' + new Date().toISOString(),
					requestSentClass: PlaylistEditorsViewModel.AddClass,
					requestSentIcon: IconsCode.UserPlus,
					selectedClass: ''
				};
				this.searchEditorsList.push(observableFromObject(guestSearch));
			}
		});
	}

	public async refresh(populateListView: boolean): Promise<void> {
		this.playlist = await AppRootViewModel.instance.playlistApiCaller.getPlaylist(this.playlist.id, populateListView);
		this.fillEditorsList();
	}

	public async search(keyword: string): Promise<void> {
		if (keyword.length == 0) {
			this.clearSearchList(true);
			return ;
		}
		if (keyword.length < 3) {
			this.clearSearchList(false);
			return ;
		}
		await MusicRoomApi.instance.get(MusicRoomApi.userSearch, response => {
			if (!response.hasOwnProperty("errorCode")) {
				this.fillSearchEditorsList(response.results);
			}
		}, error => {
		}, {
			keyword
		});
	}

	public fillEditorsList(): void {
		this.clearEditorsList();

		this.playlist.editors.forEach(editor => {
			let displayName = formatDisplayName(editor);
			let editorElement: EditorList = {
				...editor,
				displayName,
				avatar: MusicRoomApi.baseUrl + ':' + MusicRoomApi.port + '/' + editor.avatar + '?' + new Date().toISOString(),
				leftActionClass: PlaylistEditorsViewModel.RemoveClass,
				leftActionIcon: IconsCode.UserTimes,
				rightActionClass: PlaylistEditorsViewModel.RemoveClass,
				rightActionIcon: IconsCode.UserTimes,
				selectedClass: ''
			};
			this.editorsList.push(observableFromObject(editorElement));
		});
	}


	private fillSearchEditorsList(editors: any[]): void {
		this.clearSearchList(false);

		editors.forEach(editor => {

			// If already in the list
			if (this.editorsList.some((o: any) => o.id == editor.id)) {
				return ;
			}

			let displayName = formatDisplayName(editor);
			let editorSearch: EditorSearch = {
				...editor,
				displayName,
				avatar: MusicRoomApi.baseUrl + ':' + MusicRoomApi.port + '/' + editor.avatar + '?' + new Date().toISOString(),				
				requestSentClass: PlaylistEditorsViewModel.AddClass,
				requestSentIcon: IconsCode.UserPlus,
				selectedClass: ''
			};
			this.searchEditorsList.push(observableFromObject(editorSearch));
		});
	}

	public add(editor: EditorSearch): void {
	
		let newEditor: UserAPI = {
			id: editor.id,
			avatar: editor.avatar,
			theme: null,
			apiKey: null,
			isPremium: editor.isPremium,
			email: editor.email,
			displayName: editor.displayName,
			firstName: editor.firstName,
			lastName: editor.lastName,
			friendships: editor.friendships,
			friends: editor.friends,
			playlists: editor.playlists,
			musicalPreferences: editor.musicalPreferences,
			facebookid: ''
		}

		let newEditorList: EditorList = {
			...newEditor,
			leftActionIcon: IconsCode.UserTimes,
			rightActionClass: PlaylistEditorsViewModel.RemoveClass,
			rightActionIcon: IconsCode.UserTimes,
			leftActionClass: PlaylistEditorsViewModel.RemoveClass,
			selectedClass: '',
		}

		this.editorsList.push(observableFromObject(newEditorList));
		(<UserAPI[]>AppRootViewModel.instance.playlists[this.rights][this.playlist.id].editors).push(newEditor);
		
		let index: number = -1;
		this.searchEditorsList.filter((g, idx) => {
			if (g['id'] == editor.id) {
				index = idx;
				return true;
			}
			return false;
		});
		if (index != -1) {
			this.searchEditorsList.splice(index, 1);
		}

		MusicRoomApi.instance.put(MusicRoomApi.playlists + this.playlist.id + '/' + MusicRoomApi.editors + editor.id, {
		}, response => {
		}, error => {
		});
	}

	public addMultiple(editors: EditorSearch[]): void {
		let editorsIds: string[] = [];

		editors.forEach(editor => {

			editor.requestSentIcon = IconsCode.UserTimes;
			editor.requestSentClass = PlaylistEditorsViewModel.RemoveClass;
			
			let newEditor: UserAPI = {
				id: editor.id,
				avatar: editor.avatar,
				theme: null,
				apiKey: null,
				isPremium: editor.isPremium,
				email: editor.email,
				displayName: editor.displayName,
				firstName: editor.firstName,
				lastName: editor.lastName,
				friendships: editor.friendships,
				friends: editor.friends,
				playlists: editor.playlists,
				musicalPreferences: editor.musicalPreferences,
				facebookid: ''
			};
			
			let newEditorList: EditorList = {
				...newEditor,
				leftActionIcon: IconsCode.UserTimes,
				rightActionClass: PlaylistEditorsViewModel.RemoveClass,
				rightActionIcon: IconsCode.UserTimes,
				leftActionClass: PlaylistEditorsViewModel.RemoveClass,
				selectedClass: '',
			}

			newEditor.avatar = 'avatars/' + newEditor.avatar.split('/avatars/').pop();

			this.editorsList.push(observableFromObject(newEditorList));

			let index: number = -1;
			this.searchEditorsList.filter((g, idx) => {
				if (g['id'] == editor.id) {
					index = idx;
					return true;
				}
				return false;
			});
			if (index != -1) {
				this.searchEditorsList.splice(index, 1);
			}

			(<UserAPI[]>AppRootViewModel.instance.playlists[this.rights][this.playlist.id].editors).push(newEditor);
			editorsIds.push(editor.id);
		});
		MusicRoomApi.instance.put(MusicRoomApi.playlists + this.playlist.id + '/' + MusicRoomApi.editors, {
			userids: editorsIds
		}, response => {
		}, error => {
		});
	}

	public remove(editor: any): void {
		if (editor.hasOwnProperty('requestSentIcon')) {
			let editorSearch: EditorSearch = editor as EditorSearch;
			editorSearch.requestSentIcon = IconsCode.UserPlus;
			editorSearch.requestSentClass = PlaylistEditorsViewModel.AddClass;
			let index = -1;
			this.editorsList.filter((g, idx) => {
				if (g['id'] == editor.id) {
					index = idx;
					return true;
				}
				return false;
			});
			if (index != -1) {
				this.editorsList.splice(index, 1);
			}
		} else {
			this.editorsList.splice(this.editorsList.indexOf(editor), 1);
		}

		let index: number = -1;
		(<any[]>AppRootViewModel.instance.playlists[this.rights][this.playlist.id].guests).forEach((e: any, idx) => {
			if (typeof e == 'string' && editor.id == e) {
				index = idx;
			} else if (editor.id == e.id) {
				index = idx;
			}
		});
		if (index > -1) {
			AppRootViewModel.instance.playlists[this.rights][this.playlist.id].editors.splice(index, 1);
		}

		MusicRoomApi.instance.delete(MusicRoomApi.playlists + this.playlist.id + '/' + MusicRoomApi.editors + editor.id, {}, response => {
		}, error => {
		})

		dialogs.confirm({
			title: "Delete Guest",
			message: "Do you want to remove this user form guests too?",
			okButtonText: "Yes",
			cancelButtonText: "No",
		}).then(result => {
			if (result == true) {
				MusicRoomApi.instance.delete(MusicRoomApi.playlists + this.playlist.id + '/' + MusicRoomApi.guests + editor.id, {}, response => {
				}, error => {
				})
			}
		});
	}

	public removeMultiple(editors: EditorList[]): void {
		let editorsIds: string[] = [];

		editors.forEach(editor => {
		
			editorsIds.push(editor.id);

			let index: number = -1;
			this.editorsList.filter((e: any, idx) => {
				if (e.id == editor.id) {
					index = idx;
					return true;
				}
				return false;
			});
			if (index != -1) {
				this.editorsList.splice(index, 1);
			}
		});

		for (let i: number = 0; i < editorsIds.length; i++) {

			let index: number = -1;

			(<any[]>AppRootViewModel.instance.playlists[this.rights][this.playlist.id].editors).forEach((g, idx) => {
				if (typeof g == 'string') {
					index = editorsIds.indexOf(g);
				} else {
					index = editorsIds.indexOf(g.id);
				}
			})

			if (index > -1) {
				AppRootViewModel.instance.playlists[this.rights][this.playlist.id].editors.splice(index, 1);
			}
		}


		MusicRoomApi.instance.delete(MusicRoomApi.playlists + this.playlist.id + '/' + MusicRoomApi.editors, {
			userids: editorsIds
		}, response => {
		}, error => {
		})
		dialogs.confirm({
			title: "Delete Guests",
			message: "Do you want to remove those users form guests too?",
			okButtonText: "Yes",
			cancelButtonText: "No",
		}).then(result => {
			if (result == true) {
				for (let i: number = 0; i < editorsIds.length; i++) {

					let index: number = -1;
		
					(<any[]>AppRootViewModel.instance.playlists[this.rights][this.playlist.id].guests).forEach((g, idx) => {
						if (typeof g == 'string') {
							index = editorsIds.indexOf(g);
						} else {
							index = editorsIds.indexOf(g.id);
						}
					})
		
					if (index > -1) {
						AppRootViewModel.instance.playlists[this.rights][this.playlist.id].guests.splice(index, 1);
					}
				}
		
				MusicRoomApi.instance.delete(MusicRoomApi.playlists + this.playlist.id + '/' + MusicRoomApi.guests, {
					userids: editorsIds
				}, response => {
				}, error => {
				})
			}
		});
	}

	public clearEditorsList(): void {
		this.editorsList.length = 0;
	}

	public clearSearchList(fillFriends: boolean): void {
		this.searchEditorsList.length = 0;
		if (fillFriends) {
			this.fillSearchWithFriends();
		}
	}
}