import { EventData, Observable } from "tns-core-modules/data/observable";
import { Page, View } from "tns-core-modules/ui/page";
import { StackLayout } from "tns-core-modules/ui/layouts/stack-layout";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { RadListView, ListViewEventData, SwipeActionsEventData } from "nativescript-ui-listview";
import { TextField } from "tns-core-modules/ui/text-field/text-field";
import { PlaylistEditorsViewModel, EditorSearch, EditorList } from "~/views/playlists/playlist/editors/editors-view-model";
import { IconsCode } from "~/utils/Icons";
import { ItemEventData } from "tns-core-modules/ui/list-view/list-view";
import * as frameModule from "tns-core-modules/ui/frame"

let page: Page = null;
let playlistEditorsViewModel: PlaylistEditorsViewModel = null;
let drawer: RadSideDrawer = null;
let editorsListView: RadListView = null;
let searchLayout: StackLayout = null;
let displayed: boolean = false;
let rlvEditors: RadListView = null;
let rlvSearch: RadListView = null;
let onMultipleSelection: boolean = false;
let onMultipleSelectionSearch: boolean = false;

export function navigatingTo(args: EventData): void {
	drawer = <RadSideDrawer>frameModule.topmost().parent;
	
	page = <Page>args.object;
	onMultipleSelection = false;
	onMultipleSelectionSearch = false;
	
	let context = page.navigationContext;
	if (context) {
		playlistEditorsViewModel = new PlaylistEditorsViewModel(context.playlist);
		playlistEditorsViewModel.fillEditorsList();
		playlistEditorsViewModel.set("actionBarTitle", context.playlist.name + " Editors");
		playlistEditorsViewModel.set("menuIcon", "res://menu_light");
		page.bindingContext = playlistEditorsViewModel;
	}
	editorsListView = page.getViewById("editorsListView");

	searchLayout = <StackLayout>page.getViewById("search-layout");
	searchLayout.visibility = "collapse";
	displayed = false;

	rlvEditors = page.getViewById("editorsListView");
	rlvSearch = page.getViewById("searchEditorsListView");
	rlvSearch.height = 0;
	rlvEditors.height = "auto";

	searchEditorsEvent();
}

exports.backEvent = args => {
	if (displayed) {
		closeSearchMenu();
		args.cancel = true;
	}
}

export function pullRefreshPlaylist(args: ListViewEventData): void {
	
	rlvEditors.deselectAll();
	playlistEditorsViewModel.refresh(true);
	editorsListView.notifyPullToRefreshFinished();

}

export function searchNewEditors(args: EventData): void {
	if (searchLayout) {
		toggleSearchMenu();	
	}
}

function toggleSearchMenu() {
	if (!displayed) {
		openSearchMenu();
	} else {
		closeSearchMenu();
	}
}

function openSearchMenu() {
	rlvEditors.deselectAll();
	rlvSearch.height = "auto";
	rlvEditors.height = 0;
	displayed = true;
	searchLayout.visibility = "visible";
	playlistEditorsViewModel.clearSearchList(true);
}

function closeSearchMenu() {
	rlvSearch.deselectAll();
	rlvSearch.height = 0;
	rlvEditors.height = "auto";
	searchLayout.visibility = "collapse";
	
	let searchEditorsTF: TextField = page.getViewById("search_editors");
	searchEditorsTF.text = "";
	playlistEditorsViewModel.clearSearchList(true);
	displayed = false;
}

export function onItemSelected(args: ItemEventData): void {
	let editor: EditorSearch = playlistEditorsViewModel.getObservableEditor(args.index) as any;
	if (editor) {
		editor.selectedClass = 'list-group-item-selected';
	}
	playlistEditorsViewModel.set("actionBarTitle", "Remove Editors (" + rlvEditors.getSelectedItems().length + ")");
	
	if (!onMultipleSelection) {
		playlistEditorsViewModel.set("menuIcon", "res://back_arrow");
		playlistEditorsViewModel.set("confirmIcon", IconsCode.Remove);
		onMultipleSelection = true;
	}
}

export function onItemDeselected(args: ItemEventData): void {
	let editor: any = playlistEditorsViewModel.getObservableEditor(args.index);
	if (editor) {
		editor.selectedClass = '';
	}

	playlistEditorsViewModel.set("actionBarTitle", "Remove Editors (" + rlvEditors.getSelectedItems().length + ")");
	if (rlvEditors.getSelectedItems().length == 0) {
		onMultipleSelection = false;
		playlistEditorsViewModel.set("menuIcon", "res://menu_light");
		playlistEditorsViewModel.set("actionBarTitle", playlistEditorsViewModel.getPlaylist().name + " Editors");
		playlistEditorsViewModel.set("confirmIcon", "");
	}
}

export function onItemTap(args: ItemEventData): void {
	if (onMultipleSelection) {
		if (rlvEditors.getSelectedItems().indexOf(args.view.bindingContext) == -1) {
			rlvEditors.selectItemAt(args.index);
			
		} else {
			rlvEditors.deselectItemAt(args.index);
		}
	}
}


export function onItemSelectedSearch(args: ItemEventData): void {
	let editor: any = playlistEditorsViewModel.getObservableEditorSearch(args.index);

	editor.selectedClass = "list-group-item-selected";
	playlistEditorsViewModel.set("actionBarTitle", "Add Editors (" + rlvSearch.getSelectedItems().length + ")");
	
	if (!onMultipleSelectionSearch) {
		playlistEditorsViewModel.set("menuIcon", "res://back_arrow");
		playlistEditorsViewModel.set("confirmIcon", IconsCode.Check);
		onMultipleSelectionSearch = true;
	}
}

export function onItemDeselectedSearch(args: ItemEventData): void {
	let editor: any = playlistEditorsViewModel.getObservableEditorSearch(args.index);
	if (editor) {
		editor.selectedClass = '';
	}

	playlistEditorsViewModel.set("actionBarTitle", "Add Editors (" + rlvSearch.getSelectedItems().length + ")");
	if (rlvSearch.getSelectedItems().length == 0) {
		onMultipleSelectionSearch = false;
		playlistEditorsViewModel.set("menuIcon", "res://menu_light");
		playlistEditorsViewModel.set("actionBarTitle", playlistEditorsViewModel.getPlaylist().name + " Editors");
		playlistEditorsViewModel.set("confirmIcon", "");
	}
}

export function onItemTapSearch(args: ItemEventData): void {
	if (onMultipleSelectionSearch) {
		if (rlvSearch.getSelectedItems().indexOf(args.view.bindingContext) == -1) {
			rlvSearch.selectItemAt(args.index);
		} else {
			rlvSearch.deselectItemAt(args.index);
		}
	}
}

export function onConfirm(args: ItemEventData): void {
	if (onMultipleSelectionSearch) {
		let selectedEditors: EditorSearch[] = rlvSearch.getSelectedItems();
		rlvSearch.deselectAll();
		playlistEditorsViewModel.addMultiple(selectedEditors);
	} else {
		let selectedEditors: EditorList[] = rlvEditors.getSelectedItems();
		rlvEditors.deselectAll();
		playlistEditorsViewModel.removeMultiple(selectedEditors);
	}
}


export function onSwipeCellStarted(args: SwipeActionsEventData) {
	const swipeLimits = args.data.swipeLimits;
	const swipeView = args.object;
	const leftItem = swipeView.getViewById<View>('left-delete-view');
	const rightItem = swipeView.getViewById<View>('right-delete-view');
	swipeLimits.left = leftItem.getMeasuredWidth();
	swipeLimits.right = rightItem.getMeasuredWidth();
	swipeLimits.threshold = leftItem.getMeasuredWidth() / 2;
}

export function onLeftSwipeClick(args: ListViewEventData) {
	let editor: any = args.object.bindingContext;
	playlistEditorsViewModel.remove(editor);
}

export function onRightSwipeClick(args: ListViewEventData) {
	let editor: any = args.object.bindingContext;
	playlistEditorsViewModel.remove(editor);
}

export function onSwipeSearchCellStarted(args: SwipeActionsEventData) {
	const swipeLimits = args.data.swipeLimits;
	const swipeView = args.object;
	const leftItem = swipeView.getViewById<View>('left-add-view');
	const rightItem = swipeView.getViewById<View>('right-add-view');
	swipeLimits.left = leftItem.getMeasuredWidth();
	swipeLimits.right = rightItem.getMeasuredWidth();
	swipeLimits.threshold = leftItem.getMeasuredWidth() / 2;
}

export function onLeftSearchSwipeClick(args: ListViewEventData) {
	let editor: EditorSearch = args.object.bindingContext;
	if (editor.requestSentIcon == IconsCode.UserPlus) {
		playlistEditorsViewModel.add(editor);
	} else {
		playlistEditorsViewModel.remove(editor);
	}
}

export function onRightSearchSwipeClick(args: ListViewEventData) {
	let editor: EditorSearch = args.object.bindingContext;
	if (editor.requestSentIcon == IconsCode.UserPlus) {
		playlistEditorsViewModel.add(editor);
	} else {
		playlistEditorsViewModel.remove(editor);
	}
}

function searchEditorsEvent(): void {
	let searchEditorsTF: TextField = page.getViewById("search_editors");
	searchEditorsTF.on("textChange", () => {
		playlistEditorsViewModel.search(searchEditorsTF.text);
	});
}

export function showSideDrawer(): void {
	if (drawer) {
		if (playlistEditorsViewModel.get("menuIcon") == "res://back_arrow") {
			if (onMultipleSelection) {
				rlvEditors.deselectAll();
			} else if (onMultipleSelectionSearch) {
				rlvSearch.deselectAll();
			}
		} else {
			drawer.showDrawer();
		}
	}
}
