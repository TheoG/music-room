import { ListView, ItemEventData } from "tns-core-modules/ui/list-view";
import { Page, View, NavigatedData, Observable } from "tns-core-modules/ui/page";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { PlaylistsViewModel, Playlist } from './playlists-view-model'
import { SwipeActionsEventData, ListViewEventData, RadListView } from "nativescript-ui-listview";
import { IconsCode } from "~/utils/Icons";
import { AppRootViewModel } from "~/app-root-view-model";
import { StringValues } from "~/utils/Values";
import * as Toast from "nativescript-toast";
import * as frameModule from "tns-core-modules/ui/frame"

let page: Page = null;
let playlistsViewModel: PlaylistsViewModel = null;
let drawer: RadSideDrawer = null;
let rlvPlaylists: RadListView = null;
let onMultipleSelection: boolean = false;
let canCallApi: boolean = false;

export function navigatingTo(args: NavigatedData): void {
	drawer = <RadSideDrawer>frameModule.topmost().parent;

	page = <Page>args.object;
	
	playlistsViewModel = new PlaylistsViewModel();
	page.bindingContext = playlistsViewModel;

	rlvPlaylists = page.getViewById("playlistsListView");
	onMultipleSelection = false;

	playlistsViewModel.set("actionBarTitle", "Playlists");
	playlistsViewModel.set("menuIcon", "res://menu_light");

	let context = page.navigationContext;
	let refresh: boolean = false;
	if (context) {
		refresh = context.refresh;
	}
	playlistsViewModel.fillPlaylists(refresh);
	canCallApi = true;
}

export function onConfirm(args: ItemEventData): void {
	if (onMultipleSelection) {
		let selectedPlaylists: Playlist[] = rlvPlaylists.getSelectedItems();
		rlvPlaylists.deselectAll();
		playlistsViewModel.deleteMultiple(selectedPlaylists);
	}
}

export function onItemSelected(args: ItemEventData): void {
	let playlist: Playlist = args.view.bindingContext;

	playlist.selectedClass = "list-group-item-selected";
	playlistsViewModel.set("actionBarTitle", "Remove Playlists (" + rlvPlaylists.getSelectedItems().length + ")");
	
	if (!onMultipleSelection) {
		playlistsViewModel.set("menuIcon", "res://back_arrow");
		playlistsViewModel.set("confirmIcon", IconsCode.Remove);
		onMultipleSelection = true;
	}
}

export function onItemDeselected(args: ItemEventData): void {
	let playlist: any = playlistsViewModel.getObservablePlaylist(args.index);
	if (playlist) {
		playlist.selectedClass = '';
	}

	playlistsViewModel.set("actionBarTitle", "Remove Playlists (" + rlvPlaylists.getSelectedItems().length + ")");
	if (rlvPlaylists.getSelectedItems().length == 0) {
		onMultipleSelection = false;
		playlistsViewModel.set("menuIcon", "res://menu_light");
		playlistsViewModel.set("actionBarTitle", "Playlists");
		playlistsViewModel.set("confirmIcon", "");
	}
}


export function onSwipeCellStarted(args: SwipeActionsEventData) {
	const swipeLimits = args.data.swipeLimits;
	const swipeView = args.object;
	const leftItem = swipeView.getViewById<View>('left-delete-view');
	const rightItem = swipeView.getViewById<View>('right-delete-view');
	swipeLimits.left = leftItem.getMeasuredWidth();
	swipeLimits.right = rightItem.getMeasuredWidth();
	swipeLimits.threshold = leftItem.getMeasuredWidth() / 2;
}

export function onLeftSwipeClick(args: ListViewEventData) {
	rlvPlaylists.notifySwipeToExecuteFinished();
	playlistsViewModel.delete(args.object.bindingContext);
}

export function onRightSwipeClick(args: ListViewEventData) {
	rlvPlaylists.notifySwipeToExecuteFinished();
	playlistsViewModel.delete(args.object.bindingContext);
}

export async function onPullToRefreshInitiated(args: ListViewEventData) {
	if (!canCallApi) {
		return;
	}
	rlvPlaylists.deselectAll();
    await playlistsViewModel.fillPlaylists(true);
	rlvPlaylists.notifyPullToRefreshFinished();
}

export function onItemTap(args: ItemEventData): void {
	let playlist: Playlist = playlistsViewModel.getObservablePlaylist(args.index) as any;

	if (onMultipleSelection) {
		if (rlvPlaylists.getSelectedItems().some(pl => pl.id == playlist.id) == false) {
			rlvPlaylists.selectItemAt(args.index);
		} else {
			rlvPlaylists.deselectItemAt(args.index);
		}
	} else {
		frameModule.topmost().navigate({
			moduleName: "views/playlists/playlist/playlist",
			context: {
				playlist
			}
		});
	}
}

export function newPlaylist() {
	if (AppRootViewModel.instance.me.isPremium) {
		frameModule.topmost().navigate({
			moduleName: "views/playlists/playlist/params/playlist-params",
			context: {
				isNew: true
			}
		});
	} else {
		Toast.makeText(StringValues.upgradePremium).show();
	}
}

export function showSideDrawer(): void {
	if (drawer) {
		if (playlistsViewModel.get("menuIcon") == "res://back_arrow") {
			if (onMultipleSelection) {
				rlvPlaylists.deselectAll();
			}
		} else {
			drawer.showDrawer();
		}
	}
}