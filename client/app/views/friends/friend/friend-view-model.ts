import { Observable, fromObject } from "tns-core-modules/data/observable";
import { MusicRoomApi } from "~/utils/MusicRoomApi";
import { AppRootViewModel, UserAPI } from "~/app-root-view-model";

export class FriendViewModel extends Observable {

	constructor() {
		super();
	}

	public async getFriendData(id: string): Promise<UserAPI> {
		let friend: UserAPI = null;
		await MusicRoomApi.instance.get(MusicRoomApi.users + id, response => {
			if (!response.hasOwnProperty("errorCode")) {
				friend = response;
			}
		}, error => {
		}, {
			id: AppRootViewModel.instance.me.id
		});
		return friend;
	}
}