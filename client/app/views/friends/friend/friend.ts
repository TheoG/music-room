import { EventData } from "tns-core-modules/data/observable";
import { Page } from "tns-core-modules/ui/page";
import { FriendViewModel } from "./friend-view-model";
import { FriendList } from "../friends-view-model";
import { MusicRoomApi } from "~/utils/MusicRoomApi";
import { UserAPI } from "~/app-root-view-model";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as frameModule from "tns-core-modules/ui/frame"

let page: Page = null;
let friendViewModel: FriendViewModel = null;
let drawer: RadSideDrawer = null;

export async function navigatingTo(args: EventData): Promise<void> {
	
	page = <Page>args.object;
	drawer = <RadSideDrawer>frameModule.topmost().parent;
	
	friendViewModel = new FriendViewModel();
	page.bindingContext = friendViewModel;
	
	let context = page.navigationContext;
	if (context) {
		let friendContext: FriendList = context.friend;
		let friend: UserAPI = await friendViewModel.getFriendData(friendContext.id);

		for (let propertyName in friend) {
			if (propertyName == 'avatar') {
				friendViewModel.set(propertyName, MusicRoomApi.baseUrl + ':' + MusicRoomApi.port + '/' + friend.avatar + '?' + new Date().toISOString());
			} else {
				friendViewModel.set(propertyName, friend[propertyName]);
			}
		}
		
		friendViewModel.set("nbFriends", friend.friends.length);
		friendViewModel.set("friend", friend);
	}
}

export function showSideDrawer(): void {
	if (drawer) {		
		drawer.showDrawer();
	}
}