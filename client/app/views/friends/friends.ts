import { ItemEventData } from "tns-core-modules/ui/list-view";
import { EventData, Observable } from "tns-core-modules/data/observable";
import { Page, NavigatedData } from "tns-core-modules/ui/page";
import { StackLayout } from "tns-core-modules/ui/layouts/stack-layout";
import { FriendsViewModel, FriendStatus, FriendList, FriendSearch } from "./friends-view-model";
import { SwipeActionsEventData, ListViewEventData, RadListView } from "nativescript-ui-listview";
import { View } from "tns-core-modules/ui/core/view";
import * as frameModule from "tns-core-modules/ui/frame"
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { TextField } from "tns-core-modules/ui/text-field/text-field";
import { IconsCode } from "~/utils/Icons";


let page: Page = null;
let searchLayout: StackLayout = null;
let friendsListView: RadListView = null;
let displayed: boolean = false;
let friendsViewModel: FriendsViewModel = null;
let drawer: RadSideDrawer = null;
let rlvFriends: RadListView = null;
let rlvSearch: RadListView = null;

export function navigatingTo(args: NavigatedData): void {
	if (args.isBackNavigation) {
		return ;
	}

	drawer = <RadSideDrawer>frameModule.topmost().parent;

	friendsViewModel = new FriendsViewModel();

	page = <Page>args.object;
	page.bindingContext = friendsViewModel;

	friendsViewModel.fetchFriends(false);

	friendsListView = page.getViewById("friendsListView");
	
	searchLayout = <StackLayout>page.getViewById("search-layout");
	searchLayout.visibility = "collapse";
	displayed = false;

	rlvFriends = page.getViewById("friendsListView");
	rlvSearch = page.getViewById("searchFriendsListView");

	friendsViewModel.set("actionBarTitle", "Friends");
	friendsViewModel.set("menuIcon", "res://menu_light");

	searchFriendsEvent();
}

exports.backEvent = args => {
	if (displayed) {
		closeSearchMenu();
		args.cancel = true;
	}
}

export function onPullToRefreshInitiated(args: ListViewEventData) {
    friendsViewModel.fetchFriends(true);
	friendsListView.notifyPullToRefreshFinished();
	rlvFriends.deselectAll();
}

export function searchNewFriend(args: EventData): void {
	if (searchLayout) {
		toggleSearchMenu();	
	}
}

function toggleSearchMenu() {
	if (!displayed) {
		openSearchMenu();
	} else {
		closeSearchMenu();
	}
}

function openSearchMenu() {
	rlvFriends.deselectAll();
	rlvSearch.height = "auto";
	rlvFriends.height = 0;
	displayed = true;
	searchLayout.visibility = "visible";
}

function closeSearchMenu() {
	rlvSearch.deselectAll();
	rlvSearch.height = 0;
	rlvFriends.height = "auto";
	searchLayout.visibility = "collapse";
	
	let searchFriendsTF: TextField = page.getViewById("search_friends");
	searchFriendsTF.text = "";
	friendsViewModel.clearSearchList();
	displayed = false;
}

export function onItemTap(args: ItemEventData): void {
	let friend: FriendList = args.view.bindingContext;

	frameModule.topmost().navigate({
		moduleName: "views/friends/friend/friend",
		context: {
			friend
		}
	});

}

export function onSwipeCellStarted(args: SwipeActionsEventData) {
	const swipeLimits = args.data.swipeLimits;
	const swipeView = args.object;
	const leftItem = swipeView.getViewById<View>('left-delete-view');
	const rightItem = swipeView.getViewById<View>('right-delete-view');
	swipeLimits.left = leftItem.getMeasuredWidth();
	swipeLimits.right = rightItem.getMeasuredWidth();
	swipeLimits.threshold = leftItem.getMeasuredWidth() / 2;
}

export function onLeftSwipeClick(args: ListViewEventData) {
	const listView: RadListView = page.getViewById("friendsListView");
	listView.notifySwipeToExecuteFinished();
	
	let friend: FriendList = args.object.bindingContext;

	switch (friend.status) {
		case FriendStatus.Received:
			friendsViewModel.respondToFriendshipInvitation(friend, false);
		break;
		case FriendStatus.Sent:
		case FriendStatus.Friend:
			friendsViewModel.deleteFriendship(friend);
		break;
	}
}

export function onRightSwipeClick(args: ListViewEventData) {
    const listView: RadListView = page.getViewById("friendsListView");
	listView.notifySwipeToExecuteFinished();

	let friend: FriendList = args.object.bindingContext;
	switch (friend.status) {
		case FriendStatus.Received:
			friendsViewModel.respondToFriendshipInvitation(friend, true);
		break;
		case FriendStatus.Sent:
		case FriendStatus.Friend:
			friendsViewModel.deleteFriendship(friend);
		break;
	}
}

export function onSwipeSearchCellStarted(args: SwipeActionsEventData) {
	const swipeLimits = args.data.swipeLimits;
	const swipeView = args.object;
	const leftItem = swipeView.getViewById<View>('left-add-view');
	const rightItem = swipeView.getViewById<View>('right-add-view');
	swipeLimits.left = leftItem.getMeasuredWidth();
	swipeLimits.right = rightItem.getMeasuredWidth();
	swipeLimits.threshold = leftItem.getMeasuredWidth() / 2;
}

export function onLeftSearchSwipeClick(args: ListViewEventData) {
    const listView: RadListView = page.getViewById("searchFriendsListView");
	listView.notifySwipeToExecuteFinished();
	let friend: FriendSearch = args.object.bindingContext;

	if (friend.requestSentIcon == IconsCode.Remove) {
		friendsViewModel.deleteFriendship(friend);
	} else {
		friendsViewModel.sendFriendRequest(friend);
	}
}

export function onRightSearchSwipeClick(args: ListViewEventData) {
    const listView: RadListView = page.getViewById("searchFriendsListView");
	listView.notifySwipeToExecuteFinished();
	let friend: FriendSearch = args.object.bindingContext;

	if (friend.requestSentIcon == IconsCode.Remove) {
		friendsViewModel.deleteFriendship(friend);
	} else {
		friendsViewModel.sendFriendRequest(friend);
	}
}

export function showSideDrawer(): void {
	if (drawer) {
		drawer.showDrawer();
	}
}

function searchFriendsEvent(): void {
	let searchFriendsTF: TextField = page.getViewById("search_friends");
	searchFriendsTF.on("textChange", () => {
		friendsViewModel.search(searchFriendsTF.text);
	});
}