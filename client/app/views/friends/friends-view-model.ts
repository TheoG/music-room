import { Observable, fromObject as observableFromObject } from "tns-core-modules/data/observable";
import { ObservableArray } from "tns-core-modules/data/observable-array/observable-array";
import { MusicRoomApi } from "~/utils/MusicRoomApi";
import { IconsCode } from "~/utils/Icons";
import { AppRootViewModel, FriendshipAPI, UserAPI } from "~/app-root-view-model";
import { formatDisplayName } from "~/utils/Tools";

export enum FriendStatus {
	Sent,
	Received,
	Friend
}

export interface FriendModel extends UserAPI {
	friendshipId: string;
}

export interface FriendSearch extends FriendModel {
	requestSentClass: string;
	requestSentIcon: string;
	selectedClass: string;
}

export interface FriendList extends FriendModel {
	leftActionClass: string;
	leftActionIcon: string;
	rightActionClass: string;
	rightActionIcon: string;
	statusIcon: string;
	status: FriendStatus;
	selectedClass: string;
}

export class FriendsViewModel extends Observable {

	private friendsList: ObservableArray<Observable> = new ObservableArray();
	private searchFriendsList: ObservableArray<Observable> = new ObservableArray();

	private static readonly AddClass = "add-container";
	private static readonly RemoveClass = "delete-container";

	constructor() {
		super();

		this.set("friends", this.friendsList);
		this.set("searchFriends", this.searchFriendsList);
	}

	public getObservableFriend(index: number): Observable {
		return this.friendsList.getItem(index);
	}

	public getObservableSearchFriend(index: number): Observable {
		return this.searchFriendsList.getItem(index);
	}

	public async fetchFriends(apiCall: boolean): Promise<void> {
		this.set("loading", true);
		let friendships: FriendshipAPI[] = await AppRootViewModel.instance.friendshipApiCaller.getFriendships(apiCall);

		let alreadyFriend: FriendList[] = [];
		let pendingSent: FriendList[] = [];
		let pendingReceived: FriendList[] = [];
		friendships.forEach(item => {

			let friend: FriendList;

			if (item.pending == true && item.request.from == AppRootViewModel.instance.me.id) {
				// Invitation SENT
				friend = {
					...(item.users[1].id == AppRootViewModel.instance.me.id ? item.users[0] : item.users[1]),
					friendshipId: item.id,
					leftActionClass: FriendsViewModel.RemoveClass,
					leftActionIcon: IconsCode.Remove,
					rightActionClass: FriendsViewModel.RemoveClass,
					rightActionIcon: IconsCode.Remove,
					statusIcon: IconsCode.UserTimes,
					status: FriendStatus.Sent,
					selectedClass: ""
				}
				friend.avatar = MusicRoomApi.baseUrl + ':' + MusicRoomApi.port + '/' + friend.avatar + '?' + new Date().toISOString();
				pendingSent.push(friend);
			} else if (item.pending == true && item.request.from != AppRootViewModel.instance.me.id) {
				// Invitation RECEIVED
				friend = {
					...(item.users[1].id == AppRootViewModel.instance.me.id ? item.users[0] : item.users[1]),
					friendshipId: item.id,
					leftActionClass: FriendsViewModel.RemoveClass,
					leftActionIcon: IconsCode.Times,
					rightActionClass: FriendsViewModel.AddClass,
					rightActionIcon: IconsCode.Check,
					statusIcon: IconsCode.UserPlus,
					status: FriendStatus.Received,
					selectedClass: ""
				}
				friend.avatar = MusicRoomApi.baseUrl + ':' + MusicRoomApi.port + '/' + friend.avatar + '?' + new Date().toISOString();
				pendingReceived.push(friend);
			} else {
				// Already Friends
				friend = {
					...(item.users[1].id == AppRootViewModel.instance.me.id ? item.users[0] : item.users[1]),
					friendshipId: item.id,
					leftActionClass: FriendsViewModel.RemoveClass,
					leftActionIcon: IconsCode.Remove,
					rightActionClass: FriendsViewModel.RemoveClass,
					rightActionIcon: IconsCode.Remove,
					statusIcon: null,
					status: FriendStatus.Friend,
					selectedClass: ""
				}
				friend.avatar = MusicRoomApi.baseUrl + ':' + MusicRoomApi.port + '/' + friend.avatar + '?' + new Date().toISOString();
				alreadyFriend.push(friend);
			}
		});

		let friends: FriendList[] = pendingReceived.concat(pendingSent).concat(alreadyFriend);
	
		this.set("loading", false);		
		if (friends) {
			this.fillFriendsList(friends);
		}
	}

	private fillFriendsList(friends: FriendList[]): void {
		this.friendsList.splice(0);
		friends.forEach(friend => {
			if (!friend.displayName && friend.firstName && friend.lastName) {
				friend.displayName = friend.firstName + " " + friend.lastName;
			} else if (!friend.displayName) {
				friend.displayName = friend.email;
			}
			this.friendsList.push(observableFromObject(friend));
		});
	}

	private fillSearchFriendsList(friends: any[]): void {
		this.searchFriendsList.splice(0);

		friends.forEach(friend => {
			let displayName = formatDisplayName(friend)

			if (this.friendsList.some((f: any) => f.id == friend.id)) {
				return ;
			}

			let friendSearch: FriendSearch = {
				...friend,
				id: friend.id,
				displayName,
				email: friend.email,
				firstName: friend.firstName,
				lastName: friend.lastName,
				friendshipId: null,
				avatar: MusicRoomApi.baseUrl + ':' + MusicRoomApi.port + '/' + friend.avatar + '?' + new Date().toISOString(),
				requestSentClass: FriendsViewModel.AddClass,
				requestSentIcon: IconsCode.UserPlus,
				selectedClass: ""
			}

			this.searchFriendsList.push(observableFromObject(friendSearch));
		});
	}

	public async deleteFriendship(toRemove: FriendList|FriendSearch): Promise<void> {
		let index = -1;
		this.friendsList.filter((f: any, idx) => {
			if (f.id == toRemove.id) {
				index = idx;
				return true;
			}
			return false;
		});
		
		if (toRemove.hasOwnProperty('requestSentIcon')) {
			toRemove = toRemove as FriendSearch;
			toRemove.requestSentClass = FriendsViewModel.AddClass;
			toRemove.requestSentIcon = IconsCode.UserPlus;
		}

		if (index != -1) {
			this.friendsList.splice(index, 1);
			
			MusicRoomApi.instance.delete(MusicRoomApi.friendships + toRemove.friendshipId, {}, async response => {
				await AppRootViewModel.instance.friendshipApiCaller.getFriendships(true);
			}, error => {
			});
		}
	}

	public async respondToFriendshipInvitation(friend: FriendList, accepted: boolean) {

		await MusicRoomApi.instance.put(MusicRoomApi.friendships + friend.friendshipId + '/' + (accepted ? 'accept' : 'reject'), {

		}, response => {
			if (response.hasOwnProperty("errorCode")) {
				return;
			}
			if (accepted) {
				AppRootViewModel.instance.friendshipApiCaller.getFriendship(true, response.id);
			}
		}, error => {
		});
		
		let index = -1;
		this.friendsList.filter((f: any, idx) => {
			if (f.id == friend.id) {
				index = idx;
				return true;
			}
			return false;
		});

		if (accepted) {
			friend.leftActionClass = FriendsViewModel.RemoveClass;
			friend.leftActionIcon = IconsCode.Remove;
			friend.rightActionClass = FriendsViewModel.RemoveClass;
			friend.rightActionIcon = IconsCode.Remove;
			friend.statusIcon = null;
			friend.status = FriendStatus.Friend;
		} else if (index != -1) {
			this.friendsList.splice(index, 1);
			await AppRootViewModel.instance.friendshipApiCaller.getFriendships(true);
		}
	}

	public async sendFriendRequest(newFriend: FriendSearch): Promise<void> {
		await MusicRoomApi.instance.post(MusicRoomApi.friendships, {
			id: newFriend.id,
			message: "Please"
		}, async response => {
			newFriend.requestSentClass = FriendsViewModel.RemoveClass;
			newFriend.requestSentIcon = IconsCode.Remove;

			newFriend.friendshipId = response.id;

			let newFriendList: FriendList = {
				id: newFriend.id,
				avatar: newFriend.avatar,
				theme: newFriend.theme,
				apiKey: null,
				isPremium: newFriend.isPremium,
				friendships: newFriend.friendships,
				friends: newFriend.friends,
				playlists: newFriend.playlists,
				email: newFriend.email,
				displayName: newFriend.displayName,
				firstName: newFriend.firstName,
				lastName: newFriend.lastName,
				facebookid: '',
				musicalPreferences: newFriend.musicalPreferences,
				friendshipId: newFriend.friendshipId,
				leftActionClass: FriendsViewModel.RemoveClass,
				leftActionIcon: IconsCode.Remove,
				rightActionClass: FriendsViewModel.RemoveClass,
				rightActionIcon: IconsCode.Remove,
				statusIcon: IconsCode.UserTimes,
				status: FriendStatus.Sent,
				selectedClass: ""
			}

			this.friendsList.push(observableFromObject(newFriendList));
			await AppRootViewModel.instance.friendshipApiCaller.getFriendships(true);
		}, error => {
		});
	}

	public async search(keyword: string): Promise<void> {
		if (keyword.length < 3) {
			return ;
		}
		
		await MusicRoomApi.instance.get(MusicRoomApi.userSearch, response => {
			if (!response.hasOwnProperty("errorCode")) {
				this.fillSearchFriendsList(response.results);
			}
		}, error => {
		}, {
			keyword
		});
	}

	public clearSearchList(): void {
		this.searchFriendsList.splice(0);
	}
}