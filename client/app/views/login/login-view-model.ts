import { Observable } from "tns-core-modules/data/observable";
import * as appSettings from "tns-core-modules/application-settings";
import { MusicRoomApi } from "~/utils/MusicRoomApi";
import { StringValues } from "~/utils/Values";
import { AppRootViewModel } from "~/app-root-view-model";
import SocialLogin = require("nativescript-social-login");
import * as dialogs from "tns-core-modules/ui/dialogs";
import * as app from "tns-core-modules/application";
import { Folder, knownFolders, File } from "tns-core-modules/file-system";
import * as Toast from 'nativescript-toast';

export class LoginModel extends Observable {

	private readonly emailRegex: RegExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

	constructor() {
		super();

		AppRootViewModel.instance.set("sideDrawerEnabled", false);
		SocialLogin.init({
			activity: app.android.foregroundActivity,
		});	
	}

	signIn(): Promise<void> {
		let email = this.get("email");
		let password: string = this.get("password");

		return new Promise((resolve, reject) => {

			if (!this.emailRegex.test(String(email).toLowerCase())) {
				reject({ errorMessage: 'Bad email format'});
				return;
			}

			if (password == undefined) {
				reject({ errorMessage: 'Please provide a password'});
				return;
			}
			if (password.length < 6) {
				reject({ errorMessage: 'Password too short (6 characters min)'});
				return;
			}
			if (password.length > 72) {
				reject({ errorMessage: 'Password too long (72 characters max)'});
				return;
			}

			MusicRoomApi.instance.post(MusicRoomApi.login, {
				email,
				password
			}, response => {
				appSettings.setString(StringValues.apiKey, response.apiKey);
				appSettings.setString(StringValues.userID, response.id);
			
				AppRootViewModel.instance.apiKey = response.apiKey;
				AppRootViewModel.instance.userID = response.id;

				MusicRoomApi.instance.get(MusicRoomApi.me, user => {
					AppRootViewModel.instance.me = user;
					resolve();
				}, error => {
					reject(error);
				});
			}, error => {
				reject(error);
			});
		});
	}

	signUp(): Promise<void> {
		let email: string = this.get("email");
		let password: string = this.get("password");

		return new Promise((resolve, reject) => {

			if (!this.emailRegex.test(String(email).toLowerCase())) {
				reject({ errorMessage: 'Bad email format'});
				return;
			}

			if (password == undefined) {
				reject({ errorMessage: 'Please provide a password'});
				return;
			}
			if (password.length < 6) {
				reject({ errorMessage: 'Password too short (6 characters min)'});
				return;
			}
			if (password.length > 72) {
				reject({ errorMessage: 'Password too long (72 characters max)'});
				return;
			}

			MusicRoomApi.instance.post(MusicRoomApi.users, {
				email: email,
				password: password
			}, response => {
				resolve();
			}, error => {
				reject(error);
			});
		});
	}

	devOptions(): void {
		dialogs.prompt({
			title: "Developers Options",
			message: "Change here the server address",
			okButtonText: "Confirm",
			cancelButtonText: "Cancel",
			defaultText: MusicRoomApi.baseUrl + (MusicRoomApi.port ? ':' + MusicRoomApi.port : ''),
			inputType: dialogs.inputType.text
		}).then(res => {
			let params: string[] = res.text.split(':');
			MusicRoomApi.baseUrl = params[0] + ':' + params[1];
			MusicRoomApi.port = params[2] ? params[2] : '';

			const documents: Folder = <Folder>knownFolders.currentApp();
			const folder: Folder = <Folder>documents.getFolder('.');
			const file: File = <File>folder.getFile('config.json');

			try {
				let fileContent = JSON.parse(file.readTextSync());

				fileContent.serverName = MusicRoomApi.baseUrl;
				fileContent.serverPort = MusicRoomApi.port;

				file.writeTextSync(JSON.stringify(fileContent, null, 4));
			} catch (e) {
				Toast.makeText('File corrupted').show();
			}
		}); 
	}

	askResetCode(email: string): void {
		MusicRoomApi.instance.post(MusicRoomApi.users + MusicRoomApi.resetPassword, {
			email
		}, response => {
		}, error => {
		});
	}

	public sendResetCode(email:string, code: string, newPassword: string): Promise<void> {
		return new Promise((resolve, reject) => {
			MusicRoomApi.instance.post(MusicRoomApi.users + MusicRoomApi.resetPassword, {
				email,
				code,
				password: newPassword
			}, response => {
				resolve(response);
			}, error => {
				reject(error);
			});
		});
	}

	facebookLogin(): Promise<void> {
		return new Promise((resolve, reject) => {
			SocialLogin.loginWithFacebook(result => {

				MusicRoomApi.instance.post(MusicRoomApi.facebook, {}, response => {
					appSettings.setString(StringValues.apiKey, response.apiKey);
					appSettings.setString(StringValues.userID, response.id);
				
					AppRootViewModel.instance.apiKey = response.apiKey;
					AppRootViewModel.instance.userID = response.id;

					MusicRoomApi.instance.get(MusicRoomApi.me, user => {
						AppRootViewModel.instance.me = user;
						resolve();
					}, error => {
						reject(error);
					});
				}, error => {
					reject(error);
				}, {
					accessToken: result.authToken
				});
			});
		});
	}
}