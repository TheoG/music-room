import { EventData } from "tns-core-modules/data/observable";
import { Page } from "tns-core-modules/ui/page";
import { Label } from "tns-core-modules/ui/label";
import { Image } from "tns-core-modules/ui/image/image";
import { LoginModel } from "~/views/login/login-view-model";
import { MusicRoomApi } from "~/utils/MusicRoomApi";
import { AppRootViewModel } from "~/app-root-view-model";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { formatDisplayName } from "~/utils/Tools";

import * as frameModule from "tns-core-modules/ui/frame"
import * as Toast from 'nativescript-toast';
import * as dialogs from "tns-core-modules/ui/dialogs";

let loginModel = null;
let page = null;

export function navigatingTo(args: EventData) {
	loginModel = new LoginModel();
	page = <Page>args.object;
	page.bindingContext = loginModel;
	page.actionBarHidden = true;
}

export function emailFocus(args: EventData): void {
	if (page == null) {
		page = <Page>args.object;
	}
	let emailErrorMessage: Label = <Label>page.getViewById("email-error-message");

	if (emailErrorMessage.cssClasses.has("display-error")) {
		emailErrorMessage.cssClasses.delete("display-error");
		emailErrorMessage.className = Array.from(emailErrorMessage.cssClasses).join(' ');
	}
}

export async function signIn(): Promise<void> {
	let emailErrorMessage: Label = <Label>page.getViewById("email-error-message");
	emailErrorMessage.cssClasses.delete("display-error");
	emailErrorMessage.className = Array.from(emailErrorMessage.cssClasses).join(' ');

	loginModel.set('loading', true);

	loginModel.signIn().then(() => {
		let sideDrawer: RadSideDrawer = frameModule.topmost().parent.getViewById('sideDrawer');
		let mainAvatar: Image = frameModule.topmost().parent.getViewById('mainAvatar');
		let mainDisplayName: Label = frameModule.topmost().parent.getViewById('mainDisplayName');
	
		sideDrawer.gesturesEnabled = true;
		mainAvatar.src = MusicRoomApi.baseUrl + ':' + MusicRoomApi.port + '/' + AppRootViewModel.instance.me.avatar + "?" + new Date().toISOString();
		mainDisplayName.text = formatDisplayName(AppRootViewModel.instance.me);

		frameModule.topmost().navigate({
			moduleName: 'views/friends/friends',
			clearHistory: true
		});

		loginModel.set('loading', false);
	}).catch((error) => {
		if (error.hasOwnProperty('errorMessage')) {
			emailErrorMessage.text = error.errorMessage;
			emailErrorMessage.cssClasses.add("display-error");
			emailErrorMessage.className = Array.from(emailErrorMessage.cssClasses).join(' ');
		} else {
			Toast.makeText('Server error').show();
		}
		loginModel.set('loading', false);
	});
}

export async function signUp(): Promise<void> {
	let emailErrorMessage: Label = <Label>page.getViewById("email-error-message");

	emailErrorMessage.cssClasses.delete("display-error");

	emailErrorMessage.className = Array.from(emailErrorMessage.cssClasses).join(' ');

	loginModel.set("loading", true);
	loginModel.signUp().then(() => {
		AppRootViewModel.instance.set("sideDrawerEnabled", true);
		let sideDrawer: RadSideDrawer = frameModule.topmost().parent.getViewById('sideDrawer');
		let mainAvatar: Image = frameModule.topmost().parent.getViewById('mainAvatar');
		let mainDisplayName: Label = frameModule.topmost().parent.getViewById('mainDisplayName');
		
		sideDrawer.gesturesEnabled = true;
		mainAvatar.src = AppRootViewModel.instance.get("avatarURL");
		mainDisplayName.text = AppRootViewModel.instance.get("displayName");
		
		dialogs.alert({
			title: 'Confirm your account',
			message: 'Please check your emails and click the link to activate your account. After you confirm your email, click the Sign In button.',
			okButtonText: "Ok"
		});
		loginModel.set("loading", false);
	}).catch (error => {
		if (error.hasOwnProperty('errorMessage')) {
			emailErrorMessage.text = error.errorMessage;
			emailErrorMessage.cssClasses.add("display-error");
			emailErrorMessage.className = Array.from(emailErrorMessage.cssClasses).join(' ');
		} else {
			Toast.makeText('Server error').show();
		}
		loginModel.set("loading", false);
	});
}

export function forgotPassword(): void {
	dialogs.prompt({
		title: "Reset password?",
		message: "Please enter your email",
		okButtonText: "Send code",
		cancelButtonText: "Cancel",
		inputType: dialogs.inputType.email,
		defaultText: loginModel.get('email')
	}).then(r => {
		if (r.result) {
			loginModel.askResetCode(r.text);
			displayModalWithResetCode(r.text);
		}
	});
}

function displayModalWithResetCode(email: string): void {
	dialogs.login("Please enter the code you received by email and your new password", "Code", "New Password").then(res => {
		if (res.result) {
			loginModel.sendResetCode(email, res.userName, res.password).then(response => {
				Toast.makeText('Your password has been reset!').show();
			}).catch(error => {
				Toast.makeText(error.message).show();
				if (error.message != 'No more password reset attempts remaining ...') {
					displayModalWithResetCode(email);
				}
			});
		}
	});
}

export function signInFacebook(): void {
	loginModel.set('loading', true);

	loginModel.facebookLogin().then(() => {
		let sideDrawer: RadSideDrawer = frameModule.topmost().parent.getViewById('sideDrawer');
		let mainAvatar: Image = frameModule.topmost().parent.getViewById('mainAvatar');
		let mainDisplayName: Label = frameModule.topmost().parent.getViewById('mainDisplayName');
	
		sideDrawer.gesturesEnabled = true;
		mainAvatar.src = MusicRoomApi.baseUrl + ':' + MusicRoomApi.port + '/' + AppRootViewModel.instance.me.avatar + "?" + new Date().toISOString();
		mainDisplayName.text = formatDisplayName(AppRootViewModel.instance.me);

		frameModule.topmost().navigate({
			moduleName: 'views/friends/friends',
			clearHistory: true
		});

		loginModel.set('loading', false);
	}).catch((error) => {
		Toast.makeText('Server error').show();
		loginModel.set('loading', false);
	});
}
