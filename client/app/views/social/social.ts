import { EventData } from "tns-core-modules/data/observable";
import { Page } from "tns-core-modules/ui/page";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { SocialViewModel } from "./social-view-model"
import { IconsCode } from "~/utils/Icons";
import * as app from "tns-core-modules/application";
import * as frameModule from "tns-core-modules/ui/frame"

import SocialLogin = require("nativescript-social-login");
import { AppRootViewModel } from "~/app-root-view-model";

let page: Page = null;
let drawer: RadSideDrawer = null;
let socialViewModel: SocialViewModel = null;

export function navigatingTo(args: EventData): void {
	page = <Page>args.object;
	drawer = <RadSideDrawer>frameModule.topmost().parent;

	socialViewModel = new SocialViewModel();
	page.bindingContext = socialViewModel;

	socialViewModel.set("facebookIcon", IconsCode.Facebook);
	socialViewModel.set("googleIcon", IconsCode.Google);
	socialViewModel.set("deezerIcon", IconsCode.Deezer);
	
	let result = SocialLogin.init({
		activity: app.android.foregroundActivity,
	});
	socialViewModel.tryToRestoreDeezerConnect();
}


export function facebookConnection(): void {
	if (AppRootViewModel.instance.me.facebookid) {
		return;
	}

	SocialLogin.loginWithFacebook(async result => {
		socialViewModel.set('loading', true);
		await socialViewModel.facebookLink(result.authToken);
		socialViewModel.set('loading', false);
	});
}

export function deezerConnection(): void {
	if (socialViewModel.isDeezerRestored()) {
		socialViewModel.deezerDisconnection();
	} else {
		socialViewModel.deezerConnection();
	}
}

export function showSideDrawer(): void {
	if (drawer) {
		drawer.showDrawer();
	}
}