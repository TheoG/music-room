import { Observable } from "tns-core-modules/data/observable";
import { StringValues } from "~/utils/Values";
import { DeezerConnect, DialogListener, SessionStore } from "nativescript-deezer"
import * as app from "tns-core-modules/application";
import * as Toast from 'nativescript-toast';
import { AppRootViewModel } from "~/app-root-view-model";
import { MusicRoomApi } from "~/utils/MusicRoomApi";
import SocialLogin = require("nativescript-social-login");
import * as appSettings from "tns-core-modules/application-settings";

export class SocialViewModel extends Observable {

	private sessionStore: SessionStore = null;
	private deezerConnect: DeezerConnect = null;

	private deezerRestored: boolean = false;

	constructor() {
		super ();
		this.sessionStore = new SessionStore();
		this.deezerConnect = new DeezerConnect(StringValues.deezerAppID);

		this.set('deezerAccountText', 'Link Deezer account');

		this.set('facebookAccountText', AppRootViewModel.instance.me.facebookid ? 'Facebook account linked!' : 'Link Facebook Account');
		this.deezerRestored = false;
	}

	public deezerConnection(): void {
		let dialogListener: DialogListener = new DialogListener(complete => {
			Toast.makeText('Connected!').show();
			this.deezerRestored = true;
			this.set('deezerAccountText', 'Unlink Deezer Account');
			this.sessionStore.save(this.deezerConnect);
		}, () => {
		}, exception => {
			Toast.makeText('Error...').show();
		});

		this.deezerConnect.androidAuthorize(app.android.foregroundActivity, null, dialogListener);
	}

	public deezerDisconnection(): void {
		this.deezerConnect.logout();
		this.sessionStore.clear();
		this.set('deezerAccountText', 'Link Deezer Account');
	}

	public tryToRestoreDeezerConnect(): boolean {
		this.deezerRestored = this.sessionStore.restore(this.deezerConnect);
		if (this.deezerRestored) {
			this.set('deezerAccountText', 'Unlink Deezer Account');
		}
		return this.deezerRestored;
	}

	public isDeezerRestored(): boolean {
		return this.deezerRestored;
	}

	public async facebookLink(authToken: string): Promise<void> {

		await new Promise(resolve => {
			MusicRoomApi.instance.post(MusicRoomApi.facebook, {}, response => {
				
				if (response.message != "Facebook and Music Room accounts linked") {
					Toast.makeText('Already linked with another account.').show();
					resolve();
					return ;
				}
				
				AppRootViewModel.instance.me.facebookid = response.facebookid;
				
				this.set('facebookAccountText', 'Facebook account linked!');
				
				resolve();
			}, error => {
				resolve();
			}, {
				accessToken: authToken
			});
		});

	}
}
