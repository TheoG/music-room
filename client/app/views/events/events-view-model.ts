import { Observable, fromObject as observableFromObject } from "tns-core-modules/data/observable";
import { ObservableArray } from "tns-core-modules/data/observable-array/observable-array";
import { MusicRoomApi } from "~/utils/MusicRoomApi";
import { IconsCode } from "~/utils/Icons";
import { EventAPI, AppRootViewModel, PlaylistAPI } from "~/app-root-view-model";

export interface Event extends EventAPI {
	eventRights: string;
	removeIcon: string;
	selectedClass: string;
	// icons...
}

export class EventsViewModel extends Observable {

	private eventsList: ObservableArray<Observable> = null;

	constructor() {
		super();

		this.eventsList = new ObservableArray();
		this.fillEvents(false).catch(e => {
		});
	}

	public getObservablePlaylist(index: number): Observable {
		return this.eventsList.getItem(index);
	}

	public async fillEvents(apiCall: boolean): Promise<void> {

		this.cleanEventsList();
		this.set('loading', true);

		let events: any;
		let sum: number = Object.keys(AppRootViewModel.instance.events.owner).length + Object.keys(AppRootViewModel.instance.events.guest).length + Object.keys(AppRootViewModel.instance.events.dj).length;
		if (sum == 0 || apiCall) {
			await MusicRoomApi.instance.get(MusicRoomApi.events, response => {
				events = response;

				AppRootViewModel.instance.events.owner = {};
				AppRootViewModel.instance.events.guest = {};
				AppRootViewModel.instance.events.dj = {};

				response.owner.forEach(e => {
					AppRootViewModel.instance.events.owner[e.id] = e;
				});
					
				response.guest.forEach(e => {
					AppRootViewModel.instance.events.guest[e.id] = e;
				});
	
				response.dj.forEach(e => {
					AppRootViewModel.instance.events.dj[e.id] = e;
				}); 
			}, error => {
			}).catch((e: Error) => {
			});
		} else {
			events = {};
			events.owner = Object.values(AppRootViewModel.instance.events.owner);
			events.dj = Object.values(AppRootViewModel.instance.events.dj);
			events.guest = Object.values(AppRootViewModel.instance.events.guest);
		}

		for (let i: number = 0; i < events.owner.length; i++) {
			let event: Event = {
				...EventAPI.default(events.owner[i]),
				removeIcon: IconsCode.Remove,
				eventRights: null,
				selectedClass: ''
			}
			this.eventsList.push(observableFromObject(event));
		}

		for (let i: number = 0; i < events.dj.length; i++) {
			let event: Event = {
				...EventAPI.default(events.dj[i]),
				removeIcon: IconsCode.Remove,
				eventRights: IconsCode.HeadPhones,
				selectedClass: ''
			}
			this.eventsList.push(observableFromObject(event));
		}

		for (let i: number = 0; i < events.guest.length; i++) {
			// Skipping guest if user in dj
			if (events.dj.some(e => e.id == events.guest[i].id)) {
				break;
			}

			let event: Event = {
				...EventAPI.default(events.guest[i]),
				removeIcon: IconsCode.Remove,
				eventRights: IconsCode.Eye,
				selectedClass: ''
			}
			this.eventsList.push(observableFromObject(event));
		}

		
		this.set('events', this.eventsList);
		this.set('loading', false);
	}

	public delete(event: Event): void {

		if (event.id in AppRootViewModel.instance.events.owner) {		
			delete AppRootViewModel.instance.events.owner[event.id];
			MusicRoomApi.instance.delete(MusicRoomApi.events + event.id, {}, reponse => {
			}, error => {
			});
		} else if (event.id in AppRootViewModel.instance.events.guest) {
			delete AppRootViewModel.instance.events.guest[event.id];
			MusicRoomApi.instance.delete(MusicRoomApi.events + event.id + '/' + MusicRoomApi.guests + ':userID',  {
			}, response => {
			}, error => {
			});
		} else if (event.id in AppRootViewModel.instance.events.dj) {
			delete AppRootViewModel.instance.events.dj[event.id];
			MusicRoomApi.instance.delete(MusicRoomApi.events + event.id + '/' + MusicRoomApi.djs + ':userID',  {
			}, response => {
			}, error => {
			});
		}

		let index = -1;
		index = this.eventsList.indexOf(<any>event);
		if (index != -1) {
			this.eventsList.splice(index, 1);
		}
	}

	public deleteMultiple(events: Event[]): void {

		let eventids: string[] = [];

		events.forEach(event => {
			
			if (event.id in AppRootViewModel.instance.events.owner) {
				eventids.push(event.id);
				delete AppRootViewModel.instance.events.owner[event.id];
			}
			if (event.id in AppRootViewModel.instance.events.guest) {
				delete AppRootViewModel.instance.events.guest[event.id];
				MusicRoomApi.instance.delete(MusicRoomApi.events + event.id + '/' + MusicRoomApi.guests + ':userID',  {
				}, response => {
				}, error => {
				});
			}
			if (event.id in AppRootViewModel.instance.events.dj) {
				delete AppRootViewModel.instance.events.dj[event.id];
				MusicRoomApi.instance.delete(MusicRoomApi.events + event.id + '/' + MusicRoomApi.djs + ':userID',  {
				}, response => {
				}, error => {
				});
			}
			
			let index = this.eventsList.indexOf(<any>event);
			if (index != -1) {
				this.eventsList.splice(index, 1);
			}

		});

		MusicRoomApi.instance.delete(MusicRoomApi.events, {
			eventids
		}, reponse => {
		}, error => {
		});



	}

	public cleanEventsList(): void {
		this.eventsList.length = 0;
	}

}