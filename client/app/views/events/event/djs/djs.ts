import { EventData, Observable } from "tns-core-modules/data/observable";
import { Page, View } from "tns-core-modules/ui/page";
import { StackLayout } from "tns-core-modules/ui/layouts/stack-layout";
import * as frameModule from "tns-core-modules/ui/frame"
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { RadListView, ListViewEventData, SwipeActionsEventData } from "nativescript-ui-listview";
import { TextField } from "tns-core-modules/ui/text-field/text-field";
import { DJsViewModel, DJSearch } from "~/views/events/event/djs/djs-view-model";
import { IconsCode } from "~/utils/Icons";
import { ItemEventData } from "tns-core-modules/ui/list-view/list-view";

let page: Page = null;
let eventDJsViewModel: DJsViewModel = null;
let drawer: RadSideDrawer = null;
let searchLayout: StackLayout = null;
let displayed: boolean = false;
let onMultipleSelectionSearch: boolean = false;
let onMultipleSelection: boolean = false;
let rlvSearch: RadListView = null;
let rlvDJs: RadListView = null;

export function navigatingTo(args: EventData): void {
	drawer = <RadSideDrawer>frameModule.topmost().parent;

	page = <Page>args.object;
	
	let context = page.navigationContext;
	if (context) {
		eventDJsViewModel = new DJsViewModel(context.eventID);
		page.bindingContext = eventDJsViewModel;
	}

	searchLayout = <StackLayout>page.getViewById("search-layout");
	searchLayout.visibility = "collapse";
	displayed = false;

	rlvDJs = page.getViewById("djsListView");
	rlvSearch = page.getViewById("searchDJsListView");
	rlvSearch.height = 0;
	rlvDJs.height = "auto";

	eventDJsViewModel.set("menuIcon", "res://menu_light");
	eventDJsViewModel.set("actionBarTitle", "DJs");

	searchDJsEvent();
}

exports.backEvent = args => {
	if (displayed) {
		closeSearchMenu();
		args.cancel = true;
	}
}

export function pullRefreshDJs(): void {

	rlvDJs.deselectAll();
	eventDJsViewModel.refreshEvent();
	rlvDJs.notifyPullToRefreshFinished();

}

export function searchNewDJs(args: EventData): void {
	if (searchLayout) {
		toggleSearchMenu();	
	}
}

function toggleSearchMenu() {
	if (!displayed) {
		openSearchMenu();
	} else {
		closeSearchMenu();
	}
}

function openSearchMenu() {
	rlvDJs.deselectAll();
	rlvSearch.height = "auto";
	rlvDJs.height = 0;
	displayed = true;
	searchLayout.visibility = "visible";
	eventDJsViewModel.clearSearchList(true);
}

function closeSearchMenu() {
	rlvSearch.deselectAll();
	rlvSearch.height = 0;
	rlvDJs.height = "auto";
	searchLayout.visibility = "collapse";
	
	let searchDJsTF: TextField = page.getViewById("search_djs");
	searchDJsTF.text = "";
	eventDJsViewModel.clearSearchList(true);
	displayed = false;
}

export function onItemSelected(args: ItemEventData): void {
	let dj: any = eventDJsViewModel.getObservableDJ(args.index);
	if (dj) {
		dj.selectedClass = '';
	}

	dj.selectedClass = "list-group-item-selected";
	eventDJsViewModel.set("actionBarTitle", "Remove DJs (" + rlvDJs.getSelectedItems().length + ")");
	
	if (!onMultipleSelection) {
		eventDJsViewModel.set("menuIcon", "res://back_arrow");
		eventDJsViewModel.set("confirmIcon", IconsCode.Remove);
		onMultipleSelection = true;
	}
}

export function onItemDeselected(args: ItemEventData): void {
	let dj: any = eventDJsViewModel.getObservableDJ(args.index);
	if (dj) {
		dj.selectedClass = '';
	}

	eventDJsViewModel.set("actionBarTitle", "Remove DJs (" + rlvDJs.getSelectedItems().length + ")");
	if (rlvDJs.getSelectedItems().length == 0) {
		onMultipleSelection = false;
		eventDJsViewModel.set("menuIcon", "res://menu_light");
		eventDJsViewModel.set("actionBarTitle", "DJs");
		eventDJsViewModel.set("confirmIcon", "");
	}
}

export function onItemTap(args: ItemEventData): void {
	if (onMultipleSelection) {
		if (rlvDJs.getSelectedItems().indexOf(args.view.bindingContext) == -1) {
			rlvDJs.selectItemAt(args.index);
			
		} else {
			rlvDJs.deselectItemAt(args.index);
		}
	}
}

export function onItemSelectedSearch(args: ItemEventData): void {
	let dj: any = eventDJsViewModel.getObservableSearchDJ(args.index);

	dj.selectedClass = "list-group-item-selected";
	eventDJsViewModel.set("actionBarTitle", "Add DJs (" + rlvSearch.getSelectedItems().length + ")");
	
	if (!onMultipleSelectionSearch) {
		eventDJsViewModel.set("menuIcon", "res://back_arrow");
		eventDJsViewModel.set("confirmIcon", IconsCode.Check);
		onMultipleSelectionSearch = true;
	}
}

export function onItemDeselectedSearch(args: ItemEventData): void {
	let dj: any = eventDJsViewModel.getObservableSearchDJ(args.index);
	if (dj) {
		dj.selectedClass = '';
	}

	eventDJsViewModel.set("actionBarTitle", "Add DJs (" + rlvSearch.getSelectedItems().length + ")");
	if (rlvSearch.getSelectedItems().length == 0) {
		onMultipleSelectionSearch = false;
		eventDJsViewModel.set("menuIcon", "res://menu_light");
		eventDJsViewModel.set("actionBarTitle", "DJs");
		eventDJsViewModel.set("confirmIcon", "");
	}
}

export function onItemTapSearch(args: ItemEventData): void {
	if (onMultipleSelectionSearch) {
		if (rlvSearch.getSelectedItems().indexOf(args.view.bindingContext) == -1) {
			rlvSearch.selectItemAt(args.index);
			
		} else {
			rlvSearch.deselectItemAt(args.index);
		}
	}
}

export function onConfirm(args: ItemEventData): void {
	if (onMultipleSelectionSearch) {
		let selectedDJs: DJSearch[] = rlvSearch.getSelectedItems();
		rlvSearch.deselectAll();
		eventDJsViewModel.addMultiple(selectedDJs);
	} else {
		let selectedDJs: DJSearch[] = rlvDJs.getSelectedItems();
		rlvDJs.deselectAll();
		eventDJsViewModel.removeMultiple(selectedDJs);
	}
}

export function onSwipeCellStarted(args: SwipeActionsEventData) {
	const swipeLimits = args.data.swipeLimits;
	const swipeView = args.object;
	const leftItem = swipeView.getViewById<View>('left-delete-view');
	const rightItem = swipeView.getViewById<View>('right-delete-view');
	swipeLimits.left = leftItem.getMeasuredWidth();
	swipeLimits.right = rightItem.getMeasuredWidth();
	swipeLimits.threshold = leftItem.getMeasuredWidth() / 2;
}

export function onLeftSwipeClick(args: ListViewEventData) {
	let dj: any = args.object.bindingContext;
	eventDJsViewModel.remove(dj);
}

export function onRightSwipeClick(args: ListViewEventData) {
	let dj: any = args.object.bindingContext;
	eventDJsViewModel.remove(dj);
}

export function onSwipeSearchCellStarted(args: SwipeActionsEventData) {
	const swipeLimits = args.data.swipeLimits;
	const swipeView = args.object;
	const leftItem = swipeView.getViewById<View>('left-add-view');
	const rightItem = swipeView.getViewById<View>('right-add-view');
	swipeLimits.left = leftItem.getMeasuredWidth();
	swipeLimits.right = rightItem.getMeasuredWidth();
	swipeLimits.threshold = leftItem.getMeasuredWidth() / 2;
}

export function onLeftSearchSwipeClick(args: ListViewEventData) {
	let dj: DJSearch = args.object.bindingContext;
	if (dj.requestSentIcon == IconsCode.Check) {
		eventDJsViewModel.add(dj);
	} else {
		eventDJsViewModel.remove(dj);
	}
}

export function onRightSearchSwipeClick(args: ListViewEventData) {
	let dj: DJSearch = args.object.bindingContext;
	if (dj.requestSentIcon == IconsCode.Check) {
		eventDJsViewModel.add(dj);
	} else {
		eventDJsViewModel.remove(dj);
	}
}

function searchDJsEvent(): void {
	let searchDJsTF: TextField = page.getViewById("search_djs");
	searchDJsTF.on("textChange", () => {
		eventDJsViewModel.search(searchDJsTF.text);
	});
}

export function showSideDrawer(): void {
	if (drawer) {
		if (eventDJsViewModel.get("menuIcon") == "res://back_arrow") {
			if (onMultipleSelection) {
				rlvDJs.deselectAll();
			} else if (onMultipleSelectionSearch) {
				rlvSearch.deselectAll();
			}
		} else {
			drawer.showDrawer();
		}
	}
}
