import { Observable, fromObject as observableFromObject } from 'tns-core-modules/data/observable';
import { ObservableArray } from "tns-core-modules/data/observable-array/observable-array";
import { IconsCode } from "~/utils/Icons"
import { MusicRoomApi } from '~/utils/MusicRoomApi';
import { UserAPI, AppRootViewModel } from '~/app-root-view-model';
import { formatDisplayName } from '~/utils/Tools';
import { EventRights } from '~/views/events/event/event-view-model';
import * as frameModule from "tns-core-modules/ui/frame"

export interface DJSearch extends UserAPI {
	requestSentClass: string;
	requestSentIcon: string;
	selectedClass: string;
}

export interface DJList extends UserAPI {
	leftActionClass: string;
	leftActionIcon: string;
	rightActionClass: string;
	rightActionIcon: string;
	selectedClass: string;
}

export class DJsViewModel extends Observable {

	private searchDJsList: ObservableArray<Observable> = new ObservableArray();
	private djsList: ObservableArray<Observable> = new ObservableArray();
	
	private eventID: string;
	private rights: EventRights;

	private static readonly AddClass = "add-container";
	private static readonly RemoveClass = "delete-container";

	constructor(eventID: string) {
		super()
		this.eventID = eventID;
		this.set("searchDJs", this.searchDJsList);
		this.set("djs", this.djsList);

		if (eventID in AppRootViewModel.instance.events.owner) {
			this.rights = EventRights.Owner;
		} else if (eventID in AppRootViewModel.instance.events.dj) {
			this.rights = EventRights.DJ;
		} else if (eventID in AppRootViewModel.instance.events.guest) {
			this.rights = EventRights.Guest;
		} else {
			frameModule.topmost().navigate({
				moduleName: "views/events/events"
			});
			return ;
		}

		this.fillDJsList();
		this.fillSearchWithFriends();
	}

	private async fillSearchWithFriends(): Promise<void> {
		this.searchDJsList.length = 0;
		let friendships = await AppRootViewModel.instance.friendshipApiCaller.getFriendships(false);
		
		friendships.forEach(friendship => {

			if (!friendship.pending) {
				let friend = friendship.users[0].id != AppRootViewModel.instance.me.id ? friendship.users[0] : friendship.users[1];
				
				if (this.djsList.some((dj: any) => dj.id == friend.id)) {
					return ;
				}

				let displayName = formatDisplayName(friend);
				let djSearch: DJSearch = {
					...friend,
					avatar: MusicRoomApi.baseUrl + ':' + MusicRoomApi.port + '/' + friend.avatar + '?' + new Date().toISOString(),
					displayName,
					selectedClass: "",
					requestSentClass: DJsViewModel.AddClass,
					requestSentIcon: IconsCode.Check
				};
				this.searchDJsList.push(observableFromObject(djSearch));
			}
		});
	}

	public getObservableDJ(index: number): Observable {
		return this.djsList.getItem(index);
	}

	public getObservableSearchDJ(index: number): Observable {
		return this.searchDJsList.getItem(index);
	}

	public async search(keyword: string): Promise<void> {
		if (keyword.length == 0) {
			this.clearSearchList(true);
			return ;
		}
		if (keyword.length < 3) {
			this.clearSearchList(false);
			return ;
		}
		await MusicRoomApi.instance.get(MusicRoomApi.userSearch, response => {
			if (!response.hasOwnProperty("errorCode")) {
				this.fillSearchDJsList(response.results);
			}
		}, error => {
		}, {
			keyword
		});
	}

	public refreshEvent(): void {
		MusicRoomApi.instance.get(MusicRoomApi.events + this.eventID, response => {
			AppRootViewModel.instance.events[this.rights][this.eventID] = response;
			this.fillDJsList();
		}, error => {
		});
	}

	public fillDJsList(): void {
		this.clearDJsList();
		
		AppRootViewModel.instance.events[this.rights][this.eventID].djs.forEach(dj => {
			let djElement: DJList = {
				...dj,
				avatar: MusicRoomApi.baseUrl + ':' + MusicRoomApi.port + '/' + dj.avatar + '?' + new Date().toISOString(),
				leftActionClass: DJsViewModel.RemoveClass,
				leftActionIcon: IconsCode.Remove,
				rightActionClass: DJsViewModel.RemoveClass,
				rightActionIcon: IconsCode.Remove,
				selectedClass: ""
			};
			djElement.displayName = formatDisplayName(djElement);
			this.djsList.push(observableFromObject(djElement));
		});
	}


	private fillSearchDJsList(djs: any[]): void {
		this.clearSearchList(false);

		djs.forEach(dj => {

			// If already in the list
			if (this.djsList.some((o: any) => o.id == dj.id)) {
				return ;
			}

			let djSearch: DJSearch = {
				...dj,
				avatar: MusicRoomApi.baseUrl + ':' + MusicRoomApi.port + '/' + dj.avatar + '?' + new Date().toISOString(),
				selectedClass: "",
				requestSentClass: DJsViewModel.AddClass,
				requestSentIcon: IconsCode.Check
			};
			djSearch.displayName = formatDisplayName(djSearch);
			this.searchDJsList.push(observableFromObject(djSearch));
		});
	}

	public add(DJ: DJSearch): void {
		
		DJ.requestSentIcon = IconsCode.Remove;
		DJ.requestSentClass = DJsViewModel.RemoveClass;
		
		let newDJ: UserAPI = {
			id: DJ.id,
			avatar: DJ.avatar,
			theme: null,
			apiKey: null,
			isPremium: DJ.isPremium,
			email: DJ.email,
			displayName: DJ.displayName,
			firstName: DJ.firstName,
			lastName: DJ.lastName,
			friendships: DJ.friendships,
			friends: DJ.friends,
			playlists: DJ.playlists,
			musicalPreferences: DJ.musicalPreferences,
			facebookid: ''
		}
		
		let newDJList: DJList = {
			...newDJ,
			leftActionClass: DJsViewModel.RemoveClass,
			leftActionIcon: IconsCode.Remove,
			rightActionClass: DJsViewModel.RemoveClass,
			rightActionIcon: IconsCode.Remove,
			selectedClass: ""
		}
		this.djsList.push(observableFromObject(newDJList));
		
		newDJ.avatar = 'avatars/' + newDJ.avatar.split('/avatars/').pop();

		AppRootViewModel.instance.events[this.rights][this.eventID].djs.push(newDJ);


		let index: number = -1;
		this.searchDJsList.filter((dj, idx) => {
			if (dj['id'] == newDJ.id) {
				index = idx;
				return true;
			}
			return false;
		});
		if (index != -1) {
			this.searchDJsList.splice(index, 1);
		}

		MusicRoomApi.instance.put(MusicRoomApi.events + this.eventID + '/' + MusicRoomApi.djs + DJ.id, {
		}, response => {
		}, error => {
		});
	}

	public addMultiple(djs: DJSearch[]): void {
		let djsIds: string[] = [];
		
		djs.forEach(DJ => {
			DJ.requestSentIcon = IconsCode.Remove;
			DJ.requestSentClass = DJsViewModel.RemoveClass;
			
			let newDJ: UserAPI = {
				id: DJ.id,
				avatar: DJ.avatar,
				theme: null,
				apiKey: null,
				isPremium: DJ.isPremium,
				email: DJ.email,
				displayName: DJ.displayName,
				firstName: DJ.firstName,
				lastName: DJ.lastName,
				friendships: DJ.friendships,
				friends: DJ.friends,
				playlists: DJ.playlists,
				musicalPreferences: DJ.musicalPreferences,
				facebookid: ''
			}
			
			let newDJList: DJList = {
				...newDJ,
				leftActionIcon: IconsCode.Remove,
				rightActionClass: DJsViewModel.RemoveClass,
				rightActionIcon: IconsCode.Remove,
				selectedClass: "",
				leftActionClass: DJsViewModel.RemoveClass,
			}

			newDJ.avatar = 'avatars/' + newDJ.avatar.split('/avatars/').pop();
			this.djsList.push(observableFromObject(newDJList));
			
			for (let i: number = 0; i < this.searchDJsList.length; i++) {
				if ((this.searchDJsList.getItem(i) as any).id == DJ.id) {
					this.searchDJsList.splice(i, 1);
					break;
				}
			}

			AppRootViewModel.instance.events[this.rights][this.eventID].djs.push(newDJ);
			djsIds.push(DJ.id);
		});
		MusicRoomApi.instance.put(MusicRoomApi.events + this.eventID + '/' + MusicRoomApi.djs, {
			userids: djsIds
		}, response => {
		}, error => {
		});
	}
	
	public remove(DJ: any): void {
		if (DJ.hasOwnProperty('requestSentIcon')) {
			let djSearch: DJSearch = DJ as DJSearch;
			djSearch.requestSentIcon = IconsCode.Check;
			djSearch.requestSentClass = DJsViewModel.AddClass;
			let index = -1;
			this.djsList.filter((g, idx) => {
				if (g['id'] == DJ.id) {
					index = idx;
					return true;
				}
				return false;
			});
			if (index != -1) {
				this.djsList.splice(index, 1);
			}
		} else {
			this.djsList.splice(this.djsList.indexOf(DJ), 1);
		}

		MusicRoomApi.instance.delete(MusicRoomApi.events + this.eventID + '/' + MusicRoomApi.djs + DJ.id, {}, response => {

			let index: number = -1;
			AppRootViewModel.instance.events[this.rights][this.eventID].djs.forEach((dj, idx) => {
				if (typeof dj == 'string' && dj == DJ.id) {
					index = idx;
				} else if (dj.id == DJ.id) {
					index = idx;
				}
			});
			if (index > -1) {
				AppRootViewModel.instance.events[this.rights][this.eventID].djs.splice(index, 1);
			}
		}, error => {
		});
	}

	public removeMultiple(djs: any[]): void {
		let djsIds: string[] = [];
		
		djs.forEach(DJ => {

			if (DJ.hasOwnProperty('requestSentIcon')) {
				let djSearch: DJSearch = DJ as DJSearch;
				djSearch.requestSentIcon = IconsCode.Check;
				djSearch.requestSentClass = DJsViewModel.AddClass;
				let index = -1;
				this.djsList.filter((g, idx) => {
					if (g['id'] == DJ.id) {
						index = idx;
						return true;
					}
					return false;
				});
				if (index != -1) {
					this.djsList.splice(index, 1);
				}
			} else {
				this.djsList.splice(this.djsList.indexOf(DJ), 1);
			}
			djsIds.push(DJ.id);
		});
		
		MusicRoomApi.instance.delete(MusicRoomApi.events + this.eventID + '/' + MusicRoomApi.djs, {
			userids: djsIds
		}, response => {

			for (let i: number = 0; i < djsIds.length; i++) {
				let index: number = -1;
				AppRootViewModel.instance.events[this.rights][this.eventID].djs.forEach((dj, idx) => {
					if (typeof dj == 'string') {
						index = djsIds.indexOf(dj);
					} else {
						index = djsIds.indexOf(dj.id);
					}
				});
				if (index > -1) {
					AppRootViewModel.instance.events[this.rights][this.eventID].djs.splice(index, 1);
				}
			}
		}, error => {
		});
	}

	public clearDJsList(): void {
		this.djsList.length = 0;
	}

	public clearSearchList(fillFriends: boolean): void {
		this.searchDJsList.length = 0;
		if (fillFriends) {
			this.fillSearchWithFriends();
		}
	}
}