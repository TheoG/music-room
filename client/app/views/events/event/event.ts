import { EventData, Page, PropertyChangeData, NavigatedData, Color } from "tns-core-modules/ui/page/page";
import { EventViewModel, EventTrack, EventRights } from "~/views/events/event/event-view-model";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as frameModule from "tns-core-modules/ui/frame"
import { Label } from "tns-core-modules/ui/label/label";
import { Image } from "tns-core-modules/ui/image";
import { IconsCode } from "~/utils/Icons";
import { CFAlertGravity, CFAlertActionAlignment, CFAlertActionStyle, CFAlertStyle } from 'nativescript-cfalert-dialog';
import * as dialogs from "tns-core-modules/ui/dialogs";
import { Slider } from "tns-core-modules/ui/slider";
import { TouchGestureEventData } from "tns-core-modules/ui/gestures/gestures";
import { ListViewEventData } from "nativescript-ui-listview";
import { Progress } from "tns-core-modules/ui/progress";
import { setText as clipboardSetText } from 'nativescript-clipboard'
import { MusicRoomApi } from "~/utils/MusicRoomApi";
import * as Toast from 'nativescript-toast';
import { Locker } from "~/utils/Lock";

let page = null;
let drawer = null;
let eventViewModel: EventViewModel = null;
let albumImage: Image = null;
let settingsIcon: Label = null;
let timelineSlider: Slider = null;
let timelineProgress: Progress = null;
let wasInParam: boolean = false;
let returning: boolean = false;

export function navigatingTo(args: NavigatedData): void {
	page = <Page>args.object;
	page.actionBarHidden = true;
	drawer = <RadSideDrawer>frameModule.topmost().parent;
	
	let context = page.navigationContext;
	if (context) {
		if (EventViewModel.instance) {
			eventViewModel = EventViewModel.instance;
			page.bindingContext = eventViewModel;
			returning = true;
		} else {
			eventViewModel = new EventViewModel(context.eventID);
			page.bindingContext = eventViewModel;
		}
		
		if (context.wasInParam) {
			wasInParam = context.wasInParam;
		}
	}
	
	
	albumImage = page.getViewById('album-image') as Image;
	settingsIcon = page.getViewById('settings-icon') as Label;
	
	timelineSlider = page.getViewById('timelineSlider') as Slider;
	timelineProgress = page.getViewById('timelineProgress') as Progress;
}

export function navigatedTo(args: NavigatedData): void {
	if (returning) {
		returning = false;
		eventViewModel.refreshEvent();
	}
}

export function navigatingFrom(args: NavigatedData): void {
	if (eventViewModel.shouldKeepSingleton) {
		eventViewModel.shouldKeepSingleton = false;
	} else {
		eventViewModel.destroy();
		gc();
	}
}

exports.backEvent = args => {
	if (wasInParam) {
		frameModule.topmost().navigate({
			moduleName: "views/events/events"
		});
		args.cancel = true;
	}
}

export function onTouchTimeline(args: TouchGestureEventData) {
	if (args.action == "move") {
		eventViewModel.moveCursor(timelineSlider.value);
	} else if (args.action == "up") {
		eventViewModel.stopMoveCursor(timelineSlider.value);
	}
}

export function onItemSelected(args: ListViewEventData) {
	if (eventViewModel.getRights() != EventRights.Guest) {
		eventViewModel.setCurrentTrack(args.index, true);
	}
}

export function rightActionIcon(args: ListViewEventData): void {

	let track: EventTrack = args.object.bindingContext;

	if (!eventViewModel.getEvent().votingActive) {
		Toast.makeText('Voting not active').show();
		return;
	}

	if (track.rightIcon.includes(IconsCode.Like)) {
		track.rightActionClass = 'icon right-action-icon liked';
		track.rightIcon = track.rightIcon.replace(IconsCode.Like, IconsCode.LikeFull);
		eventViewModel.like(track);

		if (track.leftIcon.includes(IconsCode.DislikeFull)) {
			track.leftActionClass = 'icon';
			track.leftIcon = track.leftIcon.replace(IconsCode.DislikeFull, IconsCode.Dislike);
		}

	} else if (track.rightIcon.includes(IconsCode.LikeFull)) {
		track.rightActionClass = 'icon right-action-icon';
		track.rightIcon = track.rightIcon.replace(IconsCode.LikeFull, IconsCode.Like);
		eventViewModel.unlike(track);
	}

}

export function leftActionIcon(args: ListViewEventData): void {

	let track: EventTrack = args.object.bindingContext;
	
	if (!eventViewModel.getEvent().votingActive) {
		Toast.makeText('Voting not active').show();
		return;
	}

	if (track.leftIcon.includes(IconsCode.Dislike)) {
		track.leftActionClass = 'icon disliked';
		track.leftIcon = track.leftIcon.replace(IconsCode.Dislike, IconsCode.DislikeFull);
		eventViewModel.dislike(track);

		if (track.rightIcon.includes(IconsCode.LikeFull)) {
			track.rightActionClass = 'icon right-action-icon';
			track.rightIcon = track.rightIcon.replace(IconsCode.LikeFull, IconsCode.Like);
		}

	} else if (track.leftIcon.includes(IconsCode.DislikeFull)) {
		track.leftActionClass = 'icon';
		track.leftIcon = track.leftIcon.replace(IconsCode.DislikeFull, IconsCode.Dislike);
		eventViewModel.undislike(track);
	}

}

export function goToPlaylists(): void {
	eventViewModel.shouldKeepSingleton = true;
	frameModule.topmost().navigate({
		moduleName: "views/events/event/playlists/playlists",
		context: {
			eventID: eventViewModel.getEvent().id,
			rights: eventViewModel.getRights()
		}
	});
}

export function toggleAlbumImage(args: EventData): void {
	albumImage.visibility = albumImage.visibility == "visible" ? "collapse" : "visible";
	settingsIcon.visibility = albumImage.visibility;
	if (albumImage.visibility == "visible") {
		eventViewModel.set("chevronIcon", IconsCode.ChevronUp);
	} else {
		eventViewModel.set("chevronIcon", IconsCode.ChevronDown);
	}
}

export function openSettings(args: EventData): void {

	let buttons = [];

	if (eventViewModel.getRights() == EventRights.Owner) {

		buttons = [
			{
				text: eventViewModel.getEvent().votingActive ? 'Stop Voting' : 'Start Voting',
				buttonStyle: CFAlertActionStyle.DEFAULT,
				buttonAlignment: CFAlertActionAlignment.JUSTIFIED,
				onClick: response => {
					if (eventViewModel.getEvent().votingActive) {
						eventViewModel.stopVoting();
					} else {
						eventViewModel.startVoting();
					}
				},
			},
			{
				text: eventViewModel.getEvent().eventActive ? 'Stop Event' : 'Start Event',
				buttonStyle: CFAlertActionStyle.DEFAULT,
				buttonAlignment: CFAlertActionAlignment.JUSTIFIED,
				onClick: response => {
					if (eventViewModel.getEvent().eventActive) {
						eventViewModel.stopEvent();
					} else {
						eventViewModel.startEvent();
					}
				},
			},
			{
				text: 'Guests',
				buttonStyle: CFAlertActionStyle.DEFAULT,
				buttonAlignment: CFAlertActionAlignment.JUSTIFIED,
			
				onClick: response => {
					eventViewModel.shouldKeepSingleton = true;
					frameModule.topmost().navigate({
						moduleName: "views/events/event/guests/guests",
						context: {
							eventID: eventViewModel.getEvent().id
						}
					});
				},
			},
			{
				text: 'DJs',
				buttonStyle: CFAlertActionStyle.DEFAULT,
				buttonAlignment: CFAlertActionAlignment.JUSTIFIED,
			
				onClick: response => {
					eventViewModel.shouldKeepSingleton = true;
					frameModule.topmost().navigate({
						moduleName: "views/events/event/djs/djs",
						context: {
							eventID: eventViewModel.getEvent().id
						}
					});
				},
			},
			{
				text: 'Parameters',
				buttonStyle: CFAlertActionStyle.DEFAULT,
				buttonAlignment: CFAlertActionAlignment.JUSTIFIED,
				onClick: response => {
					eventViewModel.shouldKeepSingleton = true;
					frameModule.topmost().navigate({
						moduleName: "views/events/event/params/event-params",
						context: {
							eventID: eventViewModel.getEvent().id
						}
					});
				},
			},
			{
				text: 'Delete',
				buttonStyle: CFAlertActionStyle.DEFAULT,
				buttonAlignment: CFAlertActionAlignment.JUSTIFIED,
			
				onClick: response => {
					dialogs.confirm({
						title: "Delete Event",
						message: "Are you sure?",
						okButtonText: "Yes",
						cancelButtonText: "No",
					}).then(result => {
						if (result) {
							eventViewModel.delete();
							frameModule.topmost().navigate({
								moduleName: "views/events/events"
							});
						}
					});
				},
			},
		];
	}

	buttons.push({
		text: 'Copy event link to clipboard',
		buttonStyle: CFAlertActionStyle.DEFAULT,
		buttonAlignment: CFAlertActionAlignment.JUSTIFIED,
		onClick: response => {
			clipboardSetText(`${MusicRoomApi.clientUrl}/${MusicRoomApi.events}${eventViewModel.getEvent().id}`).then(() => {
				Toast.makeText('Copied to clipboard!').show();
			});
		},
	});

	eventViewModel.options = {
		dialogStyle: CFAlertStyle.BOTTOM_SHEET,
		title: '',
		message: '',
		messageColor: "#212121",
		textAlignment: CFAlertGravity.START,
		buttons: buttons
	};
	eventViewModel.cfalertDialog.show(eventViewModel.options);
}

export function playPause(args: EventData): void {

	Locker.acquire(eventViewModel.getEvent().id, async () => {
		let label: Label = <Label>args.object;
	
		label.animate({
			scale: {x: 1.5, y: 1.5},
			duration: 100
		}).then(() => {
			label.animate({
				scale: {x: 1, y: 1},
				duration: 100
			});
		});
		
		if (eventViewModel.isPlaying()) {
			await eventViewModel.pause();
		} else {
			await eventViewModel.play();
		}
		Locker.release(eventViewModel.getEvent().id);
		
	});


}

export function prev(args: EventData): void {
	let label: Label = <Label>args.object;

	eventViewModel.prev();

	label.animate({
		scale: {x: 1.5, y: 1.5},
		duration: 100
	}).then(() => {
		label.animate({
			scale: {x: 1, y: 1},
			duration: 100
		});
	});
}

export function next(args: EventData): void {
	let label: Label = <Label>args.object;

	eventViewModel.next();

	label.animate({
		scale: {x: 1.5, y: 1.5},
		duration: 100
	}).then(() => {
		label.animate({
			scale: {x: 1, y: 1},
			duration: 100
		});
	});
}