import { Observable, fromObject as observableFromObject } from "tns-core-modules/data/observable";
import { ObservableArray } from "tns-core-modules/data/observable-array/observable-array";
import { IconsCode } from "~/utils/Icons"
import { MusicRoomApi } from "~/utils/MusicRoomApi";
import { EventAPI, AppRootViewModel, PlaylistAPI, TrackAPI, UserAPI } from "~/app-root-view-model";
import { userIsGuest, userIsEditor } from "~/utils/Tools";
import { EventRights } from "~/views/events/event/event-view-model";

export interface PlaylistModel {
	id: string;
	name: string;
	tracks: string[] | TrackAPI[];
	editors: string[] | UserAPI[];
	guests: string[] | UserAPI[];
	sortingType: string;
	owner: string;
	visibility: string;
	editing: string;
}

export interface Playlist extends PlaylistModel {
	sortingTypeIcon: string;
	swipeActionIcon: string;
	swipeActionClass: string;
	accessTypeIcon: string;
	selectedClass: string;
}

export class PlaylistsViewModel extends Observable {
	
	private eventPlaylists: ObservableArray<Observable> = new ObservableArray();
	private eventSearchPlaylists: ObservableArray<Observable> = new ObservableArray();

	private eventID: string = null;
	
	private userPlaylists: Playlist[];
	private rights: EventRights = null;

	public activePlaylist: string = null;

	constructor(rights: EventRights, eventID: string) {
		super();

		this.set("actionBarTitle", "Event Playlists");
		this.set("searchPlaylists", this.eventSearchPlaylists);
		this.set("playlists", this.eventPlaylists);

		this.userPlaylists = [];
		this.activePlaylist = null;
		this.eventID = eventID;
		this.rights = rights;

		if (this.rights == EventRights.Owner && ((!AppRootViewModel.instance.events[this.rights][this.eventID].votingActive && !AppRootViewModel.instance.events[this.rights][this.eventID].eventActive))) {
			this.set("isOwnerAndNotStarted", true);
		}
	}

	public getRights(): EventRights {
		return this.rights;
	}

	public getEvent(): EventAPI {
		return AppRootViewModel.instance.events[this.rights][this.eventID];
	}

	public getObservablePlaylist(index: number): Observable {
		if (index < 0 || !this.eventPlaylists || index > this.eventPlaylists.length) {
			return null;
		}
		return this.eventPlaylists.getItem(index);
	}

	public getObservableSearchPlaylist(index: number): Observable {
		return this.eventSearchPlaylists.getItem(index);
	}

	public async fillEventPlaylists(apiCall: boolean): Promise<void> {
		this.eventPlaylists.length = 0;

		if (apiCall) {
			await MusicRoomApi.instance.get(MusicRoomApi.events + this.eventID, response => {
				AppRootViewModel.instance.events[this.rights][this.eventID] = response;
			}, error => {
			});
		}

		if (AppRootViewModel.instance.events[this.rights][this.eventID].votingActive || AppRootViewModel.instance.events[this.rights][this.eventID].eventActive) {
			// Event playlists
			AppRootViewModel.instance.events[this.rights][this.eventID].eventPlaylists.forEach((pl: any) => {
				let playlist: Playlist = {
					id: pl.id,
					tracks: pl.tracks,
					editors: pl.editors,
					guests: pl.guests,
					name: pl.name,
					owner: pl.owner,
					visibility: pl.visibility,
					editing: pl.editing,
					sortingType: pl.sortingType,
					sortingTypeIcon: pl.sortingType == "move" ? IconsCode.ArrowUp + IconsCode.ArrowDown : IconsCode.Like,
					swipeActionIcon: IconsCode.Play,
					swipeActionClass: "add-container",
					accessTypeIcon: '',
					selectedClass: ''
				};
				this.eventPlaylists.push(observableFromObject(playlist));
			});

		} else {
			// Regular playlists
			AppRootViewModel.instance.events[this.rights][this.eventID].playlists.forEach((pl: any) => {
				let playlist: Playlist = {
					id: pl.id,
					tracks: pl.tracks,
					editors: pl.editors,
					guests: pl.guests,
					name: pl.name,
					owner: pl.owner,
					visibility: pl.visibility,
					editing: pl.editing,
					sortingType: pl.sortingType,
					sortingTypeIcon: pl.sortingType == "move" ? IconsCode.ArrowUp + IconsCode.ArrowDown : IconsCode.Like,
					swipeActionIcon: IconsCode.Remove,
					swipeActionClass: "delete-container",
					accessTypeIcon: '',
					selectedClass: ''
				};
	
				if (userIsGuest(pl)) {
					playlist.accessTypeIcon = IconsCode.Eye;
				} else if (userIsEditor(pl)) {
					playlist.accessTypeIcon = IconsCode.Pencil;
				}
	
				this.eventPlaylists.push(observableFromObject(playlist));
			});
		}
	}

	public setActivePlaylist(playlist: Playlist): void {
		AppRootViewModel.instance.activePlaylist = playlist.id;
	}

	public async fillPlaylists(): Promise<void> {

		if (AppRootViewModel.instance.events[this.rights][this.eventID].votingActive || AppRootViewModel.instance.events[this.rights][this.eventID].eventActive) {
			return ;
		}

		this.userPlaylists = [];
		await MusicRoomApi.instance.get(MusicRoomApi.playlists, response => {
			response.owner.forEach(pl => {
				let playlist: Playlist = {
					id: pl.id,
					tracks: pl.tracks,
					editors: pl.editors,
					guests: pl.guests,
					name: pl.name,
					owner: pl.owner,
					visibility: pl.visibility,
					editing: pl.editing,
					sortingType: pl.sortingType,
					sortingTypeIcon: pl.sortingType == "move" ? IconsCode.ArrowUp + IconsCode.ArrowDown : IconsCode.Like,
					swipeActionIcon: IconsCode.Remove,
					swipeActionClass: "delete-container",
					accessTypeIcon: null,
					selectedClass: ""
				}
				if (!AppRootViewModel.instance.events[this.rights][this.eventID].playlists.some((p: any) => p.id == playlist.id)) {
					this.userPlaylists.push(playlist);
				}
			});

			response.editor.forEach(pl => {
				let playlist: Playlist = {
					id: pl.id,
					tracks: pl.tracks,
					editors: pl.editors,
					guests: pl.guests,
					name: pl.name,
					owner: pl.owner,
					visibility: pl.visibility,
					editing: pl.editing,
					sortingType: pl.sortingType,
					sortingTypeIcon: pl.sortingType == "move" ? IconsCode.ArrowUp + IconsCode.ArrowDown : IconsCode.Like,
					swipeActionIcon: IconsCode.Remove,
					swipeActionClass: "delete-container",
					accessTypeIcon: IconsCode.Pencil,
					selectedClass: ""
				}
				if (!AppRootViewModel.instance.events[this.rights][this.eventID].playlists.some((p: any) => p.id == playlist.id)) {
					this.userPlaylists.push(playlist);
				}
			});

			response.guest = response.guest.filter(playlist => response.editor.filter(pl => pl.id == playlist.id).length == 0);

			response.guest.forEach(pl => {
				let playlist: Playlist = {
					id: pl.id,
					tracks: pl.tracks,
					editors: pl.editors,
					guests: pl.guests,
					name: pl.name,
					owner: pl.owner,
					visibility: pl.visibility,
					editing: pl.editing,
					sortingType: pl.sortingType,
					sortingTypeIcon: pl.sortingType == "move" ? IconsCode.ArrowUp + IconsCode.ArrowDown : IconsCode.Like,
					swipeActionIcon: IconsCode.Remove,
					swipeActionClass: "delete-container",
					accessTypeIcon: IconsCode.Eye,
					selectedClass: ""
				}
				if (!AppRootViewModel.instance.events[this.rights][this.eventID].playlists.some((p: any) => p.id == playlist.id)) {
					this.userPlaylists.push(playlist);
				}
			});
		}, error => {
		});
		this.fillUserPlaylists();
	}

	public fillUserPlaylists(): void {
		this.eventSearchPlaylists.length = 0;

		this.userPlaylists.forEach(playlist => {
			if (this.eventPlaylists.length == 0 || !this.eventPlaylists.some(pl => pl['id'] == playlist.id)) {
				playlist.swipeActionIcon = IconsCode.Check;
				playlist.swipeActionClass = "add-container";
				this.eventSearchPlaylists.push(observableFromObject(playlist));
			}
		});
	}

	public fillSearchPlaylists(playlists: PlaylistAPI[]): void {
		this.eventSearchPlaylists.splice(0);

		playlists.forEach(playlist => {
			if (this.eventPlaylists.some((p: any) => p.id == playlist.id)) {
				return ;
			}

			let pl: Playlist = {
				id: playlist.id,
				tracks: playlist.tracks,
				editors: playlist.editors,
				guests: playlist.guests,
				name: playlist.name,
				owner: playlist.owner,
				visibility: playlist.visibility,
				editing: playlist.editing,
				sortingType: playlist.sortingType,
				sortingTypeIcon: playlist.sortingType == "move" ? IconsCode.ArrowUp + IconsCode.ArrowDown : IconsCode.Like,
				swipeActionIcon: IconsCode.Check,
				swipeActionClass: "add-container",
				accessTypeIcon: '',
				selectedClass: ''
			};
			this.eventSearchPlaylists.push(observableFromObject(pl));
		});
	}


	public add(playlist: Playlist): void {
		let newPlaylist: Playlist = {
			id: playlist.id,
			tracks: playlist.tracks,
			editors: playlist.editors,
			guests: playlist.guests,
			name: playlist.name,
			owner: playlist.owner,
			visibility: playlist.visibility,
			editing: playlist.editing,
			sortingType: playlist.sortingType,
			sortingTypeIcon: playlist.sortingType == "move" ? IconsCode.ArrowUp + IconsCode.ArrowDown : IconsCode.Like,
			swipeActionIcon: IconsCode.Remove,
			swipeActionClass: "delete-container",
			accessTypeIcon: playlist.accessTypeIcon,
			selectedClass: ""
		}

		this.eventPlaylists.push(observableFromObject(newPlaylist));
		let index: number = -1;
		this.eventSearchPlaylists.filter((pl: any, idx) => {
			if (pl.id == playlist.id) {
				index = idx;
			}
			return false;
		});
		if (index > -1) {
			this.eventSearchPlaylists.splice(index, 1);
		}
		index = this.userPlaylists.findIndex(pl => pl.id == playlist.id);
		if (index > -1) {
			this.userPlaylists.splice(index, 1);
		}

		AppRootViewModel.instance.events[this.rights][this.eventID].playlists.push(playlist as any as PlaylistAPI);
		AppRootViewModel.instance.activePlaylist = playlist.id;

		MusicRoomApi.instance.put(MusicRoomApi.events + this.eventID + '/' + MusicRoomApi.playlists + playlist.id, {
		}, response => {
		}, error => {
		});
	}

	public addMultiple(playlists: Playlist[]): void {
		let playlistids: string[] = [];
		
		playlists.forEach(playlist => {			
			let newPlaylist: Playlist = {
				id: playlist.id,
				tracks: playlist.tracks,
				editors: playlist.editors,
				guests: playlist.guests,
				name: playlist.name,
				owner: playlist.owner,
				visibility: playlist.visibility,
				editing: playlist.editing,
				sortingType: playlist.sortingType,
				sortingTypeIcon: playlist.sortingType == "move" ? IconsCode.ArrowUp + IconsCode.ArrowDown : IconsCode.Like,
				swipeActionIcon: IconsCode.Remove,
				swipeActionClass: "delete-container",
				accessTypeIcon: playlist.accessTypeIcon,
				selectedClass: ""
			}

			this.eventPlaylists.push(observableFromObject(newPlaylist));
			playlistids.push(playlist.id);

			AppRootViewModel.instance.events[this.rights][this.eventID].playlists.push(playlist as any as PlaylistAPI);
			AppRootViewModel.instance.activePlaylist = playlist.id;

			let index: number = -1;
			this.eventSearchPlaylists.filter((pl, idx) => {
				if (pl['id'] == playlist.id) {
					index = idx;
				}
				return false;
			});
			if (index > -1) {
				this.eventSearchPlaylists.splice(index, 1);
			}
			index = this.userPlaylists.findIndex(pl => pl['id'] == playlist.id);
			if (index > -1) {
				this.userPlaylists.splice(index, 1);
			}
		});

		MusicRoomApi.instance.put(MusicRoomApi.events + this.eventID + '/' + MusicRoomApi.playlists, {
			playlistids
		}, response => {
		}, error => {
		});
	}

	public remove(playlist: Playlist): void {

		let playlistSearch: Playlist = {
			id: playlist.id,
			tracks: playlist.tracks,
			editors: playlist.editors,
			guests: playlist.guests,
			name: playlist.name,
			owner: playlist.owner,
			visibility: playlist.visibility,
			editing: playlist.editing,
			sortingType: playlist.sortingType,
			sortingTypeIcon: playlist.sortingType == "move" ? IconsCode.ArrowUp + IconsCode.ArrowDown : IconsCode.Like,
			swipeActionIcon: IconsCode.Remove,
			swipeActionClass: "delete-container",
			accessTypeIcon: playlist.accessTypeIcon,
			selectedClass: ""
		};

		let index: number = -1;
		this.eventPlaylists.filter((pl: any, idx) => {
			if (pl.id == playlist.id) {
				index = idx;
				return true;
			}
			return false;
		});
		if (index > -1) {
			this.eventPlaylists.splice(index, 1);	
		}
		
		this.userPlaylists.push(playlistSearch);
		
		index = -1;
		AppRootViewModel.instance.events[this.rights][this.eventID].playlists.forEach((pl, idx) => {
			if (pl.id == playlist.id) {
				index = idx;
			}
		});
		if (index != -1) {
			AppRootViewModel.instance.events[this.rights][this.eventID].playlists.splice(index);
			AppRootViewModel.instance.activePlaylist = AppRootViewModel.instance.events[this.rights][this.eventID].playlists.length > 0 ? AppRootViewModel.instance.events[this.rights][this.eventID].playlists[0].id : '';
		}

		MusicRoomApi.instance.delete(MusicRoomApi.events + this.eventID + '/' + MusicRoomApi.playlists + playlist.id, {
		}, response => {
		}, error => {
		});
	}

	public removeMultiple(playlists: Playlist[]): void {
		let playlistids: string[] = [];
		
		playlists.forEach(playlist => {
			let playlistSearch: Playlist = {
				id: playlist.id,
				tracks: playlist.tracks,
				editors: playlist.editors,
				guests: playlist.guests,
				name: playlist.name,
				owner: playlist.owner,
				visibility: playlist.visibility,
				editing: playlist.editing,
				sortingType: playlist.sortingType,
				sortingTypeIcon: playlist.sortingType == "move" ? IconsCode.ArrowUp + IconsCode.ArrowDown : IconsCode.Like,
				swipeActionIcon: IconsCode.Remove,
				swipeActionClass: "delete-container",
				accessTypeIcon: playlist.accessTypeIcon,
				selectedClass: ""
			};
			let index: number = -1;
			this.eventPlaylists.filter((pl: any, idx) => {
				if (pl.id == playlist.id) {
					index = idx;
					return true;
				}
				return false;
			});
			if (index > -1) {
				this.eventPlaylists.splice(index, 1);	
			}		
			this.userPlaylists.push(playlistSearch);
			playlistids.push(playlist.id);

			index = -1;
			AppRootViewModel.instance.events[this.rights][this.eventID].playlists.forEach((pl, idx) => {
				if (pl.id == playlist.id) {
					index = idx;
				}
			});
			if (index != -1) {
				AppRootViewModel.instance.events[this.rights][this.eventID].playlists.splice(index);
				AppRootViewModel.instance.activePlaylist = AppRootViewModel.instance.events[this.rights][this.eventID].playlists.length > 0 ? AppRootViewModel.instance.events[this.rights][this.eventID].playlists[0].id : '';
			}
		});

		MusicRoomApi.instance.delete(MusicRoomApi.events + this.eventID + '/' + MusicRoomApi.playlists, {
			playlistids
		}, response => {
		}, error => {
		});
	}

	public async search(keyword: string): Promise<void> {
		if (keyword.length < 3) {
			return ;
		}
		
		await MusicRoomApi.instance.get(MusicRoomApi.playlistsSearch, response => {
		if (!response.hasOwnProperty("errorCode")) {
			this.fillSearchPlaylists(response.results);
		}
		}, error => {
		}, {
			keyword
		});
	}

	public clearSearchList(): void {
		this.eventSearchPlaylists.length = 0;
	}
}