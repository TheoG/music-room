import { ItemEventData } from "tns-core-modules/ui/list-view";
import { Page, View, NavigatedData, EventData, Observable } from "tns-core-modules/ui/page";
import { TextField } from "tns-core-modules/ui/text-field/text-field";
import { StackLayout } from "tns-core-modules/ui/layouts/stack-layout/stack-layout";
import { SwipeActionsEventData, ListViewEventData, RadListView } from "nativescript-ui-listview";
import { PlaylistsViewModel } from './playlists-view-model'
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Playlist } from "~/views/events/event/playlists/playlists-view-model";
import { EventAPI, AppRootViewModel } from "~/app-root-view-model";
import { IconsCode } from "~/utils/Icons";
import { EventRights } from "~/views/events/event/event-view-model";

import * as Toast from 'nativescript-toast';
import * as frameModule from "tns-core-modules/ui/frame"

let page: Page = null;
let playlistsViewModel: PlaylistsViewModel = null;
let drawer: RadSideDrawer = null;
let searchLayout: StackLayout = null;
let displayed: boolean = false;
let rlvSearch: RadListView = null;
let rlvPlaylists: RadListView = null;
let onMultipleSelection: boolean = false;
let onMultipleSelectionSearch: boolean = false;

export function navigatingTo(args: NavigatedData): void {
	drawer = <RadSideDrawer>frameModule.topmost().parent;

	page = <Page>args.object;

	let context = page.navigationContext;
	let eventID: string = null;
	let rights: EventRights = null;
	if (context) {
		eventID = context.eventID;
		rights = context.rights;
	}
	
	playlistsViewModel = new PlaylistsViewModel(rights, eventID);
	page.bindingContext = playlistsViewModel;

	searchLayout = <StackLayout>page.getViewById("search-layout");
	searchLayout.visibility = "collapse";
	displayed = false;

	rlvPlaylists = page.getViewById("playlistsListView");
	rlvSearch = page.getViewById("searchPlaylistsListView");
	rlvSearch.height = 0;
	rlvPlaylists.height = "auto";

	searchPlaylistsEvent();

	onMultipleSelection = false;
	onMultipleSelectionSearch = false;

	playlistsViewModel.fillEventPlaylists(false);
	playlistsViewModel.fillPlaylists();
}

exports.backEvent = args => {
	if (displayed) {
		closeSearchMenu();
		args.cancel = true;
	}
}

export function pullRefreshPlaylists(): void {
	rlvPlaylists.deselectAll();
	playlistsViewModel.fillEventPlaylists(true);
	rlvPlaylists.notifyPullToRefreshFinished();
}

export function onItemSelected(args: ItemEventData): void {
	let playlist: Playlist = playlistsViewModel.getObservablePlaylist(args.index) as any;

	playlist.selectedClass = "list-group-item-selected";
	playlistsViewModel.set("actionBarTitle", "Remove Playlists (" + rlvPlaylists.getSelectedItems().length + ")");
	
	if (!onMultipleSelection) {
		playlistsViewModel.set("menuIcon", "res://back_arrow");
		playlistsViewModel.set("confirmIcon", IconsCode.Remove);
		onMultipleSelection = true;
	}
}

export function onItemDeselected(args: ItemEventData): void {
	let playlist: Observable = playlistsViewModel.getObservablePlaylist(args.index);
	if (playlist) {
		playlist['selectedClass'] = '';
	}

	playlistsViewModel.set("actionBarTitle", "Remove Playlists (" + rlvPlaylists.getSelectedItems().length + ")");
	if (rlvPlaylists.getSelectedItems().length == 0) {
		onMultipleSelection = false;
		playlistsViewModel.set("menuIcon", "res://menu_light");
		playlistsViewModel.set("actionBarTitle", "Event Playlists");
		playlistsViewModel.set("confirmIcon", "");
	}
}

export function onItemTap(args: ItemEventData): void {
	let playlist: Playlist = playlistsViewModel.getObservablePlaylist(args.index) as any;
	if (onMultipleSelection) {
		if (rlvPlaylists.getSelectedItems().indexOf(args.view.bindingContext) == -1) {
			rlvPlaylists.selectItemAt(args.index);
			
		} else {
			rlvPlaylists.deselectItemAt(args.index);
		}
	} else {
		let eventAPI: EventAPI = playlistsViewModel.getEvent();
		if (eventAPI.votingActive || eventAPI.eventActive) {
			playlistsViewModel.setActivePlaylist(playlist);
			frameModule.topmost().goBack();
			
		} else {
			let canAccess: boolean = false;
			if (typeof playlist.owner == 'string' && playlist.owner == AppRootViewModel.instance.me.id) {
				canAccess = true;
			} else if (typeof playlist.owner != 'string' && (<any>playlist.owner).id == AppRootViewModel.instance.me.id) {
				canAccess = true;
			}
			if (!canAccess) {
				canAccess = (<any[]>playlist.guests).some(e => {
					if (typeof e == 'string' && e == AppRootViewModel.instance.me.id) {
						return true;
					} else if (typeof e != 'string' && e.id == AppRootViewModel.instance.me.id) {
						return true;
					}
					return false;
				});
			}
			if (!canAccess) {
				canAccess = (<any[]>playlist.editors).some(e => {
					if (typeof e == 'string' && e == AppRootViewModel.instance.me.id) {
						return true;
					} else if (typeof e != 'string' && e.id == AppRootViewModel.instance.me.id) {
						return true;
					}
					return false;
				});
			}
			if (!canAccess && (playlist.visibility == 'PUBLIC' || playlist.editing == 'PUBLIC')) {
				canAccess = true;
			}

			if (canAccess) {
				frameModule.topmost().navigate({
					moduleName: "views/playlists/playlist/playlist",
					context: {
						playlist,
						wasInParam: false
					}
				});
			} else {
				Toast.makeText('You\'re not allowed to view and / or edit this playlist', 'short').show();
			}
		}
	}
}
	
export function onItemSelectedSearch(args: ItemEventData): void {
	let playlist: Playlist = args.view.bindingContext;

	playlist.selectedClass = "list-group-item-selected";
	playlistsViewModel.set("actionBarTitle", "Add Playlists (" + rlvSearch.getSelectedItems().length + ")");
	
	if (!onMultipleSelectionSearch) {
		playlistsViewModel.set("menuIcon", "res://back_arrow");
		playlistsViewModel.set("confirmIcon", IconsCode.Check);
		onMultipleSelectionSearch = true;
	}
}

export function onItemDeselectedSearch(args: ItemEventData): void {
	let playlist: any = playlistsViewModel.getObservableSearchPlaylist(args.index);
	if (playlist) {
		playlist.selectedClass = '';
	}

	playlistsViewModel.set("actionBarTitle", "Add Playlists (" + rlvSearch.getSelectedItems().length + ")");
	
	if (rlvSearch.getSelectedItems().length == 0) {
		onMultipleSelectionSearch = false;
		playlistsViewModel.set("menuIcon", "res://menu_light");
		playlistsViewModel.set("actionBarTitle", "Event Playlists");
		playlistsViewModel.set("confirmIcon", "");
	}
}

export function onItemTapSearch(args: ItemEventData): void {
	if (onMultipleSelectionSearch) {
		if (rlvSearch.getSelectedItems().indexOf(args.view.bindingContext) == -1) {
			rlvSearch.selectItemAt(args.index);
		} else {
			rlvSearch.deselectItemAt(args.index);
		}
	}
}

export function onConfirm(args: ItemEventData): void {
	if (onMultipleSelectionSearch) {
		let selectedPlaylists: Playlist[] = rlvSearch.getSelectedItems();
		rlvSearch.deselectAll();
		playlistsViewModel.addMultiple(selectedPlaylists);
	} else {
		let selectedPlaylists: Playlist[] = rlvPlaylists.getSelectedItems();
		rlvPlaylists.deselectAll();
		playlistsViewModel.removeMultiple(selectedPlaylists);
	}
}

export function onSwipeCellStarted(args: SwipeActionsEventData) {
	if (playlistsViewModel.getRights() == EventRights.Guest) {
		return ;
	}
	const swipeLimits = args.data.swipeLimits;
	const swipeView = args.object;
	const leftItem = swipeView.getViewById<View>('left-delete-view');
	const rightItem = swipeView.getViewById<View>('right-delete-view');
	swipeLimits.left = leftItem.getMeasuredWidth();
	swipeLimits.right = rightItem.getMeasuredWidth();
	swipeLimits.threshold = leftItem.getMeasuredWidth() / 2;
}
	

export function onLeftSwipeClick(args: ListViewEventData) {
	const listView: RadListView = page.getViewById("playlistsListView");
	let playlist: Playlist = args.object.bindingContext;
	if (playlist.swipeActionIcon == IconsCode.Check) {
		playlistsViewModel.add(playlist);
		rlvSearch.deselectAll();
	} else if (playlist.swipeActionIcon == IconsCode.Play) {
		playlistsViewModel.setActivePlaylist(playlist);
		listView.notifySwipeToExecuteFinished();
	} else {
		playlistsViewModel.remove(playlist);
		rlvPlaylists.deselectAll();
	}
}

export function onRightSwipeClick(args: ListViewEventData) {
    const listView: RadListView = page.getViewById("playlistsListView");
	let playlist: Playlist = args.object.bindingContext;
	if (playlist.swipeActionIcon == IconsCode.Check) {
		rlvSearch.deselectAll();
		playlistsViewModel.add(playlist);
	} else if (playlist.swipeActionIcon == IconsCode.Play) {
		playlistsViewModel.setActivePlaylist(playlist);
		listView.notifySwipeToExecuteFinished();
	} else {
		rlvPlaylists.deselectAll();
		playlistsViewModel.remove(playlist);
	}
}

export function onSwipeSearchCellStarted(args: SwipeActionsEventData) {
	const swipeLimits = args.data.swipeLimits;
	const swipeView = args.object;
	const leftItem = swipeView.getViewById<View>('left-add-view');
	const rightItem = swipeView.getViewById<View>('right-add-view');
	swipeLimits.left = leftItem.getMeasuredWidth();
	swipeLimits.right = rightItem.getMeasuredWidth();
	swipeLimits.threshold = leftItem.getMeasuredWidth() / 2;
}

export function onLeftSearchSwipeClick(args: ListViewEventData) {
	let playlist: Playlist = args.object.bindingContext;
	if (playlist.swipeActionIcon == IconsCode.Check) {
		rlvSearch.deselectAll();
		playlistsViewModel.add(playlist);
	}
}

export function onRightSearchSwipeClick(args: ListViewEventData) {
	let playlist: Playlist = args.object.bindingContext;
	if (playlist.swipeActionIcon == IconsCode.Check) {
		rlvSearch.deselectAll();
		playlistsViewModel.add(playlist);
	}
}

export function onPullToRefreshInitiated(args: ListViewEventData) {
    const listView: RadListView = page.getViewById("playlistsListView");
    playlistsViewModel.fillPlaylists();
	listView.notifyPullToRefreshFinished();
}

export function addNewPlaylist(args: EventData): void {
	if (searchLayout) {
		toggleSearchMenu();	
	}
}

function toggleSearchMenu() {
	if (!displayed) {
		openSearchMenu();
	} else {
		closeSearchMenu();
	}
}

function openSearchMenu() {
	rlvPlaylists.deselectAll();
	rlvSearch.height = "auto";
	rlvPlaylists.height = 0;
	displayed = true;
	searchLayout.visibility = "visible";
	playlistsViewModel.fillUserPlaylists();
}

function closeSearchMenu() {
	rlvSearch.deselectAll();
	rlvSearch.height = 0;
	rlvPlaylists.height = "auto";
	searchLayout.visibility = "collapse";
	
	let searchGuestsTF: TextField = page.getViewById("search_playlists");
	searchGuestsTF.text = "";
	playlistsViewModel.clearSearchList();
	displayed = false;
}

export function showSideDrawer(): void {
	if (drawer) {
		if (playlistsViewModel.get("menuIcon") == "res://back_arrow") {
			if (onMultipleSelection) {
				rlvPlaylists.deselectAll();
			} else if (onMultipleSelectionSearch) {
				rlvSearch.deselectAll();
			}
		} else {
			drawer.showDrawer();
		}
	}
}

function searchPlaylistsEvent(): void {
	let searchPlaylistsTF: TextField = page.getViewById("search_playlists");
	searchPlaylistsTF.on("textChange", () => {
		playlistsViewModel.search(searchPlaylistsTF.text);
	});
}