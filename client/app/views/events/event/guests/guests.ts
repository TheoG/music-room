import { EventData, Observable } from "tns-core-modules/data/observable";
import { Page, View } from "tns-core-modules/ui/page";
import { StackLayout } from "tns-core-modules/ui/layouts/stack-layout";
import * as frameModule from "tns-core-modules/ui/frame"
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { RadListView, ListViewEventData, SwipeActionsEventData } from "nativescript-ui-listview";
import { TextField } from "tns-core-modules/ui/text-field/text-field";
import { GuestsViewModel, GuestSearch } from "~/views/events/event/guests/guests-view-model";
import { IconsCode } from "~/utils/Icons";
import { ItemEventData } from "tns-core-modules/ui/list-view/list-view";

let page: Page = null;
let eventGuestsViewModel: GuestsViewModel = null;
let drawer: RadSideDrawer = null;
let searchLayout: StackLayout = null;
let displayed: boolean = false;
let onMultipleSelectionSearch: boolean = false;
let onMultipleSelection: boolean = false;
let rlvSearch: RadListView = null;
let rlvGuests: RadListView = null;

export function navigatingTo(args: EventData): void {
	drawer = <RadSideDrawer>frameModule.topmost().parent;

	page = <Page>args.object;
	
	let context = page.navigationContext;
	if (context) {
		eventGuestsViewModel = new GuestsViewModel(context.eventID);
		page.bindingContext = eventGuestsViewModel;
	}

	searchLayout = <StackLayout>page.getViewById("search-layout");
	searchLayout.visibility = "collapse";
	displayed = false;

	rlvGuests = page.getViewById("guestsListView");
	rlvSearch = page.getViewById("searchGuestsListView");
	rlvSearch.height = 0;
	rlvGuests.height = "auto";

	eventGuestsViewModel.set("menuIcon", "res://menu_light");
	eventGuestsViewModel.set("actionBarTitle", "Guests");

	searchGuestsEvent();
}

exports.backEvent = args => {
	if (displayed) {
		closeSearchMenu();
		args.cancel = true;
	}
}

export function pullRefreshGuests(): void {

	rlvGuests.deselectAll();
	eventGuestsViewModel.refreshEvent();
	rlvGuests.notifyPullToRefreshFinished();

}

export function searchNewGuests(args: EventData): void {
	if (searchLayout) {
		toggleSearchMenu();	
	}
}

function toggleSearchMenu() {
	if (!displayed) {
		openSearchMenu();
	} else {
		closeSearchMenu();
	}
}

function openSearchMenu() {
	rlvGuests.deselectAll();
	rlvSearch.height = "auto";
	rlvGuests.height = 0;
	displayed = true;
	searchLayout.visibility = "visible";
	eventGuestsViewModel.clearSearchList(true);
}

function closeSearchMenu() {
	rlvSearch.deselectAll();
	rlvSearch.height = 0;
	rlvGuests.height = "auto";
	searchLayout.visibility = "collapse";
	
	let searchGuestsTF: TextField = page.getViewById("search_guests");
	searchGuestsTF.text = "";
	eventGuestsViewModel.clearSearchList(true);
	displayed = false;
}

export function onItemSelected(args: ItemEventData): void {
	let guest: any = eventGuestsViewModel.getObservableGuest(args.index);
	if (guest) {
		guest.selectedClass = '';
	}

	guest.selectedClass = "list-group-item-selected";
	eventGuestsViewModel.set("actionBarTitle", "Remove Guests (" + rlvGuests.getSelectedItems().length + ")");
	
	if (!onMultipleSelection) {
		eventGuestsViewModel.set("menuIcon", "res://back_arrow");
		eventGuestsViewModel.set("confirmIcon", IconsCode.Remove);
		onMultipleSelection = true;
	}
}

export function onItemDeselected(args: ItemEventData): void {
	let guest: any = eventGuestsViewModel.getObservableGuest(args.index);
	if (guest) {
		guest.selectedClass = '';
	}

	eventGuestsViewModel.set("actionBarTitle", "Remove Guests (" + rlvGuests.getSelectedItems().length + ")");
	if (rlvGuests.getSelectedItems().length == 0) {
		onMultipleSelection = false;
		eventGuestsViewModel.set("menuIcon", "res://menu_light");
		eventGuestsViewModel.set("actionBarTitle", "Guests");
		eventGuestsViewModel.set("confirmIcon", "");
	}
}

export function onItemTap(args: ItemEventData): void {
	if (onMultipleSelection) {
		if (rlvGuests.getSelectedItems().indexOf(args.view.bindingContext) == -1) {
			rlvGuests.selectItemAt(args.index);
			
		} else {
			rlvGuests.deselectItemAt(args.index);
		}
	}
}

export function onItemSelectedSearch(args: ItemEventData): void {
	let guest: any = eventGuestsViewModel.getObservableSearchGuest(args.index);

	guest.selectedClass = "list-group-item-selected";
	eventGuestsViewModel.set("actionBarTitle", "Add Guests (" + rlvSearch.getSelectedItems().length + ")");
	
	if (!onMultipleSelectionSearch) {
		eventGuestsViewModel.set("menuIcon", "res://back_arrow");
		eventGuestsViewModel.set("confirmIcon", IconsCode.Check);
		onMultipleSelectionSearch = true;
	}
}

export function onItemDeselectedSearch(args: ItemEventData): void {
	let guest: any = eventGuestsViewModel.getObservableSearchGuest(args.index);
	if (guest) {
		guest.selectedClass = '';
	}

	eventGuestsViewModel.set("actionBarTitle", "Add Guests (" + rlvSearch.getSelectedItems().length + ")");
	if (rlvSearch.getSelectedItems().length == 0) {
		onMultipleSelectionSearch = false;
		eventGuestsViewModel.set("menuIcon", "res://menu_light");
		eventGuestsViewModel.set("actionBarTitle", "Guests");
		eventGuestsViewModel.set("confirmIcon", "");
	}
}

export function onItemTapSearch(args: ItemEventData): void {
	if (onMultipleSelectionSearch) {
		if (rlvSearch.getSelectedItems().indexOf(args.view.bindingContext) == -1) {
			rlvSearch.selectItemAt(args.index);
			
		} else {
			rlvSearch.deselectItemAt(args.index);
		}
	}
}

export function onConfirm(args: ItemEventData): void {
	if (onMultipleSelectionSearch) {
		let selectedGuests: GuestSearch[] = rlvSearch.getSelectedItems();
		rlvSearch.deselectAll();
		eventGuestsViewModel.addMultiple(selectedGuests);
	} else {
		let selectedGuests: GuestSearch[] = rlvGuests.getSelectedItems();
		rlvGuests.deselectAll();
		eventGuestsViewModel.removeMultiple(selectedGuests);
	}
}

export function onSwipeCellStarted(args: SwipeActionsEventData) {
	const swipeLimits = args.data.swipeLimits;
	const swipeView = args.object;
	const leftItem = swipeView.getViewById<View>('left-delete-view');
	const rightItem = swipeView.getViewById<View>('right-delete-view');
	swipeLimits.left = leftItem.getMeasuredWidth();
	swipeLimits.right = rightItem.getMeasuredWidth();
	swipeLimits.threshold = leftItem.getMeasuredWidth() / 2;
}

export function onLeftSwipeClick(args: ListViewEventData) {
	let guest: any = args.object.bindingContext;
	eventGuestsViewModel.remove(guest);
}

export function onRightSwipeClick(args: ListViewEventData) {
	let guest: any = args.object.bindingContext;
	eventGuestsViewModel.remove(guest);
}

export function onSwipeSearchCellStarted(args: SwipeActionsEventData) {
	const swipeLimits = args.data.swipeLimits;
	const swipeView = args.object;
	const leftItem = swipeView.getViewById<View>('left-add-view');
	const rightItem = swipeView.getViewById<View>('right-add-view');
	swipeLimits.left = leftItem.getMeasuredWidth();
	swipeLimits.right = rightItem.getMeasuredWidth();
	swipeLimits.threshold = leftItem.getMeasuredWidth() / 2;
}

export function onLeftSearchSwipeClick(args: ListViewEventData) {
	let guest: GuestSearch = args.object.bindingContext;
	if (guest.requestSentIcon == IconsCode.Check) {
		eventGuestsViewModel.add(guest);
	} else {
		eventGuestsViewModel.remove(guest);
	}
}

export function onRightSearchSwipeClick(args: ListViewEventData) {
	let guest: GuestSearch = args.object.bindingContext;
	if (guest.requestSentIcon == IconsCode.Check) {
		eventGuestsViewModel.add(guest);
	} else {
		eventGuestsViewModel.remove(guest);
	}
}

function searchGuestsEvent(): void {
	let searchGuestsTF: TextField = page.getViewById("search_guests");
	searchGuestsTF.on("textChange", () => {
		eventGuestsViewModel.search(searchGuestsTF.text);
	});
}

export function showSideDrawer(): void {
	if (drawer) {
		if (eventGuestsViewModel.get("menuIcon") == "res://back_arrow") {
			if (onMultipleSelection) {
				rlvGuests.deselectAll();
			} else if (onMultipleSelectionSearch) {
				rlvSearch.deselectAll();
			}
		} else {
			drawer.showDrawer();
		}
	}
}
