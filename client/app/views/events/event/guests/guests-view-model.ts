import { Observable, fromObject as observableFromObject } from 'tns-core-modules/data/observable';
import { ObservableArray } from "tns-core-modules/data/observable-array/observable-array";
import { IconsCode } from "~/utils/Icons"
import { MusicRoomApi } from '~/utils/MusicRoomApi';
import { EventAPI, UserAPI, AppRootViewModel } from '~/app-root-view-model';
import { formatDisplayName } from '~/utils/Tools';
import { EventRights } from '~/views/events/event/event-view-model';
import * as frameModule from "tns-core-modules/ui/frame"

export interface GuestSearch extends UserAPI {
	requestSentClass: string;
	requestSentIcon: string;
	selectedClass: string;
}

export interface GuestList extends UserAPI {
	leftActionClass: string;
	leftActionIcon: string;
	rightActionClass: string;
	rightActionIcon: string;
	selectedClass: string;
}

export class GuestsViewModel extends Observable {

	private searchGuestsList: ObservableArray<Observable> = new ObservableArray();
	private guestsList: ObservableArray<Observable> = new ObservableArray();
	
	private eventID: string;
	private rights: EventRights;

	private static readonly AddClass = "add-container";
	private static readonly RemoveClass = "delete-container";
	
	constructor(eventID: string) {
		super()
		this.eventID = eventID;
		this.set("searchGuests", this.searchGuestsList);
		this.set("guests", this.guestsList);

		if (eventID in AppRootViewModel.instance.events.owner) {
			this.rights = EventRights.Owner;
		} else if (eventID in AppRootViewModel.instance.events.dj) {
			this.rights = EventRights.DJ;
		} else if (eventID in AppRootViewModel.instance.events.guest) {
			this.rights = EventRights.Guest;
		} else {
			frameModule.topmost().navigate({
				moduleName: "views/events/events"
			});
			return ;
		}

		this.fillGuestsList();
		this.fillSearchWithFriends();
	}

	private async fillSearchWithFriends(): Promise<void> {
		this.searchGuestsList.length = 0;
		let friendships = await AppRootViewModel.instance.friendshipApiCaller.getFriendships(false);
		
		friendships.forEach(friendship => {

			if (!friendship.pending) {
				let friend = friendship.users[0].id != AppRootViewModel.instance.me.id ? friendship.users[0] : friendship.users[1];
				
				if (this.guestsList.some((guest: any) => guest.id == friend.id)) {
					return ;
				}

				let displayName = formatDisplayName(friend);
				let guestSearch: GuestSearch = {
					...friend,
					avatar: MusicRoomApi.baseUrl + ':' + MusicRoomApi.port + '/' + friend.avatar + '?' + new Date().toISOString(),
					displayName,
					selectedClass: "",
					requestSentClass: GuestsViewModel.AddClass,
					requestSentIcon: IconsCode.Check
				};
				this.searchGuestsList.push(observableFromObject(guestSearch));
			}
		});
	}

	public getObservableGuest(index: number): Observable {
		return this.guestsList.getItem(index);
	}

	public getObservableSearchGuest(index: number): Observable {
		return this.searchGuestsList.getItem(index);
	}

	public async search(keyword: string): Promise<void> {
		if (keyword.length == 0) {
			this.clearSearchList(true);
			return ;
		}
		if (keyword.length < 3) {
			this.clearSearchList(false);
			return ;
		}
		await MusicRoomApi.instance.get(MusicRoomApi.userSearch, response => {
			if (!response.hasOwnProperty("errorCode")) {
				this.fillSearchGuestsList(response.results);
			}
		}, error => {
		}, {
			keyword
		});
	}

	public refreshEvent(): void {
		MusicRoomApi.instance.get(MusicRoomApi.events + this.eventID, response => {
			AppRootViewModel.instance.events[this.rights][this.eventID] = response;
			this.fillGuestsList();
		}, error => {
		});
	}

	public fillGuestsList(): void {
		this.clearGuestsList();

		AppRootViewModel.instance.events[this.rights][this.eventID].guests.forEach(guest => {
			let guestElement: GuestList = {
				...guest,
				avatar: MusicRoomApi.baseUrl + ':' + MusicRoomApi.port + '/' + guest.avatar + '?' + new Date().toISOString(),
				leftActionClass: GuestsViewModel.RemoveClass,
				leftActionIcon: IconsCode.Remove,
				rightActionClass: GuestsViewModel.RemoveClass,
				rightActionIcon: IconsCode.Remove,
				selectedClass: ""
			};
			guestElement.displayName = formatDisplayName(guestElement);
			this.guestsList.push(observableFromObject(guestElement));
		});
	}


	private fillSearchGuestsList(guests: any[]): void {
		this.clearSearchList(false);

		guests.forEach(guest => {

			// If already in the list
			if (this.guestsList.some((o: any) => o.id == guest.id)) {
				return ;
			}

			let guestSearch: GuestSearch = {
				...guest,
				avatar: MusicRoomApi.baseUrl + ':' + MusicRoomApi.port + '/' + guest.avatar + '?' + new Date().toISOString(),
				selectedClass: "",
				requestSentClass: GuestsViewModel.AddClass,
				requestSentIcon: IconsCode.Check
			};
			guestSearch.displayName = formatDisplayName(guestSearch);
			this.searchGuestsList.push(observableFromObject(guestSearch));
		});
	}

	public add(Guest: GuestSearch): void {
		
		Guest.requestSentIcon = IconsCode.Remove;
		Guest.requestSentClass = GuestsViewModel.RemoveClass;
		
		let newGuest: UserAPI = {
			id: Guest.id,
			avatar: Guest.avatar,
			theme: null,
			apiKey: null,
			isPremium: Guest.isPremium,
			email: Guest.email,
			displayName: Guest.displayName,
			firstName: Guest.firstName,
			lastName: Guest.lastName,
			friendships: Guest.friendships,
			friends: Guest.friends,
			playlists: Guest.playlists,
			musicalPreferences: Guest.musicalPreferences,
			facebookid: ''
		}
		
		let newGuestList: GuestList = {
			...newGuest,
			leftActionClass: GuestsViewModel.RemoveClass,
			leftActionIcon: IconsCode.Remove,
			rightActionClass: GuestsViewModel.RemoveClass,
			rightActionIcon: IconsCode.Remove,
			selectedClass: ""
		}
		this.guestsList.push(observableFromObject(newGuestList));

		newGuest.avatar = 'avatars/' + newGuest.avatar.split('/avatars/').pop();

		AppRootViewModel.instance.events[this.rights][this.eventID].guests.push(newGuest);


		let index: number = -1;
		this.searchGuestsList.filter((guest, idx) => {
			if (guest['id'] == newGuest.id) {
				index = idx;
				return true;
			}
			return false;
		});
		if (index != -1) {
			this.searchGuestsList.splice(index, 1);
		}

		MusicRoomApi.instance.put(MusicRoomApi.events + this.eventID + '/' + MusicRoomApi.guests + Guest.id, {
		}, response => {
		}, error => {
		});
	}

	public addMultiple(guests: GuestSearch[]): void {
		let guestsIds: string[] = [];
		
		guests.forEach(Guest => {
			Guest.requestSentIcon = IconsCode.Remove;
			Guest.requestSentClass = GuestsViewModel.RemoveClass;
			
			let newGuest: UserAPI = {
				id: Guest.id,
				avatar: Guest.avatar,
				theme: null,
				apiKey: null,
				isPremium: Guest.isPremium,
				email: Guest.email,
				displayName: Guest.displayName,
				firstName: Guest.firstName,
				lastName: Guest.lastName,
				friendships: Guest.friendships,
				friends: Guest.friends,
				playlists: Guest.playlists,
				musicalPreferences: Guest.musicalPreferences,
				facebookid: ''
			}
			
			let newGuestList: GuestList = {
				...newGuest,
				leftActionIcon: IconsCode.Remove,
				rightActionClass: GuestsViewModel.RemoveClass,
				rightActionIcon: IconsCode.Remove,
				selectedClass: "",
				leftActionClass: GuestsViewModel.RemoveClass,
			}

			newGuest.avatar = 'avatars/' + newGuest.avatar.split('/avatars/').pop();
			this.guestsList.push(observableFromObject(newGuestList));
			
			for (let i: number = 0; i < this.searchGuestsList.length; i++) {
				if ((this.searchGuestsList.getItem(i) as any).id == Guest.id) {
					this.searchGuestsList.splice(i, 1);
					break;
				}
			}

			AppRootViewModel.instance.events[this.rights][this.eventID].guests.push(newGuest);
			guestsIds.push(Guest.id);
		});
		MusicRoomApi.instance.put(MusicRoomApi.events + this.eventID + '/' + MusicRoomApi.guests, {
			userids: guestsIds
		}, response => {
		}, error => {
		});
	}
	
	public remove(Guest: any): void {
		if (Guest.hasOwnProperty('requestSentIcon')) {
			let guestSearch: GuestSearch = Guest as GuestSearch;
			guestSearch.requestSentIcon = IconsCode.Check;
			guestSearch.requestSentClass = GuestsViewModel.AddClass;
			let index = -1;
			this.guestsList.filter((g, idx) => {
				if (g['id'] == Guest.id) {
					index = idx;
					return true;
				}
				return false;
			});
			if (index != -1) {
				this.guestsList.splice(index, 1);
			}
		} else {
			this.guestsList.splice(this.guestsList.indexOf(Guest), 1);
		}

		MusicRoomApi.instance.delete(MusicRoomApi.events + this.eventID + '/' + MusicRoomApi.guests + Guest.id, {}, response => {

			let index: number = -1;
			AppRootViewModel.instance.events[this.rights][this.eventID].guests.forEach((guest, idx) => {
				if (typeof guest == 'string' && guest == Guest.id) {
					index = idx;
				} else if (guest.id == Guest.id) {
					index = idx;
				}
			});
			if (index > -1) {
				AppRootViewModel.instance.events[this.rights][this.eventID].guests.splice(index, 1);
			}
		}, error => {
		});
	}

	public removeMultiple(guests: any[]): void {
		let guestsIds: string[] = [];
		
		guests.forEach(Guest => {

			if (Guest.hasOwnProperty('requestSentIcon')) {
				let guestSearch: GuestSearch = Guest as GuestSearch;
				guestSearch.requestSentIcon = IconsCode.Check;
				guestSearch.requestSentClass = GuestsViewModel.AddClass;
				let index = -1;
				this.guestsList.filter((g, idx) => {
					if (g['id'] == Guest.id) {
						index = idx;
						return true;
					}
					return false;
				});
				if (index != -1) {
					this.guestsList.splice(index, 1);
				}
			} else {
				this.guestsList.splice(this.guestsList.indexOf(Guest), 1);
			}
			guestsIds.push(Guest.id);
		});
		
		MusicRoomApi.instance.delete(MusicRoomApi.events + this.eventID + '/' + MusicRoomApi.guests, {
			userids: guestsIds
		}, response => {

			for (let i: number = 0; i < guestsIds.length; i++) {
				let index: number = -1;
				AppRootViewModel.instance.events[this.rights][this.eventID].guests.forEach((guest, idx) => {
					if (typeof guest == 'string') {
						index = guestsIds.indexOf(guest);
					} else {
						index = guestsIds.indexOf(guest.id);
					}
				});
				if (index > -1) {
					AppRootViewModel.instance.events[this.rights][this.eventID].guests.splice(index, 1);
				}
			}
		}, error => {
		});
	}

	public clearGuestsList(): void {
		this.guestsList.length = 0;
	}

	public clearSearchList(fillFriends: boolean): void {
		this.searchGuestsList.length = 0;
		if (fillFriends) {
			this.fillSearchWithFriends();
		}
	}
}