import { Observable, fromObject as observableFromObject } from "tns-core-modules/data/observable";
import { MusicRoomApi } from "~/utils/MusicRoomApi";
import { SocketIO } from 'nativescript-socketio';
import { IconsCode } from "~/utils/Icons";
import { EventAPI, AppRootViewModel, PlaylistAPI, TrackAPI, TrackDeezer, UserAPI } from "~/app-root-view-model";
import { ObservableArray } from "tns-core-modules/data/observable-array/observable-array";
import { DeezerConnect, TrackPlayer, SessionStore, DialogListener } from "nativescript-deezer";
import { toHHMMSS, addLike, removeLike, addDislike, removeDislike } from "~/utils/Tools";
import { DeezerApi } from "~/utils/DeezerApi";
import * as Toast from 'nativescript-toast';
import { ListViewEventData, RadListView } from "nativescript-ui-listview";
import { DialogOptions, CFAlertDialog } from "nativescript-cfalert-dialog";
import { StringValues } from "~/utils/Values";
import { device } from "tns-core-modules/platform/platform";
import { Locker } from "~/utils/Lock";
import { DeezerTrack } from "~/models/Track";
import * as frameModule from "tns-core-modules/ui/frame"

export interface EventTrack extends TrackAPI, TrackDeezer {
	selectedClass: string;
	formatedDuration: string;
	leftIcon: string;
	rightIcon: string;
	leftActionClass: string;
	rightActionClass: string;
}

export enum EventRights {
	Guest = 'guest',
	DJ = 'dj',
	Owner = 'owner'
}

export interface CurrentPlaylist extends PlaylistAPI {
	currentTracks: {[id: string]: TrackAPI}
}

export class EventViewModel extends Observable {

	private eventID: string = null;

	private playing: boolean;

	private socketIO: SocketIO = null;

	private deezerApi: DeezerApi = null;

	private tracksList: ObservableArray<Observable> = null;
	
	private deezerConnect: DeezerConnect = null;
	private trackPlayer: TrackPlayer = null;
	
	// private currentPlaylistID: string = null;
	private currentPlaylist: CurrentPlaylist = null;

	private currentTrack: EventTrack = null;
	private updateCurrentTime: boolean = true;

	private rights: EventRights;

	private firstClickPlay: boolean = true;

	private interval: any = null;
	private deezerApiCallInterval: any = null;
	private currentTimeMs: number = 0;

	private votingTime: boolean;
	private eventTime: boolean;

	public options: DialogOptions = null;
	public cfalertDialog: CFAlertDialog = null;

	private trackTimestamp: Date = null;
	private playlistIndex: number = -1;

	public shouldKeepSingleton: boolean = false;

	public static instance: EventViewModel = null;

	public isOwnerConnected: boolean = false;

	constructor(eventID: string) {
		
		super();
		this.set("loading", true);
		AppRootViewModel.instance.activePlaylist = null;
		this.playing = false;
		this.firstClickPlay = true;
		this.eventID = eventID;

		this.cfalertDialog = new CFAlertDialog();

		this.set("playlistIcon", IconsCode.ListBullet);
		this.set("playPauseIcon", IconsCode.Play);
		this.set("prevIcon", IconsCode.Prev);
		this.set("nextIcon", IconsCode.Next);
		this.set("settingsIcon", IconsCode.Gear);
		this.set("chevronIcon", IconsCode.ChevronUp);

		this.deezerApi = new DeezerApi();

		if (eventID in AppRootViewModel.instance.events.owner) {
			this.rights = EventRights.Owner;
		} else if (eventID in AppRootViewModel.instance.events.dj) {
			this.rights = EventRights.DJ;
		} else if (eventID in AppRootViewModel.instance.events.guest) {
			this.rights = EventRights.Guest;
		} else {
			frameModule.topmost().navigate({
				moduleName: "views/events/events"
			});
			return ;
		}

		this.set("visibilityGuest", this.rights == EventRights.Guest ? 'visible' : 'collapse');
		this.set("visibilityDJ", (this.rights == EventRights.DJ || this.rights == EventRights.Owner) ? 'visible' : 'collapse');
		this.set("visibilityOwner", this.rights == EventRights.Owner ? 'visible' : 'collapse');

		this.set("currentTime", "00:00");
		this.set("totalTime", "00:00");
	

		new Promise((resolve, reject) => {
			// Get the Event with detailed playlists etc.
			MusicRoomApi.instance.get(MusicRoomApi.events + eventID, response => {
				resolve(response);	
			}, error => {
				reject(error);
			});
		}).then((response: any) => {
			AppRootViewModel.instance.events[this.rights][this.eventID] = response;
			
			if (this.rights == EventRights.Owner) {
				this.deezerConnect = new DeezerConnect(StringValues.deezerAppID);
				let sessionStore: SessionStore = new SessionStore();
				if (sessionStore.restore(this.deezerConnect)) {
					this.trackPlayer = new TrackPlayer(this.deezerConnect);
				} else {
					Toast.makeText("You are not connected to Deezer. Please connect before starting the event.", "long").show();
				}
			}
			
			this.tracksList = new ObservableArray();
			this.set("tracks", this.tracksList);

			this.setupSocketIO();
	
			this.refreshEvent();

			AppRootViewModel.instance.events[this.rights][this.eventID].eventPlaylists.forEach((playlist, index) => {
				if (playlist.id == this.currentPlaylist.id) {
					this.playlistIndex = index;
					AppRootViewModel.instance.activePlaylist = playlist.id;
				}
			});
		});

		this.shouldKeepSingleton = false;

		if (!EventViewModel.instance) {
			EventViewModel.instance = this;
		}
	}

	public emptyListView(): void {
		this.tracksList.length = 0;
	}

	private fillCurrentPlaylistTracks(playlist: PlaylistAPI): void {
		this.currentPlaylist = {
			...playlist,
			currentTracks: {}
		};
		if (playlist.unplayedTracks) {
			playlist.playedTracks.forEach(unplayedTrack => {
				let track: TrackAPI = {
					...unplayedTrack,
					alreadyPlayed: true,
					activeTrack: playlist.activeTrack == unplayedTrack.id
				}
				this.currentPlaylist.currentTracks[unplayedTrack.id] = track;
			});
			playlist.unplayedTracks.forEach(unplayedTrack => {
				let track: TrackAPI = {
					...unplayedTrack,
					alreadyPlayed: false,
					activeTrack: playlist.activeTrack == unplayedTrack.id
				}
				this.currentPlaylist.currentTracks[unplayedTrack.id] = track;
			});
		} else {
			((playlist.tracks) as TrackAPI[]).forEach(unplayedTrack => {
				let track: TrackAPI = {
					...unplayedTrack
				}
				this.currentPlaylist.currentTracks[unplayedTrack.id] = track;
			});
		}
	}

	private async buildPlaylist(): Promise<void> {
		Locker.acquire(this.eventID, async () => {

			
			this.set("loading", true);
			this.tracksList.splice(0, this.tracksList.length);
			if (this.currentPlaylist) {
				this.set("isReorder", this.currentPlaylist.sortingType == "move");
				let shouldCallLater: number[] = [];

				await Promise.all(Object.keys(this.currentPlaylist.currentTracks).map((trackID, idx) => {
					let element = this.currentPlaylist.currentTracks[trackID];

					return new Promise(async (resolve, reject) => {

						let deezerTrack: DeezerTrack = await AppRootViewModel.instance.trackApiCaller.getTrack(element.serviceid);
						let track: EventTrack = {
							id: element.id,
							serviceid: element.serviceid,
							name: '',
							service: 'deezer',
							album: '',
							artist: '',
							coverImageURL:  '',
							duration: 0,
							formatedDuration: '',
							leftIcon: '',
							rightIcon: '',
							likes: element.likes,
							dislikes: element.dislikes,
							alreadyPlayed: element.alreadyPlayed,
							activeTrack: element.activeTrack,
							selectedClass: '',
							rightActionClass: '',
							leftActionClass: 'icon'
						}

						if (deezerTrack) {
							track.name = deezerTrack.title;
							track.album = deezerTrack.albumTitle;
							track.artist = deezerTrack.artistName;
							track.coverImageURL = deezerTrack.albumCover_medium;
							track.duration = deezerTrack.duration;
							track.formatedDuration = toHHMMSS(deezerTrack.duration.toString());
						} else {
							track.name = 'Downloading...';
							track.album = 'Waiting for Deezer Response...';
							shouldCallLater.push(element.serviceid);
						}


						if (track.alreadyPlayed) {
							track.selectedClass = 'list-group-item-disabled';
						}

						if (this.currentPlaylist.sortingType == 'move') {
							track.leftIcon = IconsCode.ArrowUp + IconsCode.ArrowDown;
							if (!element.hasOwnProperty('likes')) {
								track.selectedClass = 'list-group-item-disabled';
							}
						} else {
							
							if (element.hasOwnProperty('dislikes')) {
								if (element.dislikes.includes(AppRootViewModel.instance.me.id)) {
									track.leftIcon = IconsCode.DislikeFull + ' ' + element.dislikes.length;
									track.leftActionClass += ' disliked';
								} else {
									track.leftIcon = IconsCode.Dislike + ' ' + element.dislikes.length;
								}
							} else {
								track.leftIcon = IconsCode.Dislike;
								track.selectedClass = 'list-group-item-disabled';
							}
							
							track.rightActionClass = 'icon right-action-icon';
							if (element.hasOwnProperty('likes')) {
								if (element.likes.includes(AppRootViewModel.instance.me.id)) {
									track.rightIcon = IconsCode.LikeFull + ' ' + element.likes.length;
									track.rightActionClass += ' liked';
								} else {
									track.rightIcon = IconsCode.Like + ' ' + element.likes.length;
								}
							} else {
								track.rightIcon = IconsCode.Like;
								track.selectedClass = 'list-group-item-disabled';
							}
						}
						resolve(track);
					
					}).catch(e => {
					});
				})).catch(e => {
				}).then(async (trackList: EventTrack[]) => {
					this.set("loading", false);
					trackList.forEach((track, idx) => {
						this.tracksList.push(observableFromObject(track));

						if (track.activeTrack) {
							if (!this.currentTrack || this.currentTrack.id == track.id) {
								this.setCurrentTrack(idx, false, true, true);
								let listView: RadListView = <RadListView>(frameModule.topmost().currentPage.getViewById("tracksListView"));
								if (listView) {
									listView.scrollToIndex(this.tracksList.length - 1);
								}
							}
						}
					});

					if (shouldCallLater.length > 0) {
						this.deezerApiCallInterval = setInterval(() => {
							if (!shouldCallLater.length) {
								clearInterval(this.deezerApiCallInterval);
								return ;
							}

							shouldCallLater.forEach(serviceID => {
								new Promise(async (resolve, reject) => {
									let deezerTrack: DeezerTrack = await AppRootViewModel.instance.trackApiCaller.getTrack(serviceID);
									if (!deezerTrack) {
										reject();
									} else {
										Locker.acquire('receiveDeezerData', () => {
											shouldCallLater.splice(shouldCallLater.indexOf(serviceID), 1);
											for (let i = 0; i < this.tracksList.length; i++) {
												let track: EventTrack = this.tracksList.getItem(i) as any;
												if (track.serviceid == serviceID) {
													track.name = deezerTrack.title;
													track.album = deezerTrack.albumTitle;
													track.artist = deezerTrack.artistName;
													track.coverImageURL = deezerTrack.albumCover_medium;
													track.duration = deezerTrack.duration;
													track.formatedDuration = toHHMMSS(deezerTrack.duration.toString());

													if (this.currentTrack && this.currentTrack.serviceid == serviceID) {
														this.currentTrack = track;
														this.set("coverImageURL", this.currentTrack.coverImageURL);
														this.set("currentTrackName", this.currentTrack.name);
														this.set("currentTrackAlbumName", this.currentTrack.album);
														this.set("totalTime", toHHMMSS(this.currentTrack.duration.toString()));	
													}
													break;
												}
											}
											resolve();	
											Locker.release('receiveDeezerData');
										});
									}
								});
							});
						}, 2000);
					}
				});
			} else {
				this.set("loading", false);
			}
			Locker.release(this.eventID);
		});
	}

	public like(track: EventTrack): void {
		MusicRoomApi.instance.put(MusicRoomApi.events + this.eventID + '/' + track.id + '/like', {}, response => {
		}, error => {
		});
	}

	public unlike(track: EventTrack): void {
		MusicRoomApi.instance.delete(MusicRoomApi.events + this.eventID + '/' + track.id + '/like', {}, response => {
		}, error => {
		});
	}

	public dislike(track: EventTrack): void {
		MusicRoomApi.instance.put(MusicRoomApi.events + this.eventID + '/' + track.id + '/dislike', {}, response => {
		}, error => {
		});
	}

	public undislike(track: EventTrack): void {
		MusicRoomApi.instance.delete(MusicRoomApi.events + this.eventID + '/' + track.id + '/dislike', {}, response => {
		}, error => {
		});
	}
	
	public getRights(): EventRights {
		return this.rights;
	}

	public getEvent(): EventAPI {
		return AppRootViewModel.instance.events[this.rights][this.eventID];
	}

	public isPlaying(): boolean {
		return this.playing;
	}

	
	public async refreshEvent(): Promise<void> {
		if (AppRootViewModel.instance.activePlaylist) {
			AppRootViewModel.instance.events[this.rights][this.eventID].eventPlaylists.forEach(playlist => {
				if (playlist.id == AppRootViewModel.instance.activePlaylist) {
					this.fillCurrentPlaylistTracks(playlist);
				}
			});
		} else {
			if (this.currentPlaylist) {
				this.currentPlaylist.currentTracks = {};
			}
			AppRootViewModel.instance.events[this.rights][this.eventID].eventPlaylists.forEach(playlist => {
				if (playlist.id == AppRootViewModel.instance.activePlaylist) {
					AppRootViewModel.instance.activePlaylist = playlist.id;
					this.fillCurrentPlaylistTracks(playlist);	
				}
			});
		}
		if (!AppRootViewModel.instance.activePlaylist && ((AppRootViewModel.instance.events[this.rights][this.eventID].playlists && AppRootViewModel.instance.events[this.rights][this.eventID].playlists.length > 0) || (AppRootViewModel.instance.events[this.rights][this.eventID].eventPlaylists && AppRootViewModel.instance.events[this.rights][this.eventID].eventPlaylists.length > 0))) {
			let playlist = (AppRootViewModel.instance.events[this.rights][this.eventID].votingActive || AppRootViewModel.instance.events[this.rights][this.eventID].eventActive) ? AppRootViewModel.instance.events[this.rights][this.eventID].eventPlaylists[0] : AppRootViewModel.instance.events[this.rights][this.eventID].playlists[0];
			AppRootViewModel.instance.activePlaylist = playlist.id;
			this.fillCurrentPlaylistTracks(playlist);
		}
		await this.buildPlaylist();
	}
	
	public startVoting(): void {

		Locker.acquire(this.eventID, async () => {
			await MusicRoomApi.instance.put(MusicRoomApi.events + this.eventID + '/' + MusicRoomApi.startVoting, {
			}, response => {
				this.options.buttons[0].text = "Stop Voting";
				let shouldRefreshEvent: boolean = !AppRootViewModel.instance.events[this.rights][this.eventID].votingActive && !AppRootViewModel.instance.events[this.rights][this.eventID].eventActive;
				AppRootViewModel.instance.events[this.rights][this.eventID] = response;
				if (shouldRefreshEvent) {
					if (!AppRootViewModel.instance.events[this.rights][this.eventID].activePlaylist) {
						AppRootViewModel.instance.activePlaylist = AppRootViewModel.instance.events[this.rights][this.eventID].eventPlaylists.length ? AppRootViewModel.instance.events[this.rights][this.eventID].eventPlaylists[0].id : '';
					}
					this.refreshEvent();
				}
			}, error => {
			});
			Locker.release(this.eventID);
		});
	}

	public stopVoting(): void {
		Locker.acquire(this.eventID, async () => {
		await MusicRoomApi.instance.put(MusicRoomApi.events + this.eventID + '/' + MusicRoomApi.endVoting, {
			}, response => {
				this.options.buttons[0].text = "Start Voting";
				AppRootViewModel.instance.events[this.rights][this.eventID] = response;
				if (!AppRootViewModel.instance.events[this.rights][this.eventID].votingActive && !AppRootViewModel.instance.events[this.rights][this.eventID].eventActive) {
					AppRootViewModel.instance.activePlaylist = null;
					this.refreshEvent();
				}
			}, error => {
			});
			Locker.release(this.eventID);
		});
	}

	public startEvent(): void {
		Locker.acquire(this.eventID, async () => {
			await MusicRoomApi.instance.put(MusicRoomApi.events + this.eventID + '/' + MusicRoomApi.startEvent, {
			}, response => {
				this.options.buttons[1].text = "Stop Event";
				let shouldRefreshEvent: boolean = !AppRootViewModel.instance.events[this.rights][this.eventID].votingActive && !AppRootViewModel.instance.events[this.rights][this.eventID].eventActive;
				AppRootViewModel.instance.events[this.rights][this.eventID] = response;
				if (shouldRefreshEvent) {
					if (!AppRootViewModel.instance.events[this.rights][this.eventID].activePlaylist) {
						AppRootViewModel.instance.activePlaylist = AppRootViewModel.instance.events[this.rights][this.eventID].eventPlaylists.length ? AppRootViewModel.instance.events[this.rights][this.eventID].eventPlaylists[0].id : '';
					}
					this.refreshEvent();
				}
			}, error => {
			});
			Locker.release(this.eventID);
		});
	}
	
	public stopEvent(): void {
		Locker.acquire(this.eventID, async () => {
			await MusicRoomApi.instance.put(MusicRoomApi.events + this.eventID + '/' + MusicRoomApi.endEvent, {
			}, response => {
				AppRootViewModel.instance.events[this.rights][this.eventID] = response;
				this.options.buttons[1].text = "Start Event";
				if (!AppRootViewModel.instance.events[this.rights][this.eventID].votingActive && !AppRootViewModel.instance.events[this.rights][this.eventID].eventActive) {
					AppRootViewModel.instance.activePlaylist = null;
					this.refreshEvent();
				}

				this.pause();
				this.currentTrack = null;
				if (this.interval) {
					clearInterval(this.interval);
					this.interval = null;
				}
				this.set("timelineValue", 0);
				this.set('currentTime', '00:00');
				this.set('totalTime', '00:00');	

			}, error => {
			});
			Locker.release(this.eventID);
		});
	}

	public setCurrentTrack(trackIndex: number, playTrack?: boolean, leaveInterval?: boolean, ignoreOwner?: boolean): void {

		Locker.acquire(this.eventID, () => {
			if (!AppRootViewModel.instance.events[this.rights][this.eventID].eventActive) {
				Toast.makeText('Start the event first', 'short').show();
				Locker.release(this.eventID);
				return;
			}
			
			if (this.rights == EventRights.Owner && !this.trackPlayer && playTrack) {
				Toast.makeText('Please link your deezer account first.', 'short').show();
				Locker.release(this.eventID);
				return;
			}
			
			if (this.rights != EventRights.Owner && !this.isOwnerConnected && !ignoreOwner) {
				Toast.makeText('The owner is not connected', 'short').show();
				Locker.release(this.eventID);
				return;
			}
			
			let nextTrack: EventTrack = this.tracksList.getItem(trackIndex) as any
			
			if (!nextTrack) {
				Locker.release(this.eventID);
				return;
			}
			
			if (nextTrack == this.currentTrack && this.playing) {
				Locker.release(this.eventID);
				return ;
			}

			if (!leaveInterval && this.interval) {
				clearInterval(this.interval);
				this.interval = null;
			}

			
			if (this.currentTrack) {
				this.currentTrack.selectedClass = 'list-group-item-disabled';
			}
			let newIndex: number = -1;
	
			for (var currentIndex: number = 0; currentIndex < this.tracksList.length; currentIndex++) {
				let trackLoop: EventTrack = this.tracksList.getItem(currentIndex) as any;
				if (trackLoop.selectedClass == 'list-group-item-disabled') {
					newIndex = currentIndex;
				}
				if (trackLoop.id == nextTrack.id) {
					break;
				}
			}

			newIndex++;
			if (newIndex < this.tracksList.length && nextTrack.selectedClass != 'list-group-item-disabled') {
				let toMove: any = this.tracksList.splice(currentIndex, 1)[0];
				this.tracksList.splice(newIndex, 0, toMove);
				let listView: RadListView = <RadListView>(frameModule.topmost().currentPage.getViewById("tracksListView"));
				if (listView) {
					listView.scrollToIndex(newIndex, true);
				}

			}
				
			this.currentTrack = nextTrack;

			this.set("coverImageURL", this.currentTrack.coverImageURL);
			this.set("currentTrackName", this.currentTrack.name);
			this.set("currentTrackAlbumName", this.currentTrack.album);
			this.set("totalTime", toHHMMSS(this.currentTrack.duration.toString()));
			this.currentTrack.selectedClass = 'list-group-item-selected';

			if (this.rights == EventRights.Owner && this.trackPlayer && playTrack) {
				this.trackPlayer.setTrackID(this.currentTrack.serviceid);
				this.firstClickPlay = true;
				this.play();
			} else if (this.rights == EventRights.DJ && playTrack) {
				this.play();
			}
			Locker.release(this.eventID);
		});
	}

	public onItemReorderStart(args: ListViewEventData): void {
		let track: EventTrack = this.tracksList.getItem(args.index) as any;
		if (track.alreadyPlayed || track.selectedClass == 'list-group-item-selected' || track.selectedClass == 'list-group-item-disabled' || !AppRootViewModel.instance.events[this.rights][this.eventID].votingActive) {
			args.returnValue = false;
		}
	}

	public onItemReordered(args: ListViewEventData): void {
		let track: EventTrack = this.tracksList.getItem(args.data.targetIndex) as any;

		let playlistIndex: number;
		let trackIndex: number;
		[playlistIndex, trackIndex] = this.getIndexByTrackID(track.id);

		trackIndex = (args.data.targetIndex - AppRootViewModel.instance.events[this.rights][this.eventID].eventPlaylists[playlistIndex].playedTracks.length);
	
		if (trackIndex > -1) {
			MusicRoomApi.instance.put(MusicRoomApi.events + this.eventID + '/' + track.id + '/' + MusicRoomApi.move, {
				index: trackIndex
			}, response => {
			}, error => {
			});
		} else {
			Toast.makeText('Cannot place track before current track').show();
			let toMove: any = this.tracksList.splice(args.data.targetIndex, 1)[0];
			this.tracksList.splice(args.index, 0, toMove);
		}
	}

	public moveCursor(position: number) {

		if (this.rights == EventRights.Owner && !this.trackPlayer) {
			Toast.makeText("Please link your deezer account first.").show();
			return;
		}
		
		this.updateCurrentTime = false;
		this.set("currentTime", toHHMMSS(((position / 1000) * this.currentTrack.duration).toString()));
	}
	
	public stopMoveCursor(position: number, trackPosition?: number) {
		
		let data: any = {};
		if (this.rights == EventRights.Owner) {
			if (!this.trackPlayer) {
				Toast.makeText("Please link your deezer account first.").show();
				return;
			}
			data.timestamp = new Date().toISOString();
		}
		if (trackPosition) {
			this.currentTimeMs = trackPosition;
		} else {
			this.currentTimeMs = Math.floor((position / 1000) * this.currentTrack.duration * 1000);
		}
		this.trackTimestamp = new Date(new Date().getTime() - this.currentTimeMs);
		
		data.trackPosition = this.currentTimeMs;

		if (this.rights == EventRights.Owner) {
			this.trackPlayer.seek(Math.floor(this.currentTimeMs));
		}

		MusicRoomApi.instance.put(MusicRoomApi.events + this.eventID + '/player/' + MusicRoomApi.seek, data, response => {
		}, error => {
		});

		this.updateCurrentTime = true;
	}

	public seek(): void {

	}

	public async pause(): Promise<void> {
		let data: any = {};
		if (this.rights == EventRights.Owner) {
			if (!this.trackPlayer) {
				Toast.makeText("Please link your deezer account first.").show();
				return;
			}
			this.trackPlayer.pause();
			data.timestamp = new Date().toISOString();
			data.trackPosition = this.currentTimeMs;
		}
		
		this.playing = false;
		this.set("playPauseIcon", IconsCode.Play);
		
		clearInterval(this.interval);
		this.interval = null;
		await MusicRoomApi.instance.put(MusicRoomApi.events + this.eventID + '/player/' + MusicRoomApi.pause, data, response => {
			if (this.rights == EventRights.Owner) {
				if (response.message == 'no change') {
					return;
				}
				AppRootViewModel.instance.events[this.rights][this.eventID] = response;
			}
		}, error => {
		});
	}

	public async play(): Promise<void> {
		if (this.rights == EventRights.Owner) {
			if (!this.trackPlayer) {
				Toast.makeText("Please link your deezer account first.").show();
				return;
			}
	
			if (!this.currentTrack) {
				return;
			}
	
			if (this.firstClickPlay) {
				this.firstClickPlay = false;
				this.trackPlayer.setTrackID(this.currentTrack.serviceid);
				this.currentTimeMs = 0;
				this.trackTimestamp = new Date();
			} else {
				this.trackTimestamp = new Date(new Date().getTime() - this.currentTimeMs);
			}
			
			this.setTrackInterval();
			this.set("playPauseIcon", IconsCode.Pause);
			this.playing = true;
	
			this.trackPlayer.play();
		}

		let data: any = {};

		if (this.rights == EventRights.Owner) {
			data.timestamp = new Date().toISOString();
		}
		
		data.trackPosition = this.currentTimeMs;
		data.trackid = this.currentTrack.id,

		await MusicRoomApi.instance.put(MusicRoomApi.events + this.eventID + '/player/' + MusicRoomApi.play, data, response => {
			if (this.rights == EventRights.Owner) {
				if (response.message == 'no change') {
					return;
				}
				AppRootViewModel.instance.events[this.rights][this.eventID] = response;
			}
		}, error => {
		});
	}

	public next(): void {
		if (this.rights == EventRights.Owner && !this.trackPlayer) {
			Toast.makeText("Please link your deezer account first.").show();
			return;
		}

		let index: number = -1;
		if (this.currentTrack) {
			let nextTrack: EventTrack;
			[index, nextTrack] = this.findNextTrack(this.currentTrack);
			this.firstClickPlay = true;
		} else {
			this.tracksList.forEach((track: any, idx) => {
				if (track.selectedClass != 'list-group-item-disabled' && index == -1) {
					index = idx;
				}
			});
		}
		if (index > -1) {
			this.setCurrentTrack(index, true);
		}
	}

	public prev(): void {
		if (this.rights == EventRights.Owner && !this.trackPlayer) {
			Toast.makeText("Please link your deezer account first.").show();
			return;
		}

		let index: number = -1;

		for (let i: number = 0; i < this.tracksList.length; i++) {
			let track: EventTrack = <EventTrack><any>this.tracksList.getItem(i);
			if (track.selectedClass == 'list-group-item-disabled') {
				index = i;
			} else if (track.selectedClass == 'list-group-item-selected') {
				break ;
			}
		}

		if (index > -1) {
			this.firstClickPlay = true;
			this.setCurrentTrack(index, true);
		}
	}

	public setTrackInterval(): void {
		if (this.interval) {
			clearInterval(this.interval);
		}
		this.interval = setInterval(() => {
			if (!this.updateCurrentTime || !this.currentTrack) {
				return ;
			}
			this.set("timelineValue", ((this.currentTimeMs / (this.currentTrack.duration * 1000)) * 1000.0));
			this.set("currentTime", toHHMMSS((this.currentTimeMs / 1000).toString()));

			if (this.currentTimeMs >= this.currentTrack.duration * 1000) {
				this.firstClickPlay = true;
				this.playing = false;
				clearInterval(this.interval);
				this.interval = null;
				if (this.rights == EventRights.Owner) {
					let index: number;
					let nextTrack: EventTrack;
					[index, nextTrack] = this.findNextTrack(this.currentTrack);
					if (index > -1) {
						this.setCurrentTrack(index);
						this.play();
					} else {
						this.pause();
					}
				}
			}
			this.currentTimeMs = (new Date().getTime() - this.trackTimestamp.getTime());
		}, 250);
	}

	private findNextTrack(prevTrack: EventTrack): [number, EventTrack] {
		let isNext: boolean = false;
		let index: number = -1;
		let nextTrack: EventTrack;
		this.tracksList.forEach((track: any, idx) => {
			if (isNext) {
				index = idx;
				nextTrack = track;
				isNext = false;
				return ;
			}
			if (track.id == prevTrack.id) {
				isNext = true;
			}
		});
		return [index, nextTrack];
	}

	public delete(): void {
		delete AppRootViewModel.instance.events.owner[this.eventID];
		MusicRoomApi.instance.delete(MusicRoomApi.events + this.eventID, {}, reponse => {
		}, error => {
		});
	}

	public destroy(): void {
		this.destroySocketIO();
		if (this.trackPlayer) {
			this.trackPlayer.stop();
			this.trackPlayer = null;
		}
		if (this.interval) {
			clearInterval(this.interval);
			this.interval = null;
		}
		if (this.deezerApiCallInterval) {
			clearInterval(this.deezerApiCallInterval);
			this.deezerApiCallInterval = null;
		}
		EventViewModel.instance = null;
	}

	private destroySocketIO(): void {
		if (this.socketIO) {
			this.socketIO.disconnect();
		}
	}

	private setupSocketIO(): void {
		this.socketIO = new SocketIO(MusicRoomApi.baseUrl + ':' + MusicRoomApi.sockerIOPort + '/events', {
			query: {
				apiKey: AppRootViewModel.instance.me.apiKey,
				eventid: this.eventID
			},
			
		});

		this.socketIO.on('CONNECTED', data => {
			console.log('CONNECTED');
		});

		this.socketIO.on('EVENT_STARTER_PACK', async data => {
			console.log('EVENT_STARTER_PACK');

			this.isOwnerConnected = data.ownerIsConnected;

			if (data.eventActive && data.activeTrack) {

				let playlist: PlaylistAPI =  null;
				let index: number = -1;

				this.getIndexByTrackID

				let done: boolean = false;
				for (let playlistIndex: number = 0; playlistIndex < AppRootViewModel.instance.events[this.rights][this.eventID].eventPlaylists.length; playlistIndex++) {

					let currentPlaylist: PlaylistAPI = AppRootViewModel.instance.events[this.rights][this.eventID].eventPlaylists[playlistIndex];
		
					for (let trackIndex: number = 0; trackIndex < currentPlaylist.playedTracks.length; trackIndex++) {
						if (currentPlaylist.playedTracks[trackIndex].id == data.activeTrack) {
							playlist = currentPlaylist;
							index = trackIndex;
							done = true;
							break;
						}
					}
					if (done) {
						break;
					}

					for (let trackIndex: number = 0; trackIndex < currentPlaylist.unplayedTracks.length; trackIndex++) {
						if (currentPlaylist.unplayedTracks[trackIndex].id == data.activeTrack) {
							playlist = currentPlaylist;
							index = currentPlaylist.playedTracks.length + trackIndex;
							done = true;
						}
					}
					if (done) {
						break;
					}
					
				}
				if (index == -1 || !playlist) {
					return ;
				}
				// if not the same
				if (AppRootViewModel.instance.activePlaylist != playlist.id) {
					
					this.fillCurrentPlaylistTracks(playlist);
					await this.buildPlaylist();
					await new Promise(resolve => setTimeout(resolve, 200));
					let keys = Object.keys(this.currentPlaylist.currentTracks);

					for (let i: number = 0; i < keys.length; i++) {
						if (keys[i] == data.activeTrack) {
							this.setCurrentTrack(i);
							break;
						}
					}
					AppRootViewModel.instance.activePlaylist = data.playlistid;
				} else {
					this.tracksList.forEach((track: any, index) => {
						if (track.id == data.trackid) {
							this.setCurrentTrack(index);
						}
					});
				}
				if (data.hasOwnProperty('isPlaying')) {

					if (data.isPlaying) {
						this.trackTimestamp = new Date(new Date(data.lastTrackActionTimestamp).getTime() - data.trackPosition);//new Date(new Date(data.lastTrackActionTimestamp).getTime() - (new Date(data.lastTrackActionTimestamp).getTime() - new Date().getTime()) - data.trackPosition);
						this.currentTimeMs = data.trackPosition + Date.now() - new Date(data.lastTrackActionTimestamp).getTime();
					} else {
						this.trackTimestamp = new Date(new Date(data.lastTrackActionTimestamp).getTime() - (new Date(data.lastTrackActionTimestamp).getTime() - new Date().getTime()) - data.trackPosition);
						this.currentTimeMs = data.trackPosition;
					}

					this.set("timelineValue", ((this.currentTimeMs / (this.currentTrack.duration * 1000)) * 1000.0));
					this.set("currentTime", toHHMMSS((this.currentTimeMs / 1000).toString()));
					if (data.isPlaying) {
						this.setTrackInterval();
						this.playing = true;
						this.set('playPauseIcon', IconsCode.Pause);
					}

				}
			}
		});

		this.socketIO.on('disconnect', data => {
			console.log('disconnect');
		})
		
		this.socketIO.on('ERROR', data => {
			console.log('ERROR');
		});
		
		
		this.socketIO.on('EVENT_ADD_DJS', data => {
			console.log('EVENT_ADD_DJS');

			if (data.by == AppRootViewModel.instance.me.id) {
				return;
			}

			Promise.all<UserAPI>(data.userids.map(userid => {
				return new Promise<UserAPI>((resolve, reject) => {
					MusicRoomApi.instance.get(MusicRoomApi.users + userid, response => {
						resolve(response);
					}, error => {
						reject(error);
					});
				});
			})).then((users: UserAPI[]) => {
				users.forEach(user => {
					AppRootViewModel.instance.events[this.rights][this.eventID].guests.push(user);
				});
			});

			if (data.userids.includes(AppRootViewModel.instance.me.id)) {
				AppRootViewModel.instance.events.dj[this.eventID] = AppRootViewModel.instance.events[this.rights][this.eventID];
				this.rights = EventRights.DJ;
				this.set("visibilityGuest", 'collapse');
				this.set("visibilityDJ", 'visible');
				this.set('playPauseIcon', (this.playing ? IconsCode.Pause : IconsCode.Play));
			}
		});

		this.socketIO.on('EVENT_REMOVE_DJS', data => {
			console.log('EVENT_REMOVE_DJS');

			for (let j: number = 0; j < data.userids.length; j++) {
				for (let i: number = 0; i < AppRootViewModel.instance.events[this.rights][this.eventID].djs.length; i++) {
					if (AppRootViewModel.instance.events[this.rights][this.eventID].djs[i].id == data.userids[j]) {
						AppRootViewModel.instance.events[this.rights][this.eventID].djs.splice(i, 1);
						break ;
					}
				}
			}

			if (data.userids.includes(AppRootViewModel.instance.me.id)) {

				let event: EventAPI = AppRootViewModel.instance.events.dj[this.eventID];

				delete AppRootViewModel.instance.events.dj[this.eventID];

				let canStay: boolean = false;
				for (let i: number = 0; i < event.guests.length; i++) {
					if (event.guests[i].id == AppRootViewModel.instance.me.id) {
						canStay = true;
					}
				}
				if (canStay || event.visibility == 'PUBLIC') {
					AppRootViewModel.instance.events.guest[this.eventID] = event;
					this.rights = EventRights.Guest;
					this.set("visibilityGuest", 'visible');
					this.set("visibilityDJ", 'collapse');
				} else {
					frameModule.topmost().navigate({
						moduleName: "views/events/events"
					});
					return;
				}
			}
		});

		this.socketIO.on('EVENT_ADD_GUESTS', data => {
			console.log('EVENT_ADD_GUESTS');
			
			if (data.by == AppRootViewModel.instance.me.id) {
				return;
			}

			Promise.all<UserAPI>(data.userids.map(userid => {
				return new Promise<UserAPI>((resolve, reject) => {
					MusicRoomApi.instance.get(MusicRoomApi.users + userid, response => {
						resolve(response);
					}, error => {
						reject(error);
					});
				});
			})).then((users: UserAPI[]) => {
				users.forEach(user => {
					AppRootViewModel.instance.events[this.rights][this.eventID].guests.push(user);
				});
			});
		});

		this.socketIO.on('EVENT_REMOVE_GUESTS', data => {
			console.log('EVENT_REMOVE_GUESTS');
			for (let j: number = 0; j < data.userids.length; j++) {
				for (let i: number = 0; i < AppRootViewModel.instance.events[this.rights][this.eventID].guests.length; i++) {
					if (AppRootViewModel.instance.events[this.rights][this.eventID].guests[i].id == data.userids[j]) {
						AppRootViewModel.instance.events[this.rights][this.eventID].guests.splice(i, 1);

						if (data.userids[j] == AppRootViewModel.instance.me.id) {
							let index: number = -1;
							for (let k: number = 0; k < AppRootViewModel.instance.events[this.rights][this.eventID].djs.length; k++) {
								if (AppRootViewModel.instance.events[this.rights][this.eventID].djs[k].id == AppRootViewModel.instance.me.id) {
									index = k;
								}
							}
							if (index == -1 && AppRootViewModel.instance.events[this.rights][this.eventID].visibility != 'PUBLIC') {
								delete AppRootViewModel.instance.events.guest[this.eventID];
								frameModule.topmost().navigate({
									moduleName: "views/events/events"
								});
								return;
							}
				
						}
						break ;
					}
				}
			}
		});

		this.socketIO.on('EVENT_VOTING_STARTED', data => {
			Locker.acquire(this.eventID, () => {
				console.log('EVENT_VOTING_STARTED');
				if (data.by != AppRootViewModel.instance.me.id) {
	
					AppRootViewModel.instance.events[this.rights][this.eventID].eventPlaylists = data.eventPlaylists;
					let shouldRefreshEvent: boolean = !AppRootViewModel.instance.events[this.rights][this.eventID].votingActive && !AppRootViewModel.instance.events[this.rights][this.eventID].eventActive;
					AppRootViewModel.instance.events[this.rights][this.eventID].votingActive = true;
					if (shouldRefreshEvent) {
						AppRootViewModel.instance.activePlaylist = null;
						this.refreshEvent();
					}
				}
				Locker.release(this.eventID);
			});
		});
		
		this.socketIO.on('EVENT_VOTING_ENDED', data => {
			Locker.acquire(this.eventID, () => {
				if (data.by != AppRootViewModel.instance.me.id) {

					console.log('EVENT_VOTING_ENDED');

					if (data.eventPlaylists) {
						AppRootViewModel.instance.events[this.rights][this.eventID].eventPlaylists = data.eventPlaylists;
						AppRootViewModel.instance.events[this.rights][this.eventID].playlists = [];
					} else if (data.playlists) {
						AppRootViewModel.instance.events[this.rights][this.eventID].playlists = data.playlists;
						AppRootViewModel.instance.events[this.rights][this.eventID].eventPlaylists = [];
					}
					AppRootViewModel.instance.events[this.rights][this.eventID].votingActive = false;

					if (!AppRootViewModel.instance.events[this.rights][this.eventID].votingActive && !AppRootViewModel.instance.events[this.rights][this.eventID].eventActive) {
						AppRootViewModel.instance.activePlaylist = null;
						this.refreshEvent();
					}
				}
				Locker.release(this.eventID);
			});
		});
		
		this.socketIO.on('EVENT_EVENT_STARTED', data => {

			Locker.acquire(this.eventID, () => {
				console.log('EVENT_EVENT_STARTED');

				if (data.by != AppRootViewModel.instance.me.id) {
					AppRootViewModel.instance.events[this.rights][this.eventID].eventPlaylists = data.eventPlaylists;
					let shouldRefreshEvent: boolean = !AppRootViewModel.instance.events[this.rights][this.eventID].votingActive && !AppRootViewModel.instance.events[this.rights][this.eventID].eventActive;
					AppRootViewModel.instance.events[this.rights][this.eventID].eventActive = true;
					if (shouldRefreshEvent) {
						AppRootViewModel.instance.activePlaylist = null;
						this.refreshEvent();
					}
				}
				Locker.release(this.eventID);
			});
		});
		
		this.socketIO.on('EVENT_EVENT_ENDED', data => {
			Locker.acquire(this.eventID, () => {
				console.log('EVENT_EVENT_ENDED');
				if (data.by != AppRootViewModel.instance.me.id) {
					if (data.eventPlaylists) {
						AppRootViewModel.instance.events[this.rights][this.eventID].eventPlaylists = data.eventPlaylists;
						AppRootViewModel.instance.events[this.rights][this.eventID].playlists = [];
					} else if (data.playlists) {
						AppRootViewModel.instance.events[this.rights][this.eventID].playlists = data.playlists;
						AppRootViewModel.instance.events[this.rights][this.eventID].eventPlaylists = [];
					}
					AppRootViewModel.instance.events[this.rights][this.eventID].eventActive = false;

					if (!AppRootViewModel.instance.events[this.rights][this.eventID].votingActive && !AppRootViewModel.instance.events[this.rights][this.eventID].eventActive) {
						AppRootViewModel.instance.activePlaylist = null;
						this.refreshEvent();
						this.currentTrack = null;
					}
					if (this.interval) {
						clearInterval(this.interval);
						this.interval = null;
					}
					this.set("timelineValue", 0);
					this.set('currentTime', '00:00');
					this.set('totalTime', '00:00');	
					this.set('playPauseIcon', IconsCode.Play);
				}
				Locker.release(this.eventID);
			});
		});
		
		
		this.socketIO.on('EVENT_TRACK_LIKE', data => {
			Locker.acquire(this.eventID, () => {
				console.log('EVENT_TRACK_LIKE');
				this.tracksList.forEach((track: any, idx) => {
					if (track.id == data.trackid) {
						let separatorIndex: number = track.rightIcon.indexOf(' ') + 1;
						if (separatorIndex <= track.rightIcon.length) {
							track.rightIcon = track.rightIcon.substring(0, separatorIndex);
						}
						track.rightIcon += data.trackLikesTotal;			
					}
				});
				addLike(this.rights, this.eventID, this.getPlaylistIndexByTrackID(data.trackid), data.trackid);
				Locker.release(this.eventID);
			});
		});
		
		this.socketIO.on('EVENT_TRACK_UNLIKE', data => {
			Locker.acquire(this.eventID, () => {
				console.log('EVENT_TRACK_UNLIKE');
				this.tracksList.forEach((track: any, idx) => {
					if (track.id == data.trackid) {
						let separatorIndex: number = track.rightIcon.indexOf(' ') + 1;
						if (separatorIndex <= track.rightIcon.length) {
							track.rightIcon = track.rightIcon.substring(0, separatorIndex);
						}
						track.rightIcon += data.trackLikesTotal;
					}
				});
				removeLike(this.rights, this.eventID, this.getPlaylistIndexByTrackID(data.trackid), data.trackid);
				Locker.release(this.eventID);
			});
		});

		this.socketIO.on('EVENT_TRACK_DISLIKE', data => {
			Locker.acquire(this.eventID, () => {

				console.log('EVENT_TRACK_DISLIKE');
				this.tracksList.forEach((track: any, idx) => {
					if (track.id == data.trackid) {
						let separatorIndex: number = track.leftIcon.indexOf(' ') + 1;
						if (separatorIndex <= track.leftIcon.length) {
							track.leftIcon = track.leftIcon.substring(0, separatorIndex);
						}
						track.leftIcon += data.trackDislikesTotal;
					}
				});
				addDislike(this.rights, this.eventID, this.getPlaylistIndexByTrackID(data.trackid), data.trackid);
				Locker.release(this.eventID);
			});
		});
		
		this.socketIO.on('EVENT_TRACK_UNDISLIKE', data => {
			Locker.acquire(this.eventID, () => {
				console.log('EVENT_TRACK_UNDISLIKE', data);
				this.tracksList.forEach((track: any, idx) => {
					if (track.id == data.trackid) {
						let separatorIndex: number = track.leftIcon.indexOf(' ') + 1;
						if (separatorIndex <= track.leftIcon.length) {
							track.leftIcon = track.leftIcon.substring(0, separatorIndex);
						}
						track.leftIcon += data.trackDislikesTotal;	
					}
				});
				removeDislike(this.rights, this.eventID, this.getPlaylistIndexByTrackID(data.trackid), data.trackid);
				Locker.release(this.eventID);
			});
		});

		this.socketIO.on('EVENT_TRACK_MOVE', data => {
			console.log('EVENT_TRACK_MOVE');

			Locker.acquire(this.eventID, () => {


				let playlistIndex: number;
				let trackIndex: number;

				[playlistIndex, trackIndex] = this.getIndexByTrackID(data.trackid);

				if (trackIndex > -1) {
					// avoid double move?
					let toMove: any = AppRootViewModel.instance.events[this.rights][this.eventID].eventPlaylists[playlistIndex].unplayedTracks.splice(trackIndex, 1)[0];
					AppRootViewModel.instance.events[this.rights][this.eventID].eventPlaylists[playlistIndex].unplayedTracks.splice(data.index, 0, toMove);

					if (data.playlistid == AppRootViewModel.instance.activePlaylist) {
						for (var index: number = 0; index < this.tracksList.length; index++) {
							if ((this.tracksList.getItem(index) as any as EventTrack).id == data.trackid) {
								break;
							}
						}
						toMove = this.tracksList.splice(index, 1)[0];
						
						let realIndex = data.index + AppRootViewModel.instance.events[this.rights][this.eventID].eventPlaylists[playlistIndex].playedTracks.length;

						this.tracksList.splice(realIndex, 0, toMove);
					}
				}
				Locker.release(this.eventID);
			});
		});

		this.socketIO.on('EVENT_TRACK_PLAY', async data => {

			let deviceDate = new Date();

			if (data.by == AppRootViewModel.instance.me.id) {
				return ;
			}
			
			console.log('EVENT_TRACK_PLAY');

			if (!this.isOwnerConnected) {
				this.isOwnerConnected = true;
			}

			let playlist: PlaylistAPI =  null;
			let index: number = -1;
			AppRootViewModel.instance.events[this.rights][this.eventID].eventPlaylists.forEach((pl, idx) => {
				if (pl.id == data.playlistid) {
					playlist = pl;
					index = idx;
				}
			});

			if (index == -1 || !playlist) {
				return ;
			}

			for (let i: number = 0; i < AppRootViewModel.instance.events[this.rights][this.eventID].eventPlaylists[index].unplayedTracks.length; i++) {
				let track: TrackAPI = AppRootViewModel.instance.events[this.rights][this.eventID].eventPlaylists[index].unplayedTracks[i];

				if (track.id == data.trackid) {
					AppRootViewModel.instance.events[this.rights][this.eventID].eventPlaylists[index].playedTracks.push(track);
					AppRootViewModel.instance.events[this.rights][this.eventID].eventPlaylists[index].unplayedTracks.splice(i, 1);
					break;
				}
			}

			AppRootViewModel.instance.events[this.rights][this.eventID].eventPlaylists[index].activeTrack = data.trackid;
			

			// if not the same
			if (AppRootViewModel.instance.activePlaylist != data.playlistid) {
				
				if (playlist.id == data.playlistid) {
					this.fillCurrentPlaylistTracks(playlist);
				}
			
				
				await this.buildPlaylist();
				await new Promise(resolve => setTimeout(resolve, 200));
				let keys = Object.keys(this.currentPlaylist.currentTracks);

				for (let i: number = 0; i < keys.length; i++) {
					if (keys[i] == data.trackid) {
						this.setCurrentTrack(i);
						break;
					}
				}
				AppRootViewModel.instance.activePlaylist = data.playlistid;
			} else {
				this.tracksList.forEach((track: any, index) => {
					if (track.id == data.trackid) {
						this.setCurrentTrack(index);
					}
				});
			}
			this.trackTimestamp = new Date(new Date(data.timestamp).getTime() - (new Date(data.timestamp).getTime() - deviceDate.getTime()) - data.trackPosition);
			this.currentTimeMs = data.trackPosition;
			this.setTrackInterval();
			this.playing = true;
			if (this.rights == EventRights.DJ) {
				this.set("playPauseIcon", IconsCode.Pause);
			}
		});

		this.socketIO.on('EVENT_OWNER_CONNECT', data => {
			console.log('EVENT_OWNER_DISCONNECT');
			this.isOwnerConnected = true;
		});
		
		this.socketIO.on('EVENT_OWNER_DISCONNECT', data => {
			console.log('EVENT_OWNER_DISCONNECT');
			this.isOwnerConnected = false;
			if (this.interval) {
				clearInterval(this.interval);
				this.interval = null;
			}
			if (this.rights == EventRights.DJ) {
				this.set("playPauseIcon", IconsCode.Play);
			}
		});

		this.socketIO.on('EVENT_TRACK_PAUSE', data => {
			if (data.by == AppRootViewModel.instance.me.id) {
				return ;
			}
			console.log('EVENT_TRACK_PAUSE');

			clearInterval(this.interval);
			this.interval = null;
			if (this.rights == EventRights.DJ) {
				this.set("playPauseIcon", IconsCode.Play);
			}
			this.playing = false;
		});
		
		this.socketIO.on('EVENT_TRACK_SEEK', data => {
			
			console.log('EVENT_TRACK_SEEK');

			if (data.by != AppRootViewModel.instance.me.id) {
				this.trackTimestamp = new Date(new Date(data.timestamp).getTime() - (new Date(data.timestamp).getTime() - new Date().getTime()) - data.trackPosition);
				this.currentTimeMs = data.trackPosition;
			}
		});
		
		this.socketIO.on('EVENT_TRACK_PLAY_REQUEST', data => {
			
			Locker.acquire('EVENT_TRACK_REQUEST', async () => {
				console.log('EVENT_TRACK_PLAY_REQUEST');
				let playlist: PlaylistAPI;
				let trackIndex: number;

				let done: boolean = false;
				for (let playlistIndex: number = 0; playlistIndex < AppRootViewModel.instance.events[this.rights][this.eventID].eventPlaylists.length; playlistIndex++) {
					
					let currentPlaylist: PlaylistAPI = AppRootViewModel.instance.events[this.rights][this.eventID].eventPlaylists[playlistIndex];
					
					for (let i: number = 0; i < currentPlaylist.playedTracks.length; i++) {
						if (currentPlaylist.playedTracks[i].id == data.trackid) {
							playlist = currentPlaylist;
							trackIndex = i;
							done = true;
							break;
						}
					}
					if (done) {
						break;
					}
					
					for (let i: number = 0; i < currentPlaylist.unplayedTracks.length; i++) {
						if (currentPlaylist.unplayedTracks[i].id == data.trackid) {
							playlist = currentPlaylist;
							trackIndex = currentPlaylist.playedTracks.length + i;
							done = true;
						}
					}
					if (done) {
						break;
					}
				}

				if (playlist && trackIndex > -1) {
					if (data.playlistid != AppRootViewModel.instance.activePlaylist) {
						this.fillCurrentPlaylistTracks(playlist);
						await this.buildPlaylist();
						await new Promise(resolve => setTimeout(resolve, 200));
						AppRootViewModel.instance.activePlaylist = playlist.id;
					}

					if (this.currentTrack && data.trackid == this.currentTrack.id) {
						this.play();
					} else {
						this.setCurrentTrack(trackIndex, true);
					}
				}
				Locker.release('EVENT_TRACK_REQUEST');
			});
		});

		this.socketIO.on('EVENT_TRACK_PAUSE_REQUEST', data => {
			Locker.acquire('EVENT_TRACK_REQUEST', async () => {
				
				console.log('EVENT_TRACK_PAUSE_REQUEST');
				
				await this.pause();
				Locker.release('EVENT_TRACK_REQUEST');
			});
		});

		this.socketIO.on('EVENT_TRACK_SEEK_REQUEST', data => {
			Locker.acquire('EVENT_TRACK_REQUEST', async () => {
				console.log('EVENT_TRACK_SEEK_REQUEST');
				this.stopMoveCursor(-1, data.trackPosition)
				Locker.release('EVENT_TRACK_REQUEST');
			});
		});


		this.socketIO.on('EVENT_UPDATE', data => {
			console.log('EVENT_UPDATE');

			Locker.acquire(this.eventID, () => {
				AppRootViewModel.instance.events[this.rights][this.eventID].name = data.name;
				AppRootViewModel.instance.events[this.rights][this.eventID].visibility = data.visibility;
				Locker.release(this.eventID)
			});
		});

		this.socketIO.on('EVENT_DELETED', data => {
			console.log('EVENT_DELETED');


			Toast.makeText("Event deleted", "long").show();
			this.destroy();
			delete AppRootViewModel.instance.events.owner[this.eventID];
			delete AppRootViewModel.instance.events.dj[this.eventID];
			delete AppRootViewModel.instance.events.guest[this.eventID];
			frameModule.topmost().navigate({
				moduleName: "views/events/events"
			});
		});
		this.socketIO.connect();
	}

	private getPlaylistIndexByTrackID(trackID: string): number {
		for (let playlistIndex: number = 0; playlistIndex < AppRootViewModel.instance.events[this.rights][this.eventID].eventPlaylists.length; playlistIndex++) {			
			let currentPlaylist: PlaylistAPI = AppRootViewModel.instance.events[this.rights][this.eventID].eventPlaylists[playlistIndex];
			
			for (let trackIndex: number = 0; trackIndex < currentPlaylist.playedTracks.length; trackIndex++) {
				if (currentPlaylist.playedTracks[trackIndex].id == trackID) {
					return playlistIndex;
				}
			}
			for (let trackIndex: number = 0; trackIndex < currentPlaylist.unplayedTracks.length; trackIndex++) {
				if (currentPlaylist.unplayedTracks[trackIndex].id == trackID) {
					return playlistIndex;
				}
			}
		}
		return -1;
	}

	private getIndexByTrackID(trackID: string): [number, number] {
		let index: number = -1;

		for (let playlistIndex: number = 0; playlistIndex < AppRootViewModel.instance.events[this.rights][this.eventID].eventPlaylists.length; playlistIndex++) {

			let currentPlaylist: PlaylistAPI = AppRootViewModel.instance.events[this.rights][this.eventID].eventPlaylists[playlistIndex];

			for (let trackIndex: number = 0; trackIndex < currentPlaylist.unplayedTracks.length; trackIndex++) {
				if (currentPlaylist.unplayedTracks[trackIndex].id == trackID) {
					return [playlistIndex, trackIndex];
				}
			}
			
			for (let trackIndex: number = 0; trackIndex < currentPlaylist.playedTracks.length; trackIndex++) {
				if (currentPlaylist.playedTracks[trackIndex].id == trackID) {
					return [playlistIndex, trackIndex];
				}
			}
		}
		return [-1, -1];
	}

}