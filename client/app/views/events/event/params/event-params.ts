import { EventData } from "tns-core-modules/data/observable";
import { Page } from "tns-core-modules/ui/page";
import { Label } from "tns-core-modules/ui/label/label";
import { Switch } from "tns-core-modules/ui/switch";
import { TextField } from "tns-core-modules/ui/text-field/text-field";
import { ModalDatetimepicker } from "nativescript-modal-datetimepicker";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { EventAPI, AppRootViewModel } from "~/app-root-view-model";
import { EventParamsViewModel } from "~/views/events/event/params/event-params-view-model";
import * as frameModule from "tns-core-modules/ui/frame"

let drawer: RadSideDrawer = null;
let page: Page = null;
let eventParamsViewModel: EventParamsViewModel = new EventParamsViewModel();
let event: EventAPI = null;
const picker = new ModalDatetimepicker();

export function navigatingTo(args: EventData): void {
	drawer = <RadSideDrawer>frameModule.topmost().parent;

	page = <Page>args.object;
	
	page.bindingContext = eventParamsViewModel;

	eventParamsViewModel.fillSortingTypes();

	let context = page.navigationContext;

	if (context) {
		let votingScheduleSwitch: Switch = <Switch>page.getViewById("votingScheduleSwitch");
		let eventScheduleSwitch: Switch = <Switch>page.getViewById("eventScheduleSwitch");

		if (context.isNew) {
			event = null;
			eventParamsViewModel.set("actionBarTitle", "New Event");

			eventParamsViewModel.set("votingScheduleClassName", '');
			eventParamsViewModel.set("eventScheduleClassName", '');

			votingScheduleSwitch.checked = true;
			eventScheduleSwitch.checked = true;
		} else {
			let eventName: TextField = <TextField>page.getViewById("eventName");
			let eventPrivacy: Switch = <Switch>page.getViewById("eventPrivacy");

			event = AppRootViewModel.instance.events.owner[context.eventID];

			eventParamsViewModel.set("actionBarTitle", "Update event");
			eventName.text = event.name;
			eventPrivacy.checked = event.visibility == "PUBLIC";
			
			eventParamsViewModel.set("votingScheduleClassName", event.votingSchedule ? '' : 'hidden');
			eventParamsViewModel.set("eventScheduleClassName", event.eventSchedule ? '' : 'hidden');

			votingScheduleSwitch.checked = event.votingSchedule ? true : false;
			eventScheduleSwitch.checked = event.eventSchedule ? true : false;

			if (event.votingSchedule) {
				eventParamsViewModel.setVotingStartDate(new Date(event.votingSchedule.start));
				eventParamsViewModel.setVotingStartTime(new Date(event.votingSchedule.start));
				eventParamsViewModel.setVotingEndDate(new Date(event.votingSchedule.end));
				eventParamsViewModel.setVotingEndTime(new Date(event.votingSchedule.end));
			}

			if (event.eventSchedule) {
				eventParamsViewModel.setEventStartDate(new Date(event.eventSchedule.start));
				eventParamsViewModel.setEventStartTime(new Date(event.eventSchedule.start));
				eventParamsViewModel.setEventEndDate(new Date(event.eventSchedule.end));
				eventParamsViewModel.setEventEndTime(new Date(event.eventSchedule.end));
			}
		}
	}
}

export async function saveChanges(): Promise<void> {
	let errorMessage: Label = <Label>page.getViewById("error-message");
	let eventName: TextField = <TextField>page.getViewById("eventName");
	let eventPrivacy: Switch = <Switch>page.getViewById("eventPrivacy");
	let isVotingScheduleSwitch: Switch = <Switch>page.getViewById("votingScheduleSwitch");
	let isEventScheduleSwitch: Switch = <Switch>page.getViewById("eventScheduleSwitch");
	let isVotingSchedule: boolean = isVotingScheduleSwitch.checked;
	let isEventSchedule: boolean = isEventScheduleSwitch.checked;

	if (eventName.text.length == 0) {
		errorMessage.className = 'display-error'
		return ;
	}

	if (eventParamsViewModel.get("loading") == true) {
		return ;
	}

	eventParamsViewModel.set("loading", true);
	eventName.isEnabled = false;
	eventPrivacy.isEnabled = false;
	isVotingScheduleSwitch.isEnabled = false;
	isEventScheduleSwitch.isEnabled = false;

	if (event == null) {
		let event = await eventParamsViewModel.createEvent(eventName.text, eventPrivacy.checked, isVotingSchedule, isEventSchedule);
		
		if (event) {
			frameModule.topmost().navigate({
				moduleName: "views/events/event/event",
				context: {
					eventID: event.id,
					wasInParam: true
				}
			});
		}
	} else {
		
		let updatedEvent = await eventParamsViewModel.updateEvent(event.id, eventName.text, eventPrivacy.checked, isVotingSchedule, isEventSchedule).catch(e => {
		});
		if (updatedEvent) {
			frameModule.topmost().navigate({
				moduleName: "views/events/event/event",
				context: {
					eventID: updatedEvent.id,
					wasInParam: true
				}
			});
		}
	}

	eventName.isEnabled = true;
	eventPrivacy.isEnabled = true;
	isVotingScheduleSwitch.isEnabled = true;
	isEventScheduleSwitch.isEnabled = true;
	eventParamsViewModel.set("loading", false);
}

export function onVotingScheduleChecked(args: EventData): void {
	let votingScheduleSwitch: Switch = <Switch>args.object;
	eventParamsViewModel.set("votingScheduleClassName", votingScheduleSwitch.checked ? 'hidden' : '');
}

export function onEventScheduleChecked(args: EventData): void {
	let eventScheduleSwitch: Switch = <Switch>args.object;
	eventParamsViewModel.set("eventScheduleClassName", eventScheduleSwitch.checked ? 'hidden' : '');
}

export function selectVotingStartDate(): void {
	picker.pickDate({
		title: "Voting Start Date",
		minDate: new Date(),
		maxDate: new Date(new Date().setFullYear(new Date().getFullYear() + 1)),
	})
	.then(result => {
		eventParamsViewModel.setVotingStartDate(new Date(result.year, result.month - 1, result.day));
	});
};

export function selectVotingStartTime(): void {
	let date: Date = new Date();
	picker.pickTime({
		is24HourView: true
	}).then(result => {
		date.setHours(result.hour, result.minute);
		// Set time local timezone
		eventParamsViewModel.setVotingStartTime(date);
	});
}

export function selectVotingEndDate(): void {
	picker.pickDate({
		title: "Voting End Date",
		minDate: eventParamsViewModel.getVotingStartDate(),
		maxDate: new Date(new Date().setFullYear(new Date().getFullYear() + 1)),
	})
	.then(result => {
		eventParamsViewModel.setVotingEndDate(new Date(result.year, result.month - 1, result.day));
	});
};

export function selectVotingEndTime(): void {
	let date: Date = new Date();
	picker.pickTime({
		is24HourView: true
	}).then(result => {
		date.setHours(result.hour, result.minute);
		// Set time local timezone
		eventParamsViewModel.setVotingEndTime(date);
	});
}

export function selectEventStartDate(): void {
	picker.pickDate({
		title: "Event Start Date",
		minDate: new Date(),
		maxDate: new Date(new Date().setFullYear(new Date().getFullYear() + 1)),
	})
	.then(result => {
		eventParamsViewModel.setEventStartDate(new Date(result.year, result.month - 1, result.day));
	});
};

export function selectEventStartTime(): void {
	let date: Date = new Date();
	picker.pickTime({
		is24HourView: true
	}).then(result => {
		date.setHours(result.hour, result.minute);
		eventParamsViewModel.setEventStartTime(date);
	});
}

export function selectEventEndDate(): void {
	picker.pickDate({
		title: "Event End Date",
		minDate: eventParamsViewModel.getEventStartDate(),
		maxDate: new Date(new Date().setFullYear(new Date().getFullYear() + 1)),
	})
	.then(result => {
		eventParamsViewModel.setEventEndDate(new Date(result.year, result.month - 1, result.day));
	});
};

export function selectEventEndTime(): void {
	let date: Date = new Date();
	picker.pickTime({
		is24HourView: true
	}).then(result => {
		date.setHours(result.hour, result.minute);
		eventParamsViewModel.setEventEndTime(date);
	});
}

export function showSideDrawer(): void {
	if (drawer) {
		drawer.showDrawer();
	}
}