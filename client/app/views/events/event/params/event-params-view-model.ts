import { Observable } from "tns-core-modules/ui/page/page";
import { MusicRoomApi } from "~/utils/MusicRoomApi";
import { EventAPI, AppRootViewModel } from "~/app-root-view-model";
import { IconsCode } from "~/utils/Icons";
import * as Toast from 'nativescript-toast';

export class EventParamsViewModel extends Observable {

	private votingStartDateTime: Date;
	private votingEndDateTime: Date;
	private eventStartDateTime: Date;
	private eventEndDateTime: Date;
	
	constructor() {
		super();
	}

	public fillSortingTypes() {
 
		this.set('calendarIcon', IconsCode.Calendar);
		this.set('clockIcon', IconsCode.Clock);

		this.votingStartDateTime = new Date();
		this.votingEndDateTime = new Date();

		this.eventStartDateTime = new Date();
		this.eventEndDateTime = new Date();

		this.set('votingStartDate', this.formatDate(this.votingStartDateTime));
		this.set('votingEndDate', this.formatDate(this.votingEndDateTime));
		this.set('votingStartTime', this.formatTime(this.votingStartDateTime));
		this.set('votingEndTime', this.formatTime(this.votingEndDateTime));

		this.set('eventStartDate', this.formatDate(this.eventStartDateTime));
		this.set('eventEndDate', this.formatDate(this.eventEndDateTime));
		this.set('eventStartTime', this.formatTime(this.eventStartDateTime));
		this.set('eventEndTime', this.formatTime(this.eventEndDateTime));

		this.votingStartDateTime.setTime(this.votingStartDateTime.getTime() + this.votingStartDateTime.getTimezoneOffset() * 60 * 1000);
		this.votingEndDateTime.setTime(this.votingEndDateTime.getTime() + this.votingEndDateTime.getTimezoneOffset() * 60 * 1000);

		this.eventStartDateTime.setTime(this.eventStartDateTime.getTime() + this.eventStartDateTime.getTimezoneOffset() * 60 * 1000);
		this.eventEndDateTime.setTime(this.eventEndDateTime.getTime() + this.eventEndDateTime.getTimezoneOffset() * 60 * 1000);
	}

	public async createEvent(name: string, isPublic: boolean, isVotingSchedule: boolean, isEventSchedule: boolean): Promise<EventAPI> {

		let event: EventAPI = null;

		await MusicRoomApi.instance.post(MusicRoomApi.events, {
			name,
			visibility: isPublic ? 'PUBLIC' : 'PRIVATE',
			votingSchedule: isVotingSchedule ? {
				start: this.votingStartDateTime.toISOString(),
				end: this.votingEndDateTime.toISOString(),
			} : null,
			eventSchedule: isEventSchedule ? {
				start: this.eventStartDateTime.toISOString(),
				end: this.eventEndDateTime.toISOString(),
			} : null
		}, response => {
			event = response;
			AppRootViewModel.instance.events.owner[event.id] = event;
		}, error => {
			Toast.makeText(error.errorMessage).show();
		});

		return event;
	}

	public async updateEvent(eventID: string, name: string, isPublic: boolean, isVotingSchedule: boolean, isEventSchedule: boolean): Promise<EventAPI> {
		let event: EventAPI = null;

		await MusicRoomApi.instance.put(MusicRoomApi.events + eventID, {
			name,
			visibility: isPublic ? 'PUBLIC' : 'PRIVATE',
			votingSchedule: isVotingSchedule ? {
				start: this.votingStartDateTime.toISOString(),
				end: this.votingEndDateTime.toISOString(),
			} : null,
			eventSchedule: isEventSchedule ? {
				start: this.eventStartDateTime.toISOString(),
				end: this.eventEndDateTime.toISOString(),
			} : null
		}, response => {
			if (!response.id) {
				Toast.makeText(response.message).show();
			} else {
				event = response;
				AppRootViewModel.instance.events.owner[event.id] = event;
			}
		}, error => {
			Toast.makeText(error.errorMessage).show();
		});
		return event;
	}

	public setVotingStartDate(date: Date): void {
		this.votingStartDateTime.setFullYear(date.getFullYear());
		this.votingStartDateTime.setMonth(date.getMonth());
		this.votingStartDateTime.setDate(date.getDate());
		
		this.notifyPropertyChange('votingStartDate', this.formatDate(this.votingStartDateTime));
		
		if (this.votingStartDateTime > this.votingEndDateTime) {
			this.votingEndDateTime.setDate(this.votingStartDateTime.getDate());
			this.votingEndDateTime.setMonth(this.votingStartDateTime.getMonth());
			this.votingEndDateTime.setFullYear(this.votingStartDateTime.getFullYear());
			this.notifyPropertyChange('votingEndDate', this.formatDate(this.votingEndDateTime));
		}
	}
	
	public setVotingStartTime(date: Date): void {
		this.notifyPropertyChange('votingStartTime', this.formatTime(date));

		this.votingStartDateTime.setHours(date.getHours());
		this.votingStartDateTime.setMinutes(date.getMinutes());
		this.votingStartDateTime.setSeconds(0);
		this.votingStartDateTime.setMilliseconds(0);
	}
	
	public setVotingEndDate(date: Date): void {
		this.votingEndDateTime.setFullYear(date.getFullYear());
		this.votingEndDateTime.setMonth(date.getMonth());
		this.votingEndDateTime.setDate(date.getDate());
		this.notifyPropertyChange('votingEndDate', this.formatDate(this.votingEndDateTime));
	}

	public setVotingEndTime(date: Date): void {
		this.notifyPropertyChange('votingEndTime', this.formatTime(date));

		this.votingEndDateTime.setHours(date.getHours());
		this.votingEndDateTime.setMinutes(date.getMinutes());
		this.votingEndDateTime.setSeconds(0);
		this.votingEndDateTime.setMilliseconds(0);
	}

	public getVotingEndDate(): Date {
		return this.votingStartDateTime;
	}

	public getVotingStartDate(): Date {
		return this.votingStartDateTime;
	}

	public setEventStartDate(date: Date): void {
		this.eventStartDateTime.setFullYear(date.getFullYear());
		this.eventStartDateTime.setMonth(date.getMonth());
		this.eventStartDateTime.setDate(date.getDate());
		this.notifyPropertyChange('eventStartDate', this.formatDate(this.eventStartDateTime));

		if (this.eventStartDateTime > this.eventEndDateTime) {
			this.eventEndDateTime.setDate(this.eventStartDateTime.getDate());
			this.eventEndDateTime.setMonth(this.eventStartDateTime.getMonth());
			this.eventEndDateTime.setFullYear(this.eventStartDateTime.getFullYear());
			this.notifyPropertyChange('eventEndDate', this.formatDate(this.eventEndDateTime));
		}
	}

	public setEventStartTime(date: Date): void {
		this.notifyPropertyChange('eventStartTime', this.formatTime(date));
		this.eventStartDateTime.setHours(date.getHours());
		this.eventStartDateTime.setMinutes(date.getMinutes());
		this.eventStartDateTime.setSeconds(0);
		this.eventStartDateTime.setMilliseconds(0);
	}

	public setEventEndDate(date: Date): void {
		this.eventEndDateTime.setFullYear(date.getFullYear());
		this.eventEndDateTime.setMonth(date.getMonth());
		this.eventEndDateTime.setDate(date.getDate());
		this.notifyPropertyChange('eventEndDate', this.formatDate(this.eventEndDateTime));
	}

	public setEventEndTime(date: Date): void {
		this.notifyPropertyChange('eventEndTime', this.formatTime(date));
		this.eventEndDateTime.setHours(date.getHours());
		this.eventEndDateTime.setMinutes(date.getMinutes());
		this.eventEndDateTime.setSeconds(0);
		this.eventEndDateTime.setMilliseconds(0);
	}

	public getEventEndDate(): Date {
		return this.eventStartDateTime;
	}

	public getEventStartDate(): Date {
		return this.eventStartDateTime;
	}

	private formatDate(date: Date): string {
		let day: string = date.getDate() < 10 ? '0' + date.getDate() : date.getDate().toString();
		let month: string = date.getMonth() + 1 < 10 ? '0' + date.getMonth() : date.getMonth().toString();
		return day + '/' + month + '/' + date.getFullYear();
	}

	private formatTime(date: Date): string {
		let hour: string = date.getHours() < 10 ? '0' + date.getHours() : date.getHours().toString();
		let minutes: string = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes().toString();
		return hour + ':' + minutes;
	}

}