import { Page, View } from "tns-core-modules/ui/page";
import { EventData, Observable } from "tns-core-modules/data/observable";
import { ListView, ItemEventData } from "tns-core-modules/ui/list-view";
import { EventsViewModel, Event } from "./events-view-model";
import { EventAPI, AppRootViewModel } from "~/app-root-view-model"; 
import * as frameModule from "tns-core-modules/ui/frame"
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { ListViewEventData, RadListView, SwipeActionsEventData } from "nativescript-ui-listview";
import { IconsCode } from "~/utils/Icons";
import * as Toast from "nativescript-toast";
import { StringValues } from "~/utils/Values";

let page: Page = null;
let eventsListView: RadListView = null;
let eventViewModel: EventsViewModel= null;
let drawer: RadSideDrawer = null;
let rlvEvents: RadListView = null;
let onMultipleSelection: boolean = false;

export function navigatingTo(args: EventData): void {
	page = <Page>args.object;
	
	eventViewModel = new EventsViewModel();
	page.bindingContext = eventViewModel;
	

	eventsListView = page.getViewById("eventsListView");
	eventsListView.refresh();

	drawer = <RadSideDrawer>frameModule.topmost().parent;

	rlvEvents = page.getViewById("eventsListView");
	onMultipleSelection = false;

	eventViewModel.set("actionBarTitle", "Events");
	eventViewModel.set("menuIcon", "res://menu_light");
}

export function onConfirm(args: ItemEventData): void {
	
	if (onMultipleSelection) {
		let selectedEvents: Event[] = rlvEvents.getSelectedItems();
		rlvEvents.deselectAll();
		eventViewModel.deleteMultiple(selectedEvents);
	}
}


export function onItemTap(args: ItemEventData): void {
	let event: EventAPI = args.view.bindingContext;

	if (onMultipleSelection) {
		if (rlvEvents.getSelectedItems().indexOf(args.view.bindingContext) == -1) {
			rlvEvents.selectItemAt(args.index);
			
		} else {
			rlvEvents.deselectItemAt(args.index);
		}
	} else {
		frameModule.topmost().navigate({
			moduleName: "views/events/event/event",
			context: {
				eventID: event.id
			}
		});
	}
}

export function newEvent() {
	if (AppRootViewModel.instance.me.isPremium) {
		frameModule.topmost().navigate({
			moduleName: "views/events/event/params/event-params",
			context: {
				isNew: true
			}
		});
	} else {
		Toast.makeText(StringValues.upgradePremium).show();
	}
}

export function onItemSelected(args: ItemEventData): void {
	let event: Event = args.view.bindingContext;

	event.selectedClass = "list-group-item-selected";
	eventViewModel.set("actionBarTitle", "Remove Events (" + rlvEvents.getSelectedItems().length + ")");
	
	if (!onMultipleSelection) {
		eventViewModel.set("menuIcon", "res://back_arrow");
		eventViewModel.set("confirmIcon", IconsCode.Remove);
		onMultipleSelection = true;
	}
}

export function onItemDeselected(args: ItemEventData): void {
	let playlist: Observable = eventViewModel.getObservablePlaylist(args.index);
	if (playlist) {
		playlist['selectedClass'] = '';
	}

	eventViewModel.set("actionBarTitle", "Remove Events (" + rlvEvents.getSelectedItems().length + ")");
	if (rlvEvents.getSelectedItems().length == 0) {
		onMultipleSelection = false;
		eventViewModel.set("menuIcon", "res://menu_light");
		eventViewModel.set("actionBarTitle", "Events");
		eventViewModel.set("confirmIcon", "");
	}
}


export function onPullToRefreshInitiated(args: ListViewEventData) {
	rlvEvents.deselectAll();
    eventViewModel.fillEvents(true);
	eventsListView.notifyPullToRefreshFinished();
}

export function onSwipeCellStarted(args: SwipeActionsEventData) {
	const swipeLimits = args.data.swipeLimits;
	const swipeView = args.object;
	const leftItem = swipeView.getViewById<View>('left-delete-view');
	const rightItem = swipeView.getViewById<View>('right-delete-view');
	swipeLimits.left = leftItem.getMeasuredWidth();
	swipeLimits.right = rightItem.getMeasuredWidth();
	swipeLimits.threshold = leftItem.getMeasuredWidth() / 2;
}

export function onLeftSwipeClick(args: ListViewEventData) {
	eventsListView.notifySwipeToExecuteFinished();
	eventViewModel.delete(args.object.bindingContext);
}

export function onRightSwipeClick(args: ListViewEventData) {
	eventsListView.notifySwipeToExecuteFinished();
	eventViewModel.delete(args.object.bindingContext);
}

export function showSideDrawer(): void {
	if (drawer) {
		if (eventViewModel.get("menuIcon") == "res://back_arrow") {
			if (onMultipleSelection) {
				rlvEvents.deselectAll();
			}
		} else {
			drawer.showDrawer();
		}
	}
}