import { EventData } from "tns-core-modules/data/observable";
import { Page } from "tns-core-modules/ui/page"
import { AppRootViewModel } from "./app-root-view-model";
import { RadSideDrawer, DrawerStateChangingEventArgs } from "nativescript-ui-sidedrawer";
import * as application from "tns-core-modules/application";
import * as appSettings from "tns-core-modules/application-settings";
import * as frameModule from "tns-core-modules/ui/frame"
import * as view from "tns-core-modules/ui/core/view";
import { StringValues } from "~/utils/Values";
import { init } from "nativescript-facebook";
import { launchEvent, lowMemoryEvent, resumeEvent, uncaughtErrorEvent, 
	ApplicationEventData, LaunchEventData, UnhandledErrorEventData,
	on as applicationOn, run as applicationRun } from "tns-core-modules/application";
import * as utils from "tns-core-modules/utils/utils";

import { EventViewModel } from "./views/events/event/event-view-model";
import { MusicRoomApi } from "./utils/MusicRoomApi";
import { formatDisplayName } from "./utils/Tools";

import { handleOpenURL, AppURL } from 'nativescript-urlhandler';
import * as Toast from "nativescript-toast";

var frame = require("tns-core-modules/ui/frame");

let page: Page = null;
let drawer: RadSideDrawer = null;

const defaultPage = "views/friends/friends";

// Format: http://address/endpoint/param1
function parseURL(params: string[]) {
	if (params[1] == 'events' && params[2]) {
		MusicRoomApi.instance.get(MusicRoomApi.events + params[2], response => {
			if (response.owner.id == AppRootViewModel.instance.me.id) {
				AppRootViewModel.instance.events.owner[params[2]] = response;
			} else if (response.djs.some(dj => dj.id == AppRootViewModel.instance.me.id)) {
				AppRootViewModel.instance.events.dj[params[2]] = response;
			} else if (response.guests.some(guest => guest.id == AppRootViewModel.instance.me.id) || response.visibility == 'PUBLIC') {
				AppRootViewModel.instance.events.guest[params[2]] = response;
			} else {
				Toast.makeText('Error while opening this page. Do you have the permissions?').show();
				return ;
			}
			frameModule.topmost().navigate({
				moduleName: 'views/events/event/event', 
				context: {
					eventID: params[2]
				}
			});
		}, error => {
			Toast.makeText(error.errorMessage).show();
		});
	}
}

handleOpenURL((appURL: AppURL) => {
	parseURL([ ...appURL.params.keys() ]);
});

applicationOn(lowMemoryEvent, (args) => {
	console.log('Low Memory');
});

applicationOn(launchEvent, async (args: LaunchEventData) => {
	new AppRootViewModel();
	init("335219297115348");

	if (appSettings.getString(StringValues.apiKey, "none") != "none") {
		AppRootViewModel.instance.apiKey = appSettings.getString(StringValues.apiKey);
		AppRootViewModel.instance.userID = appSettings.getString(StringValues.userID);
		
		await MusicRoomApi.instance.get(MusicRoomApi.me, response => {
			AppRootViewModel.instance.set("avatarURL", MusicRoomApi.baseUrl + ':' + MusicRoomApi.port + '/' + response.avatar + "?" + new Date().toISOString());
			AppRootViewModel.instance.me = response;
			AppRootViewModel.instance.set("displayName", formatDisplayName(AppRootViewModel.instance.me));
			AppRootViewModel.instance.set("route", defaultPage);
		}, error => {
			appSettings.clear();
			(<RadSideDrawer>view.getViewById(page, "sideDrawer")).gesturesEnabled = false;
			AppRootViewModel.instance.set("route", "views/login/login-page");
		})
	} else {
		AppRootViewModel.instance.set("route", "views/login/login-page");
	}
});

applicationOn(uncaughtErrorEvent, (args: UnhandledErrorEventData) => {
	console.log('UnhandledErrorEventData: ', args.error);
});

applicationOn(resumeEvent, (args: ApplicationEventData) => {
	let intent: any = args.android.getIntent();
	if (intent) {
		let data = intent.getData();
		if (!data) {
			return ;
		}
		data = data.toString()
		intent.setData(null);
		if (data) {
			if (data.startsWith('http://')) {
				data = data.slice('http://'.length);
				parseURL(data.split('/'));
			}
		}
	}

	if (AppRootViewModel.instance.me) {
		let sideDrawer: RadSideDrawer = frameModule.topmost().parent.getViewById('sideDrawer');
		sideDrawer.gesturesEnabled = true;
	}
});

if (application.android) {
	application.android.on(application.AndroidApplication.activityBackPressedEvent, (args) => {
		var currentPage = frame.topmost().currentPage;
		if (currentPage && currentPage.exports && typeof currentPage.exports.backEvent === "function") {
			currentPage.exports.backEvent(args);
		}
	});
}

export async function onLoaded(args: EventData) {

	page = <Page>args.object;
	
	page.bindingContext = AppRootViewModel.instance;
	drawer = <RadSideDrawer>view.getViewById(page, "sideDrawer");
	page.actionBarHidden = true;
}

export function goToEvents(): void {
	drawer.closeDrawer();
	if (EventViewModel.instance) {
		EventViewModel.instance.destroy();
	}
	frameModule.topmost().navigate({
		moduleName: "views/events/events"
	});
}

export function goToPlaylists(): void {
	if (EventViewModel.instance) {
		EventViewModel.instance.destroy();
	}
	drawer.closeDrawer();
	frameModule.topmost().navigate({
		moduleName: "views/playlists/playlists"
	});
}

export function goToFriends(): void {
	if (EventViewModel.instance) {
		EventViewModel.instance.destroy();
	}
	drawer.closeDrawer();
	frameModule.topmost().navigate({
		moduleName: "views/friends/friends"
	});
}

export function goToSocial(): void {
	if (EventViewModel.instance) {
		EventViewModel.instance.destroy();
	}
	drawer.closeDrawer();
	frameModule.topmost().navigate({
		moduleName: "views/social/social"
	});
}

export function goToParameters(): void {
	if (EventViewModel.instance) {
		EventViewModel.instance.destroy();
	}
	drawer.closeDrawer();
	frameModule.topmost().navigate({
		moduleName: "views/parameters/parameters"
	});
}

export function onDrawerOpening(args: DrawerStateChangingEventArgs) {
	utils.ad.dismissSoftInput();
}


application.run({
	moduleName: "app-root",
});