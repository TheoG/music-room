import { Observable } from "tns-core-modules/data/observable";
import { IconsCode } from "~/utils/Icons";
import { MusicRoomApi } from "~/utils/MusicRoomApi";
import { FriendshipApiCaller } from "~/models/Friendship";
import { StringValues } from "./utils/Values";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { PlaylistApiCaller } from "~/models/Playlist";
import { TrackApiCaller } from "./models/Track";
import * as appSettings from "tns-core-modules/application-settings";
import * as frameModule from "tns-core-modules/ui/frame"

export enum Theme {
	LIGHT = "light-theme",
	DARK = "dark-theme"
}

export class AppRootViewModel extends Observable {

	public theme: Theme;

	public static instance: AppRootViewModel = null;

	public apiKey: string = null;
	public userID: string = null;
	public activePlaylist: string = null;

	public me: UserAPI = null;
	public events: { [rights: string]: {[id: string]: EventAPI} } = null;
	public playlists: { [rights: string]: {[id: string]: PlaylistAPI} } = null;
	public friendships: FriendshipAPI[] = null;


	public friendshipApiCaller: FriendshipApiCaller = null;
	public playlistApiCaller: PlaylistApiCaller = null;
	public trackApiCaller: TrackApiCaller = null;

	constructor() {
		super();
		
		this.theme = Theme.DARK;
		
		this.apiKey = null;
		this.userID = null;
		this.activePlaylist = null;

		let darkMode: boolean = appSettings.getBoolean(StringValues.darkMode, true);
		
		if (!darkMode) {
			this.theme = Theme.LIGHT;
		}
		
		this.set("theme", this.theme);
		this.set("gearIcon", IconsCode.Gears);
			
		this.events = {
			owner: {},
			dj: {},
			guest: {}
		};
		this.playlists = {
			owner: {},
			editor: {},
			guest: {}
		};
		this.friendships = [];
		
		if (AppRootViewModel.instance == null) {
			AppRootViewModel.instance = this;
		}

		this.friendshipApiCaller = new FriendshipApiCaller();
		this.playlistApiCaller = new PlaylistApiCaller();
		this.trackApiCaller = new TrackApiCaller();
		new MusicRoomApi();

		if (!this.apiKey) {
			this.apiKey = appSettings.getString(StringValues.apiKey);
			this.userID = appSettings.getString(StringValues.userID);
			if (!this.apiKey) {
				return;
			}
		}
	}
	
	public isLightTheme(): boolean {
		return this.theme == Theme.LIGHT;
	}

	public setTheme(theme: Theme): void {
		this.theme = theme;
		appSettings.setBoolean(StringValues.darkMode, this.theme == Theme.DARK);
		this.set("theme", this.theme);
		let sideDrawer: RadSideDrawer = frameModule.topmost().parent.getViewById('sideDrawer');
		sideDrawer.className = this.theme;
	}
}

export interface BaseAPI {
	id: string;
}

export interface UserAPI extends BaseAPI {
	avatar: string;
	theme: string;
	apiKey: string;
	isPremium: boolean;
	email: string;
	displayName: string;
	firstName: string;
	lastName: string;
	friendships: string[];
	friends: string[];
	playlists: string[];
	musicalPreferences: string;
	facebookid: string;
}

export interface FriendshipAPI extends BaseAPI {
	pending: boolean;
	request: any;
	acceptedOn: string; // DateTime
	users: UserAPI[];
}



export interface TrackAPI extends BaseAPI {
	serviceid: number;
	service: string;
	likes: string[];
	dislikes: string[];
	alreadyPlayed: boolean;
	activeTrack: boolean;
}

export interface TrackDeezer {
	name: string;
	album: string;
	artist: string;
	coverImageURL: string;
	duration: number;
}

export interface PlaylistAPI extends BaseAPI {
	name: string;
	owner: string;
	events: EventAPI[];
	visibility: string;
	editing: string;
	sortingType: "move" | "vote";
	tracks: string[] | TrackAPI[];
	playedTracks: TrackAPI[];
	unplayedTracks: TrackAPI[];
	activeTrack: string;
	guests: string[] | UserAPI[];
	editors: string[] | UserAPI[];
}

export interface Schedule {
	start: string | Date;
	end: string | Date;
}

export class EventAPI {
	id: string;
	name: string;
	description: string;
	owner: UserAPI;
	visibility: string;
	votingSchedule: Schedule;
	eventSchedule: Schedule;
	guests: UserAPI[];
	djs: UserAPI[];
	playlists: PlaylistAPI[];
	eventPlaylists: PlaylistAPI[];
	coordinates: any;
	activePlaylist: string;
    votingActive: boolean;
    eventActive: boolean;
    trackPosition: number;
    lastTrackActionTimestamp: string;


	public static default(e: any): EventAPI {
		return {
			id: e.id,
			guests: e.guests,
			djs: e.djs,
			name: e.name,
			description: e.description,
			playlists: e.playlists,
			owner: e.owner,
			visibility: e.visibility,
			votingSchedule: e.votingSchedule ? {
				start: e.votingSchedule.start,
				end: e.votingSchedule.end
			} : null,
			eventSchedule: e.eventSchedule ? {
				start: e.eventSchedule.start,
				end: e.eventSchedule.end
			} : null,
			coordinates: null,
			activePlaylist: null,
			votingActive: false,
			eventActive: false,
			eventPlaylists: [],
			trackPosition: 0,
			lastTrackActionTimestamp: "0",
		}
	}
}