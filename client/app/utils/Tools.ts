import { UserAPI, AppRootViewModel, TrackAPI } from "~/app-root-view-model";

export function toHHMMSS(duration: string): string {
	let sec_num	= parseInt(duration, 10);
	let hours	= Math.floor(sec_num / 3600);
	let minutes	= Math.floor((sec_num - (hours * 3600)) / 60);
	let seconds	= sec_num - (hours * 3600) - (minutes * 60);

	let hoursStr: string = hours.toString();
	let minutesStr: string = minutes.toString();
	let secondsStr: string = seconds.toString();

	if (hours	< 10) {hoursStr	= "0" + hours;}
	if (minutes	< 10) {minutesStr	= "0" + minutes;}
	if (seconds	< 10) {secondsStr	= "0" + seconds;}

	if (hours < 1) {
		return minutesStr + ':' + secondsStr;
	} else {
		return hoursStr + ':' + minutesStr + ':' + secondsStr;
	}
}

export function formatDisplayName(user: UserAPI): string {
	if (user.displayName) {
		return user.displayName;
	} else if (user.firstName && user.lastName) {
		return user.firstName + " " + user.lastName;
	} else if (user.email) {
		return user.email;
	} else {
		return user.id;
	}
}

export function userIsOwner(instance: any): boolean {
	return (typeof instance.owner == "string" && instance.owner == AppRootViewModel.instance.me.id ||
		instance.owner.id == AppRootViewModel.instance.me.id);
}

export function userIsGuest(instance: any): boolean {
	return instance.guests.some(user => {
		return ((typeof user == 'string' && user == AppRootViewModel.instance.me.id) ||
				user.id == AppRootViewModel.instance.me.id);
	});
}

export function userIsEditor(instance: any): boolean {
	return instance.editors.some(user => {
		return ((typeof user == 'string' && user == AppRootViewModel.instance.me.id) ||
				user.id == AppRootViewModel.instance.me.id);
	});
}

export function userIsDJ(instance: any): boolean {
	return instance.djs.some(user => {
		return ((typeof user == 'string' && user == AppRootViewModel.instance.me.id) ||
				user.id == AppRootViewModel.instance.me.id);
	});
}

export function addLike(rights: string, eventID: string, playlistIndex: number, trackID: string): void {
	let found: boolean = false;
	for (let i: number = 0; i < AppRootViewModel.instance.events[rights][eventID].eventPlaylists[playlistIndex].playedTracks.length; i++) {
		let tr: TrackAPI = AppRootViewModel.instance.events[rights][eventID].eventPlaylists[playlistIndex].playedTracks[i];
		if (tr.id == trackID) {
			AppRootViewModel.instance.events[rights][eventID].eventPlaylists[playlistIndex].playedTracks[i].likes.push(AppRootViewModel.instance.me.id);
			found = true;
			break;
		}
	}
	if (!found) {
		for (let i: number = 0; i < AppRootViewModel.instance.events[rights][eventID].eventPlaylists[playlistIndex].unplayedTracks.length; i++) {
			let tr: TrackAPI = AppRootViewModel.instance.events[rights][eventID].eventPlaylists[playlistIndex].unplayedTracks[i];
			if (tr.id == trackID) {
				AppRootViewModel.instance.events[rights][eventID].eventPlaylists[playlistIndex].unplayedTracks[i].likes.push(AppRootViewModel.instance.me.id);
				found = true;
				break;
			}
		}
	}
}

export function removeLike(rights: string, eventID: string, playlistIndex: number, trackID: string): void {
	let found: boolean = false;
	for (let i: number = 0; i < AppRootViewModel.instance.events[rights][eventID].eventPlaylists[playlistIndex].playedTracks.length; i++) {
		let tr: TrackAPI = AppRootViewModel.instance.events[rights][eventID].eventPlaylists[playlistIndex].playedTracks[i];
		if (tr.id == trackID) {
			let arrayIndex: number = AppRootViewModel.instance.events[rights][eventID].eventPlaylists[playlistIndex].playedTracks[i].likes.indexOf(AppRootViewModel.instance.me.id);
			if (arrayIndex != -1) {
				AppRootViewModel.instance.events[rights][eventID].eventPlaylists[playlistIndex].playedTracks[i].likes.splice(arrayIndex, 1);
			}
			found = true;
			break;
		}
	}
	if (!found) {
		for (let i: number = 0; i < AppRootViewModel.instance.events[rights][eventID].eventPlaylists[playlistIndex].unplayedTracks.length; i++) {
			let tr: TrackAPI = AppRootViewModel.instance.events[rights][eventID].eventPlaylists[playlistIndex].unplayedTracks[i];
			if (tr.id == trackID) {
				let arrayIndex: number = AppRootViewModel.instance.events[rights][eventID].eventPlaylists[playlistIndex].unplayedTracks[i].likes.indexOf(AppRootViewModel.instance.me.id);
				if (arrayIndex != -1) {
					AppRootViewModel.instance.events[rights][eventID].eventPlaylists[playlistIndex].unplayedTracks[i].likes.splice(arrayIndex, 1);
				}
				found = true;
			}
		}
	}
}

export function addDislike(rights: string, eventID: string, playlistIndex: number, trackID: string): void {
	let found: boolean = false;
	for (let i: number = 0; i < AppRootViewModel.instance.events[rights][eventID].eventPlaylists[playlistIndex].playedTracks.length; i++) {
		let tr: TrackAPI = AppRootViewModel.instance.events[rights][eventID].eventPlaylists[playlistIndex].playedTracks[i];
		if (tr.id == trackID) {
			AppRootViewModel.instance.events[rights][eventID].eventPlaylists[playlistIndex].playedTracks[i].dislikes.push(AppRootViewModel.instance.me.id);
			found = true;
		}
	}
	if (!found) {
		for (let i: number = 0; i < AppRootViewModel.instance.events[rights][eventID].eventPlaylists[playlistIndex].unplayedTracks.length; i++) {
			let tr: TrackAPI = AppRootViewModel.instance.events[rights][eventID].eventPlaylists[playlistIndex].unplayedTracks[i];
			if (tr.id == trackID) {
				AppRootViewModel.instance.events[rights][eventID].eventPlaylists[playlistIndex].unplayedTracks[i].dislikes.push(AppRootViewModel.instance.me.id);
				found = true;
			}
		}
	}

}

export function removeDislike(rights: string, eventID: string, playlistIndex: number, trackID: string): void {
	let found: boolean = false;
	for (let i: number = 0; i < AppRootViewModel.instance.events[rights][eventID].eventPlaylists[playlistIndex].playedTracks.length; i++) {
		let tr: TrackAPI = AppRootViewModel.instance.events[rights][eventID].eventPlaylists[playlistIndex].playedTracks[i];
		if (tr.id == trackID) {
			let arrayIndex: number = AppRootViewModel.instance.events[rights][eventID].eventPlaylists[playlistIndex].playedTracks[i].dislikes.indexOf(AppRootViewModel.instance.me.id);
			if (arrayIndex != -1) {
				AppRootViewModel.instance.events[rights][eventID].eventPlaylists[playlistIndex].playedTracks[i].dislikes.splice(arrayIndex, 1);
			}
			found = true;
		}
	}
	if (!found) {
		for (let i: number = 0; i < AppRootViewModel.instance.events[rights][eventID].eventPlaylists[playlistIndex].unplayedTracks.length; i++) {
			let tr: TrackAPI = AppRootViewModel.instance.events[rights][eventID].eventPlaylists[playlistIndex].unplayedTracks[i];
			if (tr.id == trackID) {
				let arrayIndex: number = AppRootViewModel.instance.events[rights][eventID].eventPlaylists[playlistIndex].unplayedTracks[i].dislikes.indexOf(AppRootViewModel.instance.me.id);
				if (arrayIndex != -1) {
					AppRootViewModel.instance.events[rights][eventID].eventPlaylists[playlistIndex].unplayedTracks[i].dislikes.splice(arrayIndex, 1);
				}
				found = true;
			}
		};
	}
}
