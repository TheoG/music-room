import { request, getFile, getImage, getJSON, getString } from "tns-core-modules/http";
import * as appSettings from "tns-core-modules/application-settings";
import { StringValues } from "./Values";
import * as appversion from "nativescript-appversion";

import * as fs from "tns-core-modules/file-system";

import * as bgHttp from "nativescript-background-http";
import { AppRootViewModel } from "~/app-root-view-model";
import { device } from "tns-core-modules/platform/platform";
import * as Toast from 'nativescript-toast';

export class MusicRoomApi {

	static baseUrl: string = ""; //e2r10p12
	static port: string = "";
	static clientUrl: string = "";
	static sockerIOPort: string = "3030";


	// Users End Points
	static readonly users = "users/";
	static readonly me = "users/:userID/";
	static readonly userSettings = "users/:userID/settings/";
	static readonly userSearch = "users/search/";
	static readonly login = "login/";
	static readonly premium = "premium/";
	static readonly resetPassword = "resetpassword/";
	static readonly facebook = MusicRoomApi.users + "facebook/";
	
	// Friends End Points
	static readonly friends = "friends/";
	static readonly friendships = "friendships/";
	
	// Playlists End Points
	static readonly playlists = "playlists/";
	static readonly editors = "editors/";
	static readonly guests = "guests/";
	static readonly tracks = "tracks/";
	static readonly move = "move/";
	static readonly playlistsSearch = "playlists/search/";

	// Events End Points
	static readonly events = "events/";
	static readonly djs = "djs/";
	static readonly startVoting = "startvoting";
	static readonly endVoting = "endvoting";
	static readonly startEvent = "startevent";
	static readonly endEvent = "endevent";
	// Player Actions
	static readonly play = "play";
	static readonly pause = "pause";
	static readonly seek = "seek";

	private timeout: number;

	public static instance: MusicRoomApi = null;
	private appVersion: string = null;
	constructor() {

		let configFile = fs.knownFolders.currentApp().getFile("config.json");
		let data: any = JSON.parse(configFile.readTextSync());

		MusicRoomApi.baseUrl = data.serverName;
		MusicRoomApi.port = data.serverPort;
		MusicRoomApi.clientUrl = data.clientUrl;

		this.timeout = 1000;
		this.appVersion = appversion.getVersionNameSync();


		if (MusicRoomApi.instance == null) {
			MusicRoomApi.instance = this;
		}


	}

	async get(endPoint: string, success, error, parameters?: object): Promise<void> {
		endPoint = endPoint.replace(':userID', AppRootViewModel.instance.userID);
		
		await request({
			url: MusicRoomApi.baseUrl + ":" + MusicRoomApi.port + "/" + endPoint + "?" + this.encodeParameters(parameters, {[StringValues.apiKey]: AppRootViewModel.instance.apiKey}),
			method: "GET",
			timeout: this.timeout,
			headers: {
				"MusicRoomInfo-DeviceOS": device.os,
				"MusicRoomInfo-DeviceModel": device.model,
				"MusicRoomInfo-DeviceVersion": device.osVersion,
				"MusicRoomInfo-AppVersion": this.appVersion,
			}
		}).then(response => {
			if (response.statusCode == 200) {
				success(response.content.toJSON());
			} else {
				error(response.content.toJSON());
			}
		}, e => {
			error(e);
		});
	}

	async post(endPoint: string, data, success, error, parameters?: object): Promise<void> {
		
		endPoint = endPoint.replace(':userID', AppRootViewModel.instance.userID);

		await request({
			url: MusicRoomApi.baseUrl + ":" + MusicRoomApi.port + "/" + endPoint + "?" + this.encodeParameters(parameters, {[StringValues.apiKey]: AppRootViewModel.instance.apiKey}),
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				"MusicRoomInfo-DeviceOS": device.os,
				"MusicRoomInfo-DeviceModel": device.model,
				"MusicRoomInfo-DeviceVersion": device.osVersion,
				"MusicRoomInfo-AppVersion": this.appVersion,
			},
			timeout: this.timeout,
			content: JSON.stringify(data)
		}).then(response => {
			if (response.statusCode == 200) {
				success(response.content.toJSON());
			} else {
				error(response.content.toJSON());
			}
		}, e => {
			error(e);
		});
	}

	async put(endPoint: string, data, success, error, parameters?: object): Promise<void> {
		
		endPoint = endPoint.replace(':userID', AppRootViewModel.instance.userID);
		
		await request({
			url: MusicRoomApi.baseUrl + ":" + MusicRoomApi.port + "/" + endPoint + "?" + this.encodeParameters(parameters, {[StringValues.apiKey]: AppRootViewModel.instance.apiKey}),
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				"MusicRoomInfo-DeviceOS": device.os,
				"MusicRoomInfo-DeviceModel": device.model,
				"MusicRoomInfo-DeviceVersion": device.osVersion,
				"MusicRoomInfo-AppVersion": this.appVersion,
			},
			timeout: this.timeout,
			content: JSON.stringify(data),
			
		}).then(response => {
			if (response.statusCode == 200) {
				success(JSON.parse(response.content.raw));
			} else {
				error(response.content.toJSON());
			}
		}, e => {
			error(e);
		});
	}

	async delete(endPoint: string, data, success, error, parameters?: object): Promise<void> {
		endPoint = endPoint.replace(':userID', AppRootViewModel.instance.userID);
	
		await request({
			url: MusicRoomApi.baseUrl + ":" + MusicRoomApi.port + "/" + endPoint + "?" + this.encodeParameters(parameters, {[StringValues.apiKey]: AppRootViewModel.instance.apiKey}),
			method: "POST",
			headers: { 
				"Content-Type": "application/json",
				"X-HTTP-Method-Override": "DELETE",
				"MusicRoomInfo-DeviceOS": device.os,
				"MusicRoomInfo-DeviceModel": device.model,
				"MusicRoomInfo-DeviceVersion": device.osVersion,
				"MusicRoomInfo-AppVersion": this.appVersion,
			},
			timeout: this.timeout,
			content: JSON.stringify(data)
		}).then(response => {
			if (response.statusCode == 200) {
				success(response.content.toJSON());
			} else {
				error(response.content.toJSON());
			}
		}, e => {
			error(e);
		});
	}

	async sendFile(endPoint: string, filename: string, success, error, parameters?: object): Promise<void> {
		endPoint = endPoint.replace(':userID', AppRootViewModel.instance.userID);
		
		let name = filename.substr(filename.lastIndexOf("/") + 1);
		let url = MusicRoomApi.baseUrl + ":" + MusicRoomApi.port + "/" + endPoint + "?" + this.encodeParameters(parameters, {[StringValues.apiKey]: AppRootViewModel.instance.apiKey})
		let session: bgHttp.Session = bgHttp.session("avatar");
		
		let request: bgHttp.Request = {
			url,
            method: "PUT",
            headers: {
				"Content-Type": "application/octet-stream",
				"MusicRoomInfo-DeviceOS": device.os,
				"MusicRoomInfo-DeviceModel": device.model,
				"MusicRoomInfo-DeviceVersion": device.osVersion,
				"MusicRoomInfo-AppVersion": this.appVersion,
                "File-Name": name
            },
            description: "Uploading " + "avatar",
            androidAutoDeleteAfterUpload: false,
            androidNotificationTitle: 'NativeScript HTTP background'
        };
		
		
		let task: bgHttp.Task;
		const params = [
			{ name: "test", value: "value" },
			{ name: "avatar", filename: filename, mimeType: 'image/jpeg' }
		];
		try {
			task = session.multipartUpload(params, request);
		} catch (e) {
			Toast.makeText('Error with the file name. Please don\'t use any special character').show();
		}
		task.on("progress", progress => {
		});

		task.on("error", error => {
		});
		task.on("responded", response => {
			success(JSON.parse(response.data));
		});

		task.on("complete", response => {
		});
	}

	private encodeParameters(data: object, concat?: object): string {
		const ret = [];
		try {
			for (let d in data) {
				if (d == 'apiKey' && !data[d]) {
					continue;
				}
				ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
			}
			if (concat) {
				for (let d in concat) {
					if (d == 'apiKey' && !concat[d]) {
						continue;
					}
					ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(concat[d]));
				}
			}
		} catch(e) {
		}
		return ret.join('&');
	}
}