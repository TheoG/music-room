import { request, getFile, getImage, getJSON, getString } from "tns-core-modules/http";
import * as appSettings from "tns-core-modules/application-settings";
import { clipPathProperty } from "tns-core-modules/ui/page/page";
import { StringValues } from "./Values";

export class DeezerApi {

	static baseUrl: string = "https://api.deezer.com";
	static port: string = "443";

	static readonly searchEP = "search/";
	static readonly trackEP = "track/";

	private timeout: number;

	constructor() {
		this.timeout = 2000;
	}

	async get(endPoint: string, success, error, parameters?: object): Promise<void> {
		await request({
			url: DeezerApi.baseUrl + ":" + DeezerApi.port + "/" + endPoint + "?" + this.encodeParameters(parameters),
			method: "GET",
			timeout: this.timeout
		}).then(response => {
			if (response.statusCode == 200) {
				success(response.content.toJSON());
			} else {
				error(response.content.toJSON());
			}
		}, e => {
			error(e);
		});
	}

	async post(endPoint: string, data, success, error, parameters?: object): Promise<void> {
		await request({
			url: DeezerApi.baseUrl + ":" + DeezerApi.port + "/" + endPoint + "?" + this.encodeParameters(parameters),
			method: "POST",
			headers: { "Content-Type": "application/json" },
			timeout: this.timeout,
			content: JSON.stringify(data)
		}).then(response => {
			if (response.statusCode != 200) {
				error(response);
			}
			success(response.content.toJSON());
		}, e => {
			error(e);
		});
	}

	async put(endPoint: string, data, success, error, parameters?: object): Promise<void> {
		await request({
			url: DeezerApi.baseUrl + ":" + DeezerApi.port + "/" + endPoint + "?" + this.encodeParameters(parameters),
			method: "PUT",
			headers: { "Content-Type": "application/json" },
			timeout: this.timeout,
			content: JSON.stringify(data)
		}).then(response => {
			success(response.content.toJSON());
		}, e => {
			error(e);
		});
	}

	async delete(endPoint: string, success, error, parameters?: object): Promise<void> {
		await request({
			url: DeezerApi.baseUrl + ":" + DeezerApi.port + "/" + endPoint + "?" + this.encodeParameters(parameters),
			timeout: this.timeout,
			method: "DELETE"
		}).then(response => {
			success(response.content.toJSON());
		}, e => {
			error(e);
		});
	}

	private encodeParameters(data: object, concat?: object): string {
		const ret = [];
		for (let d in data) {
			ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
		}
		if (concat) {
			for (let d in concat) {
				ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(concat[d]));
			}
		}
		return ret.join('&');
	}

}