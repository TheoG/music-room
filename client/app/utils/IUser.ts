export interface IUser {
	id: string;
	email: string;
	displayName: string;
	firstName: string;
	lastName: string;
	friends: string[];
}