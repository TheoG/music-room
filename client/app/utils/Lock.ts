export class Lock {

	private counter: number;
	private waiters: any[];

	constructor() {
		this.counter = 1;
		this.waiters = [];
	}

	public acquire(next): void {
		if (this.counter > 0) {
			this.counter--;
			next();
		} else {
			this.waiters.push(next);
		}
	}
	
	public release(): void {
		if (this.waiters.length > 0) {
			let next = this.waiters.pop();
			next();
		} else {
			this.counter++;
		}
	}

	public isLocked(): boolean {
		return this.counter > 0;
	}
}

export class Locker {

	private static locks = new Map();

	static acquire(lockerName: string, next): void {
		if (!Locker.locks.has(lockerName)) {
			Locker.locks.set(lockerName, new Lock());
		}
		Locker.locks.get(lockerName).acquire(next);
	}
	
	static release(lockerName: string): void {
		if (!Locker.locks.has(lockerName)) {
			return ;
		}		
		Locker.locks.get(lockerName).release();
	}

	static isLocked(lockerName: string): boolean {
		if (!Locker.locks.has(lockerName)) {
			return ;
		}		
		Locker.locks.get(lockerName).isLocked();
	}
}