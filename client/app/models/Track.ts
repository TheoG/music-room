import { TrackAPI } from "~/app-root-view-model";
import * as appSettings from "tns-core-modules/application-settings"
import { DeezerApi } from "~/utils/DeezerApi";

export interface DeezerTrack {
	title: string;
	albumTitle: string;
	artistName: string;
	albumCover_medium: string;
	duration: number;
}

// title: string;
// album.title: string;
// artist.name: string;
// album.cover_medium: string;
// duration: string;


export class TrackApiCaller {

	public static instance: TrackApiCaller = null;
	private deezerApi: DeezerApi = null;

	constructor() {
		if (!TrackApiCaller.instance) {
			TrackApiCaller.instance = this;
		}

		this.deezerApi = new DeezerApi();
	}

	public async getTrack(serviceID: number): Promise<DeezerTrack> {
		let stringTrack: string = appSettings.getString('deezer_track_' + serviceID, 'none');
		let track: DeezerTrack = null;
		if (stringTrack == 'none') {
			await new Promise((resolve, reject) => {
				this.deezerApi.get(DeezerApi.trackEP + serviceID, response => {
					if (response.hasOwnProperty('error')) {
						resolve();
						return ;
					}
					track = {
						title: response.title,
						albumTitle: response.album.title,
						artistName: response.artist.name,
						albumCover_medium: response.album.cover_medium,
						duration: response.duration
					};

					appSettings.setString('deezer_track_' + serviceID, JSON.stringify(track));
					
					resolve();
				}, error => {
					reject();
				});
			});
		} else {
			track = JSON.parse(stringTrack);
		}
		return track;
	}

}
