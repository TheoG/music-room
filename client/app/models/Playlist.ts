import { MusicRoomApi } from "~/utils/MusicRoomApi";
import { AppRootViewModel, FriendshipAPI, PlaylistAPI } from "~/app-root-view-model";

export class PlaylistApiCaller {
	public static instance: PlaylistApiCaller = null;

	constructor() {
		if (!PlaylistApiCaller.instance) {
			PlaylistApiCaller.instance = this;
		}
	}

	public async getPlaylist(playlistID: string, apiCall: boolean): Promise<PlaylistAPI> {

		let rights: string = null;

		if (apiCall) {
			await MusicRoomApi.instance.get(MusicRoomApi.playlists + playlistID, response => {
				if (response.owner.id == AppRootViewModel.instance.me.id) {
					rights = 'owner';
					AppRootViewModel.instance.playlists.owner[response.id] = response;
				} else if (response.editors.some(editor => editor.id == AppRootViewModel.instance.me.id) || response.editing == 'PUBLIC') {
					rights = 'editor';
					AppRootViewModel.instance.playlists.editor[response.id] = response;
				} else if (response.guests.some(guest => guest.id == AppRootViewModel.instance.me.id) || response.visibility == 'PUBLIC') {
					rights = 'guest';
					AppRootViewModel.instance.playlists.guest[response.id] = response;
				}
				
			}, error => {
			});
		} else {
			if (playlistID in AppRootViewModel.instance.playlists.owner) {
				rights = 'owner';
			} else if (playlistID in AppRootViewModel.instance.playlists.editor) {
				rights = 'editor';
			} else if (playlistID in AppRootViewModel.instance.playlists.guest) {
				rights = 'guest';
			}
		}

		return AppRootViewModel.instance.playlists[rights][playlistID];
	}
}
