import { MusicRoomApi } from "~/utils/MusicRoomApi";
import { AppRootViewModel, FriendshipAPI } from "~/app-root-view-model";

export class FriendshipApiCaller {
	public static instance: FriendshipApiCaller = null;

	constructor() {
		if (!FriendshipApiCaller.instance) {
			FriendshipApiCaller.instance = this;
		}
	}

	public async getFriendships(apiCall: boolean): Promise<FriendshipAPI[]> {

		if (apiCall || AppRootViewModel.instance.friendships.length == 0) {
			await MusicRoomApi.instance.get(MusicRoomApi.friendships, response => {
				AppRootViewModel.instance.friendships = response.friendships;
			}, error => {
			});
		}

		return AppRootViewModel.instance.friendships;
	}

	public async getFriendship(apiCall: boolean, friendshipid: string): Promise<FriendshipAPI> {
		if (apiCall) {
			await MusicRoomApi.instance.get(MusicRoomApi.friendships + friendshipid, response => {

				if (response.hasOwnProperty('errorCode')) {
					return null;
				}

				for (let i: number = 0; i < AppRootViewModel.instance.friendships.length; i++) {
					let friendship: FriendshipAPI = AppRootViewModel.instance.friendships[i];
					if (friendship.id == friendshipid) {
						AppRootViewModel.instance.friendships[i] = response;
						return AppRootViewModel.instance.friendships[i];
					}
				}

				AppRootViewModel.instance.friendships.push(response);
				return AppRootViewModel.instance.friendships[AppRootViewModel.instance.friendships.length - 1];
			}, error => {
			});
		} else {
			for (let i: number = 0; i < AppRootViewModel.instance.friendships.length; i++) {
				let friendship: FriendshipAPI = AppRootViewModel.instance.friendships[i];
				if (friendship.id == friendshipid) {
					return AppRootViewModel.instance.friendships[i];
				}
			}
		}
	}
}
