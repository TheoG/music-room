import { Permission } from './deezer.common';
import { android as androidApp } from "tns-core-modules/application";

declare var com: any;

export class DialogListener {
	private dialogListener: any;

	constructor(complete, cancel, exception) {
		
		this.dialogListener = new com.deezer.sdk.network.connect.event.DialogListener({
			onComplete(values: any) {
				complete(values);
			},

			onCancel() {
				cancel();
			},

			onException(e: any) {
				exception(e);
			}
		});
	}

	getDeezerDialogListener(): any {
		return this.dialogListener;
	}
}

export class DeezerConnect {

	public deezerConnect: any;

	constructor(appID: string) {
		this.deezerConnect = com.deezer.sdk.network.connect.DeezerConnect.forApp(appID).withContext(androidApp.context).build();
	}

	public androidAuthorize(activity: any, permissions: Permission[], listener: DialogListener): void {
		let strPerms: string[] = [];
		this.deezerConnect.authorize(activity, strPerms, listener.getDeezerDialogListener());
	}

	public getAccessExpires(): number {
		return this.deezerConnect.getAccessExpires();
	}

	public isSessionValid(): boolean {
		return this.deezerConnect.isSessionValid();
	}

	public logout(): void {
		this.deezerConnect.logout(androidApp.context);
	}
}

export class AlbumPlayer {
	private albumPlayer: any;
	private albumID: number;

	constructor(deezerConnect: DeezerConnect) {
		try {
			this.albumPlayer = new com.deezer.sdk.player.AlbumPlayer(androidApp.nativeApp, deezerConnect.deezerConnect, new com.deezer.sdk.player.networkcheck.WifiAndMobileNetworkStateChecker());
		} catch (e) {
		}
	}

	setAlbumID(albumId: number): void {
		this.albumID = albumId;
	}

	play(): void {
		this.albumPlayer.playAlbum(this.albumID);
	}

	stop(): void {

	}
}


export class TrackPlayer {
	private trackPlayer: any;
	private trackID: number;
	private trackStarted: boolean;

	constructor(deezerConnect: DeezerConnect) {
		try {
			this.trackPlayer = new com.deezer.sdk.player.TrackPlayer(androidApp.nativeApp, deezerConnect.deezerConnect, new com.deezer.sdk.player.networkcheck.WifiAndMobileNetworkStateChecker());
		} catch (e) {
		}
		this.trackStarted = false;
	}

	setTrackID(trackID: number): void {
		if (!trackID) {
			return;
		}
		this.trackID = trackID;
		this.trackStarted = false;
	}

	play(): void {

		if (!this.trackPlayer) {
			return ;
		}

		if (this.trackStarted) {
			this.trackPlayer.play();
		} else {
			this.trackPlayer.playTrack(this.trackID);
			this.trackStarted = true;
		}
	}

	pause(): void {

		if (!this.trackPlayer) {
			return ;
		}
		this.trackPlayer.pause();
	}

	stop(): void {

		if (!this.trackPlayer) {
			return ;
		}
		
		this.trackPlayer.stop();
		this.trackPlayer.release();
		this.trackStarted = false;
	}

	addProgressListener(callback): void {
		this.trackPlayer.addOnPlayerProgressListener(new com.deezer.sdk.player.event.OnPlayerProgressListener({
			onPlayerProgress(l: number): void {
				callback(l);
			}
		}))
	}

	setProgressListenerInterval(interval: number): void {
		if (this.trackPlayer) {
			this.trackPlayer.setPlayerProgressInterval(interval);
		}
	}

	seek(position: number): void {
		this.trackPlayer.seek(position);
	}
}

export class SessionStore {
	private sessionStore: any;

	constructor() {
		this.sessionStore = new com.deezer.sdk.network.connect.SessionStore();
	}

	save(deezerConnect: DeezerConnect): boolean {
		return this.sessionStore.save(deezerConnect.deezerConnect, androidApp.context);
	}

	restore(deezerConnect: DeezerConnect): boolean {
		return this.sessionStore.restore(deezerConnect.deezerConnect, androidApp.context);
	}

	clear(): boolean {
		return this.sessionStore.clear(androidApp.context);
	}
}