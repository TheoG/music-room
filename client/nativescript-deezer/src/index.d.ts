export declare class DialogListener {
    private dialogListener;
    constructor(complete: any, cancel: any, exception: any);
}

export declare class DeezerConnect {
    deezerConnect: any;
    constructor(appID: string);
    androidAuthorize(activity: any, permissions: any, listener: any): void;
	getAccessExpires(): number;
    isSessionValid(): boolean;
    logout(): void;
}

export declare class AlbumPlayer {
    private albumPlayer;
    private albumID;
    constructor(deezerConnect: DeezerConnect);
    setAlbumID(albumId: number): void;
    play(): void;
    stop(): void;
}
export declare class TrackPlayer {
    private trackPlayer;
    private trackID;
    private trackStarted;
    constructor(deezerConnect: DeezerConnect);
    setTrackID(trackID: number): void;
    play(): void;
    pause(): void;
    stop(): void;
	addProgressListener(callback: any): void;
	setProgressListenerInterval(interval: number): void;
    seek(position: number): void;	
}
export declare class SessionStore {
    private sessionStore;
    constructor();
    save(deezerConnect: DeezerConnect): boolean;
	restore(deezerConnect: DeezerConnect): boolean;
    clear(): boolean;	
}


export {};
